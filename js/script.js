$(document).ready(function(){
	s = new slider(".galerie");
});

var slider = function(id){
	//alert(id);
	var self = this;
	this.div = $(id);
	//alert(this.div.html());
	this.slider = this.div.find(".slider");
	this.largeurCache = this.div.width();
	//alert(this.largeurCache);
	this.largeur = 0;
	this.div.find('a').each(function(){
		self.largeur+=$(this).width();
		self.largeur+=parseInt($(this).css("padding-left"));
		self.largeur+=parseInt($(this).css("padding-right"));
		self.largeur+=parseInt($(this).css("margin-left"));
		self.largeur+=parseInt($(this).css("margin-right"));
	});
	//alert(self.largeur);
	this.prec = this.div.find(".prec");
	this.suiv = this.div.find(".suiv");
	this.saut = this.largeurCache/5;
	this.nbEtapes = Math.ceil(this.largeur/this.saut - this.largeurCache/this.saut);
	this.courant = 0;
	//alert(this.nbEtapes);
	this.suiv.click(function(){
		if(self.courant<=self.nbEtapes){
			self.courant++;
			self.slider.animate({
				left:-self.courant*self.saut
				//left:-self.saut
			},1000);
		}
	});
	this.prec.click(function(){
		if(self.courant>0){
			self.courant--;
			self.slider.animate({
				left:-self.courant*self.saut
				//left:-self.saut
			},1000);
		}
	});
}