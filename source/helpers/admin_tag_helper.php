<?php 
/*
* admin_tag_helper.php
* Update @ 17-4-2015 
* Author: Chim Pesal
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_tag_join_link($post_id){
	$ci =& get_instance();
	$ci->db->select();
	$ci->db->from('tbl_tags tag');
	$ci->db->join('tbl_links link','tag.tag_id = link.tag_id');
	$ci->db->where('link.post_id',$post_id);
	$ci->db->group_by('tag_name');
	$query = $ci->db->get();
	$result = $query->result_array();
	$output = '';
	  if(!empty($result)){
          foreach ($result as $tag) {
              $tag_name = $tag['tag_name'];
              $output .= '<a href="'.base_admin_url().'publish/post/search?tag_id='.$tag['tag_id'].'" title="'.$tag_name.'"><span class="badge badge-primary"> '.$tag_name.' </span></a>';
          }
      }
     return $output;
}

function get_all_tag_by_post_id($post_id){
	$ci =& get_instance();
	$ci->db->select('tag_name');
	$ci->db->from('tbl_tags tag');
	$ci->db->join('tbl_links link','tag.tag_id = link.tag_id');
	$ci->db->where('link.post_id',$post_id);
	$ci->db->group_by('tag_name');
	$query = $ci->db->get();
	$result = $query->result_array();
	$out = '';
	foreach ($result as $value) {
		$out .= $value['tag_name'].',';
	}
	return $out;
}


