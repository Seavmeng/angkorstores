<?php 
	if (!function_exists('selectCategories')){
	function selectCategories(){
		$ci =& get_instance();
		//$id=get_permission();
		$ci->db->select();
		$ci->db->from('tbl_categories');
	//	$ci->db->limit(8);
		$ci->db->where(array('parent_id'=>0,'status'=>1, 'category_id !='=>'456'));
		$ci->db->order_by('category_name','asc');
		$result = $ci->db->get();
		$catArray = $result->result_array();
		foreach ($catArray as $val){
			$catName = sqlreturn($val['category_name']);
			$catId = sqlreturn($val['category_id']); 
			?>
		<li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown" style="background: #fff;">
			<a title="Computers &amp; Accessories" data-hover="dropdown" href="<?php echo base_url().'category/'.$val['permalink'];?>" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true"><?php echo $catName; ?></a>
			<ul role="menu" class=" dropdown-menu">
				<li class="menu-item animate-dropdown menu-item-object-static_block">
					<div class="yamm-content">
						<div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
							<div class="col-sm-12">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="wpb_single_image wpb_content_element vc_align_left">
											<figure class="wpb_wrapper vc_figure">
												<div class="vc_single_image-wrapper vc_box_border_grey">
												
												</div>
											</figure>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="vc_column-inner ">
									<div class="wpb_wrapper">
										<div class="wpb_text_column wpb_content_element ">
											<div class="wpb_wrapper">
												<ul>
													<li class="nav-title"><?php echo $catName;?> Maket</li>
													<?php echo selectSubCategories($catId); ?>
					</div>
				</li>
				</ul>
		</li>
		
		<?php }
		
	}
}
if(!function_exists('selectSubCategories')){
	function selectSubCategories($parentId){
		$ci =& get_instance();
		//$id = get_permission();
		$ci->db->select();
		$ci->db->from('tbl_categories');
		$ci->db->where(array('parent_id'=>$parentId));
		$ci->db->order_by('category_name','DESC');
		$result= $ci->db->get();
		$subCatArray = $result->result_array(); 
				
		foreach($subCatArray as $val){
			$catName= sqlreturn($val['category_name']);
			
			
			$allsub_cate=allsub_cate($val['category_id']);
			echo '
			
							<div class="col-sm-3">
								<div class="vc_column-inner ">
									<div class="wpb_wrapper">
										<div class="wpb_text_column wpb_content_element ">
											<div class="wpb_wrapper">
											<li class="nav-title"><a href="'.base_url().'category/'.$val['permalink'].'">' .$catName. '</a></li>
			<ul>';
			foreach($allsub_cate as $data){
				echo '<li><a href="'.base_url().'category/'.$data['category_id'].'">'.$data['category_name'].'</a></li>';
			}
			echo '</ul>
			</div>
										</div>
									</div>
								</div>
							</div>
	
						
			';
		 } ?>
		 
		 </ul>

											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
		 <?php
	}
	
}
function allsub_cate($cate_id){
	$ci =& get_instance();
	//$id = get_permission();
	$ci->db->select();
	$ci->db->from('tbl_categories');
	$ci->db->where(array('parent_id'=>$cate_id));
	$ci->db->order_by('category_id','DESC');
	$result= $ci->db->get();
	return $result->result_array(); 
}
function sidebarCategories(){
	$me =& get_instance();
	$me->db->select();
	$me->db->from('tbl_categories');
	$me->db->where('parent_id', 0);
	$me->db->where('category_id >',1);
	$qry = $me->db->get();
	return $qry->result_array();
}
function sidebarSubCategories(){
	$me =& get_instance();
	$me->db->select();
	$me->db->from('tbl_categories');
	$me->db->where('parent_id >',0);
	$qry=$me->db->get();
	return $qry->result_array();
}
/* 
	Author khaing 
	Vendor categories menu
*/
function sidebarVendorCategories(){
	$me =& get_instance();
	$me->db->select();
	$me->db->from('sys_categories_vendor');
	$me->db->join('tbl_categories','tbl_categories.category_id=sys_categories_vendor.category_id');
	$me->db->where('tbl_categories.parent_id', 0);
	$me->db->where('tbl_categories.category_id >',1);
	$qry = $me->db->get();
	return $qry->result_array();
}
function sidebarVendorSubCategories(){
	$me =& get_instance();
	$page = $me->uri->segment(1);
	//var_dump($page);
	$me->db->select();
	$me->db->from('sys_categories_vendor');
	$me->db->join('tbl_categories','tbl_categories.category_id = sys_categories_vendor.category_id');
	$me->db->join('sys_users','sys_users.user_id=sys_categories_vendor.user_id');
	$me->db->join('sys_users_detail','sys_users_detail.user_id=sys_users.user_id');
	$me->db->where('sys_categories_vendor.parent_id >',0);
	$me->db->where('sys_users_detail.page_name',$page);
	$qry=$me->db->get();
	return $qry->result_array();
}
function countItemVendor($param){
	$me =& get_instance();
	$page = $me->uri->segment(1);
	$me->db->select('category_id');
	$me->db->from('sys_relationship');
	$me->db->join('sys_product','sys_product.product_id=sys_relationship.post_id');
	$me->db->join('sys_users','sys_users.user_id=sys_product.user_id');
	$me->db->join('sys_users_detail','sys_users_detail.user_id=sys_users.user_id');
	$me->db->where('category_id',$param);
	$me->db->where('sys_users_detail.page_name',$page);
	$row_num = $me->db->get();
	return $row_num->num_rows();
}
// function pro_category($paramet){
// 	$ci = & get_instance();
// 		if($paramet == ""){
// 			redirect(base_url());
// 		}
// 		$data['product']=$ci->categories_model->category($paramet);
// 		$data['cat_title'] = $ci->categories_model->getCategoryTitle($paramet);
// 		$data['recommend'] = $ci->moproduct->recommend_product();
// 		$data['latestPro'] = $ci->categories_model->getLatestPro();
// 		$category=$data['product'];
// 		if(empty($category)){
// 			$ci->load->view('home/recommend', $data);
// 		}
// 		else{
// 		$ci->load->view('home/product_category', $data);
// 		}
// 	}/*End*/

function postedCat($param){
	$me =& get_instance();
	//$me->db->query('select * from tbl_categories join sys_relationship on tbl_categories.category_id = sys_relationship.category_id '
	//				. 'join sys_inventory on sys_relationship.post_id = sys_inventory.product_id where '
	//				. 'sys_inventory.product_id = '. $param);
	$me->db->select()->from('tbl_categories cat')->join('sys_relationship rel','cat.category_id = rel.category_id')->join('sys_inventory inven','rel.post_id = inven.product_id');
	$me->db->where('inven.product_id', $param);
	$qry = $me->db->get();
	return $qry->result_array();
}
function countItem($param){
	$me =& get_instance();
	$me->db->select('category_id');
	$me->db->from('sys_relationship');
	$me->db->where('category_id',$param);
	$row_num = $me->db->get();
	return $row_num->num_rows();
}
function footerMenu(){
	$me =& get_instance();
	$me->db->select();
	$me->db->from('tbl_categories');
	$me->db->where('parent_id',0);
	$me->db->where('category_id >',1);
	$me->db->limit(7,0);
	$qry = $me->db->get();
	return $qry->result_array();
}
function footerMenu2(){
	$me =& get_instance();
	$me->db->select();
	$me->db->from('tbl_categories');
	$me->db->where('parent_id',0);
	$me->db->where('category_id !=', 456);
	$me->db->where('category_id >',1);
	$me->db->limit(7,7);
	$qry = $me->db->get();
	return $qry->result_array();
}
function searchCat(){
	$me =& get_instance();
	$me->db->select();
	$me->db->from('tbl_categories');
	//$me->db->where('parent_id',0);
	$me->db->order_by('category_name');
	$qry = $me->db->get();
	return $qry->result_array();
	
}
function mainCatFilter(){
	$me =& get_instance();
	$me->db->select();
	$me->db->from('tbl_categories');
	$me->db->where('parent_id',0);
	$qry = $me->db->get();
	$resultArray = $qry->result_array();
	
	$output = "";
	foreach($resultArray as $row){
		$output = '<option';
	}
	
	
}
function subCatFilter($param){
	
}
	
