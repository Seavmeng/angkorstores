<?php 
/*
* admin_menu_helper.php
* Update @ 27-3-2015 
* Author: Chim Pesal
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function base_admin_url(){
	return base_url().config_item('admin_dir');
}

function base_admin_css_url(){
	return base_url().config_item('admin_css_dir');
}

function base_admin_js_url(){
	return base_url().config_item('admin_js_dir');
}

function base_admin_img_url(){
	return base_url().config_item('admin_img_dir');
}

function base_admin_template_url(){
	return base_url().config_item('admin_template_dir');
}

function base_admin_assets_url(){
	return base_url().config_item('admin_assets_dir');
}

function base_upload_img_url(){
	return base_url().config_item('upload_img_dir');
}
function has_error($label,$msg='has-error'){
	echo form_error($label) == ''? '': $msg;
}

function btn_action($edit="", $delete="", $discount=""){
	$btn = '<a href="'.base_admin_url().$edit.'" class="btn default btn-xs purple" title="'.word_r('edit').'"><i class="fa fa-edit"></i> '.word_r('edit').'</a>';
	$btn .= '<a href="'.base_admin_url().$delete.'" onclick="return confirm(\''.word_r('msg_confirm_delete').'\')" class="btn default btn-xs red" title="'.word_r('delete').'"><i class="fa fa-trash-o"></i> '.word_r('delete').' </a>';
	if($discount!=""){
		$btn .= '<a href="'.base_admin_url().$discount.'" class="btn btn-xs btn-primary" title="Discount">Discount</a>';
	}
	return $btn;
}

function btn_actions($create="", $view="",$action="view"){
	$view_active = $action == 'view'? 'active': '';
	$create_active = $action == 'create'? 'active': '';
	$eidt_active = $action == 'update'? 'active': '';

	$btn = '<a href="'.base_admin_url().$create.'" class="btn btn-default '.$create_active.'" title="'.word_r('create').'"><i class="fa fa-plus"></i> '.word_r('create').'</a>';
	$btn .= '<a href="'.base_admin_url().$view.'"  class="btn btn-default '.$view_active.'" title="'.word_r('view').'"><i class="fa fa-th"></i> '.word_r('view').'</a>';
	if(!empty($eidt_active)){
		$btn .= '<a href="#"  class="btn btn-default '.$eidt_active.'" title="'.word_r('update').'"><i class="fa fa-pencil"></i> '.word_r('update').'</a>';	
	}else{
			$btn .= '<a href="#"  class="btn btn-default disabled" title="'.word_r('update').'"><i class="fa fa-pencil"></i> '.word_r('update').'</a>';
	}
	$btn .= '<a href="#"  class="btn btn-icon-only btn-default fullscreen" data-original-title="" title=""></a>';
	return $btn;
}
//success
//info
//warning
//danger
function show_msg($errors,$type='success',$msg=''){
	if($errors !=""){
		$output = '';
		$output .= '<div class="alert alert-'.$type.'" role="alert">';
       	$output .= $errors;
       	$output .= $msg;
        $output .= '</div>';
        return $output;
    }
}
function show_error_msg($errors,$type='danger',$msg=''){
	if($errors !=""){
		$output = '';
		$output .= '<div class="alert alert-'.$type.'" role="alert">';
       	$output .= $errors;
       	$output .= $msg;
        $output .= '</div>';
        return $output;
    }
}
function check_active($active=""){
	if($active ==1 ){
		$btn = '<span class="label label-sm label-success label-mini">'.word_r('active').'</span>';
	}else{
		$btn = '<span class="label label-sm label-danger label-mini">'.word_r('inactive').'</span>';
	}
	return $btn;
}

function check_visibility($visibility=""){
	if($visibility ==1 ){
		$btn = '<span class="label label-sm label-success label-mini">'.word_r('visibility_public').'</span>';
	}else{
		$btn = '<span class="label label-sm label-danger label-mini">'.word_r('visibility_private').'</span>';
	}
	return $btn;
}


function check_public($status="",$date=""){
	if($status == 1 ){
		$btn = '<span class="label label-sm label-success label-mini">'.word_r('public').'</span>';
	}else{
		$btn = '<span class="label label-sm label-warning label-mini">'.word_r('draft').'</span>';
	}
	
	if( $date == 0) {
		$btn .= '&nbsp;<span class="label label-sm label-danger label-mini">Expired</span>';
	}
	return $btn;
}

function breadcrumb(){
	$ci =& get_instance();
	$url2 = $ci->uri->segment(2);
	$url3 = $ci->uri->segment(3);
	$url4 = $ci->uri->segment(4);
	$url5 = $ci->uri->segment(5);

	if(!empty($url3)){
		if(is_numeric($url3)){
		$word3 = $url3;
		}else{
			$word3 = word_r($url3);
		}

	}
	
	if(!empty($url4)){
		if(is_numeric($url4)){
		$word4 = $url4;
		}else{
			$word4 = word_r($url4);
		}
	}
	
	if(!empty($url5)){
		if(is_numeric($url5)){
		$word5 = $url5;
		}else{
			$word5 = word_r($url5);
		}
	}

	$h = '<ul class="page-breadcrumb">
			<li>
				<i class="fa fa-dashboard"></i>
				<a href="'.base_admin_url().'dashboard'.'">'.word_r('dashboard').'</a>
			</li>';
			if(!empty($url2)){
				$h .='<li >
						<i class="fa fa-angle-right"></i>
						<a href="'.base_admin_url().$url2.'">'.word_r($url2).'</a>
						
					</li>';
			}
			if(!empty($url3)){
				$h .='<li >
						<i class="fa fa-angle-right"></i>
						<a href="'.base_admin_url().$url2.'/'.$url3.'">'.$word3.'</a>
					</li>';
			}
			if(!empty($url4)){
				$h .='<li >
						<i class="fa fa-angle-right"></i>
						<a href="'.base_admin_url().$url2.'/'.$url3.'/'.$url4.'">'.$word4.'</a>
					</li>';
			}
			if(!empty($url5)){
				$h .='<li >
						<i class="fa fa-angle-right"></i>
						<a href="'.base_admin_url().$url2.'/'.$url3.'/'.$url4.'/'.$url5.'">'.$word5.'</a>
					</li>'; 
			} 
			
		
	$h .='</ul>';
	return $h;
}



function admin_image_gallery($image, $title=""){
	$o ='<div id="blueimp-gallery" class="blueimp-gallery">
                                    <!-- The container for the modal slides -->
                                    <div class="slides"></div>
                                    <!-- Controls for the borderless lightbox -->
                                    <h3 class="title"></h3>
                                    <a class="prev">‹</a>
                                    <a class="next">›</a>
                                    <a class="close">×</a>
                                    <a class="play-pause"></a>
                                    <ol class="indicator"></ol>
                                    <!-- The modal dialog, which will be used to wrap the lightbox content -->
                                    <div class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title"></h4>
                                                </div>
                                                <div class="modal-body next"></div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default pull-left prev">
                                                        <i class="glyphicon glyphicon-chevron-left"></i>
                                                        Previous
                                                    </button>
                                                    <button type="button" class="btn btn-primary next">
                                                        Next
                                                        <i class="glyphicon glyphicon-chevron-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                           </div>';
    $o .= '<div id="links"> ';
    if(!empty($image)){
    	 $o .= anchor(base_url().'images/products/'.str_replace('thumb/', '', $image),' 
    				<img src="'.base_url().'images/products/'.$image.'" width="40" height="40" class="">',
    			array('data-gallery'=>null, 'title' => $title));
    }
   
    $o .= ' </div>';
    return $o;
}

//***************** drop down **************************//

function drop_down($fields,$table_name,$selected=null,$translate=false,$where=null){
	$ci =& get_instance();
	$ci->db->select($fields)->from($table_name);
	if(!empty($where)){
		$this->db->where($where);
	}
	$query = $ci->db->get();
	$result = $query->result_array();
	//$options = array();
	$o ='';
	$o .= '<select name="'.$fields[0].'" class="form-control">';
	$o .= '<option value=""></option>';
	foreach($result as $value){
	      $o .= '<option value="'.$value[$fields[0]].'"';
	      if(!empty($selected)){
	      	 if($value[$fields[0]] == $selected){
	          	$o .= 'selected ';
	          }
	      }
	      $o .= '>';
	      if($translate){
	      	$o .= word_r($value[$fields[1]]);
	      }else{
	      	$o .= $value[$fields[1]];
	      }
	      $o .= '</option>';
	}
	$o .= '</select>';

return $o;
}
//***************** drop down **************************//

function utf8_uri_encode( $utf8_string, $length = 0 ) {
    $unicode = '';
    $values = array();
    $num_octets = 1;
    $unicode_length = 0;

    $string_length = strlen( $utf8_string );
    for ($i = 0; $i < $string_length; $i++ ) {

        $value = ord( $utf8_string[ $i ] );

        if ( $value < 128 ) {
            if ( $length && ( $unicode_length >= $length ) )
                break;
            $unicode .= chr($value);
            $unicode_length++;
        } else {
            if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

            $values[] = $value;

            if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
                break;
            if ( count( $values ) == $num_octets ) {
                if ($num_octets == 3) {
                    $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
                    $unicode_length += 9;
                } else {
                    $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
                    $unicode_length += 6;
                }

                $values = array();
                $num_octets = 1;
            }
        }
    }

    return $unicode;
}

//taken from wordpress
function seems_utf8($str) {
    $length = strlen($str);
    for ($i=0; $i < $length; $i++) {
        $c = ord($str[$i]);
        if ($c < 0x80) $n = 0; # 0bbbbbbb
        elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
        elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
        elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
        elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
        elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
        else return false; # Does not match any model
        for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
            if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
                return false;
        }
    }
    return true;
}

//function sanitize_title_with_dashes taken from wordpress
function slug($title) {
    $title = strip_tags($title);
    // Preserve escaped octets.
    $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
    // Remove percent signs that are not part of an octet.
    $title = str_replace('%', '', $title);
    // Restore octets.
    $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

    if (seems_utf8($title)) {
        if (function_exists('mb_strtolower')) {
            $title = mb_strtolower($title, 'UTF-8');
        }
        $title = utf8_uri_encode($title, 200);
    }

    $title = strtolower($title);
    $title = preg_replace('/&.+?;/', '', $title); // kill entities
    $title = str_replace('.', '-', $title);
    $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
    $title = preg_replace('/\s+/', '-', $title);
    $title = preg_replace('|-+|', '-', $title);
    $title = trim($title, '-');

    return urldecode($title);
}



if(!function_exists('escape_str')){
	function escape_str($string){
		return htmlentities($string, ENT_QUOTES | ENT_IGNORE, "UTF-8");
	}
}

if(!function_exists('sqle')){
	function sqle($string){
		echo htmlentities($string, ENT_QUOTES | ENT_IGNORE, "UTF-8");
	}
}

if(!function_exists('sqlreturn')){
	function sqlreturn($string){
		return htmlentities($string, ENT_QUOTES | ENT_IGNORE, "UTF-8");
	}
}


if(!function_exists('e')){
	function e($post){
		if(isset($_POST["$post"])){
			echo escape_str($_POST["$post"]);
		}
	}
}

if(!function_exists('eget')){
	function eget($get){
		if(isset($_GET["$get"])){
			echo escape_str($_GET["$get"]);
		}
	}
}

//http://stackoverflow.com/a/1001437
if(!function_exists('real_int')){
	function real_int($value){
		$value = trim($value);
		  if (ctype_digit($value)) {
		    return $value;
		  }
		  $value = preg_replace("/[^0-9](.*)$/", '', $value);
		  if (ctype_digit($value)) {
		    return $value;
		  }
		  return 0;
	}
}

//http://www.webdevdoor.com/php/php-seconds-minutes-hours-ago/
function normal_date($postedDateTime, $systemDateTime = "", $typeOfTime = "s") {
	$systemDateTime = date("Y-m-d H:i:s");
	$changePostedTimeDate=strtotime($postedDateTime);
	$changeSystemTimeDate=strtotime($systemDateTime);
	$timeCalc=$changeSystemTimeDate-$changePostedTimeDate;
	if ($typeOfTime == "s") {
		if ($timeCalc > 0) {
		$typeOfTime = "s";
	}
	if ($timeCalc > 60) {
		$typeOfTime = "m";
	}
	if ($timeCalc > (60*60)) {
		$typeOfTime = "h";
	}
	if ($timeCalc > (60*60*24)) {
		$typeOfTime = "d";
	}
	}
	if ($typeOfTime == "s") {
		if($timeCalc < 0){
			$typeOfTime = "d";
		}else{
			$timeCalc .= ' '.word_r('seconds').' '.word_r('ago');
		}
	}
	if ($typeOfTime == "m") {
		if(round($timeCalc/60) > 1){
			$timeCalc = round($timeCalc/60) .' '.word_r('minutes').' '.word_r('ago');
		}else{
			$timeCalc = round($timeCalc/60) .' '.word_r('minute').' '.word_r('ago');
		}
	}
	if ($typeOfTime == "h") {		
		if(round($timeCalc/60/60) > 1){
			$timeCalc = round($timeCalc/60/60).' '.word_r('hours').' '.word_r('ago');;
		}else{
			$timeCalc = round($timeCalc/60/60).' '.word_r('hour').' '.word_r('ago');;
		}
	}
	if ($typeOfTime == "d") {
		$m = date("m", strtotime($postedDateTime));
		switch ($m) {
			case 1:
				$mt = word_r('jan');
				break;
			case 2:
				$mt = word_r('feb');
				break;
			case 3:
				$mt = word_r('mar');
				break;
			case 4:
				$mt = word_r('apr');
				break;
			case 5:
				$mt = word_r('may');
				break;
			case 6:
				$mt = word_r('jun');
				break;
			case 7:
				$mt = word_r('jul');
				break;
			case 8:
				$mt = word_r('aug');
				break;
			case 9:
				$mt = word_r('sep');
				break;
			case 10:
				$mt = word_r('oct');
				break;
			case 11:
				$mt = word_r('nov');
				break;
			case 12:
				$mt = word_r('dec');
				break;
		}

		$d = date("D", strtotime($postedDateTime));
		switch ("$d") {
			case 'Mon':
				$dt = word_r('mon');
				break;
			case 'Tue':
				$dt = word_r('tue');
				break;
			case 'Wed':
				$dt = word_r('wed');
				break;
			case 'Thu':
				$dt = word_r('thu');
				break;
			case 'Fri':
				$dt = word_r('fri');
				break;
			case 'Sat':
				$dt = word_r('sat');
				break;
			case 'Sun':
				$dt = word_r('sun');
				break;
		}

		$t = date("A", strtotime($postedDateTime));
		switch ("$t") {
			case 'AM':
				$tt = word_r('am');
				break;
			case 'PM':
				$tt = word_r('pm');
				break;
		}

		$timeCalc = $dt.'/'.date("d", strtotime($postedDateTime)).'/'.$mt.'/'.date("Y h:i", strtotime($postedDateTime)).$tt;
	}
	return $timeCalc;
}



function label($str){
	$ci =& get_instance();
	echo $ci->lang->line($str);
}

function word($str){
	$ci =& get_instance();
	echo $ci->lang->line($str);
}

function word_r($str){
	$ci =& get_instance();
	return $ci->lang->line($str);
}

function pages($base_url,$count_all,$per_page){
		$ci =& get_instance();
		$config['base_url'] = $base_url;
		$config['total_rows'] =  $count_all;
		$config['per_page'] = $per_page;
		$config['cur_tag_open'] = '<li class="active"><a href="#" >';
		$config['cur_tag_close'] = '</a></li>';
		$config['first_link'] = '&laquo; '.word_r('first');
   		$config['last_link'] = word_r('last').' &raquo;';
   		$config['full_tag_open'] = '<ul class="pagination">';
   		$config['full_tag_close'] = '</ul>';
   		$config['page_query_string'] = TRUE;
   		$config['query_string_segment'] = "start";
   		$config['first_tag_open'] = '<li>';
   		$config['first_tag_close'] = '</li>';
   		$config['last_tag_open'] = '<li>';
   		$config['last_tag_close'] = '</li>';
   		$config['num_tag_open'] = '<li>';
   		$config['num_tag_close'] = '</li>';
   		$config['next_link'] = '&#8250';
   		$config['next_tag_open'] = '<li>';
   		$config['next_tag_close'] = '</li>';
   		$config['prev_link'] = '&#8249;';
   		$config['prev_tag_open'] = '<li>';
   		$config['prev_tag_close'] = '</li>';
		$ci->pagination->initialize($config);
		return $ci->pagination->create_links();
}

function showing($count_all,$data,$per_page){
		$ci =& get_instance();
		$page = $ci->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		if(empty($count_all)) $start = 0; // start panination
			else $start = $page+1; // start panination
			$end   = $page+$per_page; // end pagination
		if($end >= $count_all){ //if end >= all records in table set it equal all records
				$end = $count_all;
		}
		return word_r('showing').' '.$start.' '.word_r('to').' '.$end.' '.word_r('of').' '.$count_all.' '.word_r('entries');
}

/*function get_admin_user($user_id){
	$ci =& get_instance();
	$ci->db->select()->from('sys_users_detail')->where('user_id', $user_id);
	$query = $ci->db->get();
	return $query->first_row('array');
}*/
/*function get_admin_user($user_id){
        $ci =& get_instance();
	$ci->db->select()->from('sys_users')->where('user_id', $user_id);
	$query = $ci->db->get();
	return $query->first_row('array');
}*/
function get_admin_user($user_id){
	$ci =& get_instance();
	$ci->db->select()->from('sys_users_detail')->join('sys_users','sys_users.user_id=sys_users_detail.user_id')->where('sys_users.user_id', $user_id);
	$query = $ci->db->get();
	return $query->first_row('array');
}

function sys_config($name){
	$ci =& get_instance();
	$ci->db->select('value')->from('sys_config')->where('name',$name);
	$query = $ci->db->get();
	$get = $query->first_row('array');
	return $get['value'];
}
function category_type($name){
	$ci =& get_instance();
	$ci->db->select('category_group_id')->from('tbl_category_groups')->where('name',$name);
	$query = $ci->db->get();
	$get = $query->first_row('array');
	return $get['category_group_id'];
}

function show_page_check_box(){
	$ci =& get_instance();
	$ci->db->select()->from('tbl_posts')->where('post_type','page')->order_by('post_id','DESC');
	$query = $ci->db->get();
	$page_array = $query->result_array();
	if(!empty($page_array)){
		$output ="";
		$output .='<ul class="ul-category">';
		foreach ($page_array as  $value) {
			$post_id = sqlreturn($value['post_id']);
			$post_title = sqlreturn($value['post_title']);
			$output .='<li>';
			$output .='<label class="checkbox"><input ';
			$output .='id="'.$post_id.'" type="checkbox" class="pages" ';
			$output .= 'name="id[]" value="'.$post_id.'"> ';
			$output .= $post_title;
			$output .='</label></li>';
		}
		$output .='</ul>';
		return $output;
	}
}


function media_checked($group_id,$field_name){
	$ci =& get_instance();
	$query = $ci->db->get_where('sys_kcfinder_permission', array('group_id'=>$group_id));
	$result = $query->first_row('array');
	if(!empty($result)){
		if($result[$field_name] == 1){
			echo 'checked';
		}
	}
}
//=-----------------------
function popup($title,$id,$email){
	$ci =& get_instance();
	$output='<span class="cusor time small text-muted fa fa-envelope-o" data-toggle="modal" data-target="#'.$id.'"> Interested</span>';
	$output.=form_open("http://localhost:81/ci3/admin/dashboard/message", array('role' => 'form','class' => 'form-horizontal','id'=>'id'.$id));
	$output.='<div class="modal fade" id="'.$id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">'.$title.'</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="panel panel-default mb5 mini-box panel-hovered">
						<div class="panel-body">
							<div class="col-md-12">
									<div class="form-group">
										<label class="col-md-3 control-label required">Your Name</label>	
										<div class="col-md-9">
											'.form_input("name", $ci->input->post("name", true),"class = 'form-control' id='msg_name'").'
											<font color="red">'.form_error('name').'</font>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label required">Your Email</label>
										<div class="col-md-9">
											'.form_input("email", $ci->input->post("email", true),"class = 'form-control' id='msg_email'").'
											<font color="red">'.form_error("email").'</font>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Your Phone</label>
										<div class="col-md-9">
											'.form_input('phone', $ci->input->post('phone', true),"class = 'form-control' id='msg_phnoe'").'
											<font color="red">'.form_error("phone").'</font>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label required">Subject</label>
										<div class="col-md-9">
											'.form_input('subject', $ci->input->post("subject", true),"class = 'form-control' id='msg_subject'").'
											<font color="red">'.form_error("subject").'</font>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5 control-label required">Message</label><br/><br/>
										'.form_textarea(array("name" => "message", "rows" => 5, "id" => 'msg_message', "value" => $ci->input->post("message")) ).'
										<font color="red">'.form_error("message").'</font>
									</div>
								</div>
							</div>
						</div>
					</row>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="send_msg" id="id'.$id.'" class="btn btn-primary">Save changes</button>
				</div>
				<div id="alert-msg"></div>
			</div>
		</div>
	</div>
	
	';
	$output.=form_close();
	return $output;
}
function get_total_price($param){
	$ci =& get_instance();
	$ci->db->select();
	$ci->db->from('sys_billing b');
	$ci->db->join('sys_orders o', 'b.bill_id = o.bill_id');
	$ci->db->where('b.bill_id', $param);
	$qry = $ci->db->get();
	$result = $qry->result_array();
	$price = 0;
	$output=0;
	foreach($result as $row){
		$item_discount += (($row['original_price']*$row['qty']) * $row['discount_amount'] / 100);
		$price += ($row['original_price'] * $row['qty'])-$item_discount;
		$output += $row['total_shipping'];
	}
	$total = $price + $output;
	return number_format($total,2);
		
}
function attributes($id,$color){
	$ci =& get_instance();
	$ci->db->select('ap.size,ap.color,ap.image,a.attr_name');
	$ci->db->from('sys_attributes_pro ap');
	$ci->db->join('sys_attribute a','ap.color=a.attr_id');
	$ci->db->where(array('product_id'=>$id,'color'=>$color));
	$qry = $ci->db->get();
	return $qry->first_row('array');
}
function varians($id,$color,$size){
	$ci =& get_instance();
	$ci->db->select();
	$ci->db->from('sys_variable');
	$ci->db->where(array('product_id'=>$id,'colors'=>$color,'sizes'=>$size));
	$qry = $ci->db->get();
	return $qry->first_row('array');
}

