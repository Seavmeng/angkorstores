<?php 
/*
* Access Control List
* file_uploads.php
* Update @ 15-5-2015 
* Author: Chim Pesal
*/

//Upload Feature image
function upload($thumb_with=200,$thumb_height=150,$full_width=960, $full_height=720)
{
	$ci =& get_instance();
	$ci->load->library('upload');
	$ci->load->library('image_lib');

	$files = $_FILES;
	
	$cpt = count($_FILES['userfile']['name']);
	/*$count = $cpt;
	for($j=0;$j<$cpt;$j++){
		if($files['userfile']['name'][$j] == ''){
			$count--;
		}
	}
	
	var_dump($_FILES);*/
	
	$gallery = array();
	for($i=0; $i<$cpt; $i++)
	{
		if($_FILES['userfile']['name']= $files['userfile']['name'][$i] != ''):
			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			$ci->upload->initialize(set_upload_options());
			
			if($ci->upload->do_upload_gallery()):
				echo $_FILES['userfile']['name'];exit;
					$data_upload = $ci->upload->data();   
							
						
						$dir = date('m-Y');
						if (!is_dir('images/products/'.$dir)) {
							mkdir('./images/products/' . $dir, 0777, TRUE);
						
						}
						if (!is_dir('images/products/'.$dir.'/thumb')) {
							mkdir('./images/products/' . $dir.'/thumb', 0777, TRUE);
						
						}
						$gallery[] = $data_upload['file_name'];
						$config['image_library'] = 'gd2';
						$config['source_image'] = './images/products/'.$data_upload['file_name'];
						$config['new_image'] = './images/products/'.$dir;
						//$config['quality'] = 20;
						//$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = TRUE;
						$config['width']          = $full_width;
						$config['height']         = $full_height;
						$ci->image_lib->initialize($config);
						$ci->image_lib->resize();
						
						//
						$config['image_library'] = 'gd2';
						$config['source_image'] = './images/products/'.$data_upload['file_name'];
						$config['new_image'] = './images/products/'.$dir.'/thumb/';
						//$config['quality'] = 20;
						//$config['create_thumb'] = TRUE;
						//$config['thumb_marker'] = '_thumb';
						$config['maintain_ratio'] = TRUE;
						$config['width']         = $thumb_with;
						$config['height']       = $thumb_height;
						//$ci->load->library('image_lib', $config2);
						$ci->image_lib->initialize($config);
						$ci->image_lib->resize();
						unlink($data_upload['full_path']);
				endif;
		 endif;
	}
	return $gallery;
}

//Upload Color image
function color($thumb_with=200,$thumb_height=150,$full_width=3000, $full_height=1700)
{
	$ci =& get_instance();
	$ci->load->library('upload');
	$ci->load->library('image_lib');

	$files = $_FILES;

        if(isset($_FILES['img']['name'])){
	
	$cpt = count($_FILES['img']['name']);
	/*$count = $cpt;
	for($j=0;$j<$cpt;$j++){
            if($files['userfile']['name'][$j] == ''){
			$count--;
		}
	}
	
	 var_dump($_FILES);*/
	
	$gallery = array();
	for($i=0; $i<$cpt; $i++)
	{
		if($_FILES['img']['name']= $files['img']['name'][$i] != ''):
			$_FILES['img']['name']= $files['img']['name'][$i];
			$_FILES['img']['type']= $files['img']['type'][$i];
			$_FILES['img']['tmp_name']= $files['img']['tmp_name'][$i];
			$_FILES['img']['error']= $files['img']['error'][$i];
			$_FILES['img']['size']= $files['img']['size'][$i];
			$ci->upload->initialize(set_upload_options());
			if($ci->upload->do_upload_color()):
					$data_upload = $ci->upload->data();   
							
						//var_dump($data_upload);
						$dir = date('m-Y');
						if (!is_dir('images/products/'.$dir)) {
							mkdir('./images/products/' . $dir, 0777, TRUE);
						
						}
						if (!is_dir('images/products/'.$dir.'/thumb')) {
							mkdir('./images/products/' . $dir.'/thumb', 0777, TRUE);
						
						}
						$gallery[] = $data_upload['file_name'];
						$config['image_library'] = 'gd2';
						$config['source_image'] = './images/products/'.$data_upload['file_name'];
						$config['new_image'] = './images/products/'.$dir;
						//$config['quality'] = 20;
						//$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = TRUE;
						$config['width']          = $full_width;
						$config['height']         = $full_height;
						$ci->image_lib->initialize($config);
						$ci->image_lib->resize();
						
						//
						$config['image_library'] = 'gd2';
						$config['source_image'] = './images/products/'.$data_upload['file_name'];
						$config['new_image'] = './images/products/'.$dir.'/thumb/';
						//$config['quality'] = 20;
						//$config['create_thumb'] = TRUE;
						//$config['thumb_marker'] = '_thumb';
						$config['maintain_ratio'] = TRUE;
						$config['width']         = $thumb_with;
						$config['height']       = $thumb_height;
						//$ci->load->library('image_lib', $config2);
						$ci->image_lib->initialize($config);
						$ci->image_lib->resize();
						unlink($data_upload['full_path']);
				endif;
		 endif;
	}
	return $gallery;
        }else{
		
	}

}

//Upload Gallery image
function galleries($thumb_with=200,$thumb_height=150,$full_width=960, $full_height=720)
{
	$ci =& get_instance();
	$ci->load->library('upload');
	$ci->load->library('image_lib');
	$files = $_FILES;
	
	$cpt = count($_FILES['gallery']['name']);
	/*$count = $cpt;
	for($j=0;$j<$cpt;$j++){
		if($files['userfile']['name'][$j] == ''){
			$count--;
		}
	}
	
	var_dump($_FILES);*/
	$gallery = array();
	for($i=0; $i<$cpt; $i++)
	{
		if($_FILES['gallery']['name']= $files['gallery']['name'][$i] != ''):
			$_FILES['gallery']['name']= $files['gallery']['name'][$i];
			$_FILES['gallery']['type']= $files['gallery']['type'][$i];
			$_FILES['gallery']['tmp_name']= $files['gallery']['tmp_name'][$i];
			$_FILES['gallery']['error']= $files['gallery']['error'][$i];
			$_FILES['gallery']['size']= $files['gallery']['size'][$i];
			$ci->upload->initialize(set_upload_options());
			if($ci->upload->do_upload_gallery()):
					$data_upload = $ci->upload->data();   
				
						//var_dump($data_upload);
						$dir = date('m-Y');
						if (!is_dir('images/products/'.$dir)) {
							mkdir('./images/products/' . $dir, 0777, TRUE);
						
						}
						if (!is_dir('images/products/'.$dir.'/thumb')) {
							mkdir('./images/products/' . $dir.'/thumb', 0777, TRUE);
						
						}
						$gallery[] = $data_upload['file_name'];
						$config['image_library'] = 'gd2';
						$config['source_image'] = './images/products/'.$data_upload['file_name'];
						$config['new_image'] = './images/products/'.$dir;
						//$config['quality'] = 20;
						//$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = TRUE;
						$config['width']         = $full_width;
						$config['height']       = $full_height;
						$ci->image_lib->initialize($config);
						$ci->image_lib->resize();
						
						//
						$config['image_library'] = 'gd2';
						$config['source_image'] = './images/products/'.$data_upload['file_name'];
						$config['new_image'] = './images/products/'.$dir.'/thumb/';
						//$config['quality'] = 20;
						//$config['create_thumb'] = TRUE;
						//$config['thumb_marker'] = '_thumb';
						$config['maintain_ratio'] = TRUE;
						$config['width']         = $thumb_with;
						$config['height']       = $thumb_height;
						//$ci->load->library('image_lib', $config2);
						$ci->image_lib->initialize($config);
						$ci->image_lib->resize();
						unlink($data_upload['full_path']);
				endif;
		 endif;
	}
	return $gallery;
}

function set_upload_options()
{   
//  upload an image options
	$config = array();
	$config['upload_path'] = './images/products/';
	$config['allowed_types'] = 'gif|jpg|png';
	//$config['max_size']      = '0';
	$config['overwrite']     = FALSE;
	$config['encrypt_name']     = true;


	return $config;
}