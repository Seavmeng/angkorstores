<?php 
/*
* Author: kimpheng
* website: smartbtoc.com
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*----------category--------------*/
function default_procategory(){
	$ci =& get_instance();
	$ci->db->select('category_id')->from('tbl_categories')->where(array('category_name'=>'Uncategorized'));
	$query = $ci->db->get();
	$row = $query->first_row('array');
	return $row['category_id'];
}
	

if(!function_exists('widget_category')){
	function widget_category($cate_type,$category_id_array=null){
		$ci =& get_instance();
		$id=get_permission();
		$ci->db->select();
		$ci->db->from('tbl_categories');
		//$ci->db->where_in('user_id',$arr);
		$ci->db->where(array('parent_id'=>0,'category_group_id'=>$cate_type));
		$ci->db->order_by('category_name','asc');

		$result = $ci->db->get();
		$category_array = $result->result_array();
		$output ="";
	
		foreach ($category_array as  $value) {
			$category_id = sqlreturn($value['category_id']);
			$category_name = sqlreturn($value['category_name']);
		
			$output .='<div><input ';
			$output .='id="'.$category_id.'" type="checkbox" class="categories" ';
			if(!empty($category_id_array)){
				foreach ($category_id_array as $category_id2) {
					if($category_id2 == $category_id){
						$output .=" checked ";
					}
				}
			}
			$output .= 'name="categories[]" value="'.$category_id.'">';
			$output .= '&nbsp'.$category_name;
			$output .=widget_sub_category_check_box($category_id,$category_id_array);
			$output .='</div>';
		}
	
		return $output;
	}
}
if(!function_exists('widget_sub_category_check_box')){
	function widget_sub_category_check_box($parent_id,$category_id_array=null){
		$ci =& get_instance();
		$id=get_permission();
		/*$admin = $ci->ion_auth->is_admin();
		//$ids = array();
		foreach ($id as $row){
			$ids[] = $row['id'];
		}
		$idu = implode(',', $ids);	
		$arr = explode(',',$idu);*/
		if(1==1){
			$ci->db->select();
			$ci->db->from('tbl_categories');
			//$ci->db->where_in('user_id',$arr);
			$ci->db->where('parent_id',$parent_id);
			$ci->db->order_by('category_name','asc');
		}
		else{
			$ci->db->select();
			$ci->db->from('tbl_categories');
			//$ci->db->where(array('parent_id'=>$parent_id, 'user_id'=>user_id_logged()));
			$ci->db->where(array('parent_id'=>$parent_id));
			$ci->db->order_by('category_id','asc');
		}		//$ci->db->select()->from('sys_category')->where('parent_id',$parent_id)->order_by('category_id','asc');
		$result = $ci->db->get();
		$category_array = $result->result_array();
		$output ="";
	
		foreach ($category_array as $value) {
			$category_id = sqlreturn($value['category_id']);
			$category_name = sqlreturn($value['category_name']);			

			
			$output .='<div style="padding-left:25px"><input ';
			$output .='id="'.$category_id.'" type="checkbox" class="categories" ';
			if(!empty($category_id_array)){
				foreach ($category_id_array as $category_id2) {
					if($category_id2 == $category_id){
						$output .=" checked ";
					}
				}
			}
			$output .= 'name="categories[]" value="'.$category_id.'">';
			$output .= '&nbsp'.$category_name;
			$output .=widget_sub_category_check_box($category_id,$category_id_array);
			$output .='</div>';
		}
	
		return $output;
	}
}
//----------update----------
if(!function_exists('getBiddingValue')){
	
	function getBiddingValue($param=null){
		
		foreach($param as $val){
			$str = $val['bid_status'];
			if($str === "1"){
				
				return "checked";
				//return $str;
			}
			else{
				return "";
		
			}
			
		}
	}
}

if(!function_exists('update_widget_category')){
	function update_widget_category($cate_type,$get_category_id_array=null){
		$ci =& get_instance();
		$id=get_permission();
			$ci->db->select();
			$ci->db->from('tbl_categories');
			$ci->db->where(array('parent_id'=>0,'category_group_id'=>$cate_type));
			$ci->db->order_by('category_name','asc');
		

		$query = $ci->db->get();
		$category_array = $query->result_array();
		$output ="";
	
		foreach ($category_array as  $value) {
			$category_id = sqlreturn($value['category_id']);
			$category_name = sqlreturn($value['category_name']);
			
			$output .='<div><input ';
			$output .='id="'.$category_id.'" type="checkbox" class="categories" ';
				//$category_id_array=explode(",", $get_category_id_array);
				$category_id_array=$get_category_id_array;
				
				
				foreach ($category_id_array as $category_id2) {
					foreach($category_id2 as $strCat){
					//var_dump($strCat);
						if($strCat == $category_id){
							$output .=" checked ";
						}
					}
					
				}
			$output .= 'name="categories[]" value="'.$category_id.'">';
			$output .= '&nbsp'.$category_name;
			$output .=get_widget_sub_category_check_box($category_id,$category_id_array);
			$output .='</div>';
			
		}
		return $output;
	}
}
if(!function_exists('get_widget_sub_category_check_box')){
	function get_widget_sub_category_check_box($parent_id,$category_id_array=null){
		$ci =& get_instance();
		$ci->db->select()->from('tbl_categories')->where('parent_id',$parent_id)->order_by('category_id','asc');
		$query = $ci->db->get();
		$category_array = $query->result_array();
		$output ="";
	
		foreach ($category_array as $value) {
			//$category_id = sqlreturn($value['category_id']);
			//$category_name = sqlreturn($value['category_name']);
			$category_id = $value['category_id'];
			$category_name = $value['category_name'];

			
			$output .='<div style="padding-left:25px"><input ';
			$output .='id="'.$category_id.'" type="checkbox" class="categories" ';
			//$i = 0;
			if(!empty($category_id_array)){
				foreach ($category_id_array as $category_id2) {
					foreach($category_id2 as $strCat){
					//var_dump($strCat);
						if($strCat == $category_id){
							$output .=" checked ";
						}
					}
				}
			}
			$output .= 'name="categories[]" value="'.$category_id.'">';
			//var_dump($category_id_array);
			$output .= '&nbsp'.$category_name;
			$output .=get_widget_sub_category_check_box($category_id,$category_id_array);
			$output .='</div>';
			//$i++;
		}
	
		return $output;
	}
}
function default_cate(){
	$ci =& get_instance();
	$ci->db->select('category_id')->from('tbl_categories')->where(array('category_name'=>'Uncategorized'));
	$query = $ci->db->get();
	$row = $query->first_row('array');
	return $row['category_id'];
}
function get_option($paramet){
	$ci =& get_instance();
	$ci->db->select('option_id')->from('sys_options')->where('option_value',$paramet);
	$query = $ci->db->get();
	$option_array = $query->first_row('array');
	$option_id= $option_array['option_id'];
	
	$ci->db->select()->from('sys_options')->where('option_parent',$option_id);
	$query = $ci->db->get();
	return $query->result_array();
}

function get_role($username){
	$ci =& get_instance();
	$ci->db->select('user_id')->from('users_detail')->where('company_name',$username);
	$query = $ci->db->get();
	$get_query=$query->result_array();
	return $get_query;
}

function get_roles($username){
	$ci =& get_instance();
	$ci->db->select('user_id')->from('users_detail')->where('username',$username);
	$query = $ci->db->get();
	$get_query=$query->result_array();
	return $get_query;
}

function get_permission(){
	$ci =& get_instance();
	$ci->db->select('user_id')->from('sys_users')->where('user_permission',0);
	$query = $ci->db->get();
	$get_query = $query->result_array();
	return $get_query;
}

function get_admin_cate(){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('users');
	$ci->db->join('users_detail', 'users.id = users_detail.user_id');
	$ci->db->where('user_permission',0);
	$query = $ci->db->get();
	$get_query = $query->result_array();
	return $get_query;
}

function check_id($array_par){
	foreach($array_par as $parent){
		$parent_id = $parent['category_id'];
		return $parent_id;
	}
}

function check_sub_menu($id){
	$ci =& get_instance();
	$ci->db->select('category_id')->from('sys_category')->where_in('parent_id',$id);
	$res = $ci->db->get();
	$category = $res->result_array();
	$implode = '';
	foreach($category as $test){
		$get_cate[] = $test['category_id'];
		$implode = implode(',',$get_cate);
	}
	return $implode;
}

/*----------HOME category--------------*/
if(!function_exists('home_category')){
	function home_category($user=null){
		$ci =& get_instance(); 
		$id=get_permission();
		//$ids = array();
		foreach ($id as $row){
			$ids[] = $row['id'];
		}
		$idu = implode(',', $ids);	
		$arr = explode(',',$idu);
		$admin = get_admin_cate();
		foreach($admin as $name){
			$names[] = $name['company_name'];
		}
		$nm = implode(',', $names);	
		$array = explode(',',$nm);
		if($user == null){
			$ci->db->select();
			$ci->db->from('sys_category');
			$ci->db->where_in('user_id',$arr);
			$ci->db->where('parent_id',0);
			$ci->db->order_by('rang','asc');
		}
		elseif(in_array($user,$array)){
			$ci->db->select();
			$ci->db->from('sys_category');
			$ci->db->where_in('user_id',$arr);
			$ci->db->where('parent_id',0);
			$ci->db->order_by('rang','asc');
		}
		else{
			$role = get_role($user);
			foreach($role as $roles){
				$rol[] = $roles['user_id'];
			}
			$ro = implode(',', $rol);
			$ci->db->select();
			$ci->db->from('sys_category');
			$ci->db->where_in('user_id',$ro);
			//$ci->db->where('parent_id',0);
			//$ci->db->order_by('rang','asc');
		}
		$result = $ci->db->get();
		$category_array = $result->result_array();
		$output ="";
		$output .='<div class="box-vertical-megamenus">';
			$output .='<h4 class="title">';
				$output .='<span class="title-menu">'.word_r("category").'</span>';
				$output .='<span class="btn-open-mobile pull-right home-page"><i class="fa fa-bars"></i></span>';
			$output .='</h4>';
			$output .='<div class="vertical-menu-content is-home">';
				$output .='<ul class="vertical-menu-list">';
					foreach ($category_array as  $value) {
						$category_id = sqlreturn($value['category_id']);
						$parent_id   = sqlreturn($value['parent_id']);
						if($ci->session->userdata('sys_lang') == "khmer"){
							$category_name = sqlreturn($value['category_kh']);
						}elseif($ci->session->userdata('sys_lang') == "chinese"){
							$category_name = sqlreturn($value['category_ch']);
						}elseif($ci->session->userdata('sys_lang') == "vietnamese"){
							$category_name = sqlreturn($value['category_vn']);
						}else{
							$category_name = sqlreturn($value['category']);
						}
						$category_img = sqlreturn($value['image']);
						$category_class = sqlreturn($value['description']);
						$class = substr($category_class, 9,-12);
						//$permalink = sqlreturn($value['permalink']);
						/*----------Sub Menu Query-------------*/						
						$ci->db->select('*');
						$ci->db->from('sys_category');
						$ci->db->where('parent_id',$category_id);
						$ci->db->where_in('user_id',$arr);
						$res = $ci->db->get();
						$category_arr = $res->result_array();
						$par_id[] = check_id($category_arr);
						if(in_array($category_id, $par_id)){
							
						}else{
						
						$num = $res->num_rows();
						if($num >= 1){
							$output .='<li class="'.$class.'"><a href="'.base_url().'categories/'.$category_id.'" class="parent"><i class="'.$category_img.' iconmenu">&nbsp;</i><span class="padding">'.$category_name.'</span></a>';	
								$output.='<div class="vertical-dropdown-menu">';
									$output.='<div class="vertical-groups col-sm-12">';
										foreach($category_arr as $var){
											$var_id = sqlreturn($var['category_id']);
											if($ci->session->userdata('sys_lang') == "khmer"){
												$var_title = sqlreturn($var['category_kh']);
											}elseif($ci->session->userdata('sys_lang') == "chinese"){
												$var_title = sqlreturn($var['category_ch']);
											}elseif($ci->session->userdata('sys_lang') == "vietnamese"){
												$var_title = sqlreturn($var['category_vn']);
											}else{
												$var_title = sqlreturn($var['category']);
											}
											//$var_title = sqlreturn($var['category']);
											$output.='<div class="mega-group col-sm-4">';
												$output.='<h4 class="mega-group-header"><span>'.$var_title.'</span></h4>';
												/*----------Sub Sub Menu-----------*/
												$ci->db->select()->from('sys_category')->where('parent_id',$var_id)->order_by('rang','asc');
												$resu = $ci->db->get();
												$category_a = $resu->result_array();
													$output.='<ul class="group-link-default">';
														foreach($category_a as $val){
															$val_id = sqlreturn($val['category_id']);
															if($ci->session->userdata('sys_lang') == "khmer"){
																$val_name = sqlreturn($val['category_kh']);
															}elseif($ci->session->userdata('sys_lang') == "chinese"){
																$val_name = sqlreturn($val['category_ch']);
															}elseif($ci->session->userdata('sys_lang') == "vietnamese"){
																$val_name = sqlreturn($val['category_vn']);
															}else{
																$val_name = sqlreturn($val['category']);
															}
															//$val_name = sqlreturn($val['category']);
															$output .='<li><a href="'.base_url().'categories/'.$val_id.'">'.$val_name.'</a></li>';
														}
													$output.='</ul>';
											$output.='</div>';
										}
									$output.='</div>';
								$output.='</div>';
							$output.='</li>';
						}else{
							$output .='<li class="'.$class.'"><a href="'.base_url().'categories/'.$category_id.'"><i class="'.$category_img.' iconmenu">&nbsp;</i><span class="padding">'.$category_name.'</li></a></li>';	
						}
						}
					}
				$output .='</ul>';
				$output .='<div class="all-category"><span class="open-cate">'.word_r("All Categories").'</span></div>';
			$output .='</div>';
		$output .='</div>';
		return $output;
	}
}

if(!function_exists('home_sub_category')){
	function home_sub_category($parent_id,$category_id_array=null,$level){
		$ci =& get_instance();
		$ci->db->select()->from('sys_category')->where('parent_id',$parent_id)->order_by('rang','asc');
		$result = $ci->db->get();
		$category_array = $result->result_array();
		$output ="";		
		foreach ($category_array as $value) {
			$category_id = sqlreturn($value['category_id']);
			$category_name = sqlreturn($value['category']);	
			$category_img = sqlreturn($value['image']);
			//$permalink = sqlreturn($value['permalink']);	
			
			$ci->db->select('product_code, count(*) as posts_count');
			$ci->db->from('sys_product');
			$ci->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$ci->db->join('sys_currency', 'sys_product.currency_id = sys_currency.currency_id');
			$ci->db->join('sys_relationship', 'sys_product.product_id= sys_relationship.post_id');
			$ci->db->join('sys_category', 'sys_category.category_id = sys_relationship.category_id');
			$ci->db->where('sys_category.category_id',$category_id);
			$query = $ci->db->get();
			$crow=$query->first_row('array');
			$count_p= $crow['posts_count'];
			
			$output .='<li>'.str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $level).'<span class="glyphicon glyphicon-share-alt"></span>
						<a href="categories/'.$category_id.'">'.$category_name.'</a><span class="count">('.$count_p.')</span>';
				$output .=home_sub_category($category_id,$category_id_array,$level+1);
			$output .='</li>';
		}
		$output .=$result->num_rows();
		return $output;
	}
}

if(!function_exists('choose_category')){
	function choose_category(){
		$ci =& get_instance(); 
		$ci->db->select();
		$ci->db->from('sys_category');
		$ci->db->join('users', 'sys_category.user_id = users.id');
		$ci->db->where(array('users.user_permission' => 0, 'parent_id' => 0));
		$query = $ci->db->get();
		$get_query = $query->result_array();
		$output ="";
		$i = 0;
		foreach($get_query as $array){
			$i++;
			$head_id   = $array['category_id']; 
			$head_name = $array['category'];
			$output .= '<div class="panel panel-default col-sm-4 corner">';
				$output .= '<div class="panel-heading">';
					$output .= '<label><input type="checkbox" value="'.$head_id.'" name="cate[]"> '.$head_name.'</label>';
				$output .= '</div>';
				/*----------Sub Menu Query-------------*/
				$ci->db->select()->from('sys_category')->where('parent_id',$head_id)->order_by('rang','asc');
				$result = $ci->db->get();
				$result_arr = $result->result_array();
				$num = $result->num_rows();
				if($num >= 1){
					$output .=' <div>';
						$output .='<div class="panel-body">';
						foreach($result_arr as $value){
							$body_id = $value['category_id'];
							$body_name = $value['category'];
							$output .='<div>';
								$output .='<input type="checkbox" value="'.$body_id.'" name="cate[]"> '.$body_name;
								/*----------Sub Sub Menu-----------*/
								$ci->db->select()->from('sys_category')->where('parent_id',$body_id)->order_by('rang','asc');
								$results = $ci->db->get();
								$results_arr = $results->result_array();
								$output .='<div class="col-sm-12" class="input">';
									foreach($results_arr as $val){
										$bodys_id = $val['category_id'];
										$bodys_name = $val['category'];
										$output .='<input type="checkbox" value="'.$bodys_id.'" name="cate[]"> '.$bodys_name.'&nbsp;&nbsp;&nbsp;';
									}
								$output .='</div>';
							$output .='</div>';
						}
						$output .='</div>';
					$output .='</div>';
				}else{
					
				}			
			$output .= '</div>';
		}
		return $output;
	}
}

if(!function_exists('product_category')){
	function product_category(){
		$ci =& get_instance();
		$ci->db->select()->from('sys_category')->where('parent_id',0)->limit(3);
		$query = $ci->db->get();
		$get_query = $query->result_array();
		return $get_query;
		/*
		$output ="";
		$output.='<ul class="box-tabs nav-tab hot-pro">';
			$output.='<li class="active"><a data-toggle="tab" href="#tab-1" id="0">All</a></li>';
			$i = 1;
			foreach($get_query as $value){
				$i++;
				$cate_id   = sqlreturn($value['category_id']);
				$cate_name = sqlreturn($value['category']);
					$output.='<li><a data-toggle="tab" href="#tab-'.$i.'" id="'.$test.'">'.$cate_name.'</a></li>';
			}
		$output.='</ul>';
		return $output;
		*/
	}
}

if(!function_exists('shop_category')){
	function shop_category(){
		$ci =& get_instance();
		$link = $ci->uri->segment(1);
		$user = $ci->uri->segment(2);
		$url = base_url().$link.'/'.$user.'/';
		$id=get_permission();
		foreach ($id as $row){
			$ids[] = $row['id'];
		}
		$idu = implode(',', $ids);	
		$arr = explode(',',$idu);
		$admin = get_admin_cate();
		foreach($admin as $name){
			$names[] = $name['username'];
		}
		$nm = implode(',', $names);	
		$array = explode(',',$nm);
		if($user == null){
			$ci->db->select();
			$ci->db->from('sys_category');
			$ci->db->where_in('user_id',$arr);
			$ci->db->where('parent_id',0);
			$ci->db->order_by('rang','asc');
		}
		elseif(in_array($user,$array)){
			$ci->db->select();
			$ci->db->from('sys_category');
			$ci->db->where_in('user_id',$arr);
			$ci->db->where('parent_id',0);
			$ci->db->order_by('rang','asc');
		}
		else{
			$role = get_roles($user);
			foreach($role as $roles){
				$rol[] = $roles['user_id'];
			}
			$ro = implode(',', $rol);
			$ci->db->select();
			$ci->db->from('sys_category');
			$ci->db->where_in('user_id',$ro);
		}
		$result = $ci->db->get();
		$category_array = $result->result_array();
		//print_r($category_array);exit;
		$output ="";
		$output .='<div class="box-vertical-megamenus">';
			$output .='<h4 class="title">';
				$output .='<span class="title-menu">'.word_r("category").'</span>';
				$output .='<span class="btn-open-mobile pull-right home-page"><i class="fa fa-bars"></i></span>';
			$output .='</h4>';
			$output .='<div class="vertical-menu-content is-home">';
				$output .='<ul class="vertical-menu-list">';
					foreach ($category_array as  $value) {
						$category_id = sqlreturn($value['category_id']);
						$parent_id   = sqlreturn($value['parent_id']);
						if($ci->session->userdata('sys_lang') == "khmer"){
							$category_name = sqlreturn($value['category_kh']);
						}elseif($ci->session->userdata('sys_lang') == "chinese"){
							$category_name = sqlreturn($value['category_ch']);
						}elseif($ci->session->userdata('sys_lang') == "vietnamese"){
							$category_name = sqlreturn($value['category_vn']);
						}else{
							$category_name = sqlreturn($value['category']);
						}
						$category_img = sqlreturn($value['image']);
						$category_class = sqlreturn($value['description']);
						$class = substr($category_class, 9,-12);
						//$permalink = sqlreturn($value['permalink']);
						/*----------Sub Menu Query-------------*/
						$ci->db->select('*')->from('sys_category')->where('parent_id',$category_id);
						$res = $ci->db->get();
						$category_arr = $res->result_array();
						$par_id[] = check_id($category_arr);
						if(in_array($category_id, $par_id)){
							
						}else{
							$num = $res->num_rows();
							if($num >= 1){
								$output .='<li class="'.$class.'"><a href="'.$url.$category_id.'" class="parent"><i class="'.$category_img.' iconmenu">&nbsp;</i><span class="padding">'.$category_name.'</span></a>';	
									$output.='<div class="vertical-dropdown-menu">';
										$output.='<div class="vertical-groups col-sm-12">';
											foreach($category_arr as $var){
												$var_id = sqlreturn($var['category_id']);
												if($ci->session->userdata('sys_lang') == "khmer"){
													$var_title = sqlreturn($var['category_kh']);
												}elseif($ci->session->userdata('sys_lang') == "chinese"){
													$var_title = sqlreturn($var['category_ch']);
												}elseif($ci->session->userdata('sys_lang') == "vietnamese"){
													$var_title = sqlreturn($var['category_vn']);
												}else{
													$var_title = sqlreturn($var['category']);
												}
												//$var_title = sqlreturn($var['category']);
												$output.='<div class="mega-group col-sm-4">';
													$output.='<h4 class="mega-group-header"><span>'.$var_title.'</span></h4>';
													/*----------Sub Sub Menu-----------*/
													$ci->db->select()->from('sys_category')->where('parent_id',$var_id)->order_by('rang','asc');
													$resu = $ci->db->get();
													$category_a = $resu->result_array();
														$output.='<ul class="group-link-default">';
															foreach($category_a as $val){
																$val_id = sqlreturn($val['category_id']);
																if($ci->session->userdata('sys_lang') == "khmer"){
																	$val_name = sqlreturn($val['category_kh']);
																}elseif($ci->session->userdata('sys_lang') == "chinese"){
																	$val_name = sqlreturn($val['category_ch']);
																}elseif($ci->session->userdata('sys_lang') == "vietnamese"){
																	$val_name = sqlreturn($val['category_vn']);
																}else{
																	$val_name = sqlreturn($val['category']);
																}
																//$val_name = sqlreturn($val['category']);
																$output .='<li><a href="'.$url.$val_id.'">'.$val_name.'</a></li>';
															}
														$output.='</ul>';
												$output.='</div>';
											}
										$output.='</div>';
									$output.='</div>';
								$output.='</li>';
							}else{
								$output .='<li class="'.$class.'"><a href="'.$url.$category_id.'"><i class="'.$category_img.' iconmenu">&nbsp;</i><span class="padding">'.$category_name.'</li></a></li>';	
							}
						}
					}
				$output .='</ul>';
				$output .='<div class="all-category"><span class="open-cate">'.word_r("All Categories").'</span></div>';
			$output .='</div>';
		$output .='</div>';
		return $output;
	}
}
function pagesub_category($category_id){
	$ci =& get_instance();
	$ci->db->select("parent_id,category_id, category_name")
			->from('tbl_categories')
			->where('parent_id',$category_id);

	$query=$ci->db->get();
	$allquery=$query->result_array();
	$output="";
	foreach ($allquery as $key) {
		$output .=$key['category_name'];
	}
	return $output;
}
function getCatTitle($id){
	$ci=& get_instance();
	$ci->db->select();
	$ci->db->from('tbl_categories');
	$ci->db->join('sys_relationship', 'tbl_categories.category_id = sys_relationship.category_id');
	$ci->db->where(array('sys_relationship.post_id'=> $id));
	$qry = $ci->db->get();
	$getTitle = $qry->first_row('array');
	return $getTitle['category_name'];
}
function khmerProduct(){
	$ci=& get_instance();
	$ci->db->select();
	$ci->db->from('kp_product_view');
	$ci->db->join('sys_relationship', 'kp_product_view.product_id = sys_relationship.post_id');
	$ci->db->where('sys_relationship.category_id',336);
	$ci->db->limit(3,0);
	$ci->db->order_by('rand()');
	$qry = $ci->db->get();
	return $qry->result_array();
}
