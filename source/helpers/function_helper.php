<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function includes_url(){
	return base_url().config_item('include_dir');
}
function account_url(){
	return base_url().config_item('admin_dir');
}
function msg($errors){
	if($errors !=""){
		$output = '';
		$output .= '<div class="alert alert-danger">';
		$output .= $errors;
        $output .= '<button data-dismiss="alert" class="close" type="button">
						<span aria-hidden="true" class="glyphicon glyphicon-remove-circle"></span>
					</button>'; 
        $output .= '</div>';
        return $output;
    }
}
function msg_suc($success){
	if($success !=""){
		$output = '';
		$output .= '<div class="alert alert-success">';
		$output .= $success;
        $output .= '<button data-dismiss="alert" class="close" type="button">
						<span aria-hidden="true">�</span>
					</button>'; 
        $output .= '</div>';
        return $output;
    }
}
function orderID($order_id){
	return 'ID'.str_pad($order_id, 6, '0', STR_PAD_LEFT);
}
function bigthumb($image){
	$date=substr($image, 0, 7);
	$output=$date.'/'.substr($image, 14);
	return $output;
}
function permalink($url){
	$date=date("ymdHis");
	$symbol = array('.','!', '&', '?', '/', '/\/', ':', ';', '#', '<', '>', '=', '^', '@', '~', '`', '[', ']', '{', '}');
	$newsym=str_replace($symbol,"",$url);
	if($newsym>6){
		//$newtitle=character_limiter($newsym,6); // First 6 words
		$newtitle=$newsym;
	}else{
		$newtitle=$newsym;
	}
	$urltitle=preg_replace('/[^A-Za-z0-9\-]/','',$newtitle);
	$newurltitle=str_replace(" ","",$urltitle);
	$new_url=$date.'-'.$newurltitle.'.html'; // Final URL
	return $new_url;
}
function page_name($url){
	$ci =& get_instance();
	$date=date("ymi");
	$symbol = array('.','!', '&', '?', '/', '/\/', ':', ';', '#', '<', '>', '=', '^', '@', '~', '`', '[', ']', '{', '}');
	$newsym=str_replace($symbol,"",$url);
	if($newsym>20){
		$newtitle=character_limiter($newsym,20); // First 6 words
	}else{
		$newtitle=$newsym;
	}
	$urltitle=preg_replace('/[^A-Za-z0-9\-]/','',$newtitle);
	$newurltitle=str_replace(" ","",$urltitle);
	
	$ci->db->select('page_name');
	$ci->db->from('sys_users_detail');
	$ci->db->where('page_name', $newurltitle);
	$query = $ci->db->get();
	$data=$query->first_row('array');
	if($data){
		$new_url=$newurltitle.$date; // Final URL
	}else{
		$new_url=$newurltitle; // Final URL
	}	
	return $new_url;
}
function encrypt_url($data){
	$output=create_hash($data);
	$symbol = array('+','.','!', '&', '?', '/', '/\/', ':', ';', '#', '<', '>', '=', '^', '@', '~', '`', '[', ']', '{', '}');
	$output=str_replace($symbol,"-",$output);
	$output=str_replace('sha256',"ubfjtSho-pdEcHOa".md5(date('Hisa')),$output);
	$output=str_replace('1000',"ubfjtSho-pdEcHOa".sha1(date('Ymd')),$output);
	return $output;
}
function has_logged(){
	$ci =& get_instance();
	if($ci->session->userdata('user_id') !=""){
		return true;
	}
	return false;
	
}
function admin_id_logged(){
	$ci =& get_instance();
	$user_id=$ci->session->userdata('user_permission');
	if(!empty($user_id)){
		return $user_id;
	}else{
		return false;
	}
}
function user_id_logged(){
	$ci =& get_instance();
	$user_id=$ci->session->userdata('user_id');
	if(!empty($user_id)){
		return $user_id;
	}else{
		return false;
	}
}
function has_save(){
	return word_r('has_save');
}
function has_del(){
	return word_r('has_del');
	
}
function has_upd(){
	//return word_r('has_upd');
	return 'has been updated!';
	
}
function save_fail(){
	return word_r('save_fail');
	
}
function upd_fail(){
	return word_r('upd_fail');
	
}
function del_fail(){
	return word_r('del_fail');
	
}
function data_already(){
	return 'Category already exits';
	
}
/*function word_r($str){
	$ci =& get_instance();
	return $ci->lang->line($str);
}*/
function get_user_info($user_id){
	$ci =& get_instance();
	$ci->db->select()->from('sys_users')->where('user_id', $user_id);
	$query = $ci->db->get();
	return $query->first_row('array');
}
function get_profile($user_id){
	$ci =& get_instance();
	$ci->db->select()->from('sys_users_detail')->where('user_id', $user_id);
	$query = $ci->db->get();
	return $query->first_row('array');
}
function title_actions($create="", $view="",$action="view"){
	$view_active = $action == 'view'? 'active': '';
	$create_active = $action == 'create'? 'active': '';
	$eidt_active = $action == 'update'? 'active': '';

	$btn = '<a href="'.account_url().$create.'" class="btn btn-link '.$create_active.'" title="'.word_r('create').'"><i class="glyphicon glyphicon-plus"></i> '.word_r('create').'</a>';
	$btn .= '<a href="'.account_url().$view.'"  class="btn btn-link '.$view_active.'" title="'.word_r('view').'"><i class=" glyphicon glyphicon-search"></i> '.word_r('view').'</a>';
	$btn .= '<a href="javascript:void(0)"  class="btn btn-link" title="'.word_r('update').'"><i class="glyphicon glyphicon-pencil"></i> '.word_r('update').'</a>';
	$btn .= '<a href="javascript:void(0)"  class="btn btn-link fullscreen" data-original-title="" title=""><i class="glyphicon glyphicon-resize-full"></i></a>';
	return $btn;
}
function user_roles($roles){
	$ci =& get_instance();
	$ci->db->select('user_group_id')->from('sys_users_roles')->where('user_group_name',$roles);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	$user_rule=$get_query['user_group_id'];
	return $user_rule;
}
function get_pro_field($product_id){
	/*if(user_id_logged()){
		$user_id=user_id_logged();
	}else{
		$user_id=user_roles('Administrator');
	}*/
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_product');
	$ci->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
	$ci->db->where('sys_product.product_id', $product_id); 
	$query = $ci->db->get();
	return $query->first_row('array');
}
//Get vender data
function descript($user_id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('users_detail');
	$ci->db->join('shop_management', 'shop_management.user_id = users_detail.user_id');
	$ci->db->where('users_detail.user_id',$user_id);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	return $get_query;
}
//get vender name
function vender_name($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('users_detail');
	$ci->db->where('user_id',$id);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	return $get_query;
}
//Check Login
function complete($email){
	$ci =& get_instance();
	$ci->db->select('complete');
	$ci->db->from('users');
	$ci->db->where('email',$email);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	return $get_query;
}
//Check second_login
function second_login($user_id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('users');
	$ci->db->where('id',$user_id);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	return $get_query;
}

//Check active 
function checks_active($email){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('users');
	$ci->db->where('email',$email);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	return $get_query;
}

//link to shop vender website
function shop_vender($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sites');
	$ci->db->where('users_id',$id);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	return $get_query;
}
//manage shop vender
function if_user($user){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('users_detail');
	//$ci->db->where('company_name',$user);
	$ci->db->where('username',$user);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	return $get_query;
}
function if_users($user){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('users_detail');
	//$ci->db->where('company_name',$user);
	$ci->db->where('user_id',$user);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	return $get_query;
}

function check_unique($field, $val = null, $id){
	if($val == ""){
		
	}
}

function get_view_pro($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_product');
	$ci->db->where('product_id',$id);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	$user_rule=$get_query['view'];
	return $user_rule;
}

function get_pro_view($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_views');
	$ci->db->where('view_pro_id',$id);
	$query = $ci->db->get();
	return $query->first_row('array');
}

function get_sale_pro($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_inventory');
	$ci->db->where('product_id',$id);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	$user_rule=$get_query['sale_amoung'];
	return $user_rule;
}

function get_cate_id($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_search_pro');
	//$ci->db->from('sys_product');
	//$ci->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
	//$ci->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
	$ci->db->where('category_id', $id); 
	$ci->db->or_where('parent_id', $id); 
	$query = $ci->db->get();
	return $query->result_array();
}

function views_product($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_product');
	$ci->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
	$ci->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
	$ci->db->where('category_id', $id); 
	$query = $ci->db->get();
	return $query->result_array();
}

//get guess pro
function guess_product($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_product');
	$ci->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
	$ci->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
	$ci->db->where_in('category_id', $id);
	$ci->db->order_by("sys_product.created_date", "DESC");
	$ci->db->limit(6);
	$query = $ci->db->get();
	return $query->result_array();
}

//Relative product on cart list
function guess_u_like($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_category');
	$ci->db->join('sys_relationship', 'sys_category.category_id = sys_relationship.category_id');
	$ci->db->where_in('post_id', $id); 
	$query = $ci->db->get();
	return $query->result_array();
}

function search_cate($name){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_search_pro');
	$ci->db->like('product_name', $name);
	$ci->db->or_like('category', $name);
	$query = $ci->db->get();
	return $query->result_array();
}
//get user id from product name
function pro_user_id($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_product');
	$ci->db->where('product_id',$id);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	$user_rule=$get_query['user_id'];
	return $user_rule;
}

//get all url 
function current_full_url()
{
    $CI =& get_instance();

    $url = $CI->config->site_url($CI->uri->uri_string());
    return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
}
function top_sale(){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_product');
	$ci->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
	$ci->db->where('sale_amoung >=', 1); 
	$ci->db->order_by('sale_amoung','desc');
	$query = $ci->db->get();
	return $query->result_array();
}

function get_template($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_template');
	$ci->db->where('shop_id', $id); 
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	$user_rule=$get_query['shop_id'];
	return $user_rule;
}

function get_subcate($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_category');
	$ci->db->where('parent_id', $id); 
	$query = $ci->db->get();
	return $query->result_array();
}

function get_category($id){
	$ci =& get_instance();
	$ci->db->distinct();
	$ci->db->select('*');
	$ci->db->from('sys_search_pro');
	if(is_array($id)){
		$ci->db->where_in('category_id', $id); 
	}else{
		$ci->db->where('category_id', $id);
	}
	$ci->db->group_by('category');
	$query = $ci->db->get();
	return $query->result_array();
}

function get_brand($id){
	$ci =& get_instance();
	$ci->db->distinct();
	$ci->db->select('*');
	$ci->db->from('sys_brand');
	if(is_array($id)){
		$ci->db->where_in('category_id', $id); 
	}else{
		$ci->db->where('category_id',$id);
	}
	$ci->db->group_by('brand');
	$query = $ci->db->get();
	return $query->result_array();
}

function cut_string($img){
	$image = substr($img,14);
	$date = substr($img,0,8);
	$result = $date.$image;
	return $result;
}

function discount($id){
	$ci =& get_instance();
	$query = $ci->db->query("SELECT * FROM sys_inventory WHERE 'product_id' = $id and from_date <= date('Y-m-d H:i:s') and todate >= date('Y-m-d H:i:s')");
	/*
	$ci->db->select('*');
	$ci->db->from('sys_inventory');
	$ci->db->where(array('from_date <='=>date('Y-m-d H:i:s'), 'todate >='=>date('Y-m-d H:i:s')));
	$ci->db->or_where('product_id',$id);
	$query = $ci->db->get();
	*/
	//$result = $query->num_rows();
	//$result = $query->result_array();
	return $query;
}

function get_parent_id($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_search_pro');
	$ci->db->where('parent_id',$id);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	if(empty($get_query)){
		
	}else{
		$user_rule=$get_query['parent_id'];
		return $user_rule;
	}
}
function get_option_value($value){
	
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_options');
	$ci->db->where('option_value',$value);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	$user_rule=$get_query['description'];
	return $user_rule;
}
function get_stock_status($proId){
	$ci =& get_instance();
	$ci->db->select('stock_qty');
	$ci->db->from('sys_inventory');
	$ci->db->where(array('product_id'=>$proId));
	$query = $ci->db->get();
	$getQuery = $query->first_row('array');
	$qty = $getQuery['stock_qty'];
	if ($qty > 0 ){
		
		return "In Stock";
	}
	else{
		return "Out Of Stock";
	}	
}
function currency($sign){

	$ci =& get_instance();
	$currency_id=get_option_value('currency');
	
	if($sign=='sign'){
		$ci->db->select('sign');
		$ci->db->from('sys_currency');
		$ci->db->where(array('currency_id'=>$currency_id));
		$query = $ci->db->get();
		$getQuery = $query->first_row('array');	
		return $getQuery['sign'];
	}else{
		$ci->db->select('currency');
		$ci->db->from('sys_currency');
		$ci->db->where(array('currency_id'=>$currency_id));
		$query = $ci->db->get();
		$getQuery = $query->first_row('array');	
		return $getQuery['currency'].'&nbsp;';
	}
	

}
function hasBid($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_inventory');
	$ci->db->where(array('product_id'=>$id));
	$query=$ci->db->get();
	$getQuery = $query->first_row('array');
	$status = $getQuery['bid_status'];
	return $status;
	
}
function is_complete(){
	$ci =& get_instance();
	$user_id =get_user_id();
	$ci->db->select('iscomplete')->from('sys_users')->where('user_id', $user_id);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	return $get_query['iscomplete'];
}
function register_type(){
	$ci =& get_instance();
	$user_id =get_user_id();
	$ci->db->select('user_group_name');
	$ci->db->from('sys_users');
	$ci->db->join('sys_user_groups', 'sys_users.group_id = sys_user_groups.user_group_id');
	$ci->db->where('user_id',$user_id);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	$user_group_id=$get_query['user_group_name'];
	return $user_group_id;
}
function get_user_id(){
	$ci =& get_instance();
	$user_id =$ci->session->userdata('user_id');
	if(!empty($user_id)){
		return $user_id;
	}else{
		$error = 'ID doesn\'t exist'; 
	}
}
function user_group_id($group_name){
	$ci =& get_instance();
	$user_id =get_user_id();
	$ci->db->select('*');
	$ci->db->from('sys_user_groups');
	$ci->db->where('user_group_name',$group_name);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	return $get_query['user_group_id'];
}
function country(){
	$ci =& get_instance();
	$output="";
	$ci->db->select()->from('sys_location');
	$query = $ci->db->get();
	$get_query=$query->result_array();
	$output.="<select class='personSelect' name='country'>";
	$output.="<option value=''>Choose Your Country</option>";
	foreach($get_query as $row){
		$output .='<option ';
		/*if(!empty($country_name)){
			if($country_name == $row['loc_id']){
				$output .="selected";
			}
		}*/
		$output .= ' value="'.$row['loc_id'].'">';
		$output .= $row['location_name'].'</option>';
	}
	$output.="</select>";
	return $output;
}
function country_byid($id){
	$ci =& get_instance();
	$ci->db->select()->from('sys_location');
	$ci->db->where('loc_id',$id);
	$query = $ci->db->get();
	$data=$query->first_row('array');
	if($data['loc_id']==386){
		return 'Phnom Penh (Angkorstores Office)';
	}else{
		return $data['location_name'];
	} 
}
function update_country($country){
	$ci =& get_instance();
	$output="";
	$ci->db->select()->from('sys_location');
	$query = $ci->db->get();
	$get_query=$query->result_array();
	$output.="<select class='personSelect' name='country'>";
	$output.="<option value=''>Choose Your Country</option>";
	foreach($get_query as $row){
		$output .='<option ';
		if(!empty($country)){
			if($country == $row['loc_id']){
				$output .="selected";
			}
		}
		$output .= ' value="'.$row['loc_id'].'">';
		$output .= $row['location_name'].'</option>';
	}
	$output.="</select>";
	return $output;
}
function city($country_name){
	$ci =& get_instance();
	$output="";
	$ci->db->select()->from('sys_location');
	$ci->db->where('parent_id',$country_name);
	$query = $ci->db->get();
	$get_query=$query->result_array();
	$output.="<select class='personSelect' name='city'>";
	$output.="<option value=''>Choose Your City/State</option>";
	foreach($get_query as $row){
		$output .='<option ';
		$output .= ' value="'.$row['loc_id'].'">';
		$output .= $row['location_name'].'</option>';
	}
	$output.="</select>";
	return $output;
}
function update_city($city){
	$ci =& get_instance();
	$output="";
	$ci->db->select()->from('sys_location');
	
	$query = $ci->db->get();
	$get_query=$query->result_array();
	$output.="<select class='personSelect' name='city'>";
	$output.="<option value=''>Choose Your city</option>";
	foreach($get_query as $row){
		$output .='<option ';
		if(!empty($city)){
			if($city == $row['loc_id']){
				$output .="selected";
			}
		}
		$output .= ' value="'.$row['loc_id'].'">';
		$output .= $row['location_name'].'</option>';
	}
	$output.="</select>";
	return $output;
}
function getNextMinimumBid($id){
	$ci =& get_instance();
	$ci->db->select_max('bid_price');
	$ci->db->from('sys_bid_track');
	$ci->db->where('product_id',$id);
	$query = $ci->db->get();
	$getQuery = $query->first_row('array');
	if(empty($getQuery['bid_price'])){
		return '';
	}else{
		return $getQuery['bid_price'];
	}
}
function getFirstBid($id){
	$ci =& get_instance();
	$ci->db->select('bid_price');
	$ci->db->from('sys_bid_track');
	$ci->db->join('sys_inventory', 'sys_bid_track.product_id = sys_inventory.product_id');
	$ci->db->where(array('sys_bid_track.product_id'=>$id));
	$query = $ci->db->get();
	return $query->first_row('array');
}
function sub_package($id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_user_packages');
	$ci->db->where('parent_id',$id);
	$query = $ci->db->get();
	return $query->result_array();
}
function check_package($id){
	$ci =& get_instance();
	$ci->db->select('package_id');
	$ci->db->from('sys_user_packages');
	$ci->db->where('package_id',$id);
	$query = $ci->db->get();
	$data=$query->first_row('array');
	if(empty($data['package_id'])){
		redirect(base_admin_url().'register');
	}else{
		return $data['package_id'];
	}
}
function userID(){
	$ci =& get_instance();
	$ci->db->select_max('AccountID');
	$ci->db->from('sys_users');
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	$AccountID=$get_query['AccountID']+1;
	return $AccountID;
}
function shipp_city($parent_id,$city_id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_location');
	$ci->db->where('parent_id',$parent_id);
	$ci->db->order_by('location_name','ASC');
	$query=$ci->db->get();
	$results=$query->result_array();
	if($results){
		$output="";
		$output.="<select id='calc_shipping_state' name='calc_shipping_state'>
					<option value=''>Select an City...</option>";
					foreach($results as $data){
							if($data['loc_id']==$city_id){
								$check="selected";
							}else{
								$check="";
							}
							$output.='<option value="'.$data['loc_id'].'" '.$check.'>'.$data['location_name'].'</option>';	
					}
		$output.="</select>";
		echo $output;
	}
}
function ship_conpany_byid($id){
	$ci =& get_instance();
	$ci->db->select()->from('sys_shopping_name');
	$ci->db->where('shipping_id',$id);
	$query = $ci->db->get();
	$data=$query->first_row('array');
	return $data['shipping_name']; 
}
function payment_method($id){
	$ci =& get_instance();
	$ci->db->select('payment_method')->from('sys_payment');
	$ci->db->where('payment_id',$id);
	$query = $ci->db->get();
	$data=$query->first_row('array');
	return $data['payment_method']; 
}
function check_ship_insert($ship_id,$zone_id){
	$ci =& get_instance();
	$ci->db->select()->from('sys_shopping_method');
	$ci->db->where(array('shipping_id'=>$ship_id,'location_id'=>$zone_id));
	$query = $ci->db->get();
	return $query->num_rows();
}
function order_number(){
	$ci =& get_instance();
	$ci->db->select_max('order_number');
	$ci->db->from('sys_billing');
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	$AccountID=$get_query['order_number'];
	return $AccountID;
}
function ref_number($type){
	$ci =& get_instance();
	$ci->db->select_max('order_refer');
	$ci->db->from('sys_transaction');
	$ci->db->where('type',$type);
	$query = $ci->db->get();
	$get_query=$query->first_row('array');
	$AccountID=$get_query['order_refer'];
	return $AccountID;
}
function total_ship($weight,$method_id){
	$ci =& get_instance();
	$ci->db->select()->from('sys_shopping_price');
	$ci->db->where('method_id',$method_id);
	$query = $ci->db->get();
	return $query->result_array();
	/*
	foreach($results as $row){
		if($weight<= $row['max_weight']){
			return $row['fee_amount'];
		}else{
			return 0;
		}
		
	}*/
}
function sub_zone($zone_id){
	$ci =& get_instance();
	$ci->db->select('sys_location.location_name, sys_shopping_zone.country_id')->from('sys_shopping_zone');
	$ci->db->join('sys_location','sys_location.loc_id = sys_shopping_zone.country_id');
	$ci->db->where('sys_shopping_zone.parent_id',$zone_id);
	$query = $ci->db->get();
	return $query->result_array();

}
function hotProduct(){
	$ci =& get_instance();
	$per_page = get_option_value('perpage');
	$ci->db->select('*');
	$ci->db->from('kp_product_view');
	$ci->db->where('hot_pro',1);
	//$ci->db->order_by('rand()');
	$ci->db->limit(6,0);
	$query=$ci->db->get();
	return $query->result_array();
}
function update_price($method_id){
	$ci =& get_instance();
	//$per_page = get_option_value('perpage');
	$ci->db->select('*');
	$ci->db->from('sys_shopping_price');
	$ci->db->where('method_id',$method_id);
	//$ci->db->order_by('rand()');
	//$ci->db->limit(3,0);
	$query=$ci->db->get();
	return $query->result_array();
}
function show_cart($user_id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_cart');
	$ci->db->where('user_id',$user_id);
	$query=$ci->db->get();
	return $query->result_array();
}
function tID(){
        $ci =& get_instance();
	$ci->db->select('tid');
	$ci->db->from('sys_wing_invoice');
	$ci->db->order_by('id','DESC');
	$ci->db->limit(0,1);
	$query=$ci->db->get();
	$data=$query->first_row('array');
	return $data['tid'];
}
//field_name,from,condition
function check_duplicate($primary,$field_name,$from,$condition){
    $ci =& get_instance();
	$ci->db->select($primary,$field_name);
	$ci->db->from($from);
	$ci->db->where($field_name,$condition);
	$query=$ci->db->get();
	return $query->first_row('array');
	
}

function getSupplierUrl($param){
	$ci =& get_instance();
	$ci->db->select('page_name')->from('sys_users_detail');
	$ci->db->where('user_id', $param);
	$qry = $ci->db->get();
	$result = $qry->first_row('array');
	return $result['page_name'];
}
function getUserShipTo($param){
	$ci =& get_instance();
	$ci->db->select('ship_to');
	$ci->db->from('sys_users_detail');
	$ci->db->where(array('user_id' => $param));
	$qry = $ci->db->get();
	$result = $qry->first_row('array');
	return $result['ship_to'];
	
}
function getUserShipMethod($param){
	$ci =& get_instance();
	$ci->db->select('shipcompany');
	$ci->db->from('sys_users_detail');
	$ci->db->where(array('user_id' => $param));
	$qry = $ci->db->get();
	$result = $qry->first_row('array');
	return $result['shipcompany'];
}
function getShipMethod($ship_id,$l_id){
	$ci =& get_instance();
	$ci->db->select('zone_id,parent_id');
	$ci->db->from('sys_shopping_zone');
	$ci->db->where(array('shipping_id'=>$ship_id,'country_id'=>$l_id));
	$query=$ci->db->get();
	$zone=$query->first_row('array');
	$zone_id=$zone['parent_id'];
	if($zone_id){
		$ci->db->select('method_id');
		$ci->db->from('sys_shopping_method');
		$ci->db->where('location_id',$zone_id);
		$query=$ci->db->get();
		$method=$query->first_row('array');
		return $method['method_id'];
	}else{
		return '0';
	}
}
function getShipCompany($param){
	$ci =& get_instance();
	$ci->db->select('shipping_name');
	$ci->db->from('sys_shopping_name');
	$ci->db->where('shipping_id', $param);
	$qry = $ci->db->get();
	$result = $qry->first_row('array');
	return $result['shipping_name'];
}
function get_uid_by_pid($param){
	$ci =& get_instance();
	$ci->db->select('user_id');
	$ci->db->from('sys_product');
	$ci->db->where('product_id', $param);
	$result = $ci->db->get();
	$user_id = $result->first_row('array');
	return $user_id['user_id'];
	
}
function get_group_id_by_uid($param){
	$ci =& get_instance();
	$ci->db->select('group_id')->from('sys_users')->where('user_id', $param);
	$result = $ci->db->get();
	$group_id = $result->first_row('array');
	return $group_id['group_id'];
}
function getPageStatus($param){
	$ci =& get_instance();
	$ci->db->select('page_status')->from('sys_users_detail')->where('user_id', $param);
	$qry = $ci->db->get();
	$row = $qry->first_row('array');
	return $row['page_status'];
}
function check_has_attribute($id){
	$ci =& get_instance();
	$ci->db->select()->from('sys_attributes_pro')->where('product_id', $id);
	$result = $ci->db->get();
	$row = $result->num_rows();
	return $row;
}