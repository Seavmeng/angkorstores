<?php 
/*
* admin_category_helper.php
* Update @ 27-3-2015 
* Author: Chim Pesal
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_category_parent_id($category_id){
	$ci =& get_instance();
	$ci->db->select('parent_id')->from('tbl_categories')->where('category_id',$category_id);
	$q = $ci->db->get();
	$get = $q->first_row('array');
	return $get['parent_id'];
}
function get_location_parent_id($location_id){
	$ci =& get_instance();
	$ci->db->select('parent_id')->from('sys_location')->where('loc_id',$location_id);
	$q = $ci->db->get();
	$get = $q->first_row('array');
	return $get['parent_id'];
}
function sub_category_select_option($parent_id,$child_id=false,$level){
	$ci =& get_instance();
	$group_id=category_type('Product');
	$ci->db->select();
	$ci->db->from('tbl_categories');
	$ci->db->where(array('parent_id'=>$parent_id,'category_group_id'=>$group_id));
	if(!empty($child_id)){
		$ci->db->where(array('category_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('category_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	foreach ($category_array as $value) {
		$category_id = sqlreturn($value['category_id']);
		$category_name = sqlreturn($value['category_name']);
		$category_slug = sqlreturn($value['category_slug']);

		$output .='<option ';
		$output .='id="'.$category_id.'" class="opt-child" ';
		if(!empty($child_id)){
			if(get_category_parent_id($child_id) == $category_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$category_id.'">';
		$output .= str_repeat(" __ ", $level);
		$output .= $category_name.'</option>';
		$output .=sub_category_select_option($category_id,$child_id,$level+1);
	}
	return $output;
}
function sub_location_select_option($parent_id,$child_id=false,$level){
	$ci =& get_instance();
	$ci->db->select();
	$ci->db->from('sys_location');
	$ci->db->where(array('parent_id'=>$parent_id));
	if(!empty($child_id)){
		$ci->db->where(array('loc_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('loc_id','DESC');
	$result = $ci->db->get();
	$location_array = $result->result_array();
	$output ="";
	foreach ($location_array as $value) {
		$location_id = sqlreturn($value['loc_id']);
		$location_name = sqlreturn($value['location_name']);
		//$category_slug = sqlreturn($value['category_slug']);

		$output .='<option ';
		$output .='id="'.$location_id.'" class="opt-child" ';
		if(!empty($child_id)){
			if(get_location_parent_id($child_id) == $location_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$location_id.'">';
		$output .= str_repeat(" __ ", $level);
		$output .= $location_name.'</option>';
		$output .=sub_location_select_option($location_id,$child_id,$level+1);
	}
	return $output;
}

function show_category_select_option($name="category",$child_id=false){
	$ci =& get_instance();
	$group_id=category_type('Product');
	$ci->db->select()->from('tbl_categories');
	$ci->db->where(array('parent_id'=>0,'category_group_id'=> 5));
	if(!empty($child_id)){
		$ci->db->where(array('category_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('category_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	$output .='<select class="form-control" name="'.$name.'">';
	$output .='<option value="0">'.word_r('none').'</option>';
	foreach ($category_array as  $value) {
		$category_id = sqlreturn($value['category_id']);
		$category_name = sqlreturn($value['category_name']);
		$output .='<option ';
		$output .='id="'.$category_id.'" class="opt-parent" ';
		if(!empty($child_id)){
			if(get_category_parent_id($child_id) == $category_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$category_id.'">';
		$output .= $category_name.'</option>';
		$output .=sub_category_select_option($category_id,$child_id,$level=1);
	}
	$output .='</select>';
	return $output;
}
function show_location_select_option($name="location",$child_id=false){
	$ci =& get_instance();
	$ci->db->select()->from('sys_location');
	$ci->db->where(array('parent_id'=>0));
	if(!empty($child_id)){
		$ci->db->where(array('loc_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('loc_id','DESC');
	$result = $ci->db->get();
	$location_array = $result->result_array();
	$output ="";
	$output .='<select class="form-control" name="'.$name.'">';
	$output .='<option value="0">'.word_r('none').'</option>';
	foreach ($location_array as  $value) {
		$location_id = sqlreturn($value['loc_id']);
		$location_name = sqlreturn($value['location_name']);
		$output .='<option ';
		$output .='id="'.$location_id.'" class="opt-parent" ';
		if(!empty($child_id)){
			if(get_location_parent_id($child_id) == $location_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$location_id.'">';
		$output .= $location_name.'</option>';
		$output .=sub_location_select_option($location_id,$child_id,$level=1);
	}
	$output .='</select>';
	return $output;
}
function show_category_product($name="category",$child_id=false,$category_group_id=1){
	$ci =& get_instance();
	$ci->db->select()->from('tbl_categories');
	$ci->db->where(array('parent_id'=>0,'category_group_id'=>$category_group_id));
	if(!empty($child_id)){
		$ci->db->where(array('category_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('category_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	$output .='<select class="form-control" name="'.$name.'">';
	$output .='<option value="0">'.word_r('none').'</option>';
	foreach ($category_array as  $value) {
		$category_id = sqlreturn($value['category_id']);
		$category_name = sqlreturn($value['category_name']);
		$output .='<option ';
		$output .='id="'.$category_id.'" class="opt-parent" ';
		if(!empty($child_id)){
			if(get_category_parent_id($child_id) == $category_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$category_id.'">';
		$output .= $category_name.'</option>';
		$output .=sub_category_select_option($category_id,$child_id,$category_group_id,$level=1);
	}
	$output .='</select>';
	return $output;
}
if(!function_exists('sub_category_check_box')){
	function sub_category_check_box($parent_id,$category_id_array=null,$category_group_id=1){
		$ci =& get_instance();
		$ci->db->select()->from('tbl_categories')->where(array('parent_id'=>$parent_id,'category_group_id'=>$category_group_id))->order_by('category_id','asc');
		$result = $ci->db->get();
		$category_array = $result->result_array();
		$output ="";
		$output .='<ul class="ul-category">';
		foreach ($category_array as $value) {
			$category_id = sqlreturn($value['category_id']);
			$category_name = sqlreturn($value['category_name']);			$category_slug = sqlreturn($value['category_slug']);

			$output .='<li>';
			$output .='<label class="checkbox"><input ';
			$output .='id="'.$category_id.'" type="checkbox" class="categories" ';
			if(!empty($category_id_array)){
				foreach ($category_id_array as $category_id2) {
					if($category_id2 == $category_id){
						$output .=" checked ";
					}
				}
			}
			$output .= 'name="id[]" value="'.$category_id.'">';
			$output .= $category_name;
			$output .=sub_category_check_box($category_id,$category_id_array,$category_group_id);
			$output .='</label></li>';
		}
		$output .='</ul>';
		return $output;
	}
}

if(!function_exists('show_category_check_box')){
	function show_category_check_box($category_id_array=null,$category_group_id=1){
		$ci =& get_instance();
		$ci->db->select()->from('tbl_categories')->where(array('parent_id'=>0,'category_group_id'=>$category_group_id))->order_by('category_id','asc');
		$result = $ci->db->get();
		$category_array = $result->result_array();
		$output ="";
		$output .='<ul class="ul-category">';
		foreach ($category_array as  $value) {
			$category_id = sqlreturn($value['category_id']);
			$category_name = sqlreturn($value['category_name']);
			$output .='<li>';
			$output .='<label class="checkbox"><input ';
			$output .='id="'.$category_id.'" type="checkbox" class="categories" ';
			if(!empty($category_id_array)){
				foreach ($category_id_array as $category_id2) {
					if($category_id2 == $category_id){
						$output .=" checked ";
					}
				}
			}

			$output .= 'name="id[]" value="'.$category_id.'">';
			$output .= $category_name;
			$output .=sub_category_check_box($category_id,$category_id_array,$category_group_id);
			$output .='</label></li>';
		}
		$output .='</ul>';
		return $output;
	}
}


function get_category_join_link($post_id){
		$ci =& get_instance();
		$ci->db->select();
		$ci->db->from('tbl_categories cat');
		$ci->db->join('sys_relationship link','link.category_id = cat.category_id');
		$ci->db->where('link.post_id',$post_id);
		$ci->db->group_by('category_name');
		$query = $ci->db->get();
		$result = $query->result_array();
		$output = '';
		  if(!empty($result)){
	          foreach ($result as $category) {
	              $category_name = $category['category_name'];
	              $output .= '<a href="'.base_admin_url().'publish/post/search?category_id='.$category['category_id'].'" title="'.$category_name.'"><span class="badge badge-primary"> '.$category_name.' </span></a>';
	          }
	      }
	     return $output;
}

function get_all_category_by_post_id($post_id){
	$ci =& get_instance();
	$ci->db->select('cat.category_id');
	$ci->db->from('tbl_categories cat');
	$ci->db->join('tbl_links link','link.category_id = cat.category_id');
	$ci->db->where('link.post_id',$post_id);
	$ci->db->group_by('category_id');
	$query = $ci->db->get();
	$result = $query->result_array();
	$category_array = array();
	foreach ($result as $value) {
		$category_array[] = $value['category_id'];
	}
	return $category_array;

}

function sub_category_select_option_search($parent_id,$cat_id=false,$category_group_id=1,$level){
	$ci =& get_instance();
	$ci->db->select();
	$ci->db->from('tbl_categories');
	$ci->db->where(array('parent_id'=>$parent_id,'category_group_id'=>$category_group_id));
	
	$ci->db->order_by('category_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	foreach ($category_array as $value) {
		$category_id = sqlreturn($value['category_id']);
		$category_name = sqlreturn($value['category_name']);
		$category_slug = sqlreturn($value['category_slug']);

		$output .='<option ';
		$output .='id="'.$category_id.'" class="opt-child" ';
		if(!empty($cat_id)){
			if($cat_id == $category_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$category_id.'">';
		$output .= str_repeat(" * ", $level);
		$output .= $category_name.'</option>';
		$output .=sub_category_select_option_search($category_id,$cat_id,$category_group_id,$level+1);
	}
	return $output;
}


function show_category_select_option_search($name="category",$cat_id=false,$category_group_id=1){
	$ci =& get_instance();
	$ci->db->select()->from('tbl_categories');
	$ci->db->where(array('parent_id'=>0,'category_group_id'=>$category_group_id));
	$ci->db->order_by('category_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	$output .='<select class="form-control" name="'.$name.'">';
	$output .='<option value="0">---</option>';
	foreach ($category_array as  $value) {
		$category_id = sqlreturn($value['category_id']);
		$category_name = sqlreturn($value['category_name']);
		$output .='<option ';
		$output .='id="'.$category_id.'" class="opt-parent" ';
		if(!empty($cat_id)){
			if($cat_id == $category_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$category_id.'">';
		$output .= $category_name.'</option>';
		$output .=sub_category_select_option_search($category_id,$cat_id,$category_group_id,$level=1);
	}
	$output .='</select>';
	return $output;
	
}

 








