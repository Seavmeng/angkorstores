<?php 
/*
* Author: kimpheng
* website: smartbtoc.com
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*----------category--------------*/
function get_categories_parent_id($category_id){
	$ci =& get_instance();
	$ci->db->select('parent_id')->from('sys_category')->where('category_id',$category_id);
	$q = $ci->db->get();
	$get = $q->first_row('array');
	return $get['parent_id'];
}
function get_sub_category_select_option($parent_id,$child_id=false,$user_id="",$level){
	$ci =& get_instance();
	$user_id=has_logged('user_id');
	$ci->db->select();
	$ci->db->from('sys_category');
	$ci->db->where(array('parent_id'=>$parent_id,'user_id'=>$user_id));
	if(!empty($child_id)){
		$ci->db->where(array('category_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('category_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	foreach ($category_array as $value) {
		$category_id = sqlreturn($value['category_id']);
		$category_name = sqlreturn($value['category']);
	//	$category_slug = sqlreturn($value['category_slug']);

		$output .='<option ';
		$output .='id="'.$category_id.'" class="opt-child" ';
		if(!empty($child_id)){
			if(get_categories_parent_id($child_id) == $category_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$category_id.'">';
		$output .= str_repeat("--- ", $level);
		$output .= $category_name.'</option>';
		$output .=get_sub_category_select_option($category_id,$child_id,$user_id,$level+1);
	}
	return $output;
}
function get_category_select_option($name="category",$child_id=false){
	$ci =& get_instance();
	$user_id = has_logged();
	//$user_id = user_id_logged();
	//$id=get_permission();
	//$ids = array();
	foreach ($id as $row){
		$ids[] = $row['id'];
	}
	$idu = implode(',', $ids);
	$exp = explode(',', $idu);
	if(in_array($user_id,$exp)){
		$ci->db->select();
		$ci->db->from('tbl_categories');
		$ci->db->where('parent_id',0);
		$ci->db->where_in('user_id',$idu);
	}else{
		$ci->db->select();
		$ci->db->from('tbl_categories');
		//$ci->db->where('parent_id',0);
		$ci->db->where_in('user_id',$user_id);
	}
	if(!empty($child_id)){
		$ci->db->where(array('category_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('category_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	$output .='<select class="form-control" name="'.$name.'">';
	$output .='<option value="0">'.word_r('none').'</option>';
	foreach ($category_array as  $value) {
		$category_id = sqlreturn($value['category_id']);
		$category_name = sqlreturn($value['category']);
		$output .='<option ';
		$output .='id="'.$category_id.'" class="opt-parent" ';
		if(!empty($child_id)){
			if(get_categories_parent_id($child_id) == $category_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$category_id.'">';
		$output .= $category_name.'</option>';
		$output .=get_sub_category_select_option($category_id,$child_id,$user_id,$level=1);
	}
	$output .='</select>';
	return $output;
}
function show_parent_category_select_option($name="category",$child_id=false,$user_id=""){
	$ci =& get_instance();
	$user_id = has_logged();
	//$user_id=user_id_logged('user_id');
	$id=get_permission();
	//$ids = array();
	foreach ($id as $row){
		$ids[] = $row['id'];
	}
	$idu = implode(',', $ids);
	$exp = explode(',', $idu);
	if(in_array($user_id,$exp)){
		$ci->db->select();
		$ci->db->from('sys_category');
		$ci->db->where('parent_id',0);
		$ci->db->where_in('user_id',$idu);
	}else{
		$ci->db->select();
		$ci->db->from('sys_category');
		//$ci->db->where('parent_id',0);
		$ci->db->where_in('user_id',$user_id);
	}
	if(!empty($child_id)){
		$ci->db->where(array('category_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('category_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	$output .='<select class="form-control" name="'.$name.'">';
	$output .='<option value="0">'.word_r('none').'</option>';
	foreach ($category_array as  $value) {
		$category_id = sqlreturn($value['category_id']);
		$category_name = sqlreturn($value['category']);
		$output .='<option ';
		$output .='id="'.$category_id.'" class="opt-parent" ';
		if(!empty($child_id)){
			if(get_categories_parent_id($child_id) == $category_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$category_id.'">';
		$output .= $category_name.'</option>';
		$output .=get_sub_category_select_option($category_id,$child_id,$user_id,$level=1);
	}
	$output .='</select>';
	return $output;
}
	function _child_category($parent_id,$user_id="",$level){
		$ci =& get_instance();
		$ci->db->select();
		$id = get_permission();
		foreach ($id as $row){
			$ids[] = $row['id'];
		}
		$users_id = implode(', ', $ids);
		$user_ids = explode(',', $users_id);
		$user_id  = user_id_logged();
		
		$ci->db->from('sys_category');
		if(in_array($user_id,$user_ids)){
			$ci->db->where('parent_id',$parent_id);
			$ci->db->where_in('user_id',$user_ids);
			//$ci->db->where(array('parent_id'=>$parent_id,'user_id'=>$user_id));
		}else{
			$ci->db->where(array('parent_id'=>$parent_id,'user_id'=>$user_id));
		}
		$ci->db->order_by('category_id','ASC');
		$result = $ci->db->get();
		$category_array = $result->result_array();
		$output ="";
		foreach ($category_array as $value) {
			$category_id = $value['category_id'];
			$category_name = $value['category'];
			$category_kh = $value['category_kh'];
			$category_ch = $value['category_ch'];
			$category_vn = $value['category_vn'];
			$description = $value['description'];
			$output .= '<tr>';
			$output .= '<td width="50px"><input type="checkbox" class="select"></td>';
			$output .= '<td>'.str_repeat("--- ", $level).word_r('sub_ca').'</td>';

			$output .='<td';
			$output .=' id="'.$category_id.'" class="opt-child" >';
			$output .= str_repeat("--- ", $level);
			$output .= $category_name.'</td>';
			
			$output .='<td';
			$output .=' id="'.$category_id.'" class="opt-child" >';
			$output .= str_repeat("--- ", $level);
			$output .= $category_kh.'</td>';
			
			$output .='<td';
			$output .=' id="'.$category_id.'" class="opt-child" >';
			$output .= str_repeat("--- ", $level);
			$output .= $category_ch.'</td>';
			
			$output .='<td';
			$output .=' id="'.$category_id.'" class="opt-child" >';
			$output .= str_repeat("--- ", $level);
			$output .= $category_vn.'</td>';


			//$output .='<td ';
			//$output .=' id="'.$category_id.'" class="opt-parent" >';
			//$output .= $description.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= btn_action('category/update/'.$category_id,'category/delete/'.$category_id).'</td>';

			$output .=_child_category($category_id,$user_id,$level+1);
			$output .= '</tr>';
		}
		return $output;
	}
	function _child_attribute($parent_id,$user_id="",$level){
		$ci =& get_instance();
		$ci->db->select();
		$user_id=has_logged('user_id');
		$ci->db->from('sys_attribute');
		$ci->db->where(array('parent_id'=>$parent_id,'user_id'=>$user_id));
		$ci->db->order_by('attr_id','ASC');
		$result = $ci->db->get();
		$category_array = $result->result_array();
		$output ="";
		foreach ($category_array as $value) {
			$attr_id = $value['attr_id'];
			$attr_name = $value['attr_name'];
			$description = $value['description'];
			$output .= '<tr>';
			$output .= '<td width="50px"><input type="checkbox" class="select"></td>';
			$output .= '<td>'.str_repeat("--- ", $level).'</td>';

			$output .='<td';
			$output .=' id="'.$attr_id.'" class="opt-child" >';
			$output .= str_repeat("--- ", $level);
			$output .= $attr_name.'</td>';


			$output .='<td ';
			$output .=' id="'.$attr_id.'" class="opt-parent" >';
			$output .= $description.'</td>';

			$output .='<td ';
			$output .=' id="'.$attr_id.'" class="opt-parent" >';
			$output .= btn_action('attributes/attribute/update/'.$attr_id,'attributes/attribute/delete/'.$attr_id).'</td>';
			$output .=_child_attribute($attr_id,$user_id,$level+1);
			$output .= '</tr>';
		}
		return $output;
	}
/*-----------attribute-----------*/
function get_attribute_parent_id($attr_id){
	$ci =& get_instance();
	$ci->db->select('parent_id')->from('sys_attribute')->where('attr_id',$attr_id);
	$q = $ci->db->get();
	$get = $q->first_row('array');
	return $get['parent_id'];
}
function get_attribute_select_option($name="attr_name",$child_id=false,$level){
	$ci =& get_instance();
	$user_id=has_logged('user_id');
	$ci->db->select()->from('sys_attribute');
	$ci->db->where(array('parent_id'=>0,'user_id'=>$user_id));
	if(!empty($child_id)){
		$ci->db->where(array('attr_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('attr_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	$output .='<select class="form-control" name="'.$name.'">';
	$output .='<option value="0">'.word_r('none').'</option>';
	foreach ($category_array as  $value) {
		$attr_id = sqlreturn($value['attr_id']);
		$category_name = sqlreturn($value['attr_name']);
		$output .='<option ';
		$output .='id="'.$attr_id.'" class="opt-parent" ';
		if(!empty($child_id)){
			if(get_attribute_parent_id($child_id) == $attr_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$attr_id.'">';
		$output .= $category_name.'</option>';
		$output .=get_sub_attribute_select_option($attr_id,$child_id,$user_id,$level=1);
	}
	$output .='</select>';
	return $output;
}
function get_sub_attribute_select_option($parent_id,$child_id=false,$user_id="",$level){
	$ci =& get_instance();
	$user_id=has_logged('user_id');
	$ci->db->select();
	$ci->db->from('sys_attribute');
	$ci->db->where(array('parent_id'=>$parent_id,'user_id'=>$user_id));
	if(!empty($child_id)){
		$ci->db->where(array('attr_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('attr_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	foreach ($category_array as $value) {
		$attr_id = sqlreturn($value['attr_id']);
		$category_name = sqlreturn($value['attr_name']);
	//	$category_slug = sqlreturn($value['category_slug']);

		$output .='<option ';
		$output .='id="'.$attr_id.'" class="opt-child" ';
		if(!empty($child_id)){
			if(get_attribute_parent_id($child_id) == $attr_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$attr_id.'">';
		$output .= str_repeat("--- ", $level);
		$output .= $category_name.'</option>';
		$output .=get_sub_attribute_select_option($attr_id,$child_id,$user_id,$level+1);
	}
	return $output;
}
function show_parent_attribute_select_option($name="attr_name",$child_id=false,$user_id=""){
	$ci =& get_instance();
	$user_id=has_logged('user_id');
	$ci->db->select()->from('sys_attribute');
	$ci->db->where(array('parent_id'=>0,'user_id'=>$user_id));
	if(!empty($child_id)){
		$ci->db->where(array('attr_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('attr_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	$output .='<select class="form-control" name="'.$name.'">';
	$output .='<option value="0">'.word_r('none').'</option>';
	foreach ($category_array as  $value) {
		$attr_id = sqlreturn($value['attr_id']);
		$category_name = sqlreturn($value['attr_name']);
		$output .='<option ';
		$output .='id="'.$attr_id.'" class="opt-parent" ';
		if(!empty($child_id)){
			if(get_attribute_parent_id($child_id) == $attr_id){
				$output .="selected";
			}
			
		}

		$output .= ' value="'.$attr_id.'">';
		$output .= $category_name.'</option>';
		$output .=get_sub_attribute_select_option($attr_id,$child_id,$user_id,$level=1);
	}
	
	$output .='</select>';
	return $output;
}
function get_attribute_product($name="attr_name",$child_id=false,$level){
	$ci =& get_instance();
	$user_id=has_logged('user_id');
	$ci->db->select()->from('sys_attribute');
	$ci->db->where(array('parent_id'=>0,'user_id'=>$user_id));
	if(!empty($child_id)){
		$ci->db->where(array('attr_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('attr_id','DESC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	$output .='<select class="form-control" name="attr_name[]" id="attr_name">';
	$output .='<option value="">None</option>';
	foreach ($category_array as  $value) {
		$attr_id = sqlreturn($value['attr_id']);
		$category_name = sqlreturn($value['attr_name']);
		$output .='<option ';
		$output .='id="'.$category_name.'" class="opt-parent" ';
		if(!empty($child_id)){
			if(get_attribute_parent_id($child_id) == $attr_id){
				$output .="selected";
			}
		}

		$output .= ' value="'.$attr_id.'">';
		$output .= $category_name.'</option>';
	}
	$output .='</select>';
	return $output;
}
function select_option($name="select_opt"){
	$ci =& get_instance();
	$user_id=has_logged('user_id');
	$ci->db->select()->from('sys_users_roles');
	$ci->db->where('user_group_id !=',1);
	$ci->db->order_by('user_group_id','asc');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	$output .='<select class="form-control" name="select_opt" style="color:#B2BCC5;background-color:#fff;border:1;-webkit-appearance: none;-moz-appearance: none;">';
	$output .='<option value="" disabled selected>Select Type*</option>';
	foreach ($category_array as  $value) {
		$attr_id = sqlreturn($value['user_group_id']);
		$category_name = sqlreturn($value['user_group_name']);
		$output .='<option ';
		$output .='id="'.$attr_id.'"';
		$output .= ' value="'.$attr_id.'">';
		$output .= $category_name.'</option>';
	}
	
	$output .='</select>';
	return $output;
}
//----------------left menu------------
function left_menu(){
	$ci =& get_instance();
	$ci->db->select()->from('sys_category');
	$ci->db->where('parent_id',0);
	if(!empty($child_id)){
		$ci->db->where(array('category_id !=' => $child_id,'parent_id !=' => $child_id) );
	}
	$ci->db->order_by('rang','ASC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	$output .='<div class="amenu triangle-border top">';
	$output .='<ul class="main-menu">';
	foreach ($category_array as  $value) {
		$category_id = sqlreturn($value['category_id']);
		$category_name = sqlreturn($value['category']);
		$icon = $value['image'];
		$output .='<li class="category">';
		$output .='<a href="#"><i class="'.$icon.'"></i>&nbsp;'.$category_name.'</a>';
		$output .=get_sub_left_menu($category_id,has_logged('user_id'),$level=1);
		$output .='</li>';
	}
	$output .='</ul>';
	$output .='</div>';
	return $output;
}
function get_sub_left_menu($parent_id,$level){
	$ci =& get_instance();
	$ci->db->select();
	$user_id=has_logged('user_id');
	$ci->db->from('sys_category');
	$ci->db->where('parent_id',$parent_id);
	$ci->db->order_by('category_id','ASC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	$output .='<div class="sub-menu">';
	foreach ($category_array as $value) {
		$category_id = $value['category_id'];
		$category_name = $value['category'];
		$description = $value['description'];
		$icon = $value['image'];
	
			$output .='<div class="float_ca">';
			$output .= '<div class="cat_head">';
			$output .='<a href="#"><i class="'.$icon.'"></i>&nbsp;'.$category_name.'</a>';
			$output .=get_child_left_menu($category_id,$level+1);
			$output .= '</div>';
		$output .='</div>';

		
	}
	$output .='</div>';
	return $output;
}
function get_child_left_menu($parent_id,$level){
	$ci =& get_instance();
	$ci->db->select();
	$user_id=has_logged('user_id');
	$ci->db->from('sys_category');
	$ci->db->where('parent_id',$parent_id);
	$ci->db->order_by('category_id','ASC');
	$result = $ci->db->get();
	$category_array = $result->result_array();
	$output ="";
	foreach ($category_array as $value) {
		$category_id = $value['category_id'];
		$category_name = $value['category'];
		$description = $value['description'];
		$icon = $value['image'];

		$output .= '<div class="list_sub">';
		$output .='<i class="'.$icon.'"></i>&nbsp;'.$category_name;
		$output .=get_child_left_menu($category_id,$level+1);
		$output .= '</div>';
		
	}
	return $output;
	
}