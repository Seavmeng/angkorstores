<?php 
function logged_only(){
	if(!has_logged()){
		redirect(base_admin_url());
	}
}
function admin_only(){
	$user_id=get_user_id();
	//------------role-------
	$userRole=rolename($user_id);
	if($userRole !='Admin'){
		redirect(base_url());
	}
}
function vendor_only(){
	$user_id=get_user_id();
	//------------role-------
	$userRole=rolename($user_id);
	if($userRole !='vendor'){
		redirect(base_admin_url());
	}
}
function admin_vendor_only(){
	$ci =& get_instance();
	$user_id=get_user_id();
	//------------role-------
	$userRole=rolename($user_id);
	if($userRole !='Admin' && $userRole !='vendor'){
		redirect(base_admin_url());
	}
}
function rolename($user_id){
	$ci =& get_instance();
	$ci->db->select('b.user_group_name');
	$ci->db->from('sys_users a');
	$ci->db->join('sys_user_groups b','a.group_id=b.user_group_id');
        $ci->db->where('user_id',$user_id);
	$query = $ci->db->get();
	$getRole=$query->first_row('array');
	return $getRole['user_group_name'];
}
function acl($url){
	$ci =& get_instance();
	if(!has_logined()){
		redirect(base_admin_url().'login');
	}
	if(is_complete() !="yes"){
		$ci->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<strong>Please complete your profile!</strong>.
						</div>');
		redirect(base_admin_url().'register/info');
	}
	//***** check get permission by user group ***********//
	$group_id = $ci->session->userdata('group_id');
	$url = str_replace('admin/', '', $url);
	
	$se=$ci->uri->segment(3);
	if($se=="" || $ci->uri->segment(3) == 'create' || $ci->uri->segment(3) == 'delete' || $ci->uri->segment(3) == 'update'){
		$menu_link = $ci->uri->segment(2);
	}else{
		$menu_link = $ci->uri->segment(2).'/'.$ci->uri->segment(3);
	}
	// Check permission
	/*
	if(empty($ci->uri->segment(4)) || $ci->uri->segment(4) == 'create' || $ci->uri->segment(4) == 'delete' || $ci->uri->segment(4) == 'update'){
		$menu_link = $ci->uri->segment(3);
	}else{
		$menu_link = $ci->uri->segment(2).'/'.$ci->uri->segment(3);
	}*/
	//get menu_id
	$query = $ci->db->get_where('sys_menus',array('menu_link'=>$menu_link));
	$get = $query->first_row('array');
	$menu_id = $get['menu_id'];
	//die();
	//get menu_id
	$query = $ci->db->get_where('sys_permission',array('group_id'=>$group_id,'menu_id'=>$menu_id));
	$get = $query->first_row('array');
	//Permission for create
	if($get['create'] == 1){
		$url = str_replace('/create', '', $url);
	}
	//echo $url;exit;
	$se1=$ci->uri->segment(5);
	//Permission for create
	if($se1 !=""){
		$id = $ci->uri->segment(5);
	}else{
		$id = $ci->uri->segment(4);
	}
        //Permission for view
	if($get['view'] == 1){
		$url = str_replace('/view/'.$id, '', $url);
	}
	//Permission for update
	if($get['update'] == 1){
		$url = str_replace('/update/'.$id, '', $url);
	}
	//Permission for update 
	
	//Permission for delete
	if($get['delete'] == 1){
		$url = str_replace('/delete/'.$id, '', $url);
	}
	
	//Permission for search
	if($get['search'] == 1){
		$url = str_replace('/search', '', $url); 
	}
	//Permission for search
	
	//Permission for discount
	if($get['update'] == 1){
		$url = str_replace('/discount/'.$id, '', $url);
		$url = str_replace('/search', '', $url);
	}

	//Permission for ml
	if($get['ml'] == 1){
		$url = str_replace('/ml', '', $url);
	}
	//Permission for ml

	//Permission for ajax
	if($get['ajax'] == 1){
		$url = str_replace('/save', '', $url);
	}
	//Permission for ajax

	//Permission for permission
	if($get['permission'] == 1){
		$url = str_replace('/permission/'.$id, '', $url);
		$url = str_replace('/permission_update/'.$id, '', $url);
		$url = str_replace('/filter_discount/'.$id, '', $url); 
	}
	// echo $url;
	//Permission for permission 
	
	
   /* 
   ****
   **** This is for check permission and always errors so that I command it.
   **** If other developer want to Check permission you can uncommand it.
   **** 
   */
 
   // Check permission

		$ci->db->select();
		$ci->db->from('sys_menus m');
		$ci->db->join('sys_permission p','p.menu_id = m.menu_id');
		$ci->db->where(array('m.menu_link' => $url,'p.group_id'=>$group_id));
		$query = $ci->db->get();
		$result = $query->first_row('array');
		$menu_id = $result['menu_id'];
		if($menu_id==""){
			//$ci->load->view(config_item('admin_dir').'no_permission');
			$output = ":'( Your Group Is No Permission To Used This Function";
			$output .= '<hr><a href="'.base_admin_url().'dashboard"> Back To Dashboard</a>';
			echo $output;
			die();
		}	 
	return $url;
}