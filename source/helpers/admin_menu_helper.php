<?php 
/*
* admin_menu_helper.php
* Update @ 27-3-2015 
* Author: Chim Pesal
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if(!function_exists('sub_menu_sort')){
	function sub_menu_sort($parent_id,$menu_group_id=1){
		$ci =& get_instance();
		$ci->db->select()->from('tbl_menus')->where(array('parent_id'=>$parent_id,'menu_group_id'=>$menu_group_id))->order_by('rang','asc');
		$result = $ci->db->get();
		$menu_array = $result->result_array();
		$output = "";
		$output .= "<ol class='dd-list'>";
			foreach ($menu_array as $value) {
				
					$output .= "<li class='dd-item  dd3-item' data-id='{$value['menu_id']}'>";
				
				

					$output .= "<div class='dd-handle dd3-handle'>&nbsp;</div>";
					$output .= '<div class="dd3-content">';
						$output .= sqlreturn($value['menu_name']);
					$output .= '</div>';
							$output .= "<span class='edit-menu'>";

							$output .= '<span >'.ucfirst($value['menu_type']).'</span>&nbsp;&nbsp;';

							$output .= "<a href='".base_admin_url()."menus/menu/update/{$value['menu_id']}' title='".word_r('edit')."' >";
							$output .= "<i class='fa fa-edit'></i></a>&nbsp;&nbsp;&nbsp;";

							$output .= '<a href="'.base_admin_url().'menus/menu/delete/'.$value["menu_id"].'" onclick="return confirm(\''.word_r('msg_confirm_delete').'\')" title="'.word_r('delete').'" ><i class="fa fa-trash-o"></i></a>';
							$output .= "</span>";
							$output .= sub_menu_sort($value['menu_id'],$menu_group_id);
							$output .= '</li>';
			}
		$output .= '</ol>';

		return $output;
	}
}
if(!function_exists('show_menu_sort')){
	function show_menu_sort($menu_group_id=1){
		$ci =& get_instance();
		$ci->db->select()->from('tbl_menus')->where(array('parent_id'=>0,'menu_group_id'=>$menu_group_id))->order_by('rang','asc');
		$result = $ci->db->get();
		$menu_array = $result->result_array();

		$output = "";
		if(!empty($menu_array)){
			$output .= "<div class='cf nestable-lists' id='menus'>";
			$output .= "<div class='dd' id='nestableMenu'>";
			$output .= "<ol class='dd-list'>";
				foreach ($menu_array as $value) {
	
					
						$output .= "<li class='dd-item  dd3-item' data-id='{$value['menu_id']}'>";
				

						$output .= "<div class='dd-handle dd3-handle'>&nbsp;</div>";
						$output .= '<div class="dd3-content">';
							$output .= sqlreturn($value['menu_name']);
						$output .= '</div>';
						
							$output .= "<span class='edit-menu'>";

							$output .= '<span >'.ucfirst($value['menu_type']).'</span>&nbsp;&nbsp;';

							$output .= "<a href='".base_admin_url()."menus/menu/update/{$value['menu_id']}' title='".word_r('edit')."' >";
							$output .= "<i class='fa fa-edit'></i></a>&nbsp;&nbsp;&nbsp;";

							$output .= '<a href="'.base_admin_url().'menus/menu/delete/'.$value["menu_id"].'" onclick="return confirm(\''.word_r('msg_confirm_delete').'\')" title="'.word_r('delete').'" ><i class="fa fa-trash-o"></i></a>';
							$output .= "</span>";
							$output .= sub_menu_sort($value['menu_id'],$menu_group_id);
						
					$output .= '</li>';
				}
			$output .= '</ol>';
			$output .= '</div>';
			$output .= '</div>';
		}else{
			return 'Empty ;\')';
		}
		return $output;
	}
}



//************* For  Admin Menu *************************//


function has_child($menu_id){
	$ci =& get_instance();
	$ci->db->select('menu_id')->from('sys_menus')->where('menu_parent_id',$menu_id);
	$query = $ci->db->get();
	return $query->num_rows();
}

//******** Sub menu ******************//
function sub_sys_menu($menu_id){
	$ci =& get_instance();
	//***** check get permission by user group ***********//
	$group_id = $ci->session->userdata('group_id');
	$ci->db->select('menu_id')->from('sys_permission')->where('group_id',$group_id);
	$query = $ci->db->get();
	$result = $query->result_array();
	if(!empty($result)){
		$where_in = array();
		foreach($result as $value){
			$where_in[] = $value['menu_id'];
		}
	}else{
		$where_in = false;
	}
	//***** check get permission by user group ***********//
	$ci->db->select()->from('sys_menus');
	$ci->db->where(array('menu_parent_id' => $menu_id, 'active' => 1));
	$ci->db->where_in('menu_id', $where_in);
	$ci->db->order_by('range', 'ASC');
	$result = $ci->db->get();
	$menu_array = $result->result_array();

	$output = "";
	if(!empty($menu_array)){
		$output .='<ul class="sub-menu">';
			foreach ($menu_array as $value) {	
					//********** Change language *************//
					if($ci->session->userdata('sys_lang') == "khmer"){
						$menu_name = $value['menu_name_kh'];
					}else{
						$menu_name = $value['menu_name'];
					}
					//********** Check active main menu *************//
				
					/*
					$url = $ci->uri->segment(2).'/'.$ci->uri->segment(3);
					if(!empty($ci->uri->segment(4))){
						$url .= '/'.$ci->uri->segment(4);
					}
					if($value['menu_link'] == $url){
						$output .= '<li class="start active open">';
						if(has_child($value['menu_id']) > 0){
							$output .= '<a href="javascript:;">';
						}else{
							$output .= '<a href="'.base_url().config_item('admin_dir').$value['menu_link'].'">';
						}
							$output .= '<i class="'.$value['icon'].'"></i>';
							$output .= '<span class="title">'.$menu_name.'</span>';
							$output .= '<span class="selected"></span>';
						if(has_child($value['menu_id']) > 0){
							$output .= '<span class="arrow open"></span>';
						}	
						$output .= '</a>';
						
					}else{
						$output .= '<li class="">';
						if(has_child($value['menu_id']) > 0){
							$output .= '<a href="javascript:;">';
						}else{
							$output .= '<a href="'.base_url().config_item('admin_dir').$value['menu_link'].'">';
						}
							$output .= '<i class="'.$value['icon'].'"></i>';
							$output .= '<span class="title">'.$menu_name.'</span>';
							if(has_child($value['menu_id']) > 0){
							$output .= '<span class="arrow"></span>';
						}	
						$output .= '</a>';
						
					}
					*/
				
						$output .= '<li class="">';
						if(has_child($value['menu_id']) > 0){
							$output .= '<a href="javascript:;">';
						}else{
							$output .= '<a href="'.base_url().config_item('admin_dir').$value['menu_link'].'">';
						}
							$output .= '<i class="'.$value['icon'].'"></i>';
							$output .= '<span class="title">'.$menu_name.'</span>';
							if(has_child($value['menu_id']) > 0){
							$output .= '<span class="arrow"></span>';
						}	
						$output .= '</a>';
						
					
					//********** Show link admin *************//
					
				//********** Call sub menu *************//
				$output .= sub_sys_menu($value['menu_id']);
				$output .= '</li>';
			}
		$output .= '</ul>';
	}
	return $output;
}
//******** Main Admin Menu ******************//
function sys_menu(){ 
	$ci =& get_instance();
	//***** check get permission by user group ***********//
	$group_id = $ci->session->userdata('group_id');
	$ci->db->select('menu_id')->from('sys_permission')->where('group_id',$group_id);
	$query = $ci->db->get();
	$result = $query->result_array();
	if(!empty($result)){
		$where_in = array();
		foreach($result as $value){
			$where_in[] = $value['menu_id'];
		}
	}else{
		$where_in = false;
	}
	//***** check get permission by user group ***********//
	$ci->db->select()->from('sys_menus')->where(array('menu_parent_id' => 0, 'active' => 1))->where_in('menu_id', $where_in)->order_by('range','ASC');
	$result = $ci->db->get();
	$menu_array = $result->result_array();

	$output = "";
	if(!empty($menu_array)){
			foreach ($menu_array as $value) {
				//********** Change language *************//
				if($ci->session->userdata('sys_lang') == "khmer"){
					$menu_name = $value['menu_name_kh'];
				}else{
					$menu_name = $value['menu_name'];
				}
				
				//********** Check active main menu *************//
				$url = $ci->uri->segment(2);
				if($value['menu_link'] == $url){
						$output .= '<li class="start active open">';
						if(has_child($value['menu_id']) > 0){
							$output .= '<a href="javascript:;">';
						}else{
							$output .= '<a href="'.base_url().config_item('admin_dir').$value['menu_link'].'">';
						}
							$output .= '<i class="'.$value['icon'].'"></i>';
							$output .= '<span class="title">'.$menu_name.'</span>';
							$output .= '<span class="selected"></span>';
						if(has_child($value['menu_id']) > 0){
							$output .= '<span class="arrow open"></span>';
						}	
						$output .= '</a>';
							
				}else{
						$output .= '<li class="">';
						if(has_child($value['menu_id']) > 0){
							$output .= '<a href="javascript:;">';
						}else{
							$output .= '<a href="'.base_url().config_item('admin_dir').$value['menu_link'].'">';
						}
							$output .= '<i class="'.$value['icon'].'"></i>';
							$output .= '<span class="title">'.$menu_name.'</span>';
							if(has_child($value['menu_id']) > 0){
							$output .= '<span class="arrow"></span>';
						}	
						 
						$output .= '</a>';
							
				}
				//********** Show link admin *************//
				
				//********** Call sub menu *************//
				$output .= sub_sys_menu($value['menu_id']);
				$output .= '</li>';
			}
		
	} 	 		
	return $output;
}


//**************** sys menu check box for permission ***************//

function sub_sys_menu_check_box($parent_id,$menu_id_array=null){
	$ci =& get_instance();
	$ci->db->select()->from('sys_menus')->where('menu_parent_id', $parent_id)->order_by('range','asc');
	$result = $ci->db->get();
	$menu_array = $result->result_array();
	$output ="";
	$output .='<ul class="ul-category">';
	foreach ($menu_array as $value) {
		$menu_id = $value['menu_id'];
		if($ci->session->userdata('sys_lang') == "khmer"){
			$menu_name = $value['menu_name_kh'];
		}else{
			$menu_name = $value['menu_name'];
		}
		$group_id = $ci->uri->segment(5);

		$output .='<li>';
		$output .='<div  class="checkbox"><label>'.$menu_name.'<i class="fa fa-key"></i> <input ';
		$output .='id="'.$menu_id.'" type="checkbox" ';
		if(!empty($menu_id_array)){
			foreach ($menu_id_array as $menu_id2) {
				if($menu_id2 == $menu_id){
					$output .=" checked ";
				}
			}
		}
		$output .= 'name="menu_id[]" value="'.$menu_id.'"> ';
		$output .= word_r('view').'</label>';

		//create
		$output .= '<label> <input type="checkbox" ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			if($get['create'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="create'.$menu_id.'"> '.word_r('create').' </label>';
		//create

		//update
		$output .= '<label> <input type="checkbox" ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			if($get['update'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="update'.$menu_id.'"> '.word_r('update').' </label>';
		//update

		//delete
		$output .= '<label> <input type="checkbox" ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			
			if($get['delete'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="delete'.$menu_id.'"> '.word_r('delete').' </label>';
		//delete

		//search
		$output .= '<label> <input type="checkbox" ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			
			if($get['search'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="search'.$menu_id.'"> '.word_r('search').' </label>';
		//search

		//ml
		$output .= '<label> <input type="checkbox"  ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			
			if($get['ml'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="ml'.$menu_id.'"> '.word_r('ml').' </label>';
		//ml

		//ajax
		$output .= '<label> <input type="checkbox"  ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			
			if($get['ajax'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="ajax'.$menu_id.'"> '.word_r('ajax').' </label>';
		//ajax

		//permission
		$output .= '<label> <input type="checkbox"  ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			
			if($get['permission'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="permission'.$menu_id.'"> '.word_r('permission').' </label>';
		//permission

		$output .=' </div>';
		$output .=sub_sys_menu_check_box($menu_id,$menu_id_array);
		$output .='</li>';
	}
	$output .='</ul>';
	return $output;
}


function show_sys_menu_check_box($menu_id_array=null){
	$ci =& get_instance();
	$ci->db->select()->from('sys_menus')->where(array('menu_parent_id'=>0,'active'=>1))->order_by('range','asc');
	$result = $ci->db->get();
	$menu_array = $result->result_array();
	$output ="";
	$output .='<ul class="ul-category">';
	foreach ($menu_array as  $value) {
		$menu_id = $value['menu_id'];
		if($ci->session->userdata('sys_lang') == "khmer"){
			$menu_name = $value['menu_name_kh'];
		}else{
			$menu_name = $value['menu_name'];
		}
		$group_id = $ci->uri->segment(5);
		$output .='<li>';
		$output .='<div  class="checkbox"><label>'.$menu_name.'<i class="fa fa-key"></i> <input ';
		$output .='id="'.$menu_id.'" type="checkbox" ';
		if(!empty($menu_id_array)){
			foreach ($menu_id_array as $menu_id2) {
				if($menu_id2 == $menu_id){
					$output .=" checked ";
				}
			}
		}
		$output .= 'name="menu_id[]" value="'.$menu_id.'"> ';
		$output .= word_r('view').'</label>';

		//create
		$output .= '<label> <input type="checkbox" ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			//var_dump($get);
			if($get['create'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="create'.$menu_id.'"> '.word_r('create').' </label>';
		//create

		//update
		$output .= '<label> <input type="checkbox" ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			if($get['update'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="update'.$menu_id.'"> '.word_r('update').' </label>';
		//update

		//delete
		$output .= '<label> <input type="checkbox" ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			if($get['delete'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="delete'.$menu_id.'"> '.word_r('delete').' </label>';
		//delete

		//search
		$output .= '<label> <input type="checkbox" ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			
			if($get['search'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="search'.$menu_id.'"> '.word_r('search').' </label>';
		//search

		//ml
		$output .= '<label> <input type="checkbox"  ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			
			if($get['ml'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="ml'.$menu_id.'"> '.word_r('ml').' </label>';
		//ml

		//ajax
		$output .= '<label> <input type="checkbox"  ';
		if(!empty($menu_id)){
			$query = $ci->db->get_where('sys_permission',array('menu_id'=> $menu_id,'group_id'=>$group_id));
			$get = $query->first_row('array');
			
			if($get['ajax'] == 1){
				$output .=" checked ";
			}
		}
		$output .= 'value="1" name="ajax'.$menu_id.'"> '.word_r('ajax').' </label>';
		//ajax

		$output .=' </div>';
		$output .=sub_sys_menu_check_box($menu_id,$menu_id_array);
		$output .='</li>';
	}
	$output .='</ul>';
	return $output;
}

//**************** sys menu check box for permission ***************//

//**************** Sys menu sort **********************//
if(!function_exists('sub_sys_menu_sort')){
	function sub_sys_menu_sort($parent_id){
		$ci =& get_instance();
		$ci->db->select()->from('sys_menus')->where(array('menu_parent_id'=>$parent_id))->order_by('range','asc');
		$result = $ci->db->get();
		$menu_array = $result->result_array();
		$output = "";
		$output .= "<ol class='dd-list'>";
			foreach ($menu_array as $value) {
				
					$output .= "<li class='dd-item  dd3-item' data-id='{$value['menu_id']}'>";
				
				

					$output .= "<div class='dd-handle dd3-handle'>&nbsp;</div>";
					$output .= '<div class="dd3-content ';
						if($value['active'] == 0){
							$output .= 'red';
						}
					$output .= '">';
						$output .= sqlreturn($value['menu_name']);
					$output .= '</div>';
							$output .= "<span class='edit-menu'>";

						

							$output .= "<a href='".base_admin_url()."settings/menu/update/{$value['menu_id']}' title='".word_r('edit')."' >";
							$output .= "<i class='fa fa-edit'></i></a>&nbsp;&nbsp;&nbsp;";

							$output .= '<a href="'.base_admin_url().'settings/menu/delete/'.$value["menu_id"].'" onclick="return confirm(\''.word_r('msg_confirm_delete').'\')" title="'.word_r('delete').'" ><i class="fa fa-trash-o"></i></a>';
							$output .= "</span>";
							$output .= sub_sys_menu_sort($value['menu_id']);
							$output .= '</li>';
			}
		$output .= '</ol>';

		return $output;
	}
}
if(!function_exists('show_sys_menu_sort')){
	function show_sys_menu_sort(){
		$ci =& get_instance();
		$ci->db->select()->from('sys_menus')->where(array('menu_parent_id'=>0))->order_by('range','asc');
		$result = $ci->db->get();
		$menu_array = $result->result_array();

		$output = "";
		if(!empty($menu_array)){
			$output .= "<div class='cf nestable-lists' id='menus'>";
			$output .= "<div class='dd' id='nestableMenu'>";
			$output .= "<ol class='dd-list'>";
				foreach ($menu_array as $value) {
	
					
						$output .= "<li class='dd-item  dd3-item' data-id='{$value['menu_id']}'>";
				

						$output .= "<div class='dd-handle dd3-handle'>&nbsp;</div>";
						$output .= '<div class="dd3-content ';
						if($value['active'] == 0){
							$output .= 'red';
						}
						$output .= '">';
							$output .= sqlreturn($value['menu_name']);
						$output .= '</div>';
						
							$output .= "<span class='edit-menu'>";

						

							$output .= "<a href='".base_admin_url()."settings/menu/update/{$value['menu_id']}' title='".word_r('edit')."' >";
							$output .= "<i class='fa fa-edit'></i></a>&nbsp;&nbsp;&nbsp;";

							$output .= '<a href="'.base_admin_url().'settings/menu/delete/'.$value["menu_id"].'" onclick="return confirm(\''.word_r('msg_confirm_delete').'\')" title="'.word_r('delete').'" ><i class="fa fa-trash-o"></i></a>';
							$output .= "</span>";
							$output .= sub_sys_menu_sort($value['menu_id']);
						
					$output .= '</li>';
				}
			$output .= '</ol>';
			$output .= '</div>';
			$output .= '</div>';
		}else{
			return 'Empty ;\')';
		}
		return $output;
	}
}

//**************** Sys menu sort **********************//