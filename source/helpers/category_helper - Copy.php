<?php 
	if (!function_exists('selectCategories')){
	function selectCategories(){
		$ci =& get_instance();
		$id=get_permission();
		$ci->db->select();
		$ci->db->from('tbl_categories');
//		$ci->db->limit(8);
		$ci->db->where(array('parent_id'=>0,'status'=>1));
		$ci->db->order_by('category_name','asc');
		$result = $ci->db->get();
		$catArray = $result->result_array();
		foreach ($catArray as $val){
			$catName = sqlreturn($val['category_name']);
			$catId = sqlreturn($val['category_id']); 
			?>
		<li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
			<a title="Computers &amp; Accessories" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true"><?php echo $catName; ?></a>
			<ul role="menu" class=" dropdown-menu">
				<li class="menu-item animate-dropdown menu-item-object-static_block">
					<div class="yamm-content">
						
						<div class="row">
							<div class="col-sm-6">
								<div class="vc_column-inner ">
									<div class="wpb_wrapper">
										<div class="wpb_text_column wpb_content_element ">
											<div class="wpb_wrapper">
												<ul>
													<li class="nav-title"><?php echo $catName.' Martet';?></li>
													<?php echo selectSubCategories($catId); ?>
											</div>
				</li>
				</ul>
		</li>
		
		<?php }
		
	}
}
if(!function_exists('selectSubCategories')){
	function selectSubCategories($parentId){
		$ci =& get_instance();
		$id = get_permission();
		$ci->db->select();
		$ci->db->from('tbl_categories');
		$ci->db->where(array('parent_id'=>$parentId));
		$result= $ci->db->get();
		$subCatArray = $result->result_array(); 
				
		foreach($subCatArray as $val){
			$catName= sqlreturn($val['category_name']);
			
			echo '<li><a href="'.base_url().'category/'.$val['permalink'].'">' .$catName. '</a></li>';
			
		 } ?>
		 
		 </ul>

											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="vc_column-inner ">
									<div class="wpb_wrapper">
										<div class="wpb_text_column wpb_content_element ">
											<div class="wpb_wrapper">
										<!--	<ul>
													<li class="nav-title">Office &amp; Stationery</li>
													<li><a href="#">All Office &amp; Stationery</a></li>
													<li><a href="#">Pens &amp; Writing</a></li>
												</ul>-->

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
		 <?php
	}
	
}
	
