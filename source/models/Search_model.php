<?php 
class search_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	
	public $per_page = 10;
	
	public $start = 0;

	public $count_all = false;
	
	public function index($param){
		$param = $this->db->escape_str(trim($param));
		
		$this->db->select('p.*,u.group_id,u.username,g.user_group_name');
		$this->db->from('kp_product_view p');
		$this->db->join('sys_users u','u.user_id = p.user_id');
		$this->db->join('sys_user_groups g','g.user_group_id = u.group_id');
		$this->db->join('sys_users_detail ud', 'ud.user_id = u.user_id'); 
		if($this->uri->segment(3) == 'find_supplier_product'){
			$this->db->where('u.group_id',2);
		} 
		$this->db->where("
						p.stock_qty > 0 AND p.tags LIKE '%".$param."%' AND p.status = 1
						OR p.stock_qty > 0 AND  p.product_id = '".$param."' AND p.status = 1
						OR p.stock_qty > 0 AND p.product_code = '".$param."' AND p.status = 1
						");  
		$this->db->where('ud.page_status', 1);   
		
		$query1 = $this->db->limit($this->per_page,$this->start)->get()->result_array();  
		
        $query2 = $this->db->select('p.*')
							->from('kp_product_view p')
							->where(" p.stock_qty > 0 AND p.product_name LIKE '%".$param."%' AND p.status = 1
									OR p.stock_qty > 0 AND  p.product_id = '".$param."' AND p.status = 1
									OR p.stock_qty > 0 AND p.product_code = '".$param."' AND p.status = 1
									")
							->limit($this->per_page,$this->start)
							->get() 
							->result_array();
		 		
		if(!empty($query1)) {
			 return $query1; /// product search by tags name
		 }
		 else {
			 return $query2; /// product search by title name
		 } 
	}
	
	public function vendorProduct($strSearch, $strVendor, $strStatus){
		 $param = $this->db->escape_str(trim($strSearch));
		$param2 =  $this->db->escape_str(trim($strVendor));
		$param3 =  $this->db->escape_str(trim($strStatus));
		
		$this->db->select();
		$this->db->from('kp_product_view p');
		$this->db->join('sys_users u','u.user_id = p.user_id');
		$this->db->join('sys_users_detail ud' , 'ud.user_id = u.user_id');
		$this->db->join('sys_user_groups g','g.user_group_id = u.group_id'); 
		$this->db->where('u.group_id',2); 
	 
	
		if($param != "" && $param2 != "" && $param3 != "all") {
			$this->db->where("p.product_name like '%$param%' or p.product_code = '$param' ");
			$this->db->where("ud.company_name like '%$param2%' or ud.page_name like '%$param2%'");
			if($param3 != 'all'){
				if($param3 == 'approved'){
					$this->db->where('status',1);
				}else{
					$this->db->where('status',0);
				}
			} 
		}else {    
			if($param != "" ){
				$this->db->where("p.product_name like '%$param%' or p.product_code = '$param' ");
			}
			if($param2 != ''){
				$this->db->where("ud.company_name like '%$param2%' or ud.page_name like '%$param2%'");
			} 
			if($param3 != 'all'){
				if($param3 == 'approved'){
					$this->db->where('status',1);
				}else{
					$this->db->where('status',0);
				}
			}if($param3=='all'){
				$this->db->order_by('status','ASC'); 
			} 
		}
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		
		$qry = $this->db->get(); 
		
		return $qry->result_array();
	} 
	
	public function auto_complete_vendor($param){
		$param = $this->db->escape_str(trim($param)); 
		$this->db->select();
		$this->db->from('sys_users_detail');
		$this->db->join('sys_users', 'sys_users_detail.user_id = sys_users.user_id');
		$this->db->where("(company_name like '$param%' or page_name like '$param%')");
		$this->db->where('sys_users.group_id', 2); 
		$qry = $this->db->get();
		return $qry->result_array();
	}
	
	public function searchSupplier($param){
		$param = $this->db->escape_str(trim($param));
		$this->db->select();
		$this->db->from('sys_users_detail');
		$this->db->join('sys_users u','u.user_id = sys_users_detail.user_id');
		//$this->db->join('sys_user_groups g','g.user_group_id = u.group_id');
		//$this->db->join('sys_inventory inv', 'p.product_id = inv.product_id');
		//$this->db->join('sys_relationship rel', 'inv.product_id = rel.post_id');
		//if($this->uri->segment(3) == 'find_supplier_product'){
			$this->db->where('u.group_id',2);
		//}
		$this->db->where("(sys_users_detail.company_name LIKE '%$param%' OR sys_users_detail.page_name LIKE '%$param%')");
		//$this->db->where('status',1);
		if(!$this->count_all){
			//var_dump(!$this->count_all);exit();
			$this->db->limit($this->per_page,$this->start);
		}
		$qry = $this->db->get();
		return $qry->result_array();
	}
	
	public function filterSearch($param, $param2,$param3, $max_discount){
		$max_discount = $this->db->escape_str(trim($max_discount)); /// max discount
		$param =  $this->db->escape_str(trim($param)); 
		$param2 =  $this->db->escape_str(trim($param2));
		$param3 =  $this->db->escape_str(trim($param3)); /// min discount
		
		$user_id = user_id_logged();
		$groupAdmin=$this->session->userdata('group_id');
		
		$this->db->select('p.*, u.group_id, u.username, g.user_group_name');
		$this->db->from('kp_product_view p');
		$this->db->join('sys_users u', 'u.user_id = p.user_id');
		$this->db->join('sys_user_groups g', 'g.user_group_id = u.group_id');
		$this->db->where(array('p.post_status'=>0));
		
		if($this->uri->segment(3) == 'find_supplier_product'){
			$this->db->where('u.group_id',2);
		}
		
		if($groupAdmin !=1){
			switch($param2){
			case 'approved':
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				$this->db->where(array('p.status'=>1,'p.user_id'=>$user_id, 'p.post_status'=>0));
			break;
			
			case 'pending':
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				$this->db->where("(p.status =0 or p.status is null)");
				$this->db->where(array('p.user_id'=>$user_id, 'p.post_status'=>0));
			break;
			case 'weight':
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				
				$this->db->where("(p.weight > 0)");
				$this->db->where(array('p.user_id'=>$user_id));
			break;
			case 'no_weight':
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				
				$this->db->where("(p.weight =0 or p.weight is null)");
				$this->db->where(array('p.user_id'=>$user_id, 'p.post_status'=>0));
			break;
			case 'contact_now':
				$this->db->where(array('p.contact-type'=>1, 'p.post_status'=>0));
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				
			break;
			case 'discount':
				$this->db->where(array('p.have_discount'=>1, 'p.post_status'=>0));
				
				$this->db->where('p.discount_amount >= ',$param3);
				$this->db->where('p.discount_amount <= ',$max_discount);
				$this->db->order_by('p.discount_amount');
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				// if($param3 >0){
					// $this->db->where(array('p.have_discount >'=>0,'p.discount_amount'=>$param3));
				// }
			break;
			default:
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				$this->db->where('p.user_id',$user_id);
				$this->db->where('p.post_status', 0);
				// if($param3 >0){
					// $this->db->where(array('p.have_discount >'=>0,'p.discount_amount'=>$param3));
				// }
			}
		}else{

			switch($param2){
			case 'approved':
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				$this->db->where(array('p.status'=>1, 'p.post_status'));
				$this->db->where('u.group_id', 1);
			break;
			
			case 'pending':
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				
				$this->db->where("((p.status =0 or p.status is null) and u.group_id = 1)");
			break;
			case 'weight':
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				
				$this->db->where("(p.weight > 0 and u.group_id = 1)");
			break;
			case 'no_weight':
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				
				$this->db->where("((p.weight =0 or p.weight is null) and u.group_id = 1)");	
			break;
			case 'contact_now':
				$this->db->where(array('p.contact-type'=>1));
				$this->db->where('u.group_id', 1);
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
			break;
			case 'discount': 
				$this->db->where(array('p.have_discount'=>1, 'p.post_status'=>0));
				$this->db->where('p.discount_amount >= ',$param3);
				$this->db->where('p.discount_amount <= ',$max_discount);
				$this->db->order_by('p.discount_amount');
				// $this->db->where('p.discount_amount >= ', );
				
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				// if($param3 >0){
					// $this->db->where(array('p.have_discount >'=>0,'p.discount_amount'=>$param3));
				// }
			
			default:
				$this->db->where("(p.product_name like '%$param%' or p.product_code like '%$param%' or p.product_id like '%$param%')");
				$this->db->where('u.group_id', 1);
			}
		}
		if(!$this->count_all){
			// var_dump($this->count_all);exit();
			$this->db->limit($this->per_page,$this->start);
		}
		$this->db->order_by('p.product_id','DESC');
		
		$qry = $this->db->get();
		 // var_dump($this->db->last_query());exit;
		return $qry->result_array();
	}
	
	public function searchByCat($param1, $param2){
		$param1 = $this->db->escape_str(trim($param1));
		$param2 = $this->db->escape_str(trim($param2));
		
		$this->db->select();
		$this->db->from('kp_product_view p');
		$this->db->join('sys_users u','u.user_id = p.user_id');
		//$this->db->join('sys_inventory inv', 'p.product_id = inv.product_id');
		$this->db->join('sys_relationship rel', 'p.product_id = rel.post_id', 'inner');
		$this->db->where('rel.category_id', $param2);
		$this->db->like('p.product_name', $param1);
		if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
		}	
		
		$qry = $this->db->get();
		return $qry->result_array();	
	}
	
	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	
	public function showing($data){
		$count_all = count($data);
		return showing($count_all,$data,$this->per_page);
	}

	public function count_total_product(){
		$user = $this->session->userdata('user_id');
		$group_id = $this->session->userdata('group_id');
		$this->db->select('product_id');
		$this->db->where('post_status',0);
		if($group_id != 1){
			$this->db->where('user_id',$user);
		}
		$qry = $this->db->get('kp_product_view');
		return $qry->num_rows();
	}

	public function count_pending_product(){
		$user = $this->session->userdata('user_id');
		$group_id = $this->session->userdata('group_id');		

		if($group_id != 1){
			$qry = $this->db->query("SELECT * FROM `kp_product_view` WHERE (`status` = 0 or `status` is null) AND post_status = 0 AND `user_id` = '$user'");
		}else{
			$qry = $this->db->query("SELECT * FROM `kp_product_view` WHERE (`status` = 0 or `status` is null)");
		}
		return $qry->num_rows();
	}
	
	public function count_approved_product(){
		$user = $this->session->userdata('user_id');
		$group_id = $this->session->userdata('group_id');
		$this->db->select('product_id');
		$this->db->where('post_status',0);
		$this->db->where('status',1);
		if($group_id != 1){
			$this->db->where('user_id',$user);
		}
		$qry = $this->db->get('kp_product_view');
		return $qry->num_rows();		
	}
	
	public function count_order_product(){
		$user = $this->session->userdata('user_id');
		$group_id = $this->session->userdata('group_id');
		$this->db->select('bill_id');
		$this->db->where('status',1);
		if($group_id != 1){
			$this->db->where('user_id',$user);
		}
		$qry = $this->db->get('sys_billing');
		return $qry->num_rows();	
	}
	
	public function count_wishlist_product(){
		$user = $this->session->userdata('user_id');
		$this->db->select('product_id');
		$this->db->where('user_id',$user);
		$qry = $this->db->get('sys_wishlist');
		return $qry->num_rows();	
	}
} 
?>