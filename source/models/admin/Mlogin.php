<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mlogin extends CI_Model
{
	private $_table_name = 'sys_users';
	private $_primary_key = 'user_id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';

	public function login($username_or_email,$password){
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->where('email', $username_or_email);
		$this->db->where('user_active',1);
		$result = $this->db->get();
		return $result->first_row('array');
	}
	
	public function get_user_by_id($id)
	{
		$row = $this->db->select("*")
			   ->from("$this->_table_name")
			   ->where("user_id",$id)
			   ->get()->row();
		return $row;
	}
	
}
?>