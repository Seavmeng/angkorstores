<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mbrand extends CI_Model{

	public function add_brand($data){
		$this->db->insert('sys_brand',$data);
		return $this->db->insert_id();
	}
	public function show_all(){
		$query = $this->db->query("SELECT * FROM sys_brand");
		return $query->result_array();
	}
	public function show($id){
		$this->db->select('*');
		$this->db->from('sys_brand');
		$this->db->where(array('brand_id'=>$id));
		$query = $this->db->get();
		return $query->first_row('array');
		//return $query->result_array();
	}
	public function update($id,$data1){
		$this->db->where('brand_id',$id);
		$this->db->update('sys_brand',$data1);
		return $this->db->affected_rows();
	}	
	public function delete_brand($id){
		$this->db->where('brand_id',$id);
		$this->db->delete('sys_brand');
		return $this->db->affected_rows();
	}
}