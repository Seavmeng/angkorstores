<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdictionary extends CI_Model
{
	private $_table_name = 'tbl_dictionary';
	private $_primary_key = 'id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';
	public $field_order = 'id';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_id();
	}

	public function all(){
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}


	public function update($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}

	public function delete($id){
		$this->db->where($this->_primary_key,$id);
		$this->db->delete($this->_table_name);
		return $this->db->affected_rows();
	}


	public function find_by_id($id){
		$this->db->select()->from($this->_table_name)->where($this->_primary_key,$id);
		$query = $this->db->get();
		return $query->first_row('array');
	}

	public function search($name){
		$this->db->select();
		$this->db->from($this->_table_name);
		if(!empty($name)){
			$this->db->like('english',$name);
			$this->db->or_like('khmer',$name);
		}
		
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}


	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}

	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	
}
?>