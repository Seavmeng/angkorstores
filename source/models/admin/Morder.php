<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Morder extends CI_Model
{
	private $_table_name = 'sys_billing';
	private $_primary_key = 'bill_id';

	public $per_page = 10;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';
	public $field_order = 'product_id';

	public function show_all(){
		
		$this->db->select('*');
		$this->db->from('sys_billing');
		//$this->db->join('sys_orders o', 'b.bill_id = o.bill_id');
		$this->db->where('status',1);
		$this->db->order_by('bill_id','DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function count_show_all(){
		$this->db->select('*');
		$this->db->from('sys_billing');
		$this->db->where('status',1);
		$this->db->order_by('bill_id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	public function count_show_one($userid){
		$this->db->select('*');
		$this->db->from('sys_billing');
		//$this->db->join('sys_orders','sys_orders.bill_id = sys_billing.bill_id');
		$this->db->where(array('user_id' => $userid,'status',1));       
		$query = $this->db->get();
		return $query->result_array();
	}
	public function show_one($userid){
		//var_dump($userid); exit();
		$this->db->select('*');
		$this->db->from('sys_billing');
		//$this->db->join('sys_orders','sys_orders.bill_id = sys_billing.bill_id');
		$this->db->where(array('user_id' => $userid));
               
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function order_info($id){
		$this->db->select('*');
		$this->db->from('sys_billing');
		$this->db->where('bill_id',$id);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	public function show_detail($uri){
		$this->db->select('*');
		$this->db->from('sys_billing');
		$this->db->join('sys_orders','sys_orders.bill_id = sys_billing.bill_id');
                $this->db->join('sys_product','sys_product.product_id = sys_orders.product_id');
                $this->db->join('sys_payment','sys_orders.payment_id = sys_payment.payment_id');
		$this->db->where(array('sys_billing.bill_id' => $uri));
		$query = $this->db->get();
		return $query->result_array();
	}
    public function delete_order($id){
		$this->db->where($this->_primary_key,$id);
		$this->db->delete($this->_table_name);
		$bill=$this->db->affected_rows();
		if($bill){
			$this->db->where($this->_primary_key,$id);
			$this->db->delete('sys_orders');
		}
		return $this->db->affected_rows();
	}
	public function find_orderid($keyword, $from, $to){
		$keyword = $this->db->escape_str($keyword);
		if($keyword !='' && $from =='' && $to ==''){
			$query = $this->db->query("SELECT * FROM sys_billing WHERE (first_name LIKE '%$keyword%' OR last_name LIKE '%$keyword%' OR order_number = '$keyword' OR (CONCAT(first_name,' ',last_name) LIKE '%$keyword%')) AND (`status` = 1)");
		}
		else if($keyword !='' && $from !='' && $to !=''){
			$query = $this->db->query("SELECT * FROM sys_billing WHERE (first_name LIKE '%$keyword%' OR last_name LIKE '%$keyword%' OR order_number = '$keyword' OR (CONCAT(first_name,' ',last_name) LIKE '%$keyword%')) AND (created >= '$from' AND created <='$to') AND (`status` = 1)");
		}
		else{
			$this->db->select('*');
			$this->db->from('sys_billing');
			if($from=='' || $to ==''){
				redirect('admin/reports/report');	 
			}else{
				$this->db->where("created >= '".$from."' AND created <='".$to."'");
				$this->db->where('status',1);
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
			}
			$query = $this->db->get();
		}
		return $query->result_array();
	}
	public function find_order_buyer($keyword, $from, $to){  
		$keyword = $this->db->escape_str($keyword);
		if($keyword !='' && $from =='' && $to ==''){
			$query = $this->db->query("SELECT * FROM sys_billing WHERE (first_name LIKE '%$keyword%' OR last_name LIKE '%$keyword%' OR order_number = '$keyword' OR (CONCAT(first_name,' ',last_name) LIKE '%$keyword%')) AND (`status` = 1)");
		}
		else if($keyword !='' && $from !='' && $to !=''){
			$query = $this->db->query("SELECT * FROM sys_billing WHERE (first_name LIKE '%$keyword%' OR last_name LIKE '%$keyword%' OR order_number = '$keyword' OR (CONCAT(first_name,' ',last_name) LIKE '%$keyword%')) AND (created >= '$from' AND created <='$to') AND (`status` = 1)");
		}
		else{
			$this->db->select('*');
			$this->db->from('sys_billing');
			if($from=='' || $to ==''){
				redirect('admin/reports/report');	
			}else{
				$this->db->where("created >= '".$from."' AND created <='".$to."'");
				$this->db->where('status',1);
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
			}
			$query = $this->db->get();
		}
		return $query->result_array();
	}
	//End khaing
	public function getpost(){
		$query = $this->db->query("SELECT * FROM tbl_categories WHERE parent_id = 456");
		return $query->result_array();
	}
	public function page($url,$data){
		$count_all = count($data);
		return pages($url,$count_all,$this->per_page);
	}
	public function showing($data){
		$count_all = count($data);
		return showing($count_all,$data,$this->per_page);
	}
	public function show_daily(){
		$this->db->select('*');
		$this->db->from('sys_orders');
		//$this->db->join('sys_orders o', 'b.bill_id = o.bill_id');
		//$this->db->where('created',$date);
		$this->db->order_by('bill_id','DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function daily_search($date){
		$date = $this->db->escape_str($date);

		$this->db->select('*');
		$this->db->from('sys_orders');
		//$this->db->join('sys_orders o', 'b.bill_id = o.bill_id');
		$this->db->where("DATE_FORMAT(created,'%Y-%m-%d') = '".$date."'");
		$this->db->order_by('bill_id','DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function daily_search_bill($date){
		$date = $this->db->escape_str($date);

		$this->db->select('*');
		$this->db->from('sys_billing');
		//$this->db->join('sys_orders o', 'b.bill_id = o.bill_id');
		$this->db->where("DATE_FORMAT(created,'%Y-%m-%d') = '".$date."'");
		$this->db->order_by('bill_id','DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function show_monthly(){
		$this->db->select('*');
		$this->db->from('sys_orders');
		//$this->db->join('sys_orders o', 'b.bill_id = o.bill_id');
		//$this->db->where('created',$date);
		$this->db->order_by('bill_id','DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function monthly_search($date){
		$date = $this->db->escape_str($date);

		$this->db->select('*');
		$this->db->from('sys_orders');
		//$this->db->join('sys_orders o', 'b.bill_id = o.bill_id');
		$this->db->where("DATE_FORMAT(created,'%Y-%m') = '".$date."'");
		$this->db->order_by('bill_id','DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function month_search_bill($date){
		$date = $this->db->escape_str($date);

		$this->db->select('*');
		$this->db->from('sys_billing');
		//$this->db->join('sys_orders o', 'b.bill_id = o.bill_id');
		$this->db->where("DATE_FORMAT(created,'%Y-%m') = '".$date."'");
		$this->db->order_by('bill_id','DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
    public function show_supplier(){
		$this->db->select('*');
		$this->db->from('kp_product_view');
                $this->db->where('supplier_id >',0);
		$this->db->or_where('supplier_id != ""');
		$this->db->order_by('created_date','DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function find_code_name($supplier= null,$keyword=null){
		$supplier = $this->db->escape_str(trim($supplier));
		$keyword = $this->db->escape_str(trim($keyword)); 
		$this->db->select('*');
		$this->db->from('kp_product_view');	
		$this->db->where(array('status'=>1, 'post_status'=>0));  
		if($supplier >0) {
			$this->db->where("product_code = '".$keyword."' and supplier_id ='".$supplier."' 
						OR product_name LIKE '%".$keyword."%' and supplier_id ='".$supplier."' ");
		}else {
			$this->db->where("product_code = '".$keyword."' OR product_name LIKE '%".$keyword."%' ");
		} 
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();  
		return $query->result_array();
	}
	public function view_pro($id){
		$this->db->select('*');
		$this->db->from('kp_product_view');	
		$this->db->where('product_id',$id);	
		$query = $this->db->get();
		return $query->first_row('array');
	}
	public function get_supplier(){
		$supplier_id=user_group_id('Supplier');
		$this->db->select();
		$this->db->from('sys_users');
		$this->db->where('group_id',$supplier_id);
		$qry = $this->db->get();
		return $qry->result_array();
	}
	
}