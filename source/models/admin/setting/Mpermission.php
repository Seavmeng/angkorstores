<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mpermission extends CI_Model
{
	private $_table_name = 'sys_permission';
	private $_primary_key = 'group_id';

	public function save($data){
		$this->db->insert($this->_table_name,$data);
		return $this->db->affected_rows();
	}
	public function show(){
			$this->db->select('*');
			$this->db->from('sys_permission p');
			$this->db->join('sys_user_groups g','g.user_group_id=p.group_id');
			$this->db->join('sys_menus m','p.menu_id=m.menu_id');
			$this->db->order_by('group_id','ASC');
			$query=$this->db->get();
			return $query->result_array();
		}
	public function find_by_id($menu_id){
		$this->db->select()->from($this->_table_name)->where($this->_primary_key,$menu_id);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	public function update($menu_id,$data){
		$this->db->where($this->_primary_key,$menu_id);
		$this->db->update($this->_table_name,$data);
		return $this->db->affected_rows();
	}

	public function delete($menu_id){
		$this->db->where($this->_primary_key,$menu_id);
		$this->db->delete($this->_table_name);
		return $this->db->affected_rows();
	}

	public function update_parent_id($menu_id){
		$this->db->where('parent_id',$menu_id);
		$this->db->update($this->_table_name,array('parent_id'=>0));
		
	}

	public function parseJsonArray($jsonArray, $parent_id = 0)
	{
	  $return = array();
	  foreach ($jsonArray as $subArray) {
		 $returnSubSubArray = array();
		 if (isset($subArray['children'])) {
		   $returnSubSubArray = $this->parseJsonArray($subArray['children'], $subArray['id']);
		 }
		 $return[] = array('id' => $subArray['id'], 'parent_id' => $parent_id);
		 $return = array_merge($return, $returnSubSubArray);
	  }

	  return $return;
	}

	public function add_permission($data)
	{
		$check=$this->db->insert('sys_permission',$data);
		return $check;
	}

	public function get_permissionbygroupid_menu_id($group_id=null,$menu_id=null)
	{
		$array = array('group_id' => $group_id, 'menu_id' => $menu_id);
		$query = $this->db->where($array)->get('sys_permission')->row();
		return $query;
	}

	public function delete_permission($id=null)
	{
		$check=$this->db->delete('sys_permission',array('permission_id'=>$id));
		return $check;
	}

	public function getpermission_byid($id)
	{
		$query=$this->db->where('permission_id',$id)->get('sys_permission')->row();
		return $query;
	}

	public function update_permission($data,$id=null)
	{
		if ($id) 
		{
			$check=$this->db->where('permission_id',$id)->update('sys_permission',$data);
			return $check;
		}
		return false;
	}

	public function getSettingBy($param = ""){
		$result = $this->db->where("param",$param)->get("sys_system_options")->row();
		return $result;
	}
	
}
?>