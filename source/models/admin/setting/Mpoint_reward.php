<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mpoint_reward extends CI_Model
{
	private $_table_name = 'sys_point_rewards';
	private $_primary_key = 'id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'ASC';
	public $field_order = 'id';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_id();
	}
	
	public function show_all(){ 
			/* $result = $this->db->query("SELECT
										sys_billing.bill_id,
										sys_users.username as account,
										sys_billing.first_name,
										sys_billing.last_name,
										sys_billing.email,
										sys_billing.phone,
										SUM(prices) as price,
										SUM(qty) as qty
									FROM
										sys_billing
									LEFT JOIN sys_orders ON sys_orders.bill_id = sys_billing.bill_id
									LEFT JOIN sys_users ON sys_users.user_id = sys_billing.user_id
									WHERE prices is not null
									GROUP BY email ")->result();
		 return $result;  */
		   
		 $this->db->select("sys_billing.bill_id,
							sys_users.username as account,
							sys_billing.first_name,
							sys_billing.last_name,
							sys_billing.email,
							sys_billing.phone,
							SUM(prices) as price,
							SUM(qty) as qty");
		$this->db->from("sys_billing");
		$this->db->join("sys_orders","sys_orders.bill_id = sys_billing.bill_id","LEFT");
		$this->db->join("sys_users","sys_users.user_id = sys_billing.user_id","LEFT"); 
		$this->db->where("prices is not null");
		$this->db->group_by('email');
		 
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result(); 
		 
	}
	
	public function update($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}	 

	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	
	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}
	
	public function delete($id = null) { 
		$result =  $this->db->where('bill_id', $id)
						->delete("sys_billing"); 
		return $result; 
	}
	
	public function filter_search($account,$email,$phone){
		
		$result = $this->db->query("SELECT
										sys_billing.bill_id,
										sys_users.username as account,
										sys_billing.first_name,
										sys_billing.last_name,
										sys_billing.email,
										sys_billing.phone,
										SUM(prices) as price,
										SUM(qty) as qty
									FROM
										sys_billing
									LEFT JOIN sys_orders ON sys_orders.bill_id = sys_billing.bill_id
									LEFT JOIN sys_users ON sys_users.user_id = sys_billing.user_id
									WHERE prices is not null 
									AND  sys_billing.email LIKE '%".$email."%'
									AND  sys_billing.phone LIKE '%".$phone."%'
									AND  sys_users.username  LIKE '%".$account."%'
									GROUP BY email")->result();
									 
		return $result;
		
	} 

	
}
?>