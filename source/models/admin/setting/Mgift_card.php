<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mgift_card extends CI_Model
{
	private $_table_name = 'sys_gift_cards';
	private $_primary_key = 'id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'ASC';
	public $field_order = 'id';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_id();
	}
	public function show_all(){
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function update($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}
	 
	public function getGiftCardById($id = NULL){
		$this->db->select('*');
		$this->db->from('sys_gift_cards'); 
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	} 
	
	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}
	public function delete($id = null) { 
		$result =  $this->db->where('id', $id)
						->delete($this->_table_name); 
		return $result; 
	}
	
}
?>