<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Msms_notification extends CI_Model
{
	private $_table_name = 'tbl_sms_notification';
	private $_primary_key = 'id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'ASC';
	public $field_order = 'id';

	public function save($data,$id=null)
	{
		if($id != null)
		{
			$return_update = $this->db->where('id',$id)->update($this->_table_name,$data);
			return $check;
		}
		else
		{
			$return_saved=$this->db->insert($this->_table_name,$data);
			return $return_saved;
		}	
	}

	public function get_sms_notification()
	{
		$result = $this->db->get("tbl_sms_notification")->row();
		return $result;
	}

}
?>