<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mmenu extends CI_Model
{
	private $_table_name = 'sys_menus';
	private $_primary_key = 'menu_id';

	public function save($data){
		$this->db->insert($this->_table_name,$data);
		return $this->db->affected_rows();
	}

	public function find_by_id($menu_id){
		$this->db->select()->from($this->_table_name)->where($this->_primary_key,$menu_id);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	public function update($menu_id,$data){
		$this->db->where($this->_primary_key,$menu_id);
		$this->db->update($this->_table_name,$data);
		return $this->db->affected_rows();
	}

	public function delete($menu_id){
		$this->db->where($this->_primary_key,$menu_id);
		$this->db->delete($this->_table_name);
		return $this->db->affected_rows();
	}


	//if delete menu_id update child parent_id to 0 if you don't want to lose it
	public function update_parent_id($menu_id){
		$this->db->where('parent_id',$menu_id);
		$this->db->update($this->_table_name,array('parent_id'=>0));
		
	}

	public function parseJsonArray($jsonArray, $parent_id = 0)
	{
	  $return = array();
	  foreach ($jsonArray as $subArray) {
		 $returnSubSubArray = array();
		 if (isset($subArray['children'])) {
		   $returnSubSubArray = $this->parseJsonArray($subArray['children'], $subArray['id']);
		 }
		 $return[] = array('id' => $subArray['id'], 'parent_id' => $parent_id);
		 $return = array_merge($return, $returnSubSubArray);
	  }

	  return $return;
	}
}
?>