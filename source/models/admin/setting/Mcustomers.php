<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mcustomers extends CI_Model
{
	private $_table_name = 'sys_billing';
	private $_primary_key = 'bill_id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'desc';
	public $field_order = 'bill_id';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_bill_id();
	}
	public function show_all(){
		 
		$this->db->select("sys_billing.*, sys_users.username,sys_users.user_id,prices,payment_method");
		$this->db->from($this->_table_name);
		$this->db->join("sys_users"," sys_users.user_id = sys_billing.user_id","left"); 
		$this->db->join("sys_orders"," sys_billing.bill_id = sys_orders.bill_id","inner");
		$this->db->join('sys_payment','sys_orders.payment_id = sys_payment.payment_id',"inner");
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		} 
		$query = $this->db->get();
		return $query->result_array();
	}
	public function update($bill_id,$SQLdata){
		$this->db->where($this->_primary_key,$bill_id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}

	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}
	public function delete($bill_id = null) { 
		$result =  $this->db->where('bill_id', $bill_id)
						->delete($this->_table_name); 
		return $result; 
	}
	
	public function get_user() {
		$result = $this->db->select("sys_users.user_id,sys_users.username")->from("sys_users")->get()->result();
		return $result;
	}
	
	public function search_customer($status,$username,$email,$phone) {
		 
		$username = $this->db->escape_str($username);
		$email = $this->db->escape_str($email);
		$phone = $this->db->escape_str($phone);
		 
		$this->db->select("sys_billing.*, sys_users.username,sys_users.user_id,prices,payment_method");
		$this->db->from($this->_table_name); 
		$this->db->join("sys_users"," sys_users.user_id = sys_billing.user_id","left"); 
		$this->db->join("sys_orders"," sys_billing.bill_id = sys_orders.bill_id","left");
		$this->db->join('sys_payment','sys_orders.payment_id = sys_payment.payment_id',"left");
		
		
		if($status=="inactive"){
			$this->db->where("sys_billing.status",0);
		}
		if($status == "active"){
			$this->db->where("sys_billing.status",1);
		}
		if($status == "all") {
			$this->db->order_by("sys_billing.status","asc");
		}
		
		if($username != ""){
			$this->db->where(" sys_users.username like '%".$username."%' ");
			$this->db->or_where(" sys_billing.first_name like '%".$username."%' ");
			$this->db->or_where(" sys_billing.last_name like '%".$username."%' ");
		}
		
		if($email != ""){
			$this->db->where(" sys_billing.email like '%".$email."%' ");
		}
		if($phone != ""){
			$this->db->where(" sys_billing.phone like '%".$phone."%' ");
		}
 
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
}
?>