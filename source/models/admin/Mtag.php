<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mtag extends CI_Model
{
	private $_table_name = 'tbl_tags';
	private $_primary_key = 'tag_id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';
	public $field_order = 'tag_id';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_id();
	}

	public function all(){
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}


	public function update($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}

	public function delete($id){
		$this->db->where($this->_primary_key,$id);
		$this->db->delete($this->_table_name);
		return $this->db->affected_rows();
	}


	public function find_by_id($id){
		$this->db->select()->from($this->_table_name)->where($this->_primary_key,$id);
		$query = $this->db->get();
		return $query->first_row('array');
	}

	public function search($tag_name){
		$this->db->select();
		$this->db->from($this->_table_name);
		if(!empty($tag_name)){
			$this->db->like('tag_name',$tag_name);
			$this->db->or_like('description',$tag_name);
		}
		
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function ajax_search($search){
		$result = $this->search($search);
		$json_result = array();
		foreach ($result as $value) {
			$json_result[] = array('name'=>$value['tag_name'],'value'=>$value['tag_name']);
		}
		return json_encode($json_result);
	}

	public function check_exist_tag($tag_name){
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->where('tag_name',$tag_name);
		$query = $this->db->get();
		return  $query->result_array();
	}

	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}

	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	
}
?>