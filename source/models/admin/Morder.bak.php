<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Morder extends CI_Model
{
	private $_table_name = 'sys_billing';
	private $_primary_key = 'bill_id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';
	public $field_order = 'product_id';

	public function show_all(){				   
		$this->db->select('*');
		$this->db->from('sys_billing');
		$this->db->where('status',1);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function show_one($userid){
		$this->db->select('*');
		$this->db->from('sys_billing');
		$this->db->where(array('status'=>1,'user_id' => $userid));
		$query = $this->db->get();
		return $query->result_array();
	}
	public function order_info($id){
		$this->db->select('*');
		$this->db->from('sys_billing');
		$this->db->where('bill_id',$id);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	public function show_detail($uri){
		$this->db->select('*');
		$this->db->from('sys_billing');
		$this->db->join('sys_orders','sys_orders.bill_id = sys_billing.bill_id');
                $this->db->join('sys_product','sys_product.product_id = sys_orders.product_id');
                $this->db->join('sys_payment','sys_orders.payment_id = sys_payment.payment_id');
		$this->db->where(array('sys_billing.bill_id' => $uri));
		$query = $this->db->get();
		return $query->result_array();
	}
        public function delete_order($id){
		$this->db->where($this->_primary_key,$id);
		$this->db->delete($this->_table_name);
		$bill=$this->db->affected_rows();
		if($bill){
			$this->db->where($this->_primary_key,$id);
			$this->db->delete('sys_orders');
		}
		return $this->db->affected_rows();
	}
	public function find_orderid($keyword){
		$this->db->select('*');
		$this->db->from('sys_billing');
		$this->db->join('sys_orders','sys_orders.bill_id = sys_billing.bill_id');
		$this->db->where(array('order_number' => $keyword));
		$query = $this->db->get();;
		//var_dump($query);exit();
		return $query->result_array();
	}
	public function getpost(){
		$query = $this->db->query("SELECT * FROM tbl_categories WHERE parent_id = 456");
		return $query->result_array();
	}
}