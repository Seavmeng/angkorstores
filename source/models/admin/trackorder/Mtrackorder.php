<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mtrackorder extends CI_Model{

	private $_table_name = 'sys_location';
	private $_primary_key = 'loc_id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';
	public $field_order = 'loc_id';

        function show_all(){
		$query = $this->db->query("SELECT * FROM sys_track_order");
		return $query->result_array();
	}
	function get_ordernum(){
		$query = $this->db->query("SELECT DISTINCT order_number FROM sys_billing");
		return $query->result_array();
	}
	function add_trackorder($data){
		$this->db->insert('sys_track_order',$data);
	}
        public function deletetrackorder($id){
		$this->db->where('track_order_id',$id);
		$this->db->delete('sys_track_order');
		return $this->db->affected_rows();
	}
}