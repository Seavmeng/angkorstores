<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Muser extends CI_Model{
	
	private $_table_name = 'sys_users';
	private $_primary_key = 'user_id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';
	public $field_order = 'user_id';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_id();
	}

	public function all(){
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function join(){
		$this->db->select('u.*,ug.*,ud.image');
		$this->db->from('sys_users u');
		$this->db->join('sys_user_groups ug','ug.user_group_id = u.group_id');
		$this->db->join('sys_users_detail ud','ud.user_id=u.user_id');
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function supplier(){
		$user_id=get_user_id();
		$group_id = user_group_id('Supplier');
		$this->db->select('*');
		$this->db->from('sys_users');
		$this->db->where('group_id',$group_id);
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function update($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}

	public function delete($id){
		$this->db->where($this->_primary_key,$id);
		$this->db->delete($this->_table_name);
		return $this->db->affected_rows();
	}

	public function find_by_id($id){
		$this->db->select()->from($this->_table_name)->where($this->_primary_key,$id);
		$query = $this->db->get();
		return $query->first_row('array');
	}

	public function search($uname,$email,$user_active, $role){
	  
		$username = trim($uname);
		$email = trim($email);
		$user_active = trim($user_active); 
		$role = trim($role);
		
		$this->db->select("*");
		$this->db->from('sys_users u');
		$this->db->join('sys_user_groups ug','ug.user_group_id = u.group_id');
		$this->db->join('sys_users_detail ud', 'ud.user_id = u.user_id');
		if(!empty($username)){
			$this->db->like('username',$username);
		}
		if(!empty($email)){
			$this->db->like('email',$email);
		}
		if(!empty($user_active)){
			$user_actives = ($user_active == 'active') ? 1 : 0;
			$this->db->or_where('user_active', $user_actives);
		}
		if(!empty($role)){
			$this->db->where('u.group_id', $role);
		}
		$this->db->order_by('u.user_id',$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}

	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}

	public function select_user_detil_by_id($id){
		//$this->db->select()->from('sys_users_detail')->where('user_id',$id);
		$this->db->select('*');
		$this->db->from('sys_users_detail');
		$this->db->join('sys_users','sys_users.user_id = sys_users_detail.user_id');
		$this->db->where('sys_users_detail.user_id',$id);
		$query = $this->db->get();
		return $query->first_row('array');
	}

	public function getuser_detail_byname($user_page){
		$this->db->select()->from('sys_users_detail')->join('sys_users','sys_users.user_id=sys_users_detail.user_id')->where('page_name',$user_page);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	
	public function getuser($user_id){
		$this->db->select('*');
		$this->db->from('sys_users_detail');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	
	public function country(){
		$query = $this->db->query("SELECT * FROM sys_location");
		return $query->result_array();
	}

	
}
?>