<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor_model extends CI_Model{
	public function index(){
		$this->db->select();
		$this->db->from('sys_users_detail');
		$this->db->join('sys_users', 'sys_users.user_id = sys_users_detail.user_id');
		$this->db->where('sys_users.group_id', 2);
		$qry = $this->db->get();
		return $qry->result_array();
	}
	public function disable($param, $param2){
		$this->db->where('user_id', $param);
		$this->db->update('sys_users_detail', $param2);
		return $this->db->affected_rows();
	}
}