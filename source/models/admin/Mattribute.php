<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mattribute extends CI_Model{
	
	private $_table_name = 'sys_attribute';
	private $_primary_key = 'attr_id';

	public $per_page =10;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'ASC';
	public $field_order = 'attr_id';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_id();
	}
	public function check_dup_2column($field1="",$field2="",$user_id=""){
		$ci =& get_instance();
		$this->db->from($this->_table_name);
		$this->db->where(array('parent_id'=>$field1,'attr_name'=>$field2,'user_id'=>$user_id));
		$query = $ci->db->get();
		return $query->first_row('array');
	}
	public function all(){
		$user_id = has_logged();
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->where(array('user_id'=>$user_id,'parent_id'=>0));
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function count_all(){
		$user_id = has_logged();
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}


	public function update($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}

	public function delete($id){
		$this->db->where($this->_primary_key,$id);
		$this->db->delete($this->_table_name);
		return $this->db->affected_rows();
	}
	public function find_by_id($id){
		$this->db->select()->from($this->_table_name)->where($this->_primary_key,$id);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	public function results($category_name){
		$user_id = has_logged();
		$this->db->select();
		$this->db->from($this->_table_name);

		$this->db->where('user_id', $user_id);
		if(!empty($category_name)){
			$this->db->like('category',$category_name);
		}
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}	
	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	public function more_attr($attr_id){
		$user_id = has_logged();
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->where(array('user_id'=>$user_id,'parent_id'=>$attr_id));
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
}
?>