<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Muser extends CI_Model
{
	private $_table_name = 'sys_users';
	private $_primary_key = 'user_id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';
	public $field_order = 'user_id';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_id();
	}
	public function saveShop($data){
		$this->db->insert('sys_users_detail',$data);
		return $this->db->insert_id();
	}
	public function updateUser($user_id,$data){
		$this->db->where('user_id',$user_id);
		$this->db->update('sys_users_detail',$data);
		return $this->db->affected_rows();
	}
	public function check_user_email($email){
		$ci =& get_instance();
		$this->db->from($this->_table_name);
		$this->db->where('email',$email);
		$query = $ci->db->get();
		$data=$query->first_row('array');
		return $data['email'];
	}
	public function all(){
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function update($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}
	public function updateShop($user_id,$SQLdata){
		$this->db->where('user_id',$user_id);
		$this->db->update('sys_user_detail',$SQLdata);
		return $this->db->affected_rows();
	}
	public function activate($id,$SQLdata){
		$this->db->where('Activecode',$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}
	public function complete($id,$SQLdata){
		$this->db->where('user_id',$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}
	public function recovery($email,$id,$SQLdata){
		$this->db->where(array('email'=>$email,'ResetCode'=>$id));
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}
	public function users_role(){
		$this->db->select();
		$this->db->from('sys_user_groups');
		$this->db->Where('user_group_name !=','Administrator');
		$this->db->Where('user_group_name !=','Subscriber');
		$query = $this->db->get();
		return $query->result_array();
	}
	function businessType(){
		$this->db->select()->from('sys_industry');
		$query = $this->db->get();
		return $query->result_array();
	}
	function find_profile_id($user_id){
		$this->db->select();
		$this->db->from('kp_user_view');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}

	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	public function member_package(){
		$this->db->select();
		$this->db->from('sys_user_packages');
		$this->db->where('parent_id',0);
	//	$this->db->order_by($this->field_order,$this->order_by);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function country(){
		$query = $this->db->query("SELECT * FROM sys_location GROUP BY location_name");
		return $query->result_array();
	}
	public function shipCompany(){
		$this->db->select();
		$this->db->from('sys_shopping_name');
		//$this->db->where('parent_id', 103);
		$qry = $this->db->get();
		return $qry->result_array();
		
	}
	public function getuser($user_id){
		//$query = $this->db->query("SELECT * FROM sys_users_detail WHERE user_id = '.$id.'");
		$this->db->select('*');
		$this->db->from('sys_users_detail');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	public function recove($id,$email,$data){
		$this->db->where(array('email'=>$email,'ResetCode'=>$id));
		$this->db->update($this->_table_name,$data);
		return $this->db->affected_rows();
	}
	public function get_vender(){
		$query = $this->db->query("SELECT * FROM sys_user_groups WHERE user_group_id=2");
		return $query->first_row('array');
	}
	public function get_billing(){
		$query = $this->db->query("SELECT * FROM sys_user_groups WHERE user_group_id=6");
		return $query->first_row('array');
	}
	
	public function find_usID($id){
		$this->db->select('user_id');
		$this->db->from('sys_users_detail');
		$this->db->where('user_id',$id);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	public function add_UserId_Detail($data){
		$this->db->insert('sys_users_detail',$data);
		return $this->db->insert_id();
	}

}
?>