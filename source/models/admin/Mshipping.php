<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mshipping extends CI_Model
{
	private $_table_name = 'sys_shopping_name';
	private $_primary_key = 'shipping_id';

	public $per_page = 50;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';
	public $field_order = 'shipping_id';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_id();
	}
	// public function save_method($SQLdata){
	// 	$this->db->insert('sys_shopping_method',$SQLdata);
	// 	return $this->db->insert_id();
	// }
	public function save_method($data){
		$this->db->insert('sys_shopping_method',$data);
	 	return $this->db->insert_id();
	}
	// public function save_weight_price($SQLdata){
	// 	$this->db->insert('sys_shopping_price',$SQLdata);
	// 	return $this->db->insert_id();
	// }
	public function save_weight_price($data1){
		$this->db->insert('sys_shopping_price',$data1);
	 	//return $this->db->insert_id();
	}
	public function add_price($data2){
		$this->db->insert('sys_shopping_price',$data2);
	}
	public function show_all(){
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function show_all_methods(){
		$this->db->select();
		$this->db->from('sys_shopping_method');
		$this->db->order_by('method_id','DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function show_all_methods_by_id($id=false){
		$this->db->select();
		$this->db->where('shipping_id',$id);
		$this->db->from('sys_shopping_method');
		$this->db->order_by('method_id','DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function shipping_name($id){
		$this->db->select('shipping_name');
		$this->db->from('sys_shopping_name');
		$this->db->where('shipping_id',$id); 
		$query = $this->db->get();
		$data=$query->first_row('array');
		if($data['shipping_name'] ==""){
			redirect(base_admin_url().'shipping');
		}else{
			return $data['shipping_name'];
		}
	}
	/*
	 khorng
	*/
	public function update_method($mid,$data){
		$this->db->where('method_id',$mid);
		$this->db->update('sys_shopping_method',$data);
		return $this->db->affected_rows();
	}

	public function update_price($pid,$data1){
		$this->db->where('shipping_price_id',$pid);
		$this->db->update('sys_shopping_price',$data1);
		//return $this->db->affected_rows();
	}
	/*end*/
	public function update($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}
	public function update_inv($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update('sys_inventory',$SQLdata);
		return $this->db->affected_rows();
	}
	public function delete($id){
		$this->db->where($this->_primary_key,$id);
		$this->db->delete($this->_table_name);
		return $this->db->affected_rows();
	}
	public function delete_md($id){
		$this->db->where('method_id',$id);
		$this->db->delete('sys_shopping_method');
		return $this->db->affected_rows();
	}
	public function delete_shipping_price($id){
		$this->db->where('shipping_price_id',$id);
		$this->db->delete('sys_shopping_price');
		return $this->db->affected_rows();
	}
	public function find_by_id($id){
		$this->db->select('*');
		$this->db->from('sys_shopping_method');
		$this->db->join('sys_shopping_price','sys_shopping_price.method_id = sys_shopping_method.method_id');
		$this->db->where(array('sys_shopping_method.method_id' => $id)); 
		$query = $this->db->get();
		return $query->first_row('array');
		
	}
	// public function find_id($pid){
	// 	//$query = $this->db->query("SELECT * FROM sys_shopping_price WHERE shipping_price_id = ".$pid."");
	// 	$this->db->select('*');
	// 	$this->db->from('sys_shopping_price');
	// 	$this->db->where(array('shipping_price_id' => $pid));
	// 	$query = $this->db->get();
	// 	return $query->result_array();
	// }
	public function brand(){

		$this->db->select();
		$this->db->from('sys_brand');
		$query = $this->db->get();
		return $query->result_array();
	}
	public function category(){

		$this->db->select();
		$this->db->from('sys_category');
		$query = $this->db->get();
		return $query->result_array();
	}
	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}


	/*
		@khorng
	*/
	public function get_country(){
	$this->db->select('*');
	$this->db->from('sys_location');
	$this->db->group_by('location_name');
	return $this->db->get()->result_array();
	}
	public function insert_zone($data){
		$this->db->insert('sys_shopping_zone', $data);
	}

	public function get_shipping(){
		$this->db->select('*');
		$this->db->from('sys_shopping_name');
		return $this->db->get()->result_array();
	}	

	public function get_tblzone($id){
		$this->db->select('*');
		$query = $this->db->get_where('sys_shopping_zone',array('zone_id' => $id)); 
		return $query->row_array();
	}

	/*************************zone********************/
	public function get_zname(){
		$query = $this->db->query("SELECT *  FROM sys_shopping_name ssn
							INNER JOIN sys_shopping_zone ssz ON ssz.shipping_id = ssn.shipping_id
							WHERE zone_name IS NOT NULL ORDER BY zone_id DESC
							;");
		return $query->result_array();
	}
	
	//************ save new shipping
	public function save_shipping($data=null,$id=null) 
	{
		if(!empty($id)) {
			$result=$this->db->where("shipping_id",$id)->update("sys_shopping_name",$data);
			return $result;
		}else{ 
			$this->db->insert('sys_shopping_name',$data);
			return $this->db->insert_id();
		}
	}
	//*********** delete shipping 
	public function delete_shipping($id = null) 
	{
		$result = $this->db->where("shipping_id",$id)->delete("sys_shopping_name");
		return $result;
	}
 
	//*********** get shipping  by id
	public function getShippingById($id = null) 
	{
		$this->db->select("*");
		$this->db->where('shipping_id',$id);
		$this->db->from('sys_shopping_name');  
		$query = $this->db->get();
		return $query->row();
	}
 
 
 }
?>