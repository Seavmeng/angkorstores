<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Moproduct extends CI_Model
{
	private $_table_name = 'sys_product';
	private $_primary_key = 'product_id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';
	public $field_order = 'product_id';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_id();
	}
	public function show_all(){
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function update($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}
	public function update_inv($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update('sys_inventory',$SQLdata);
		return $this->db->affected_rows();
	}
	public function delete($id){
		$this->db->where($this->_primary_key,$id);
		$this->db->delete($this->_table_name);
		return $this->db->affected_rows();
	}
	public function find_by_id($id, $user_id){
	//	$user_id = has_logged();
		$this->db->select('*');
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->where(array('sys_product.product_id' => $id,'user_id'=>$user_id));
		$query = $this->db->get();
		return $query->first_row('array');
	}
	public function brand(){

		$this->db->select();
		$this->db->from('sys_brand');
		$query = $this->db->get();
		return $query->result_array();
	}
	public function category(){

		$this->db->select();
		$this->db->from('sys_category');
		$query = $this->db->get();
		return $query->result_array();
	}
	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}
}
?>