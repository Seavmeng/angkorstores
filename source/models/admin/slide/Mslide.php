<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mslide extends CI_Model
{
	private $_table_name = 'tbl_slide';
	private $_primary_key = 'slide_id';

	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';
	public $field_order = 'slide_id';
	//public $post_type = 'post';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_id();
	}

	public function all(){
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->order_by($this->field_order,$this->order_by);
		//$this->db->where('post_type', $this->post_type);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	// Updated 2-3-2017
	public function join(){
		$group_id = $this->session->userdata('group_id');
		$user_id = $this->session->userdata('user_id');
		//var_dump($group_id);var_dump($user_id);exit();
		$this->db->select();
		$this->db->from('tbl_slide');
		if($group_id != 1){
			$this->db->where('user_id',$user_id);
			$this->db->where('group_id',$group_id);
		}else{
			$this->db->where('group_id',$group_id);
		}
		//$this->db->join('sys_users u','u.user_id = post.user_id');
		//$this->db->where(array('post_type'=> $this->post_type));
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}


	public function update($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}

	public function delete($id){
		$this->db->where(array($this->_primary_key=>$id));
		$this->db->delete($this->_table_name);
		return $this->db->affected_rows();
	}


	public function find_by_id($id){
		$this->db->select()->from($this->_table_name)->where(array($this->_primary_key=>$id));
		$query = $this->db->get();
		return $query->first_row('array');
	}

	public function search($tag_id,$from,$to,$text){
		$this->db->select('slide.*');
		$this->db->from('tbl_slide slide');
		$this->db->join('tbl_links link','link.slide_id = slide.slide_id');
		//$this->db->join('sys_users u','u.user_id = post.user_id');
		//$this->db->where('post_type',$this->post_type);
	/*	if(!empty($user_id)){
			$user_id = real_int($user_id);
			$this->db->where('post.user_id',$user_id);
		}
	*/	
		if(!empty($tag_id)){
			$tag_id = real_int($tag_id);
			$this->db->where('tag_id',$tag_id);
		}
	/*	if(!empty($category_id)){
			$category_id = real_int($category_id);
			$this->db->where('link.category_id',$category_id);
		}
	
		if(!empty($from) && !empty($to)){
			$this->db->where(array('publish_date >=' => $from, 'publish_date <=' => $to));
		}
	*/
	/*	if(!empty($visibility)){
			$visibility = $visibility == 'public' ? 1 : 0 ;
			$this->db->where('visibility',$visibility);
		}
	*/
		if(!empty($text)){
				$this->db->where("(`post_title` LIKE '%".$this->db->escape_like_str($text)."%' OR `description` LIKE '%".$this->db->escape_like_str($text)."%' OR `article` LIKE '%".$this->db->escape_like_str($text)."%' )", null);
		}

		$this->db->group_by('slide.slide_id');
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}

	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	public function slide_count(){
		$user_id = $this->session->userdata('user_id');
		$this->db->select('*');
		$this->db->from('tbl_slide');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		return $query->num_rows();
	}// Developed by Khaing
}
?>