<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Moproduct extends CI_Model
{
	private $_table_name = 'sys_product';
	private $_view_table_name = 'sys_viewproduct';
	private $_comment_table = 'sys_comment';
	private $_image_table = 'image_liabrary';
	private $_primary_key = 'product_id';

	public $per_page = 10;
	public $start = 0;
	public $count_all = false;
	public $order_by = 'DESC';
	public $field_order = 'product_id';

	public function save($SQLdata){
		$this->db->insert($this->_table_name,$SQLdata);
		return $this->db->insert_id();
	}
	
	public function save_attr($SQLdata){
		$this->db->insert('sys_attributes_pro',$SQLdata);
		return $this->db->insert_id();
	}
	
	public function save_varian($SQLdata){
		$this->db->insert('sys_variable',$SQLdata);
		return $this->db->insert_id();
	}
	
	public function getBidStatus($param){
		$this->db->select('*');
		$this->db->from('sys_inventory');
		$this->db->where('product_id', $param);
		$qry = $this->db->get();
		return $qry->result_array();
	}
	
	public function show_all($user_id,$group_id){
		$this->db->select('p.*,u.username,g.user_group_name');
		$this->db->from('kp_product_view p');
		$this->db->join('sys_users u','u.user_id = p.user_id');
		$this->db->join('sys_user_groups g','g.user_group_id = u.group_id');
		$this->db->where(array('p.post_status'=>0));
		$this->db->where('p.status',1);		
		if($group_id !=1){
			$this->db->where(array(
					'p.user_id'=>$user_id, 
				)
			);
		}else{
			if($this->uri->segment(3) == 'supplier_product'){
				$this->db->where('u.group_id',2);  
				$this->db->or_where('p.status',0);  
				
			}else{
				$this->db->where('u.group_id',1);
			}
		}
		
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		return $query->result_array();
	}
	
	public function update($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name,$SQLdata);
		return $this->db->affected_rows();
	}
	
	public function update_inv($id,$SQLdata){
		$this->db->where($this->_primary_key,$id);
		$this->db->update('sys_inventory',$SQLdata);
		return $this->db->affected_rows();
	}
	
	public function update_cate($id,$SQLdata){
		$this->db->where('post_id',$id);
		$this->db->update('sys_relationship',$SQLdata);
		return $this->db->affected_rows();
	}
	
	public function update_gall($id,$SQLdata){
		$this->db->where('post_id',$id);
		$this->db->update('sys_gallery',$SQLdata);
		return $this->db->affected_rows();
	}
	
	public function deleteproduct($id){
		$this->db->where($this->_primary_key,$id);
		$this->db->update($this->_table_name, array("post_status"=>1, "status"=>0));
		return $this->db->affected_rows();
	}
	
	public function deleteinventory($id){
		$this->db->where($this->_primary_key,$id);
		$this->db->delete('sys_inventory');
		return $this->db->affected_rows();
	}
	
	public function deletegallery($id){
		$this->db->where('post_id',$id);
		$this->db->delete('sys_gallery');
		return $this->db->affected_rows();
	}
	
	public function deleterelation($id){
		$this->db->where('post_id',$id);
		$this->db->delete('sys_relationship');
		return $this->db->affected_rows();
	}
	
	public function deleteByProCode($proCode){
		$this->db->where('product_code', $proCode);
		$this->db->update($this->_table_name, array("post_status"=>1, "status"=>0));
		return $this->db->affected_rows();
	}

	public function get_id_by_code($proCode){
		$this->db->select($this->_primary_key);
		$this->db->from($this->_table_name);
		$this->db->where('product_code', $proCode);
		$qry = $this->db->get();
		$data = $qry->first_row('array');
		return $data['product_id'];
	}
	
	public function find_group_id($permalink){
		$this->db->select('*');
		$this->db->from('sys_product');
		$this->db->join('sys_users','sys_users.user_id = sys_product.user_id');
		$this->db->where(array('sys_product.permalink' => $permalink));
		$query = $this->db->get();
		return $query->first_row('array');	
	}
	
	public function get_contact_user($id){
		$this->db->select('*');
		$this->db->from('sys_users_detail');
		$this->db->join('sys_users','sys_users.user_id = sys_users_detail.user_id');
		$this->db->where(array('sys_users_detail.user_id' =>$id ));
		$query=$this->db->get();
		return $query->first_row('array');
	}

	public function find_by_id($permalink){
		$user_id = has_logged();
		$this->db->select('*');
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
		$this->db->where(array('sys_product.permalink' => $permalink));
		//$this->db->where('sys_product.status',1);
		//$this->db->where('post_status',0);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	
	public function getProById($id, $userId){
		$user_id = has_logged();
		$this->db->select('*');
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
		$this->db->where(array('sys_product.product_id'=>$id));
		$query = $this->db->get();
		return $query->first_row('array');
	}
	
	public function getCatRelationship($id){
		$user_id = has_logged();
		$this->db->select('category_id');
		$this->db->from('sys_relationship');
		$this->db->join('sys_inventory', 'sys_relationship.post_id = sys_inventory.product_id');
		$this->db->where(array('sys_relationship.post_id'=>$id));
		$query = $this->db->get();
		return $query->result_array();
	}

	public function product_detail($id,$user_id){
		$this->db->select('*');
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
		$this->db->where(array('sys_product.product_id' => $id,'user_id'=>$user_id)); 
		$query = $this->db->get();
		return $query->first_row('array');
		
	}
	
	public function get_id_by_permalink($permalink){
		$this->db->select('product_id');
		$this->db->from('kp_product_view');
		$this->db->where(array('permalink'=> $permalink));
		$query = $this->db->get();
		return $query->first_row('array');
		
	}
	
	public function find_gallery($id){
		$this->db->select('*');
		$this->db->from('sys_gallery');
		$this->db->where(array('post_id' => $id)); 
		$query = $this->db->get();
		return $query->first_row('array');
		
	}
	
	public function gallery_detail($id){
	//	$user_id = has_logged();
		$this->db->select('*');
		$this->db->from('sys_gallery');
		$this->db->where(array('post_id' => $id)); 
		$query = $this->db->get();
		return $query->first_row('array');
		
	}
	
	public function find_inventory(){
	//	$user_id = has_logged();
		$this->db->select('*');
		$this->db->from('sys_inventory');
		$this->db->where(array('product_id' => $id)); 
		$query = $this->db->get();
		return $query->first_row('array');
		
	}
	
	public function find_attribut(){
	//	$user_id = has_logged();
		$this->db->select('*');
		$this->db->from('sys_attribute');
		$query = $this->db->get();
		return $query->first_row('array');
		
	}
	
	public function brand($id){
		$this->db->distinct();
		$this->db->select('brand');
		$this->db->from('sys_brand');
		$this->db->where('category_id',$id);
		$this->db->or_where('parent_id',$id);
		$this->db->group_by('brand');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function search_brand($search){
		$this->db->distinct();
		$this->db->select();
		$this->db->from('sys_brand');
		$this->db->where('category_id',$id);
		$this->db->group_by('brand');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function category($search){
		$this->db->select();
		$this->db->from('sys_category');
		$this->db->like('category',$search);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function page($base_url,$data){
		$count_all = count($data);
		return pages($base_url,$count_all,$this->per_page);
	}
	
	public function showing($data){
		$count_all = count($data);		
		return showing($count_all,$data,$this->per_page);
	}
	
	public function search($tag_name){
		$this->db->select();
		$this->db->from('sys_tags');
		if(!empty($tag_name)){
			$this->db->like('tag_name',$tag_name);
			$this->db->or_like('description',$tag_name);
		}
		
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function ajax_search($search){
		$result = $this->search($search);
		$json_result = array();
		foreach ($result as $value) {
			$json_result[] = array('name'=>$value['tag_name'],'value'=>$value['tag_name']);
		}
		return json_encode($json_result);
	}

	public function check_exist_tag($tag_name){
		$this->db->select();
		$this->db->from($this->_table_name);
		$this->db->where('tag_name',$tag_name);
		$query = $this->db->get();
		return  $query->result_array();
	}
	
	public function relatedCat($param){
		$this->db->select();
		$this->db->from('sys_relationship');
		$this->db->where('post_id',$param);
		$qry = $this->db->get();
		return $qry->first_row('array');
	}
	
	public function relatedPro($param){
		$this->db->select();
		$this->db->from('kp_product_view p');
		$this->db->join('sys_relationship r', 'p.product_id = r.post_id');
		$this->db->where('r.category_id', $param);
		$this->db->limit(18,0);
		$this->db->order_by('rand()');
		return $this->db->get()->result_array();
	}
	
	public function relatedProVendor($param){
		$this->db->select();
		$this->db->from('kp_product_view p');
		$this->db->join('sys_relationship r', 'p.product_id = r.post_id');
		$this->db->join('sys_users u','u.user_id = p.user_id');
		$this->db->where('r.category_id', $param);
		$this->db->where('p.status',1);
		$this->db->where('u.group_id',2);
		$this->db->limit(18,0);
		$this->db->order_by('rand()');
		return $this->db->get()->result_array();
	}
	//####################### Select new Product ###########################//
	public function hot_product(){
		$this->db->select('*');
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
		$this->db->where('hot_pro', 1); 
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function phone_hot($name){
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		//$this->db->where('hot_pro', 1); 
		$this->db->where('category', $name); 
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//####################### Select top sale Product ###########################//
	public function top_sale_pro(){
		$this->db->select('*');
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->where('sale_amoung >=', 1); 
		$this->db->order_by('sale_amoung','desc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//####################### Select Discount Product ###########################//
	public function discount_pro(){
		$this->db->select('*');
		$this->db->from('sys_viewproduct');
		$this->db->where('have_discount', 1);
		$this->db->order_by('created_date', 'desc');
		$this->db->limit(10);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//####################### Select all Product ###########################//
	public function all_pro(){
		$this->db->select('*');
		$this->db->from('sys_viewproduct');
		//$this->db->limit(10);
		$this->db->order_by('created_date', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function complete_save($SQLdata){
		$this->db->insert('users_detail',$SQLdata);
		return $this->db->insert_id();
	}
	
	public function complete_update($id,$SQLdata){
		$this->db->where('id',$id);
		$this->db->update('users',$SQLdata);
		return $this->db->affected_rows();
	}
	
	public function complete_active($id,$SQLdata){
		$this->db->where('id',$id);
		$this->db->update('users',$SQLdata);
		return $this->db->affected_rows();
	}
	
	public function group_pro_list($id){
		$this->db->select('*');
		$this->db->from('sys_category');
		$this->db->where('category_id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//get product price on category page
	public function get_pro_pirce($id){
		$this->db->select('*');
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
		$this->db->where('category_id', $id); 
		$this->db->order_by("sale_price", "asc");
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//get all info on category page
	public function get_pro_detail($id,$type,$brand){
		if(is_numeric($type)){
			if($brand == ""){
				$this->db->select('*');
				$this->db->from('sys_product');
				$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
				$this->db->where('category_id', $type); 
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($brand == "sale"){
				$this->db->select('*');
				$this->db->from('sys_product');
				$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
				$this->db->where('category_id', $type); 
				//$this->db->where('sale_amoung >=', 3);
				$this->db->order_by('sale_amoung', 'desc');
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($brand == "pop"){
				$this->db->select('*');
				$this->db->from('sys_product');
				$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
				$this->db->where('view >=', 3);
				$this->db->where('category_id', $type); 
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($brand == "new"){
				$this->db->select('*');
				$this->db->from('sys_product');
				$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
				//$this->db->where(array('hot_pro' => 1,'category_id'=>$id)); 
				$this->db->where('category_id',$type);
				$this->db->order_by('created_date', 'DESC');
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($brand == "price"){
				$this->db->select('*');
				$this->db->from('sys_product');
				$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
				$this->db->where('category_id',$type);
				$this->db->order_by("regular_prices", "asc");
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
		}
		else{
			if($type==""){
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				//$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				//$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
				$this->db->where('category_id',$id);
				$this->db->or_where('parent_id',$id);
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($type == "sale"){
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				//$this->db->from('sys_product');
				//$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				//$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
				//$this->db->where('sale_amoung >=', 3);
				$this->db->where('category_id', $id);
				$this->db->or_where('parent_id',$id);
				$this->db->order_by('sale_amoung', 'desc');
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($type == "pop"){
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				//$this->db->from('sys_product');
				//$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				//$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
				//$this->db->where('view >=', 3);
				$this->db->where('category_id', $id); 
				$this->db->or_where('parent_id',$id);
				$this->db->order_by('view', 'desc');
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($type == "new"){
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				//$this->db->where(array('hot_pro' => 1,'category_id'=>$id)); 
				$this->db->where('category_id',$id);
				$this->db->or_where('parent_id',$id);
				$this->db->order_by('created_date', 'DESC');
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($type == "price"){
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				$this->db->where('category_id', $id); 
				$this->db->or_where('parent_id',$id);
				$this->db->order_by("regular_prices", "asc");
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($brand=="sale"){
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				$this->db->where('category_id', $id); 
				//$this->db->or_where('parent_id', $id); 
				$this->db->where('sale_amoung >=', 3);
				$this->db->where('brand',$type);
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($brand=="pop"){
				$this->db->select('*');
				$this->db->from('sys_product');
				$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
				$this->db->where('view >=', 3);
				$this->db->where('category_id', $id); 
				$this->db->where('brand',$type);
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($brand=="new"){
				$this->db->select('*');
				$this->db->from('sys_product');
				$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
				//$this->db->where(array('hot_pro' => 1,'category_id'=>$id)); 
				$this->db->where('category_id',$id);
				$this->db->where('brand',$type);
				$this->db->order_by('created_date', 'DESC');
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			elseif($brand=="price"){
				$this->db->select('*');
				$this->db->from('sys_product');
				$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
				$this->db->where('category_id', $id); 
				$this->db->where('brand',$type);
				$this->db->order_by("regular_prices", "asc");
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
			else{
				$parent_id = get_parent_id($id);
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				//$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
				//$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id'); 
				$this->db->where('brand',$type);
				if($parent_id == $id){
					$this->db->where('parent_id', $id);
				}else{
					$this->db->where('category_id', $id);
				}
				
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
		}
	}
	
	public function view_detail($id,$type,$brand){
		$this->db->select('attribute_size');
		$this->db->from('sys_search_pro');
		$this->db->where('category_id',$id);
		$query = $this->db->get();
		$get_category = $query->result_array();
		foreach($get_category as $row){
			$get_size []=$row['attribute_size'];
		}
		$get_sizes=implode(',', $get_size);
		$exsize = explode(',',$get_sizes);
		$test = array_values(array_unique($exsize));
		foreach($test as $rows){
			$this->db->select('*');
			$this->db->from('sys_product');
			$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
			$this->db->where($rows, $type);
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
		}
		return $query->result_array();
		
		/*
		if($brand==""){
			$this->db->select('*');
			$this->db->from('sys_product');
			$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
			$this->db->where('attribute_size', $type);
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}
		*/		
	}
	
	public function get_input_detail($id,$type){
		if($type==""){
			$this->db->select('*');
			$this->db->from('sys_product');
			$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
			$this->db->where('category_id',$id);
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}elseif($type == "sale"){
			$this->db->select('*');
			$this->db->from('sys_product');
			$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
			$this->db->where('sale_amoung >=', 3);
			$this->db->where('category_id', $id);
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}elseif($type == "pop"){
			$this->db->select('*');
			$this->db->from('sys_product');
			$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
			$this->db->where('view >=', 3);
			$this->db->where('category_id', $id); 
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}elseif($type == "new"){
			$this->db->select('*');
			$this->db->from('sys_product');
			$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
			//$this->db->where(array('hot_pro' => 1,'category_id'=>$id)); 
			$this->db->where('category_id',$id);
			$this->db->order_by('created_date', 'DESC');
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}elseif($type == "price"){
			$this->db->select('*');
			$this->db->from('sys_product');
			$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
			$this->db->where('category_id', $id); 
			$this->db->order_by("sale_price", "asc");
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}else{
			$this->db->select('*');
			$this->db->from('sys_product');
			$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
			$this->db->where('category_id', $id); 
			$this->db->where('brand',$type);
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}
	}
	
	//#################### Get Attribute #######################//
	
	//show new product on category page
	public function new_product($id){
		$this->db->select('*');
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
		//$this->db->where(array('hot_pro' => 1,'category_id'=>$id)); 
		$this->db->where('category_id',$id);
		$this->db->order_by('created_date', 'DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
		
	//show new product search data
	public function new_products($id){
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		$this->db->where(array('created_date <='=>date('Y-m-d h:i:s'),'user_id'=>$id));
		$this->db->order_by('created_date', 'DESC');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//show popular product on category page
	public function popular_product($id){
		$this->db->select('*');
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
		$this->db->where('view >=', 3);
		$this->db->where('category_id', $id); 
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//show recommend products in blank categories
	//show popular product on category page
	public function recommend_product(){
		$this->db->select('*');
		$this->db->from('kp_product_view p');
		$this->db->join('sys_users u','u.user_id=p.user_id');
		//$this->db->from('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		//$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
		//$this->db->where('hot_pro', 1);
		//$this->db->where('category_id', $id);
		$this->db->order_by('p.product_id','ASC');
		
		if(!$this->count_all){
			$this->db->limit(12,0);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//show sale product on category page
	public function sale_product($id){
		$this->db->select('*');
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
		$this->db->where('sale_amoung >=', 3);
		$this->db->where('category_id', $id);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function count_all(){
		$user_id = has_logged();
		$this->db->select();
		$this->db->from($this->_view_table_name);
		$this->db->order_by($this->field_order,$this->order_by);
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_pro_by_shop($user_id,$id){
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		if(empty($id)){
			$this->db->where('user_id', $user_id); 
		}else{
			$this->db->where(array('user_id'=>$user_id, 'category_id'=>$id)); 
		}
		$this->db->order_by("product_id", "DESC");
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_sale_shop($user_id){
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		$this->db->where(array('user_id'=>$user_id,'sale_amoung >='=>2)); 
		$this->db->order_by("sale_amoung", "DESC");
		$this->db->limit(2);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_view_shop($user_id){
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		$this->db->where(array('user_id'=>$user_id,'view >='=>2)); 
		$this->db->order_by("view", "DESC");
		$this->db->limit(2);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function insert_comment($SQLdata){
		$this->db->insert($this->_comment_table,$SQLdata);
		return $this->db->insert_id();
	}
	
	//#################### Search General ######################//
	
	//show all search data
	public function search_cate($name,$type,$brand){
		if($type == ""){
			$this->db->select('*');
			$this->db->from('sys_search_pro');
			$this->db->like('product_name', $name);
			$this->db->or_like('category', $name);
			$this->db->order_by("product_id", "DESC");
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}elseif($type == "sale"){
			$this->db->select('*');
			$this->db->from('sys_search_pro');
			//$this->db->where('sale_amoung >=', 3);
			$this->db->where("(category like '%$name%' OR product_name like '%$name%')", NULL, FALSE);
			$this->db->order_by("sale_amoung", "DESC");
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}elseif($type == "pop"){
			$this->db->select('*');
			$this->db->from('sys_search_pro');
			//$this->db->where('view >=', 3);
			$this->db->where("(category like '%$name%' OR product_name like '%$name%')", NULL, FALSE);
			$this->db->order_by("view", "DESC");
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}elseif($type == "new"){
			$this->db->select('*');
			$this->db->from('sys_search_pro');
			$this->db->like('product_name', $name);
			$this->db->or_like('category', $name);
			$this->db->order_by("created_date", "DESC");
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}elseif($type == "price"){
			$this->db->select('*');
			$this->db->from('sys_search_pro');
			$this->db->like('product_name', $name);
			$this->db->or_like('category', $name);
			$this->db->order_by("regular_prices", "asc");
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query = $this->db->get();
			return $query->result_array();
		}elseif($type != "sale" and $type != "pop" and $type != "new" and $type != "price"){
			if($brand == ""){
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				$this->db->where('brand',$type);
				$this->db->where("(category like '%$name%' OR product_name like '%$name%')", NULL, FALSE);
				//$this->db->like('product_name',$name);
				//$this->db->order_by("sale_price", "asc");
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}elseif($brand == "sale"){
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				$this->db->where('brand',$type);
				$this->db->where("(category like '%$name%' OR product_name like '%$name%')", NULL, FALSE);
				//$this->db->like('product_name',$name);
				$this->db->order_by("sale_amoung", "desc");
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}elseif($brand == "pop"){
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				$this->db->where(array('brand'=>$type,'view >=' => 3));
				$this->db->where("(category like '%$name%' OR product_name like '%$name%')", NULL, FALSE);
				//$this->db->like('product_name',$name);
				$this->db->order_by("view", "desc");
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}elseif($brand == "new"){
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				$this->db->where('brand',$type);
				$this->db->where("(category like '%$name%' OR product_name like '%$name%')", NULL, FALSE);
				//$this->db->like('product_name',$name);
				$this->db->order_by("created_date", "desc");
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}elseif($brand == "price"){
				$this->db->select('*');
				$this->db->from('sys_search_pro');
				$this->db->where('brand',$type);
				$this->db->where("(category like '%$name%' OR product_name like '%$name%')", NULL, FALSE);
				//$this->db->like('product_name',$name);
				$this->db->order_by("sale_price", "desc");
				if(!$this->count_all){
					$this->db->limit($this->per_page,$this->start);
				}
				$query = $this->db->get();
				return $query->result_array();
			}
		}
	}
	
	//show sale product search data
	public function search_cate_sale($name){
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		$this->db->where('sale_amoung >=', 3);
		$this->db->where("(category like '%$name%' OR product_name like '%$name%')", NULL, FALSE);
		$this->db->order_by("product_id", "DESC");
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//show popular product search data
	public function search_cate_pop($name){
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		$this->db->where('view >=', 3);
		$this->db->where("(category like '%$name%' OR product_name like '%$name%')", NULL, FALSE);
		$this->db->order_by("product_id", "DESC");
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//show new product search data
	public function search_cate_new($name){
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		$this->db->like('product_name', $name);
		$this->db->or_like('category', $name);
		$this->db->order_by("created_date", "DESC");
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	//show price product search data
	public function search_cate_price($name){
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		$this->db->like('product_name', $name);
		$this->db->or_like('category', $name);
		$this->db->order_by("sale_price", "asc");
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//######################## Search Shop ############################//
	public function shop_search_cate($name,$id){
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		$this->db->where('user_id', $id);
		$this->db->where("(category like '%$name%' OR product_name like '%$name%')", NULL, FALSE);
		$this->db->order_by("product_id", "DESC");
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	//######################## Shop Template ############################//
	public function shop_template($id){
		$this->db->select('*');
		$this->db->from('users_detail');
		$this->db->join('sys_template', 'users_detail.shop_id = sys_template.shop_id');
		$this->db->where('users_detail.user_id', $id);
		//$this->db->from('sites');
		//$this->db->join('pages', 'sites.sites_id = pages.sites_id');
		//$this->db->where('sites.users_id', $id);
		$query = $this->db->get();
		return $query->first_row('array');
	}
	
	//#################### Select Category ###########################//
	public function get_categories($id){
		$this->db->select('*');
		$this->db->from('sys_category');
		$this->db->where('parent_id',$id);
		$query = $this->db->get();
		$get_category = $query->result_array();
		return $get_category;
	}
	
	//#################### Get Size ###########################//
	public function get_size($id){
		$this->db->select('attribute_size');
		$this->db->from('sys_search_pro');
		$this->db->where('category_id',$id);
		$query = $this->db->get();
		$get_category = $query->result_array();
		return $get_category;
	}
	
	public function search_categories($search){
		$this->db->select('*');
		$this->db->from('sys_category');
		$this->db->like('category', $search);
		$query = $this->db->get();
		$get_category = $query->result_array();
		return $get_category;
	}
	
	public function save_slide($SQLdata){
		$this->db->insert('shop_management',$SQLdata);
		return $this->db->insert_id();
	}
	
	public function update_slide($id,$SQLdata){
		$this->db->where('user_id',$id);
		$this->db->update('shop_management',$SQLdata);
		return $this->db->affected_rows();
	}
	
	public function update_user_detail($id,$SQLdata){
		$this->db->where('user_id',$id);
		$this->db->update('users_detail',$SQLdata);
		return $this->db->affected_rows();
	}
	
	public function get_user_detail($id){
		$this->db->select('*');
		$this->db->from('users_detail');
		$this->db->where('user_id',$id);
		$query = $this->db->get();
		$get_category = $query->result_array();
		return $get_category;
	}
	
	public function get_shop_detail($id){
		$this->db->select('*');
		$this->db->from('shop_management');
		$this->db->where('user_id',$id);
		$query = $this->db->get();
		$get_category = $query->first_row('array');
		return $get_category;
	}
	
	//##################### Filer Price #######################//
	public function search_filer_price($id,$f,$t){
		$parent_id = get_parent_id($id);
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		if($parent_id == $id){
			$this->db->where(array('regular_prices >='=>$f,'regular_prices <='=>$t,'parent_id'=>$id));
		}else{
			$this->db->where(array('regular_prices >='=>$f,'regular_prices <='=>$t,'category_id'=>$id));
		}
		
		//$this->db->or_where('parent_id',$id);
		$this->db->order_by('regular_prices', 'desc');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function search_price($name,$f,$t){
		$this->db->select('*');
		$this->db->from('sys_search_pro');
		$this->db->where(array('regular_prices >='=>$f,'regular_prices <='=>$t)); 
		$this->db->where("(category like '%$name%' OR product_name like '%$name%')", NULL, FALSE);
		$this->db->order_by('regular_prices','desc');
		if(!$this->count_all){
			$this->db->limit($this->per_page,$this->start);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function comment_already($user_id, $pro_id){
		$this->db->select('*');
		$this->db->from('sys_comment');
		$this->db->where(array('user_id' => $user_id,'product_id' => $pro_id)); 
		$query = $this->db->get();
		return $query->first_row('array');
	}
	
	public function image($SQLdata){
		$this->db->insert($this->_image_table,$SQLdata);
		return $this->db->insert_id();
	}
	
	public function image_update($SQLdata){
		//$this->db->where('user_id',$id);
		$this->db->update($this->_image_table,$SQLdata);
		return $this->db->affected_rows();
	}
	
	public function get_image(){
		$this->db->select('*');
		$this->db->from($this->_image_table);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_shop_category($id){
		$this->db->select('*')->from('sys_category')->where_in('user_id',$id);
		$res = $this->db->get();
		$category = $res->result_array();
		return $category;
	}

	public function get_new_product($name){
		$this->db->select();
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
		$this->db->join('sys_category', 'sys_category.category_id = sys_relationship.category_id');
		
		if($this->session->userdata('sys_lang') == "khmer"){
			$this->db->where(array('sys_inventory.hot_pro'=>1,'sys_category.category_kh'=>$name));
		}elseif($this->session->userdata('sys_lang') == "chinese"){
			$this->db->where(array('sys_inventory.hot_pro'=>1,'sys_category.category_ch'=>$name));
		}elseif($this->session->userdata('sys_lang') == "vietnamese"){
			$this->db->where(array('sys_inventory.hot_pro'=>1,'sys_category.category_vn'=>$name));
		}else{
			$this->db->where(array('sys_inventory.hot_pro'=>1,'sys_category.category'=>$name));
		}
		
		//$this->db->limit(6);
		$this->db->order_by('sys_product.created_date','DESC');
		$result = $this->db->get();
		return $result->result_array();
	}
	
	public function get_top_product($name){
		$this->db->select();
		$this->db->from('sys_product');
		$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
		$this->db->join('sys_relationship', 'sys_product.product_id = sys_relationship.post_id');
		$this->db->join('sys_category', 'sys_category.category_id = sys_relationship.category_id');
		
		if($this->session->userdata('sys_lang') == "khmer"){
			$this->db->where(array('sys_inventory.sale_amoung >='=>2,'sys_category.category_kh'=>$name));
		}elseif($this->session->userdata('sys_lang') == "chinese"){
			$this->db->where(array('sys_inventory.sale_amoung >='=>2,'sys_category.category_ch'=>$name));
		}elseif($this->session->userdata('sys_lang') == "vietnamese"){
			$this->db->where(array('sys_inventory.sale_amoung >='=>2,'sys_category.category_vn'=>$name));
		}else{
			$this->db->where(array('sys_inventory.sale_amoung >='=>2,'sys_category.category'=>$name));
		}

		//$this->db->limit(6);
		$this->db->order_by('sys_product.created_date','DESC');
		$result = $this->db->get();
		return $result->result_array();
	}
    
	public function add_new($SQLdata){
		$this->db->insert('sys_template',$SQLdata);
		return $this->db->insert_id();
	}
	
	public function independent($sqlData){
		$this->db->insert('sys_bid_track', $sqlData);
		return $this->db->insert_id();
	}
	
	public function comment($id){
		$this->db->select();
		$this->db->from('sys_comment');
		$this->db->where(array('product_id'=>$id, 'comment_parent'=>0));
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function childComment($id){
		$this->db->select();
		$this->db->from('sys_comment');
		$this->db->where(array('product_id'=>$id,'comment_parent >'=>0));
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function getBidHistory($id){
		$this->db->select();
		$this->db->from('sys_bid_track');
		$this->db->where(array('product_id'=> $id));
		$result = $this->db->get();
		return $result->result_array();
	}
	
	public function get_brand(){
		$query = $this->db->query("SELECT * FROM sys_brand");
		return $query->result_array();
	}
	
	public function get_brandid($id){
		$this->db->select('brand');
		$this->db->from('sys_product');
		$this->db->where(array('product_id' =>$id));
		$query = $this->db->get();
		return $query->first_row('array');
	}
	
	public function getVendors(){
		$this->db->select();
		$this->db->from('sys_users');
		$this->db->join('sys_users_detail','sys_users_detail.user_id = sys_users.user_id');
		$this->db->where('group_id',2);
		$this->db->where('company_name !=',null);
		$qry = $this->db->get();
		return $qry->result_array();
	}
    
	public function get_supplier(){
		$supplier_id=user_group_id('Supplier');
		$this->db->select();
		$this->db->from('sys_users');
		$this->db->where('group_id',$supplier_id);
		$qry = $this->db->get();
		return $qry->result_array();
	}
	
	public function getVendorCats($param){
		$this->db->select('category_id');
		$this->db->from('sys_categories_vendor');
		$this->db->where('user_id',$param);
		$qry = $this->db->get();
		return $qry->result_array();
	}
	
	public function getVendorsId($param){
		$this->db->select('vendor_id');
		$this->db->from('sys_product');
		$this->db->where(array('product_id' =>$param));
		$query = $this->db->get();
		return $query->first_row('array');
	}
	
	public function get_contactType($id){
		$this->db->select('contact-type');
		$this->db->from('sys_product');
		$this->db->where(array('product_id' => $id));
		$query = $this->db->get();
		return $query->first_row();
	}
	
	public function insert_category($data1){
		$this->db->insert('sys_categories_vendor',$data1);
		return $this->db->insert_id();
	}
	
	public function get_parentid($id){
		$this->db->select('*');
		$this->db->from('tbl_categories');
		$this->db->where(array('category_id' =>$id));
		$query = $this->db->get();
		return $query->result_array();

	}
	
	public function find_catego($id,$uid){
		$this->db->select('*');
		$this->db->from('sys_categories_vendor');
		$this->db->where(array('category_id' =>$id,'user_id' =>$uid));
		$query = $this->db->get();
		return $query->first_row();
	}
	
	public function find_category($id){
		$this->db->select('parent_id');
		$this->db->from('tbl_categories');
		$this->db->where(array('category_id' =>$id));
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function beforeReInsert($param){
		$this->db->where('user_id', $param);
		$this->db->delete('sys_categories_vendor');
		
	}
	
	public function getRecentCat($userId){
		$this->db->select('*');
		$this->db->from('sys_categories_vendor');		
		$this->db->where(array('sys_categories_vendor.user_id' =>$userId,
		));
	
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_cat($id){
		$this->db->select('*');
		$this->db->from('tbl_categories');
		$this->db->join('sys_categories_vendor','sys_categories_vendor.category_id = tbl_categories.category_id');
		$this->db->where(array('sys_categories_vendor.user_id' =>$id,
						//'parent_id'=>0
		));
		$this->db->order_by('category_name', 'asc');
		$query = $this->db->get();
		//return $query->result_array();
		$results = $query->result_array();
		$getRecentCat = $this->getRecentCat($id);
		
		$output = '';
		$rememberCat = $this->session->userdata('categories');
		$old ='';
		$i=0;
		$hidden = '';
		//var_dump($getRecentCat); exit;
		foreach($results as $row){
			$getParId = getParId($row['category_id']);
			$parent = getParentCat($id, $row['category_id']);
			//if($i>1) continue;
			//var_dump($getParId);
			//print_r($getParId['category_id']);
			//var_dump($row['category_id']);
			$output .='<div>';
			//if(empty($parent['child'])){
				
				//$output .='<input id="parent-'.$row['category_id'].'" type="checkbox" class="categories" ';
					//echo $getParId;
					
						//var_dump($child);
						//echo $child['category_id']. ' '. $row['category_id']. '<br>';
						//
							$output .='<input id="parent-'.$row['category_id'].'" type="checkbox" class="categories';
							//foreach($parent['child'] as $cat){
								//if($parent['child']!= null){
									//echo $cat['category_id']. ' '. $row['category_id'] . '<br>';
								//if ($cat['category_id'] != $row['category_id'] ){
									//$output .= '';
								//		$hidden = '';
									//}
								//}else{
									//break;
									//$hidden = ' cat-hidden';
								//}
								
								
								
						//var_dump($i);
							//}
					
						//$output .= $hidden;
						//$hidden = '';
					
					
					$output .= '"';
					if(!empty($rememberCat)){
						foreach ($rememberCat as $category_id2) {
							if($category_id2 == $row['category_id']){
							//if($category_id2 == $rememberCat[$i]){
								//$output .=" checked ";
							}
							
						}
					}
					$output .= 'name="recents[]" value="'.$row['category_id'].'">';
					
					$output .= '&nbsp'.$row['category_name'];
				
					/*foreach($parent as $par){
						$output .= '<span style="padding-left:25px;">'.$par['category_id'].'<span>';
					}*/
					//}
					//$output .= $parent['output'];
					
					//$output .=$parent;
				
				$output .='</div>';
				
				$i++;
			
				//$old = $row['category_id'];
				
		}
		return $output;
	}
	
	public function check_permalink($from,$condition){
		$this->db->select('permalink');
		$this->db->from($from);
		$this->db->where('permalink',$condition);
		$query=$this->db->get();
		$data= $query->first_row('array');
		return $data['permalink'];
	}
	
	public function check_duplicate($primary,$field_name,$from,$condition){
		$this->db->select($primary,$field_name);
		$this->db->from($from);
		$this->db->where($field_name,$condition);
                $this->db->where(array('post_status'=>0));
		$query=$this->db->get();
		return $query->first_row('array');
	}
	
	public function check_duplicate_update($from,$product_code,$product_id){
		$this->db->select('product_id,product_code');
		$this->db->from($from);
		$this->db->where('product_code',$product_code);
		$this->db->where('product_id !=',$product_id);
                $this->db->where(array('post_status'=>0));
		$query=$this->db->get();
		return $query->result_array();	
	}
	
	public function find_duplicate_update($from,$condition){
		$this->db->select('product_id,product_code');
		$this->db->from($from);
		$this->db->where('product_code',$condition);
		$query=$this->db->get();
		return $query->first_row('array');	
	}
	
	public function edit_visible($id,$data){
		$this->db->where('product_id',$id);
		$this->db->update('sys_product',$data);
		return $this->db->affected_rows();
	}
	
	public function update_poststatus_product($pid,$data){
		$this->db->where('product_id',$pid);
		$this->db->update('sys_product',$data);
		return $this->db->affected_rows();
	}
    
	public function varians($id){
		$this->db->select();
		$this->db->from('sys_attributes_pro');
		$this->db->where(array('product_id'=>$id));
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function varians_size($id){
		$this->db->select('size');
		$this->db->from('sys_attributes_pro');
		$this->db->where(array('product_id'=>$id));
		$query = $this->db->get();
	//	return $query->result_array();
		$allsizes=$query->result_array();
		
		$tags = array(); // define empty array
		foreach($allsizes as $tag_result){
			$tags = array_merge($tags,explode(",",$tag_result['size']));
		}
		$tags = array_unique($tags); // remove duplicates
		$get_tag=array();
		foreach($tags as $value){
			$get_tag []=$value;
		}
		return implode(',', $get_tag);
	}
	
	public function check_varians($id){
		$this->db->select();
		$this->db->from('sys_attributes_pro');
		$this->db->where(array('product_id'=>$id));
		$query = $this->db->get();
		return $query->first_row('array');
	}
	
	public function get_attrs($id){
		$this->db->select();
		$this->db->from('sys_attributes_pro');
		$this->db->where(array('product_id'=>$id));
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_varians($id){
		$this->db->select();
		$this->db->from('sys_variable');
		$this->db->where(array('product_id'=>$id));
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_tags() {
		$this->db->select();
		$this->db->from('tbl_tags'); 
		$query = $this->db->get();
		return $query->result_array();
	}
	
	
	

}
?>