<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Mo_detail extends CI_Model{
		private $_table_name = 'sys_product';
		private $_view_table = 'sys_views';
		private $_view_table_name = 'sys_viewproduct';
		private $_inven_table_name = 'sys_inventory';
		private $_comment_table = 'sys_comment';
		private $_primary_key = 'product_id';
		private $_views_key = 'view_id';
		//########################## For Detail Product ###########################//
		public function product_detail($id,$user_id){
		//	$user_id = has_logged();
			$this->db->select('*');
			$this->db->from('sys_product');
			$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$this->db->join('sys_gallery', 'sys_gallery.post_id = sys_product.product_id');
			$this->db->where('sys_product.product_id', $id); 
			$query = $this->db->get();
			return $query->first_row('array');
		}
		
		public function cate_id($id){
			$this->db->select('*');
			$this->db->from('sys_relationship');
			$this->db->where('post_id', $id); 
			$query = $this->db->get();
			$get_query=$query->first_row('array');
			$user_rule=$get_query['category_id'];
			return $user_rule;
		}
		
		public function get_all(){
			$query = $this->db->get('sys_product');
			return $query->result_array();
		}
		
		public function view_pro($id,$SQLdata){
			$this->db->where($this->_primary_key,$id);
			$this->db->update($this->_table_name,$SQLdata);
			return $this->db->affected_rows();
		}
		
		public function insert_views($SQLdata){
			$this->db->insert($this->_view_table,$SQLdata);
			return $this->db->insert_id();
		}
		
		public function update_views($id,$SQLdata){
			$this->db->where('view_pro_id',$id);
			$this->db->update($this->_view_table,$SQLdata);
			return $this->db->affected_rows();
		}
		
		public function get_all_view($userid){
			$this->db->select('*');
			$this->db->from('sys_views');
			$this->db->join('sys_product', 'sys_views.view_pro_id = sys_product.product_id');
			$this->db->join('sys_inventory', 'sys_inventory.product_id = sys_views.view_pro_id');
			$this->db->where(array('sys_views.user_id' => $userid, 'sys_views.view_amoung >=' => 2));
			$this->db->order_by("sys_views.view_date", "DESC");
			$this->db->limit(6);
			$query = $this->db->get();
			return $query->result_array();
		}
		
		public function sale_pro($id,$SQLdata){
			$this->db->where('product_id',$id);
			$this->db->update($this->_inven_table_name,$SQLdata);
			return $this->db->affected_rows();
		}
		
		public function top_sale_pro(){
			$this->db->select('*');
			$this->db->from('sys_product');
			$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$this->db->where('view >=', 5); 
			$query = $this->db->get();
			return $query->result_array();
		}
		
		public function top_sale(){
			$this->db->select('*');
			$this->db->from('sys_product');
			$this->db->join('sys_inventory', 'sys_product.product_id = sys_inventory.product_id');
			$this->db->where('sale_amoung >=', 3); 
			$query = $this->db->get();
			return $query->result_array();
		}
		
		public function shop_name($id){
			$this->db->select('*');
			$this->db->from('users_detail');
			$this->db->where('user_id',$id);
			$query = $this->db->get();
			$get_query=$query->first_row('array');
			return $get_query;
		}
		
		public function deck_click_dup($field1="",$user_id=""){
			$this->db->from($this->_view_table);
			$this->db->where(array('view_pro_id'=>$field1,'user_id'=>$user_id));
			$query = $this->db->get();
			return $query->first_row('array');
		}
		
		public function get_all_hot($userid){
			$this->db->select('*');
			$this->db->from('sys_views');
			$this->db->join('sys_product', 'sys_views.view_pro_id = sys_product.product_id');
			$this->db->join('sys_inventory', 'sys_inventory.product_id = sys_views.view_pro_id');
			$this->db->where(array('sys_views.user_id' => $userid, 'sys_inventory.hot_pro >=' => 1));
			$this->db->order_by("sys_product.created_date", "DESC");
			$this->db->limit(6);
			$query = $this->db->get();
			return $query->result_array();
		}
		
		public function discount($id){
			$now = date('Y-m-d h:i:s');
			//$query = $this->db->query("SELECT * FROM sys_inventory where from_date <= '".$now."' and product_id='".$id."' and todate >='".$now."';");
			//return $query->result();
			$this->db->select('*');
			$this->db->from('sys_inventory');
			$this->db->where(array('from_date <='=>$now, 'todate >=' => $now, 'product_id' => $id));
			$query = $this->db->get();
			return $query->first_row('array');
		}
		
		public function get_comment_good($id){
			$this->db->select('*');
			$this->db->from('sys_comment');
			$this->db->where(array('product_id'=>$id, 'comment'=>1));
			$query = $this->db->get();
			$get_query=$query->result_array();
			return $get_query;
		}
		
		public function get_comment_average($id){
			$this->db->select('*');
			$this->db->from('sys_comment');
			$this->db->where(array('product_id'=>$id, 'comment'=>2));
			$query = $this->db->get();
			$get_query=$query->result_array();
			return $get_query;
		}
		
		public function get_comment_poor($id){
			$this->db->select('*');
			$this->db->from('sys_comment');
			$this->db->where(array('product_id'=>$id, 'comment'=>3));
			$query = $this->db->get();
			$get_query=$query->result_array();
			return $get_query;
		}
		
		public function get_comment_pro($id){
			$this->db->select('*');
			$this->db->from('sys_comment');
			$this->db->where('product_id',$id);
			$query = $this->db->get();
			$get_query=$query->result_array();
			return $get_query;
		}
		
		public function get_shop_rate($id){
			$this->db->select('*');
			$this->db->from('sys_comment');
			$this->db->join('sys_product', 'sys_comment.product_id = sys_product.product_id');
			$this->db->where('sys_product.user_id', $id);
			$query = $this->db->get();
			$get_query=$query->result_array();
			return $get_query;
		}
		
		public function test_comment($id,$user_id){
			$this->db->select('*');
			$this->db->from('sys_comment');
			$query = $this->db->get();
			$get_query=$query->result_array();
			foreach($get_query as $comment){
				$pro_id = $comment['product_id'];
				$userid = $comment['user_id'];
				if($pro_id == $id && $userid == $user_id){
					$result = 1;
				}else{
					$result = 0;
				}
			}
			return $result;
		}
		
		public function get_permission(){
			
		}
		
		//link to shop vender website
		/*public function shop_vender($user_id){
			$this->db->select('*');
			$this->db->from('sites');
			$this->db->where('user_id',$user_id);
			$this->db->where('sites_trashed',0);
			$query = $this->db->get();
			return $query->first_row('array');
		}*/
	}
?>