<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Categories_model extends CI_Model{
		private $_table_name = 'kp_product_view';
		private $_primary_key = 'product_id';
		public $per_page = 12;
		public $start = 0;
		public $count_all = false;
		public $order_by = 'DESC';
		public $field_order = 'product_id';

		public function allCategories($limit=0,$offset=0){
			$this->db->select("parent_id,category_id, category_name")
					->from('tbl_categories')
					->where('parent_id = 0')
					->order_by('category_name','asc')
					->limit($limit,$offset);
			$query=$this->db->get();
			$allquery=$query->result_array();
			return $allquery;
		}
		function category($paramet){
			$this->db->select('*');
			$this->db->from('kp_product_view p');
			$this->db->join('sys_relationship r','p.product_id=r.post_id');
			$this->db->join('sys_users u','u.user_id = p.user_id');
			$this->db->join('sys_users_detail ud', 'ud.user_id =  u.user_id');
			$this->db->where('r.category_id',$paramet);
			$this->db->where('p.status',1);
			$this->db->where('ud.page_status', 1);
			$this->db->order_by('product_id','DESC');
                        if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query=$this->db->get();
			return $query->result_array();
		}
		function getCategoryTitle($param){
			$this->db->select('category_name');
			$this->db->from('tbl_categories');
			//$this->db->join('sys_relationship', 'tbl_categories.category_id = sys_relationship.category_id');
			$this->db->where(array('category_id'=>$param));
			$qry = $this->db->get();
			return $qry->first_row('array');
		}
		function getLatestPro(){
			$this->db->select();
			$this->db->from('kp_product_view');
			$this->db->limit(4,0);
			$this->db->where(array('status'=>1));
			$this->db->order_by('product_id','DESC');
			$qry = $this->db->get();
			return $qry->result_array();
			
		}

		/* Vendor Category*/
		function category_vendor($paramet){
			$this->db->select('*');
			$this->db->from('kp_product_view p');
			$this->db->join('sys_relationship r','p.product_id=r.post_id');
			$this->db->where('r.category_id',$paramet);
			$query=$this->db->get();
			return $query->result_array();
		}
        public function page($base_url,$data){
			$count_all = count($data);
			return pages($base_url,$count_all,$this->per_page);
		}
		public function showing($data){
			$count_all = count($data);		
			return showing($count_all,$data,$this->per_page);
		}
	}
?>