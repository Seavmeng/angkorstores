<?php
defined('BASEPATH') OR exit ('No direct access allowed');

class Client_chat_model extends CI_Model
{

	function add_message($message, $nickname, $conversation_id, $send_by)
	{
		$data = array(
			'message' => (string) $message,
			'conversation_id' =>  $conversation_id,
			'send_by' =>  $send_by,
			'timestamp' => time()
		);

		$this->db->insert('sys_chats', $data);
	}

	function get_messages($conversation_id, $timestamp)
	{
		$this->db->where('conversation_id', $conversation_id)
		->where('timestamp >', $timestamp);
		$this->db->order_by('timestamp', 'DESC');
		$this->db->limit(10);
		$query = $this->db->get('sys_chats');
		return array_reverse($query->result_array());
	}

	public function get_existing_conversations($user_1, $user_2)
	{
		$where = " (user_1='" . $user_1 . "' AND user_2='" . $user_2 . "') OR (user_1='" . $user_2 . "' AND user_2='" . $user_1 . "') ";
		$query = $this->db->select('conversation_id')
		->where($where)
		->from('sys_conversations')
		->get();

		if ($query->num_rows() > 0) {
		            return $query->row();
    }
    return FALSE;
	}

	public function create_conversation($user_1,$user_2)
	{
		$data = array('user_1' => $user_1,
								'user_2' => $user_2,
								'timestamp' => time()
							);
		$save=$this->db->insert('sys_conversations',$data);
   	$insert_id = $this->db->insert_id();
		return $insert_id;
	}

}
