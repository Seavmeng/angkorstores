<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Home_model extends CI_Model
	{
		
		private $_table_name = 'tbl_posts';
		private $_primary_key = 'post_id';
		public $perpage = 8;
		public $per_page = 12;
		public $per_page_blog = 45;
		public $start = 0;
		public $count_all = false;
		public $order_by = 'DESC';
		public $field_order = 'post_id';
		public $post_type = 'page';
	
		public function recently_added(){
			
			$recent_option = $this->Site->getSettingBy("recent-products");
			$order_by = ($recent_option->value);
			$per_page = get_option_value('perpage');
			$this->db->select('*');
			$this->db->from('kp_product_view p');
			$this->db->join('sys_users u','u.user_id = p.user_id');
			$this->db->where(array('u.group_id'=>1,'p.status'=>1));
			$this->db->order_by('p.product_id',$order_by);
			$this->db->limit(18,$this->start);
			$query=$this->db->get();
			return $query->result_array();
		}
		public function featured(){
			
			$per_page = get_option_value('perpage');
			$this->db->select('*');
			$this->db->from('kp_product_view p');
			$this->db->join('sys_relationship r', 'p.product_id = r.post_id');
			$this->db->join('sys_users u','u.user_id = p.user_id');
			$this->db->where('u.group_id',1);
			$this->db->where('status',1); 
			$this->db->where_in('r.category_id', array('54','129','130','131','172','164'));
			$this->db->order_by('p.product_id','desc');
			$this->db->limit(12,$this->start);
			$query=$this->db->get();
			return $query->result_array();
		}

		public function featured_homepage_vendor_id($user_id){
			$this->db->select('*');
			$this->db->from('kp_product_view');
			$this->db->where(array('user_id'=>$user_id, 'status'=>1, 'post_status'=>0));
			$this->db->order_by('product_id',$this->order_by);
			if(!$this->count_all){
			   $this->db->limit($this->per_page,$this->start);
			}
			$query=$this->db->get();
			return $query->result_array();
		}
		public function discount(){
			$today = date("Y-m-d H:i:s");
			$this->db->select('*');
			$this->db->from('kp_product_view');
			$this->db->where(array('status'=>1,'have_discount'=>1,'from_date <='=>$today,'todate >='=>$today));
			$this->db->order_by('rand()');
			$this->db->limit(6,0);
			$query=$this->db->get();
			return $query->result_array();
		}
		public function hotProduct(){
			$per_page = get_option_value('perpage');
			$this->db->select('*');
			$this->db->from('kp_product_view');
			$this->db->where('hot_pro',1);
			$this->db->order_by('rand()');
			$this->db->limit(3,0);
			$query=$this->db->get();
			return $query->result_array();
		}
		public function random1(){
			$per_page = get_option_value('perpage');
			$this->db->select('*');
			$this->db->from('kp_product_view');
			//$this->db->where('hot_pro',1);
			$this->db->order_by('rand()');
			$this->db->limit(1,0);
			$query=$this->db->get();
			return $query->result_array();
		}
		public function random2(){
			$per_page = get_option_value('perpage');
			$this->db->select('*');
			$this->db->from('kp_product_view');
			//$this->db->where('hot_pro',1);
			$this->db->order_by('rand()');
			$this->db->limit(1,0);
			$query=$this->db->get();
			return $query->result_array();
		}
		public function random3(){
			$per_page = get_option_value('perpage');
			$this->db->select('*');
			$this->db->from('kp_product_view');
			//$this->db->where('hot_pro',1);
			$this->db->order_by('rand()');
			$this->db->limit(3,0);
			$query=$this->db->get();
			return $query->result_array();
		}
		public function khmerProducts(){
			$this->db->select();
			$this->db->from('kp_product_view p');
			$this->db->join('sys_relationship r', 'p.product_id = r.post_id');
			$this->db->where('r.category_id',181);
			$this->db->limit(3,0);
			$this->db->order_by('rand()');
			$qry = $this->db->get();
			return $qry->result_array();
		}
		
		/*public function best_sellers(){
			$per_page = get_option_value('perpage');
			$this->db->select('*');
			$this->db->from('kp_product_view p');
			//$this->db->join('sys_inventory i','p.product_id = i.product_id');
			//$this->db->where('i.sale_amoung >=', 1);
			//$this->db->order_by('p.product_id',$this->order_by);
			$this->db->join('sys_relationship r', 'p.product_id = r.post_id');
			$this->db->join('sys_users u','u.user_id = p.user_id');
			
			$this->db->where('r.category_id',172);
			$this->db->or_where('r.category_id',128);
			$this->db->or_where('r.category_id',119);
			$this->db->order_by('rand()');
			$this->db->limit(6,0);
			$query=$this->db->get();
			return $query->result_array();
		}*/
		public function best_sellers(){
			$per_page = get_option_value('perpage');
			$this->db->select('*');
			$this->db->from('kp_product_view p');
			//$this->db->join('sys_inventory i','p.product_id = i.product_id');
			//$this->db->where('i.sale_amoung >=', 1);
			//$this->db->order_by('p.product_id',$this->order_by);
			//$this->db->join('sys_relationship r', 'p.product_id = r.post_id');
			$this->db->join('sys_users u','u.user_id = p.user_id');
			$this->db->join('sys_users_detail ud','ud.user_id = u.user_id');
			
			
			/*$this->db->where('r.category_id',172);
			$this->db->or_where('r.category_id',128);
			$this->db->or_where('r.category_id',119);*/
			$this->db->where(array('u.group_id'=>2,'p.status'=>1, 'ud.page_status'=>1));
			$this->db->order_by('rand()');
			$this->db->limit(6,0);
			$query=$this->db->get();
			return $query->result_array();
		}
		public function home_slide(){
			$this->db->select('*');
			$this->db->from('tbl_slide');
			$this->db->where('group_id',1);
			$query=$this->db->get();
			return $query->result();
		}
		/* Vendor home page slideshow
		   added 2-3-2017
		*/
		public function home_vendorPage_Slide(){
			$page = $this->uri->segment(1);
			$this->db->select('*');
			$this->db->from('tbl_slide');
			$this->db->join('sys_users','sys_users.user_id = tbl_slide.user_id');
			$this->db->join('sys_users_detail','sys_users_detail.user_id = sys_users.user_id');
			$this->db->where('page_name',$page);
			$query = $this->db->get();
			return $query->result_array();
		}
		public function check_out($data)
		{
			$this->db->insert('sys_billing',$data);
		}
		public function subscribe($data)
		{
			$this->db->insert('sys_users',$data);
		}

		public function featured_categorypage_vendor_id($user_id,$cat_id){
			$this->db->select('*');
			$this->db->from('kp_product_view p');
			$this->db->join('sys_relationship r','p.product_id=r.post_id');
			$this->db->where('r.category_id',$cat_id);
			//$this->db->where('user_id',$user_id);
			$this->db->where(array('user_id'=>$user_id, 'status'=>1, 'post_status'=>0));
			$this->db->order_by('product_id',$this->order_by);
			if(!$this->count_all){
				$this->db->limit($this->per_page,$this->start);
			}
			$query=$this->db->get();
			return $query->result_array();
		}
		public function page($url,$data){
			$count_all = count($data);
			return pages($url,$count_all,$this->per_page);
		}
		public function page_blog($url,$data){
			$count_all = count($data);
			return pages($url,$count_all,$this->per_page_blog);
		}
		//------------categories about Camboid ----------
		public function blog_product($parent_cat){

			if($parent_cat != null){
				$sectermentAnd = array( 'parent_id' => $parent_cat) ;
			}else{

				$sectermentAnd = array('category_group_id' => category_type('about_cambodia') , 'parent_id' => 0 ) ;

			}
			
			$this->db->select(" *,`category_id as `cat` , ( SELECT COUNT(*) FROM `tbl_categories` WHERE `parent_id` = `cat` ) AS 'count_child' ");
			$this->db->from('tbl_categories');
			$this->db->where($sectermentAnd);
			if(!$this->count_all){
				$this->db->limit($this->per_page_blog,$this->start);
			}
			
			$query = $this->db->get();
			return $query->result_array();
		}
		public function get_banner() {
			$this->db->select('*');
			$this->db->from('tbl_banner'); 
			$this->db->where("tbl_banner.expiring_date > NOW()"); 
			$query=$this->db->get();
			return $query->result();
		}
	
		public function select_topic()
		{
			$result = $this->db->select('*')->from('tbl_topic')->get()->result();
			return $result;
		}
	
	}
?>