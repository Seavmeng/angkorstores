<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Mcart extends CI_Model{
		private $_table_name = 'tbl_posts';
		private $_primary_key = 'post_id';
		public $perpage = 8;
		public $start = 0;
		public $count_all = false;
		public $order_by = 'DESC';
		public $field_order = 'post_id';
		public $post_type = 'page';
		
		public function add_cart($data){
			$this->db->insert('sys_cart',$data);
			return $this->db->insert_id();
		}
		public function show_cart($user_id){ 
			$this->db->select('*');
			$this->db->from('sys_cart');
			$this->db->where('user_id',$user_id);
			$query=$this->db->get();
			return $query->result_array();
		}
		public function update_cart($user_id,$cart_id,$SQLdata){
			$this->db->where(array('user_id'=>$user_id,'cart_id'=>$cart_id));
			$this->db->update('sys_cart',$SQLdata);
			return $this->db->affected_rows();
		}
		public function delete_cart($id){
			$this->db->where(array('cart_id'=>$id,'user_id'=>get_user_id()));
			$this->db->delete('sys_cart');
			return $this->db->affected_rows();
		}
		public function update($id,$SQLdata){
			$this->db->where($this->_primary_key,$id);
			$this->db->update($this->_table_name,$SQLdata);
			return $this->db->affected_rows();
		}
		public function check_out($data){
			$this->db->insert('sys_billing',$data);
			return $this->db->insert_id();
		}
		public function save_wishlist($data){
			$this->db->insert('sys_wishlist',$data);
			return $this->db->insert_id();
		}
		public function check_add_wishlist($user_id,$pro_id){
			$this->db->select('*');
			$this->db->from('sys_wishlist');
			$this->db->where(array('user_id'=>$user_id,'product_id'=>$pro_id));
			$query=$this->db->get();
			if($query->num_rows() > 0){
				return 1;
			}
		}
		public function show_wishlist($user_id){
			$this->db->select('*');
			$this->db->from('sys_wishlist');
			$this->db->where('user_id',$user_id);
			$query=$this->db->get();
			return $query->result_array();
		}
		public function order($param){
			$this->db->insert('sys_orders', $param);
			return $this->db->insert_id();
		}
		
		public function shipping(){
			$this->db->select('*');
			$this->db->from('sys_location');
			$this->db->order_by('location_name','ASC');
			$query=$this->db->get();
			return $query->result_array();
		}
		public function shipping_cam(){
			$this->db->select('*');
			$this->db->from('sys_location');
	//		$this->db->where('loc_id',103);
			$this->db->order_by('location_name','ASC');
			$query=$this->db->get();
			return $query->result_array();
		}
		public function get_shippingname($ship_id,$l_id){
			$this->db->select('zone_id,parent_id');
			$this->db->from('sys_shopping_zone');
			$this->db->where(array('shipping_id'=>$ship_id,'country_id'=>$l_id));
			$query=$this->db->get();
			$zone=$query->first_row('array');
			$zone_id=$zone['parent_id'];
			if($zone_id){
				$this->db->select('method_id');
				$this->db->from('sys_shopping_method');
				$this->db->where('location_id',$zone_id);
				$query=$this->db->get();
				$method=$query->first_row('array');
				return $method['method_id'];
			}else{
				return '0';
			}
		}
		public function shipping_city(){
			$this->db->select('*');
			$this->db->from('sys_location');
			$this->db->where('parent_id',103);
			$this->db->order_by('location_name','ASC');
			$query=$this->db->get();
			return $query->result_array();
		}
		function ship_company(){
			$this->db->select('*');
			$this->db->from('sys_shopping_name');
			$query=$this->db->get();
			return $query->result_array();
		}
		function company_name($id){
			$this->db->select('shipping_name');
			$this->db->from('sys_shopping_name');
			$this->db->where('shipping_id',$id);
			$query=$this->db->get();
			$data=$query->first_row('array');
			return $data['shipping_name'];
		}
		function receied_order($id){
			$this->db->select('*');
			$this->db->from('sys_billing');
			$this->db->where('permalink',$id);
			$query=$this->db->get();
			return $query->first_row('array');
		}
		function receied_pro_order($id){
			$this->db->select('*');
			$this->db->from('sys_billing b');
			$this->db->join('sys_orders o', 'b.bill_id=o.bill_id');
			$this->db->where('permalink',$id);
			$query=$this->db->get();
			return $query->result_array();
		}
		function cal_shipping($weight,$city,$compsny_id){
			$this->db->select('*');
			$this->db->from('sys_shopping_method');
			$this->db->where(array('shipping_id'=>$compsny_id,'location_id'=>$city));
			$query=$this->db->get();
			$data=$query->first_row('array');
			if($data['method_id']){

				$this->db->select('*');
				$this->db->from('sys_shopping_price');
				$this->db->where('method_id',$data['method_id']);
				$query=$this->db->get();
				$data=$query->result_array();
				foreach($data as $row){
				//	echo $weight.$row['min_weight'].$row['max_weight'].$row['fee_amount'].'<br>';
					if($weight >= $row['min_weight'] && $weight <= $row['max_weight']){
						if($row['fee_amount']){
							return $row['fee_amount'];
						}
					}
				}
			}
		}
		function payment_method(){
			$this->db->select('*');
			$this->db->from('sys_payment');
			$this->db->where('status',1);
			$this->db->order_by('payment_id','desc');
			$query=$this->db->get();
			return $query->result_array();
		}
		function check_order($id){
			$this->db->select('*');
			$this->db->from('sys_billing');
			$this->db->where('permalink',$id);
			$query=$this->db->get();
			return $query->first_row('array');
		}
		function billing_info($user_id){
			$this->db->select('*');
			$this->db->from('kp_user_view');
			$this->db->where('user_id',$user_id);
			$query=$this->db->get();
			return $query->first_row('array');
		}

		/* khorng */
		public function search_order_id($order_id){
			$this->db->like('order_id',$order_id);
			$this->db->order_by('track_order_id','DESC');
			$query = $this->db->get('sys_track_order');
			return $query->result_array();
		}
		// khorng 5/9/2017 
		public function get_price($p_id){
			$this->db->select('sale_price');
			$this->db->from('sys_inventory');
			$this->db->where('product_id',$p_id);
			$query = $this->db->get();
			return $query->first_row();
		}
		
		
		public function total_original_price($bill_id){
			$result = $this->db->query("
								SELECT SUM(sys_orders.original_price)  as total
								FROM sys_orders 
								WHERE sys_orders.bill_id = $bill_id ")->row();
			return $result; 
		}
		 
		function get_point_reward() {
			$result = $this->db->get("sys_point_rewards")->row();
			return $result;
		}
		
	
	}
	?>