<?php 
class search extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('search_model');
		$this->load->helper('product');
		
	}
	
	public function _varaible(){

		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->search_model->start = $page;
	}
	
	public function index(){
		//var_dump($this->input->get('search')); exit;
		//$this->form_validation->set_rules('search', 'Search Field', 'required');
		$this->_varaible();
		if($this->input->get('search') == '' ){
			redirect(base_url());
			//echo validation_errors();
			//echo "hello";
		}else{
			$strSearch = trim($this->input->get('search'));
			$strCat = trim($this->input->get('product_cat'));
			$data['searchFor']=$strSearch;
			if($this->input->get('product_cat')== 'Product'){
				//var_dump($strSearch);var_dump($strCat); exit;
				

				//if($strCat == 'all'){
					$data['results'] = $this->search_model->index($strSearch);
					$base_url = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'?search='.$strSearch.'&product_cat=Product&post_type=product'.'&';
					$this->search_model->count_all = true;
					$data['page'] = $this->search_model->page($base_url,$this->search_model->index($strSearch)); 
					$data['showing'] = $this->search_model->showing($this->search_model->index($strSearch));
				/*}else{
					//echo $strCat; exit;
					$data['results'] = $this->search_model->searchByCat($strSearch, $strCat);
					$base_url = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'?search='.$strSearch.'&product_cat='.$strCat.'&post_type=product'.'&';
					$this->search_model->count_all = true;
					$data['page'] = $this->search_model->page($base_url,$this->search_model->searchByCat($strSearch, $strCat)); 
					$data['showing'] = $this->search_model->showing($this->search_model->searchByCat($strSearch, $strCat));
				}*/
				$this->load->view('home/search', $data);
			}else{
				$data['results'] = $this->search_model->searchSupplier($strSearch);
				$base_url = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'?search='.$strSearch.'&';
				$this->search_model->count_all = true;
				$data['page'] = $this->search_model->page($base_url,$this->search_model->searchSupplier($strSearch)); 
				$data['showing'] = $this->search_model->showing($this->search_model->searchSupplier($strSearch));
				//var_dump($data['results']); exit;
				$this->load->view('home/search_vendor_page', $data);
			}
			
			
		}	
	}
	public function auto_complete(){
		$request=$_GET['term'];
	//	$_POST['term']=$_GET['term'];
	//	$this->form_validation->set_rules('term', 'Keyword', 'xss_clean|trim');
		 
		if(isset($request)){
			$this->_data['type'] = $this->input->get('type', TRUE);
			$this->_data['services']= $this->search_model->index($request);
			/*$this->moproduct->json_search_user_($request);*/
			if(!empty($this->_data['services'])){  
				$this->_data['response'] = 'true'; //Set response  
				$this->_data['message'] = array(); //Create array  
				foreach( $this->_data['services'] as $row ){  
					//$new_row['product_id']=$row['user_id'];
					$new_row['product_name']=$row['product_name'];
					/*if($row['profile']){
						$new_row['picture']=base_url().'images/products/'.$row['profile'];
					}else{
						$new_row['picture']=base_url().'images/empty_logo.png';
					}*/
					$new_row['permalink']=$row['permalink'];
					$data[] = $new_row;
				}	
			}
		}
		 echo json_encode($data);
	}
	
	public function auto_complete_vendor(){
		$request=$_GET['name_startsWith'];
		//$_POST['term']=$_GET['term'];
		//$this->form_validation->set_rules('term', 'Keyword', 'xss_clean|trim');
		if(isset($request)){
			$this->_data['type'] = $this->input->get('type', TRUE);
			$this->_data['services']= $this->search_model->auto_complete_vendor($request);
			/*$this->moproduct->json_search_user_($request);*/
			if(!empty($this->_data['services'])){  
				$this->_data['response'] = 'true'; //Set response  
				$this->_data['message'] = array(); //Create array  
				foreach( $this->_data['services'] as $row ){  
					//$new_row['product_id']=$row['user_id'];
					$new_row['company_name']=$row['company_name'];
					/*if($row['profile']){
						$new_row['picture']=base_url().'images/products/'.$row['profile'];
					}else{
						$new_row['picture']=base_url().'images/empty_logo.png';
					}*/
					$new_row['page_name']=$row['page_name'];
					$data[] = $new_row;
				}
				echo json_encode($data);	
			}
		}
	}
}

?>