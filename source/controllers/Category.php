<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('categories_model');
		$this->load->model('admin/user/muser');
		$this->load->model('home_model');
		$this->load->helper('product_helper');
		$this->load->model('admin/product/moproduct');
                $this->_controller_url = $this->uri->segment(1).'/'.$this->uri->segment(2).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}

	private $_controller_url = '';
	private $_data = array();

    private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->categories_model->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('category');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );

		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}
	
	function index(){
		redirect(base_url());
	}
	
	function pro_category($paramet){
		if($paramet == ""){
			redirect(base_url());
		}
		$this->_variable();
		$data['cat_title'] = $this->categories_model->getCategoryTitle($paramet);
		$data['recommend'] = $this->moproduct->recommend_product();
		$data['latestPro'] = $this->categories_model->getLatestPro();
		
		$data['product']=$this->categories_model->category($paramet);
		$base_url = base_url().$this->_controller_url.'?';
		$this->categories_model->count_all = true;
		$data['page'] = $this->categories_model->page($base_url,$this->categories_model->category($paramet));
		$data['showing'] =$this->categories_model->showing($this->categories_model->category($paramet));
		$data['banner']=$this->home_model->get_banner();
		
		
		$category=$data['product'];
		if(empty($category)){
			$this->load->view('home/recommend', $data);
		}else{
			$this->load->view('home/product_category', $data);
		}
	}
	
	public function allcategory(){
		$data['categories']=$this->categories_model->allCategories(5,0);
		$data['categories_sub']=$this->categories_model->allCategories(5,5);
		$this->load->view('more_category',$data);
	}
	
	function vendor_categorypage($user_page=null){
		$this->_variable();
		$cat_id = $this->uri->segment(3);
		$this->data['user_detail']=$this->muser->getuser_detail_byname($user_page);
		$this->data['Featured'] = $this->home_model->featured_categorypage_vendor_id($this->data['user_detail']['user_id'],$cat_id);
		$url = base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->home_model->count_all = true;
		$this->data['page'] = $this->home_model->page($url,$this->home_model->featured_categorypage_vendor_id($this->data['user_detail']['user_id'],$cat_id));
		$this->load->view('home/vendor_pages/category', $this->data);
	}
	
	function vendor_profilepage(){
		$user_page= $this->uri->segment(1);
		$this->data['user_detail']=$this->muser->getuser_detail_byname($user_page);
		$this->load->view('home/vendor_pages/profile',$this->data);
	}
	
	function vendor_contactpage($user_page=null){
		$this->data['user_detail']=$this->muser->getuser_detail_byname($user_page);
		$this->load->view('home/vendor_pages/contact',$this->data);
	}
	
	function contact_mail(){
		$page_name = $this->uri->segment(1);
                $this->data['user_detail']=$this->muser->getuser_detail_byname($page_name);
		$page_mail = $this->muser->getuser_detail_byname($page_name);

		$from = $this->input->post('email');
		$to = $page_mail['email'];
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		$fname = $this->input->post('first-name');
		$lname = $this->input->post('last-name');

	 	$this->load->library('email'); 
     	$this->email->from($from, $fname.' '.$lname); 
     	$this->email->to($to);
     	$this->email->subject($subject); 
     	$this->email->message($message); 

     	if($this->email->send()){
			$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
										    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
										    <strong>Success!</strong> Your mail has been sent successfully!
										  </div>');
		 }else{
			$this->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissable text-center">
										    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
										    <strong>Warning!</strong> Can not send Mail ! '.$this->email->print_debugger().'</div>'
										 );
		 }
		$this->load->view('home/vendor_pages/contact',$this->data);
	}


}
?>