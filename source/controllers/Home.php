<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	private $_controller_url = '';
	private $_data = array();
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('admin/user/muser');
		$this->load->helper('password');
		$this->load->helper('product_helper'); 
		$this->load->helper('category_helper');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
		
	}
	
	private function _variable()
	{
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mpost->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('posts');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-thumb-tack';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$this->_data['check_ml_categories'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.categories'));
		$option = array(
						''=>'----',
						'public'=>word_r('visibility_public'),
						'private'=>word_r('visibility_private'),
						'delete'=>word_r('delete')
				  );
		$option_search = array(
						''=>'----',
						'public'=>word_r('visibility_public'),
						'private'=>word_r('visibility_private')
				  );
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
		$this->_data['option_search'] = form_dropdown('visibility',$option_search,$this->input->get('visibility'),'class="form-control" ');
	}
	
	public function index()
	{ 
		$data['data1'] = 'Data1';
		$data['recently_added'] = $this->home_model->recently_added();
		$data['Featured'] = $this->home_model->featured();
        $data['discounts'] = $this->home_model->discount();
		$data['hotProduct'] = $this->home_model->hotProduct();
		$data['random1'] = $this->home_model->random1();
		$data['random2'] = $this->home_model->random2();
		$data['random3'] = $this->home_model->random3();
		$data['khmerProducts'] = $this->home_model->khmerProducts();
		$data['best_sellers'] = $this->home_model->best_sellers();
		$data['slide']=$this->home_model->home_slide();
		$data['banner']=$this->home_model->get_banner();
		$this->load->view('index', $data);
	}
	
	public function subscribe()	
	{
		$this->form_validation->set_rules('email',word_r('email'),'required|trim|xss_clean|valid_email|is_unique[sys_users.email]');
		if($this->form_validation->run() == FALSE){
			echo validation_errors();
		}else{
			$subscriber=$this->home_model->subscribe();
			if($subscriber){
				echo 'yes';
			}else{
				echo 'no';
			}
		}
	}
    
	public function size_bycolor()
	{
		$id = $this->input->post('id');
		$pro_id = $this->input->post('pro_id');
		$this->db->select();
		$this->db->from('sys_attributes_pro');
		$this->db->where(array('product_id'=>$pro_id,'color'=>$id));
		$result = $this->db->get();
		$search_array = $result->result_array();
		$attr='
				<div class="detail_left"><strong>Size</strong>:</div>
				<div class="detail_right">
					<div class="varian">';
					foreach($search_array as $row){
						$varians_sizes=explode(',',$row['size']);
						foreach($varians_sizes as $row){
							$size=get_attr_name($row);
							if(!empty($row)){
								$attr.='<a href="javascript:void(0)" class="size" value="'.$row.'">'.$size['attr_name'].'</a>';
							}
						}
					}
		$attr.='	</div>
				</div>
				<div class="clear"></div>
			';
		echo $attr;
	}
	
	public function varian_price()
	{
		$today = date("Y-m-d H:i:s");
		$pro_id = $this->input->post('pro_id');
		$color = $this->input->post('color');
		$size = $this->input->post('size');
		$data=get_pro_field($pro_id);
		$state_date = $data['from_date'];
		$end_date = $data['todate'];
        $colors = $color =="" ? 0: $color;
		$this->db->select();
		$this->db->from('sys_variable');
		$this->db->where(array('product_id'=>$pro_id,'colors'=>$color,'sizes'=>$size));
		$result = $this->db->get();
		$varians = $result->first_row('array');
		
		if ($varians['price'] !=null){
			if ($data['have_discount'] && $today >= $state_date &&  $today <= $end_date &&
			$data['discount_amount'] > 0  && !empty($data['discount_amount']) ) {
				$dis = $varians['price'] - ($varians['price'] * $data['discount_amount'])/100 ;
				echo '
					<div class="detail_width">
						<div class="detail_left"><strong>Price</strong>:</div>
						<div class="detail_right price">
							<del><span class="amount">'.currency('sign'). number_format((float)$varians['price'], 2, '.', '') .'</span></del>					  
						</div>
						<div class="clear"></div>
					</div>';
				echo '
				<div class="detail_width">
					<div class="detail_left"><strong>Discount Price</strong>:</div>
					<div class="detail_right price">
						<ins><span class="amount">'.currency('sign'). number_format((float)$dis, 2, '.', '') .'</span></ins> <span class="discount_amount">-'.$data['discount_amount'].'%</span>
					</div>
					<div class="clear"></div>
				</div>';
			}else{
				echo '
				<div class="detail_width">
					<div class="detail_left"><strong>Price</strong>:</div>
					<div class="detail_right price">
						<ins><span class="amount">'.currency('sign'). number_format((float)$varians['price'], 2, '.', '') .'</span></ins>
					</div>
					<div class="clear"></div>
				</div>';
			}
		}else{
			echo 00;
		}
	}
	public function chat_realtime(){ 
		$this->load->view('includes/firebase_live_chat/index');
	}
	
}
