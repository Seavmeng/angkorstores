<?php 
class newsletter extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('newsletter_model');
		
		
	}
	public function index(){
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[sys_newsletter.email]', array('is_unique'=>'E-mail already 	subscribed!')
		);
		if($this->form_validation->run() == FALSE){
			echo validation_errors();	
		}
		else{
			$field = array('email' => $this->input->post('email', true));
			if($this->newsletter_model->insert($field)){
			//redirect(base_url());
			echo "You have successfully subscribed to our mailing list.";
			}else{
				echo "error";
			}
			
			
		}
		
	}
}
?>