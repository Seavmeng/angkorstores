<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('mcart');
		$this->load->helper('password');
	    $this->load->helper('product');
		$this->load->model('admin/user/muser');
		$this->load->model('admin/setting/msms_notification'); 
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
		
	}
	private $username = "online.momvarin";
    private $password = "85a731b9d40b528bdd684e7e9de958c2";
	
	private $final_total;
	private $total_with_coupon;
	private $amount_reward;
	private $amount_coupon;
	private $reward_point; 
	private $total_amount; 
		
	public function index(){
		if(has_logged()){
			$cart = $this->mcart->show_cart(get_user_id());
			$count=count($cart);
			if(!$count){
				redirect(base_url());
			}
		}else{
			$cart = $this->cart->contents();
			if(!$cart){
				redirect(base_url());
			}
		}
		$this->form_validation->set_rules('calc_shipping_country', 'Shipping Country','required|trim|xss_clean');
                $this->form_validation->set_rules('calc_shipping_state', 'Shipping State','required|trim|xss_clean');
                $this->form_validation->set_rules('con_name', 'Shipping Company Name','required|trim|xss_clean');
	
		if($this->input->post('calc_shipping')){
			if ($this->form_validation->run() === FALSE){
				echo validation_errors();
			}else{
				if ($cart = $this->cart->contents()):
							  
					$weight=0;
					foreach ($cart as $item):
						
						$row=get_pro_field($item['id']);
						$weight +=$row['weight'];
					endforeach; 
				
				$this->_data['country']=$this->input->post('calc_shipping_country');
				$this->_data['city']=$this->input->post('calc_shipping_state');
				
				$city=$this->input->post('calc_shipping_state');
				$compsny_id=$this->input->post('con_name');
				$this->_data['company_name']=$this->mcart->company_name($compsny_id);
				$this->_data['cal_shipping']=$this->mcart->cal_shipping($weight,$city,$compsny_id);
				endif;
			}
		}
		$this->_data['ship_company']=$this->mcart->ship_company();
		if(has_logged()){
			$this->_data['cart'] = $this->mcart->show_cart(get_user_id());
		}
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$this->load->view('home/cart',$this->_data);
	}
	
	public function received_errors(){
		$message['rmsg']=$this->session->flashdata('msg');
		$this->load->view('home/order_received_errors',$message);
	}
	
	function billing(){ 
		$this->session->set_userdata('billing',array(
			'first_name'=>$this->input->post('billing_first_name'),
			'last_name'=>$this->input->post('billing_last_name'),
			'company'=>$this->input->post('billing_company'),
			'email'=>$this->input->post('billing_email'),
			'phone'=>$this->input->post('billing_phone'), 
            'country'=>$this->input->post('billing_country'), 
			'address'=>$this->input->post('billing_address_1'),
			'city'=>$this->input->post('billing_city'),
			'postcode'=>$this->input->post('billing_postcode'),
			'payment_method'=>$this->input->post('payment_method')
		));
	}
	
	public function ship_calculate(){
		if($this->session->userdata('ship_cal')){
			$ship_data=$this->session->userdata('ship_cal');
			$ship_name=$ship_data['company_name'];
			//--------buy now----
			$buy_now=$this->session->userdata('buy_now');
			if(isset($buy_now)){
				$total_weight=0;
				$row=get_pro_field($buy_now['id']);
				$total_weight +=$row['weight'] * $buy_now['qty'];
			}else{
				if(has_logged()){
					$cart=show_cart(get_user_id());
					$total_weight=0;
					foreach ($cart as $item):
						$row=get_pro_field($item['product_id']);
						$total_weight +=$row['weight'] * $item['qty'];
					endforeach;
				}else{
					if ($cart = $this->cart->contents()):
						$total_weight=0;
						foreach ($cart as $item):
							$row=get_pro_field($item['id']);
							$total_weight +=$row['weight'] * $item['qty'];
						endforeach;
					endif;
				}
			}
			$results=total_ship($total_weight,$ship_data['method_id']);
			$fee_amount=0;
			foreach($results as $row){
				if($total_weight >= $row['min_weight'] && $total_weight <=$row['max_weight']){
					/*echo $ship_name.': <span class="amount">'.currency('sign').$row['fee_amount'].'</span>';
					$fee_amount =$row['fee_amount'];*/
					if($this->session->userdata('parcel_city')){
						if($this->session->userdata('parcel_city') == 386){
							$fee_amount = 0;
						}
						else{
							$fee_amount =$row['fee_amount'];
						}
					}
					else{
						$fee_amount =$row['fee_amount'];
					}
				}
			}													
			if($fee_amount){
				$total_ship=$fee_amount;
			}else{
				$total_ship=0;
			}
		}else{
			if(has_logged()){
				$ship_data=getUserShipMethod(get_user_id());
				$ship_to = getUserShipTo(get_user_id());
				$getShipM = getShipMethod($ship_data, $ship_to);
				if(isset($buy_now)){
					$total_weight=0;
					$row=get_pro_field($buy_now['id']);
					$total_weight +=$row['weight'] * $buy_now['qty'];
				}else{
					//if(has_logged()){
						$cart=show_cart(get_user_id());
						$total_weight=0;
						foreach ($cart as $item):
							$row=get_pro_field($item['product_id']);
							$total_weight +=$row['weight'] * $item['qty'];
						endforeach;
					/*}else{
						$total_weight=0;
						if ($cart = $this->cart->contents()):
							
							foreach ($cart as $item):
								$row=get_pro_field($item['id']);
								$total_weight +=$row['weight'] * $item['qty'];
							endforeach;
						endif;
					}*/
				}
				
					$results=total_ship($total_weight,$getShipM);
				
				
				
				//echo $total_weight;
				//var_dump($results); exit;
				//var_dump($total_weight);
				//echo getUserShipMethod(get_user_id()) . "HEllo!"; exit;
				//var_dump($total_weight); 
				//var_dump($ship_data);
				//var_dump($results); 
				foreach($results as $row){
					if($total_weight >= $row['min_weight'] && $total_weight <=$row['max_weight']){
						//var_dump($this->session->userdata('parcel_city')); exit;
						//if($this->session->userdata('parcel_city')){
							if($ship_to == 386){
								//echo getShipCompany($ship_data).': <span class="amount">'.currency('sign').'0</span>';
								$fee_amount = 0;
							}
							else{
								//echo getShipCompany($ship_data).': <span class="amount">'.currency('sign').$row['fee_amount'].'</span>';
								$fee_amount =$row['fee_amount'];
							}
						/*}
						else{
							echo getShipCompany($ship_data).': <span class="amount">'.currency('sign').$row['fee_amount'].'</span>';
							$fee_amount =$row['fee_amount'];
						}*/
						
						
					}
				}
				//var_dump($ship_data['shipping_city']); exit;
				if(isset($fee_amount)){
					$total_ship=$fee_amount;
				}else{
					$total_ship=0;
					//echo '<span class="amount" id="shipping-amount">'.$total_ship.'</span>';
				}
				/*echo '
				<span class="product_list_widget">
					<span class="mini_cart_item">
						<a class="remove change_ship" href="#">×</a>
					</span>
				</span>
				';*/
																		//echo '<span class="amount" id="shipping-amount">No Shipping</span>';
			}else{
				$ship_name='';
				$total_ship='';
			}
		}
		$data=array(
			'ship_name'=>$ship_name,
			'total_ship'=>$total_ship
		);
		return $data;
	}
	
	public function payment_info(){
		$buy_now=$this->session->userdata('buy_now');
		if(isset($buy_now)){
			$sub_total=0;
			$row=get_pro_field($buy_now['id']);
			//----varian-
			$product_id=$buy_now['id'];
			$color_id=$buy_now['options']['color'];
			$size_id=$buy_now['options']['size'];
			
			$varian=attributes($product_id,$color_id);
			$items_price=price_item($product_id,$color_id,$size_id,$buy_now['qty']);
			$price=$items_price['price'];
			
			$sub_total =$price;  
		}else{
			if(has_logged()){
				$cart=show_cart(get_user_id());
				$sub_total=0;
				foreach ($cart as $item):
					$row=get_pro_field($item['product_id']);
					$total=$item['qty'] * $row['sale_price'];
					//----varian-
					$product_id=$item['product_id'];
					$color_id=$item['color'];
					$size_id=$item['size'];
					
					$varian=attributes($product_id,$color_id);
					$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
					$price=$items_price['price'];
			
					$sub_total +=$price;
				endforeach;
			}else{
				if ($cart = $this->cart->contents()):
					$sub_total=0;
					foreach ($cart as $item):
						//----varian-
						$product_id=$item['id'];
						$color_id=$item['options']['color'];
						$size_id=$item['options']['size'];
						$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
						$price=$items_price['price'];
						$item_price=$items_price['price'];
						
						$sub_total +=$price;
					endforeach;
				endif;
			}
		}
		$total_ship =0;
		if($this->session->userdata('ship_cal')){
			$ship_data=$this->session->userdata('ship_cal');
			if(isset($buy_now)){
				$total_weight=0;
				$row=get_pro_field($buy_now['id']);
				$total_weight +=$row['weight'] * $buy_now['qty'];
			}else{
				if(has_logged()){
					$cart=show_cart(get_user_id());
					$total_weight=0;
					foreach ($cart as $item):
						$row=get_pro_field($item['product_id']);
						$total_weight +=$row['weight'] * $item['qty'];
					endforeach;
				}else{
					if ($cart = $this->cart->contents()):
						$total_weight=0;
						foreach ($cart as $item):
							$row=get_pro_field($item['id']);
							$total_weight +=$row['weight'] * $item['qty'];
						endforeach;
					endif;
				}
			}
			$results=total_ship($total_weight,$ship_data['method_id']);
			$fee_amount=0;
			foreach($results as $row){
				if($total_weight >= $row['min_weight'] && $total_weight <=$row['max_weight']){
					if($this->session->userdata('parcel_city')){
						if($this->session->userdata('parcel_city') == 386){
							$fee_amount = 0;
						}
						else{
							$fee_amount =$row['fee_amount'];
						}
					}
					else{
						$fee_amount =$row['fee_amount'];
					}
				}
			}
			if($fee_amount){
				$total_ship=$fee_amount;
			}else{
				$total_ship=0;
			}
			
		}else{
			if(has_logged()){
				$ship_data=getUserShipMethod(get_user_id());
				$ship_to = getUserShipTo(get_user_id());
				$getShipM = getShipMethod($ship_data, $ship_to);
				if(isset($buy_now)){
					$total_weight=0;
					$row=get_pro_field($buy_now['id']);
					$total_weight +=$row['weight'] * $buy_now['qty'];
				}else{
					//if(has_logged()){
						$cart=show_cart(get_user_id());
						$total_weight=0;
						foreach ($cart as $item):
							$row=get_pro_field($item['product_id']);
							$total_weight +=$row['weight'] * $item['qty'];
						endforeach;
					/*}else{
						$total_weight=0;
						if ($cart = $this->cart->contents()):
							
							foreach ($cart as $item):
								$row=get_pro_field($item['id']);
								$total_weight +=$row['weight'] * $item['qty'];
							endforeach;
						endif;
					}*/
				}
				
					$results=total_ship($total_weight,$getShipM);
				
				
				
				//echo $total_weight;
				//var_dump($results); exit;
				//var_dump($total_weight);
				//echo getUserShipMethod(get_user_id()) . "HEllo!"; exit;
				//var_dump($total_weight); 
				//var_dump($ship_data);
				//var_dump($results); 
				foreach($results as $row){
					if($total_weight >= $row['min_weight'] && $total_weight <=$row['max_weight']){
						//var_dump($this->session->userdata('parcel_city')); exit;
						//if($this->session->userdata('parcel_city')){
							if($ship_to == 386){
								//echo getShipCompany($ship_data).': <span class="amount">'.currency('sign').'0</span>';
								$fee_amount = 0;
							}
							else{
								//echo getShipCompany($ship_data).': <span class="amount">'.currency('sign').$row['fee_amount'].'</span>';
								$fee_amount =$row['fee_amount'];
							}
						/*}
						else{
							echo getShipCompany($ship_data).': <span class="amount">'.currency('sign').$row['fee_amount'].'</span>';
							$fee_amount =$row['fee_amount'];
						}*/
						
						
					}
				}
				//var_dump($ship_data['shipping_city']); exit;
				if(isset($fee_amount)){
					$total_ship=$fee_amount;
				}else{
					$total_ship=0;
					//echo '<span class="amount" id="shipping-amount">'.$total_ship.'</span>';
				}
				/*echo '
				<span class="product_list_widget">
					<span class="mini_cart_item">
						<a class="remove change_ship" href="#">×</a>
					</span>
				</span>
				';*/
																		//echo '<span class="amount" id="shipping-amount">No Shipping</span>';
			}else{
				//echo '<span class="amount" id="shipping-amount">No Shipping</span>';
			}
		}
		$order_num=order_number() + 1;
		$order_number='ID'.str_pad($order_num, 6, '0', STR_PAD_LEFT);
		$total= ($sub_total + $total_ship) * 100;
		$total_pay= $sub_total + $total_ship;
		$data=array(
			'order_number'=>$order_number,
			'total'=>$total,
			'total_pay'=>$total_pay
			
		);
		return $data;
	}
	
	public function success_pay(){
		$billing=$this->session->userdata('billing');
		/* if(!isset($billing)){
			redirect(base_url()); 
		} */
		$ship=$this->ship_calculate();
		$ship_name=$ship['ship_name'];
		$total_ship=$ship['total_ship'];
		
		$order_num='Cus&nbsp;00'.order_number() + 1;
		$order_link=encrypt_url($order_num);
		
		if(get_user_id()){
			$user_id=get_user_id();
		}else{
			$user_id=0;
		}
		$data = array(
			'user_id' => $user_id,
			'order_number' => order_number() + 1,
			'first_name' => $billing['first_name'],
			'last_name' => $billing['last_name'],
			'company_name' => $billing['company'],
			'email' => $billing['email'],
			'phone' => $billing['phone'],
			'country' => $billing['country'],
			'address' => $billing['address'],
			'city' => $billing['city'],
			'postcode' => $billing['postcode'],
			'total_amount' => $this->total_amount, 
			'shipping_company' => $ship_name,
			'total_shipping' => $total_ship,
			'permalink' => $order_link,
			'created' => date('Y-m-d H:i'),  
			'amount_coupon' => $this->amount_coupon,
			'amount_reward_point' => $this->amount_reward,
            'status' => 1
		);    
		$results_billing=$this->mcart->check_out($data);
		if($results_billing){
			$billing_id=$results_billing;
			$buy_now=$this->session->userdata('buy_now');
			if(isset($buy_now)){
				$row=get_pro_field($buy_now['id']);
				//----varian-
				$product_id=$buy_now['id'];
				$color_id=$buy_now['options']['color'];
				$size_id=$buy_now['options']['size'];
				
				$varian=attributes($product_id,$color_id);
				$items_price=price_item($product_id,$color_id,$size_id,$buy_now['qty']);
				$price=$items_price['price'];

				$data_ship = array(
					'bill_id' => $billing_id,
					'product_id'=> $buy_now['id'],
					'product_name' => $buy_now['name'],
					'color' => $color_id,
					'size' => $size_id,
					'qty' => $buy_now['qty'],
					'original_price'=>$items_price['original_price'],
					'discount_amount'=>$items_price['discount_amount'],
					'prices' => $price,
					'payment_id' => $billing['payment_method'],
					'created' => date('Y-m-d H:i')
				);
				$this->mcart->order($data_ship);
			}else{
				if(has_logged()){
					$cart=show_cart(get_user_id());
					foreach ($cart as $item){
						$row=get_pro_field($item['product_id']);
						//----varian-
						$product_id=$item['product_id'];
						$color_id=$item['color'];
						$size_id=$item['size'];
						
						$varian=attributes($product_id,$color_id);
						$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
						$price=$items_price['price'];
						
						$data_ship = array(
							'bill_id' => $billing_id,
							'product_id'=> $item['product_id'],
							'product_name' => $row['product_name'],
							'color' => $color_id,
							'size' => $size_id,
							'qty' => $item['qty'],
							'original_price'=>$items_price['original_price'],
							'discount_amount'=>$items_price['discount_amount'],
							'prices' => $price,
							'payment_id' => $billing['payment_method'],
							'created' => date('Y-m-d H:i')
						);
						$this->mcart->order($data_ship);
						$this->db->where(array(
							'product_id'=>$item['product_id'],
							'user_id'=>get_user_id()
						));
						$this->db->delete('sys_cart');
					}	
				}else{
					$cart = $this->cart->contents();
					foreach ($cart as $item){
						$row=get_pro_field($item['id']);
						//----varian-
						$product_id=$item['id'];
						$color_id=$item['options']['color'];
						$size_id=$item['options']['size'];
						$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
						$price=$items_price['price'];
						
						$data_ship = array(
							'bill_id' => $billing_id,
							'product_id'=> $item['id'],
							'product_name' => $item['name'],
							'color' => $color_id,
							'size' => $size_id,
							'qty' => $item['qty'],
							'original_price'=>$items_price['original_price'],
							'discount_amount'=>$items_price['discount_amount'],
							'prices' => $price,
							'payment_id' => $billing['payment_method'],
							'created' => date('Y-m-d H:i')
						);
						$this->mcart->order($data_ship);
					}
				}
			}
			
			
			/// -------START reward point
			
			$total_price = $this->mcart->total_original_price($billing_id);
			$point_reward = $this->mcart->get_point_reward();
			
			$calculate = (int)(($total_price->total*$point_reward->to_point)/$point_reward->from_amount); /// calculate point
			if($point_reward->enable_point) { /// enable point reward	 
				if($point_reward->up_to_point>0){ //** Up to point reward and discount
					if($total_price->total > $point_reward->from_amount) {
						$point = $point_reward->to_point;
						$data1 = array(
								"reward_point"=>$point
							); 
						$result = $this->db->where("bill_id",$billing_id)->update("sys_billing", $data1); 							
					}
				 }else{  
					if($total_price->total > $point_reward->from_amount) {
						$point2 = $calculate;
						$data2 = array(
								"reward_point"=>$point2
							); 
						$result = $this->db->where("bill_id",$billing_id)->update("sys_billing", $data2);  
					}
				 }    
			}
			
			$billing = $this->get_billing($user_id);
			if(!empty($billing)){			
				foreach($billing as $row){
					$total_point += $row->reward_point;
				}  
				$data = array(
						"reward_point" => $total_point
				); 
				$result = $this->db->where("user_id",$user_id)->update("sys_users", $data); 		
			}else if($this->final_total < 0) {
				$remain_point = (-1) * ( $this->final_total * $this->reward_point/$this->amount_reward );
				$result = $this->db->where("user_id",$user_id)->update("sys_users", array("reward_point"=>$remain_point)); 														
			}else { 				
				$result = $this->db->where("user_id",$user_id)->update("sys_billing", array("reward_point"=>0));  
				$result = $this->db->where("user_id",$user_id)->update("sys_users", array("reward_point"=>0)); 														
			}
			/// -------END reward point
			$this->send_receipt($order_link);	
			$this->cart->destroy();
			$this->session->unset_userdata('ship_cal');
			$this->session->unset_userdata('billing');
			$this->session->unset_userdata('buy_now');
			$this->session->unset_userdata('ref_gen');
			redirect(base_url().'cart/rec_order/'.$order_link);
		}
	}
	
	public function cana_pay(){
		$this->load->helper('payment_helper');
		$conn = new VPCPaymentConnection();
		$secureSecret = "85F1644BB218FC3FCA5FA8D73A28AD94"; 
		$conn->setSecureSecret($secureSecret);
		ksort ($_POST);
		$vpcURL = $_POST["virtualPaymentClientURL"];
		$title  = $_POST["Title"];
		$againLink ="";
		
		// unset post
		unset($_POST['con_name']);
		unset($_POST['ship_method']);
		unset($_POST['shipping_country']);
		unset($_POST['billing_first_name']);
		unset($_POST['billing_last_name']);
		unset($_POST['billing_company']);
		unset($_POST['billing_email']);
		unset($_POST['billing_phone']);
		unset($_POST['billing_country']);
		unset($_POST['billing_address_1']);
		unset($_POST['calc_shipping_state']);
		unset($_POST['billing_postcode']);
		unset($_POST['payment_method']);
		unset($_POST['terms']);
		unset($_POST['terms-field']);
		unset($_POST['place_order']);
		unset($_POST['billing_city']);
		unset($_POST['ship_method_val']);
		
		//
		
		unset($_POST["virtualPaymentClientURL"]); 
		unset($_POST["SubButL"]);
		unset($_POST["Title"]);
		foreach($_POST as $key => $value) {
			if (strlen($value) > 0) {
				$conn->addDigitalOrderField($key, $value);
			}
		}
		//$conn->addDigitalOrderField("AgainLink", $againLink);
		$secureHash = $conn->hashAllFields();
		$conn->addDigitalOrderField("Title", $title);
		$conn->addDigitalOrderField("vpc_SecureHash", $secureHash);
		$conn->addDigitalOrderField("vpc_SecureHashType", "SHA256");
		// Obtain the redirection URL and redirect the web browser
		$vpcURL = $conn->getDigitalOrder($vpcURL);
		//var_dump($vpcURL);
		//exit;
		//echo $vpcURL;
		redirect($vpcURL);
		//header("Location: ".$vpcURL);

	}
	
	public function cana_pay_form(){
		$pay=$this->payment_info();
		$order_number=$pay['order_number'];
		$total=$pay['total'];
		$ref_num=ref_number('canapay') + 1;
		$_POST['Title'] = 'MOM VARIN INVESTMENT';
		$_POST['virtualPaymentClientURL'] = "https://migs.mastercard.com.au/vpcpay";
		$_POST['vpc_Version'] = "1";
		$_POST['vpc_Command'] = "pay";
		$_POST['vpc_AccessCode'] = "CCC0BB54";
		$_POST['vpc_MerchTxnRef'] = $order_number;
		$_POST['vpc_Merchant'] = "MOMVARIN";
		$_POST['vpc_OrderInfo'] = $ref_num;
		$_POST['vpc_Amount'] = $total;
		$_POST['vpc_ReturnURL'] = base_url().'cart/notify';
		$_POST['vpc_Locale'] = "en_AU";
		$_POST['vpc_Currency'] = "USD";
		$_POST['SubButL'] = "Pay Now!";
		$this->cana_pay();

	}
	
	public function notify(){
		$this->load->helper('payment_helper');
		$conn = new VPCPaymentConnection();
		$secureSecret = "85F1644BB218FC3FCA5FA8D73A28AD94";
		$conn->setSecureSecret($secureSecret);
		$data['conn']=$conn;
		$type='canapay';
		$vpc_OrderInfo = array_key_exists("vpc_OrderInfo", $_GET)? $_GET["vpc_OrderInfo"] 	: "";
		$vpc_Amount  = array_key_exists("vpc_Amount", $_GET)? $_GET["vpc_Amount"] 	: "";
		$vpc_Currency = array_key_exists("vpc_Currency", $_GET)? $_GET["vpc_Currency"] 	: "";
		$txnResponseCode = array_key_exists("vpc_TxnResponseCode", $_GET)? $_GET["vpc_TxnResponseCode"] 	: "";
		$data=array(
			'order_refer'=>$vpc_OrderInfo,
			'total'=>$vpc_Amount,
			'currency'=>$vpc_Currency,
			'type'=>$type,
			'status'=>$txnResponseCode,
			'created_date'=>date('Y-m-d h:s')
		);
		$this->db->insert('sys_transaction',$data);
		
		if($txnResponseCode =='A'){
			$result = "Transaction Aborted";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='B'){
			$result = "Fraud Risk Blocked";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='C'){
			$result = "Transaction Cancelled";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='D'){
			$result = "Deferred transaction has been received and is awaiting processing";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='E'){
			$result = "Transaction Declined - Refer to card issuer";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='F'){
			$result = "3D Secure Authentication failed";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='G'){
			 $result = "Issuer does not participate in AVS (international transaction)";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='I'){
			$result = "Card Security Code verification failed";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='L'){
			$result = "Shopping Transaction Locked (Please try the transaction again later)";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='N'){
			$result = "Cardholder is not enrolled in Authentication scheme";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='P'){
			$result = "Transaction has been received by the Payment Adaptor and is being processed";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='R'){
			$result = "Transaction was not processed - Reached limit of retry attempts allowed";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='S'){
			$result = "Duplicate SessionID (Amex Only)";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='T'){
			$result = "Address Verification Failed";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='U'){
			$result = "Card Security Code Failed";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='X'){
			$result = "Exact match - address and 9 digit ZIP/postal code";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='Y'){
			$result = "Exact match - address and 5 digit ZIP/postal code";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='W'){
			$result = "9 digit ZIP/postal code matched, Address not Matched";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode =='Z'){
			$result = "5 digit ZIP/postal code matched, Address not Matched";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}elseif($txnResponseCode ==0){
			$this->success_pay();
		}else{
			$result = "Unable to be determined";
			$this->session->set_flashdata('msg', $result);
			redirect(base_url().'cart/received_errors');
		}
	}
	
	public function wingnotify_service(){
		if(isset($_GET['status'])){
		   if($_GET['status']=='success'){
		   
			 $token = "";
			 if(isset($_GET['status'])){
			  $token = $_GET['token'];
			 }
			  
			// get ref id from session
			$ref_gen = $this->session->userdata('ref_gen');
			$inquiry_url = "https://ketluy.wingmoney.com/wing/commitInquiry";
		//	$inquiry_url = "https://110.74.218.217/onlinepay/wing/commitInquiry";

			$param = array(
			 'loginId'=> $this->username,
			 'password' => $this->password,
			 'order_referenceno' => $ref_gen
			 );
			$content = json_encode($param);

			$curl = curl_init($inquiry_url);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			$response = curl_exec($curl);
			if ($response === FALSE) {
				echo "cURL Error: " . curl_error($curl);
			}else{
				$json = json_decode($response, true);
				if(isset($json['tid'])){
					$tid = $json['tid'];
					$tstatus = $json['errorCode'];
					$account = $json['account'];
					$currency = $json['currency'];
					$amount = $json["amount"];
					$order_referenceno = $json['order_referenceno'];
					$token = $token;
					$item =  "";
					$message =  isset($json['message'])?$json['message']:"Transfer has been successfully";
					$data = array(
					 'tid'  => $tid,
					 'account' => $account,
					 'amount' => $amount,
					 'order_referenceno' => $order_referenceno,
					 'token'  => $token,
					 'currency' => $currency,
					 'item'  => $item,
					 'message' => $message,
					 'tstatus' => $tstatus,
					 'dates'  => date('Y-m-d h:s'),  
					);
					$result=$this->db->insert('sys_wing_invoice',$data); 
					if($result){
						$this->success_pay(); 
					}        
				 }
			}
		  }else{
				$errorCode="";
				if(isset($_GET['status'])){
				 echo 'Status: '. $_GET['status'].'<br/>';
				}
				if(isset($_GET['token'])){
				 echo 'Token: '. $_GET['token'].'<br/>';
				}

				if(isset($_GET['message'])){
					$errorCode=$_GET['message'];
				}
				$this->session->set_flashdata('msg', $errorCode);
				redirect(base_url().'cart/received_errors');
				
			}
		}
	}
	
	public function commitInquiry(){
	  $post = $this->input->get();
	  $order = $this->security->xss_clean($post['order']);
	  $token = $this->security->xss_clean($post['token']);
	  $url = "https://ketluy.wingmoney.com/wing/commitInquiry";
	  $urlHost = base_url();
	  $param = array(
		 'loginId' => $this->username,
		 'password' => $this->password,
		 'order_referenceno' => $order,
	  );
	  $content = json_encode($param);           
	  $curl = curl_init($url);
	  curl_setopt($curl, CURLOPT_HEADER, false);
	  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	  curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
	  curl_setopt($curl, CURLOPT_POST, true);
	  curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
	  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);         

	  $response = curl_exec($curl); 
	  $json= json_decode($response,true);
	  
	  if ($response === FALSE) 
	  {
	   echo "cURL Error: " . curl_error($curl);
	  }
	  else
	  {
		var_dump($json);
	  }
	}
	
	public function reversal(){
	   $post = $this->input->post();
	   $order = $this->security->xss_clean($post['order']);
	   $token = $this->security->xss_clean($post['token']);
	   $row = $this->course->select_tranfer_token($token) or die("Your link has been expired.");
	   $url = "https://ketluy.wingmoney.com/wing/reversal";
	   $urlHost = base_url();
	   $param = array(
		  'loginId' => $this->username,
		  'password' => $this->password,
		  'order_referenceno' => $order
	   );
	   $content = json_encode($param);           
	   $curl = curl_init($url);
	   curl_setopt($curl, CURLOPT_HEADER, false);
	   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	   curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
	   curl_setopt($curl, CURLOPT_POST, true);
	   curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
	   curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	   $response = curl_exec($curl); 
	   $json= json_decode($response,true);
	  
	   if ($response === FALSE) 
	   {
		echo "cURL Error: " . curl_error($curl);
	   }
	   else
	   { 
			var_dump($json);
	   } 
	   
	}
	
	public function wingnotify(){
		
		$get = $this->input->get();
		if(isset($get['status']) && isset($get['token'])){		
			$status = htmlspecialchars($get['status']);
			$token = htmlspecialchars($get['token']);
			$result = $this->db->where('token',$token)->get('sys_wing_invoice')->row();
			
			if(count($result) > 0){
				if($result->tstatus == '200'){
					$this->success_pay();	
				}else if($result->tstatus=='00335'){
					$errorCode = "Security Code & Wing Account mismatch";
					$this->session->set_flashdata('msg', $errorCode);
					redirect(base_url().'cart/received_errors');
				}else if($result->tstatus=='00340'){
					$errorCode = "Security Code Expired";
					$this->session->set_flashdata('msg', $errorCode);
					redirect(base_url().'cart/received_errors');
				}else if($result->tstatus=='000030'){	
					$errorCode = "General Fail Exception";
					$this->session->set_flashdata('msg', $errorCode);
					redirect(base_url().'cart/received_errors');					
				}else if($result->tstatus=='00002'){
					$errorCode = "Token is expired";
					$this->session->set_flashdata('msg', $errorCode);
					redirect(base_url().'cart/received_errors');
				}else if($result->tstatus=='006035'){
					$errorCode = "Transaction not found.";
					$this->session->set_flashdata('msg', $errorCode);
					redirect(base_url().'cart/received_errors');
				}else{
					$errorCode = "Something error, please contact to administrator";
					$this->session->set_flashdata('msg', $errorCode);
					redirect(base_url().'cart/received_errors');
				}
			}else{
				$errorCode = "Transaction not found.";
				$this->session->set_flashdata('msg', $errorCode);
				redirect(base_url().'cart/received_errors');
			}			
		}else{
			$errorCode = "Transaction not found.";
			$this->session->set_flashdata('msg', $errorCode);
			redirect(base_url().'cart/received_errors');
		}
	}
	
	public function checkout()
	{ 
		$this->total_with_coupon = $this->input->post('total_with_coupon');
		$this->final_total = $this->input->post('final_total');
		$this->reward_point = $this->input->post('reward_point'); 
		$cart = $this->cart->contents();
		
		$buy_now = $this->session->userdata('buy_now');			
		if($this->input->get("vc") == NULL){
			$this->session->unset_userdata('buy_now');
		}

		$shipMethod = $this->input->post('ship_method_val');
		
		$this->billing();
		$this->form_validation->set_rules('billing_first_name', 'billing_first_name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('billing_last_name', 'billing_last_name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('billing_email', 'billing_email', 'required|trim|xss_clean');
		$this->form_validation->set_rules('billing_phone', 'billing_phone', 'required|trim|xss_clean');
		$this->form_validation->set_rules('billing_country', 'billing_country', 'required|trim|xss_clean');
		$this->form_validation->set_rules('billing_address_1', 'billing_address_1', 'required|trim|xss_clean');
		$this->form_validation->set_rules('calc_shipping_state', 'billing_city', 'trim|xss_clean');
		if($shipMethod == 3){
			$this->form_validation->set_rules('billing_postcode', 'billing_postcode', 'trim|xss_clean');
		}else{
			$this->form_validation->set_rules('billing_postcode', 'billing_postcode', 'required|trim|xss_clean');
		}
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'required|trim|xss_clean');
		$this->form_validation->set_rules('terms', 'Terms and Conditions', 'required|trim|xss_clean');

		if ($this->form_validation->run() === TRUE){
			if(has_logged()){ 
				$cart = $this->mcart->show_cart(get_user_id());
				foreach ($cart as $row) {
					$product_id = $row['product_id'];
					$user_id = $row['user_id'];
					$qty = $row['qty'];
					$row=get_pro_field($product_id);
					$insert_data = array(
						'id' => $product_id,
						'name' => $row['product_name'],
						'color' => $row['color'],
						'size' => $row['size'],
						'price' => $row['sale_price'],
						'qty' => $qty 
					); 
					$this->cart->insert($insert_data);
				}  
			}else{
				$cart = $this->cart->contents();
			}
			$isms_noti = $this->msms_notification->get_sms_notification();
			if($isms_noti->isms > 0)
			{
				
			}     
			$payMethod = $this->input->post('payment_method', true); 
			
			//****** Wing Payment method.*******
			if($payMethod==1){   
				if(has_logged()){
					$cart = $this->mcart->show_cart(get_user_id());
					$count=count($cart);
					if(!$count and $buy_now==null){
						redirect(base_url());
					}
				}else{
					$cart = $this->cart->contents();
					if(!$cart and $buy_now==null){
						redirect(base_url());
					}
				}
				$this->billing();
				$pay=$this->payment_info();
				/// IMP from Seavmeng
					if($this->input->post('amount_reward') > 0 ){
						$total = $this->final_total; /// total amount after redeem point 
						$this->amount_reward = $this->input->post('amount_reward');
					}else if(isset($this->total_with_coupon)) { //// total amount after redeem with coupon
						$total = $this->total_with_coupon; 
						$this->amount_coupon = $this->input->post('amount_coupon');
					}else{
						$total = $pay['total_pay'];
					}
				$this->total_amount = $total;   
				//$this->success_pay();
				//***** END *****/ 
				$order_number=$pay['order_number']; 
				//-------
				$authenticate = "https://ketluy.wingmoney.com/wing/Authenticate";
				$payment = "https://ketluy.wingmoney.com/payment";
				$notify_url =  base_url().'cart/wingnotify_service'; // change ip address
				$return_url = base_url().'cart/wingnotify_service'; // change ip address
				$this->session->set_userdata('ref_gen',$order_number);
				$param = array(
					'loginId'=> $this->username,
					'password' => $this->password,
					'item'=>$order_number,
					'amount'=>$total,
					'merchant_name'=> 'Angkorstores.com',
					'notify_url'=>  $notify_url,
					'return_url'=>  $return_url,
					'order_referenceno' => $order_number,
					'biller'=> $order_number
				); 
				$content = json_encode($param);
				$curl = curl_init($authenticate);
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
				$response = curl_exec($curl);
				var_dump("response:".$response);
				if ($response === FALSE) {
					echo "cURL Error: " . curl_error($curl);
				}else{
					redirect($payment."?token=".$response);
				} 
			}elseif($payMethod==2){
				if(has_logged()){
					$cart = $this->mcart->show_cart(get_user_id());
					$count=count($cart);
					if(!$count and $buy_now==null){
						redirect(base_url());
					}
				}else{
					$cart = $this->cart->contents();
					if(!$cart and $buy_now==null){
						redirect(base_url());
					}
				}
				$this->billing();
				$this->cana_pay_form();
			}elseif($payMethod==3){
				if(has_logged()){
					$cart = $this->mcart->show_cart(get_user_id());
					$count=count($cart);
					if(!$count and $buy_now==null){
						redirect(base_url());
					}
				}else{
					$cart = $this->cart->contents();
					if(!$cart and $buy_now==null){
						redirect(base_url());
					}
				}
				$this->billing();
				$this->session->set_flashdata('msg', 'confirm_buy');
				redirect(base_url().'cart/confirm_pay');

			}elseif($payMethod==5){  
				if(has_logged()){
					$cart = $this->mcart->show_cart(get_user_id());
					$count=count($cart);
					if(!$count and $buy_now==null){ 
						redirect(base_url());
					}
				}else{ 
					$cart = $this->cart->contents();
					if(!$cart and $buy_now==null){
						redirect(base_url());
					}
				}
				$this->billing();
				$this->session->set_flashdata('msg', 'confirm_buy');
				$this->payway_confirm();
			}else{
				echo 'Please choose the correct payment method';
			}
		}
		else
		{ 			
			$this->_data['point'] = $this->get_user_reward(get_user_id());
			$this->_data['reward'] = $this->get_reward_point();
			$this->_data['rmsg']=$this->session->flashdata('msg');
			$this->_data['billing_info']=$this->mcart->billing_info(get_user_id());
			$this->_data['ship_company']=$this->mcart->ship_company();
			$this->_data['ship_country']=$this->mcart->shipping_cam();
			$this->_data['ship_city']=$this->mcart->shipping_city();
			$this->_data['payment_method']=$this->mcart->payment_method();
			$this->load->view('home/checkout',$this->_data);
			$this->session->unset_userdata('coupon');
		}
	}
	
	public function get_user_reward($id) {
		$result = $this->db->select("reward_point")->from("sys_users")->where("user_id",$id)->get()->row();
		return $result;
	} 
	
	public function get_reward_point() {
		$result = $this->db->get("sys_point_rewards")->row();
		return $result;
	}
	
	public function confirm_pay()
	{  
		$rmsg=$this->session->flashdata('msg');
		if($rmsg){
			ini_set("soap.wsdl_cache_enabled", "0"); 
			ini_set('max_execution_time', 500);
			$pay=$this->payment_info();
			$order_number=$pay['order_number'];
			$total=$pay['total_pay'];
			$wsdl = 'https://epayment.acledabank.com.kh:8443/ANGKORSTORES/XPAYConnectorServiceInterfaceImpl?wsdl';
			$options = array(
				'cache_wsdl'=>WSDL_CACHE_NONE,
				'trace'=>true,
				'encoding'=>'UTF-8',
				'exceptions'=>true,
			);
			$data=array(
				'description' =>"PIV",
				'txid' => date('hhiiss'),	
				'purchaseAmount' => $total,
				'purchaseCurrency' => 'USD',
				'purchaseDesc' =>'',
				'ipAddress' =>'',
				'longitude' =>'',
				'lattitude' =>'',
				'invoiceid' =>$order_number,
				'item' =>'1',
				'quantity' =>1,
				'initialUrl' =>base_url().'cart/xnotify?p=success',
				'expiryTime' =>5,
				'errorUrl' =>base_url()."cart/xnotify?p=error",
				'loginId' =>"Angkorstores",
				'password' =>"Angkorstores123@",
				'merchantID' =>"qNElVvHaaN446Wdeu/ayIOfRJpI=",
				'signature' =>"aba407d59",
				'purchaseDate' =>''
			);

			try {
				$client = new SoapClient($wsdl);
				$params = array (
					"loginId" => $data['loginId'],
					"password" => $data['password'],
					"merchantID" => $data['merchantID'],
					"signature" => $data['signature'],
					'XpayTransaction' => array(
						'txid' => $data['txid'],
						'purchaseAmount' => $data['purchaseAmount'],
						'purchaseCurrency'=> $data['purchaseCurrency'],
						'purchaseDate' => $data['purchaseDate'],
						'purchaseDesc' => $data['purchaseDesc'],
						'ipAddress' => $data['ipAddress'],
						'longitude' => $data['longitude'],
						'lattitude' => $data['lattitude'],
						'invoiceid' => $data['invoiceid'],
						'item' => $data['item'],
						'quantity' => $data['quantity'],
						'initialUrl' => $data['initialUrl'],
						'expiryTime' => $data['expiryTime'],
					),
				);
				$response = $client->OpenSessionV2($params); 
				$data['sessionID'] = $response->return->sessionid;
				$data['paymentTokenID'] = $response->return->xTran->paymentTokenid;
			}
			catch(Exception  $error){}
			$this->_data['xdata']=$data;
            $this->_data['sessionID']=$data['sessionID'];
			$this->_data['paymentTokenID']=$data['paymentTokenID'];
			$this->_data['ship_country']=$this->mcart->shipping_cam();
			$this->_data['ship_city']=$this->mcart->shipping_city();
			$this->load->view('home/checkout_confirm',$this->_data);
		}else{
			$this->load->view('errors/404');
		}
	}
	
	public function payway_confirm()
	{
		$billing=$this->session->userdata('billing');
		if(!isset($billing)){
			redirect(base_url());
		}
		$ship=$this->ship_calculate();
	
		$ship_name=$ship['ship_name'];
		$total_ship=$ship['total_ship'];
		$order_num= order_number() + 1;
		$order_link=encrypt_url($order_num);

		if(get_user_id()){
			$user_id=get_user_id();
		}else{
			$user_id=0;
		}
		$data = array(
			'user_id' => $user_id,
			'order_number' => $order_num,
			'first_name' => $billing['first_name'],
			'last_name' => $billing['last_name'],
			'company_name' => $billing['company'],
			'email' => $billing['email'],
			'phone' => $billing['phone'],
			'country' => $billing['country'],
			'address' => $billing['address'],
			'city' => $billing['city'],
			'postcode' => $billing['postcode'],
			'shipping_company' => $ship_name,
			'total_shipping' => $total_ship,
			'permalink' => $order_link,
			'created' => date('Y-m-d H:i'),
            'status' => 0
		);
		
		$results_billing=$this->mcart->check_out($data);
		if($results_billing){
			$billing_id=$results_billing;
			$buy_now=$this->session->userdata('buy_now');
			if(isset($buy_now)){
				
				$row=get_pro_field($buy_now['id']);
				$product_id=$buy_now['id'];
				$color_id=$buy_now['options']['color'];
				$size_id=$buy_now['options']['size'];
				$varian=attributes($product_id,$color_id);
				$items_price=price_item($product_id,$color_id,$size_id,$buy_now['qty']);
				$price=$items_price['price'];
				$data_ship = array(
					'bill_id' => $billing_id,
					'product_id'=> $buy_now['id'],
					'product_name' => $buy_now['name'],
					'color' => $color_id,
					'size' => $size_id,
					'qty' => $buy_now['qty'],
					'original_price'=>$items_price['original_price'],
					'discount_amount'=>$items_price['discount_amount'],
					'prices' => $price,
					'payment_id' => $billing['payment_method'],
					'created' => date('Y-m-d H:i')
				);
				$this->mcart->order($data_ship);
				
			}else{
				
				if(has_logged()){
					$cart=show_cart(get_user_id());
					foreach ($cart as $item){
						$row=get_pro_field($item['product_id']);
						$product_id=$item['product_id'];
						$color_id=$item['color'];
						$size_id=$item['size'];
						$varian=attributes($product_id,$color_id);
						$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
						$price=$items_price['price'];
						$data_ship = array(
							'bill_id' => $billing_id,
							'product_id'=> $item['product_id'],
							'product_name' => $row['product_name'],
							'color' => $color_id,
							'size' => $size_id,
							'qty' => $item['qty'],
							'original_price'=>$items_price['original_price'],
							'discount_amount'=>$items_price['discount_amount'],
							'prices' => $price,
							'payment_id' => $billing['payment_method'],
							'created' => date('Y-m-d H:i')
						);
						$this->mcart->order($data_ship);
						/*$this->db->where(array(
							'product_id'=>$item['product_id'],
							'user_id'=>get_user_id()
						));
						$this->db->delete('sys_cart');*/
					}	
				}else{
					$cart = $this->cart->contents();
					foreach ($cart as $item){
						$row=get_pro_field($item['id']);
						$product_id=$item['id'];
						$color_id=$item['options']['color'];
						$size_id=$item['options']['size'];
						$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
						$price=$items_price['price'];
						
						$data_ship = array(
							'bill_id' => $billing_id,
							'product_id'=> $item['id'],
							'product_name' => $item['name'],
							'color' => $color_id,
							'size' => $size_id,
							'qty' => $item['qty'],
							'original_price'=>$items_price['original_price'],
							'discount_amount'=>$items_price['discount_amount'],
							'prices' => $price,
							'payment_id' => $billing['payment_method'],
							'created' => date('Y-m-d H:i')
						);
						$this->mcart->order($data_ship);
					}
				}
			}
		}
		$this->_data['point'] = $this->get_user_reward(get_user_id());
		$this->_data['reward'] = $this->get_reward_point();
		$this->_data['order_number'] = $order_num;
		$this->_data['ship_country']=$this->mcart->shipping_cam();
		$this->_data['ship_city']=$this->mcart->shipping_city();
		$this->load->view('home/payway_confirm',$this->_data);	
	}
	
	public function payway_notify()
	{
		$content = file_get_contents("php://input");
		if(isset($content)) {
			$json = json_decode($content, true);
			if(isset($json)){
				$tran_id = $json['tran_id'];
				$tstatus = $json['status'];
				$tprocess = $this->db->where("order_number",$tran_id)->update("sys_billing", array("status"=>1));
				if($tprocess){
					$result = $this->db->where("order_number",$json['tran_id'])->get("sys_billing")->row();
					if($result){
					   $this->send_receipt($result->permalink);				
					}
				}				
			}
		}				
	}
	
	public function xnotify(){
		ini_set("soap.wsdl_cache_enabled", "0"); 
		ini_set('max_execution_time', 300);
		if(isset($_GET['p'])):
			// Auto generated unique identity code by T24
			$_arNo = htmlspecialchars($_GET['_arNo']); 
			// Auto generated id for transaction
			$_transactionid = htmlspecialchars($_GET['_transactionid']);
			// Result of payment done
			$_paymentresult = htmlspecialchars($_GET['_paymentresult']);
			// Auto generated Token id generated by Xpay
			$_paymenttokenid = htmlspecialchars($_GET['_paymenttokenid']);
			// Error code value
			$_resultCode = htmlspecialchars($_GET['_resultCode']);
			if($_GET['p']=='success'){
				$this->success_pay();
			}elseif($_GET['p']=='error'){
				// error
				redirect(base_url().'cart/received_errors');
			}else{
				redirect(base_url().'cart/received_errors');
			}
		endif;
	}
	
	public function rec_order($id=null){
		if($id==""){
			redirect(base_url().'cart');
		}
		$check_order=$this->mcart->check_order($id);
		if($check_order['bill_id'] ==""){
			redirect(base_url().'cart');
		} 
		
		$this->_data['rec_order']=$this->mcart->receied_order($id);
		$this->_data['rec_product_order']=$this->mcart->receied_pro_order($id);
		$this->load->view('home/order_received',$this->_data);
	}
	
	public function send_receipt($order_id){
	    $bill=$this->session->userdata('billing');
		$payment_method=$bill['payment_method'];
		if($payment_method==1){
			$tid='<li class="date">
				TID: <strong>'.tID().'</strong>
				</li>';
		}else{
			$tid="";
		}
		$rec_order=$this->mcart->receied_order($order_id);
		$rec_product_order=$this->mcart->receied_pro_order($order_id);
		$order_number='ID'.str_pad($rec_order['order_number'], 6, '0', STR_PAD_LEFT);
		$fullname = $rec_order['first_name'].' '.$rec_order['last_name'];
		$head='
		<header class="entry-header">
			<h1 class="entry-title" itemprop="name">Order Received</h1>
		</header>
						<div class="entry-content" itemprop="mainContentOfPage">
							<div class="woocommerce">
								<p class="woocommerce-thankyou-order-received">Thank you. Your order has been received.</p>

								<ul class="woocommerce-thankyou-order-details order_details">
								        <li class="fullname">
										Name: <strong>'.$fullname.'</strong>
									</li>
									<li class="phone">
										Phone: <strong>'.$rec_order['phone'].'</strong>
									</li>
									<li class="order">
										Order Number:<strong>'.$order_number.'</strong>
									</li>
									<li class="date">
										Date: <strong>'.$rec_order['created'].'</strong>
									</li>
									'.$tid.'
								</ul>
								<div class="clear"></div>
							<h2>Order Details</h2>
							<table style="width:100%;border:1px solid #d8d8d8;border-right:1px solid #eceeef;">
								<thead style="background:#F5F5F5;">
									<tr>
										<th style="border-right:1px solid #eceeef;padding:10px;">Product</th>
									    <th style="border-right:1px solid #eceeef;padding:10px;">Unit Price</th>
										<th style="border-right:1px solid #eceeef;padding:10px;">Item Discount (%)</th>
										<th style="border-right:1px solid #eceeef;padding:10px;">Total</th>
									</tr>
								</thead>
								<tbody>
				';
		
				$sub_total=0;
				$body="";
				foreach($rec_product_order as $data){
					$color_id=$data['color'];
					$size_id=$data['size'];
					$varian=attributes($data['product_id'],$color_id);
					$per_price = $data['prices'];
					$unit_price = $data['original_price'];
					$discount = $data['discount_amount'];
					
					$color = $color_id !="" ? '<strong>Color: </strong>'.$varian['attr_name'].'<br />' : '';
					if($size_id){
						$size=get_attr_name($size_id);
						$getsize ='<strong>Size: </strong>'.$size['attr_name'].'<br />';
					}else{
						$getsize ='';
					}
					$body.='
					<tr>
						<td style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:8px;">
							<a href="#">'.$data['product_name'].'</a> <strong class="product-quantity">&times; '.$data['qty'].'</strong><br/>
							'.$color.$getsize.'
						</td>
						<td style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:8px;text-align:right;">
							<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>'.$unit_price.'</span>		
						</td>
						<td style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:8px;text-align:right;">
							'.$discount.' %</span>	
						</td>
						<td style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:8px;text-align:right;">
							<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>'.$per_price.'</span>	
						</td>
					</tr>';
					$sub_total += $per_price;
					$pay_id=$data['payment_id'];
				}
				$total_ship=$rec_order['total_shipping'];
				if($rec_order['shipping_company'] ==""){
					$shipping_company='<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">No Shipping</span></span>';
				}else{
					$shipping_company='<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>'.$total_ship.'</span>&nbsp;<small class="shipped_via">via '.$rec_order['shipping_company'].'</small>';
				}
                $total=$sub_total+$total_ship;
		
		        $footer='	
					</tbody>
					<tfoot>
						<tr>
							<th colspan="3" style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:8px;text-align:right;">Subtotal:</th>
							<td style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:8px;text-align:right;"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>'.$sub_total.'</span></td>
						</tr>
						<tr>
							<th colspan="3" style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:8px;text-align:right;">Shipping:</th>
							<td style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:8px;text-align:right;">'.$shipping_company.'</td>
						</tr>
						<tr>
							<th colspan="3" style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:10px;text-align:right;">Payment Method:</th>
							<td style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:10px;text-align:right;">'.payment_method($pay_id).'</td>
						</tr>
						<tr>
							<th colspan="3" style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:10px;text-align:right;">Total:</th>
							<td style="border-top:1px solid #eceeef;border-right:1px solid #eceeef;padding:10px;text-align:right;"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>'.$total.'</span></td>
						</tr>
					</tfoot>
				</table>
				</div>
			</div>		
		';
		$output=$head.$body.$footer;

		$to = $rec_order['email'];
		$from = "customerservice@angkorstores.com";
		$subject = "Orders";
		$send = $this->send_email($to, $from, $output, $subject);
		if($send){
			$subject = "Orders";
			$this->send_email($from, $to, $output, $subject);
		}
	}
	
	public function send_email($to = "", $from = "", $message= "", $subject = "Invoice"){
			
		$this->email->from($from, "Customer Service");
		$this->email->to($to); 
		$this->email->subject($subject);					
		$this->email->message($message);  
		$this->email->set_mailtype('html');
		if($this->email->send()){
			return true;
		}else{		
			$to      = $to;
			$headers = 'Content-Type: text/html; charset=UTF-8'. "\r\n" .
						'From: 	Customer Service <customerservice@angkorstores.com>' . "\r\n" .
			    		'Reply-To: Customer Service <customerservice@angkorstores.com>' . "\r\n" .
			    	    'X-Mailer: PHP/' . phpversion();
			$send = mail($to, $subject, $message, $headers);
			return $send;
		}
	}
	
	public function update_addtocart() {
		if(has_logged()){
			$cart = $this->mcart->show_cart(get_user_id());
			$exists = false;
			$cart_id = '';
			$qtyArray = $this->input->post('custom');
			$proidArray = $this->input->post('product_id');
			$i =0;
			foreach($cart as $item){
				//if($item['product_id'] == $this->input->post('product_id'))     //if the item we're adding is in cart add up those two quantities
				if($item['product_id'] == $proidArray[$i]);     //if the item we're adding is in cart add up those two quantities
				{
					$exists = true;
					$cart_id = $item['cart_id'];
					$qty = $qtyArray[$i];
				}
				if($exists){
					$data = array(
						'cart_id'   => $cart_id,
						'qty'     => $qty
					);
					$result=$this->mcart->update_cart(get_user_id(),$cart_id,$data);
					if($result){
						//redirect(base_url().'cart');
						echo $card_id . "<br>";
					}/*else{
						redirect(base_url().'cart/checkout');
					}*/
				}/*else{
					// Set array for send data.
					$insert_data = array(
						'product_id' => $proidArray[$i],
						'user_id' => get_user_id(),
						'qty' => $qtyArray[$i],
						'created_date'=> date('Y-m-d H:i')
					);
					$this->mcart->add_cart($insert_data);
					if(isset($_POST['action'])){
						redirect(base_url().'cart');
					}
					else{
						redirect(base_url().'cart/checkout');
					}
				}*/$i++;       
			}redirect(base_url().'cart');
			
		}else{
			$qtyArray = $this->input->post('custom');
			$i =0;
			foreach ($this->cart->contents() as $item) {
			        $cart_up_data = array(
			        'rowid' => $item['rowid'],
			        //'qty' => $this->input->post('custom')
			        'qty' => $qtyArray[$i]
			        );
		        //echo $cart_up_data['rowid']. "<br>";
			        $i++;
			        $this->cart->update($cart_up_data);
		    	}
		    	redirect(base_url().'cart');
	    }
	
	    
	
	    //echo "<pre>";
	    //print_r($cart_up_data);

	}
	
	function addcart(){
		$color=$this->input->post('color');
		$size=$this->input->post('size');
		$varian=array('size'=>$size,'color'=>$color);
		$row=get_pro_field($this->input->post('product_id'));

		if($this->input->post('buy_now')){
				$this->session->set_userdata('buy_now',array(
				'id' => $this->input->post('product_id'),
				'name' => $this->input->post('product_name'),
				'options' =>$varian,
				'price' => $row['sale_price'],
				'qty' => $this->input->post('quantity')
			));
			redirect(base_url().'cart/checkout?vc=cart');
		}else{
			if(has_logged()){
				$cart = $this->mcart->show_cart(get_user_id()); 
				$exists = false;
				$cart_id = '';
				foreach($cart as $item){
					if($item['product_id'] == $this->input->post('product_id')) 
					{
						$exists = true;
						$cart_id = $item['cart_id'];
						$qty = $item['qty'] + $this->input->post('quantity');
					}       
				}
				if($exists){
					$color_id=$item['color'];
					$size_id=$item['size'];					
					if($color != $color_id || $size !=$size_id){
						$insert_data = array(
							'product_id' => $this->input->post('product_id'),
							'user_id' => get_user_id(),
							'color' => $color,
							'size' => $size,
							'qty' => $this->input->post('quantity'),
							'created_date'=> date('Y-m-d H:i')
						);
						$this->mcart->add_cart($insert_data);
					}else{
						$data = array(
							'cart_id'   => $cart_id,
							'qty'     => $qty
						);
						$result=$this->mcart->update_cart(get_user_id(),$cart_id,$data);
						redirect(base_url().'cart');
					}
				}else{
					$insert_data = array(
						'product_id' => $this->input->post('product_id'),
						'user_id' => get_user_id(),
						'color' => $color,
						'size' => $size,
						'qty' => $this->input->post('quantity'),
						'created_date'=> date('Y-m-d H:i')
					);
					$this->mcart->add_cart($insert_data);
					redirect(base_url().'cart');
				}
			//------------close logged add cart-----
			}else{
 				$cart = $this->cart->contents(); //get all items in the cart
				$exists = false;             //lets say that the new item we're adding is not in the cart
				$has_op = false;             //lets say that the new item we're adding is not in the cart
				$rowid = '';
				foreach($cart as $item){
					$color_id=$item['varian']['color'];
					$size_id=$item['varian']['size'];
					if($item['id'] == $this->input->post('product_id')){
						$exists = true;
						$rowid = $item['rowid'];
						$qty = $item['qty'] + $this->input->post('quantity');
					}       
				}
				if($exists){
					if($color != $color_id && $size !=$size_id){
						$insert_data = array(
							'id' => $this->input->post('product_id'),
							'name' => $this->input->post('product_name'),
							'options' =>$varian,
							'price' => $row['sale_price'],
							'qty' => $this->input->post('quantity')
						);
						$this->cart->insert($insert_data);
					}else{
						$data = array(
							'rowid'   => $rowid,
							'qty'     => $qty
						);
						$this->cart->update($data);
					}
				}else{
					// Set array for send data.
					$insert_data = array(
						'id' => $this->input->post('product_id'),
						'name' => $this->input->post('product_name'),
						'options' => $varian,
						'price' => $row['sale_price'],
						'qty' => $this->input->post('quantity')
					);
					$this->cart->insert($insert_data);
				}
			}
		}
	}
	
	function remove_allcart(){
		$this->cart->destroy();
		redirect(base_url().'cart');
	}
	
	function remove_cart($rowid) {
		// Destroy selected rowid in session.
		if(has_logged()){
			$data = array(
				'rowid'   => $rowid,
				'qty'     => 0
			);
			// Update cart data, after cancle.
			$this->cart->update($data);
			
			$del=$this->mcart->delete_cart($rowid);
			if($del){
				$this->session->set_flashdata('msg', 'Your product remove success from cart...');
			}
			
		}else{
			$data = array(
				'rowid'   => $rowid,
				'qty'     => 0
			);
			// Update cart data, after cancle.
			$this->cart->update($data);
		}
		redirect(base_url().'cart');
	}
	
	function update_qty(){
		$data = array(
			'rowid'   => $this->input->post('row_id'),
			'qty'     => $this->input->post('upqty')
		);
		//var_dump($data);
		// Update cart data, after cancle.
		$this->cart->update($data);
		//redirect(base_url());
	}
	
	function city_bycountry($parent_id, $child_id =''){
		if(empty($parent_id)){
			echo "Please choose Country";
		}else{
			//$selectedCity = $this->input->post('child');
			$this->db->select('*');
			$this->db->from('sys_location');
			$this->db->where('parent_id',$parent_id);
			$this->db->order_by('location_name','ASC');
			$query=$this->db->get();
			$results=$query->result_array();
			if($results){
				
				$output="";
				$output.="<select id='calc_shipping_state' class='form-control' name='calc_shipping_state'>
							<option value=''>Select an City...</option>";
							/*if($this->session->userdata('ship_cal')){
							$ship_data=$this->session->userdata('ship_cal');
													
							}*/
							foreach($results as $data){
								
									if($child_id == $data['loc_id']){
										$select="selected";
									}
									else{
										$select="";
									}
									$output.='<option value="'.$data['loc_id'].'"'.' '.$select.'>'.$data['location_name'].'</option>';	
							}
				$output.="</select>";
				echo $output;
			}
		}
	}
	
	function shipLocal(){
		$results=$this->mcart->shipping_city();
		$getShipTo = getUserShipTo(get_user_id());
		if($results){
				
				$output="";
				$output.="<select id='shipping_city' class='form-control' name='shipping_city'>
							<option value=''>Select an City...</option>";
							foreach($results as $data){
									if($getShipTo == $data['loc_id']){
										$select = 'selected';
									}else{
											$select = '';
									}
									$output.='<option value="'.$data['loc_id'].'"'.' '.$select.'>'.$data['location_name'].'</option>';	
							}
				$output.="</select>";
				echo $output;
			}
		
	}
	
	function shipDHL(){
		$this->db->select('*');
		$this->db->from('sys_shopping_zone');
		$this->db->where(array('shipping_id'=>1,'country_id !='=>""));
		$query=$this->db->get();
		$results=$query->result_array();
		$getShipTo = getUserShipTo(get_user_id());
		//var_dump ($getShipTo); exit;
		if($results){
			$output="";
			$output.="<select id='shipping_country' class='form-control' name='shipping_country'>
						<option value=''>...Select Country...</option>";
						foreach($results as $data){
								if($getShipTo == $data['country_id']){
										$select = 'selected';
								}else{
										$select = '';
								}
								$output.='<option value="'.$data['country_id'].'"'. ' '.$select.'>'.country_byid($data['country_id']).'</option>';	
						}
			$output.="</select>";
			echo $output;
		}
	}
             
    function shipEms(){
		$this->db->select('*');
		$this->db->from('sys_shopping_zone');
		$this->db->where(array('shipping_id'=>4,'country_id !='=>""));
		$query=$this->db->get();
		$results=$query->result_array();
		$getShipTo = getUserShipTo(get_user_id());
		if($results){
			$output="";
			$output.="<select id='shipping_country' class='form-control' name='shipping_country'>
						<option value=''>...Select Country...</option>";
						foreach($results as $data){
								if($getShipTo == $data['country_id']){
										$select = 'selected';
								}else{
										$select = '';
								}
								$output.='<option value="'.$data['country_id'].'"'. ' '.$select.'>'.country_byid($data['country_id']).'</option>';	
						}
			$output.="</select>";
			echo $output;
		}
	}
	
	function shipfedex(){ 
		$this->db->select('*');
		$this->db->from('sys_shopping_zone');
		$this->db->where(array('shipping_id'=>2,'country_id !='=>""));
		$query=$this->db->get();
		$results=$query->result_array();
		$getShipTo = getUserShipTo(get_user_id());
		if($results){
			$output="";
			$output.="<select id='shipping_country' class='form-control' name='shipping_country'>
						<option value=''>...Select Country...</option>";
						foreach($results as $data){
								if($getShipTo == $data['country_id']){
										$select = 'selected';
								}else{
										$select = '';
								}
								$output.='<option value="'.$data['country_id'].'"'. ' '.$select.'>'.country_byid($data['country_id']).'</option>';	
						}
			$output.="</select>";
			echo $output;
		}
	}
	
	function ship_city($parent_id){
		if(empty($parent_id)){
			echo "Please choose Country";
		}else{
			$this->db->select('*');
			$this->db->from('sys_location');
			$this->db->where('parent_id',$parent_id);
			$this->db->order_by('location_name','ASC');
			$query=$this->db->get();
			$results=$query->result_array();
			if($results){
				
				$output="";
				$output.="<select id='shipping_city' class='form-control' name='shipping_city'>
							<option value=''>Select City...</option>";
							foreach($results as $data){
									$output.='<option value="'.$data['loc_id'].'">'.$data['location_name'].'</option>';	
							}
				$output.="</select>";
				echo $output;
			}
		}
	}
	
	function change_ship(){
		$this->session->unset_userdata('ship_cal');
		$this->session->unset_userdata('parcel_city');
		$this->session->set_userdata('billing',array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'email'=>$this->input->post('billing_email'),
			'phone'=>$this->input->post('billing_phone'),
			'country'=>$this->input->post('calc_shipping_country'),
			'address'=>$this->input->post('billing_address_1'),
			'city'=>$this->input->post('calc_shipping_state'),
			'postcode'=>$this->input->post('billing_postcode'),
			'payment_method'=>$this->input->post('payment_method_cheque'),
			'terms'=>$this->input->post('terms')
		));
		$this->session->userdata('billing');
		$this->session->set_userdata('shipping',array(
		'no_ship'=>$this->input->post('ship_val')
		));
		$this->session->userdata('shipping');
	}
	
	function cancel_buynow(){
		$this->session->unset_userdata('buy_now');
        redirect(base_url());
	}
	
	function update_shipping(){
		
		$this->session->unset_userdata('shipping');
		$ship_id=$this->input->post('con_name');
		$this->session->set_userdata('ship_method', $ship_id);
		if($ship_id ==3){
			$l_id=$this->input->post('shipping_country');
			$shipping_city=$this->input->post('shipping_city');
			if($shipping_city){
				$location_id=$shipping_city;
			}else{
				$location_id='';
			}
			$method_id=$this->mcart->get_shippingname($ship_id,$location_id);
			$company_name=ship_conpany_byid($ship_id);
			$this->session->set_userdata('parcel_city',$shipping_city);
			$this->session->set_userdata('ship_cal',array(
				'company_name'=>$company_name,
				'method_id'=>$method_id,
				'shipping_value'=>103,
				'shipping_city'=>$shipping_city
			));
			//echo $this->city_bycountry($l_id,$shipping_city);
			
			$this->session->userdata('ship_cal'); 
		}else{
			$this->session->unset_userdata('parcel_city');
			$l_id=$this->input->post('shipping_country');
			$shipping_city=$this->input->post('shipping_city');
			
			$method_id=$this->mcart->get_shippingname($ship_id,$l_id);
			$company_name=ship_conpany_byid($ship_id);
			$this->session->set_userdata('ship_cal',array(
				'company_name'=>$company_name,
				'method_id'=>$method_id,
				'shipping_value'=>$l_id
			));
			$this->session->userdata('ship_cal'); 
		}
		$this->session->set_userdata('billing',array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'email'=>$this->input->post('billing_email'),
			'phone'=>$this->input->post('billing_phone'),
			'country'=>$this->input->post('calc_shipping_country'),
			'address'=>$this->input->post('billing_address_1'),
			'city'=>$this->input->post('calc_shipping_state'),
			'postcode'=>$this->input->post('billing_postcode'),
			'payment_method'=>$this->input->post('payment_method_cheque'),
			'terms'=>$this->input->post('terms')
		));
		$this->session->userdata('ship_method');
		$this->session->userdata('billing');
		//echo $this->input->post('shipping_country');
		$this->session->set_userdata('shipping_chose', array(
		'ship_chose'=>$this->input->post('shipping')
		));
		$this->session->userdata('shipping_chose');
	}
	
	function get_discount_by_coupon(){
		$post = $this->input->get();
		$coupon = $this->db->select('*')->from('sys_gift_cards')->where('coupon_code',$post['coupon_code'])->get()->row();
		if(empty($coupon)){
			header('HTTP/1.1 404 Bad Request');
			header('Content-Type: application/json; charset=UTF-8');
			$error = "Coupon ".$post['coupon_code']." does not exist !";
		    echo json_encode($error);
			exit;
		}	
		$this->session->set_userdata('coupon',array(
			'amount'   => $coupon->amount,
			'discount' => $coupon->discount
		));
		$this->session->userdata('coupon');
		echo json_encode($coupon);
	}
	
	function remove_coupon(){
		$this->session->unset_userdata('coupon');
	}
	
	function wishlist(){
		$this->session->unset_userdata('wishlist');
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$this->_data['wishlist']=$this->mcart->show_wishlist(get_user_id());
		$this->load->view('home/wishlist',$this->_data);
	}
	
	function ad_wishlist($rowid) {
		if(has_logged()){
			$data=array(
				'user_id'=>get_user_id(),
				'product_id'=>$rowid,
				'created_date'=> date('Y-m-d H:i')
			);
			$check_add_wishlist=$this->mcart->check_add_wishlist(get_user_id(),$rowid);
			if($check_add_wishlist){
				$this->session->set_flashdata('msg', 'You can\'t add the same product to wishlist');
			}else{
				$result=$this->mcart->save_wishlist($data);
			}
			
		}else{
			$this->session->set_flashdata('msg', 'Please login to add your producs to wishlist');
		}
		redirect(base_url().'wishlist');
	}
	
	function remove_wishlist($rowid) {
		$this->db->where(array(
			'product_id'=>$rowid,
			'user_id'=>get_user_id()
			));
		$this->db->delete('sys_wishlist');
		redirect(base_url().'wishlist');
	}
	
	function clear_wishlist() {
		$this->db->where(array( 
			'user_id'=>get_user_id()
			));
		$this->db->delete('sys_wishlist');
		redirect(base_url().'wishlist');
	}
	
	function compare(){
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$this->load->view('home/compare',$this->_data);
	}
	
	function ad_compare($rowid) {		
		$compare=$this->session->userdata('compare');
		if(count($compare)<3){
			if(empty($compare)){
				$this->session->set_userdata('compare',array(
						array('id'=>$rowid)
					)
				);
			}else{
				array_push($compare,array('id'=>$rowid));
				$this->session->set_userdata('compare',$compare);
			}
		}else{
			$this->session->set_flashdata('msg', 'You can\'t add more than three product...');
		}
		redirect(base_url().'compare');
	}
	
	function remove_compare($rowid) {
		$compare=$this->session->userdata('compare');
		$index = array_search($rowid, $compare);
		unset($compare[$index]);
		$this->session->set_userdata('compare', $compare);
		redirect(base_url().'compare');
	}
	
	function clear_compare() {
		$this->session->unset_userdata('compare');
		redirect(base_url().'compare');
	}
	
	function trackorder(){
		$this->load->view('home/track-your-order');
	}
	
	function track_order($order_id){
		$data_['order_id']=$order_id;
		$this->load->view('home/track-your-order',$data_);
	}
	
	function serch_trackorder(){
		
		$order_id = $this->input->post('orderid');
		$data['results'] = $this->mcart->search_order_id($order_id);
		$this->load->view('home/result_searching_trackorder',$data);
	}
	
	function get_billing($user_id) {
		$result= $this->db->select("sum(reward_point) as reward_point")->from("sys_billing")->where("user_id",$user_id)->get()->result();
		return $result;
	}
}
?>