<?php
class Register extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('email');
		$this->load->model(array('admin/CI_captcha','admin/muser'));
		$this->load->helper('captcha');
		$this->load->helper('password');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('category');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );
		
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}
	public function captcha_refresh(){
		$vals = array(
			'img_path' => './/images/captcha/', // PATH for captcha ( *Must mkdir (htdocs)/captcha )
			'img_url' => base_url().'/images/captcha/', // URL for captcha img
			'img_width' => 200, // width
			'img_height' => 50, // height
			'font_path'     => '../system/fonts/Humanistic.ttf',
			'expiration' => 3600
		);
		// Create captcha
		$cap = create_captcha( $vals );
		// Write to DB
		if ( $cap ) {
			$data = array(
			'captcha_id' =>'',
			'captcha_time' => $cap['time'],
			'ip_address' => $this -> input -> ip_address(),
			'word' => $cap['word'] ,
			);
			$query = $this -> db -> insert_string( 'kp_captcha', $data );
			$this -> db -> query( $query );
		}else {
			return "Captcha not work" ;
		}
			echo $cap['image'] ;
        
    }
	public function active($id){
		if(empty($id)){
			redirect(account_url());
		}else{
			$this->db->select('Activecode');
			$this->db->from('sys_users');
			$this->db->where('Activecode',$id);
			$active=$this->db->get();
			$get_activeCode = $active->first_row('array');
			$activeCode=$get_activeCode['Activecode'];
			if(!empty($activeCode)){
				$SQLdata = array(
					'user_active'=>1,
					'Activecode'=>''
				);
				$result = $this->muser->activate($id,$SQLdata);
				if($result){
                    $this->session->set_flashdata('msg','Congratulation! your account has been activated.');
					redirect(account_url());
				}
				
			}else{
				redirect(account_url());
			}
		}
		
	}
	/*
	public function index(){
		$data['members'] = $this->muser->member_package();
		$this->load->view('home/package', $data);
	}
	public function user_info($id=null){
		if(has_logged()){
			redirect(base_url());
		}
		$package_id=check_package($id);
	*/
	public function index(){

		if(has_logged()){
			redirect(base_url());
		}
		$this->session->set_userdata('fields', array(
													'username'=>$this->input->post('username'), 
													'email'=>$this->input->post('email'),
													'password'=>$this->input->post('password'),
													'password_confirm'=>$this->input->post('password_confirm'),
													'checkout'=>$this->input->post('checkout')
													)
									);
		$package_id=1;
		$sub_data['cap_img'] = $this ->CI_captcha->make_captcha();
		$sub_data['vender'] = $this->muser->get_vender();
		$sub_data['billing'] = $this->muser->get_billing();
		$this->form_validation->set_rules('username','username','required|trim|xss_clean|max_length[20]');
		//$this->form_validation->set_rules('checkout','checkout','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('email','email','required|trim|xss_clean|valid_email|max_length[50]|is_unique[sys_users.email]', array(
                        'is_unique' => 'E-mail already registered!')
                        );
		$this->form_validation->set_rules('password','password','required|trim|xss_clean|min_length[8]|max_length[20]|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm','password_conf','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('captcha', 'Captcha', 'required|xss_clean');
		$this->form_validation->set_rules('checkout','checkout','required');
		
		if ($this->form_validation->run() == FALSE){
		//	$sub_data['errors']=validation_errors();
		}else{
			if($this->input->post()){

			$password=$this->input->post('password', true);
			if(checkpassword($password)) {
				$sub_data['errors']=checkpassword($password);
			}else{

				if($this->CI_captcha->check_captcha()==TRUE){
             
					$toemail = $this->input->post('email', true);
					$From =get_option_value('email');
					$ActiveCode=actived_email($toemail);
					$resetCode=reset_code($toemail);
					$MsgContent='
                         <div>
							<div>Dear User,</div>
							<div style="padding:10px 10px 10px 0;color:#365ab5;font-size:16px;">Welcome To Angkorstores.com!</div>
							<div style="padding:0px 10px 10px 0;text-align: justify;">
								Angkorstores.com is the E-Commerce website opened worldwide and based in Cambodia. The project is invested by Mom Varin Investment (www.varinholdings.com), which has its affiliated companies – V Japan Tech Inter (www.vjpti.com) and Asia-Pacific Fredfort International School (www.afisedu.com). V Japan Tech Inter is the leading company of LPG and fuel equipment and services while Asia-Pacific Fredfort International School is an IB PYP Curriculum School in Cambodia. 

							</div>
                        <div style="padding:10px 10px 0 0;">Your user name: '.$toemail.'</div>
						<div style="padding:10px 10px 0 0;">Your Password: ******</div>
						</div>
<p>
						To activate your account, click <a href="'.account_url().'register/active/'.$ActiveCode.'">here</a></p>
						
					';
					$MsgContent.='<div style="padding:0px 10px 0 0;color:#375BB5;font-size:14px;border-bottom:1px solid #375BB5;padding-bottom:10px;margin-top:20px;">
						Best Regards,
					</div>
					<table width="100%">
						<tbody>
						<tr>
							<td width="160">
								<img width="150" src="'.base_url().'images/logo.png">
							</td>
							<td>
								<font color="#375BB5">
								Address: #40, St 348 Sangkat Toul Svay Prey I,<br>
								Khan Chamkarmon, Phnom Penh, Cambodia<br>
												Tel: (855) 23 555 500 8 | (855) 23 969 278 <br>
								E-mail: <u>customerservice@angkorstores.com</u><br>
								Website: <u>www.angkorstores.com</u>
								</font>
							</td>
						</tr>
						<tr>
							<td colspan="2">
							
							</td>
						</tr>
					</table>';
					
					$this->email->from($From);
					$this->email->to($toemail);
					$this->email->subject("Actived your Account");
					$this->email->message($MsgContent);
					$this->email->set_mailtype('html');
				/*	$register_type=$this->input->post('checkout', true);
					if($register_type !=1){
						$get_user_type=$this->input->post('checkout', true);
					}else{
						$get_user_type=3;
					}*/
					//$get_user_type=6;
					
					$get_user_type=$this->input->post('checkout',true);			
					
					
					if($get_user_type == 2 || $get_user_type == 6) {
						if($this->email->send()){ 
						    				
							$SQLDATA = array(
								  "AccountID" =>userID(),
								  "username" => $this->input->post('username', true),
								  "password" => create_hash($this->input->post('password', true)),
								  "email" => $this->input->post('email', true),
								  //"group_id" =>$get_user_type,
								  'group_id' => $get_user_type,
								  'Activecode'=>$ActiveCode,
								  'ResetCode'=>$resetCode,
								  'package_id'=>$package_id,
								  'iscomplete'=>'no',
								  "created_date" => date('Y-m-d H:i')
							);
							$result = $this->muser->save($SQLDATA);
							if($result){
							        $data = array(
								  "user_id" => $result,
								  "create_date" => date('Y-m-d H:i')
								);
								$result=$this->muser->saveShop($data);
								
								$sub_data['success'] = "Thank you for your registration! Please check your E-mail to activate your account.";
								$this->session->set_flashdata('msg',$sub_data['success']);
								redirect(account_url());
							}else{
								$sub_data['errors'] ="Fail to send try again later.";	
							}
												
						}else{
							$sub_data['errors'] = "Sorry Unable to send email...";
						}
					
					}else{
					$sub_data['errors'] = "<strong>Wrong!</strong> Authentication ! .Please try again.";			
								$this->session->set_flashdata('msg',$sub_data['errors']);
					
						
					}
				}else{
					$sub_data['errors'] = "invalid_captcha";
				}
			}
			}
		}
		$sub_data['user_role']= $this->muser->users_role();
		$sub_data['user_info']= $this->session->userdata('fields');
		$this->load->view('admin/register',$sub_data);
	}
		//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('first_name',word_r('first_name'),'required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('last_name',word_r('last_name'),'required|trim|xss_clean|max_length[20]');
		//$this->form_validation->set_rules('email',word_r('email'),'required|trim|xss_clean|valid_email|max_length[50]|is_unique[sys_user_detail.email]');
		//	$this->form_validation->set_rules('company',word_r('company'),'required|trim|xss_clean|max_length[20]');
		//	$this->form_validation->set_rules('businessType',word_r('businessType'),'required|trim|xss_clean|max_length[20]');

		$this->form_validation->set_rules('street',word_r('street'),'required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('district',word_r('district'),'required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('city',word_r('city'),'required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('country',word_r('country'),'required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('countryCode',word_r('countryCode'),'trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('phoneNumber',word_r('phoneNumber'),'required|trim|xss_clean|max_length[20]');
		//$this->form_validation->set_rules('fax',word_r('fax'),'trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('companyInfo',word_r('companyInfo'),'trim|xss_clean|max_length[2000]');
		if($this->form_validation->run() == FALSE){
	
			return validation_errors();
		}else{
			return true;
		}
	}
	private function _validation_create_guest(){
		$this->form_validation->set_rules('first_name','First Name','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('last_name','Last name','required|trim|xss_clean|max_length[20]');

		if($this->session->userdata('group_id')== 2 ){
		$this->form_validation->set_message('is_unique', '%s is already taken. Please try another one!');
		$this->form_validation->set_rules('company','Company Name','required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('page_name','Page Name','required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('info','Company Profile','required|trim|xss_clean');
		
		}elseif ($this->session->userdata('group_id')== 1) {
			$this->form_validation->set_rules('company','Company Name','required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('page_name','Page Name','required|trim|xss_clean|max_length[200]');
		}
		$this->form_validation->set_rules('city','City','required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('country','Country','required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('zip','Zip','trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('phone','Phone Number','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('description','Description','trim|xss_clean|max_length[2000]');
		if($this->form_validation->run() == FALSE){
			echo validation_errors();
		//	return false;
		}else{
			return true;
		}
	}
	function page_name(){
		$url=$this->input->post('company_name');
		$date=date("ymi");
		$symbol = array('.','!', '&', '?', '/', '/\/', ':', ';', '#', '<', '>', '=', '^', '@', '~', '`', '[', ']', '{', '}');
		$newsym=str_replace($symbol,"",$url);
		if($newsym>20){
			$newtitle=character_limiter($newsym,20); // First 6 words
		}else{
			$newtitle=$newsym;
		}
		$urltitle=preg_replace('/[^A-Za-z0-9\-]/','',$newtitle);
		$newurltitle=str_replace(" ","",$urltitle);
		
		$this->db->select('page_name');
		$this->db->from('sys_users_detail');
		$this->db->where('page_name', $newurltitle);
		$query = $this->db->get();
		$data=$query->first_row('array');
		if($data){
			//$new_url=$newurltitle.$date; // Final URL
			$bool =1;
			echo $bool;
		}else{
			$new_url=$newurltitle; // Final URL
			echo $new_url;
		}	
		//echo $new_url;
	}

	function info(){
	 	logged_only();
		
	 	$this->_variable();
	 	$user_id=get_user_id();
	 	$sub_data="";
		$pageExist = $this->input->post('page-exist');
		/*var_dump($this->input->post('shipcompany')); 
		var_dump($this->input->post('shipping_country'));
		var_dump($this->input->post('shipping_city'));
		exit;*/
		//$shipTo = null;
		$shipCountry = $this->input->post('shipping_country');
		$shipCity = $this->input->post('shipping_city');
		if($shipCountry != null && $shipCity == null){
			$shipTo = $shipCountry;
		}elseif($shipCity != null && $shipCountry == null){
			$shipTo = $shipCity;
		}
		else{
			$shipTo = null;
		}
	
		if(1==$pageExist){
			$this->session->set_flashdata('exist','Page Name is already taken!');
			$sub_data['exist'] = $this->session->flashdata('exist');
		}
		else{
			if($this->input->post()){
				//var_dump($shipTo); exit;
				
				if($this->_validation_create_guest()){
					//var_dump($this->input->post('page-exist')); exit;
					//---------featured image ----
					$image_featured = upload();
					if(!empty($image_featured)){
						$image_featured = date('m-Y').'/thumb/'.implode(',', $image_featured);
					}else{
						$image_featured = $this->input->post('feature');
					}


					$image_gallery = galleries();
					if(!empty($image_gallery)){
						foreach($image_gallery as $image_galleres){
							$img_galley[] = date('m-Y').'/thumb/'.$image_galleres; 					
						}
						$img_cover = implode(',',$img_galley);
					}else{
						$img_cover = $this->input->post('cover_image');
					}
					//var_dump($img_cover);exit();
					//$page_name=page_name($this->input->post('company'));
					$page_name=$this->input->post('page_name');

					$word = array('http://','https://');
					$wsite = $this->input->post('website');
					$website = str_replace($word,"", $wsite);
					$fb_page = $this->input->post('facebook_page');
					$facebook_page = str_replace($word,"",$fb_page);

					$data = array(
							  "user_id" => $user_id,
							  "first_name" => $this->input->post('first_name'),
							  "last_name" => $this->input->post('last_name'),
							  "company_name" => $this->input->post('company'),
							  "page_name" => $page_name,
							  "address" => $this->input->post('description'),
							  "info" => $this->input->post('info',true),
							  "zip" => $this->input->post('zip'),
							  "fax" => $this->input->post('fax'),
							  "image" => $image_featured,
							  "phone" =>$this->input->post('phone'),
							  "city" =>$this->input->post('city'),
							  "country" =>$this->input->post('country'),
							  "create_date" => date('Y-m-d H:i:s'),
							  "website" => $website,
							  "facebook_page" => $facebook_page, 
							  "shipcompany" => $this->input->post('shipcompany'), 
							  "ship_to" => $shipTo,
							  "cover_image" => $img_cover
					);

					###### find user_id in sys_user_detail #######
					$find_user_id = $this->muser->find_usID($user_id);
					if($find_user_id['user_id'] == null || $find_user_id['user_id'] == ''){
						$result = $this->muser->add_UserId_Detail($data);
						
					}else{
						
						$result=$this->muser->updateUser($user_id,$data);
					}
					
					if($result){
						
						$SQLdata = array(
							'iscomplete'=>'yes'
						);
						$this->muser->complete($user_id,$SQLdata);
						$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
											Your profile has been updated!
										</div>');
					}else{
						
						$this->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissable">
											<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
											<strong>Update failed!</strong> Try again later.
										</div>');
					}
						
				}
			}
		}
		$sub_data['get_user'] = $this->muser->getuser($user_id);
		$sub_data['get_country'] = $this->muser->country();
		$sub_data['get_ship_com'] = $this->muser->shipCompany();
	 	$sub_data['data']=$this->muser->find_profile_id($user_id);
		$sub_data['rmsg']=$this->session->flashdata('msg');
	 	$this->load->view('admin/guest_info',$sub_data);	
	}
	function reset_pwd(){
        if(has_logged()){
			redirect(base_url());
		}
		$this->_data['cap_img'] = $this ->CI_captcha->make_captcha();
		$this->form_validation->set_rules('email',word_r('email'),'required|trim|xss_clean|valid_email|max_length[50]');
		$this->form_validation->set_rules('captcha', 'Captcha', 'required|xss_clean');
		if ($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			$this->load->view('admin/users/reset_password',$this->_data);
		}else{
			
				if($this->CI_captcha->check_captcha()==TRUE){
					
					$email= $this->input->post('email');
					
					if(check_email($email)==null){
						$this->_data['errors'] = '<div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <strong>Sorry,</strong> this account is not in our records, please try again.
  </div>';
						
						$this->session->set_flashdata('msg',$this->_data['errors']);
						
						redirect(base_url().'admin/register/reset_pwd');
							
					}else{
						$resetCode=resetCode($email);
						
						//var_dump($resetCode);exit();
						// $get_from= get_option_value('email');
					    //$From =$get_from['description'];
					    $From = get_option_value('email');
						$MsgContent='
							 <div>
								<div>Dear, User</div>
								<div style="padding:10px 10px 10px 0;color:#365ab5;font-size:16px;">Welcome to Angkorstores!</div>
								<div style="padding:0px 10px 10px 0;text-align: justify;">
									Angkorstores.com is the E-Commerce website opened worldwide and based in Cambodia. The project is invested by Mom Varin Investment (www.varinholdings.com), which has its affiliated companies – V Japan Tech Inter (www.vjpti.com) and Asia-Pacific Fredfort International School (www.afisedu.com). V Japan Tech Inter is the leading company of LPG and fuel equipment and services while Asia-Pacific Fredfort International School is an IB PYP Curriculum School in Cambodia. 
									<div style="padding:10px 10px 10px 0;color:#365ab5;font-size:16px">You have requested for a password reset!</div>
								</div>
							</div>
							<p>
							<a href="'.account_url().'register/recovery/'.$resetCode.'">Click here<a> to reset your password.
							</p>
							<div style="padding:10px 10px 0 0;color:#375BB5;font-size:14px;border-bottom:1px solid #375BB5;padding-bottom:10px;margin-top:20px;">
								Best Regards,
							</div><br/>
							<table width="100%">
								<tbody>
								<tr>
									<td width="160">
										<img width="150" src="'.base_url().'images/logo.png">
									</td>
									
									<td>
										<font color="#375BB5">
											Address: #40, St 348 Sangkat Toul Svay Prey I,<br>
											Khan Chamkarmon, Phnom Penh, Cambodia<br>
															Tel: (855) 23 555 500 8 | (855) 23 969 278 <br>
											E-mail: <u>customerservice@angkorstores.com</u><br>
											Website: <u>www.angkorstores.com</u>
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="2">
						
									</td>
								</tr>
							</table>
						';
						$this->email->from($From);
						$this->email->to($email);
						$this->email->subject("Reset your password");
						$this->email->message($MsgContent);
						$this->email->set_mailtype('html');
						if($this->email->send()){ 
							$this->_data['success'] = "Please check your email to recovery.Good luck ):";
							$this->session->set_flashdata('msg',$this->_data['success']);
							redirect(account_url());
						}else{
						
						
							$this->_data['errors'] ="Fail to send try again later.";	
							
							$this->session->set_flashdata('msg',$this->_data['errors']);
							redirect('admin/register/reset_pwd');
						}
					}
				}else{
					$this->_data['errors'] = '<div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <strong>Sorry,</strong> Wrong captcha code. Please try again!
  </div>';
					$this->session->set_flashdata('msg',$this->_data['errors']);	
					
					redirect(base_url().'admin/register/reset_pwd');	
					
							
				}
			}
		
		$this->_data['cap_img'] = $this ->CI_captcha->make_captcha();
		//$this->load->view('admin/users/reset_password',$this->_data);
	}
	
	
	function recovery($id){
		$this->db->select('ResetCode,email');
		
		$this->db->from('sys_users');
		$this->db->where('ResetCode',$id);
		$active=$this->db->get();
		//var_dump($active);exit();
		$get_activeCode = $active->first_row('array');
		//var_dump($get_activeCode);exit();
		$ResetCode=$get_activeCode['ResetCode'];
		//var_dump($ResetCode);exit();
		if(empty($id) ||empty($ResetCode)){
			redirect(account_url());
		}
		else{
			$this->load->view('admin/users/recovery_password');
		}
	}

	function recov($id){
		$this->form_validation->set_rules('password','New Password','required');
		$this->form_validation->set_rules('password_confirm','Password Confirm','required');
		$this->form_validation->set_rules('email','Email','required');

		$this->db->select('ResetCode,email');
					
		
		$this->db->from('sys_users');
		$this->db->where('ResetCode',$id);
		$active=$this->db->get();
		
		
		//var_dump($active);exit();
		$get_activeCode = $active->first_row('array');
		
		//var_dump($get_activeCode);exit();
		$ResetCode=$get_activeCode['ResetCode'];
		//var_dump($ResetCode);exit();
	
		if($this->form_validation->run() == false){
			$this->_data['errors']='Fail Please confirm password again!';
			$this->load->view('admin/users/recovery_password');
		}else{
			$email= $this->input->post('email');
			
			$activeCode = $get_activeCode['email'];
			
			$password= $this->input->post('password');
			$confirm = $this->input->post('password_confirm');

			if($email != $activeCode ){
			
				$this->_data['errors']='<div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <strong>Sorry,</strong> Invalid email! Please check it again.</div>';
				
				$this->session->set_flashdata('msg',$this->_data['errors']);
				
				//echo $this->session->flashdata('msg');
				
				//redirect($this->agent->referrer());
				redirect('admin/register/recovery/'.$id);
			}else{
				if($confirm == $password){
					$data = array('password' => create_hash($password));
					$result = $this->muser->recove($id,$email,$data);
					if ($result) {
						$this->_data['success']='Congratulation! .Your password has been reset.';
						$this->session->set_flashdata('msg',$this->_data['success']);
						redirect('admin');
					}else{
						$this->_data['errors'] = '<div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <strong>Sorry,</strong> .Reset password fail, please try again.</div>';
                       				$this->session->set_flashdata('msg',$this->_data['errors']);
                        			redirect('admin/register/recovery'.$id);
					}
				}else{
					$this->_data['errors']='<div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <strong>Sorry,</strong> Fail Please confirm password again!</div>';
					$this->session->set_flashdata('msg',$this->_data['errors']);
					
					//redirect($this->agent->referrer());
					redirect('admin/register/recovery/'.$id);
				}						
			}
		}
	}
	/*
	function recovery($id){
		$this->form_validation->set_rules('password',word_r('password'),'required|trim|xss_clean|min_length[8]|max_length[20]|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm',word_r('password_confirm'),'required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('email',word_r('email'),'required|trim|xss_clean|valid_email|max_length[50]');
		$this->db->select('ResetCode,email');
		$this->db->from('sys_users');
		$this->db->where('ResetCode',$id);
		$active=$this->db->get();
		$get_activeCode = $active->first_row('array');
		
		//var_dump($get_activeCode);exit();
		$ResetCode=$get_activeCode['ResetCode'];
		
		//var_dump($id);var_dump($ResetCode);exit();
		if(empty($id) ||empty($ResetCode)){
			redirect(account_url());
		}else{
			if($this->input->post()){
				$email= $this->input->post('email', true);
				$password= $this->input->post('password', true);
				if($email != $get_activeCode['email']){
					$this->_data['errors']='Invalid email! Please check it again.';
				}else{
					if(!checkpassword($password)){
						$resetCode=md5(date('YmHisa').$email);
						if ($this->form_validation->run() == FALSE){
							$this->_data['errors'] = validation_errors();
						}else{
							$SQLdata = array(
								'user_password'=>create_hash($this->input->post('password', true)),
								'ResetCode'=>$resetCode,
								'modified_date'=> date('Y-m-d H:i')
							);
							$result = $this->muser->recovery($email,$id,$SQLdata);
							if($result){
								$this->_data['success'] = "Congratulation! .You successful reset password, please login.";
                                                                $this->session->set_flashdata('msg',$this->_data['success']);
								redirect(account_url());
							}else{
								$this->_data['errors']='Fail Please try again later.';
							}
						}
					}else{
						$this->_data['errors']=checkpassword($password);
					}
				}
			}
			$this->_data['id']=$id;
			$this->load->view('admin/Users/recovery_password',$this->_data);
		}
	}
	
	*/
	
	
	function profile(){
		logged_only();
		$user_id=get_user_id();
		$userRole=rolename($user_id);
		$this->_data['data']=$this->muser->find_profile_id($user_id);
		if($userRole=='Administrator' || $userRole=='Shop'){
			if($this->input->post()){
				if($this->_validation_create()){
					$user_id=get_user_id();
					//---------featured image ----
					$image_featured = upload();
					if(!empty($image_featured)){
						$image_featured = date('m-Y').'/thumb/'.implode(',', $image_featured);
					}else{
						$image_featured = '';
					}
					//---------gallery ----
					$image_gallery = galleries();
					if(!empty($image_gallery)){
						$image_gallery = date('m-Y').'/thumb/'.implode(',', $image_gallery);
					}else{
						$image_gallery = '';
					}
					$page=makeitseo($this->input->post('company', true));
					if(check_exist_company($page) !=""){
						$this->_data['errors']=check_exist_company($page);
					}else{
						$SQLDATA = array(
						  "FirstName" => $this->input->post('first_name', true),
						  "LastName" => $this->input->post('last_name', true),
						  "email" => $this->input->post('email', true),
						  "profile" =>$image_featured,
						  "CompanyName" =>makeitseo($this->input->post('company', true)),
						  'licence'=>$image_gallery,
						  "BusinessType" =>$this->input->post('businessType', true),
						  "countryCode" =>$this->input->post('countryCode', true),
						  "Phone" =>$this->input->post('phoneNumber', true),
						  "Fax" =>$this->input->post('fax', true),
						  "street" =>$this->input->post('street', true),
						  "district" =>$this->input->post('district', true),
						  "city" =>$this->input->post('city', true),
						  "country" =>$this->input->post('country', true),
						  "description" =>$this->input->post('companyInfo', true),
						  "modifiedComplete_date" => date('Y-m-d H:i')
						);
						$result = $this->muser->updateShop($user_id,$SQLDATA);
						if($result==true){
							$this->_data['success'] = "Congratulation !.You successful update.";
						}
					}
					$this->load->view('admin/users/shop_profile',$this->_data);
				}
			}	
			$this->load->view('admin/users/shop_profile',$this->_data);
		}elseif($userRole=='Guest'){
			if($this->input->post()){
				if($this->_validation_create_guest()){
					$user_id=get_user_id();
					//---------featured image ----
					$image_featured = upload();
					if(!empty($image_featured)){
						$image_featured = implode(',', $image_featured);
					}else{
						$image_featured = '';
					}
					$page=makeitseo($this->input->post('first_name', true).'.'.$this->input->post('last_name', true));
					if(check_exist_company($page) !=""){
						$sub_data['errors']=check_exist_company($page);
					}else{
						$SQLDATA = array(
						  "FirstName" => $this->input->post('first_name', true),
						  "LastName" => $this->input->post('last_name', true),
						  "gender"=> $this->input->post('gender', true),
						  "profile" =>date('m-Y').'/thumb/'.$image_featured,
						  "CompanyName" =>makeitseo($this->input->post('first_name', true).'.'.$this->input->post('last_name', true)),
						  "countryCode" =>$this->input->post('countryCode', true),
						  "Phone" =>$this->input->post('phoneNumber', true),
						  "Fax" =>$this->input->post('fax', true),
						  "street" =>$this->input->post('street', true),
						  "district" =>$this->input->post('district', true),
						  "city" =>$this->input->post('city', true),
						  "country" =>$this->input->post('country', true),
						  "description" =>$this->input->post('companyInfo', true),
						  "modifiedComplete_date" => date('Y-m-d H:i')
						);
						$result = $this->muser->updateShop($user_id,$SQLDATA);
						if($result==true){
							$this->_data['success'] = "Congratulation !.You successful update.";
						}
					}
				}
			}
			$this->load->view('admin/users/guest_profile',$this->_data);
		}else{
			redirect(account_url());
		}
	}
}
?>
