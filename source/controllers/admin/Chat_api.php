<?php
defined('BASEPATH') OR exit ('No direct access allowed');

/**
 *
 */
class Chat_api extends CI_Controller
{
  public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Chat_model');
	}
  public function send_message()
{
  $message = $this->input->get('message', null);
  $nickname = $this->input->get('nickname', '');
  $conversation_id = $this->input->get('conversation_id', '');
  $send_by = $this->input->get('send_by', '');
  $this->Chat_model->add_message($message, $nickname, $conversation_id,$send_by);

  $this->_setOutput($message);
}


public function get_messages()
{
  $timestamp = $this->input->get('timestamp', null);
  $conversation_id = $this->input->get('conversation_id', '');
  $messages = '';

  $messages = $this->Chat_model->get_messages($conversation_id, $timestamp);

  $this->_setOutput($messages);
}

public function get_conversations_list()
{
  $messages ='';

  $messages = $this->Chat_model->get_conversations_list();

  $this->_setOutput($messages);
}

public function get_messages_admin()
{
  $timestamp = $this->input->get('timestamp', null);
  $messages = '';

  $messages = $this->Chat_model->get_messages_admin( $timestamp);

  $this->_setOutput($messages);
}
public function retrieve_thread()
{
  $user_1 = $this->input->get('user_1', null);
  $user_2 = $this->input->get('user_2', null);
  $result = $this->Chat_model->get_existing_conversations($user_1,  $user_2);
  if (!$result) {
    $q = $this->Chat_model->create_conversation($user_1, $user_2);
    $result = array('conversation_id' => (string) $q, );
  }

  $this->_setOutput($result);
}


private function _setOutput($data)
{
  header('Cache-Control: no-cache, must-revalidate');
  header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  header('Content-type: application/json');

  echo json_encode($data);
}
}
