<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Procategory extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/category/mcategory');
		$this->load->model('admin/product/moproduct');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
		
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mcategory->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('procategory');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );
		
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}
	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('category_name',word_r('category_name'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('category_name',word_r('category_name'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}
	private function _child_category($parent_id,$category_group_id=2,$level){
		$user_id=get_user_id();
		//------------role-------
		$userRole=rolename($user_id);
		
		$this->db->select();
		$this->db->from('tbl_categories');
		$this->db->where(array('parent_id'=>$parent_id,'category_group_id'=>$category_group_id));
		$this->db->order_by('category_id','DESC');
		$result = $this->db->get();
		$category_array = $result->result_array();
		$output ="";
		foreach ($category_array as $value) {
			$category_id = $value['category_id'];
			$category_name = $value['category_name'];
			$category_slug = $value['category_slug'];
			$description = $value['description'];
			$im_caid= $userRole =='Admin' ? $category_id : "";
			$output .= '<tr>';

			$output .= '<td>'.form_checkbox(array('name'=>'id[]','value'=>$category_id,'class'=>'checkboxes')).$im_caid.'</td>';

			$output .='<td';
			$output .=' id="'.$category_id.'" class="opt-child" >';
			$output .= str_repeat(" __ ", $level);
			$output .= $category_name.'</td>';


			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $description.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $category_slug.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= btn_action($this->_controller_url.'update/'.$category_id, $this->_controller_url.'delete/'.$category_id, $this->_controller_url.'discount/'.$category_id).'</td>';

			$output .=$this->_child_category($category_id,$category_group_id,$level+1);
			$output .= '</tr>';
		}
		return $output;
	}

	public function index($name="category",$category_group_id=4){
		$user_id=get_user_id();
		//------------role-------
		$userRole=rolename($user_id);
		
		$category_group_id = category_type('Product');
		$this->_variable();
		$per_page = 2;
		//$page = $this->input->get('page');
		//$page = real_int($page);
		$this->db->select();
		$this->db->from('tbl_categories');
		$this->db->where(array('parent_id'=>0,'category_group_id'=>$category_group_id));
		$this->db->order_by('category_id','DESC');
		//$this->db->limit(2,0);
		$this->db->limit($per_page,$this->mcategory->start);
		$result = $this->db->get();
		$category_array = $result->result_array();
		$output ="";
		$output .='<table class="table table-bordered table-hover sort-table" name="'.$name.'">';
		$output .= '<thead><tr>';
				$output .= '<th>'.$this->_data['check_ml'].'</th>';
				$output .= '<th>'.word_r('category_name').'</th>';
				$output .= '<th>'.word_r('description').'</th>';
				$output .= '<th>'.word_r('slug').' </th>';
				$output .= '<th>'.word_r('action').' </th>';
		$output .= '</tr></thead>';
		foreach ($category_array as  $value) {
			$category_id = $value['category_id'];
			$category_name = $value['category_name'];
			$category_slug = $value['category_slug'];
			$description = $value['description'];
			$im_caid= $userRole =='Admin' ? $category_id : "";
			$output .='<tr>';

			$output .= '<td>'.form_checkbox(array('name'=>'id[]','value'=>$category_id,'class'=>'checkboxes')).$im_caid.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $category_name.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $description.'</td>';


			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $category_slug.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= btn_action($this->_controller_url.'update/'.$category_id, $this->_controller_url.'delete/'.$category_id, $this->_controller_url.'discount/'.$category_id).'</td>';
			$output .= $this->_child_category($category_id,$category_group_id,$level=1);
			$output .='</tr>';
		}
		$output .='</table>';
		$this->_data['data'] = $output;
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->mcategory->count_all = true;
		$this->mcategory->per_page = $per_page;
		$this->_data['showing'] = $this->mcategory->showing($category_array);
		$this->_data['page'] = $this->mcategory->page($base_url,$this->mcategory->count_all());
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);

	}

	//************************ Create ***************************//
	public function create(){
		$this->_variable();
		if($this->input->post()){
			if($this->_validation_create()){
				$slug = slug($this->input->post('category_name', true) );
				$SQLDATA = array(
							'category_name'=> $this->input->post('category_name', true),
							'category_slug'=> $slug,
							'description'=> $this->input->post('description', true),
							'parent_id'=> real_int($this->input->post('parent') ),
							'category_group_id'=> category_type('Product')
				);
				$result = $this->mcategory->save($SQLDATA);

				if($result){
					// update category_slug if exist 
					$category_group_id = category_type('Product');
					$this->db->select('category_slug')->from('tbl_categories')->where(array('category_slug'=>$slug,'category_group_id'=> $category_group_id,'category_id !='=>$result));
					$query = $this->db->get();
					$count = $query->num_rows();
					if($count > 0){
						$this->db->where('category_id',$result);
						$this->db->update('tbl_categories',array('category_slug'=>$slug.'-'.$result));
					}
					// update category_slug if exist 
					$this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    New category has been created successfully!
												  </div>');
					redirect(base_admin_url().$this->_controller_url);
				}
			}			
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);		
	}

	//************************ Create ***************************//

	//************************ Update **************************//
	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mcategory->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				if($this->_validation_update()){
					$slug = slug($this->input->post('category_name', true) );
					$SQLDATA = array(
								'category_name'=> $this->input->post('category_name', true),
								'category_slug'=> $slug,
								'description'=> $this->input->post('description', true),
								'parent_id'=> real_int($this->input->post('parent') ),
								'category_group_id'=> category_type('Product')
					);
					$result = $this->mcategory->update($id,$SQLDATA);

					if($result){
						// update category_slug if exist 
						$category_group_id = category_type('Product');
						$this->db->select('category_slug')->from('tbl_categories')->where(array('category_slug'=>$slug,'category_group_id'=> $category_group_id,'category_id !='=>$id));
						$query = $this->db->get();
						$count = $query->num_rows();
						if($count > 0){
							$this->db->where('category_id',$id);
							$this->db->update('tbl_categories',array('category_slug'=>$slug.'-'.$id));
						}
						$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    The category has been updated successfully!
												  </div>');
						
						// update category_slug if exist 
						redirect(base_admin_url().$this->_controller_url);
					}
				}
			endif;
		  $this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	//************************ Update **************************//

	// Multiple Action
	public function ml(){
		$action = $_POST['ml'];
		$id = $_POST['id'];
		$option = '';
		if(!empty($action) && !empty($id)){
			switch ($action) {
				case 'delete':
					$option = 'delete';
					break;
				case 'inactive':
						$option = 0;
						break;
			}

			
			if($option == 'delete'){
					//********* Check if delete category_id update parent_id to 0 *************//
					$this->db->where_in('parent_id',$id);
					$this->db->update('tbl_categories',array('parent_id'=>0));
					//********* Check if delete category_id update parent_id to 0 *************//

					//*********** update where category_id in tbl_links update to uncategorized *******//
					$this->db->where_in('category_id',$id);
					$this->db->update('tbl_links',array('category_id'=>1));
					//*********** update where category_id in tbl_links update to uncategorized *******//
			}
			

			//********* Check if delete category_id where id in //
			$this->db->where_in('category_id',$id);
			if($option == 'delete'){
				$this->db->delete('tbl_categories');
			}
			//********* Check if delete category_id where id in //

			if($this->db->affected_rows()){
				redirect(base_admin_url().$this->_controller_url);
			}
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
	}
	// Multiple Action

	public function delete($id = null){
		//Unable delete main category group
		if($id == 1){
			redirect(base_admin_url().$this->_controller_url);
		}
		$result = $this->mcategory->delete($id);
		if($result){
			//********* Check if delete category_id update parent_id to 0 *************//
			$this->db->where('parent_id',$id);
			$this->db->update('tbl_categories',array('parent_id'=>0));
			//********* Check if delete category_id update parent_id to 0 *************//
			
			//*********** update where category_id in tbl_links update to uncategorized *******//
			$this->db->where('category_id',$id);
			$this->db->update('tbl_links',array('category_id'=>1));
			//*********** update where category_id in tbl_links update to uncategorized *******//
			$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    The category has been deleted successfully!
												  </div>');
			redirect(base_admin_url().$this->_controller_url);
		}
		
		
	}

	//********************* Delete *****************//

	//********************** Search ***********************//
	public function search(){
		$this->_variable();
		$category_name = $this->input->get('category_name');
		$this->_data['data'] = $this->mcategory->search($category_name);
		$this->mcategory->count_all = true;
		$data = $this->mcategory->search($category_name);
		$base_url = base_admin_url().$this->_controller_url.'search?category_name='.$category_name.'&';
		$this->_data['showing'] = $this->mcategory->showing($data);
		$this->_data['page'] = $this->mcategory->page($base_url,$data);
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list_search', $this->_data);
	}
	
	///======= discount
	public function discount($id=null) {
		$supplier = $this->moproduct->get_supplier();
		$user_id=get_user_id();
		//------------role-------
		$userRole=rolename($user_id);
		
		$this->load->helper('admin_category');
		$category_group_id = category_type('Product');
		$this->_variable();
		$per_page = 100;
		
		$this->db->select("sys_product.*,username,discount_amount,from_date,todate,sale_price,regular_prices");
		$this->db->from('sys_relationship');
		$this->db->join("sys_product","sys_product.product_id=sys_relationship.post_id","left");
		$this->db->join("sys_inventory","sys_inventory.product_id=sys_relationship.post_id","left");
		$this->db->join("sys_users","sys_users.user_id=sys_product.supplier_id","left");
		$this->db->where('category_id',$id);
		$this->db->order_by('relationship_id','DESC'); 
		$this->db->limit($per_page,$this->mcategory->start);
		$result = $this->db->get();
		$category_array = $result->result_array(); 
		$output =""; 
		if($this->input->post("submit") == true){ 
			$discounts = $this->input->post("discount");
			$product_id = $this->input->post("product_id"); 
			 
			$data = array(); 
			 
			$fromDate =date('Y-m-d H:i:s', strtotime($this->input->post("from")." ".$this->input->post("from_dtime")));
			$toDate =date('Y-m-d H:i:s', strtotime($this->input->post("to")." ".$this->input->post("to_time")));   
			 
			foreach($discounts as $i => $dicount){
				$data[] = array(
						"product_id" => $product_id[$i],
						"discount_amount" => $discounts[$i],
						"have_discount"=> 1,
						"from_date"=>$fromDate,
						"todate"=>$toDate
  				);
			}   
			$result = $this->db->update_batch("sys_inventory",$data,"product_id"); 
			
			if($result) {
				$this->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
													<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
													Products discount successfully!
												  </div>');  
				redirect(base_admin_url().$this->_controller_url."discount/".$id);
			} 
		}   
		$this->_data['supplier'] = $supplier;
		$this->_data['data'] = $category_array; 
		$this->_data['id'] = $id; 
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->mcategory->count_all = true;
		$this->mcategory->per_page = $per_page;  
		$this->_data['showing'] = $this->mcategory->showing($category_array);
		$this->_data['page'] = $this->mcategory->page($base_url,$this->mcategory->count_all());
		
		$this->load->view(config_item('admin_dir').$this->_controller_url.'product_by_category', $this->_data);
	}  
	
	//********************** filter value to discount ***********************//
	public function filter_discount($id = false){		
		$supplier = $this->moproduct->get_supplier();
		$user_id=get_user_id();
		//------------role-------
		$userRole=rolename($user_id); 
		$this->load->helper('admin_category');
		$category_group_id = category_type('Product');
		$this->_variable();
		$per_page = 100; 
		$this->db->select("sys_product.*,username,discount_amount,from_date,todate,sale_price,regular_prices");
		$this->db->from('sys_relationship');
		$this->db->join("sys_product","sys_product.product_id=sys_relationship.post_id","left");
		$this->db->join("sys_inventory","sys_inventory.product_id=sys_relationship.post_id","left");
		$this->db->join("sys_users","sys_users.user_id=sys_product.supplier_id","left");
		$this->db->where('category_id',$id); 
		$between = $this->input->post("between");
		$toValue = $this->input->post("toValue"); 
		$postSupplier=$this->input->post("supplier"); 
			
			if($this->input->post("filter")=='sale_price') { 
				$this->db->where('sale_price >=', $between);
				$this->db->where('sale_price <=', $toValue); 
			}
			if($this->input->post("filter")=='regular_price') {
				$this->db->where('regular_prices >=', $between);
				$this->db->where('regular_prices <=', $toValue);
			}  
			if($postSupplier == true){ 
				$this->db->where('supplier_id', $postSupplier);
			}
			
		$this->db->order_by('relationship_id','DESC'); 
		
		$this->db->limit($per_page,$this->mcategory->start);
		$result = $this->db->get();
		$category_array = $result->result_array(); 
		
		$this->_data['supplier'] = $supplier;
		$this->_data['data'] = $category_array; 
		$this->_data['id'] = $id; 
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->mcategory->count_all = true;
		$this->mcategory->per_page = $per_page;  
		$this->_data['showing'] = $this->mcategory->showing($category_array);
		$this->_data['page'] = $this->mcategory->page($base_url,$this->mcategory->count_all());
		$this->load->view(config_item('admin_dir').$this->_controller_url.'product_by_category', $this->_data);
	}
	
}
