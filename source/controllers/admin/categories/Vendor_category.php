<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Vendor_category extends CI_Controller {
	
	function __construct()	{
		parent::__construct();
		vendor_only();
		acl($this->uri->uri_string);
		$this->load->helper('product');
		$this->load->model('admin/product/moproduct');	
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	function create(){
		$user_id = $this->session->userdata('user_id');
		$category = $this->input->post('categories');
		$count = count($category);
		for($i=0; $i<$count; $i++){
			$data = $category[$i];
			$parent_id = $this->moproduct->find_category($data);
			$parentId = $parent_id[0]['parent_id'];
			$result = $this->moproduct->find_catego($data,$user_id);
			//var_dump($result);exit();
			if($result == null){
				$data1 = array('category_id' => $category[$i],
							  'user_id'=>$user_id,
							  'parent_id'=>$parentId,
							  'created_date'=>date('Y-m-d H:i'),
							  'modified_date'=> date('Y-m-d H:i')
				);
				$this->moproduct->insert_category($data1);
			}
		}
		$this->load->view('admin/categories/vendor-category/create');
	}

}