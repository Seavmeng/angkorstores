<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/category/mcategory');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mcategory->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('category');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );
		
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}

	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('category_name',word_r('category_name'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('category_name',word_r('category_name'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	//*********************** Validation **************************//
	//private function _child_category($parent_id,$level){
	private function _child_category($parent_id,$category_group_id=2,$level){
		$this->db->select();
		$this->db->from('tbl_categories');
		$this->db->where('parent_id',$parent_id);
		//$this->db->where(array('parent_id'=>$parent_id,'category_group_id'=>$category_group_id));
		$this->db->order_by('category_id','DESC');
		$this->db->limit(15,$this->mcategory->start);
		$result = $this->db->get();
		$category_array = $result->result_array();
		$output ="";
		foreach ($category_array as $value) {
			$category_id = $value['category_id'];
			$category_name = $value['category_name'];
			$category_slug = $value['category_slug'];
			$description = $value['description'];
			$output .= '<tr>';

			$output .= '<td>'.form_checkbox(array('name'=>'id[]','value'=>$category_id,'class'=>'checkboxes')).'</td>';

			$output .='<td';
			$output .=' id="'.$category_id.'" class="opt-child" >';
			$output .= str_repeat(" * ", $level);
			$output .= $category_name.'</td>';


			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $description.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $category_slug.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= btn_action($this->_controller_url.'update/'.$category_id, $this->_controller_url.'delete/'.$category_id).'</td>';

			//$output .=$this->_child_category($category_id,$level+1);
			$output .=$this->_child_category($category_id,$category_group_id,$level+1);
			$output .= '</tr>';
		}
		return $output;
	}

	public function index($name="category"){
	//public function index($name="category",$category_group_id=2){
		$category_group_id=category_type('Product');
		$this->_variable();
		$per_page = 3;
		//$page = $this->input->get('page');
		//$page = real_int($page);
		
		$this->db->select();
		$this->db->from('tbl_categories');
		$this->db->where(array('parent_id'=>0,'category_group_id !='=>$category_group_id));
		$this->db->order_by('category_id','DESC');
		$this->db->limit($per_page,$this->mcategory->start);
		$result = $this->db->get();
		//var_dump($this->mcategory->count_all()); exit;
		$category_array = $result->result_array();
		$output ="";
		$output .='<table class="table table-bordered table-hover sort-table" name="'.$name.'">';
		$output .= '<thead><tr>';
				$output .= '<th>'.$this->_data['check_ml'].'</th>';
				$output .= '<th>'.word_r('category_name').'</th>';
				$output .= '<th>'.word_r('description').'</th>';
				$output .= '<th>'.word_r('slug').' </th>';
				$output .= '<th>'.word_r('action').' </th>';
		$output .= '</tr></thead>';
		foreach ($category_array as  $value) {
			$category_id = $value['category_id'];
			$category_name = $value['category_name'];
			$category_slug = $value['category_slug'];
			$description = $value['description'];
			$output .='<tr>';

			$output .= '<td>'.form_checkbox(array('name'=>'id[]','value'=>$category_id,'class'=>'checkboxes')).'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $category_name.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $description.'</td>';


			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $category_slug.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= btn_action($this->_controller_url.'update/'.$category_id, $this->_controller_url.'delete/'.$category_id);
			 
			$output .='</td>';
			
			//$output .= $this->_child_category($category_id,$level=1);
			$output .= $this->_child_category($category_id,$category_group_id,$level=1);
			$output .='</tr>';
		}
		$output .='</table>';
		$this->_data['data'] = $output;
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->mcategory->count_all = true;
		$this->mcategory->per_page = $per_page;
		$this->_data['showing'] = $this->mcategory->showing($category_array);
		$this->_data['page'] = $this->mcategory->page($base_url,$this->mcategory->count_all_places());
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	
	} 
	//************************ Create ***************************//
	public function create(){
		$this->_variable();
		if($this->input->post()){
			if($this->_validation_create()){
				$slug = slug($this->input->post('category_name', true) );
				$SQLDATA = array(
							'category_name'=> $this->input->post('category_name', true),
							'category_slug'=> $slug,
							'description'=> $this->input->post('description', true),
							'parent_id'=> real_int($this->input->post('parent') ),
							'category_group_id'=> $this->input->post('category_group')
					//		'category_group_id'=> sys_config('category_group_id')
				);
				$result = $this->mcategory->save($SQLDATA);

				if($result){
					// update category_slug if exist 
					$category_group_id = sys_config('category_group_id');
					$this->db->select('category_slug')->from('tbl_categories')->where(array('category_slug'=>$slug,'category_group_id'=> $category_group_id,'category_id !='=>$result));
					$query = $this->db->get();
					$count = $query->num_rows();
					if($count > 0){
						$this->db->where('category_id',$result);
						$this->db->update('tbl_categories',array('category_slug'=>$slug.'-'.$result));
					}
					// update category_slug if exist 
					$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    New Category has been created successfully!
												  </div>');
					redirect(base_admin_url().$this->_controller_url);
				}
			}			
		}
		$this->_data['group']=$this->mcategory->group();
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);		
	}

	//************************ Create ***************************//

	//************************ Update **************************//
	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mcategory->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				if($this->_validation_update()){
					$slug = slug($this->input->post('category_name', true) );
					$SQLDATA = array(
								'category_name'=> $this->input->post('category_name', true),
								'category_slug'=> $slug,
								'description'=> $this->input->post('description', true),
								'parent_id'=> real_int($this->input->post('parent') ),
								'category_group_id'=> 5
					);
					$result = $this->mcategory->update($id,$SQLDATA);

					if($result){
						// update category_slug if exist 
						$category_group_id = 5;
						$this->db->select('category_slug')->from('tbl_categories')->where(array('category_slug'=>$slug,'category_group_id'=> $category_group_id,'category_id !='=>$id));
						$query = $this->db->get();
						$count = $query->num_rows();
						if($count > 0){
							$this->db->where('category_id',$id);
							$this->db->update('tbl_categories',array('category_slug'=>$slug.'-'.$id));
						}
						// update category_slug if exist
						$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												   The Category has been updated successfully!
												  </div>');						
						redirect(base_admin_url().$this->_controller_url);
					}
				}
			endif;
			$this->_data['group']=$this->mcategory->group();
			$this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	//************************ Update **************************//
	//************************ Import **************************//
	public function importCatgories(){
		if($this->input->post()):
			$filename=$_FILES["file"]["tmp_name"];
			if($_FILES["file"]["size"] > 0){
				$file = fopen($filename, "r");
				
				$flag = true;
				$i=0;
				$err='The following product code(s) already exist: <br>';
				
				while (($Data = fgetcsv($file, 10000, ",")) !== FALSE){
					if($flag) { 
						$flag = false; continue; 
					}
					
					
					$parent_id=trim($Data[0]);
					$category_name=trim($Data[1]);
					$address=trim($Data[2]);
					$contact=trim($Data[3]);
					$email = trim($Data[4]);
					$website = trim($Data[5]);
					$category_group_id = 5;
					$status = 1;
					$slug = slug($category_name);
					
					
						if($email != '' && $website !='' && $contact !=''){
							$SQLDATA = array(
								'parent_id'=> $parent_id,
								'category_name'=> $category_name,
								'description'=> '<div>'. trim($address) . '</div><div>Tel: '. trim($contact) .'</div>'. ' <div>Email: '. trim($email). '</div><div>Website: <a target="_blank" href="http://'.$website.'">'.trim($website). '</a></div>',
								'category_slug' => $slug,
								'category_group_id' => $category_group_id,
								'status'=>$status
							);
						}elseif($email != '' && $contact != '' && $website ==''){
							$SQLDATA = array(
								'parent_id'=> $parent_id,
								'category_name'=> $category_name,
								'description'=> '<div>'. trim($address) . '</div><div>Tel: '. trim($contact) .'</div>'. ' <div>Email: '. trim($email). '</div>',
								'category_slug' => $slug,
								'category_group_id' => $category_group_id,
								'status'=>$status
							);
						}elseif($website != '' && $contact != '' && $email ==''){
							$SQLDATA = array(
								'parent_id'=> $parent_id,
								'category_name'=> $category_name,
								'description'=> '<div>'. trim($address) . '</div><div>Tel: '. trim($contact) .'</div>'. ' <div>Website: <a target="_blank" href="http://'.$website.'">'.trim($website). '</a></div>',
								'category_slug' => $slug,
								'category_group_id' => $category_group_id,
								'status'=>$status
							);
						}elseif($website != '' && $email != '' && $contact ==''){
							$SQLDATA = array(
								'parent_id'=> $parent_id,
								'category_name'=> $category_name,
								'description'=> '<div>'. trim($address) . '</div><div>Email: '. trim($email) .'</div>'. ' <div>Website: <a target="_blank" href="http://'.$website.'">'.trim($website). '</a></div>',
								'category_slug' => $slug,
								'category_group_id' => $category_group_id,
								'status'=>$status
							);
						}elseif($website == '' && $email =='' && $contact != ''){
							$SQLDATA = array(
								'parent_id'=> $parent_id,
								'category_name'=> $category_name,
								'description'=> '<div>'. trim($address) . '</div><div>Tel: '. trim($contact) .'</div>',
								'category_slug' => $slug,
								'category_group_id' => $category_group_id,
								'status'=>$status
							);
						}elseif($contact == '' && $email =='' && $website != ''){
							$SQLDATA = array(
								'parent_id'=> $parent_id,
								'category_name'=> $category_name,
								'description'=> '<div>'. trim($address) . '</div><div>Website: <a target="_blank" href="http://'.$website.'">'.trim($website). '</a></div>',
								'category_slug' => $slug,
								'category_group_id' => $category_group_id,
								'status'=>$status
							);
						}elseif($contact == '' && $website =='' && $email != ''){
							$SQLDATA = array(
								'parent_id'=> $parent_id,
								'category_name'=> $category_name,
								'description'=> '<div>'. trim($address) . '</div><div>Email: '. trim($email). '</div>',
								'category_slug' => $slug,
								'category_group_id' => $category_group_id,
								'status'=>$status
							);
						}else{
							$SQLDATA = array(
								'parent_id'=> $parent_id,
								'category_name'=> $category_name,
								'description'=> '<div>'. trim($address) .'</div>',
								'category_slug' => $slug,
								'category_group_id' => $category_group_id,
								'status'=>$status
							);
						}
	
						
						
						$result = $this->mcategory->save($SQLDATA);
						$i++;
						//var_dump($SQLDATA);
						if($result){
							$this->_data['success']= $i.' places have been successfully imported!';
						}
						
						
					//}
					
				} 
				
				
			}
				 
			
		endif;
		$this->load->view('admin/category/import', $this->_data);
	}
	//************************ Import **************************//
	

	// Multiple Action
	public function ml(){
		$action = $_POST['ml'];
		$id = $_POST['id'];
		$option = '';
		if(!empty($action) && !empty($id)){
			switch ($action) {
				case 'delete':
					$option = 'delete';
					break;
				case 'inactive':
						$option = 0;
						break;
			}

			
			if($option == 'delete'){
					//********* Check if delete category_id update parent_id to 0 *************//
					$this->db->where_in('parent_id',$id);
					$this->db->update('tbl_categories',array('parent_id'=>0));
					//********* Check if delete category_id update parent_id to 0 *************//

					//*********** update where category_id in tbl_links update to uncategorized *******//
					$this->db->where_in('category_id',$id);
					$this->db->update('tbl_links',array('category_id'=>1));
					//*********** update where category_id in tbl_links update to uncategorized *******//
			}
			

			//********* Check if delete category_id where id in //
			$this->db->where_in('category_id',$id);
			if($option == 'delete'){
				$this->db->delete('tbl_categories');
			}
			//********* Check if delete category_id where id in //

			if($this->db->affected_rows()){
				redirect(base_admin_url().$this->_controller_url);
			}
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
	}
	// Multiple Action

	public function delete($id = null){
		//Unable delete main category group
		if($id == 1){
			redirect(base_admin_url().$this->_controller_url);
		}
		$result = $this->mcategory->delete($id);
		if($result){
			//********* Check if delete category_id update parent_id to 0 *************//
			$this->db->where('parent_id',$id);
			$this->db->update('tbl_categories',array('parent_id'=>0));
			//********* Check if delete category_id update parent_id to 0 *************//
			
			//*********** update where category_id in tbl_links update to uncategorized *******//
			$this->db->where('category_id',$id);
			$this->db->update('tbl_links',array('category_id'=>1));
			//*********** update where category_id in tbl_links update to uncategorized *******//
			$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    The Category has been deleted successfully!
												  </div>');
			redirect(base_admin_url().$this->_controller_url);
		}
		
		
	}

	//********************* Delete *****************//


	//********************** Search ***********************//
	public function search(){
		$this->_variable();
		$category_name = $this->input->get('category_name');
		$this->_data['data'] = $this->mcategory->search($category_name);
		$this->mcategory->count_all = true;
		$data = $this->mcategory->search($category_name);
		$base_url = base_admin_url().$this->_controller_url.'search?category_name='.$category_name.'&';
		$this->_data['showing'] = $this->mcategory->showing($data);
		$this->_data['page'] = $this->mcategory->page($base_url,$data);
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list_search', $this->_data);

	}
	
	//********************** Search ***********************//

}
