
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Brands extends CI_Controller{

	function __construct()	{
		parent::__construct();
	//	$this->load->library('ion_auth');
		$this->load->helper('product');
		$this->load->model('admin/brands/mbrand');
		$this->_controller_url = $this->uri->segment(2).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	
	}
	function brand(){
		$data['brand'] = $this->mbrand->show_all();
		$this->load->view('admin/brands/show_brand',$data);
	}
	function create(){
		$this->form_validation->set_rules('bname','Brand Name','required');
		if($this->form_validation->run() == false){
			$this->_data['errors'] = validation_errors();
			$this->load->view('admin/brands/new_brand');
		}else{
				$image_featured = upload();
				if(!empty($image_featured)){
					foreach($image_featured as $image_feature){
						$image_features[] = date('m-Y').'/thumb/'.$image_feature; 					
					}
					$img_feature = implode(',',$image_features);
				}else{
					$img_feature = '';
				}
				$data = array('brand_name' =>$this->input->post('bname'),
							  'image' => $img_feature, 
							  'description' => $this->input->post('description'),
							  'created_date' => date('Y-m-d H:i'),
							  'modified_date' => date('Y-m-d H:i')
							 );
				$results = $this->mbrand->add_brand($data);
				if($results){
					$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    <strong>Success!</strong> New brand has been insert successfully!
												  </div>');
					redirect(base_url().'admin/brands/brand'); 
				 }else{
					$this->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    <strong>Warning!</strong> Can not add brand ! </div>'
												 );
					redirect(base_url().'admin/brands/brand');
				 }
		}
	}
	function update(){
		$id = $this->uri->segment(4);
		$data['update_br'] = $this->mbrand->show($id);
		//$this->load->view('admin/brands/update_brand',$data);
		$this->form_validation->set_rules('bname','Brand Name','required');
		if($this->form_validation->run() == false){
			$this->_data['errors'] = validation_errors();
			$this->load->view('admin/brands/update_brand',$data);
		}
		else{
			
			$image_featured = upload();
				if(!empty($image_featured)){
					foreach($image_featured as $image_feature){
						$image_features[] = date('m-Y').'/thumb/'.$image_feature; 					
					}
					$img_feature = implode(',',$image_features);
				}else{
					$img_feature = $this->input->post('feature',true);
				}
			$data1 = array('brand_name' =>$this->input->post('bname'),
							  'image' => $img_feature, 
							  'description' => $this->input->post('description'),
							  'created_date' => date('Y-m-d H:i'),
							  'modified_date' => date('Y-m-d H:i')
							 );
				
				$results = $this->mbrand->update($id,$data1);
				//var_dump($results);exit();
				//var_dump($id);exit();
				if($results){
					$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    <strong>Success!</strong> New brand has been insert successfully!
												  </div>');
					redirect(base_url().'admin/brands/brand'); 
				 }else{
					$this->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    <strong>Warning!</strong> Can not add brand ! </div>'
												 );
					redirect(base_url().'admin/brands/brand');
				 }
		}

	}
	function delete($id = null){
		$delete = $this->mbrand->delete_brand($id);
		if($delete){
			$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    <strong>Congratulation!</strong> your data hase been deleted ! </div>');
			redirect('admin/brands/brand');
		}else{
			$this->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    <strong>Warning!</strong> Delete faile ! </div>');
			redirect('admin/brands/brand');
		}
	}
}