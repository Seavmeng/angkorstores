<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller{

	public function __construct(){
		parent::__construct();
		//acl($this->uri->uri_string);
		$this->load->model('admin/user/muser');
		$this->load->helper('password');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	
	private $_controller_url = '';
	private $_data = array();

	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->muser->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('user');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-users';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'active'=>word_r('active'),
						'inactive'=>word_r('inactive')
				  );
		$option_search = array(
						''=>'----',
						'active'=>word_r('active'),
						'inactive'=>word_r('inactive')
				  );
		$role_filters = array(
							'' => '----',
							'1' => 'Admin',
							'2' => 'Vendor',
							'6' => 'Buyer'
							);
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
		$this->_data['role_filters'] = form_dropdown('role_filters', $role_filters, $this->input->get('role_filters'), 'class="form-control"');
		$this->_data['option_search'] = form_dropdown('user_active',$option_search,$this->input->get('user_active'),'class="form-control" ');
	}

	private function _validation_create(){
		$this->form_validation->set_rules('username',word_r('username'),'required|trim|xss_clean|is_unique[sys_users.username]');
		$this->form_validation->set_rules('email',word_r('email'),'required|trim|xss_clean|valid_email|is_unique[sys_users.email]');
		$this->form_validation->set_rules('password',word_r('password'),'required|trim|xss_clean|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm',word_r('password_confirm'),'required|trim|xss_clean');
        $this->form_validation->set_rules('user_group_id',word_r('user_group'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('username',word_r('username'),'required|trim|xss_clean');
		$this->form_validation->set_rules('email',word_r('email'),'required|trim|xss_clean|valid_email|callback_check_email');
        $this->form_validation->set_rules('user_group_id',word_r('user_group'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	public function index(){
		$this->_variable();
		$this->_data['data'] = $this->muser->join();
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->muser->count_all = true;
		$this->_data['showing'] = $this->muser->showing($this->muser->join());
		$this->_data['page'] = $this->muser->page($base_url,$this->muser->join());
		$this->load->view('admin/users/user/list', $this->_data);
	}

	public function create(){
		$this->_variable();
		if($this->input->post()){
			if($this->_validation_create()){
				$image = upload();
				if(!empty($image)){
					$image = implode(',', $image);
				}else{
					$image = '';
				}
				$SQLDATA = array(
					  //"first_name" => $this->input->post('first_name', true),
					  "AccountID" => userID(),
					  "username" => $this->input->post('username', true),
					  "password" => create_hash($this->input->post('password', true)),
					  "email" => $this->input->post('email', true),
					  "group_id" => $this->input->post('user_group_id', true),
					  "user_active" => $this->input->post('user_active', true),
					  "register_date" => date('Y-m-d H:i'),
				//	  "image" => $image,
					  "bio" => $this->input->post('bio', true)
				);
				$result = $this->muser->save($SQLDATA);
				if($result){
					redirect(base_admin_url().$this->_controller_url);
				}
			}			
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);		
	}

	public function update($id = null){
		$this->_variable();
		$result_password = '';
		$this->_data['data'] = $this->muser->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				$image = upload();
				if(!empty($image)){
					$image = implode(',', $image);
				}else{
					$image = $this->input->post('image', true);
				}
				//if(!empty($this->input->post('password'))){
					$this->form_validation->set_rules('password',word_r('password'),'trim|xss_clean|matches[password_confirm]');
					$this->form_validation->set_rules('password_confirm',word_r('password_confirm'),'trim|xss_clean');
					if($this->form_validation->run() == FALSE){
						$this->_data['errors'] = validation_errors();
					}else{
						$SQLDATA = array(
							  "password" => create_hash($this->input->post('password', true))
							  
						);
						$result_password = $this->muser->update($id,$SQLDATA);
					}
				//}

				if($this->_validation_update()){
						$SQLDATA = array(
							  //"first_name" => $this->input->post('first_name', true),
							  //"last_name" => $this->input->post('last_name', true),
							  "username" => $this->input->post('username', true),
							  "email" => $this->input->post('email', true),
							  "group_id" => $this->input->post('user_group_id', true),
							  "user_active" => $this->input->post('user_active', true),
							//  "image" => $image,
							  "bio" => $this->input->post('bio', true)
						);
						$result = $this->muser->update($id,$SQLDATA);
					if($result){
						$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    The user has been updated successfully!
												  </div>');
						redirect(base_admin_url().$this->_controller_url);
					}elseif($result_password){
                                                $this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												  <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    The user has been updated successfully!
												  </div>');
						redirect(base_admin_url().$this->_controller_url);
					}
				}
			endif;
		  $this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	public function ml(){
		$action = $_POST['ml'];
		$id = $_POST['id'];
		var_dump($id);
		$option = '';
		if(!empty($action) && !empty($id)){
			switch ($action) {
				case 'active':
					$option = 1;
					break;
				case 'inactive':
						$option = 0;
						break;
			}
			$this->db->where_in('user_id',$id);
			$this->db->update('sys_users',array('user_active'=>$option));
			if($this->db->affected_rows()){
				redirect(base_admin_url().$this->_controller_url);
			}
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
	}

	public function check_username($username){
		$id = $this->uri->segment(5);
		$this->db->select('username')->from('sys_users')->where(array('username' => $username,'user_id !=' => $id));
		$query = $this->db->get();
		$count = $query->num_rows(); 
		if($count > 0){
			$this->form_validation->set_message('check_username', 'The '.word_r('username').' field must contain a unique value');
			return false;
		}
	}

	public function check_email($email){
		$id = $this->uri->segment(5);
		$this->db->select('email')->from('sys_users')->where(array('email' => $email,'user_id !=' => $id));
		$query = $this->db->get();
		$count = $query->num_rows(); 
		if($count > 0){
			$this->form_validation->set_message('check_email', 'The '.word_r('email').' field must contain a unique value');
			return false;
		}
	}

	public function delete($id = null){

		$result = $this->muser->delete($id);
		if($result){
			redirect(base_admin_url().$this->_controller_url);
		}
		
		
	}

	public function search(){
		$this->_variable();
		
		$username = $this->input->get('username');
		$email = $this->input->get('email');
		$user_active = $this->input->get('user_active');
		$role = $this->input->get('role_filters');
		$this->_data['data'] = $this->muser->search($username, $email, $user_active, $role);
		$this->muser->count_all = true;
		$data = $this->muser->search($username, $email, $user_active, $role);
		$base_url = base_admin_url().$this->_controller_url.'search?username='.$username.'&email='.$email.'&user_active='.$user_active.'&role_filters='.$role.'&';
		$this->_data['showing'] = $this->muser->showing($data);
		$this->_data['page'] = $this->muser->page($base_url,$data);
		$this->_data['role'] = $role;
		
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);

	}

	public function update_vendor($param){
		//$this->load->model('admin/muser');
		$this->_data['supplier_info'] = $this->muser->getuser($param);
		$this->_data['get_country'] = $this->muser->country();
		$this->load->view(config_item('admin_dir').$this->_controller_url.'filter_vendor', $this->_data);
	}

}
