<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_group extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/user/muser_group');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}

	private $_controller_url = '';
	private $_data = array();

	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->muser_group->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('user_group');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-users';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'active'=>word_r('active'),
						'inactive'=>word_r('inactive')
				  );
		$option_search = array(
						''=>'----',
						'active'=>word_r('active'),
						'inactive'=>word_r('inactive')
				  );
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
		$this->_data['option_search'] = form_dropdown('user_group_active',$option_search,$this->input->get('user_group_active'),'class="form-control" ');
	}

	private function _validation_create(){
		$this->form_validation->set_rules('user_group_name',word_r('user_group_name'),'required|trim|xss_clean|is_unique[sys_user_groups.user_group_name]');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('user_group_name',word_r('user_group_name'),'required|trim|xss_clean|callback_check_user_group_name');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	public function index(){
		$this->_variable();

		$this->_data['data'] = $this->muser_group->all();
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->muser_group->count_all = true;
		$this->_data['showing'] = $this->muser_group->showing($this->muser_group->all());
		$this->_data['page'] = $this->muser_group->page($base_url,$this->muser_group->all());
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	}

	public function create(){
		$this->_variable();
		if($this->input->post()){
			if($this->_validation_create()){
				$SQLDATA = array(
					  "user_group_name" => $this->input->post('user_group_name', true),
					  "user_group_active" => $this->input->post('user_group_active', true)
				);
				$result = $this->muser_group->save($SQLDATA);
				if($result){
					redirect(base_admin_url().$this->_controller_url);
				}
			}			
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);		
	}

	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->muser_group->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				if($this->_validation_update()){
						$SQLDATA = array(
							  "user_group_name" => $this->input->post('user_group_name', true),
							  "user_group_active" => $this->input->post('user_group_active', true)
						);
						$result = $this->muser_group->update($id,$SQLDATA);
					if($result){
						redirect(base_admin_url().$this->_controller_url);
					}
				}
			endif;
		  $this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	public function ml(){
		$action = $_POST['ml'];
		$id = $_POST['id'];
		$option = '';
		if(!empty($action) && !empty($id)){
			switch ($action) {
				case 'active':
					$option = 1;
					break;
				case 'inactive':
						$option = 0;
						break;
			}
			$this->db->where_in('user_group_id',$id);
			$this->db->update('sys_user_groups',array('user_group_active'=>$option));
			if($this->db->affected_rows()){
				redirect(base_admin_url().$this->_controller_url);
			}
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
	}

	public function check_user_group_name($user_group_name){
		$id = $this->uri->segment(5);
		$this->db->select('user_group_name')->from('sys_user_groups')->where(array('user_group_name' => $user_group_name,'user_group_id !=' => $id));
		$query = $this->db->get();
		$count = $query->num_rows(); 
		if($count > 0){
			$this->form_validation->set_message('check_user_group_name', 'The '.word_r('user_group_name').' field must contain a unique value');
			return false;
		}
	}

	public function delete($id = null){
		if($id == 1){
			redirect(base_admin_url().$this->_controller_url);
		}
	
		$result = $this->muser_group->delete($id);
		if($result){
			redirect(base_admin_url().$this->_controller_url);
		}
		
		
	}

	public function permission($id = null){
		$this->_variable();
		$this->_data['htitle'] = word_r('permission');
		$this->_data['icon'] = 'fa fa-key';
		$this->db->select()->from('sys_permission')->where('group_id', $id);
		$query = $this->db->get();
		$result = $query->result_array();
		if(empty($id)){
			redirect(base_admin_url().$this->_controller_url);
		}
		$menu_id = array();
		foreach($result as $value){
			$menu_id[] = $value['menu_id'];
		}
		$this->_data['menu_id'] = $menu_id;
		$this->load->view(config_item('admin_dir').$this->_controller_url.'permission_list', $this->_data);
	
	}

	public function permission_update($id){
		if($this->input->post('update')){
			$group_id = $id;
			//Permission on menu
			$permission_type = $this->input->post('permission_type');
			if($permission_type == "menu"):
				
				$menu_id_array = $this->input->post('menu_id');
				//delete all record with this group_id
				$this->db->where('group_id', $group_id);
				$this->db->delete('sys_permission');
				$data = array();
				if(!empty($menu_id_array)){
					foreach($menu_id_array as $menu_id){
						$data[] = array(
									'menu_id' =>$menu_id, 
									'group_id' => $group_id,
									'create' => $this->input->post('create'.$menu_id),
									'update' => $this->input->post('update'.$menu_id),
									'delete' => $this->input->post('delete'.$menu_id),
									'search' => $this->input->post('search'.$menu_id),
									'ml' => $this->input->post('ml'.$menu_id),
									'ajax' => $this->input->post('ajax'.$menu_id),
									'permission' => $this->input->post('permission'.$menu_id)
								  );
					}
					//insert data to sys_permission
					$this->db->insert_batch('sys_permission', $data);
					if($this->db->affected_rows()){
						redirect(base_admin_url().$this->_controller_url.'permission/'.$id);
					}
				}else{
					redirect(base_admin_url().$this->_controller_url.'permission/'.$id);
				}
			//Permission on media
			else:
					//delete all record with this group_id
					$this->db->where('group_id', $group_id);
					$this->db->delete('sys_kcfinder_permission');
					$data[] = array(
									'group_id' => $group_id,
									'use' => $this->input->post('use'),
									'upload' => $this->input->post('upload'),
									'copy' => $this->input->post('copy'),
									'move' => $this->input->post('move'),
									'rename' => $this->input->post('rename'),
									'delete' => $this->input->post('delete'),
									'create_dir' => $this->input->post('create_dir'),
									'rename_dir' => $this->input->post('rename_dir'),
									'delete_dir' => $this->input->post('delete_dir')
									
							  );
					//insert data to sys_kcfinder_permission
					$this->db->insert_batch('sys_kcfinder_permission', $data);
					if($this->db->affected_rows()){
						redirect(base_admin_url().$this->_controller_url.'permission/'.$id);
					}
			endif;
		}else{
			redirect(base_admin_url().$this->_controller_url.'permission/'.$id);
		}
	}

	public function search(){
		$this->_variable();
		$user_group_name = $this->input->get('user_group_name');
		$user_group_active = $this->input->get('user_group_active');
		$this->_data['data'] = $this->muser_group->search($user_group_name, $user_group_active);
		$this->muser_group->count_all = true;
		$data = $this->muser_group->search($user_group_name, $user_group_active);
		$base_url = base_admin_url().$this->_controller_url.'search?user_group_name='.$user_group_name.'&user_group_active='.$user_group_active.'&';
		$this->_data['showing'] = $this->muser_group->showing($data);
		$this->_data['page'] = $this->muser_group->page($base_url,$data);
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	}

}
