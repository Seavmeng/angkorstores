<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_product extends CI_Controller {
	
	function __construct()	{
		parent::__construct();
		//admin_shop_only();
		//acl($this->uri->uri_string);
		$this->load->helper('product');
		$this->load->model('admin/product/moproduct');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//
	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->moproduct->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('category');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );

		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}
	
	function index(){
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$this->_variable();
		$user_id = get_user_id();
        $group_id = $this->session->userdata('group_id');
		$this->_data['data'] = $this->moproduct->show_all($user_id,$group_id);
		$base_url = account_url().$this->_controller_url.'/'.$this->uri->segment(4).'?';
		$this->moproduct->count_all = true;
		$this->_data['page'] = $this->moproduct->page($base_url,$this->moproduct->show_all($user_id,$group_id));
		$this->_data['showing'] =$this->moproduct->showing($this->moproduct->show_all($user_id,$group_id));
		$this->_data['selectVendors'] = $this->moproduct->getVendors();
		$this->load->view('admin/product/supplier_product',$this->_data);
	}
	public function supplierDetails($param){
		$this->load->model('admin/muser');
		$this->_data['supplier_info'] = $this->muser->getuser($param);
		$this->_data['get_country'] = $this->muser->country();
		$this->load->view('admin/product/supplier_info',$this->_data);
	}

	private function _validation_create(){
		$this->form_validation->set_rules('pro_code',word_r('procode'),'trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('product_name',word_r('proname'),'required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('pro_detail',word_r('pro_detail'),'trim|xss_clean|max_length[5000]');
		$this->form_validation->set_rules('pro_des',word_r('pro_des'),'trim|xss_clean|max_length[5000]');
		$this->form_validation->set_rules('status',word_r('status'),'trim|xss_clean');
		$this->form_validation->set_rules('categories[]',word_r('category'),'numeric|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('tags_con[]',word_r('tags'),'trim|xss_clean|max_length[100]');
		//------------general-----------
		$this->form_validation->set_rules('regular_price',word_r('regular_pri'),'trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('sale_price',word_r('sale_price'),'trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('pro_status',word_r('pro_status'),'trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('have_disc',word_r('have_disc'),'trim|numeric|xss_clean|max_length[100]');
		$this->form_validation->set_rules('from_ddate',word_r('from_ddate'),'trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('from_dtime',word_r('from_dtime'),'trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('to_ddate',word_r('to_ddate'),'trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('to_dtime',word_r('to_dtime'),'trim|xss_clean|max_length[100]');
		//-----------inventory=-----------
		$this->form_validation->set_rules('stock_qty',word_r('stock_qty'),'trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('stock_available',word_r('stock_available'),'trim|xss_clean|max_length[100]');
		//-----------shipping---
		$this->form_validation->set_rules('weight',word_r('weight'),'trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('length',word_r('length'),'trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('width',word_r('width'),'trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('height',word_r('height'),'trim|xss_clean|max_length[100]');
		//-----------Pricing------
		//$this->form_validation->set_rules('regular_price','Regular Price','required|trim|xss_clean');
		$this->form_validation->set_rules('sale_price','Sale Price','required|trim|xss_clean');
		//-----------attributes---
		$this->form_validation->set_rules('attr_m_id[]',word_r('attr_m_id'),'numeric|trim|xss_clean|max_length[100]');
		
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}
	
	//************************ Function Update **************************//
	//*********************** By Thean ***********************//
	public function update($id = null){
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$this->_data['brandid'] = $this->moproduct->get_brandid($id);
		$this->_data['contype'] = $this->moproduct->get_contactType($id);
		$this->_data['brand'] = $this->moproduct->get_brand();
		$this->_data['vendors'] = $this->moproduct->getVendors();
		$this->_data['vendorsId'] = $this->moproduct->getVendorsId($id);
        $this->_data['supplier'] = $this->moproduct->get_supplier();
		$this->_data['tags'] = $this->moproduct->get_tags();
		$user_id= user_id_logged();
		$this->_variable();
		//$this->_data['data'] = $this->moproduct->find_by_id($id,$user_id);
		$this->_data['relate'] = $this->moproduct->getCatRelationship($id);
		$this->_data['data'] = $this->moproduct->getProById($id, $user_id);
		$this->_data['gall'] = $this->moproduct->find_gallery($id);
		$this->_data['bid'] = $this->moproduct->getBidStatus($id);
		$user = $this->session->userdata('user_id');
		$user_group = $this->session->userdata('group_id');
		if($this->_data['data']){ 
			if($this->input->post()): 
					if($this->_validation_create()){
						$get_gategory=array();
						$get_tag=array();
						
						if(!empty($category_array)){
							foreach ($category_array as $category_id) {
								$get_gategory []=$category_id;
							}
							$get_gategories=implode(',', $get_gategory);
						}else{
							$get_gategories=default_cate();
							//$get_gategories=default_procategory();
						}
						//------------tags--------------
						$tags=$this->input->post('tags_con',true);
						if(!empty($tags)){
							foreach ($tags as $tags_) {
								$get_tag []=$tags_;
							}
							$get_tags=implode(',', $get_tag);
						}else{
							$get_tags="";
						}
						//---------featured image ----
						$image_featured = upload();
						if(!empty($image_featured)){
							foreach($image_featured as $image_feature){
								$image_features[] = date('m-Y').'/thumb/'.$image_feature; 					
							}
							$img_feature = implode(',',$image_features);
						}else{
							$img_feature = $this->input->post('feature',true);
						}
						//----------Gallery-------
						$image_gallery = galleries();
						//var_dump($image_gallery);
						if(!empty($image_gallery)){
							//$img_galleries=array();
							foreach($image_gallery as $image_galleries){
								
								$img_galleries[] =  date('m-Y').'/thumb/'.$image_galleries;
							}
							$img_gallery = implode(',', $img_galleries);
						}else{
							$img_gallery = $this->input->post('image',true);
						}
						//-------------attributes-----
						$colors  = $this->input->post('color');
						$size_attr  = $this->input->post('size');
						$image_color = color();
						if(!empty($image_color)){
							foreach($image_color as $image_attr){
								$image_attribute[] = date('m-Y').'/thumb/'.$image_attr; 					
							}
							$img_attr = $image_attribute;
						}else{
							$img_attr = $this->input->post('imgcolor');
						}
						//----varian---
						$varian_color  = $this->input->post('varian_color');
						$varian_size  = $this->input->post('varian_size');
						$varian_price  = $this->input->post('varian_price');

						/* duplicate product_code */
						$product_code = $this->input->post('pro_code');
						$product_id = $this->_data['data']['product_id'];
						//var_dump($pid);exit();
						$find_pcode = $this->moproduct->check_duplicate_update('sys_product',$product_code,$product_id);

						if(empty($find_pcode) || $product_code == ''){
							$p_cod = $this->input->post('pro_code');
						}else{
							$this->session->set_flashdata('msg','<div class="alert alert-danger">
							<strong>Danger!</strong> The field product_code already exist, Please try another one ):
						  </div>');
  							redirect(base_url().'admin/product/supplier_product/update/'.$product_id);
						}
                        if($user_group  == 1){
						$supplier_id=$this->input->post('supplier');
						$status = 1;
						$visible = $this->input->post('status', true);
						//echo 'test'; exit;
						}else{
							$supplier_id= $user;
							$visible = $this->input->post('visible');
							$status = $this->input->post('stat'); 
						}
							$is_dis=$this->input->post('have_disc', true);
							$from_date=$this->input->post('from_ddate', true);
							$from_time=$this->input->post('from_dtime', true);
							$to_date=$this->input->post('to_ddate', true);
							$to_time=$this->input->post('to_dtime', true);
								
						echo $is_dis.'<br/>';
						echo $from_date.' '.$from_time.'<br/>';
						echo $to_date.' '.$to_time.'<br/>';
						echo $this->input->post('dis_amount',true).'<br/>';
							
						//exit;
						/* -------- duplicate product_code ------- */
						$SQLDATA = array(
							//'product_code'=> $this->input->post('pro_code', true),
							'product_code' => $p_cod,
							'product_name'=> $this->input->post('product_name', true),
							'detail'=> $this->input->post('pro_short_des', true),
							'description'=>$this->input->post('pro_des', true),
							'modified_date'=>date('Y-m-d H:i'),
							'feature'=>$img_feature, 
							'user_id'=> user_id_logged(),
							'brand' => $this->input->post('brname'),
							'vendor_id'=> $this->input->post('vendor'),
							'supplier_id'=> $supplier_id,
							'contact-type' => $this->input->post('contact-type'),
							'visibility' => $visible,
							'tags' =>$this->input->post('tag_name'), 
							
						);
						//print_r($SQLDATA);
						
						$result = $this->moproduct->update($id,$SQLDATA);
						$product_id=$id;
						
						if($result){
							$is_dis=$this->input->post('have_disc', true);
							if ($is_dis==1){
								$have_dis=1;
								$from_date=$this->input->post('from_ddate', true);
								$from_time=$this->input->post('from_dtime', true);
								$to_date=$this->input->post('to_ddate', true);
								$to_time=$this->input->post('to_dtime', true);
							}else{
								$have_dis="";
								$from_date="";
								$from_time="";
								$to_date="";
								$to_time="";
							}
							$hot_product = $this->input->post('hot_pro',true);
							
							if($this->input->post('bidding', true)){

								$bidValue = 1;
								$buyNow = $this->input->post('buy_now', true);
								$bid_from_date = $this->input->post('bid_from_date',true);
								$bid_from_time = $this->input->post('bid_from_time',true);
								$bid_to_date = $this->input->post('bid_to_date', true);
								$bid_to_time = $this->input->post('bid_to_time', true);
							}
							else{

								$bidValue = 0;
								$buyNow = null;
								$bid_from_date = '';
								$bid_from_time = '';
								$bid_to_date = '';
								$bid_to_time = '';
							}
							
						//	echo $this->input->post('sale_price', true);exit;
							//############# Update inventory ##############//
							$SQLDATA_inven = array(
								'product_id'=>$product_id,
								'regular_prices'=>$this->input->post('regular_price', true),
								'sale_price'=>$this->input->post('sale_price', true),
								'product_type'=>$this->input->post('pro_status', true),
								'have_discount'=>$have_dis,
								'from_date'=>$from_date.' '.$from_time,
								'todate'=>$to_date.' '.$to_time,
								'discount_amount'=> $this->input->post('dis_amount',true),
								'stock_qty'=>$this->input->post('stock_qty', true),
								'available'=>$this->input->post('stock_available', true),
								'weight'=>$this->input->post('weight', true),
								'length'=>$this->input->post('length', true),
								'width'=>$this->input->post('width', true),
								'height'=>$this->input->post('height', true),
								'hot_pro'=>$hot_product,
								'bid_status'=>$bidValue,
								'buy_now_price'=>$buyNow,
								'bid_from_date'=>$bid_from_date . ' ' . $bid_from_time,
								'bid_to_date'=>$bid_to_date . ' ' . $bid_to_time

							);
							$inventory=$this->moproduct->update_inv($product_id,$SQLDATA_inven);
							$ids = $id;
			
							if($inventory){
								$this->_data['success'] = has_upd();
							}
							//-------attribute----
							$this->db->where('product_id', $product_id);
							$this->db->delete('sys_attributes_pro');
							for($i=0;$i < count($colors); $i++){
								$color_arr= $colors[$i];
								$color= $color_arr['color'];
								$sizes=array();
								foreach($color_arr['size'] as $size){
									$sizes []= $size;
								}
								$size= implode(',',$sizes);
						
								$SQLDATA=array(
									'product_id' => $product_id,
									'color' => $color,
									'size' => $size,
									'image' => $img_attr[$i] !="" ? $img_attr[$i] : '',
									'created' => date('Y-m-d H:i')
								);
								$result = $this->moproduct->save_attr($SQLDATA);
							}
							if($size_attr){
								foreach($size_attr as $size_){
									$SQLDATA=array(
										'product_id' => $product_id,
										'color' => '',
										'size' => $size_,
										'image' => '',
										'created' => date('Y-m-d H:i')
									);
									$result = $this->moproduct->save_attr($SQLDATA);
								}
							}
							//--------varian------
							$this->db->where('product_id', $product_id);
							$this->db->delete('sys_variable');
							for($i=0;$i < count($varian_color); $i++){
								$SQLDATA=array(
									'product_id' => $product_id,
									'sizes' => $varian_size[$i],
									'colors' => $varian_color[$i],
									'price' => $varian_price[$i],
									'created' => date('Y-m-d H:i')
								);
								$result = $this->moproduct->save_varian($SQLDATA);
							}
							// ---------close attrbute-----
							//************** Galley ******************
							//var_dump($img_gallery);
							if($this->_data['gall']){
								$SQLDATA_galley = array(
									//'image'=>''
									'image'=>$img_gallery
								);
								$gallery=$this->moproduct->update_gall($product_id,$SQLDATA_galley);
								if($gallery){
									$this->_data['success'] = has_upd();
								}
							}else{
								//************** Galley ******************
								$SQLDATA_galley = array(
									'post_id'=>$product_id,
									'title'=> $this->input->post('product_name', true),
									'image'=>$img_gallery,
									'description'=> $this->input->post('pro_detail')
								);
								
								$gallery=$this->db->insert('sys_gallery',$SQLDATA_galley);
							}
							//############# Update category ##############//
							$this->db->where('post_id', $product_id);
							$this->db->delete('sys_relationship');
							$category_array=$this->input->post('categories',true);
							if(!empty($category_array)){
								foreach ($category_array as $category_id) {
									$this->db->insert('sys_relationship',array('post_id'=>$product_id,'category_id'=>$category_id));
								}
							}else{
								// 1 for uncategorized
								$this->db->insert('sys_relationship',array('post_id'=>$product_id,'category_id'=>default_procategory()));
								//$this->db->insert('sys_relationship',array('post_id'=>$product_id,'category_id'=>default_cate()));							
							}
							
							if($category_array){
								$this->session->set_flashdata('msg', $this->input->post('product_name', true).' '.has_upd());
								redirect(base_admin_url().$this->_controller_url);
							}
							else{
								$this->session->set_flashdata('msg', $this->input->post('product_name', true).' '.has_upd());
								redirect(base_admin_url().$this->_controller_url);
							} 
						} 
					} 
			endif;
			$this->_data['get_attribute'] = $this->moproduct->get_attrs($id);
			$this->_data['get_varians'] = $this->moproduct->get_varians($id);
			$this->load->view('admin/product/product_list/update_product',$this->_data);
		}else{ 
			redirect(account_url().$this->_controller_url);
		} 
	} 
	
    //************************ Delete **************************//
	public function delete($id = null){
		
		$result = $this->moproduct->deleteproduct($id);
		$inventory = $this->moproduct->deleteinventory($id);
		$gallery = $this->moproduct->deletegallery($id);
		$relation = $this->moproduct->deleterelation($id);
		if($relation){
			$this->session->set_flashdata('msg','Congratulation! your data '.has_del());
			redirect(base_url().'admin/product/supplier_product');
		}
	}
	//************************ Delete **************************//

	public function attributes(){
		$id['ids'] = $this->input->post('id');
		$this->load->view('admin/product/attributes',$id);
	}
	
	//*********************** Shopping page **********************//
	/*public function dentail($permalink = null){
		//$user_id = user_id_logged();
		$product_id = $this->moproduct->get_id_by_permalink($permalink);
		$product_id=$product_id['product_id'];
		$this->_data['data_'] = $this->moproduct->find_by_id($permalink);
		$this->_data['gall_'] = $this->moproduct->find_gallery($product_id);
		//$this->_data['product_id'] =
		
		//$this->_data['test'] = 'WTF!';
		
		$this->load->view('home/single_product',$this->_data);
		//var_dump($product_id); exit;
	} */
	
	//*********************** Group Products **************************//
	/*
	public function products($id = null){
		$this->_variable();
		
		$this->get_pro_['id']	       =  $id;
		if(!empty($id)){
			$this->get_pro_['fname']   =  $this->input->post('get_price',true);
			$this->get_pro_['newpro_'] =  $this->moproduct->new_product($id);
			$this->get_pro_['poppro_'] =  $this->moproduct->popular_product($id);
			$this->get_pro_['salpro_'] =  $this->moproduct->sale_product($id);
			$this->get_pro_['getpri_'] =  $this->moproduct->get_pro_pirce($id);
			$this->get_pro_['getpro_'] =  $this->moproduct->get_pro_detail($id);
			$this->get_pro_['getcat_'] =  $this->moproduct->group_pro_list($id);
			$base_url = base_url().'account/'.$this->_controller_urls.$id.'/?';
			//echo $base_url;
			$this->moproduct->count_all = true;
			$this->get_pro_['page'] = $this->moproduct->page($base_url,$this->moproduct->get_pro_detail($id));
			$this->get_pro_['page_new'] = $this->moproduct->page($base_url,$this->moproduct->new_product($id));
			$this->get_pro_['page_pop'] = $this->moproduct->page($base_url,$this->moproduct->popular_product($id));
			$this->get_pro_['page_pri'] = $this->moproduct->page($base_url,$this->moproduct->get_pro_pirce($id));
			$this->get_pro_['page_sale'] = $this->moproduct->page($base_url,$this->moproduct->sale_product($id));
			$this->load->view('account/product/products', $this->get_pro_);
		}else{
			if($this->input->get('btn_general')){	
				$search = $this->input->get('search',true); 
				$this->get_pro_['pro_name_s'] = $this->moproduct->search_cate($search);
				$this->get_pro_['pro_sale'] = $this->moproduct->search_cate_sale($search);
				$this->get_pro_['pro_pop'] = $this->moproduct->search_cate_pop($search);
				$this->get_pro_['pro_new'] = $this->moproduct->search_cate_new($search);
				$this->get_pro_['pro_price'] = $this->moproduct->search_cate_price($search);
				$base_url = current_full_url();
				$this->moproduct->count_all = true;
				
				$this->get_pro_['page'] = $this->moproduct->page($base_url,$this->moproduct->search_cate($search));
				
				$this->get_pro_['page_new'] = $this->moproduct->page($base_url,$this->moproduct->search_cate_new($search));
				
				$this->get_pro_['page_pop'] = $this->moproduct->page($base_url,$this->moproduct->search_cate_pop($search));
				
				$this->get_pro_['page_pri'] = $this->moproduct->page($base_url,$this->moproduct->search_cate_price($search));
				
				$this->get_pro_['page_sale'] = $this->moproduct->page($base_url,$this->moproduct->search_cate_sale($search));
				
				$this->get_pro_['showing'] =$this->moproduct->showing($this->moproduct->get_pro_by_shop($search));
				
				$this->load->view('account/product/products', $this->get_pro_);
			}
			if($this->input->get('store')){
				$com_name = $this->input->get('pro_name',true);
				$items	  = $this->input->get('search',true);;
				$this->session->set_userdata('item', $items);
				//$this->session->set_flashdata('item', $items);
				redirect('shop/'.$com_name);
			}
		}
	}
	*/
	
	public function search_list($search = null){
		$search = $this->input->post('search',true);
		if(!empty($search)){
			$this->db->select();
			$this->db->from('sys_search_pro');
			$this->db->like('product_name',$search);
			$this->db->or_like('category',$search);
			$this->db->order_by('product_name','DESC');
			$this->db->order_by('category','DESC');
			$result = $this->db->get();
			$search_array = $result->result_array();
			$output ="";
			$output .='<ul class="dropdowns">';
				foreach ($search_array as $value) {
					$search_name = $value['product_name'];
					$output .= '<li class="select-search">';
						$output .= $search_name;
					$output .= '</li>';
				}
			$output .='</ul>';
			echo $output;
		}
	}
	
	public function shop_management(){		
		$user_id = user_id_logged();
		$this->data['data'] = $this->moproduct->get_user_detail($user_id);
		if($this->input->post('btn_shop')){
			
			//---------Slide image ----
			$image_slide = upload();
			if(!empty($image_slide)){
				foreach($image_slide as $slides){
					$slided[] = date('m-Y').'/thumb/'.$slides; 					
				}
				$image_slides = implode(',',$slided);
			}else{
				$image_slides = "";
			}
			
			//--------- Banner image ----------
			$image_banner = galleries();
			if(!empty($image_banner)){
				foreach($image_banner as $banner){
					$banners[] = date('m-Y').'/thumb/'.$banner; 					
				}
				$image_banners = implode(',',$banners);
			}else{
				$image_banners = $this->input->post('image',true);
			}
			
			//---------Background ----------
			$image_background = color();
			if(!empty($image_background)){
				foreach($image_background as $background){
					$backgrounds[] = date('m-Y').'/thumb/'.$background; 					
				}
				$image_backgrounds = implode(',',$backgrounds);
			}else{
				$image_backgrounds = "";
			}
			
			$social=$this->input->post('social',true);
			if(!empty($social)){
				foreach ($social as $socials) {
					$get_socials []=$socials;
				}
				$get_social=implode(',', $get_socials);
			}else{
				$get_social="";
			}

			$SQLslide = array(
				'images' => $image_slides,
				'background'=> $image_backgrounds,
				'social_net'=>$get_social,
				'user_id'=>$user_id,
				'created_date'=>date('Y-m-d H:i'),
				'fromday'=>$this->input->post('dfrom',true),
				'today'=>$this->input->post('dto',true),
				'fromtime'=>'0000-00-00'.' '.$this->input->post('tfrom',true),
				'totime'=>'0000-00-00'.' '.$this->input->post('tto',true),
				'efromday'=>$this->input->post('dexfrom',true),
				'etoday'=>$this->input->post('dexto',true),
				'efromtime'=>'0000-00-00'.' '.$this->input->post('texfrom',true),
				'etotime'=>'0000-00-00'.' '.$this->input->post('texto',true),
				'have_caption'=>$this->input->post('hcaption',true),
				'title'=>$this->input->post('title',true),
				'detail'=>$this->input->post('detail',true)
			);
			
			$slide = $this->moproduct->save_slide($SQLslide);
			
			$SQLinfo = array(
				'phone' => $this->input->post('phone',true),
				'address' => $this->input->post('address',true),
				'image'=> $image_banners
				//'create_date'=>date('Y-m-d H:i')
			);
			
			$update_info = $this->moproduct->update_user_detail($user_id,$SQLinfo);
				
		}else{
			echo 'she';
		}	
		//$this->load->view('index1',$this->data);
	}
	
	public function update_shop(){
		$user_id = user_id_logged();
		if($this->input->post('btn-submit')){
			//---------Slide image ----
			$image_slide = upload();
			if(!empty($image_slide)){
				foreach($image_slide as $slides){
					$slided[] = date('m-Y').'/thumb/'.$slides; 					
				}
				$image_slides = implode(',',$slided);
			}else{
				$image_slides = $this->input->post('slide',true);
			}
			
			//--------- Banner image ----------
			$image_banner = galleries();
			if(!empty($image_banner)){
				foreach($image_banner as $banner){
					$banners[] = date('m-Y').'/thumb/'.$banner; 					
				}
				$image_banners = implode(',',$banners);
			}else{
				$image_banners = $this->input->post('image',true);
			}
			
			//---------Background ----------
			$image_background = color();
			if(!empty($image_background)){
				foreach($image_background as $background){
					$backgrounds[] = date('m-Y').'/thumb/'.$background; 					
				}
				$image_backgrounds = implode(',',$backgrounds);
			}else{
				$image_backgrounds = $this->input->post('bg_image',true);
			}
			
			$social=$this->input->post('social',true);
			if(!empty($social)){
				foreach ($social as $socials) {
					$get_socials []=$socials;
				}
				$get_social=implode(',', $get_socials);
			}else{
				$get_social="";
			}
			
			$SQLslide = array(
				'images' =>$image_slides,
				'background'=>$image_backgrounds,
				'social_net'=>$get_social,
				'user_id'=>$user_id,
				'created_date'=>date('Y-m-d H:i'),
				'fromday'=>$this->input->post('dfrom',true),
				'today'=>$this->input->post('dto',true),
				'fromtime'=>'0000-00-00'.' '.$this->input->post('tfrom',true),
				'totime'=>'0000-00-00'.' '.$this->input->post('tto',true),
				'efromday'=>$this->input->post('dexfrom',true),
				'etoday'=>$this->input->post('dexto',true),
				'efromtime'=>'0000-00-00'.' '.$this->input->post('texfrom',true),
				'etotime'=>'0000-00-00'.' '.$this->input->post('texto',true),
				'have_caption'=>$this->input->post('hcaption',true),
				'title'=>$this->input->post('title',true),
				'detail'=>$this->input->post('detail',true)
			);
			//print_r($SQLslide);exit;
			$slide = $this->moproduct->update_slide($user_id, $SQLslide);
			
			$SQLinfo = array(
				'phone' => $this->input->post('phone',true),
				'address' => $this->input->post('address',true),
				'image'=>$image_banners
				//'create_date'=>date('Y-m-d H:i')
			);
			//echo 'shop_management'; echo '<pre>';print_r($SQLslide);echo '</pre>';
			//echo 'user_detail'; echo '<pre>';print_r($SQLinfo);echo '</pre>';;exit;
			$update_info = $this->moproduct->update_user_detail($user_id,$SQLinfo);
			$user_id = user_id_logged();
			$get_user = descript($user_id);
			$name = $get_user['username'];
			$names = 'PhoneDistributor';
			//redirect('account/sites/preview/'.$test);
			redirect(base_url('shop/'.$name));
		}
		//$this->load->view('index1');
	}

	public function images(){
		$all_img = $this->moproduct->get_image();
		if(empty($all_img)){
			if($this->input->post('btn_image')){
				$image_slide = upload();
				if(!empty($image_slide)){
					foreach($image_slide as $slides){
						$slided[] = date('m-Y').'/thumb/'.$slides; 					
					}
					$image_slides = implode(',',$slided);
				}else{
					$image_slides = '';
				}
				
				$image_promo = galleries();
				if(!empty($image_promo)){
					foreach($image_promo as $promo){
						$promotion[] = date('m-Y').'/thumb/'.$promo; 					
					}
					$image_promotion = implode(',',$promotion);
				}else{
					$image_promotion = '';
				}
				
				$image_rate = color();
				if(!empty($image_rate)){
					foreach($image_rate as $rate){
						$ratting[] = date('m-Y').'/thumb/'.$rate; 					
					}
					$image_ratting = implode(',',$ratting);
				}else{
					$image_ratting = '';
				}
				$SQLimage = array(
					'img_slide' => $image_slides,
					'img_promotion' => $image_promotion,
					'img_rate'=>$image_ratting
					//'create_date'=>date('Y-m-d H:i')
				);
				$image_library = $this->moproduct->image($SQLimage);
			}
			$this->load->view('admin/image/image_library');
		}
		else{
			$this->img['images'] = $this->moproduct->get_image();
			if($this->input->post('btn_image')){
				$image_slide = upload();
				if(!empty($image_slide)){
					foreach($image_slide as $slides){
						$slided[] = date('m-Y').'/thumb/'.$slides; 					
					}
					$image_slides = implode(',',$slided);
				}else{
					$image_slides = $this->input->post('slide',true);
				}
				
				$image_promo = galleries();
				if(!empty($image_promo)){
					foreach($image_promo as $promo){
						$promotion[] = date('m-Y').'/thumb/'.$promo; 					
					}
					$image_promotion = implode(',',$promotion);
				}else{
					$image_promotion = $this->input->post('promotion',true);
				}
				
				$image_rate = color();
				if(!empty($image_rate)){
					foreach($image_rate as $rate){
						$ratting[] = date('m-Y').'/thumb/'.$rate; 					
					}
					$image_ratting = implode(',',$ratting);
				}else{
					$image_ratting = $this->input->post('rate',true);
				}
				$SQLimageup = array(
					'img_slide' => $image_slides,
					'img_promotion' => $image_promotion,
					'img_rate'=>$image_ratting
					//'create_date'=>date('Y-m-d H:i')
				);
				$image_library = $this->moproduct->image_update($SQLimageup);
			}
			$this->load->view('admin/image/update_img',$this->img);
		}
	}

	public function change_visible($pid){
		$logged = has_logged();
		if($logged){
		$data = array('status' => $this->input->post('show_visible'));
		$this->moproduct->edit_visible($pid,$data);
		//redirect(base_url().'admin/product/supplier_product');
		redirect($_SERVER['HTTP_REFERER']);
		}else{
			redirect('account');
		}
	}

    
    //****************** Create new product supplier ******************
    function create(){
	    $logged = has_logged();
		if($logged){
		$user_id = user_id_logged();
		/*khorng 1-17-2017*/
		$user = $this->session->userdata('user_id');
		$user_group = $this->session->userdata('group_id');
		$this->_data['cat'] = $this->moproduct->get_cat($user);
		/*End*/
        $this->session->set_userdata('categorie',$this->input->post('categories[]'));
                
		$this->_data['brand'] = $this->moproduct->get_brand();
		$this->_data['vendors'] = $this->moproduct->getVendors();
        $this->_data['supplier'] = $this->moproduct->get_supplier();
        $this->_data['tags'] = $this->moproduct->get_tags();
		//var_dump($this->_data['brand']);exit();
		$this->_variable();
		if($this->input->post('btn_save')){
			//field_name,from,condition
			$this->session->set_userdata('supplier', $this->input->post('supplier'));
                        $pro_code=$this->input->post('pro_code');
                        if($pro_code ==""){
                             $product_code='aa_aa';
                        }else{ 
                            $product_code=$this->input->post('pro_code');
                        }
			$check_field_dup=$this->moproduct->check_duplicate('product_id','product_code','sys_product',$product_code);
			$product_id=$check_field_dup['product_id'];
			if($product_id){
				$this->_data['errors'] = 'The field product_code already exist, Please try another one ):';
			}else{
				if($this->_validation_create()){	
					$get_gategory=array();
					$get_tag=array();
					$get_attr_id=array();
					//$category_array=$this->input->post('categories',true);
					$categories_array=$this->input->post('categories');
					$usedCat = $this->input->post('recents');
					//var_dump($categories_array); var_dump($usedCat);
					if($usedCat != null && $categories_array != null){
						$merged = array_merge($categories_array, $usedCat);
						$category_array = array_unique($merged);
					}elseif($usedCat != null && $categories_array == null){
						$category_array = $usedCat;
					}else{
						$category_array = $categories_array;
					}
					
					//------------tags--------------
					$tags=$this->input->post('tags_con',true);
					if(!empty($tags)){
						foreach ($tags as $tags_) {
							$get_tag []=$tags_;
						}
						$get_tags=implode(',', $get_tag);
					}else{
						$get_tags="";
					}
					//-------------attributes-----
					$colors  = $this->input->post('color');
					$size_attr  = $this->input->post('size');
					$image_color = color();
					//----varian---
					$varian_color  = $this->input->post('varian_color');
					$varian_size  = $this->input->post('varian_size');
					$varian_price  = $this->input->post('varian_price');
					
					//---------featured image ----
					$image_featured = upload();
					if(!empty($image_featured)){
						foreach($image_featured as $image_feature){
							$image_features[] = date('m-Y').'/thumb/'.$image_feature; 					
						}
						$img_feature = implode(',',$image_features);
					}else{
						$img_feature = '';
					}
					//echo $img_feature.'feature<br/>';
					//---------gallery ----
					$image_gallery = galleries();
					if(!empty($image_gallery)){
						foreach($image_gallery as $image_galleres){
							$img_galley[] = date('m-Y').'/thumb/'.$image_galleres; 					
						}
						$img_gallies = implode(',',$img_galley);
					}else{
						$img_gallies = '';
					}
					//echo $img_gallies.'gallery<br/>';exit;
					//------------attr_id--------------
					$attr_ids=$this->input->post('attr_m_id',true);
					if(!empty($attr_ids)){
						foreach ($attr_ids as $key => $attr_id) {
							$get_attr_id []=$attr_id;
							
						}
						$get_attr_ids=implode(',', $get_attr_id);
					}else{
						$get_attr_ids="";
					}
                    if($user_group  == 1){
						$supplier_id=$this->input->post('supplier');
						$visible = $this->input->post('status');
						$status = 1;
						//echo 'test'; exit;
					}else{
						$supplier_id= $user;
						$visible = 1;
						$status = 0;
						//echo 'hello'; exit;
					}
					//var_dump($supplier_id);
					//var_dump($this->input->post('supplier')); exit;
					$SQLDATA = array(
						'product_code'=> $this->input->post('pro_code', true),
						'product_name'=> $this->input->post('product_name', true),
						'brand' => $this->input->post('brname'),
						'detail'=> $this->input->post('pro_short_des', true),
						'description'=>$this->input->post('pro_des', true),
						'status'=>$status,
						'created_date'=>date('Y-m-d H:i'),
						'feature'=>$img_feature,
						'tags'=>$get_tags,
						'permalink'=>permalink($this->input->post('product_name', true)),
						'user_id'=> user_id_logged(),
						'vendor_id'=> $this->input->post('vendor'),
                        'supplier_id'=> $supplier_id,
						'contact-type' =>$this->input->post('contact-type'), 
						'visibility' => $visible,
						'tags' => $this->input->post('tag_name'), 
					);
					//print_r($SQLDATA);exit;
					$result = $this->moproduct->save($SQLDATA);
					if($result){
						$product_id=$result;
						$is_dis=$this->input->post('have_disc', true);
						//var_dump($is_dis);exit();
						if ($is_dis==1){
							$have_dis=1;
							$from_date=$this->input->post('from_ddate', true);
							$from_time=$this->input->post('from_dtime', true);
							$to_date=$this->input->post('to_ddate', true);
							$to_time=$this->input->post('to_dtime', true);
						}else{
							$have_dis="";
							$from_date="";
							$from_time="";
							$to_date="";
							$to_time="";
						}
						$hot_product = $this->input->post('hot_pro',true);
						//************** Categories ******************
						//$category_array = $category_array;
						$count = count($category_array);
						
						$this->session->set_userdata('categories',$category_array);
						if($user_group != 2){
							if(!empty($category_array)){
								foreach ($category_array as $category_id) {
									$this->db->insert('sys_relationship',array('post_id'=>$product_id,'category_id'=>$category_id));
								}
							}else{
									// 1 for uncategorized
									$this->db->insert('sys_relationship',array('post_id'=>$product_id,'category_id'=>default_procategory()));
							}
						}else{
							if(!empty($category_array)){
									for($i=0; $i<$count; $i++){
										$this->db->insert('sys_relationship',array('post_id'=>$product_id,'category_id'=>$category_array[$i]));
									}
							}else{
								// 1 for uncategorized
									$this->db->insert('sys_relationship',array('post_id'=>$product_id,'category_id'=>default_procategory()));
							}
						}
						$this->session->userdata('categories');
						//************** Categories ******************

						if($this->input->post('bidding', true)){
							
							$bidValue = 1;
							$buyNow = $this->input->post('buy_now', true);
							$bid_from_date = $this->input->post('bid_from_date', true);
							$bid_from_time = $this->input->post('bid_from_time', true);
							$bid_to_date = $this->input->post('bid_to_date', true);
							$bid_to_time = $this->input->post('bid_to_time', true);
						}
						else{

							$bidValue = 0;
							$buyNow = '';
							$bid_from_date = '';
							$bid_from_time = '';
							$bid_to_date = '';
							$bid_to_time = '';
						}

						$SQLDATA_inven = array(
							'product_id'=>$product_id,
							'regular_prices'=>$this->input->post('regular_price', true),
							'sale_price'=>$this->input->post('sale_price', true),
							'product_type'=>$this->input->post('pro_status', true),
							'have_discount'=>$have_dis,
							'from_date'=>$from_date.' '.$from_time,
							'todate'=>$to_date.' '.$to_time,
							'discount_amount'=> $this->input->post('dis_amount',true),
							'stock_qty'=>$this->input->post('stock_qty', true),
							'available'=>$this->input->post('stock_available', true),
							'weight'=>$this->input->post('weight', true),
							'length'=>$this->input->post('length', true),
							'width'=>$this->input->post('width', true),
							'height'=>$this->input->post('height', true),
							'hot_pro'=>$hot_product,
							'sale_amoung'=>'0',
							'bid_status'=>$bidValue,
							'buy_now_price'=>$buyNow,
							'bid_from_date'=>$bid_from_date . ' ' . $bid_from_time,
							'bid_to_date'=>$bid_to_date . ' ' . $bid_to_time
						);
						$inventory=$this->db->insert('sys_inventory',$SQLDATA_inven);
                        //-------attribute----
						for($i=0;$i < count($colors); $i++){
							$color_arr= $colors[$i];
							$color= $color_arr['color'];
							$sizes=array();
							foreach($color_arr['size'] as $size){
								$sizes []= $size;
							}
							$size= implode(',',$sizes);
					
							$SQLDATA=array(
								'product_id' => $product_id,
								'color' => $color,
								'size' => $size,
								'image' => $image_color !="" ? date('m-Y').'/thumb/'.$image_color[$i] : '',
								'created' => date('Y-m-d H:i')
							);
							$result = $this->moproduct->save_attr($SQLDATA);
						}
						if($size_attr){
							foreach($size_attr as $size_){
								$SQLDATA=array(
									'product_id' => $product_id,
									'color' => '',
									'size' => $size_,
									'image' => '',
									'created' => date('Y-m-d H:i')
								);
								$result = $this->moproduct->save_attr($SQLDATA);
							}
						}
						//--------varian------
						for($i=0;$i < count($varian_color); $i++){
							$SQLDATA=array(
								'product_id' => $product_id,
								'sizes' => $varian_size[$i],
								'colors' => $varian_color[$i],
								'price' => $varian_price[$i],
								'created' => date('Y-m-d H:i')
							);
							$result = $this->moproduct->save_varian($SQLDATA);
						}
						// close attrbute-----
						//if($this->input->post('recents')){
							$recentCat = $this->input->post('recents');
						//}else{
							$allCat = $this->input->post('categories');
						//}
						
						if($recentCat != null){
							$mergedCat = array_merge($recentCat, $allCat);
							$uniqueCat = array_unique($mergedCat);
						}else{
							$uniqueCat = $allCat;
						}
						
						
						$user = user_id_logged();
						$this->moproduct->beforeReInsert($user);
						foreach($uniqueCat as $eachCat){
							$parent = $this->moproduct->find_category($eachCat);
						
							$sqlData = array('parent_id' => $parent[0]['parent_id'],
										'category_id' => $eachCat,
										'user_id' => $user,
										'created_date' => date('Y-m-d H:i'),
										'modified_date' => date('Y-m-d H:i')
							);
							$this->moproduct->insert_category($sqlData);
						}
						
						//************** Galley ******************
						$SQLDATA_galley = array(
							'post_id'=>$product_id,
							'title'=> $this->input->post('product_name', true),
							'image'=>$img_gallies,
							'description'=> $this->input->post('pro_detail')
						);
						
						$gallery=$this->db->insert('sys_gallery',$SQLDATA_galley);
						if($gallery){
							if($user_group==1){
								$strMsg = has_save();
								$this->session->set_flashdata('msg', $this->input->post('product_name', true).' '.$strMsg);
							}else{
								$strMsg = 'Your product is being checked by our administrators and will be published within 24 hours.';
								$this->session->set_flashdata('msg', $strMsg);
							}
							redirect(base_url().'admin/product/product_list/update/'.$product_id);
						}
						//************** Galley ******************
					}
				}
			}
		}
		$this->load->view('admin/product/new_product',$this->_data);	
		
		}else{
			redirect('account');
		}
	}
    
}
