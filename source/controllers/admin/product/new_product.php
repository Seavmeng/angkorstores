<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php //echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> Add New Product</span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php //echo btn_actions('admin/product_list/create','account/product_list/create', $create_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open_multipart('admin/product_list/create', array('role' => 'form') ); ?>
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
							         <!-- Row -->
							         <div class="row">
							         		 <!-- Form -->
							         		<div class="col-md-8">	
							         			<!--Form-body -->
													<div class="form-body">
														<!-- ROW -->
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																	<div class="form-group required <?php has_error('Procode'); ?>">
																		<?php echo 'Procode';?>
																		<?php echo form_input('pro_code' ,$this->input->post('pro_code', true), "class='form-control'")?>
																		<font color="red"><?php echo form_error('procode'); ?></font>
																	</div>
															</div>
															<!--End col-md-6 -->
														</div>
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																	<div class="form-group required <?php has_error('product_name'); ?>">
																		<?php echo 'Product Name';?>
																		<?php echo form_input('product_name', $this->input->post('product_name', true),"class = 'form-control'"); ?>
																		<font color="red"><?php echo form_error('product_name'); ?></font>
																	</div>
															</div>
															<!--End col-md-6 -->
														</div>
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																	<div class="form-group required <?php has_error('product_brand'); ?>">
																		<?php echo 'Product Brand';?>
																		<?php echo form_input('product_brand', $this->input->post('product_brand', true),"class = 'form-control'"); ?>
																		<font color="red"><?php echo form_error('product_brand'); ?></font>
																	</div>
															</div>
															<!--End col-md-6 -->
														</div>
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																	<div class="form-group">
																		<?php echo form_label(word_r('description'),'pro_detail',array('class' => 'control-label') ); ?>
																		<?php echo form_textarea(array('name' => 'pro_detail', 'class' => 'form-control mce', 'id' => 'pro_detail',  'value' => $this->input->post('description')) );?>
																	</div>
															</div>
															<!--End col-md-6 -->
														</div>
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																	<div class="form-group">
																		<label class="col-sm-12 control-label">Inventory</label><br /><br />
																		<div class="clearfix tabs-vertical">
																			<ul class="nav nav-tabs">
																				<li class="active"><a data-toggle="tab" href="#tab-general" aria-expanded="false"><span class="glyphicon glyphicon-signal"></span> Generral</a></li>
																				<li class=""><a data-toggle="tab" href="#tab-inventory" aria-expanded="false"><span class="glyphicon glyphicon-folder-close"></span> Inventory</a></li>
																				<li class=""><a data-toggle="tab" href="#tab-shopping" aria-expanded="true"><span class="glyphicon glyphicon-gift"></span> Shipping</a></li>
																				<li class=""><a data-toggle="tab" href="#tab-attribute" aria-expanded="true"><span class="glyphicon glyphicon-paperclip"></span> Attributes</a></li>
																			</ul>
																			<div class="tab-content">
																				<div id="tab-general" class="tab-pane active">
																					<div class="form-group col-md-12">
																						<label for="inputEmail3" class="col-sm-4 control-label">Regular Price ($)</label>
																						<div class="col-sm-8">
																						  <input type="text" class="form-control" value="<?php echo $this->input->post('regular_price', true)?>" name="regular_price">
																						  <font color="red"><?php echo form_error('regular_price'); ?></font>
																						</div>
																					</div>
																					<div class="form-group col-md-12">
																						<label class="col-sm-4 control-label">Sale Price ($)</label>
																						<div class="col-sm-8">
																						  <input type="text" class="form-control" value="<?php echo $this->input->post('sale_price', true)?>" name="sale_price">
																						  <font color="red"><?php echo form_error('sale_price'); ?></font>
																						</div>
																					</div>
																					<div class="form-group col-md-12">
																						<label class="col-sm-4 control-label">Product Type</label>
																						<div class="col-sm-8">
																						<?php 
																							$option_value =get_option('Product Status');
																							echo '<select name="pro_status" class="form-control">';
																							foreach($option_value as $value){
																								echo '<option value="'.$value['option_id'].'">'.$value['option_value'].'</option>';
																							}
																							echo '</select>';
																						?>
																						</div>
																					</div>
																					<div class="form-group col-md-12">
																						<label class="col-sm-4 control-label"><b><?php echo 'Hot Product';?></b></label>
																						<div class="col-sm-8">
																						  <input type="checkbox" id="hot_pro" value="1" name="hot_pro"> 
																						  <font color="red"><?php echo form_error('hot_pro'); ?></font>
																						</div>
																					</div>
																					<div class="form-group col-md-12">
																						<label class="col-sm-4 control-label"><b><?php echo 'Discount';?></b></label>
																						<div class="col-sm-8">
																						  <input type="checkbox" id="have_disc" value="1" name="have_disc"> 
																						  <font color="red"><?php //echo form_error('have_disc'); ?></font>
																						</div>
																					</div>
																					<div class="form-group col-md-12 is_dis show_have_dis">
																						<label class="col-sm-4 control-label"><?php echo 'From'?> :</label>
																						<div class="col-sm-4">
																							<div class="clearfix">
																								<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" value="" name="from_ddate" class="form-control datepicker"> 
																									<span class="input-group-addon">
																										<span class="glyphicon glyphicon-calendar"></span>
																									</span>
																									<font color="red"><?php echo form_error('from_ddate'); ?></font>
																								</div>
																							</div>
																						</div>
																						<div class="col-sm-4">
																							<div class="clearfix">
																								<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" class="form-control timepicker" name="from_dtime" value="">
																									<span class="input-group-addon">
																										<span class="glyphicon glyphicon-time"></span>
																									</span>
																									<font color="red"><?php echo form_error('from_dtime'); ?></font>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="form-group col-md-12 is_dis show_have_dis">
																						<label class="col-sm-4 control-label"><?php echo 'To'?> :</label>
																						<div class="col-sm-4">
																							<div class="clearfix">
																								<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" value="" name="to_ddate" class="form-control datepicker"> 
																									<span class="input-group-addon">
																										<span class="glyphicon glyphicon-calendar"></span>
																									</span>
																									<font color="red"><?php echo form_error('to_ddate'); ?></font>
																								</div>
																							</div>
																						</div>
																						<div class="col-sm-4">
																							<div class="clearfix">
																								<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" class="form-control timepicker" name="to_dtime" value="">
																									<span class="input-group-addon">
																										<span class="glyphicon glyphicon-time"></span>
																									</span>
																									<font color="red"><?php echo form_error('to_dtime'); ?></font>
																								</div>
																							</div>
																						</div>
																					</div> 
																				</div>
																				<div id="tab-inventory" class="tab-pane">
																					<div class="form-group col-md-12">
																						<label for="inputEmail3" class="col-sm-3 control-label">Stock Qty</label>
																						<div class="col-sm-9">
																						  <input type="text" class="form-control" name="stock_qty" id="inputEmail3">
																						  <font color="red"><?php echo form_error('stock_qty'); ?></font>
																						</div>
																					</div>
																					<div class="form-group col-md-12">
																						<label for="inputEmail3" class="col-sm-3 control-label">Available</label>
																						<div class="col-sm-9">
																							<?php 
																							$option_value =get_option('Stock Available');
																							echo '<select name="stock_available" class="form-control">';
																							foreach($option_value as $value){
																								echo '<option value="'.$value['option_id'].'">'.$value['option_value'].'</option>';
																							}
																							echo '</select>';
																							?>
																							<font color="red"><?php echo form_error('stock_available'); ?></font>
																						</div>
																					</div>

																				</div>
																				<div id="tab-shopping" class="tab-pane">
																					<div class="form-group col-md-12">
																						<label for="inputEmail3" class="col-sm-3 control-label">Weight (kg)</label>
																						<div class="col-sm-9">
																						  <input type="text" class="form-control" name="weight" id="inputEmail3">
																						  <font color="red"><?php echo form_error('weight'); ?></font>
																						</div>
																					</div>
																					<div class="form-group col-md-12">
																						<label for="inputEmail3" class="col-sm-3 control-label">Dimensions (cm)</label>
																						<div class="col-sm-3">
																						  <input type="text" class="form-control" name="length" id="inputEmail3" placeholder="Length">
																						  <font color="red"><?php echo form_error('length'); ?></font>
																						</div>
																						<div class="col-sm-3">
																						  <input type="text" class="form-control" name="width" id="inputEmail3" placeholder="Width">
																						  <font color="red"><?php echo form_error('width'); ?></font>
																						</div>
																						<div class="col-sm-3">
																						  <input type="text" class="form-control" name="height" id="inputEmail3" placeholder="Height">
																						   <font color="red"><?php echo form_error('height'); ?></font>
																						</div>
																					</div>
																				</div>
																				<div id="tab-attribute" class="tab-pane">
																					<div class="form-group col-md-12">
																						<div class="col-sm-6">
																							<?php  echo get_attribute_product('parent',false,has_logged('user_id') ); ?>
																						</div>
																						<div class="col-sm-3">
																							<button class="btn btn-default add_atr" name="add_atr" type="button">Add</button>
																						</div>
																						<div class="col-sm-2">
																							<img src="<?php echo base_url();?>images/loading.gif" class="load_wait" style="display:none;" />
																						</div>
																					</div>
																					<div class="form-group col-md-12">
																						<div class="col-sm-6">
																							<div class="scroll_widgets" id="show_attr">
																								<div class="parents"></div>
																							</div>
																						</div>
																						<div class="col-sm-6">
																							<div class="scroll_widgets" id="show_attr">
																								<div class="parent"></div>
																							</div>
																						</div>
																					</div>
																					
																					<div class="form-group col-md-12">
																						<div class="scroll_widgets" id="show_attr">
																							<div class="parent">
																							
																							</div>
																						</div>
																					</div>
																					
																				</div>
																			</div>
																		</div>
																	</div>
															</div>
															<!--End col-md-6 -->
														</div>
														<!-- End row -->
														
													</div>
													<!--End form-body -->
							         		</div>
							         		<!-- End Form -->
							         		<!-- Action -->
							         		<div class="col-md-4">
							         			
							         			<div class="panel panel-default">
													<div class="widget_section">
														<div class="ui-radio ui-radio-primary">
															<div class="panel-heading">
																<div class="widget_title"><h3 class="panel-title">Public</h3></div>
															</div>
															<div class="panel-body">
																<div>
																        
																	<div class="clearfix">
																		<input type="submit" name="btn_save" value="<?php echo 'save'?>" class="btn btn-primary">
																		
																		<a href="<?php echo base_url().'account/product_list';?>"><input type="button" name="btn_update" value="Cancel" class="btn btn-danger"></a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="panel panel-default">
													<div class="widget_section">
														<div class="ui-radio ui-radio-primary">
															<div class="panel-heading">
																<div class="widget_title"><h3 class="panel-title">Bidding/Auction</h3></div>
															</div>
															<div class="panel-body">
																<div>
																	<div class="clearfix">
																		<input type="checkbox" name="bidding" id="bidding" value="auction"> Post an auction for this item.<br>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="panel panel-default" id="setprice">
													<div class="widget_section">
														<div class="ui-radio ui-radio-primary">
															<div class="panel-heading">
																<div class="widget_title"><h3 class="panel-title">Set buy now price</h3></div>
															</div>
															<div class="panel-body">
																
																<div>
																	<div class="clearfix">
																		<input type="text" name="buy_now" value="" placeholder="Enter price here"><br>
																		
																	</div>
																	<div class="form-group col-md-12">
																						<label class="col-sm-12 control-label"><?php echo 'From'?> :</label>
																						<div class="col-sm-12">
																							<div class="clearfix">
																								<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" value="" name="bid_from_date" class="form-control datepicker"> 
																									<span class="input-group-addon">
																										<span class="glyphicon glyphicon-calendar"></span>
																									</span>
																									<font color="red"><?php echo form_error('from_ddate'); ?></font>
																								</div>
																							</div>
																						</div>
																						<div class="col-sm-12">
																							<div class="clearfix">
																								<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" class="form-control timepicker" name="bid_from_time" value="">
																									<span class="input-group-addon">
																										<span class="glyphicon glyphicon-time"></span>
																									</span>
																									<font color="red"><?php echo form_error('from_dtime'); ?></font>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="form-group col-md-12">
																						<label class="col-sm-12 control-label"><?php echo 'To'?> :</label>
																						<div class="col-sm-12">
																							<div class="clearfix">
																								<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" value="" name="bid_to_date" class="form-control datepicker"> 
																									<span class="input-group-addon">
																										<span class="glyphicon glyphicon-calendar"></span>
																									</span>
																									<font color="red"><?php echo form_error('to_ddate'); ?></font>
																								</div>
																							</div>
																						</div>
																						<div class="col-sm-12">
																							<div class="clearfix">
																								<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" class="form-control timepicker" name="bid_to_time" value="">
																									<span class="input-group-addon">
																										<span class="glyphicon glyphicon-time"></span>
																									</span>
																									<font color="red"><?php echo form_error('to_dtime'); ?></font>
																								</div>
																							</div>
																						</div>
																					</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="panel panel-default">
													<div class="widget_section">
														<div class="panel-heading">
															<div class="widget_title"><h3 class="panel-title">Categories</h3></div>
														</div>
														<div class="panel-body" style="height:300px; overflow:auto;">
															<div class="scroll_widget">
																<?php 
																$category_type=category_type('Product');
																echo widget_category($category_type,$this->input->post('category_id'));?>
															</div>
														</div>
														<div class="panel-body">
															<input type="checkbox" id="select"/>&nbsp;&nbsp;&nbsp;<label>Select all</label>
														</div>
													</div>
												</div>
												<div class="panel panel-default">
													<div class="widget_section">
														<div class="panel-heading">
															<div class="widget_title"><h3 class="panel-title">Feature image</h3></div>
														</div>
														<div class="panel-body" >
															<div class="fileinput fileinput-new" data-provides="fileinput">
															  <div class="fileinput-new thumbnail" style="width: 300px; height: 160px;">
																<img data-src="holder.js/100%x100%" alt="">
															  </div>
															  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 400px; max-height: 200px;"></div>
															  <div>
																<span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="userfile[]"></span>
																<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
															  </div>
															</div>
															
														</div>
													
													</div>
												</div>
												<div class="panel panel-default">
													<div class="widget_section">
														<div class="panel-heading">
															<div class="widget_title"><h3 class="panel-title">Gallery</h3></div>
														</div>
														<div class="panel-body" >
															<div class="gallery_">
																<div class="fileinput fileinput-new" data-provides="fileinput">
																	<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																		<img data-src="holder.js/100%x100%" alt="">
																	</div>
																	<div id="dvPreview"></div>
																	<div>
																		<span class="btn btn-default btn-file">
																		<span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
																		<input id="fileupload" type="file" multiple="multiple" class="file" name="gallery[]">
																		</span>
																		<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>								
												<div class="panel panel-default">
													<div class="widget_section">
														<div class="panel-heading">
															<div class="widget_title"><h3 class="panel-title">Product Tag</h3></div>
														</div>
														<div class="panel-body" >
															<div class="form-group">
																<div class="col-sm-8">
																	<input type="text" id="get_tags" class="form-control" name="tag" />
																</div>
																<div class="col-sm-3">
																	<input class="btn btn-default waves-effect add_tags" type="button" value="Add">
																</div>
															</div>
															<div class="tags_container">
																<ul class="tags_cloud" name="tags_on">
																</ul><!--/ .tags_cloud-->
															</div>
														</div>
													</div>
												</div>
							         		</div>
							         		<!-- End Action -->
							         </div>
							          <!-- End Row -->
							         




									</div>
								<!-- end portlet-body -->
							
						<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->
<script>
    var base_admin_assets_url = "<?php echo base_admin_assets_url(); ?>";
</script>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
  //tag
    $('.tags-input').tagsInput({
        width: 'auto',
        //autocomplete_url:'test/fake_plaintext_endpoint.html' //jquery.autocomplete (not jquery ui)
        autocomplete_url:"<?php echo base_admin_url().$controller_url.'save'; ?>" // jquery ui autocomplete requires a json endpoint
      });
</script>
<script type="text/javascript">
	$(document).ready(function() {		
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			startDate: '-3d'
		});
		$('.timepicker').timepicker({
			showMeridian: false,
			format: 'HH:mm',
			showSeconds: true,
			minuteStep: 1,
			secondStep: 1
		});
		
		$("#have_disc").click(function(){
			$(".show_have_dis").toggle(400);
		});
		$('#select').on('click',function(){
			if ($(this).is(':checked')) {
				$('.categories').each(function(){
					this.checked = true;
				});
			}else{
				$('.categories').each(function(){
					this.checked = false;
				});
			}
		});
		$("#fileupload").change(function () {
			if (typeof (FileReader) != "undefined") {
				var dvPreview = $("#dvPreview");
				dvPreview.html("");
				var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
				$($(this)[0].files).each(function () {
					var file = $(this);
					if (regex.test(file[0].name.toLowerCase())) {
						var reader = new FileReader();
						reader.onload = function (e) {
							var img = $("<img />");
							img.attr("style", "height:100px;width: 100px");
							img.attr("src", e.target.result);
							dvPreview.append(img);
						}
						reader.readAsDataURL(file[0]);
					} else {
						alert(file[0].name + " is not a valid image file.");
						dvPreview.html("");
						return false;
					}
				});
			} else {
				alert("This browser does not support HTML5 FileReader.");
			}
		});
		
		//################### Get value from select ###################//
		var maxappend = 0;
		$('.add_atr').click(function() {
			//alert('he');
			var id = $('#attr_name').val();
			//alert(id);
			if(id == 9){
				$.ajax({
					type:'POST',
					data:{id:id},
					url:'<?php echo site_url('/admin/product_list/attributes')?>',
					success: function(e){
						$('.parent').append(e);
					}
				});
			}
			if(id == 8){
				$.ajax({
					type:'POST',
					data:{id:id},
					url:'<?php echo site_url('/account/product_list/attributes')?>',
					success: function(e){
						$('.parents').append(e);
					}
				});
			}
		});
		

		// attach button click listener on dom ready
		$('.save').click(function(){
			var group = $('#attr_name').val();
			var color = $('input[name^=userfile]').map(function(idx, elem) {
				return $(elem).val();
			}).get();
			var price = $('input[name^=p_color]').map(function(id, eleme) {
				return $(eleme).val();
			}).get();
			var size = $('input[name^=size]').map(function(ids, elems) {
				return $(elems).val();
			}).get();
			
			if(group == 9){
				$('input[name^=str_size]').val(group + ',' + size)
			}
			if(group == 8){
				$('input[name^=str_color]').val(group + ',' + color + '/' + price)
			}

			//alert(group + ',' + color + ',' +price);
			event.preventDefault();
		});
		
		$('#attr_name').change(function(){
			$('.save').show(200);
			//$('#show_attr div').empty();
		});
		
		//####################### Product tags #######################//
		$('.add_tags').click(function(){
			var tag_id = $("#get_tags").val();
			if(tag_id ==""){
				return false;
			}else{
				var list='<li><a class="button_grey" href="#">'+tag_id+'</a>'+
						'<i class="tag_re glyphicon glyphicon-remove-circle"></i>'+
						'<input type="hidden" name="tags_con[]" value="'+tag_id+'"></li>';
				if(maxappend >=10) return;
				$(".tags_container ul").append(list);
				maxappend++;
			}
		});
		$(document).on('click','.tag_re', function(){
			$(this).parent('.tags_container ul li').remove();
		});
		$(document).on('click','.att_re', function(){
			$(this).closest('.att_on').remove();
			$(this).closest('.size_on').remove();
			//alert('ge');
		});
	});
	//CKEDITOR.replace('ckeditor');
</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
