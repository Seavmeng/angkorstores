<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deletepro extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		admin_only();
		acl($this->uri->uri_string);
		$this->load->model('admin/product/moproduct');
		$this->load->helper('product');
	}

	private $_controller_url = '';
	private $_data = array();

	function index(){
		if($this->input->post()):
			$filename=$_FILES["file"]["tmp_name"];
			if($_FILES["file"]["size"] > 0){
				$file = fopen($filename, "r");
				$flag = true;
				$i=0;
				$strList = '';
				$err='The following product code(s) already exist: <br>';
				
				while (($Data = fgetcsv($file, 10000, ",")) !== FALSE){
						if($flag) { 
							$flag = false; continue; 
						}
						$getId = $this->moproduct->get_id_by_code(trim($Data[1]));
						$product = $this->moproduct->deleteByProCode(trim($Data[1]));
						if($product){
							$i++;
						}
					}
					$this->_data['success']= $i.' products have been permanently deleted!';
				} 
		endif;
		$this->load->view('admin/product/delete_multipro', $this->_data);
	}

}
