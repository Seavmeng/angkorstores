<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {
	public function __construct(){
		parent::__construct();
		admin_only();
		acl($this->uri->uri_string);
		$this->load->model('admin/product/moproduct');
		$this->load->helper('product');
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//
	
	function index(){
		if($this->input->post()):
			$update_imp =$this->input->post('update_import');
			
			$filename=$_FILES["file"]["tmp_name"];
			if($_FILES["file"]["size"] > 0){
				$file = fopen($filename, "r");
				$flag = true;
				$i=0;
				$count=0;
				$err='The following product code(s) already exist: <br>';
				
				while (($Data = fgetcsv($file, 10000, ",")) !== FALSE){
					$count ++;
					if($flag) {
						$flag = false; continue; 
					}
					$pro_code = array();
					$product_code=trim($Data[0]);
					$product_name=trim($Data[1]);
					$detail=trim($Data[2]);
					$description=trim($Data[3]);
					$status=$Data[4];
					$regular_price = $Data[5];
					$sale_price = $Data[6];
					$stock_qty = $Data[7];
					$weight = $Data[8];
					$length = $Data[9];
					$width = $Data[10];
					$height = $Data[11];
					$category_id = $Data[12];
					$supplier_id = $Data[13];
					$hot_pro = $Data[14];
					$contact_now = $Data[15];
					$pro_tags =$Data[16];
					//field_name,from,condition
					$this->db->select_max('product_id');
					$this->db->from('sys_product');
					$query = $this->db->get();
					$getQuery = $query->first_row('array');
					$pro_id =$getQuery['product_id']+1;
					
					if($update_imp !=1){
						$check_field_dup=$this->moproduct->check_duplicate('product_id','product_code','sys_product',$product_code);
						$product_id=$check_field_dup['product_id'];
						//------permalink-----
						$permalink =permalink($product_name);
						$data_link = $this->moproduct->check_permalink('sys_product',$permalink);
						if($permalink == $data_link){
							$url_link= $count.$permalink;
						}else{
							$url_link= $permalink;
						}
						if($product_id){
							$err .='<strong>'.$product_code.'</strong><br>';
							//$pro_code[] = $product_code;
							$this->_data['error'] = $err;
						}else{
							$product_code = $product_code =="" ? $pro_id : $product_code;
							$SQLDATA = array(
								'product_code'=> $product_code,
								'product_name'=> $product_name,
								'detail'=> $detail,
								'description'=>$description,
								'status'=>$status,
								'currency_id'=>'1',
								'view'=>'0',
								'created_date'=>date('Y-m-d H:i'),
								'user_id'=>get_user_id(),
								'supplier_id'=>$supplier_id,
								'contact-type'=>$contact_now,
								'permalink'=>$url_link,
								'tags'=>$pro_tags
							);
							 
							$result = $this->moproduct->save($SQLDATA);
							
							if($result){
								$product_id=$result;
								// 1 for uncategorized
								
								$cat_id_arrays = explode(",",$category_id);
								foreach($cat_id_arrays as $cid){
									$this->db->select('category_id');
									$this->db->from('tbl_categories');
									$this->db->where('category_id',$cid);
									$query = $this->db->get();
									$getQuery = $query->first_row('array');
									$get_category_id = $getQuery['category_id'];
									$cate_id = $get_category_id !=""  ? $get_category_id : default_procategory();
									$this->db->insert('sys_relationship',array('post_id'=>$product_id,'category_id'=>$cate_id));
								}
								$SQLDATA_inven = array(
									'product_id'=>$product_id,
									'regular_prices'=>$regular_price,
									'sale_price'=>$sale_price,
									'stock_qty'=>$stock_qty,
									'weight'=>$weight,
									'length'=>$length,
									'width'=>$width,
									'height'=>$height,
									'hot_pro'=>$hot_pro
									
								);
								$inventory=$this->db->insert('sys_inventory',$SQLDATA_inven);
							}
							$i++;
							$this->_data['success']= $i.' products have been successfully imported!';
							
						}
					}else{ //---------update import----------
						$check_field_dup=$this->moproduct->check_duplicate('product_id','product_code','sys_product',$product_code);
						$product_id=$check_field_dup['product_id'];
						//------permalink-----
						$permalink =permalink($product_name);
						$data_link = $this->moproduct->check_permalink('sys_product',$permalink);
						if($permalink == $data_link){
							$url_link= $count.$permalink;
						}else{
							$url_link= $permalink;
						}
						
						if($product_id){
							$SQLDATA = array(
								'product_code'=> $product_code,
								'product_name'=> $product_name,
								'detail'=> $detail,
								'description'=>$description,
								'status'=>$status,
								'currency_id'=>'1',
								'view'=>'0',
								'created_date'=>date('Y-m-d H:i'),
								'user_id'=>get_user_id(),
								'supplier_id'=>$supplier_id,
								'contact-type'=>$contact_now,
								'permalink'=>$url_link,
								'tags'=>$pro_tags
							);
							$result = $this->moproduct->update($product_id,$SQLDATA);
							
							if($result){
								$this->db->where('post_id', $product_id);
								$this->db->delete('sys_relationship');
								// 1 for uncategorized
								$catid_arrays = explode(",",$category_id);
								foreach($catid_arrays as $catid){
								$this->db->select('category_id');
								$this->db->from('tbl_categories');
								$this->db->where('category_id',$catid);
								$query = $this->db->get();
								$getQuery = $query->first_row('array');
								$get_category_id = $getQuery['category_id'];
								
								$cate_id = $get_category_id !=""  ? $get_category_id : default_procategory();
								$this->db->insert('sys_relationship',array('post_id'=>$product_id,'category_id'=>$cate_id));
								}
								
								$SQLDATA_inven = array(
									'product_id'=>$product_id,
									'regular_prices'=>$regular_price,
									'sale_price'=>$sale_price,
									'stock_qty'=>$stock_qty,
									'weight'=>$weight,
									'length'=>$length,
									'width'=>$width,
									'height'=>$height,
									'hot_pro'=>$hot_pro
									
								);
								$inventory=$this->moproduct->update_inv($product_id,$SQLDATA_inven);

							}
							$i++;
							$this->_data['success']= $i.' products have been update successfully imported!';
							
						}
					}
				}
			}
				 
			
		endif;
		$this->load->view('admin/settings/import_data', $this->_data);
	}
}
