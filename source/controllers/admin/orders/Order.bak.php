<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {


	function __construct()	{
		parent::__construct();
	        acl($this->uri->uri_string);
		$this->load->helper('product');
		$this->load->model('admin/morder');
		$this->_controller_url = $this->uri->segment(2).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->morder->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('category');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );

		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}
	
	// function index(){
	// 	$this->_data['rmsg']=$this->session->flashdata('msg');
	// 	$this->_variable();
	// 	$user_id = user_id_logged();
	// 	$this->_data['data'] = $this->moproduct->show_all($user_id);
	// 	$base_url = account_url().$this->_controller_url.'?';
	// 	$this->moproduct->count_all = true;
	// 	$this->_data['page'] = $this->moproduct->page($base_url,$this->moproduct->show_all($user_id));
	// 	$this->_data['showing'] =$this->moproduct->showing($this->moproduct->show_all($user_id));
		
	// 	$this->load->view('admin/orders/show_order',$this->_data);
	// }
	public function index(){
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$userid = $this->session->userdata('group_id');
		if($userid == 1){
			$data['billing'] = $this->morder->show_all();
		}else{
			$data['billing'] = $this->morder->show_one(get_user_id());
		}
		$this->load->view('admin/orders/show_order',$data);		
	}
	public function find_id(){
		$key1 = $this->input->post('find_id');
		$keywor= array('ID','id', ',' ,'!', '&', '?', '/', '/\/', ':', ';', '#', '<', '>', '=', '^', '@', '~', '`', '[', ']', '{', '}');
		$keyword = str_replace($keywor,"", $key1);
		$data['billing'] = $this->morder->find_orderid($keyword);
		$this->load->view('admin/orders/show_order',$data);
	}
	function view($bill_id){
		$data['post'] = $this->morder->getpost();
		$data['billing'] = $this->morder->show_detail($bill_id);
		$data['order_info'] = $this->morder->order_info($bill_id);
		$this->load->view('admin/orders/detail_order',$data);
	}
	function delete_order($bill_id){
		$delete_order = $this->morder->delete_order($bill_id);
		if($delete_order){
			$this->session->set_flashdata('msg','Congratulation! your data '.has_del());
			redirect('admin/orders/order');
		}
	}
	public function send_mail() { 

         $from_email = get_option_value('email');
         //var_dump($from_email);exit();
         $to_email = $this->input->post('email'); 
         $subject = $this->input->post('title');
         
         $name = $this->session->userdata('username');
         $message = $this->input->post('description');

         $this->load->library('email'); 
         $this->email->from($from_email, 'Angkorstores-'.$name); 
         $this->email->to($to_email);
         $this->email->subject($subject); 
         $this->email->message($message); 
   		
         //Send mail 
         //$result = $this->email->send();
     	 if($this->email->send()){
			$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
										    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
										    <strong>Success!</strong> Your mail has been sent successfully!
										  </div>');
			redirect(base_url().'admin/orders/order'); 
		 }else{
			$this->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissable text-center">
										    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
										    <strong>Warning!</strong> Can not send Mail ! '.$this->email->print_debugger().'</div>'
										 );
			redirect(base_url().'admin/orders/order');
		 }
    }   

    public function getselect(){
  	   $id = $this->input->get('id');
  	   $result = $this->db->where('category_name',$id)->get('tbl_categories')->row();
  	   echo json_encode($result);
    }
 
}