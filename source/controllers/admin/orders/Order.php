<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {


	function __construct()	{
		parent::__construct();
		//acl($this->uri->uri_string);
		$this->load->helper('product');
		$this->load->model('admin/morder');
		$this->_controller_url = $this->uri->segment(2).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	
	}
	
	private $_controller_url = '';
	private $_data = array();

	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->morder->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('category');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );

		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}
	
	public function index(){ 
		$this->_variable();
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$userid = $this->session->userdata('group_id');
		if($userid == 1){
			$data['billing'] = $this->morder->show_all();
			$base_url = base_admin_url().$this->_controller_urls;
			$this->morder->count_all = true;
			$data['page'] = $this->morder->page($base_url,$this->morder->show_all());
			$data['showing'] = $this->morder->showing($this->morder->show_all());
		}else{
			$data['billing'] = $this->morder->show_one(get_user_id());
			$base_url = base_admin_url().$this->_controller_urls;
			$this->morder->count_all = true;
			$data['page'] = $this->morder->page($base_url,$this->morder->show_one(get_user_id()));
			$data['showing'] = $this->morder->showing($this->morder->show_one(get_user_id()));
		}

		//var_dump($data['billing']);exit();
		$this->load->view('admin/orders/show_order',$data);		
	}
	
	public function find_id(){ 
		$key1 = $this->input->post('find_id');
		$keywor= array('ID','id', ',' ,'!', '&', '?', '/', '/\/', ':', ';', '#', '<', '>', '=', '^', '@', '~', '`', '[', ']', '{', '}');
		$keyword = str_replace($keywor,"", $key1);
		$data['billing'] = $this->morder->find_orderid($keyword);
		$this->load->view('admin/orders/show_order',$data);
	}
	
	function view($bill_id){
		$data['post'] = $this->morder->getpost();
		$data['billing'] = $this->morder->show_detail($bill_id);
		$data['order_info'] = $this->morder->order_info($bill_id);
		$this->load->view('admin/orders/detail_order',$data);
	}
	
  
	
	 function print_pdf(){
		$this->load->library('Pdf');
		
		 
		
		
		exit;
		var_dump($page);exit;
		
		
		//tcpdf();
		$obj_pdf = new Pdf('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "PDF Report";
		$obj_pdf->SetTitle($title);
		/* $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN)); */
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->AddPage();
		ob_start();
			// we can have any view part here like HTML, PHP etc
			$content = $page;
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output('output.pdf', 'I');
		

		}
	

	 
	function delete_order($bill_id){
		$delete_order = $this->morder->delete_order($bill_id);
		if($delete_order){
			$this->session->set_flashdata('msg','Congratulation! your data '.has_del());
			redirect('admin/orders/order');
		}
	}
	
	public function send_mail() { 

         $from_email = get_option_value('email');
         //var_dump($from_email);exit();
         $to_email = $this->input->post('email'); 
         $subject = $this->input->post('title');
         
         $name = $this->session->userdata('username');
         $message = $this->input->post('description');

         $this->load->library('email'); 
         $this->email->from($from_email, 'Angkorstores-'.$name); 
         $this->email->to($to_email);
         $this->email->subject($subject); 
         $this->email->message($message); 
   		
         //Send mail 
         //$result = $this->email->send();
     	 if($this->email->send()){
			$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
										    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
										    <strong>Success!</strong> Your mail has been sent successfully!
										  </div>');
			redirect(base_url().'admin/orders/order'); 
		 }else{
			$this->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissable text-center">
										    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
										    <strong>Warning!</strong> Can not send Mail ! '.$this->email->print_debugger().'</div>'
										 );
			redirect(base_url().'admin/orders/order');
		 }
    }   

    public function getselect(){
  	   $id = $this->input->get('id');
  	   $result = $this->db->where('category_name',$id)->get('tbl_categories')->row();
  	   echo json_encode($result);
    }
 
}