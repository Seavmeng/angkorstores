<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Switch_Language extends CI_Controller
{

    function index($language = "") {
    	if($language == 'khmer' || $language == 'english'){
    		 $language = ($language != "") ? $language : "english"; 
    		 $this->session->set_userdata('sys_lang', $language);
        	 redirect(base_url().config_item('admin_dir').'dashboard');
    	}else{
    		die('No support for this language ');
    	}
       
       
    }
}