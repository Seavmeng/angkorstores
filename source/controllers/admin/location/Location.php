<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Location extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/location/mlocation');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	
	private $_controller_url = '';
	private $_data = array();

	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mlocation->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('location');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );
		
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}

	private function _validation_create(){
		$this->form_validation->set_rules('location_name',word_r('location_name'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('location_name',word_r('location_name'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	public function index2(){
		
	}

	private function _child_location($parent_id,$level){
		$this->db->select();
		$this->db->from('sys_location');
		$this->db->where(array('parent_id'=>$parent_id));
		$this->db->order_by('loc_id','DESC');
		$result = $this->db->get();
		$category_array = $result->result_array();
		$output ="";
		foreach ($category_array as $value) {
			$location_id = $value['loc_id'];
			$location_name = $value['location_name'];
			//$location_slug = $value['location_slug'];
			$description = $value['description'];
			$output .= '<tr>';

			$output .= '<td>'.form_checkbox(array('name'=>'id[]','value'=>$location_id,'class'=>'checkboxes')).'</td>';

			$output .='<td';
			$output .=' id="'.$location_id.'" class="opt-child" >';
			$output .= str_repeat(" * ", $level);
			$output .= $location_name.'</td>';


			$output .='<td ';
			$output .=' id="'.$location_id.'" class="opt-parent" >';
			$output .= $description.'</td>';

			
			$output .='<td ';
			$output .=' id="'.$location_id.'" class="opt-parent" >';
			$output .= btn_action($this->_controller_url.'update/'.$location_id, $this->_controller_url.'delete/'.$location_id).'</td>';

			$output .=$this->_child_location($location_id,$level+1);
			$output .= '</tr>';
		}
		return $output;
	}

	public function index($name="location"){
		//$category_group_id = sys_config('category_group_id');
		$this->_variable();
		$per_page = get_option_value('perpage');
		$page = $this->input->get('page');
		$page = real_int($page);
		$this->db->select();
		$this->db->from('sys_location');
		$this->db->where(array('parent_id'=>0));
		$this->db->order_by('loc_id','DESC');
		$this->db->limit($per_page,$page);
		$result = $this->db->get();
		$location_array = $result->result_array();
		$output ="";
		$output .='<table class="table table-bordered table-hover sort-table" name="'.$name.'">';
		$output .= '<thead><tr>';
				$output .= '<th>'.$this->_data['check_ml'].'</th>';
				$output .= '<th>'.word_r('location_name').'</th>';
				$output .= '<th>'.word_r('description').'</th>';
				//$output .= '<th>'.word_r('slug').' </th>';
				$output .= '<th>'.word_r('action').' </th>';
		$output .= '</tr></thead>';
		foreach ($location_array as  $value) {
			$location_id = $value['loc_id'];
			$location_name = $value['location_name'];
			//$location_slug = $value['location_slug'];
			$description = $value['description'];
			$output .='<tr>';

			$output .= '<td>'.form_checkbox(array('name'=>'id[]','value'=>$location_id,'class'=>'checkboxes')).'</td>';

			$output .='<td ';
			$output .=' id="'.$location_id.'" class="opt-parent" >';
			$output .= $location_name.'</td>';

			$output .='<td ';
			$output .=' id="'.$location_id.'" class="opt-parent" >';
			$output .= $description.'</td>';


		
			$output .='<td ';
			$output .=' id="'.$location_id.'" class="opt-parent" >';
			$output .= btn_action($this->_controller_url.'update/'.$location_id, $this->_controller_url.'delete/'.$location_id).'</td>';
			
			$output .= $this->_child_location($location_id,$level=1);
			$output .='</tr>';
		}
		$output .='</table>';
		$this->_data['data'] = $output;
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->mlocation->count_all = true;
		$this->mlocation->per_page = $per_page;
		$this->_data['page'] = $this->mlocation->page($base_url,$this->mlocation->count_all());
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);

	}

	public function create(){
		$this->_variable();
		if($this->input->post()){
			if($this->_validation_create()){
				//$slug = slug($this->input->post('location_name', true) );
				$SQLDATA = array(
							'location_name'=> $this->input->post('location_name', true),
							/*'location_slug'=> $slug,*/
							'description'=> $this->input->post('description', true),
							'parent_id'=> real_int($this->input->post('parent') )
							//'category_group_id'=> sys_config('category_group_id')
				);
				$result = $this->mlocation->save($SQLDATA);

				if($result){
					// update location_slug if exist 
					//$category_group_id = sys_config('category_group_id');
					$this->db->select('location_name')->from('sys_location')->where(array('location_name'=>$location_name,'loc_id !='=>$result));
					$query = $this->db->get();
					$count = $query->num_rows();
					if($count > 0){
						$this->db->where('loc_id',$result);
						$this->db->update('sys_location',array('location_name'=>$location_name.'-'.$result));
					}
					// update category_slug if exist 
					redirect(base_admin_url().$this->_controller_url);
				}
			}			
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);		
	}

	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mlocation->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				if($this->_validation_update()){
					$this->input->post('location_name', true);
					$SQLDATA = array(
								'location_name'=> $this->input->post('location_name', true),
								//'category_slug'=> $slug,
								'description'=> $this->input->post('description', true),
								'parent_id'=> real_int($this->input->post('parent') )
								//'category_group_id'=> sys_config('category_group_id')
					);
					$result = $this->mlocation->update($id,$SQLDATA);

					if($result){
						// update category_slug if exist 
						//$category_group_id = sys_config('category_group_id');
						$this->db->select('loc_id')->from('sys_location')->where(array('loc_id !='=>$id));
						$query = $this->db->get();
						$count = $query->num_rows();
						if($count > 0){
							$this->db->where('loc_id',$id);
							$this->db->update('sys_location',array('loc_id'=>$id));
						}
						// update category_slug if exist 
						redirect(base_admin_url().$this->_controller_url);
					}
				}
			endif;
		  $this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	public function ml(){
		$action = $_POST['ml'];
		$id = $_POST['id'];
		$option = '';
		if(!empty($action) && !empty($id)){
			switch ($action) {
				case 'delete':
					$option = 'delete';
					break;
				case 'inactive':
						$option = 0;
						break;
			}

			
			if($option == 'delete'){
					//********* Check if delete location_id update parent_id to 0 *************//
					$this->db->where_in('parent_id',$id);
					$this->db->update('sys_location',array('parent_id'=>0));
					//********* Check if delete location_id update parent_id to 0 *************//

					//*********** update where location_id in tbl_links update to uncategorized *******//
					$this->db->where_in('loc_id',$id);
					$this->db->update('tbl_links',array('loc_id'=>1));
					//*********** update where location_id in tbl_links update to uncategorized *******//
			}
			

			//********* Check if delete location_id where id in //
			$this->db->where_in('loc_id',$id);
			if($option == 'delete'){
				$this->db->delete('sys_location');
			}
			//********* Check if delete location_id where id in //

			if($this->db->affected_rows()){
				redirect(base_admin_url().$this->_controller_url);
			}
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
	}

	public function delete($id = null){
		//Unable delete main category group
		if($id == 1){
			redirect(base_admin_url().$this->_controller_url);
		}
		$result = $this->mlocation->delete($id);
		if($result){
			//********* Check if delete location_id update parent_id to 0 *************//
			$this->db->where('parent_id',$id);
			$this->db->update('sys_location',array('parent_id'=>0));
			//********* Check if delete location_id update parent_id to 0 *************//
			
			//*********** update where location_id in tbl_links update to uncategorized *******//
			//$this->db->where('loc_id',$id);
			//$this->db->update('tbl_links',array('loc_id'=>1));
			//*********** update where location_id in tbl_links update to uncategorized *******//
			redirect(base_admin_url().$this->_controller_url);
		}
		
		
	}

	public function search(){
		$this->_variable();
		$location_name = $this->input->get('location_name');
		$this->_data['data'] = $this->mlocation->search($location_name);
		$this->mlocation->count_all = true;
		$data = $this->mlocation->search($location_name);
		$base_url = base_admin_url().$this->_controller_url.'search?location_name='.$location_name.'&';
		$this->_data['showing'] = $this->mlocation->showing($data);
		$this->_data['page'] = $this->mlocation->page($base_url,$data);
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list_search', $this->_data);
	}
	
	
	
}
