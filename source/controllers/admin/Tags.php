<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tags extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/mtag');
		$this->_controller_url = $this->uri->segment(2).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mtag->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('tags');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-tags';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );
	
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}

	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('tag',word_r('tag'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('tag',word_r('tag'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	//*********************** Validation **************************//


	//*********************** List ****************************//
	public function index(){
		$this->_variable();

		$this->_data['data'] = $this->mtag->all();
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->mtag->count_all = true;
		$this->_data['showing'] = $this->mtag->showing($this->mtag->all());
		$this->_data['page'] = $this->mtag->page($base_url,$this->mtag->all());
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	}
	//*********************** List ****************************//
	//************************ Create ***************************//
	public function create(){
		$this->_variable();
		if($this->input->post()){
			if($this->_validation_create()){
				$slug = slug($this->input->post('tag', true) );
				$SQLDATA = array(
							'tag_name'=> $this->input->post('tag', true),
							'tag_slug'=> $slug,
							'description'=> $this->input->post('description', true)
				);
				$result = $this->mtag->save($SQLDATA);

				if($result){
					// update tag_slug if exist 
					$this->db->select('tag_slug')->from('tbl_tags')->where(array('tag_slug'=>$slug,'tag_id !='=>$result));
					$query = $this->db->get();
					$count = $query->num_rows();
					if($count > 0){
						$this->db->where('tag_id',$result);
						$this->db->update('tbl_tags',array('tag_slug'=>$slug.'-'.$result));
					}
					// update tag_slug if exist 
					redirect(base_admin_url().$this->_controller_url);
				}
			}			
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);		
	}

	//************************ Create ***************************//

	//************************ Update **************************//
	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mtag->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				if($this->_validation_update()){
						$slug = slug($this->input->post('tag', true) );
						$SQLDATA = array(
									'tag_name'=> $this->input->post('tag', true),
									'tag_slug'=> $slug,
									'description'=> $this->input->post('description', true)
						);
						$result = $this->mtag->update($id,$SQLDATA);

						if($result){
							// update tag_slug if exist 
							$this->db->select('tag_slug')->from('tbl_tags')->where(array('tag_slug'=>$slug,'tag_id !='=>$id));
							$query = $this->db->get();
							$count = $query->num_rows();
							if($count > 0){
								$this->db->where('tag_id',$id);
								$this->db->update('tbl_tags',array('tag_slug'=>$slug.'-'.$id));
							}
							// update tag_slug if exist 
							redirect(base_admin_url().$this->_controller_url);
						}
				}
			endif;
		  $this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	//************************ Update **************************//

	// Multiple Action
	public function ml(){
		$action = $_POST['ml'];
		$id = $_POST['id'];
		$option = '';
		if(!empty($action) && !empty($id)){
			switch ($action) {
				case 'delete':
					$option = 'delete';
					break;
				case 'inactive':
						$option = 0;
						break;
			}
			$this->db->where_in('tag_id',$id);
			if($option == 'delete'){
				$this->db->delete('tbl_tags');
			}
			if($this->db->affected_rows()){
				redirect(base_admin_url().$this->_controller_url);
			}
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
	}
	// Multiple Action

	// **************** Call Back Validation Function For Update **************//
	public function check_tag_name($user_group_name){
		$id = $this->uri->segment(5);
		$this->db->select('user_group_name')->from('sys_user_groups')->where(array('user_group_name' => $user_group_name,'user_group_id !=' => $id));
		$query = $this->db->get();
		$count = $query->num_rows(); 
		if($count > 0){
			$this->form_validation->set_message('check_user_group_name', 'The '.word_r('user_group_name').' field must contain a unique value');
			return false;
		}
	}

	// **************** Call Back Validation Function For Update **************//

	//********************* Delete *****************//

	public function delete($id = null){

		$result = $this->mtag->delete($id);
		if($result){
			redirect(base_admin_url().$this->_controller_url);
		}
		
		
	}

	//********************* Delete *****************//


	//********************** Search ***********************//
	public function search(){
		$this->_variable();
		$tag_name = $this->input->get('tag');
		$this->_data['data'] = $this->mtag->search($tag_name);
		$this->mtag->count_all = true;
		$data = $this->mtag->search($tag_name);
		$base_url = base_admin_url().$this->_controller_url.'search?tag='.$tag_name.'&';
		$this->_data['showing'] = $this->mtag->showing($data);
		$this->_data['page'] = $this->mtag->page($base_url,$data);

		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);

	}
	//********************** Search ***********************//

}
