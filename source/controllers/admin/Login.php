<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/mlogin');
                $this->load->helper('product');
		$this->load->helper('password');
	}

	public function index(){
		$data = array();
		if($this->input->post('login')){
			$this->form_validation->set_rules('username','User name','required|trim|xss_clean');
			$this->form_validation->set_rules('password','Password','required|trim|xss_clean');
			if($this->form_validation->run() == FALSE){
				$data['errors'] = validation_errors();
			}else{
					$username = $this->input->post('username',true);
					$password = $this->input->post('password',true);
					$username = $this->db->escape_str($username);
					$password = $this->db->escape_str($password);
					$user = $this->mlogin->login($username,$password);
					if($user){
						$passHashed = $user['password'];
						//check passwordhash
						if(validate_password($password, $passHashed) ==1){
							$group_id = $user['group_id'];
							//Check Group Active or Not
							$query = $this->db->get_where('sys_user_groups',array('user_group_id'=>$group_id));
							$get = $query->first_row('array');
							if($get['user_group_active'] == 1){
								$this->session->set_userdata('user_id',$user['user_id']);
								$this->session->set_userdata('group_id',$user['group_id']);
								$this->session->set_userdata('username',$user['username']);
								if($user['group_id']==1){
									redirect(base_admin_url().'dashboard');
								}else{
									redirect(base_url());
								}
							}else{
								$data['errors']= 'Your Group No Longer in System!';
							}
							
							
						}else{
							$data['errors']=word_r('invalid_login');
						}
					}else{
						$data['errors']=word_r('invalid_login');
					}
			}
		}
		$data['rmsg']=$this->session->flashdata('msg');
		$this->load->view(config_item('admin_dir').'login',$data);
	}
}
?>