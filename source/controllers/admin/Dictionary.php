<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dictionary extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/mdictionary');
		$this->_controller_url = $this->uri->segment(2).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mdictionary->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('dictionary');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-book';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );
	
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}

	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('english',word_r('english'),'required|trim|xss_clean|is_unique[tbl_dictionary.english]');
		$this->form_validation->set_rules('type',word_r('type'),'required|trim|xss_clean');
		$this->form_validation->set_rules('khmer',word_r('khmer'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('english',word_r('english'),'required|xss_clean|trim|callback_check_english');
		$this->form_validation->set_rules('type',word_r('type'),'required|trim|xss_clean');
		$this->form_validation->set_rules('khmer',word_r('khmer'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	//*********************** Validation **************************//


	//*********************** List ****************************//
	public function index(){
		$this->_variable();

		$this->_data['data'] = $this->mdictionary->all();
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->mdictionary->count_all = true;
		$this->_data['showing'] = $this->mdictionary->showing($this->mdictionary->all());
		$this->_data['page'] = $this->mdictionary->page($base_url,$this->mdictionary->all());
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	}
	//*********************** List ****************************//
	//************************ Create ***************************//
	public function create(){
		$this->_variable();
		if($this->input->post()){
			if($this->_validation_create()){

				$SQLDATA = array(
							'english'=> $this->input->post('english', true),
							'type'=>$this->input->post('type', true),
							'khmer'=> $this->input->post('khmer', true)
				);
				$result = $this->mdictionary->save($SQLDATA);

				if($result){
					redirect(base_admin_url().$this->_controller_url.'create');
				}
			}			
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);		
	}

	//************************ Create ***************************//

	//************************ Update **************************//
	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mdictionary->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				if($this->_validation_update()){
					$SQLDATA = array(
								'english'=> $this->input->post('english', true),
								'type'=>$this->input->post('type', true),
								'khmer'=> $this->input->post('khmer', true)
					);
						$result = $this->mdictionary->update($id,$SQLDATA);

						if($result){
							
							redirect(base_admin_url().$this->_controller_url);
						}
				}
			endif;
		  $this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	//************************ Update **************************//

	// Multiple Action
	public function ml(){
		$action = $_POST['ml'];
		$id = $_POST['id'];
		$option = '';
		if(!empty($action) && !empty($id)){
			switch ($action) {
				case 'delete':
					$option = 'delete';
					break;
				case 'inactive':
						$option = 0;
						break;
			}
			$this->db->where_in('id',$id);
			if($option == 'delete'){
				$this->db->delete('tbl_dictionary');
			}
			if($this->db->affected_rows()){
				redirect(base_admin_url().$this->_controller_url);
			}
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
	}
	// Multiple Action

	// **************** Call Back Validation Function For Update **************//
	public function check_english($english){
		$id = $this->uri->segment(4);
		$this->db->select('english')->from('tbl_dictionary')->where(array('english' => $english,'id !=' => $id));
		$query = $this->db->get();
		$count = $query->num_rows(); 
		if($count > 0){
			$this->form_validation->set_message('check_english', 'The '.word_r('english').' field must contain a unique value');
			return false;
		}
	}

	// **************** Call Back Validation Function For Update **************//

	//********************* Delete *****************//

	public function delete($id = null){

		$result = $this->mdictionary->delete($id);
		if($result){
			redirect(base_admin_url().$this->_controller_url);
		}
		
		
	}

	//********************* Delete *****************//


	//********************** Search ***********************//
	public function search(){
		$this->_variable();
		$name = $this->input->get('name');
		$this->_data['data'] = $this->mdictionary->search($name);
		$this->mdictionary->count_all = true;
		$data = $this->mdictionary->search($name);
		$base_url = base_admin_url().$this->_controller_url.'search?tag='.$name.'&';
		$this->_data['showing'] = $this->mdictionary->showing($data);
		$this->_data['page'] = $this->mdictionary->page($base_url,$data);

		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);

	}
	//********************** Search ***********************//

}
