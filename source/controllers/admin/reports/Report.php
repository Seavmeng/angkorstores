<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	function __construct()	{
		parent::__construct();
	//    acl($this->uri->uri_string);
		$this->load->helper('product');
		$this->load->model('admin/morder');
		$this->_controller_url = $this->uri->segment(2).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	
	}

	private $_controller_url = '';
	private $_data = array();
	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->morder->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('category');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );

		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}
	
	public function index(){
		$this->_variable();
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$userid = $this->session->userdata('group_id');
		$data['billing'] = $this->morder->show_all();
		$data['count_show'] = $this->morder->count_show_all();
		$base_url = base_admin_url().$this->_controller_urls;
		$this->morder->count_all = true;
		$data['page'] = $this->morder->page($base_url,$this->morder->show_all());
		$data['showing']= $this->morder->showing($this->morder->show_all());
		$this->load->view('admin/reports/show_product',$data);		
	}
	public function find_id(){ 
		$this->_variable();
		$key1 = $this->input->get('find_id');
		$from = $this->input->get('from');
		$to = $this->input->get('to');
		$userid = get_user_id();
		
		$keywor= array('ID','id', ',' ,'!', '&', '?', '/', '/\/', ':', ';', '#', '<', '>', '=', '^', '@', '~', '`', '[', ']', '{', '}');
		$keyword = str_replace($keywor,"", $key1);
		
		if($userid ==1 || $userid ==71){
			$data['billing'] = $this->morder->find_orderid($keyword, $from, $to);
			$base_url = base_admin_url().$this->_controller_urls.$this->uri->segment(4).'?find_id='.$keyword.'&from='.$from.'&to='.$to.'&';
			$this->morder->count_all = true;
			$data['page'] = $this->morder->page($base_url,$this->morder->find_orderid($keyword, $from, $to));
			$data['showing']= $this->morder->showing($this->morder->find_orderid($keyword, $from, $to));
			$data['count_show'] = $this->morder->count_show_all();
		}
		else{
			$data['billing'] = $this->morder->find_order_buyer($keyword, $from, $to);
			$base_url = base_admin_url().$this->_controller_urls.$this->uri->segment(4).'?find_id='.$keyword.'&from='.$from.'&to='.$to.'&';
			$this->morder->count_all = true;
			$data['page'] = $this->morder->page($base_url,$this->morder->find_order_buyer($keyword, $from, $to));
			$data['showing']= $this->morder->showing($this->morder->find_order_buyer($keyword, $from, $to));
			$data['count_show'] = $this->morder->count_show_one(get_user_id());
		}

		$this->load->view('admin/orders/show_order',$data);
	}
	
	function view($bill_id){
		$data['post'] = $this->morder->getpost();
		$data['billing'] = $this->morder->show_detail($bill_id);
		$data['order_info'] = $this->morder->order_info($bill_id);
		$this->load->view('admin/orders/detail_order',$data);
	}
	function delete_order($bill_id){
		$delete_order = $this->morder->delete_order($bill_id);
		if($delete_order){
			$this->session->set_flashdata('msg','Congratulation! your data '.has_del());
			redirect('admin/orders/order');
		}
	}
	public function send_mail() { 

         $from_email = get_option_value('email');
         $to_email = $this->input->post('email'); 
         $subject = $this->input->post('title');
         $name = $this->session->userdata('username');
         $message = $this->input->post('description');
         $this->load->library('email'); 
         $this->email->from($from_email, 'Angkorstores-'.$name); 
         $this->email->to($to_email);
         $this->email->subject($subject); 
         $this->email->message($message); 
     	 if($this->email->send()){
			$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
										    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
										    <strong>Success!</strong> Your mail has been sent successfully!
										  </div>');
			redirect(base_url().'admin/orders/order'); 
		 }else{
			$this->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissable text-center">
										    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
										    <strong>Warning!</strong> Can not send Mail ! '.$this->email->print_debugger().'</div>'
										 );
			redirect(base_url().'admin/orders/order');
		 }
    }   

    public function getselect(){
  	   $id = $this->input->get('id');
  	   $result = $this->db->where('category_name',$id)->get('tbl_categories')->row();
  	   echo json_encode($result);
    }
	public function daily(){
		$this->_variable();
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$userid = $this->session->userdata('group_id');
		$data['billing'] = $this->morder->show_daily();
		$base_url = base_admin_url().$this->_controller_urls.'daily';
		$this->morder->count_all = true;
		$data['page'] = $this->morder->page($base_url,$this->morder->show_daily());
		$data['showing']= $this->morder->showing($this->morder->show_daily()); 
		$this->load->view('admin/reports/daily_report',$data);
    }
	public function daily_search(){
		$this->_variable();
		$from = $this->input->get('from');
		if($from ==""){
			redirect(base_admin_url().'reports/report/daily');
		}
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$userid = $this->session->userdata('group_id');
		$data['billing'] = $this->morder->daily_search($from);
		$data['billing_detail'] = $this->morder->daily_search_bill($from);
		$this->load->view('admin/reports/daily_report_search',$data);
    }
 	public function monthly($cu_month=null){

		$this->_variable();
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$userid = $this->session->userdata('group_id');
		$data['cur_month'] = $cu_month;
		$data['billing'] = $this->morder->show_monthly();
		$base_url = base_admin_url().$this->_controller_urls.'monthly';
		$this->morder->count_all = true;
		$data['page'] = $this->morder->page($base_url,$this->morder->show_monthly());
		$data['showing']= $this->morder->showing($this->morder->show_monthly()); 
		$this->load->view('admin/reports/monthly_report',$data);
    }
	public function monthly_search(){
		$this->_variable();
		$from = $this->input->get('from');
		if($from ==""){
			redirect(base_admin_url().'reports/report/monthly');
		}
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$userid = $this->session->userdata('group_id');
		$data['billing'] = $this->morder->monthly_search($from);
		$data['billing_detail'] = $this->morder->month_search_bill($from);
		$this->load->view('admin/reports/monthly_report_search',$data);
    }
    public function suppliers(){
		$this->_variable();
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$data['suppliers'] = $this->morder->show_supplier();
		$data['get_supplier'] = $this->morder->get_supplier();
		$base_url = base_admin_url().$this->_controller_urls.'suppliers';
		$this->morder->count_all = true;
		$data['page'] = $this->morder->page($base_url,$this->morder->show_supplier());
		$data['showing']= $this->morder->showing($this->morder->show_supplier()); 
		$this->load->view('admin/reports/show_product_supplier',$data);
    }
	public function search_code(){
		$this->_variable();
		$key1 = $this->input->get('search_code');
		$supplier=$this->input->get('supplier');
	
		$keywor= array('ID','id', ',' ,'!', '&', '?', '/', '/\/', ':', ';', '#', '<', '>', '=', '^', '@', '~', '`', '[', ']', '{', '}');
		$keyword = str_replace($keywor,"", $key1);
		$data['suppliers'] = $this->morder->find_code_name($supplier,$keyword);
		
		$data['get_supplier'] = $this->morder->get_supplier();
		$base_url = base_admin_url().$this->_controller_urls.$this->uri->segment(4).'?search_code='.$keyword.'&';
		
		
		$data['page'] = $this->morder->page($base_url,$this->morder->find_code_name($supplier,$keyword));
		$data['showing']= $this->morder->showing($this->morder->find_code_name($supplier,$keyword));
		$this->load->view('admin/reports/show_product_supplier',$data);
	}
	function view_pro($pro_id){
		$data['rows'] = $this->morder->view_pro($pro_id);
		$this->load->view('admin/reports/show_detail_supplier',$data);
	}
}