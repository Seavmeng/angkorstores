<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Trackorder extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/trackorder/mtrackorder');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}


	// //*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mtrackorder->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('track');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );
		
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}

	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('location_name',word_r('location_name'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}
        public function index(){
                $this->_data['rmsg']=$this->session->flashdata('msg');
		$data['data']=$this->mtrackorder->show_all();
		$this->load->view('admin/trackorder/all_trackorder',$data);
		
	}
	public function create(){
		//$this->_variable();
		$data['get_ornumber'] = $this->mtrackorder->get_ordernum();
		$this->form_validation->set_rules('titles','titles','required');
		if($this->form_validation->run()==false){
			$this->_data['errors'] = validation_errors();
			$this->load->view('admin/trackorder/show_trackorder',$data);
		}else{
			$data = array(
					'order_id' => $this->input->post('id'),
					'title' => $this->input->post('titles'),
					'description' => $this->input->post('description'),
					'created_date'=> date("Y-m-d H:i:s")
					);
			$this->mtrackorder->add_trackorder($data);
			redirect(base_url().'admin/trackorder/trackorder/create',$data);
		}	
	}
        public function delete($id = null){
		$delete_track = $this->mtrackorder->deletetrackorder($id);
		if($delete_track){
			$this->session->set_flashdata('msg','Congratulation! your data '.has_del());
			redirect('admin/trackorder/trackorder');
		}
	}	
}
