<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/publish/mpage');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mpage->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('pages');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-rocket';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$this->_data['check_ml_categories'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.categories'));
		$option = array(
						''=>'----',
						'public'=>word_r('visibility_public'),
						'private'=>word_r('visibility_private'),
						'delete'=>word_r('delete')
				  );
		$option_search = array(
						''=>'----',
						'public'=>word_r('visibility_public'),
						'private'=>word_r('visibility_private')
				  );
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
		$this->_data['option_search'] = form_dropdown('visibility',$option_search,$this->input->get('visibility'),'class="form-control" ');
	}

	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('title',word_r('title'),'required|trim|xss_clean');
		$this->form_validation->set_rules('publish_date',word_r('publish_date'),'required|trim|xss_clean');
		$this->form_validation->set_rules('time',word_r('time'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('title',word_r('title'),'required|trim|xss_clean');
		$this->form_validation->set_rules('publish_date',word_r('publish_date'),'required|trim|xss_clean');
		$this->form_validation->set_rules('time',word_r('time'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	//*********************** Validation **************************//


	//*********************** List ****************************//
	public function index(){
		$this->_variable();

		$this->_data['data'] = $this->mpage->join();
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->mpage->count_all = true;
		$this->_data['showing'] = $this->mpage->showing($this->mpage->join());
		$this->_data['page'] = $this->mpage->page($base_url,$this->mpage->join());
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	}
	//*********************** List ****************************//
	//************************ Create ***************************//
	public function create(){
		$this->_variable();
		if($this->input->post()){
			if($this->_validation_create()){
				$image = upload();
				if(!empty($image)){
					$image = implode(',', $image);
				}else{
					$image = '';
				}
				$post_slug = slug($this->input->post('title', true));
				$publish_date = strtotime($this->input->post('publish_date', true));
				$publish_date = date('Y-m-d',$publish_date).' '.$this->input->post('time', true);
				$SQLDATA = array(
							'post_title'=> $this->input->post('title', true),
							'post_slug'=> $post_slug,
							//'description'=> $this->input->post('description', true),
							'article'=> $this->input->post('article', true),
							'post_type'=> $this->mpage->post_type,
							'publish_date'=> $publish_date,
							'user_id'=> $this->session->userdata('user_id'),
							'thumbnail'=> $image,
							//'allow_comment'=>$comment,
							'visibility'=> $this->input->post('visibility', true)
							//'slide_type'=>$slide_type,
							//'feature'=>$feature,
							//'gallery'=>$gallery
						);
				$post_id = $this->mpage->save($SQLDATA);
				if($post_id){

					//************ Check if exist post slug *************//
					$query = $this->db->get_where('tbl_posts',array('post_type'=>$this->mpage->post_type,'post_slug'=>$post_slug,'post_id !='=>$post_id));
					$count = $query->num_rows();
					if($count > 0){
						$this->mpage->update($post_id,array('post_slug'=>$post_slug.'_'.$post_id));
					}
					//************ Check if exist post slug *************//

					redirect(base_admin_url().$this->_controller_url);
				}
			}			
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);		
	}

	//************************ Create ***************************//

	//************************ Update **************************//
	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mpage->find_by_id($id);
		$post_id = $id;
		if($this->_data['data']){
			if($this->input->post()){
				if($this->_validation_update()){
					$image = upload();
					if(!empty($image)){
						$image = implode(',', $image);
					}else{
						$image = $this->input->post('image', true);
					}
					$post_slug = slug($this->input->post('title', true));
					$publish_date = strtotime($this->input->post('publish_date', true));
					$publish_date = date('Y-m-d',$publish_date).' '.$this->input->post('time', true);
					$SQLDATA = array(
								'post_title'=> $this->input->post('title', true),
								'post_slug'=> $post_slug,
								//'description'=> $this->input->post('description', true),
								'article'=> $this->input->post('article', true),
								'post_type'=> $this->mpage->post_type,
								'publish_date'=> $publish_date,
								'user_id'=> $this->session->userdata('user_id'),
								'thumbnail'=> $image,
								//'allow_comment'=>$comment,
								'visibility'=> $this->input->post('visibility', true)
								//'slide_type'=>$slide_type,
								//'feature'=>$feature,
								//'gallery'=>$gallery
							);
					$this->mpage->update($post_id,$SQLDATA);
					if($post_id){

						//************ Check if exist post slug *************//
						$query = $this->db->get_where('tbl_posts',array('post_type'=>$this->mpage->post_type,'post_slug'=>$post_slug,'post_id !='=>$post_id));
						$count = $query->num_rows();
						if($count > 0){
							$this->mpage->update($post_id,array('post_slug'=>$post_slug.'_'.$post_id));
						}
						//************ Check if exist post slug *************//

						redirect(base_admin_url().$this->_controller_url);
					}
				}			
			}
		  $this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	//************************ Update **************************//

	// Multiple Action
	public function ml(){
		$action = $_POST['ml'];
		$id = $_POST['id'];
		var_dump($id);
		$option = '';
		$delete = '';
		if(!empty($action) && !empty($id)){
			switch ($action) {
				case 'public':
					$option = 1;
					break;
				case 'private':
						$option = 0;
						break;
				case 'delete':
						$delete = 1;
						break;
			}
			$this->db->where_in('post_id',$id);
			if($delete == 1){
				$this->db->delete('tbl_posts');
				//************* Delete in tbl_menus ***********//
				$this->db->where_in('page_id', $id);
				$this->db->delete('tbl_menus');
				//************* Delete in tbl_menus ***********//
			}else{
				$this->db->update('tbl_posts',array('visibility'=>$option));
			}
			if($this->db->affected_rows()){
		

				redirect(base_admin_url().$this->_controller_url);
			}
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
	}
	// Multiple Action

	//********************* Delete *****************//

	public function delete($id = null){

		$result = $this->mpage->delete($id);
		if($result){
			//************* Delete in tbl_menus ***********//
			$this->db->where('page_id', $id);
			$this->db->delete('tbl_menus');
			//************* Delete in tbl_menus ***********//
			redirect(base_admin_url().$this->_controller_url);

		}
		
		
	}

	//********************* Delete *****************//


	//********************** Search ***********************//
	public function search(){
		$this->_variable();
		// $this->mpage->per_page = 2;
		$username = $this->input->get('username');
		$user_id = $this->input->get('user_id');
		
		$visibility = $this->input->get('visibility');
		$text = $this->input->get('text',true);

		//From Date
		$from = $this->input->get('from', true);
		if(!empty($from)){
			$from = strtotime($from);
			$from = date('Y-m-d',$from);
		}
		
		//From Date

		//To Date
		$to = $this->input->get('to', true);
		if(!empty($to)){
			$to = strtotime($to);
			$to = date('Y-m-d',$to);	
		}
		
		//To Date

		$tag_id = $this->input->get('tag_id');
		$user_active = $this->input->get('user_active');
		$this->_data['data'] = $this->mpage->search($user_id,$from,$to,$visibility,$text);
		$this->mpage->count_all = true;
		$data = $this->mpage->search($user_id,$from,$to,$visibility,$text);
		$base_url = base_admin_url().$this->_controller_url.'search?user_id='.$user_id.'&from='.$from.'&to='.$to.'&visibility='.$visibility.'&text='.$text.'&';
		$this->_data['showing'] = $this->mpage->showing($data);
		$this->_data['page'] = $this->mpage->page($base_url,$data);

		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);

	}
	//********************** Search ***********************//


}
