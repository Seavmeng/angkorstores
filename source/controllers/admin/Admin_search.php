<?php 
class admin_search extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('search_model');
		$this->load->model('admin/product/moproduct');
		$this->_controller_url = $this->uri->segment(2);
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3);
	}
	
	private function _varaible(){
		$page = $this->input->get('start',true);	
		$page = ($page) ? $page: 0;
		$page = real_int($page);
		$this->search_model->start = $page;
	}
	
	public function index(){
		$this->_varaible();
		$user_id = user_id_logged();
		$group_id = $this->session->userdata('group_id');
			
		$strSearch = $this->input->get('search');
		$data['results'] = $this->search_model->index($strSearch);
		
		$base_url = account_url().$this->_controller_url.'?search='.$strSearch.'&';
		$this->search_model->count_all = true;
		$strCat = $this->input->post('product_cat');
		$data['page'] = $this->moproduct->page($base_url,$this->search_model->index($strSearch));
		$data['showing'] =$this->moproduct->showing($this->search_model->index($strSearch));
		$data['searchFor']=$strSearch;

		$this->load->view('admin/product/search_product', $data);	
	}
	
	public function filterSearch(){
		$this->_varaible();
		$status = $this->input->get('status');
		$strSearch = $this->input->get('search');
        $str_percent = $this->input->get('discount_percent');
		$max_discount = $this->input->get('max_discount');
		$data['results'] = $this->search_model->filterSearch($strSearch, $status,$str_percent,$max_discount);
		$base_url = base_admin_url().$this->_controller_urls.$this->uri->segment(4).'?status='.$status.'&discount_percent='.$str_percent.'&max_discount='.$max_discount.'&search='.$strSearch.'&';
		$this->search_model->count_all = true;
		$data['page'] = $this->moproduct->page($base_url, $this->search_model->filterSearch($strSearch, $status,$str_percent, $max_discount));
		$data['showing'] = $this->moproduct->showing($this->search_model->filterSearch($strSearch, $status,$str_percent, $max_discount));
		$data['searchFor']=$strSearch;
		$data['status'] = $status;
		$this->load->view('admin/product/search_product', $data);
	}
	
	public function find_supplier_product(){
		$this->_varaible();
		$user_id = user_id_logged();
		$group_id = $this->session->userdata('group_id');	
		$strSearch = $this->input->get('search');
		$strVendor = $this->input->get('search_vendor');
		$strStatus = $this->input->get('status');
		
		$data['data'] = $this->search_model->vendorProduct($strSearch, $strVendor, $strStatus);

		$data['searchFor']=$strSearch;
		$base_url = base_admin_url().$this->_controller_urls.'?status='.$strStatus.'&search='.$strSearch.'&search_vendor='.$strVendor.'&';
		$this->search_model->count_all = true;
	
		$data['page'] = $this->search_model->page($base_url, $this->search_model->vendorProduct($strSearch, $strVendor, $strStatus));
		$data['showing'] =$this->search_model->showing($this->search_model->vendorProduct($strSearch, $strVendor, $strStatus));

		$this->load->view('admin/product/supplier_product', $data);
		
	}

}

?>