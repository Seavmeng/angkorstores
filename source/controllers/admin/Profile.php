<?php
class Profile extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('email');
		$this->load->model(array('admin/CI_captcha','admin/muser'));
		$this->load->helper('captcha');
		$this->load->helper('password');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('category');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );
		
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}

	public function index(){

		if(has_logged()){
			redirect(base_url());
		}
		$package_id=1;
		$sub_data['cap_img'] = $this ->CI_captcha->make_captcha();
		$this->form_validation->set_rules('username','username','required|trim|xss_clean|max_length[20]');
		//$this->form_validation->set_rules('checkout','checkout','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('email','email','required|trim|xss_clean|valid_email|max_length[50]|is_unique[sys_users.email]');
		$this->form_validation->set_rules('password','password','required|trim|xss_clean|min_length[8]|max_length[20]|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm','password_conf','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('captcha', 'Captcha', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE){
		//	$sub_data['errors']=validation_errors();
		}else{
			if($this->input->post()){

			$password=$this->input->post('password', true);
			if(checkpassword($password)) {
				$sub_data['errors']=checkpassword($password);
			}else{

				if($this->CI_captcha->check_captcha()==TRUE){
             
					$toemail = $this->input->post('email', true);
					$From =get_option_value('email');
					$ActiveCode=actived_email($toemail);
					$resetCode=reset_code($toemail);
					$MsgContent='
                         <div>
							<div>Dear, User</div>
							<div style="padding:10px 10px 10px 0;color:#365ab5;font-size:16px;">Who We Are!</div>
							<div style="padding:0px 10px 10px 0;text-align: justify;">
								Angkorstores.com is the E-Commerce website opened worldwide and based in Cambodia. The project is invested by Mom Varin Investment (www.varinholdings.com), which has its affiliated companies – V Japan Tech Inter (www.vjpti.com) and Asia-Pacific Fredfort International School (www.afisedu.com). V Japan Tech Inter is the leading company of LPG and fuel equipment and services while Asia-Pacific Fredfort International School is an IB PYP Curriculum School in Cambodia. 

								<div style="padding:10px 10px 10px 0;color:#365ab5;font-size:16px">Congratulations !</div>
							</div>
                        <div style="padding:10px 10px 0 0;">Your user name: '.$toemail.'</div>
						<div style="padding:10px 10px 0 0;">Your Password: ******</div>
						</div>
<p>
						Click <a href="'.account_url().'register/active/'.$ActiveCode.'">Re Login<a> to activated your account.</p>
						<br>
					';
					$MsgContent.='  
                                        <div style="padding:10px 10px 0 0;color:#375BB5;font-size:14px;border-bottom:1px solid #375BB5;padding-bottom:10px;margin-top:20px;">
						Best Regards,
					</div>
					<table width="100%">
						<tbody>
						<tr>
							<td width="160">
								<img width="150" src="'.base_url().'images/logo.png">
							</td>
							<td>
								<font color="#375BB5">
								A: #25, St 19 Sangkat ChakTomuk,<br>
												&nbsp;&nbsp;&nbsp;&nbsp;	 Khan Daun Penh, Phnom Penh, Cambodia<br>
												M: (+855)095 78 78 65 / 016 78 78 75<br>
								E: <u>info@smartbtoc.com</u><br>
								W: <u>www.smartbtoc.com</u>
								</font>
							</td>
						</tr>
						<tr>
							<td colspan="2">
							<br>
							Feel free to contact with us without any risk!<br>
							------------------------------------------------------------
							</td>
						</tr>
					</table>';
					
					$this->email->from($From);
					$this->email->to($toemail);
					$this->email->subject("Actived your Account");
					$this->email->message($MsgContent);
					$this->email->set_mailtype('html');
				/*	$register_type=$this->input->post('checkout', true);
					if($register_type !=1){
						$get_user_type=$this->input->post('checkout', true);
					}else{
						$get_user_type=3;
					}*/
					$get_user_type=6;
					if($this->email->send()){     				
						$SQLDATA = array(
							  "AccountID" =>userID(),
							  "username" => $this->input->post('username', true),
							  "password" => create_hash($this->input->post('password', true)),
							  "email" => $this->input->post('email', true),
							  "group_id" =>$get_user_type,
							  'user_active'=>$ActiveCode,
							  'ResetCode'=>$resetCode,
							  'package_id'=>$package_id,
							  'iscomplete'=>'no',
							  "created_date" => date('Y-m-d H:i')
						);
						$result = $this->muser->save($SQLDATA);
						if($result){
							$sub_data['success'] = "Thank you for join us! .Please check your mail to activate account.";
							$this->session->set_flashdata('msg',$sub_data['success']);
							redirect(account_url());
						}else{
							$sub_data['errors'] ="Fail to send try again later.";	
						}
					
					}else{
						$sub_data['errors'] = "Sorry Unable to send email...";
					}
				}else{
					$sub_data['errors'] = "invalid_captcha";
				}
			}
			}
		}
		$sub_data['user_role']= $this->muser->users_role();
		$this->load->view('admin/register',$sub_data);
	}
		//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('first_name',word_r('first_name'),'required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('last_name',word_r('last_name'),'required|trim|xss_clean|max_length[20]');
		//$this->form_validation->set_rules('email',word_r('email'),'required|trim|xss_clean|valid_email|max_length[50]|is_unique[sys_user_detail.email]');
		//	$this->form_validation->set_rules('company',word_r('company'),'required|trim|xss_clean|max_length[20]');
		//	$this->form_validation->set_rules('businessType',word_r('businessType'),'required|trim|xss_clean|max_length[20]');

		$this->form_validation->set_rules('street',word_r('street'),'required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('district',word_r('district'),'required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('city',word_r('city'),'required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('country',word_r('country'),'required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('countryCode',word_r('countryCode'),'trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('phoneNumber',word_r('phoneNumber'),'required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('fax',word_r('fax'),'trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('companyInfo',word_r('companyInfo'),'trim|xss_clean|max_length[2000]');
		if($this->form_validation->run() == FALSE){
	
			return validation_errors();
		}else{
			return true;
		}
	}
	private function _validation_create_guest(){
		$this->form_validation->set_rules('first_name',word_r('first_name'),'required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('last_name',word_r('last_name'),'required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('street',word_r('street'),'required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('district',word_r('district'),'required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('city',word_r('city'),'required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('country',word_r('country'),'required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('countryCode',word_r('countryCode'),'trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('phoneNumber',word_r('phoneNumber'),'required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('fax',word_r('fax'),'trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('companyInfo',word_r('companyInfo'),'trim|xss_clean|max_length[2000]');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}
	function info(){
		logged_only();
		if(is_complete()){
			redirect(base_url());
		}
		$this->_variable();
		$user_id=get_user_id();
		$sub_data="";
		if($this->input->post()){
			if($this->_validation_create_guest()){
				$user_id=get_user_id();
					//---------featured image ----
					$image_featured = upload();
					if(!empty($image_featured)){
						$image_featured = date('m-Y').'/thumb/'.implode(',', $image_featured);
					}else{
						$image_featured = '';
					}
					$page=makeitseo($this->input->post('first_name', true).'.'.$this->input->post('last_name', true));
					if(check_exist_company($page) !=""){
						$sub_data['errors']=check_exist_company($page);
					}else{
						$SQLDATA = array(
						  "user_id" => $user_id,
						  "FirstName" => $this->input->post('first_name', true),
						  "LastName" => $this->input->post('last_name', true),
						  "email" => $this->input->post('email', true),
						  "gender"=> $this->input->post('gender', true),
						  "profile" =>$image_featured,
						  "CompanyName" =>makeitseo($this->input->post('first_name', true).'.'.$this->input->post('last_name', true)),
						  "zip" =>$this->input->post('countryCode', true),
						  "Phone" =>$this->input->post('phoneNumber', true),
						  "Fax" =>$this->input->post('fax', true),
						  "street" =>$this->input->post('street', true),
						  "district" =>$this->input->post('district', true),
						  "city" =>$this->input->post('city', true),
						  "country" =>$this->input->post('country', true),
						  "description" =>$this->input->post('companyInfo', true),
						  "complete_date" => date('Y-m-d H:i')
						);
						$result = $this->muser->saveShop($SQLDATA);
						if($result==true){
							$SQLdata = array(
								'iscomplete'=>'yes'
							);
							$resultComp = $this->muser->complete($user_id,$SQLdata);
							if($result==true){
									$this->db->where("user_id",$user_id);
									$this->db->update("sys_users",array("online_status"=>1)); 
									redirect(base_url());
								$sub_data['success'] = "Congratulation !.You successful complete register. Go <a hre=".$account_url().">login</a>";
							}
						}else{
							$sub_data['errors'] = validation_errors();
						}
					}
			}
		}
		$sub_data['data']=$this->muser->find_profile_id($user_id);
		$this->load->view('admin/guest_info',$sub_data);	
	}
	function reset_pwd(){
        if(has_logged()){
			redirect(base_url());
		}
		$this->form_validation->set_rules('email',word_r('email'),'required|trim|xss_clean|valid_email|max_length[50]');
		$this->form_validation->set_rules('captcha', 'Captcha', 'required|xss_clean');
		if ($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
		}else{
			if($this->input->post()){
				if($this->CI_captcha->check_captcha()==TRUE){
					$email= $this->input->post('email', true);
					if(check_email($email)==null){
						$this->_data['errors'] = "<span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span> Sorry, this account is not in our records, please try again.";
					}else{
						$resetCode=resetCode($email);
						$get_from= get_option_value('email');
					        $From =$get_from['description'];
						$MsgContent='
							 <div>
								<div>Dear, User</div>
								<div style="padding:10px 10px 10px 0;color:#365ab5;font-size:16px;">Who We Are!</div>
								<div style="padding:0px 10px 10px 0;text-align: justify;">
									SMARTBTOC  offers a great service website such as Post services, Users Profile, Tracking visitors, Auto Post to Facebook. and marketing of your website etc.We strive to offer the best solution for your business. We are constantly investigating new technologies and recommend them when they make sense.<br/>
									We can help our customers increase their business outputs through an application of the latest technical tools and a careful handling of the changing dynamic of offshore quality service to customers to conduct their businesses even more creatively and effectively. 

									<div style="padding:10px 10px 10px 0;color:#365ab5;font-size:16px">Congratulations !</div>
								</div>
							</div>
							<p>
							Click <a href="'.account_url().'register/recovery/'.$resetCode.'">Reset New Password<a> to get new password.
							</p>
							<div style="padding:10px 10px 0 0;color:#375BB5;font-size:14px;border-bottom:1px solid #375BB5;padding-bottom:10px;margin-top:20px;">
								Best Regards,
							</div><br/>
							<table width="100%">
								<tbody>
								<tr>
									<td width="160">
										<img width="150" src="'.base_url().'images/logo.png">
									</td>
									<td>
										<font color="#375BB5">
										A: #25, St 19 Sangkat ChakTomuk,<br>
														&nbsp;&nbsp;&nbsp;&nbsp;	 Khan Daun Penh, Phnom Penh, Cambodia<br>
														M: (+855)095 78 78 65 / 016 78 78 75<br>
										E: <u>info@smartbtoc.com</u><br>
										W: <u>www.smartbtoc.com</u>
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="2">
									<br>
									Feel free to contact with us without any risk!<br>
									------------------------------------------------------------
									</td>
								</tr>
							</table>
						';
						$this->email->from($From);
						$this->email->to($email);
						$this->email->subject("Reset your password");
						$this->email->message($MsgContent);
						$this->email->set_mailtype('html');
						if($this->email->send()){ 
							$this->_data['success'] = "Please check your email to recovery.Good luck ):";
							$this->session->set_flashdata('msg',$this->_data['success']);
							redirect(account_url());
						}else{
							$this->_data['errors'] ="Fail to send try again later.";	
						}
					}
				}else{
					$this->_data['errors'] = word_r("invalid_captcha");
				}
			}
		}
		$this->_data['cap_img'] = $this ->CI_captcha->make_captcha();
		$this->load->view('admin/users/reset_password',$this->_data);
	}
	function recovery($id){
		$this->form_validation->set_rules('password',word_r('password'),'required|trim|xss_clean|min_length[8]|max_length[20]|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm',word_r('password_confirm'),'required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('email',word_r('email'),'required|trim|xss_clean|valid_email|max_length[50]');
		$this->db->select('ResetCode,email');
		$this->db->from('sys_users');
		$this->db->where('ResetCode',$id);
		$active=$this->db->get();
		$get_activeCode = $active->first_row('array');
		$ResetCode=$get_activeCode['ResetCode'];
		if(empty($id) ||empty($ResetCode)){
			redirect(account_url());
		}else{
			if($this->input->post()){
				$email= $this->input->post('email', true);
				$password= $this->input->post('password', true);
				if($email != $get_activeCode['email']){
					$this->_data['errors']='Invalid email! Please check it again.';
				}else{
					if(!checkpassword($password)){
						$resetCode=md5(date('YmHisa').$email);
						if ($this->form_validation->run() == FALSE){
							$this->_data['errors'] = validation_errors();
						}else{
							$SQLdata = array(
								'user_password'=>create_hash($this->input->post('password', true)),
								'ResetCode'=>$resetCode,
								'modified_date'=> date('Y-m-d H:i')
							);
							$result = $this->muser->recovery($email,$id,$SQLdata);
							if($result){
								$this->_data['success'] = "Congratulation! .You successful reset password, please login.";
                                                                $this->session->set_flashdata('msg',$this->_data['success']);
								redirect(account_url());
							}else{
								$this->_data['errors']='Fail Please try again later.';
							}
						}
					}else{
						$this->_data['errors']=checkpassword($password);
					}
				}
			}
			$this->_data['id']=$id;
			$this->load->view('admin/users/recovery_password',$this->_data);
		}
	}
	function profile(){
		logged_only();
		$user_id=get_user_id();
		$userRole=rolename($user_id);
		$this->_data['data']=$this->muser->find_profile_id($user_id);
		if($userRole=='Administrator' || $userRole=='Shop'){
			if($this->input->post()){
				if($this->_validation_create()){
					$user_id=get_user_id();
					//---------featured image ----
					$image_featured = upload();
					if(!empty($image_featured)){
						$image_featured = date('m-Y').'/thumb/'.implode(',', $image_featured);
					}else{
						$image_featured = '';
					}
					//---------gallery ----
					$image_gallery = galleries();
					if(!empty($image_gallery)){
						$image_gallery = date('m-Y').'/thumb/'.implode(',', $image_gallery);
					}else{
						$image_gallery = '';
					}
					$page=makeitseo($this->input->post('company', true));
					if(check_exist_company($page) !=""){
						$this->_data['errors']=check_exist_company($page);
					}else{
						$SQLDATA = array(
						  "FirstName" => $this->input->post('first_name', true),
						  "LastName" => $this->input->post('last_name', true),
						  "email" => $this->input->post('email', true),
						  "profile" =>$image_featured,
						  "CompanyName" =>makeitseo($this->input->post('company', true)),
						  'licence'=>$image_gallery,
						  "BusinessType" =>$this->input->post('businessType', true),
						  "countryCode" =>$this->input->post('countryCode', true),
						  "Phone" =>$this->input->post('phoneNumber', true),
						  "Fax" =>$this->input->post('fax', true),
						  "street" =>$this->input->post('street', true),
						  "district" =>$this->input->post('district', true),
						  "city" =>$this->input->post('city', true),
						  "country" =>$this->input->post('country', true),
						  "description" =>$this->input->post('companyInfo', true),
						  "modifiedComplete_date" => date('Y-m-d H:i')
						);
						$result = $this->muser->updateShop($user_id,$SQLDATA);
						if($result==true){
							$this->_data['success'] = "Congratulation !.You successful update.";
						}
					}
					$this->load->view('admin/users/shop_profile',$this->_data);
				}
			}	
			$this->load->view('admin/users/shop_profile',$this->_data);
		}elseif($userRole=='Guest'){
			if($this->input->post()){
				if($this->_validation_create_guest()){
					$user_id=get_user_id();
					//---------featured image ----
					$image_featured = upload();
					if(!empty($image_featured)){
						$image_featured = implode(',', $image_featured);
					}else{
						$image_featured = '';
					}
					$page=makeitseo($this->input->post('first_name', true).'.'.$this->input->post('last_name', true));
					if(check_exist_company($page) !=""){
						$sub_data['errors']=check_exist_company($page);
					}else{
						$SQLDATA = array(
						  "FirstName" => $this->input->post('first_name', true),
						  "LastName" => $this->input->post('last_name', true),
						  "gender"=> $this->input->post('gender', true),
						  "profile" =>date('m-Y').'/thumb/'.$image_featured,
						  "CompanyName" =>makeitseo($this->input->post('first_name', true).'.'.$this->input->post('last_name', true)),
						  "countryCode" =>$this->input->post('countryCode', true),
						  "Phone" =>$this->input->post('phoneNumber', true),
						  "Fax" =>$this->input->post('fax', true),
						  "street" =>$this->input->post('street', true),
						  "district" =>$this->input->post('district', true),
						  "city" =>$this->input->post('city', true),
						  "country" =>$this->input->post('country', true),
						  "description" =>$this->input->post('companyInfo', true),
						  "modifiedComplete_date" => date('Y-m-d H:i')
						);
						$result = $this->muser->updateShop($user_id,$SQLDATA);
						if($result==true){
							$this->_data['success'] = "Congratulation !.You successful update.";
						}
					}
				}
			}
			$this->load->view('admin/users/guest_profile',$this->_data);
		}else{
			redirect(account_url());
		}
	}
}
?>
