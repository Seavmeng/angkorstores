<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logout extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/mlogin');
	}

	public function index(){
		$this->session->sess_destroy();
		session_destroy();
		redirect(base_admin_url());
		
	}

}
?>