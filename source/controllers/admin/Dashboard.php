<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('Search_model');
	}

	public function index(){
		$this->load->library('nativesession');
		$user_id=get_user_id();
		//$this->nativesession->set('xxx','Good');
		$_SESSION['xxx'] = $this->session->userdata['group_id'];
		$data['total_product'] = $this->Search_model->count_total_product();
		$data['pending_product'] = $this->Search_model->count_pending_product();
		$data['approved_product'] = $this->Search_model->count_approved_product();
		$data['order_product'] = $this->Search_model->count_order_product();
		$data['wishlist_product'] = $this->Search_model->count_wishlist_product();
		$data['point'] = $this->get_user_reward($user_id);
		$data['reward'] = $this->get_reward_point();
		$this->load->view(config_item('admin_dir').'dashboard',$data);
	}
	
	public function get_user_reward($id) {
		$result = $this->db->select("reward_point")->from("sys_users")->where("user_id",$id)->get()->row();
		return $result;
	}
	
	public function get_reward_point() {
		$result = $this->db->get("sys_point_rewards")->row();
		return $result;
	}
	
	
	public function add($para=null){
		$this->load->view(config_item('admin_dir').'dashboard');
	}
	
	public function message(){
		$this->form_validation->set_rules('name','Name','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('email','Email','required|valid_email|trim|xss_clean|max_length[50]');
		$this->form_validation->set_rules('phone','Contact Person','required|trim|xss_clean|max_length[25]');
		$this->form_validation->set_rules('subject','Service Name','required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('message','Requirements','required|trim|xss_clean|max_length[2000]');
		if($this->form_validation->run() == FALSE){
			echo validation_errors();
		}else{
			$email= 'mrphengkim@gmail.com';
			$From = $this->input->post('email');
			$MsgContent='
				<div>From : '.$From.'</div>
				<div>Email : '.$From.'</div>
				<div>Phone : '.$this->input->post('phone').'</div>
				<hr>
				<div>Subject: '.$this->input->post('subject').'</div>
				<div>Message : '.$this->input->post('message').'</div>
			';
			$this->email->from($From);
			$this->email->to($email);
			$this->email->subject("Contact Us");
			$this->email->message($MsgContent);
			$this->email->set_mailtype('html');
			if($this->email->send()){ 
			//	$this->_data['success'] = "Congratulation you successfully submit ):";
				echo "YES";
			}else{
			//	$sub_data['errors'] ="Fail to send try again later.";	
				 echo "No";
			}
		}
	}
	
	public function redirect(){
		redirect(base_admin_url().'dashboard');
	}

	public function collapse(){
		if($this->input->cookie('collapse',true) == 'sidebar-collapsed'){
 			delete_cookie("collapse");
 		}else{
 			$cookie= array(
		      'name'   => 'collapse',
		      'value'  => 'sidebar-collapsed',
		       'expire' => '86500',
		  );
		  $this->input->set_cookie($cookie);
 		}
	}

}
?>