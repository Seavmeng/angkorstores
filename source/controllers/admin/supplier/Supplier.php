<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Supplier extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/user/muser');
		$this->load->helper('password');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	
	private $_controller_url = '';
	private $_data = array();

	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->muser->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('user');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-users';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'active'=>word_r('active'),
						'inactive'=>word_r('inactive')
				  );
		$option_search = array(
						''=>'----',
						'active'=>word_r('active'),
						'inactive'=>word_r('inactive')
				  );
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
		$this->_data['option_search'] = form_dropdown('user_active',$option_search,$this->input->get('user_active'),'class="form-control" ');
	}

	private function _validation_create(){
		$this->form_validation->set_rules('username',word_r('username'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('username',word_r('username'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	public function index(){
		$this->_variable();
		$this->_data['data'] = $this->muser->supplier();
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->muser->count_all = true;
		$this->_data['showing'] = $this->muser->showing($this->muser->supplier());
		$this->_data['page'] = $this->muser->page($base_url,$this->muser->supplier());
		$this->load->view('admin/supplier/list', $this->_data);
	}
	
	public function create(){
		$this->_variable();
		if($this->input->post()){
			if($this->_validation_create()){
				$SQLDATA = array(
					  //"first_name" => $this->input->post('first_name', true),
					  "AccountID" => userID(),
					  "username" => $this->input->post('username', true),
					  "password" => create_hash($this->input->post('username', true)),
					  "group_id" => user_group_id('Supplier'),
					  "user_active" => $this->input->post('user_active', true),
					  "register_date" => date('Y-m-d H:i'),
					  "bio" => $this->input->post('bio', true)
				);
				$result = $this->muser->save($SQLDATA);
				if($result){
					$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    New Supplier has been created successfully!
												  </div>');
					redirect(base_admin_url().$this->_controller_url);
				}
			}			
		}
		$this->load->view(config_item('admin_dir').'supplier/create', $this->_data);		
	}
	public function update($id = null){
		$this->_variable();
		$result_password = '';
		$this->_data['data'] = $this->muser->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				if($this->_validation_update()){
					$SQLDATA = array(
						  "username" => $this->input->post('username', true),
						  "user_active" => $this->input->post('user_active', true),
						  "bio" => $this->input->post('bio', true)
					);
					$result = $this->muser->update($id,$SQLDATA);
					if($result){
						$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    The supplier has been updated successfully!
												  </div>');
						redirect(base_admin_url().$this->_controller_url);
					}
				}
			endif;
		  $this->load->view(config_item('admin_dir').'supplier/update', $this->_data);
		}else{
			redirect(base_admin_url().'supplier');
		}
		
	}

	public function delete($id = null){

		$result = $this->muser->delete($id);
		if($result){
			$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    The Supplier has been deleted successfully!
												  </div>');
			redirect(base_admin_url().$this->_controller_url);
		} 
	}
	public function search(){
		$this->_variable();
		$username = $this->input->get('username');
		$user_active = $this->input->get('user_active');
		$this->_data['data'] = $this->muser->search($username,"",$user_active);
		$this->muser->count_all = true;
		$data = $this->muser->search($username, " " , $user_active);
		$base_url = base_admin_url().$this->_controller_url.'search?username='.$username.'&user_active='.$user_active.'&';
		$this->_data['showing'] = $this->muser->showing($data);
		$this->_data['page'] = $this->muser->page($base_url,$data);
		$this->load->view(config_item('admin_dir').'supplier/list', $this->_data);
	} 
}
