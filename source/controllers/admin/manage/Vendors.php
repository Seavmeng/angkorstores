<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendors extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/vendors/vendor_model');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	public function index(){
		$this->_data['lists'] = $this->vendor_model->index();
		
		$this->load->view('admin/vendors/index', $this->_data);
	}
	public function disable($param){
		$disabled = $this->vendor_model->disable($param, array('page_status'=>0));
		$this->session->set_flashdata('msg', 'A vendor has been disabled!');
		$this->_data['msg_disabled'] = $this->session->flashdata('msg');
		redirect($_SERVER['HTTP_REFERER']);
	}
	public function enable($param){
		$enabled = $this->vendor_model->disable($param, array('page_status'=>1));
		$this->session->set_flashdata('msg', 'A vendor has been successfully enabled!');
		$this->_data['msg_disabled'] = $this->session->flashdata('msg');
		redirect($_SERVER['HTTP_REFERER']);
	}
}