<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms_notification extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('product');
		$this->load->library('form_validation'); 
		$this->load->model('admin/setting/msms_notification'); 
		
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	private $_controller_url = '';
	private $_data = array();
	
	public function index($id=null)
	{	
		$post = $this->input->post();
		$config = array(
					array(
						 'field'   => 'api_live',
						 'label'   => lang("api_live"),
						 'rules'   => 'required'
					  ),
					array(
						 'field'   => 'type',
						 'label'   => lang("type"),
						 'rules'   => 'required'
					  ),
					array(
						 'field'   => 'sender',
						 'label'   => lang("sender"),
						 'rules'   => 'required'
					  ),
					array(
						 'field'   => 'description',
						 'label'   => lang("description"),
						 'rules'   => 'required'
					  )
				 
				);
			
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == true) {
		    $data = array(
						"isms" => $post['isms'],
						"api_live" => $post['api_live'],
						"type" => $post['type'],
						"sender" => $post['sender'],
						"description" => $post['description'],
					);
				
		   $result = $this->db->update("tbl_sms_notification", $data);
		   
		   if($result){			  
			   $this->session->set_flashdata('msg', "SMS Notification has been updated.");
			   redirect($_SERVER['HTTP_REFERER']);
		   }
		}		
		$data['result'] = $this->msms_notification->get_sms_notification();
		$this->load->view('admin/settings/sms_notification/create',$data);
	}   
	
	

	
}
