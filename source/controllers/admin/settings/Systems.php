<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Systems extends CI_Controller{
	
	public function __construct()
	{
		parent::__construct();	
	}
	
	public function index()
	{
		$this->data['result'] = $this->Site->getSettingBy("recent-products");
		$this->load->view('admin/settings/systems/index', $this->data);
	}
	
	public function update()
	{
		$post = $this->input->post();
		$config = array(
				   array(
						 'field'   => 'order_by',
						 'label'   => 'ORDER BY',
						 'rules'   => 'required'
					  ),
				);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == true){
			$data = array(
						"value" => $post['order_by'],
						"updated_by" => $this->session->userdata("user_id")
					);
			$result = $this->db->where("param","recent-products")
							   ->update("sys_system_options",$data);
			if($result){
				$this->_data['success'] = "The Recent Products Has Been Updated.";
				redirect(site_url("admin/settings/systems"));
			}
		}else{
			$this->_data['errors'] = validation_errors();
			redirect(site_url("admin/settings/systems"));
		}
	}
	
	
	
	
}
