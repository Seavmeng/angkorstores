<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Config extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/user/muser_group');
		$this->load->model('admin/setting/mpermission');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}

	private $_controller_url = '';
	private $_data = array();

	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->muser_group->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('config');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-cogs';
	}

	private function _validation_create(){
		$this->form_validation->set_rules('user_group_name',word_r('user_group_name'),'required|trim|xss_clean|is_unique[sys_user_groups.user_group_name]');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('user_group_name',word_r('user_group_name'),'required|trim|xss_clean|callback_check_user_group_name');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	public function index(){
		$this->_variable();
		$query = $this->db->get('sys_config');
		$this->_data['data'] = $query->result_array();
		if($_POST){
			foreach($_POST as $key => $value){
				$this->db->where('name', $key);
				$this->db->update('sys_config',array('value'=>$value));
			}
			if($this->db->affected_rows()){
				redirect(base_admin_url().$this->_controller_url);
			}
		}
		$this->_data["result"] = $this->mpermission->getSettingBy("recent-products");
		$this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
	}

	public function update($id=null){
		$this->_variable();
		$query = $this->db->get('sys_config');
		$this->_data['data'] = $query->result_array();
		
		if($this->input->post("update")){
			foreach($_POST as $key => $value){
				$this->db->where('name', $key);
				$this->db->update('sys_config',array('value'=>$value));
			}
			if($this->db->affected_rows()){
				redirect(base_admin_url().$this->_controller_url);
			}
		}
		
		if($this->input->post("recent_product")){
			$this->recent_product();
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
	}
	
	public function recent_product(){
		$this->_variable();
		$post = $this->input->post();
		$config = array(
				   array(
						 'field'   => 'order_by',
						 'label'   => 'ORDER BY',
						 'rules'   => 'required'
					  ),
				);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == true){
			$data = array(
						"value" => $post['order_by'],
						"updated_by" => $this->session->userdata("user_id")
					);
			$result = $this->db->where("param","recent-products")
							   ->update("sys_system_options",$data);
			if($result){
				$this->_data['success'] = "The Recent Products Has Been Updated.";
				redirect(site_url("admin/settings/config"));
			}
		}else{
			$this->_data['errors'] = validation_errors();
			redirect(site_url("admin/settings/config"));
		}
	}
	
	
}
