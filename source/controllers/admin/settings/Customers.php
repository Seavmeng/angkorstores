<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('product');
		$this->load->library('form_validation'); 
		$this->load->model('admin/setting/mcustomers'); 
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3);
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	
	private $_controller_url = '';
	private $_data = array();
	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = "Customer";
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
	}
	
	public function index()
	{	
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$this->_variable();
		$user_id = get_user_id();
        $group_id = $this->session->userdata('group_id');
		$this->_data['data'] = $this->mcustomers->show_all();
		$base_url = account_url().$this->_controller_url.'?';
		$this->mcustomers->count_all = true;
		$this->_data['page'] = $this->mcustomers->page($base_url,$this->mcustomers->show_all());
		$this->_data['showing'] =$this->mcustomers->showing($this->mcustomers->show_all());
		$this->load->view('admin/settings/customers/index',$this->_data);
	}
  
	public function search_customer() { 
		 
			$status = $this->input->get('status');
			$username = $this->input->get('username');
			$email = $this->input->get('email');
			$phone = $this->input->get('phone');
		 
			$this->_data['rmsg']=$this->session->flashdata('msg');
			$this->_variable();
			$user_id = get_user_id();
			$group_id = $this->session->userdata('group_id');
			$this->_data['data'] = $this->mcustomers->search_customer($status,$username,$email,$phone);
			
			$base_url = base_admin_url().$this->_controller_urls.$this->uri->segment(4).'?status='.$status.'&username='.$username.'&email='.$email.'&phone='.$phone.'&';
			 
			$this->mcustomers->count_all = true;
			$this->_data['page'] = $this->mcustomers->page($base_url,$this->mcustomers->search_customer($status,$username,$email,$phone));
			$this->_data['showing'] =$this->mcustomers->showing($this->mcustomers->search_customer($status,$username,$email,$phone));
			
			$this->load->view('admin/settings/customers/index',$this->_data);
		  
	}
	
}
