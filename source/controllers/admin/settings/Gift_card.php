<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gift_card extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->helper('product');
		$this->load->model('admin/setting/mgift_card'); 
		 
		$this->load->model('admin/product/moproduct');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = "Gift Card & Coupon";
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-newspaper';
	}
	//*********************** List ****************************//
	public function index(){
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$this->_variable();
		$user_id = get_user_id();
        $group_id = $this->session->userdata('group_id');
		$this->_data['data'] = $this->mgift_card->show_all($user_id,$group_id);
		$base_url = account_url().$this->_controller_url.'?';
		$this->mgift_card->count_all = true;
		$this->_data['page'] = $this->mgift_card->page($base_url,$this->mgift_card->show_all($user_id,$group_id));
		$this->_data['showing'] =$this->mgift_card->showing($this->mgift_card->show_all($user_id,$group_id));
	 
		$this->load->view('admin/settings/gift_card/list',$this->_data);
	}  
	
	private function _validation_create(){
		  
		$this->form_validation->set_rules('coupon_code','Coupon Code','required|trim|xss_clean|max_length[200]');  
		$this->form_validation->set_rules('description','Description','required|trim|xss_clean|max_length[200]'); 
		$this->form_validation->set_rules('usage_limit','Usage Limit','required|trim|xss_clean|max_length[200]'); 
		$this->form_validation->set_rules('discount','Discount','required|trim|xss_clean|max_length[200]'); 
		$this->form_validation->set_rules('ex_ddate','Expiring Date','required|trim|xss_clean|max_length[200]'); 
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}
	/// ****************  Create ********************
	function create(){ 
	    $logged = has_logged();
		if($logged){    
				if($this->input->post() == true) {    
					$status = ($this->input->post('status')==true)?1:0;

				
					$ex_date =date('Y-m-d H:i:s', strtotime($this->input->post("ex_ddate")." ".$this->input->post("ex_dtime")));  
				 
					if($this->_validation_create()){
						$SQLDATA = array(
							'coupon_code'=> $this->input->post('coupon_code', true),
							'description'=> $this->input->post('description', true),
							'amount'=> $this->input->post('amount', true),
							'allow_free_shipping'=> $this->input->post('allow_free_shipping', true),
							'discount'=> $this->input->post('discount', true),
							'usage_limit'=> $this->input->post('usage_limit', true),
							'expiry_date'=> $ex_date
							
						);    
						
						if(isset($_FILES)) {
							$config_file = array(
								'upload_path' => 'images/gift_card',
								'allowed_types' => 'gif|jpg|png' 
							); 
							$this->load->library('upload', $config_file); 
							$this->upload->initialize($config_file);  
							if($this->upload->do_upload('image_gift')){  
								$SQLDATA["image"]  = $this->upload->data('file_name');  
							} 
						}   
						$result = $this->mgift_card->save($SQLDATA);
						if($result) {
							$this->session->set_flashdata('msg','Your Gift Card '.has_save());
							redirect(base_url().'admin/settings/gift_card/');
						}
					} 
				} 
				$this->load->view('admin/settings/gift_card/create',$this->_data);	
		}else{
			redirect('account');
		}
	}
	//********************* delete ******************
	public function delete($id = null){ 
		$result = $this->mgift_card->delete($id);
		if($result){
			$this->session->set_flashdata('msg','Your Gift Card '.has_del());
			redirect(base_url().'admin/settings/gift_card/');
		}
	}
	
	//******************** Update ******************
	public function update($id = null) {
		$logged = has_logged();
		if($logged){   
				if($this->input->post() == true) {    
					$ads_date =date('Y-m-d H:i:s', strtotime($this->input->post("ads_ddate")." ".$this->input->post("ads_dtime")));
					$ex_date =date('Y-m-d H:i:s', strtotime($this->input->post("ex_ddate")." ".$this->input->post("ex_dtime")));  
					
					if($this->_validation_create()){
						$SQLDATA = array(
							'coupon_code'=> $this->input->post('coupon_code', true),
							'description'=> $this->input->post('description', true),
							'amount'=> $this->input->post('amount', true),
							'allow_free_shipping'=> $this->input->post('allow_free_shipping', true),
							'discount'=> $this->input->post('discount', true),
							'usage_limit'=> $this->input->post('usage_limit', true),
							'expiry_date'=> $ex_date
							
						);    
						
						if(isset($_FILES)) {
							$config_file = array(
								'upload_path' => 'images/gift_card',
								'allowed_types' => 'gif|jpg|png' 
							); 
							$this->load->library('upload', $config_file); 
							$this->upload->initialize($config_file);  
							 
							if($this->upload->do_upload('image_gift')){  
								$SQLDATA["image"]  = $this->upload->data('file_name');  
							} 
						}
						$result = $this->mgift_card->update($id,$SQLDATA);
						if($result) { 
							$this->session->set_flashdata('msg','Your Gift Card '.has_save());
							redirect(base_url().'admin/settings/gift_card/');
						}
					} 
				} 
				$this->data['id']=$id;
				$this->data["result"] = $this->mgift_card->getGiftCardById($id);
				$this->load->view('admin/settings/gift_card/update',$this->data);	
		}else{
			redirect('account');
		}
		
		
	}
	
	
	
	
}
