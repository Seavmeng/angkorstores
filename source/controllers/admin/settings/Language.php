<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Language extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/setting/mlanguage');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mlanguage->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('language');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-language';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'active'=>word_r('active'),
						'inactive'=>word_r('inactive')
				  );
		$option_search = array(
						''=>'----',
						'active'=>word_r('active'),
						'inactive'=>word_r('inactive')
				  );
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
		$this->_data['option_search'] = form_dropdown('user_active',$option_search,$this->input->get('user_active'),'class="form-control" ');
	}

	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('word',word_r('word'),'required|trim|xss_clean|is_unique[sys_language.word]');
		$this->form_validation->set_rules('english',word_r('english'),'required|trim|xss_clean');
		$this->form_validation->set_rules('khmer',word_r('khmer'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('word',word_r('word'),'required|trim|xss_clean|callback_check_word');
		$this->form_validation->set_rules('english',word_r('english'),'required|trim|xss_clean');
		$this->form_validation->set_rules('khmer',word_r('khmer'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	//*********************** Validation **************************//


	//*********************** List ****************************//
	public function index(){
		$this->_variable();

		$this->_data['data'] = $this->mlanguage->all();
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->mlanguage->count_all = true;
		$this->_data['showing'] = $this->mlanguage->showing($this->mlanguage->all());
		$this->_data['page'] = $this->mlanguage->page($base_url,$this->mlanguage->all());
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	}
	//*********************** List ****************************//
	//************************ Create ***************************//
	public function create(){
		$this->_variable();
		if($this->input->post()){
			if($this->_validation_create()){
				$SQLDATA = array(
					  "word" => $this->input->post('word', true),
					  "english" => $this->input->post('english', true),
					  "khmer" => $this->input->post('khmer', true)
				);
				$result = $this->mlanguage->save($SQLDATA);
				if($result){
					redirect(base_admin_url().$this->_controller_url);
				}
			}			
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);		
	}

	//************************ Create ***************************//

	//************************ Update **************************//
	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mlanguage->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				
				if($this->_validation_update()){
						$SQLDATA = array(
							  "word" => $this->input->post('word', true),
							  "english" => $this->input->post('english', true),
							  "khmer" => $this->input->post('khmer', true)
						);
						$result = $this->mlanguage->update($id,$SQLDATA);
					if($result){
						redirect(base_admin_url().$this->_controller_url);
					}
				}
			endif;
		  $this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	//************************ Update **************************//


	// **************** Call Back Validation Function For Update **************//
	public function check_word($word){
		$id = $this->uri->segment(5);
		$this->db->select('word')->from('sys_language')->where(array('word' => $word,'lang_id !=' => $id));
		$query = $this->db->get();
		$count = $query->num_rows(); 
		if($count > 0){
			$this->form_validation->set_message('check_word', 'The '.word_r('word').' field must contain a unique value');
			return false;
		}
	}

	// **************** Call Back Validation Function For Update **************//

	//********************* Delete *****************//

	public function delete($id = null){
		
			$result = $this->mlanguage->delete($id);
			if($result){
				redirect(base_admin_url().$this->_controller_url);
			}
		
	
	}

	//********************* Delete *****************//

	//********************** Search ***********************//
	public function search(){
		$this->_variable();
		// $this->mlanguage->per_page = 1;
		$word = $this->input->get('word');
		$english = $this->input->get('english');
		$khmer = $this->input->get('khmer');
		$this->_data['data'] = $this->mlanguage->search($word, $english, $khmer);
		$this->mlanguage->count_all = true;
		$data = $this->mlanguage->search($word, $english, $khmer);
		$base_url = base_admin_url().$this->_controller_url.'search?word='.$word.'&english='.$english.'&khmer='.$khmer.'&';
		$this->_data['showing'] = $this->mlanguage->showing($data);
		$this->_data['page'] = $this->mlanguage->page($base_url,$data);

		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);

	}
	//********************** Search ***********************//

}
