<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Point_reward extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('product');
		$this->load->library('form_validation'); 
		$this->load->model('admin/setting/mpoint_reward'); 
		
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	private $_controller_url = '';
	private $_data = array();
	
	public function index()
	{	
	
		 
		$post = $this->input->post();		
		$config = array(
					array(
						 'field'   => 'from_amount',
						 'label'   => lang("from_amount"),
						 'rules'   => 'required|numeric|trim'
					  ),
					array(
						 'field'   => 'to_point',
						 'label'   => lang("to_point"),
						 'rules'   => 'required|numeric|trim'
					  ),
					array(
						 'field'   => 'from_point',
						 'label'   => lang("from_point"),
						 'rules'   => 'required|numeric|trim'
					  ),
					array(
						 'field'   => 'to_amount',
						 'label'   => lang("to_amount"),
						 'rules'   => 'required|numeric|trim'
					  ),
					array(
						 'field'   => 'max_discount',
						 'label'   => lang("max_discount"),
						 'rules'   => 'numeric|trim'
					  )  
				);
			
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == true) {
		    $data = array(
						"enable_point" => $post['enable_point'],
						"from_amount" => $post['from_amount'],
						"to_point" => $post['to_point'],
						"from_point" => $post['from_point'],
						"to_amount" => $post['to_amount'],
						"max_discount" => $post['max_discount'],
						"allow_free_shipping" => $post['allow_free_shipping'],
						"allow_show_point" => $post['allow_show_point'],
						"gift_card_id" => $post['gift_card'],
						"up_to_point" => $post['up_to_point'],
					);
					
					
		//// ***** calculate reward point			
			/* $user_id = get_user_id();
			if($post['up_to_point']<0){ 
				$reward_point = round($post['from_amount'] / $post['to_point'] );
				$data1=array(
						"reward_point"=>$reward_point
					);
				$result = $this->db->where("user_id",$user_id)->update("sys_users", $data1);  
			}else {  
				$reward_point = $post['to_point'];
				$data2 = array(
						"reward_point"=>$reward_point
					); 
				$result = $this->db->insert("sys_billing", $data2);  
			}   */
		 ////===============
		 
		 
		   $result = $this->db->update("sys_point_rewards", $data);
		   
		    
		   if($result){			  
			   $this->session->set_flashdata('msg', "Reward point has been updated.");
			   redirect($_SERVER['HTTP_REFERER']);
		   }
		}		
		$this->_data['result'] = $this->get_reward_point();
		$this->_data['gift_card'] = $this->get_gift_card();
		
		$this->load->view('admin/settings/point_reward/create',$this->_data);
	}   
	
	public function get_reward_point()
	{
		$result = $this->db->get("sys_point_rewards")->row();
		return $result;
	}
	
	public function customer_reports()
	{  
		   
		$this->_data['results'] = $this->mpoint_reward->show_all();
		$base_url = account_url().$this->_controller_url.'customer_reports?';
		$this->mpoint_reward->count_all = true;
		$this->_data['page'] = $this->mpoint_reward->page($base_url,$this->mpoint_reward->show_all());
		$this->_data['showing'] =$this->mpoint_reward->showing($this->mpoint_reward->show_all());

		$this->load->view('admin/settings/point_reward/customer_reports',$this->_data);
	}  
	
	public function get_gift_card() 
	{
		$result = $this->db->query("SELECT * FROM sys_gift_cards")->result();
		return $result;
	}  
	
	public function delete_customer($id = null)
	{ 
		$result = $this->mpoint_reward->delete($id);
		if($result){
			$this->session->set_flashdata('msg','Customer '.has_del());
			redirect(base_url().'admin/settings/point_reward/customer_reports/');
		}
	}
	
	public function filter_customer_reports() 
	{
		$post = $this->input->post();
		
		if($post == true)  {
			$account = $post['account'];
			$email = $post['email'];
			$phone = $post['phone'];
			 
			if(isset($post['btn_search'])) { 
 
				$user_id = get_user_id();
				$group_id = $this->session->userdata('group_id');
				$this->_data['results'] =  $this->mpoint_reward->filter_search($account,$email,$phone);
				$base_url = account_url().$this->_controller_url.'?';
				$this->mpoint_reward->count_all = true;
				$this->_data['page'] = $this->mpoint_reward->page($base_url,$this->mpoint_reward->filter_search($account,$email,$phone));
				$this->_data['showing'] =$this->mpoint_reward->showing($this->mpoint_reward->filter_search($account,$email,$phone));
			 
				  
				$this->load->view('admin/settings/point_reward/customer_reports',$this->_data);
				
			} 
			
		}
		 
	}

	
}
