<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Banner extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->helper('product');
		$this->load->model('admin/setting/mbanner'); 
		 
		$this->load->model('admin/product/moproduct');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('banner_managment');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-newspaper';
	}
	//*********************** List ****************************//
	public function index(){
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$this->_variable();
		$user_id = get_user_id();
        $group_id = $this->session->userdata('group_id');
		$this->_data['data'] = $this->mbanner->show_all($user_id,$group_id);
		$base_url = account_url().$this->_controller_url.'?';
		$this->mbanner->count_all = true;
		$this->_data['page'] = $this->mbanner->page($base_url,$this->mbanner->show_all($user_id,$group_id));
		$this->_data['showing'] =$this->mbanner->showing($this->mbanner->show_all($user_id,$group_id));
	 
		$this->load->view('admin/settings/banner/list',$this->_data);
	}  
	
	private function _validation_create(){
		$this->form_validation->set_rules('pro_code',word_r('procode'),'trim|xss_clean|max_length[100]');
		$create = $this->input->post('create_valid');
		//var_dump($create); exit;
		if (empty($_FILES['userfile']['name'][0]) && isset($create))
		{
			$this->form_validation->set_rules('userfile[]', 'Product Image', 'required');
		}
		$this->form_validation->set_rules('name',word_r('name'),'required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('link',"Link",'required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('position',word_r('position'),'required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('ads_ddate',word_r('advertising_date'),'required|trim|xss_clean|max_length[200]');
		$this->form_validation->set_rules('ex_ddate',word_r('expiring_date'),'required|trim|xss_clean|max_length[200]');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}
	/// ****************  Create ********************
	function create(){ 
	    $logged = has_logged();
		if($logged){    
				if($this->input->post() == true) {    
					$status = ($this->input->post('status')==true)?1:0;

					$ads_date =date('Y-m-d H:i:s', strtotime($this->input->post("ads_ddate")." ".$this->input->post("ads_dtime")));
					$ex_date =date('Y-m-d H:i:s', strtotime($this->input->post("ex_ddate")." ".$this->input->post("ex_dtime")));  
					
					if($this->_validation_create()){
						$SQLDATA = array(
							'name'=> $this->input->post('name', true),
							'position'=> $this->input->post('position', true),
							'layout'=> $this->input->post('layout', true),
							'show_on'=> $this->input->post('show_on', true),
							'advertising_date'=> $ads_date,
							'expiring_date'=> $ex_date,   
							'link'=> $this->input->post('link', true),
							'status'=> $status  
						);  
						
						if(isset($_FILES)) {
							$config_file = array(
								'upload_path' => 'images/banner',
								'allowed_types' => 'gif|jpg|png' 
							); 
							$this->load->library('upload', $config_file); 
							$this->upload->initialize($config_file);  
							if($this->upload->do_upload('image_banner')){  
								$SQLDATA["images"]  = $this->upload->data('file_name');  
							} 
						} 
						//var_dump($SQLDATA);exit;
						$result = $this->mbanner->save($SQLDATA);
						if($result) {
							$this->session->set_flashdata('msg','Your banner '.has_save());
							redirect(base_url().'admin/settings/banner/');
						}
					} 
				} 
				
				
				
				
				$this->load->view('admin/settings/banner/create',$this->_data);	
		}else{
			redirect('account');
		}
	}
	//********************* delete ******************
	public function delete($id = null){ 
		$result = $this->mbanner->delete($id);
		if($result){
			$this->session->set_flashdata('msg','Your banner '.has_del());
			redirect(base_url().'admin/settings/banner/');
		}
	}
	
	//******************** Update ******************
	public function update($id = null) {
		$logged = has_logged();
		if($logged){   
				 
				if($this->input->post() == true) {    
					$status = ($this->input->post('status')==true)?1:0;
 
					$ads_date =date('Y-m-d H:i:s', strtotime($this->input->post("ads_ddate")." ".$this->input->post("ads_dtime")));
					$ex_date =date('Y-m-d H:i:s', strtotime($this->input->post("ex_ddate")." ".$this->input->post("ex_dtime")));  
					
					if($this->_validation_create()){
						$SQLDATA = array(
							'name'=> $this->input->post('name', true),
							'position'=> $this->input->post('position', true),
							'layout'=> $this->input->post('layout', true),
							'show_on'=> $this->input->post('show_on', true),
							'advertising_date'=> $ads_date,
							'expiring_date'=> $ex_date,   
							'link'=> $this->input->post('link', true),
							'status'=> $status  
						);  
						
						if(isset($_FILES)) {
							$config_file = array(
								'upload_path' => 'images/banner',
								'allowed_types' => 'gif|jpg|png' 
							); 
							$this->load->library('upload', $config_file); 
							$this->upload->initialize($config_file);  
							if($this->upload->do_upload('image_banner')){  
								$SQLDATA["images"]  = $this->upload->data('file_name');  
							} 
						}
						$result = $this->mbanner->update($id,$SQLDATA);
						if($result) {
							$this->session->set_flashdata('msg','Your banner '.has_save());
							redirect(base_url().'admin/settings/banner/');
						}
					} 
				} 
				
				$this->data['id']=$id;
				$this->data["result"] = $this->mbanner->getBannerById($id);
				$this->load->view('admin/settings/banner/update',$this->data);	
		}else{
			redirect('account');
		}
		
		
	}
	
	
	
	
}
