<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Live_chat extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('product');
		$this->load->model('admin/setting/mgift_card'); 
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	
	private $_controller_url = '';
	private $_data = array();

	private function _variable()
	{
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = "Gift Card & Coupon";
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-newspaper';
	}
	
	public function index()
	{		
		$this->load->view('admin/settings/live_chat/list',$this->_data);
	}  
	
	
}
