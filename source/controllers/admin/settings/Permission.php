<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Permission extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/setting/mpermission');
		$this->load->model('admin/user/muser_group');
		$this->load->model('admin/menu/mmenu');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('config');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-cogs';
	}
	//*********************** List ****************************//
	public function index(){
		$this->_variable();
		$this->_data['data'] = $this->mpermission->show();
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	}
	//*********************** List ****************************//
	public function create()
	{
		$post = $this->input->post();	
		if($post == true)
		{

			$creates=$this->input->post('permis_create');
			$updates=$this->input->post('permis_update');
			$deletes=$this->input->post('permis_delete');
			$data = array(
				'group_id' 		=> htmlspecialchars($post['user_group']),
				'menu_id'		=> htmlspecialchars($post['menu']),
				'create'		=> $creates,
				'update'		=> $updates,
				'delete'		=> $deletes
			);
			$row=$this->mpermission->get_permissionbygroupid_menu_id($data['group_id'],$data['menu_id']);

			if ($row->group_id > 0) 
			{			
				$this->session->set_flashdata('error', 'already add');
				redirect($_SERVER['HTTP_REFERER']);
			} 
			else
			{
				$return_saved = $this->mpermission->add_permission($data);
				if($return_saved)
				{
					$this->session->set_flashdata('message', 'permission_added');
					redirect($_SERVER["HTTP_REFERER"]);
				}
				else
				{
					$this->session->set_flashdata('error', validation_errors());
					redirect($_SERVER['HTTP_REFERER']);
				}
			}

		}else
		{
			$this->_variable();
			$this->_data['data'] = $this->mpermission->show();
			$this->_data['user_group']=$this->muser_group->getusergroup();
			$this->_data['menu']=$this->mmenu->get_menus();
			$this->_data['permis_create']=array(
					'name'  => 'permis_create',
	    	 		'id'    => 'permis_create',
	    	 		'value' => '1',
	    	 		'class' => "form-control"
				);
			$this->_data['permis_update'] =array(
					'name'  => 'permis_update',
	    	 		'id'    => 'permis_update',
	    	 		'value' => '1'	
				);
			$this->_data['permis_delete'] =array(
					'name'  => 'permis_delete',
	    	 		'id'    => 'permis_delete',
	    	 		'value' => '1'	
				);

		}
		
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);
	}

	public function delete($id)
	{
		$result_delete=$this->mpermission->delete_permission($id);
		if ($result_delete) 
		{
			$this->session->set_flashdata('message', 'permission have been deleted');
			redirect($_SERVER["HTTP_REFERER"]);
		}
	}

	public function update($id=null)
	{
	    $post = $this->input->post();
		if($post == true)
		{
		
			$creates = $this->input->post('permis_create');
			$updates = $this->input->post('permis_update');
			$deletes = $this->input->post('permis_delete');
			$data    = array(
				'group_id' 	=> htmlspecialchars($post['user_group']),
				'menu_id'	=> htmlspecialchars($post['menu']),
				'create'	=> $creates,
				'update'	=> $updates,
				'delete'	=> $deletes
			);
			$old_data = $this->mpermission->getpermission_byid($id);
			$new_data = $this->mpermission->get_permissionbygroupid_menu_id($data['group_id'],$data['menu_id']);
			if ($old_data == $new_data) 
			{
				$data_ = array(
					   'create'	=> $creates,
					   'update'	=> $updates,
					   'delete'	=> $deletes
					);
				$return_saved = $this->mpermission->update_permission($data_,$id);
				if($return_saved)
				{
					$this->session->set_flashdata('message', 'permission_updated');
					redirect($_SERVER["HTTP_REFERER"]);
				}
			}else
			{
				if ($new_data->group_id > 0) 
				{		
					$this->session->set_flashdata('error', 'already add');
					redirect($_SERVER['HTTP_REFERER']);
				} 
				else
				{
					$return_saved = $this->mpermission->update_permission($data,$id);
					if($return_saved)
					{
						$this->session->set_flashdata('message', 'permission_updated');
						redirect($_SERVER["HTTP_REFERER"]);
					}
				}
			}
		}else
		{
			$this->_variable();
			$this->_data['row']=$this->mpermission->getpermission_byid($id);
			$this->_data['data'] = $this->mpermission->show();
			$this->_data['user_group']=$this->muser_group->getusergroup();
			$this->_data['menu']=$this->mmenu->get_menus();
			
			$this->_data['permis_create']=array(
					'name'    => 'permis_create',
	    	 		'id'      => 'permis_create',
	    	 		'value'   => '1',
	    	 		'checked' => ($this->_data['row']->create == 1 ),
	    	 		'class'   => "form-control"
				);
			$this->_data['permis_update'] =array(
					'name'    => 'permis_update',
	    	 		'id'      => 'permis_update',
	    	 		'checked' => ($this->_data['row']->update == 1 ),
	    	 		'value'   => '1'	
				);
			$this->_data['permis_delete'] =array(
					'name'     => 'permis_delete',
	    	 		'id'       => 'permis_delete',
	    	 		'checked'  => ($this->_data['row']->delete == 1 ),
	    	 		'value'    => '1'	
				);

		}
		$this->_data['id']=$id;
		$this->load->view(config_item('admin_dir').$this->_controller_url.'edit', $this->_data);
	}

}
