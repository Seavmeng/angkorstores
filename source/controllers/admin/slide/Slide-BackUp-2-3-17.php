<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Slide extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/slide/mslide');
		$this->load->model('admin/mtag');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mslide->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('slide');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-thumb-tack';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$this->_data['check_ml_categories'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.categories'));
		
//		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
		//$this->_data['option_search'] = form_dropdown('visibility',$option_search,$this->input->get('visibility'),'class="form-control" ');
	}

	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('title',word_r('title'),'required|trim|xss_clean');
		$this->form_validation->set_rules('publish_date',word_r('publish_date'),'required|trim|xss_clean');
		$this->form_validation->set_rules('time',word_r('time'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('title',word_r('title'),'required|trim|xss_clean');
		$this->form_validation->set_rules('publish_date',word_r('publish_date'),'required|trim|xss_clean');
		$this->form_validation->set_rules('time',word_r('time'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	//*********************** Validation **************************//


	//*********************** List ****************************//
	public function index(){
		$this->_variable();
		$this->_data['data'] = $this->mslide->join();
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->mslide->count_all = true;
		$this->_data['showing'] = $this->mslide->showing($this->mslide->join());
		$this->_data['page'] = $this->mslide->page($base_url,$this->mslide->join());
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	}
	//*********************** List ****************************//
	//************************ Create ***************************//
	public function create(){
		$this->_variable();
		
		if($this->input->post()){
			if($this->_validation_create()){
				$image = upload();
				if(!empty($image)){
					foreach($image as $image_galleres){
						$img_galley[] = date('m-Y').'/thumb/'.$image_galleres; 					
					}
					$photo = implode(',',$img_galley);
				}else{
					$photo = '';
				}
				$tags = $this->input->post('tags', true);
				//$categories = $this->input->post('id', true);
				//$post_slug = slug($this->input->post('title', true));
				$publish_date = strtotime($this->input->post('publish_date', true));
				$publish_date = date('Y-m-d',$publish_date).' '.$this->input->post('time', true);
				$SQLDATA = array(
							'title'=> $this->input->post('title', true),
							//'post_slug'=> $post_slug,
							'description'=> $this->input->post('description', true),
							//'article'=> $this->input->post('article', true),
							//'post_type'=> $this->mpost->post_type,
							'create_date'=> $publish_date,
							//'user_id'=> $this->session->userdata('user_id'),
							'photo'=> $photo,
							//'allow_comment'=>$comment,
							//'visibility'=> $this->input->post('visibility', true)
							//'slide_type'=>$slide_type,
							//'feature'=>$feature,
							//'gallery'=>$gallery
						);
				$slide_id = $this->mslide->save($SQLDATA);
				if($slide_id){
					//************** Tags *************************//
					if(!empty($tags)):

						$tag_array = explode(',',$tags);
						//***** Foreach tags *********//
						foreach($tag_array as $value){
							$tag_array = $this->mtag->check_exist_tag($value);
							//if exist old tag just add tag id to link with post id
							if(count($tag_array) >0){
								foreach ($tag_array as $tag) {
									//insert old tag id to tbl_links
									//arguments in insert_link(post_id,tag_id,SQLdata)
									$this->db->insert('tbl_links',array('tag_id'=>$tag['tag_id'],'slide_id'=>$slide_id));
								}
							}else{
								//New Tag
								$tag_slug = slug($value);
								//insert new tag to tbl_tags return new tag id
								$new_tag_id = $this->mtag->save(array('tag_name'=>$value,'tag_slug'=>$tag_slug));
								//check if empty value tag_slug update ex : khmer unicode will be empty 
								if(empty($tag_slug)){
									$this->mtag->update($new_tag_id,array('tag_slug'=>$new_tag_id));
								}

								//insert new tag id for link with post id
								//arguments in insert_link(post_id,tag_id,SQLdata)
								$this->db->insert('tbl_links',array('tag_id'=>$new_tag_id,'slide_id'=>$slide_id));
							}
						}
						//***** Foreach tags *********//
					endif;
					//************** Tags *************************//


					//************** Categories ******************//
				/*	$category_array = $categories;
					if(!empty($category_array)){
						foreach ($category_array as $category_id) {
							$this->db->insert('tbl_links',array('post_id'=>$post_id,'category_id'=>$category_id));
						}
					}else{
							// 1 for uncategorized
							$this->db->insert('tbl_links',array('post_id'=>$post_id,'category_id'=>1));
					}
				*/	//************** Categories ******************//

					//************ Check if exist post slug *************//
				/*	$query = $this->db->get_where('tbl_posts',array('post_type'=>$this->mpost->post_type,'post_slug'=>$post_slug,'post_id !='=>$post_id));
					$count = $query->num_rows();
					if($count > 0){
						$this->mpost->update($post_id,array('post_slug'=>$post_slug.'_'.$post_id));
					}
				*/	//************ Check if exist post slug *************//

					(base_admin_url().$this->_controller_url);
				}
			}			
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);		
	}

	//************************ Create ***************************//

	//************************ Update **************************//
	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mslide->find_by_id($id);
		$slide_id = $id;
		if($this->_data['data']){
			if($this->input->post()){
				if($this->_validation_update()){
					$image = upload();
					if(!empty($image)){
						foreach($image as $image_galleres){
							$img_galley[] = date('m-Y').'/thumb/'.$image_galleres; 					
						}
						$photo = implode(',', $img_galley);
					}else{
						$photo = $this->input->post('image', true);
					}
					$tags = $this->input->post('tags', true);
					//$categories = $this->input->post('id', true);
					//$post_slug = slug($this->input->post('title', true));
					$publish_date = strtotime($this->input->post('publish_date', true));
					$publish_date = date('Y-m-d',$publish_date).' '.$this->input->post('time', true);
					$SQLDATA = array(
								'title'=> $this->input->post('title', true),
								//'post_slug'=> $post_slug,
								'description'=> $this->input->post('description', true),
								//'article'=> $this->input->post('article', true),
								//'post_type'=> $this->mpost->post_type,
								
								'modified_date'=> $publish_date,
								//'user_id'=> $this->session->userdata('user_id'),
								'photo'=> $photo,
								//'allow_comment'=>$comment,
								//'visibility'=> $this->input->post('visibility', true)
								//'slide_type'=>$slide_type,
								//'feature'=>$feature,
								//'gallery'=>$gallery
							);
					$this->mslide->update($id,$SQLDATA);
					//*********** Delete all post_id in tbl_links **********//
					$this->db->where('slide_id', $slide_id);
					$this->db->delete('tbl_links');
					//*********** Delete all post_id in tbl_links **********//
					if($slide_id){
						//************** Tags *************************//
						if(!empty($tags)):

							$tag_array = explode(',',$tags);
							//***** Foreach tags *********//
							foreach($tag_array as $value){
								$tag_array = $this->mtag->check_exist_tag($value);
								//if exist old tag just add tag id to link with post id
								if(count($tag_array) >0){
									foreach ($tag_array as $tag) {
										//insert old tag id to tbl_links
										//arguments in insert_link(slide_id,tag_id,SQLdata)
										$this->db->insert('tbl_links',array('tag_id'=>$tag['tag_id'],'slide_id'=>$slide_id));
									}
								}else{
									//New Tag
									$tag_slug = slug($value);
									//insert new tag to tbl_tags return new tag id
									$new_tag_id = $this->mtag->save(array('tag_name'=>$value,'tag_slug'=>$tag_slug));
									//check if empty value tag_slug update ex : khmer unicode will be empty 
									if(empty($tag_slug)){
										$this->mtag->update($new_tag_id,array('tag_slug'=>$new_tag_id));
									}

									//insert new tag id for link with post id
									//arguments in insert_link(post_id,tag_id,SQLdata)
									$this->db->insert('tbl_links',array('tag_id'=>$new_tag_id,'slide_id'=>$slide_id));
								}
							}
							//***** Foreach tags *********//
						endif;
						//************** Tags *************************//


						//************** Categories ******************//
				/*		$category_array = $categories;
						if(!empty($category_array)){
							foreach ($category_array as $category_id) {
								$this->db->insert('tbl_links',array('post_id'=>$post_id,'category_id'=>$category_id));
							}
						}else{
								// 1 for uncategorized
								$this->db->insert('tbl_links',array('post_id'=>$post_id,'category_id'=>1));
						}
				*/		//************** Categories ******************//

						//************ Check if exist post slug *************//
				/*		$query = $this->db->get_where('tbl_posts',array('post_type'=>$this->mpost->post_type,'post_slug'=>$post_slug,'post_id !='=>$post_id));
						$count = $query->num_rows();
						if($count > 0){
							$this->mpost->update($post_id,array('post_slug'=>$post_slug.'_'.$post_id));
						}
				*/		//************ Check if exist post slug *************/
				
						redirect(base_admin_url().$this->_controller_url);
					}
				}			
			}
		  $this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	//************************ Update **************************//

	// Multiple Action
	public function ml(){
		$action = $_POST['ml'];
		$id = $_POST['id'];
		var_dump($id);
		$option = '';
		$delete = '';
		if(!empty($action) && !empty($id)){
			switch ($action) {
				case 'public':
					$option = 1;
					break;
				case 'private':
						$option = 0;
						break;
				case 'delete':
						$delete = 1;
						break;
			}
			$this->db->where_in('slide_id',$id);
			if($delete == 1){
				$this->db->delete('tbl_slide');
				//********** Delete all category and tag in tbl_links ***************//
				$this->db->where_in('slide_id',$id);
				$this->db->delete('tbl_links');
				//********** Delete all category and tag in tbl_links ***************//
				
			}else{
				$this->db->update('tbl_slide',array('visibility'=>$option));
			}
			if($this->db->affected_rows()){
		

				redirect(base_admin_url().$this->_controller_url);
			}
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
	}
	// Multiple Action

	//********************* Delete *****************//

	public function delete($id = null){

		$result = $this->mslide->delete($id);
		if($result){
			//********** Delete all category and tag in tbl_links ***************//
			$this->db->where('slide_id', $id);
			$this->db->delete('tbl_links');
		redirect(base_admin_url().$this->_controller_url);
			//********** Delete all category and tag in tbl_links ***************//
		}
		
		
	}

	//********************* Delete *****************//


	//********************** Search ***********************//
	public function search(){
		$this->_variable();
		$this->mslide->per_page = 2;
		//$username = $this->input->get('username');
		//$user_id = $this->input->get('user_id');
		//$category_id = $this->input->get('category_id');
		//$visibility = $this->input->get('visibility');
		$text = $this->input->get('text',true);

		//From Date
		$from = $this->input->get('from', true);
		if(!empty($from)){
			$from = strtotime($from);
			$from = date('Y-m-d',$from);
		}
		
		//From Date

		//To Date
		$to = $this->input->get('to', true);
		if(!empty($to)){
			$to = strtotime($to);
			$to = date('Y-m-d',$to);	
		}
		
		//To Date

		$tag_id = $this->input->get('tag_id');
		//$user_active = $this->input->get('user_active');
		$this->_data['data'] = $this->mslide->search($tag_id,$from,$to,$text);
		$this->mslide->count_all = true;
		$data = $this->mslide->search($tag_id,$from,$to,$text);
		$base_url = base_admin_url().$this->_controller_url.'search?tag_id='.$tag_id.'&from='.$from.'&to='.$to.'&text='.$text.'&';
		$this->_data['showing'] = $this->mslide->showing($data);
		$this->_data['page'] = $this->mslide->page($base_url,$data);

		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);

	}
	//********************** Search ***********************//



	//************************** Ajax Tag Auto completed *****************//
	public function save(){
		$this->output->set_content_type('application/json');
		$this->mtag->per_page = 10;
		$term = $this->input->get('term', true);
		echo $this->mtag->ajax_search($term);
	}
	//************************** Ajax Tag Auto completed *****************//

}
