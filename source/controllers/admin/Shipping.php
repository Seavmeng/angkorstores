<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping extends CI_Controller {
	
	function __construct()	{
		parent::__construct();
	//	$this->load->library('ion_auth');
		$this->load->model('admin/mshipping');
		$this->_controller_url = $this->uri->segment(2).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//
	private function _validation_create(){
		$this->form_validation->set_rules('minweight[]','Min Weight','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('maxweight[]','Max Weight','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('feeamount[]','Fee Amount','required|trim|xss_clean|max_length[20]');
		
		$this->form_validation->set_rules('method_name','Method Name','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('city','City','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('compare','Compare','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('zone','Zone','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('from_day','From day','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('to_day','To day','required|trim|xss_clean');
		
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}
	function index(){
		$this->_data['data'] = $this->mshipping->show_all();
		$this->load->view('admin/shipping/show_shipping',$this->_data);			
	}
	function create($id=null){
		$shipping_name =$this->mshipping->shipping_name($id);
		if($shipping_name==""){
			redirect(base_admin_url().'shipping');
		}
		$this->_data['ship_name_id'] = $id;
		$this->_data['shipping_list'] = $this->mshipping->show_all();
		
		$this->load->view('admin/shipping/new_method',$this->_data);	
	}

	function new_method($id = null){
		//$shipp = $this->mshipping->get_shipping_id($id);
		$shipping_id = $this->input->post('zone');
		$this->form_validation->set_rules('minweight[]','Min Weight','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('maxweight[]','Max Weight','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('feeamount[]','Fee Amount','required|trim|xss_clean|max_length[20]');
		
		$this->form_validation->set_rules('method_name','Method Name','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('city','City','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('compare','Compare','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('zone','Zone','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('from_day','From day','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('to_day','To day','required|trim|xss_clean');

		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			$this->load->view('admin/shipping/create');
		}else{
			$data = array(
				'shipping_id' => $this->input->post('shipp'),
				'method_name' => $this->input->post('method_name'),
				'zone_name' => $this->input->post('city'),
				'compare' => $this->input->post('compare'),
				'location_id' => $this->input->post('zone'),
				'from_day' => $this->input->post('from_day'),
				'to_day' => $this->input->post('to_day'),
				'created_date' => date('Y-m-d H:i')
			);
			$results = $this->mshipping->save_method($data);
			if($results){ 
				$min = $this->input->post('minweight',true);
				$max = $this->input->post('maxweight',true);
				$fee =$this->input->post('feeamount', true);
				$count = count($min); 
				if(!empty($min)){
				
					for($i=0; $i<$count; $i++){
						$data1 = array(
							'method_id' => $results,
							//'min_weight' => implode(',',$this->input->post('minweight')),
							'min_weight' => $min[$i],
							'max_weight' => $max[$i],
							'fee_amount' => $fee[$i],
						);
						$this->mshipping->save_weight_price($data1);						
										
					}				
				}
			}
			redirect(base_url().'admin/shipping');	
		}
	}
	
	// public function update_md($id = null){
	// //	$this->_variable();
	// 	$this->_data['data'] = $this->mshipping->find_by_id($id);
	// 	if($this->_data['data']){
	// 		if($this->input->post()):
	// 			if($this->_validation_create()){
	// 				$data = array(
	// 					'shipping_id'=>$shipp_id,
	// 					'method_name'=>$this->input->post('method_name'),
	// 					'zone_name'=>$this->input->post('city'),
	// 					'compare'=>$this->input->post('compare'),
	// 					'location_id'=>$this->input->post('zone'),
	// 					'from_day'=>$this->input->post('from_day'),
	// 					'to_day'=>$this->input->post('to_day'),
	// 					'created_date'=> date('Y-m-d H:i')
	// 				);
	// 				$this->mshipping->update_method($data);
	// 			}
	// 		endif;
	// 		$this->_data['ship_name_id'] = $id;
	// 	}
	// 	$this->load->view('admin/shipping/update_method',$this->_data);
	// }
	public function update_md($id = null){
		$data['md_update'] = $this->mshipping->find_by_id($id);
		//$data['p_update'] = $this->mshipping->find_id($pid);
		//var_dump($data['md_update']);exit();
		$mid = $this->input->post('method_id');
		$pid = $this->input->post('shipping_price_id');

		$this->form_validation->set_rules('method_name','Method Name','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('city','City','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('compare','Compare','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('zone','Zone','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('from_day','From day','required|trim|xss_clean|max_length[20]');
		$this->form_validation->set_rules('to_day','To day','required|trim|xss_clean');

		if($this->form_validation->run() == false){
			$this->_data['errors'] = validation_errors();
			$this->load->view('admin/shipping/update_method',$data);
		}else{
			//edit Method
			$data = array(
				'method_name' => $this->input->post('method_name'),
				'zone_name' => $this->input->post('city'),
				'compare' => $this->input->post('compare'),
				'location_id' => $this->input->post('zone'),
				'from_day' => $this->input->post('from_day'),
				'to_day' => $this->input->post('to_day'),
				'created_date' => date('Y-m-d H:i')
				);
				$this->mshipping->update_method($mid,$data);
			//edit Price
				$min = $this->input->post('min',true);
				$max = $this->input->post('max',true);
				$fee =$this->input->post('fee', true);
				$count = count($min);
				
				if(!empty($min)){
					for($i=0; $i<$count; $i++){
						$data1 = array(
							//'min_weight' => implode(',',$this->input->post('minweight')),
							'min_weight' => $min[$i],
							'max_weight' => $max[$i],
							'fee_amount' => $fee[$i]
						);
					$this->mshipping->update_price($pid[$i],$data1);													
					}				
				}
			//add price
				$min = $this->input->post('minweight',true);
				$max = $this->input->post('maxweight',true);
				$fee =$this->input->post('feeamount', true);

				$count = count($min);

				if(!empty($min)){
				
					for($i=0; $i<$count; $i++){
						$data2 = array(
							'method_id' => $mid,
							'min_weight' => $min[$i],
							'max_weight' => $max[$i],
							'fee_amount' => $fee[$i],
						);
						if(!empty($min[$i])){
							$this->mshipping->save_weight_price($data2);
						}									
					}				
				}
			redirect(base_url().'admin/shipping/all_method');	
		}
	}
	/* khorng */
	function delete_price($id=null){
		//$this->load->library('user_agent');
		$this->load->library('user_agent');
		$this->mshipping->delete_shipping_price($id);
		redirect($this->agent->referrer());
	}

	function all_method($shipping_id =false){		
		//$this->_data['data'] = $this->mshipping->show_all_methods();
		$this->_data['data'] = $this->mshipping->show_all_methods_by_id($shipping_id);
		$this->load->view('admin/shipping/show_method',$this->_data);	
	} 
	public function delete_md($id = null,$shipping_id = null){
		$result_del = $this->mshipping->delete_md($id);  
		if($result_del) {
			$this->all_method($shipping_id);
		} 	
	}
	function fitler_method()
	
	{
		$city=$this->input->post('city');
		if($city =='country'){
			// $this->db->select('*');
			// $this->db->from('sys_location');
			// $this->db->where('parent_id !=',0);
			// $query=$this->db->get();
			$query = $this->db->query("SELECT * FROM sys_location GROUP BY location_name");
			$results=$query->result_array();
			$output="";
			$output.='<select class="form-control" id="zone" name="zone">';
			foreach($results as $data){
				$output .='<option value="'.$data['loc_id'].'">'.$data['location_name'].'</option>';
			}
			$output.="</select>";
			echo $output;
		}elseif($city =='zone'){
			// $this->db->select('*');
			// $this->db->from('sys_shopping_zone');
			// $this->db->where()
			// $query=$this->db->get();
			$query = $this->db->query("SELECT * FROM sys_shopping_zone WHERE zone_name IS NOT NULL");
			$results=$query->result_array();
			$output="";
			$output.='<select class="form-control" id="zone" name="zone">';
			foreach($results as $data){
				$output .='<option value="'.$data['zone_id'].'">'.$data['zone_name'].'</option>';
			}
			$output.="</select>";
			echo $output;
		}else{
			$output='
			<select class="form-control" id="zone" name="zone">
				<option value="">Country</option>
				<option value="">Shipping Zone</option>
			</select>';
			echo $output;
		}
	}
	/*
		@khorng
	*/
	
	public function zone()
	{
		//$this->load->view('admin/shipping/add_shipping');
		//var_dump($this->input->post('country'));
		$this->_data['rmsg']=$this->session->flashdata('msg');
		$data['get_zone']= $this->mshipping->get_zname();   
		$this->load->view('admin/shipping/show_zone',$data); 
	}
	
	public function add_subzone($id = 0){
		$data['subzone'] = $this->mshipping->get_tblzone($id);
		$data['country']= $this->mshipping->get_country();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('description','description','required');
        //var_dump($data['subzone']);exit();
        if($this->form_validation->run() == FALSE){
        	log_message('info', 'Form not validated');
        	$this->load->view('admin/shipping/add_sub_shipping',$data);
        }
        else{
        	$data = array(
        					'shipping_id' => $this->input->post('shipp'),
        					'parent_id'=> $this->input->post('zone_id'),
			        		'description'=> $this->input->post('description'),
			        		'country_id' => $this->input->post('country')
        				 );
        	$this->mshipping->insert_zone($data); 
        	redirect(base_url().'admin/shipping/zone'); 
        } 
        

	}

	public function add_zone(){
		$data['shipp']= $this->mshipping->get_shipping();
		$data['country']= $this->mshipping->get_country();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('description','description','required');
        $this->form_validation->set_rules('zone_name', 'Zonename', 'required');
        
        if($this->form_validation->run() === false){
        	log_message('info', 'Form not validated');
        	$this->load->view('admin/shipping/add_shipping',$data);
        }
        else{
        	$data = array(
        					'shipping_id' => $this->input->post('shipp'),
        					'zone_name'=> $this->input->post('zone_name'),
			        		'description'=> $this->input->post('description')
			        		//'country_id' => $this->input->post('country')
        				 );
        	$this->mshipping->insert_zone($data);
        	redirect(base_url().'admin/shipping/zone');  
        	$this->session->set_flashdata('msg');
        } 
	} 
	
	///add new shipping company name.
	function add_new_shipping(){
		$post = $this->input->post();
		$this->form_validation->set_rules('shipping_name','shipping Name','required|trim|xss_clean|max_length[20]'); 
		$this->form_validation->set_rules('description','Description','required|trim|xss_clean'); 

		if($this->form_validation->run() == false){
			$this->_data['errors'] = validation_errors();
			$this->load->view('admin/shipping/add_new_shipping',$data);
		}
		else{  
			$image_featured = upload();
						if(!empty($image_featured)){
							$image_featured = date('m-Y').'/'.implode(',', $image_featured);
						}else{
							$image_featured = $this->input->post('feature');
						} 
			$data = array(
					'shipping_name' => $this->input->post('shipping_name'),
					'description' => $this->input->post('description'),
					'image' => $image_featured,
					'created_date' => date('Y-m-d H:i')
				);
				$results = $this->mshipping->save_shipping($data);
				if($results) { 
					$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable text-center">
												    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
												    The user has been updated successfully!
												  </div>');
					redirect('admin/shipping');
				}
			$this->load->view('admin/shipping/add_new_shipping',$this->_data);	
		}
	}
	/// delete shipping name
	
	public function delete_shipping($id = null) 
	{
		if(isset($id)){
			$result = $this->mshipping->delete_shipping($id);  
			if($result) 
			{ 
				redirect('admin/shipping');
			}
		} 
	} 

	public function edit_new_shipping($id=false) 
	{  
		$post = $this->input->post();
		$this->form_validation->set_rules('shipping_name','shipping Name','required|trim|xss_clean|max_length[20]'); 
		$this->form_validation->set_rules('description','Description','required|trim|xss_clean'); 
		
		if($post == true) { 
			if($this->form_validation->run() == false){
				$this->_data['errors'] = validation_errors();
				$data['result']  = $this->mshipping->getShippingById($id); 
				$data['id']  = $id; 
				$this->load->view('admin/shipping/edit_new_shipping',$data);
			}
			else{  
				$data = array(
					'shipping_name' => $this->input->post('shipping_name'),
					'description' => $this->input->post('description'), 
					'modidied_date' => date('Y-m-d H:i')
				);  
				//***** config upload image
				$image_featured = upload(); 			
				if(isset($image_featured) && !empty($image_featured[0])){
					$image_featured = date('m-Y').'/'.implode(',', $image_featured);
					$data['image']=$image_featured;  
				} 
				//***********
				$results = $this->mshipping->save_shipping($data,$id);
				if($results) {
					redirect('admin/shipping');
				}
			}
		}
		else{
			$data['result']  = $this->mshipping->getShippingById($id); 
			$data['id']  = $id;  
			$this->load->view('admin/shipping/edit_new_shipping',$data);
		}  
	} 


	
}
