<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('account/category/mcategory');
		$this->_controller_url = $this->uri->segment(2).'/';
		//$this->_rcontroller_url = $this->uri->segment(4).'/';
		$this->_data['controller_url'] = $this->_controller_url;
		$this->load->library('ion_auth');
		$this->load->model('account/sitemodel');
		$this->load->model('account/usermodel');
		$this->load->model('account/pagemodel');
		$this->load->model('account/datamodel');
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mcategory->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('category');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
			''=>'----',
			'delete'=>word_r('delete')
		);
		
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}

	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('category_name',word_r('category_name'),'required|trim|xss_clean');
		$this->form_validation->set_rules('category_kh',word_r('category_kh'),'required|trim|xss_clean');
		$this->form_validation->set_rules('category_ch',word_r('category_ch'),'required|trim|xss_clean');
		$this->form_validation->set_rules('category_vn',word_r('category_vn'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
		//	$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('category_name',word_r('category_name'),'required|trim|xss_clean');
		$this->form_validation->set_rules('category_kh',word_r('category_kh'),'required|trim|xss_clean');
		$this->form_validation->set_rules('category_ch',word_r('category_ch'),'required|trim|xss_clean');
		$this->form_validation->set_rules('category_vn',word_r('category_vn'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
		//	$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	//*********************** Validation **************************//
	private function _child_category($parent_id){
		$this->db->select();
		$this->db->from('sys_category');
		$this->db->where(array('parent_id'=>$parent_id));
		$this->db->order_by('category_id','DESC');
		$result = $this->db->get();
		$category_array = $result->result_array();
		$output ="";
		foreach ($category_array as $value) {
			$category_id = $value['category_id'];
			$category_name = $value['category_name'];
			$category_kh = $value['category_kh'];
			$category_ch = $value['category_ch'];
			$category_vn = $value['category_vn'];
			$category_slug = $value['category_slug'];
			$description = $value['description'];
			$output .= '<tr>';
			
			$output .= '<td>'.form_checkbox(array('name'=>'id[]','value'=>$category_id,'class'=>'checkboxes')).'</td>';

			$output .='<td';
			$output .=' id="'.$category_id.'" class="opt-child" >';
			$output .= str_repeat(" * ", $level);
			$output .= $category_name.'</td>';
			
			$output .='<td';
			$output .=' id="'.$category_id.'" class="opt-child" >';
			$output .= str_repeat(" * ", $level);
			$output .= $category_kh.'</td>';
			
			$output .='<td';
			$output .=' id="'.$category_id.'" class="opt-child" >';
			$output .= str_repeat(" * ", $level);
			$output .= $category_ch.'</td>';
			
			$output .='<td';
			$output .=' id="'.$category_id.'" class="opt-child" >';
			$output .= str_repeat(" * ", $level);
			$output .= $category_vn.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $description.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $category_slug.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= btn_action('update/'.$category_id, $this->_controller_url.'delete/'.$category_id).'</td>';

			$output .=$this->_child_category($category_id,$user_id,$level+1);
			$output .= '</tr>';
		}
		return $output;
	}

	public function index(){
		$this->_variable();
		$id=get_permission();
		foreach ($id as $row){
			$ids[] = $row['id'];
		}
		$user_id = implode(', ', $ids);
		$this->_data['data'] = $this->mcategory->all($user_id);
		$base_url = account_url().$this->_controller_url.'?';
		$this->mcategory->count_all = true;
		$this->_data['page'] = $this->mcategory->page($base_url,$this->mcategory->all($user_id));
		$this->_data['showing'] =$this->mcategory->showing($this->mcategory->all($user_id));
		$this->load->view('account/category/show_categories', $this->_data);
	}

	//************************ Create ***************************//
	public function create(){
		$this->_variable();
		$user_id= user_id_logged();
		if($this->input->post()){
			if($this->_validation_create()){
				//$slug = slug($this->input->post('category_name', true) );
				$parent= $this->input->post('parent');
				$category_name= $this->input->post('category_name');
				$category_kh= $this->input->post('category_kh');
				$category_ch= $this->input->post('category_ch');
				$category_vn= $this->input->post('category_vn');
				$icon= $this->input->post('icon');
				$tag= $this->input->post('tag');
				$rang= $this->input->post('rang');
				$parent = $this->db->escape_str($parent);
				$category_name = $this->db->escape_str($category_name);
				$icon = $this->db->escape_str($icon);
				$tag = $this->db->escape_str($tag);
				$rang = $this->db->escape_str($rang);
				if( $this->ion_auth->is_admin() ){
					$check_category=$this->mcategory->admin_check_dup_2column($parent,$category_name);
				}else{
					$check_category=$this->mcategory->check_dup_2column($parent,$category_name,$user_id);
				}
				if($check_category){
					$this->_data['errors'] = data_already();
					
				}else{
					$SQLDATA = array(
						'category'=> $this->input->post('category_name', true),
						'category_kh'=> $this->input->post('category_kh', true),
						'category_ch'=> $this->input->post('category_ch', true),
						'category_vn'=> $this->input->post('category_vn', true),
						'image'=> $icon,
						'description'=> $this->input->post('description', true),
						'tag'=> $this->input->post('tag', true),
						'parent_id'=> real_int($this->input->post('parent') ),
						'created'=>date('Y-m-d H:i'),
						'permalink'=>permalink($this->input->post('category_name', true)),
						'rang'=>$rang,
						'user_id'=> user_id_logged()
					);
					$result = $this->mcategory->save($SQLDATA);

					if($result){
						redirect('account/category/category');
						$this->_data['success'] = has_save();
					}
				}
			}			
		}
		$this->load->view(config_item('account_dir').$this->_controller_url.'create', $this->_data);		
	}

	//************************ Create ***************************//

	//************************ Update **************************//
	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mcategory->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				if($this->_validation_update()){
					$icon= $this->input->post('icon');
					$tag= $this->input->post('tag');
					$rang= $this->input->post('rang');
					$icon = $this->db->escape_str($icon);
					$tag = $this->db->escape_str($tag);
					$rang = $this->db->escape_str($rang);
					$SQLDATA = array(
						'category'=> $this->input->post('category_name', true),
						'category_kh'=> $this->input->post('category_kh', true),
						'category_ch'=> $this->input->post('category_ch', true),
						'category_vn'=> $this->input->post('category_vn', true),
						'image'=> $icon,
						'description'=> $this->input->post('description', true),
						'tag'=> $this->input->post('tag', true),
						'parent_id'=> real_int($this->input->post('parent') ),
						'modified'=>date('Y-m-d H:i'),
						'permalink'=>permalink($this->input->post('category_name', true)),
						'rang'=>$rang,
						'user_id'=> user_id_logged()
					);
					$result = $this->mcategory->update($id,$SQLDATA);

					if($result){
						$this->_data['success'] = has_upd();
					}
					redirect('account/category/category');
				}
			endif;
		  $this->load->view('account/category/update', $this->_data);
		}else{
			redirect(account_url().$this->_controller_url);
		}
		
	}

	//************************ Update **************************//

	// Multiple Action
	public function ml(){
		$action = $_POST['ml'];
		$id = $_POST['id'];
		$option = '';
		if(!empty($action) && !empty($id)){
			switch ($action) {
				case 'delete':
					$option = 'delete';
					break;
				case 'inactive':
						$option = 0;
						break;
			}

			
			if($option == 'delete'){
					//********* Check if delete category_id update parent_id to 0 *************//
					$this->db->where_in('parent_id',$id);
					$this->db->update('sys_category',array('parent_id'=>0));
					//********* Check if delete category_id update parent_id to 0 *************//

					//*********** update where category_id in tbl_links update to uncategorized *******//
					$this->db->where_in('category_id',$id);
					$this->db->update('tbl_links',array('category_id'=>1));
					//*********** update where category_id in tbl_links update to uncategorized *******//
			}
			

			//********* Check if delete category_id where id in //
			$this->db->where_in('category_id',$id);
			if($option == 'delete'){
				$this->db->delete('sys_category');
			}
			//********* Check if delete category_id where id in //

			if($this->db->affected_rows()){
				redirect(base_admin_url().$this->_controller_url);
			}
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
	}
	// Multiple Action

	public function delete($id = null){
		//Unable delete main category group
				$this->_variable();
	/*	if($id == 1){
			redirect(base_admin_url().$this->_controller_url);
		}*/
		$result = $this->mcategory->delete($id);
		if($result){
			$this->_data['success'] = has_del();
		}
		$this->_data['data'] = $this->mcategory->all($id);
		$base_url = account_url().$this->_controller_url.'?';
		$this->mcategory->count_all = true;
		$this->_data['page'] = $this->mcategory->page($base_url,$this->mcategory->all($id));
		$this->_data['showing'] =$this->mcategory->showing($this->mcategory->all($id));
		$this->load->view('account/category/show_categories', $this->_data);
	}

	//********************* Delete *****************//

	//********************** Search ***********************//
	public function results(){
		$this->_variable();
		$category_name = $this->input->get('search_query');
		$this->_data['data'] = $this->mcategory->results($category_name);
		$this->mcategory->count_all = true;
		$data = $this->mcategory->results($category_name);
		$base_url = account_url().$this->_controller_url.'results?search_query='.$category_name.'&';
		$this->_data['showing'] = $this->mcategory->showing($data);
		$this->_data['page'] = $this->mcategory->page($base_url,$data);
		$this->load->view(config_item('account_dir').$this->_controller_url.'show_categories', $this->_data);

	}
	//********************** Search ***********************//

	public function choose_category(){
		$this->_data['data'] = $this->mcategory->getcate();
		$this->load->view(config_item('account_dir').$this->_controller_url.'choose_categories',$this->_data);
	}
	
	public function use_category(){
		$category_array=$this->input->post('cate',true);
		//************** Categories ******************//
		if(!empty($category_array)){
			foreach ($category_array as $category_id) {
				$get_gategory []=$category_id;
			}
			$get_gategories=implode(',', $get_gategory);
			$data = $this->mcategory->getcategory($get_gategories);
			foreach($data as $_data){
				$SQLDATA = array(
					'category'=> $_data['category'],
					'category_kh'=> $_data['category_kh'],
					'category_ch'=> $_data['category_ch'],
					'category_vn'=> $_data['category_vn'],
					'image'=> $_data['image'],
					'description'=> $_data['description'],
					'tag'=> $_data['tag'],
					'parent_id'=> $_data['category_id'],
					'created'=>date('Y-m-d H:i'),
					'permalink'=>permalink($_data['category'], true),
					'rang'=>$_data['rang'],
					'user_id'=> user_id_logged()
				);
				$result = $this->mcategory->save($SQLDATA);
			}
			redirect('account/category/category');
		}else{
			$get_gategories = "dd";
		}
		
	}
}
