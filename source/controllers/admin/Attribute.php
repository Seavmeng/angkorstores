<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Attribute extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->helper('date');
		$this->load->model('account/attributes/mattribute');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mattribute->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('category');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );
		
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}

	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('attr_name',word_r('attr_name'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
		//	$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('attr_name',word_r('attr_name'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
		//	$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	//*********************** Validation **************************//


	private function _child_category($parent_id){
		$this->db->select();
		$this->db->from('sys_category');
		$this->db->where(array('parent_id'=>$parent_id));
		$this->db->order_by('category_id','DESC');
		$result = $this->db->get();
		$category_array = $result->result_array();
		$output ="";
		foreach ($category_array as $value) {
			$category_id = $value['category_id'];
			$attr_name = $value['attr_name'];
			$category_slug = $value['category_slug'];
			$description = $value['description'];
			$output .= '<tr>';

			$output .= '<td>'.form_checkbox(array('name'=>'id[]','value'=>$category_id,'class'=>'checkboxes')).'</td>';

			$output .='<td';
			$output .=' id="'.$category_id.'" class="opt-child" >';
			$output .= str_repeat(" * ", $level);
			$output .= $attr_name.'</td>';


			//$output .='<td ';
			//$output .=' id="'.$category_id.'" class="opt-parent" >';
			//$output .= $description.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= $category_slug.'</td>';

			$output .='<td ';
			$output .=' id="'.$category_id.'" class="opt-parent" >';
			$output .= btn_action($this->_controller_url.'update/'.$category_id, $this->_controller_url.'delete/'.$category_id).'</td>';

			$output .=$this->_child_category($category_id,$user_id,$level+1);
			$output .= '</tr>';
		}
		return $output;
	}

	public function index(){
		$this->_variable();
		
		$this->_data['data'] = $this->mattribute->all();
		$base_url = account_url().$this->_controller_url.'?';
		$this->mattribute->count_all = true;
		$this->_data['page'] = $this->mattribute->page($base_url,$this->mattribute->all());
		$this->_data['showing'] =$this->mattribute->showing($this->mattribute->count_all());
		$this->load->view(config_item('account_dir').$this->_controller_url.'show_attributes', $this->_data);
	}

	//************************ Create ***************************//
	public function create(){
		$this->_variable();
		$user_id= has_logged();
		if($this->input->post()){
			if($this->_validation_create()){
				//$slug = slug($this->input->post('attr_name', true) );
				$parent= $this->input->post('parent');
				$attr_name= $this->input->post('attr_name');
				$parent = $this->db->escape_str($parent);
				$attr_name = $this->db->escape_str($attr_name);
				
				$check_attrname=$this->mattribute->check_dup_2column($parent,$attr_name,$user_id);
				if($check_attrname){
					$this->_data['errors'] = data_already();
					
				}else{
					$SQLDATA = array(
								'attr_name'=> $this->input->post('attr_name', true),
								'description'=> $this->input->post('description', true),
								'parent_id'=> real_int($this->input->post('parent') ),
								'created'=>date('Y-m-d H:i'),
								'permalink'=>permalink($this->input->post('attr_name', true)),
								'user_id'=> has_logged()
					);
					$result = $this->mattribute->save($SQLDATA);

					if($result){
					//	redirect(account_url().$this->_controller_url);
						$this->_data['success'] = has_save();
					}
				}
			}			
		}
		$this->load->view(config_item('account_dir').$this->_controller_url.'create', $this->_data);		
	}

	//************************ Create ***************************//

	//************************ Update **************************//
	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mattribute->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				if($this->_validation_update()){
					$SQLDATA = array(
						'attr_name'=> $this->input->post('attr_name', true),
						'description'=> $this->input->post('description', true),
						'parent_id'=> real_int($this->input->post('parent') ),
						'modified'=>date('Y-m-d H:i'),
						'user_id'=> has_logged()
					);
					$result = $this->mattribute->update($id,$SQLDATA);

					if($result){
						$this->_data['success'] = has_upd();
					}
				}
			endif;
			$this->load->view(config_item('account_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(account_url().$this->_controller_url);
		}
		
	}

	//************************ Update **************************//



	// Multiple Action
	public function ml(){
		$action = $_POST['ml'];
		$id = $_POST['id'];
		$option = '';
		if(!empty($action) && !empty($id)){
			switch ($action) {
				case 'delete':
					$option = 'delete';
					break;
				case 'inactive':
						$option = 0;
						break;
			}

			
			if($option == 'delete'){
					//********* Check if delete category_id update parent_id to 0 *************//
					$this->db->where_in('parent_id',$id);
					$this->db->update('sys_category',array('parent_id'=>0));
					//********* Check if delete category_id update parent_id to 0 *************//

					//*********** update where category_id in tbl_links update to uncategorized *******//
					$this->db->where_in('category_id',$id);
					$this->db->update('tbl_links',array('category_id'=>1));
					//*********** update where category_id in tbl_links update to uncategorized *******//
			}
			

			//********* Check if delete category_id where id in //
			$this->db->where_in('category_id',$id);
			if($option == 'delete'){
				$this->db->delete('sys_category');
			}
			//********* Check if delete category_id where id in //

			if($this->db->affected_rows()){
				redirect(base_admin_url().$this->_controller_url);
			}
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
	}
	// Multiple Action

	public function delete($id = null){
		//Unable delete main category group
		$this->_variable();
		if($id == 1){
			redirect(base_admin_url().$this->_controller_url);
		}
		$result = $this->mattribute->delete($id);
		if($result){
			$this->_data['success'] = has_del();
		}
		$this->_data['data'] = $this->mattribute->all();
		$base_url = account_url().$this->_controller_url.'?';
		$this->mattribute->count_all = true;
		$this->_data['page'] = $this->mattribute->page($base_url,$this->mattribute->all());
		$this->_data['showing'] =$this->mattribute->showing($this->mattribute->all());
		$this->load->view(config_item('account_dir').$this->_controller_url.'show_attributes', $this->_data);
	}

	//********************* Delete *****************//


	//********************** Search ***********************//
	public function results(){
		$this->_variable();
		$attr_name = $this->input->get('search_query');
		$this->_data['data'] = $this->mattribute->results($attr_name);
		$this->mattribute->count_all = true;
		$data = $this->mattribute->results($attr_name);
		$base_url = account_url().$this->_controller_url.'results?search_query='.$attr_name.'&';
		$this->_data['showing'] = $this->mattribute->showing($data);
		$this->_data['page'] = $this->mattribute->page($base_url,$data);
		$this->load->view(config_item('account_dir').$this->_controller_url.'show_categories', $this->_data);

	}
	//********************** Search ***********************//
	
	//********************** more attribute ***********************//
	public function more_attr(){
		$this->_variable();
		$attr_id_ =$this->input->post('attr_id');
		$attr_id = $this->db->escape_str($attr_id_);
		if($attr_id ==""){
			return false;
		}else{
			$this->db->select('attr_id,attr_name');
			$this->db->from('sys_attribute');
			$this->db->where('attr_id',$attr_id);
			$result = $this->db->get();
			$main_attr = $result->first_row('array');
			
			$this->db->select();
			$this->db->from('sys_attribute');
			$this->db->where(array('parent_id'=>$attr_id));
			$result = $this->db->get();
			$attr_array = $result->result_array();
			
			$output ="";
			$output .= "<div class='m_attr' id='".$attr_id."'>";
			$output .= "<span class='title_attr'>".$main_attr['attr_name']."</span>
					<input type='hidden' name='attr_m_id[]' value='".$main_attr['attr_id']."'>";
				$output .= "<div class='m_attr_name'>";
			foreach($attr_array as $value){
				$attr_id = $value['attr_id'];
				$attr_name = $value['attr_name'];
				$description = $value['description'];

				$output .='<div class="attr_list">';
					$output .='<input type="checkbox" name="attr_m_id[]" value="'.$attr_name.'">'.$attr_name;
				$output .='</div>';
			}
				$output .="</div>";
			$output .="<div class='remove glyphicon glyphicon-remove-circle'></div>";
			$output .="<div class='clear'></div>";
		/*	$output .="<div class='contain_addnew_attr'>
							<div class='add_box'>
								<input type='textbox' name='add_box_attr' placeholder='Add New Attribute'>
							</div>
							<div class='add_new_attr'>
								<input class='btn btn-default' type='button' value='Add New'>
							</div>
							<div class='clear'></div>
						</div>";*/
			$output .="</div>";
			
			
		//	return $output;
			$this->_data['data']=$output;
			$base_url = account_url().$this->_controller_url.'?';
			$this->mattribute->count_all = true;
			$this->load->view(config_item('account_dir').$this->_controller_url.'more_attr', $this->_data);
		}
	}
	//********************** close mroe***********************//
}
