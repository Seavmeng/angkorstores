<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu_group extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/menu/mmenu_group');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mmenu_group->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('menu_group');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-code-fork';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );
	
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}

	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('name',word_r('name'),'required|trim|xss_clean|is_unique[tbl_menu_groups.name]');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('name',word_r('name'),'required|trim|xss_clean|callback_check_name');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	//*********************** Validation **************************//


	//*********************** List ****************************//
	public function index(){
		$this->_variable();

		$this->_data['data'] = $this->mmenu_group->all();
		$base_url = base_admin_url().$this->_controller_url.'?';
		$this->mmenu_group->count_all = true;
		$this->_data['showing'] = $this->mmenu_group->showing($this->mmenu_group->all());
		$this->_data['page'] = $this->mmenu_group->page($base_url,$this->mmenu_group->all());
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	}
	//*********************** List ****************************//
	//************************ Create ***************************//
	public function create(){
		$this->_variable();
		if($this->input->post()){
			if($this->_validation_create()){
				$SQLDATA = array(
							'name'=> $this->input->post('name', true),
							'description'=> $this->input->post('description', true)
				);
				$result = $this->mmenu_group->save($SQLDATA);

				if($result){
					redirect(base_admin_url().$this->_controller_url);
				}
			}			
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'create', $this->_data);		
	}

	//************************ Create ***************************//

	//************************ Update **************************//
	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mmenu_group->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				if($this->_validation_update()){
						
						$SQLDATA = array(
									'name'=> $this->input->post('name', true),
									'description'=> $this->input->post('description', true)
						);
						$result = $this->mmenu_group->update($id,$SQLDATA);

						if($result){
							redirect(base_admin_url().$this->_controller_url);
						}
				}
			endif;
		  $this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	//************************ Update **************************//

	// **************** Call Back Validation Function For Update **************//
	public function check_name($name){
		$id = $this->uri->segment(5);
		$this->db->select('name')->from('tbl_menu_groups')->where(array('name' => $name,'menu_group_id !=' => $id));
		$query = $this->db->get();
		$count = $query->num_rows(); 
		if($count > 0){
			$this->form_validation->set_message('check_name', 'The '.word_r('name').' field must contain a unique value');
			return false;
		}
	}

	// **************** Call Back Validation Function For Update **************//

	//********************* Delete *****************//

	public function delete($id = null){
		if($id == 1){
			redirect(base_admin_url().$this->_controller_url);
		}
		
		$result = $this->mmenu_group->delete($id);
		if($result){
			$this->db->where('menu_group_id',$id);
			$this->db->update('tbl_menus',array('menu_group_id'=>1));
			redirect(base_admin_url().$this->_controller_url);
		}
		
		
	}

	//********************* Delete *****************//


	//********************** Search ***********************//
	public function search(){
		$this->_variable();
		$name = $this->input->get('name');
		$this->_data['data'] = $this->mmenu_group->search($name);
		$this->mmenu_group->count_all = true;
		$data = $this->mmenu_group->search($name);
		$base_url = base_admin_url().$this->_controller_url.'search?name='.$name.'&';
		$this->_data['showing'] = $this->mmenu_group->showing($data);
		$this->_data['page'] = $this->mmenu_group->page($base_url,$data);

		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);

	}
	//********************** Search ***********************//

}
