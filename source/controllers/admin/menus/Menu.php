<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu extends CI_Controller{

	public function __construct(){
		parent::__construct();
		acl($this->uri->uri_string);
		$this->load->model('admin/menu/mmenu');
		$this->load->model('admin/publish/mpage');
		$this->load->model('admin/category/mcategory');
		$this->_controller_url = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
	}
	//*****************  Variable  ********************//
	private $_controller_url = '';
	private $_data = array();
	//*****************  Variable  ********************//

	
	private function _variable(){

		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->mmenu->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('menu');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-code-fork';
		$this->_data['check_ml_pages'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.pages'));
		$this->_data['check_ml_categories'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.categories'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );
	
		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}

	//*********************** Validation **************************//
	private function _validation_create(){
		$this->form_validation->set_rules('label',word_r('label'),'required|trim|xss_clean');
		$this->form_validation->set_rules('url',word_r('url'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	private function _validation_update(){
		$this->form_validation->set_rules('label',word_r('label'),'required|trim|xss_clean');
		$this->form_validation->set_rules('url',word_r('url'),'required|trim|xss_clean');
		if($this->form_validation->run() == FALSE){
			$this->_data['errors'] = validation_errors();
			return false;
		}else{
			return true;
		}
	}

	//*********************** Validation **************************//


	//*********************** List ****************************//
	public function index(){
		$this->_variable();
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	}
	//*********************** List ****************************//
	//************************ Create ***************************//
	public function create(){
		$this->_variable();
		$type = $this->input->post('type');

		//Page
		if($type == 'page'){
			$this->_add_pages_to_menu();
		}
		//Page


		//Link
		if($type == 'link'){
			 $this->_add_link_to_menu();
		}
		//Link

		//categories

		if($type == 'categories'){
			 $this->_add_categories_to_menu();
		}
		//categories
		if(empty($type)){
			$this->index();
		}
		
	}

	//Add Page to menu
	private function _add_pages_to_menu(){
		if($this->input->post()){
			$id = $this->input->post('id',true);
			$i = 0;
			if(!empty($id)){
				foreach($id as $page_id){
					$i++;
					$page_id = real_int($page_id);
					$page = $this->mpage->find_by_id($page_id);
					$SQLdata = array(
						'parent_id'=>0,
						'menu_name'=> $page['post_title'],
						'page_id'=> $page['post_id'],
						'menu_type'=> 'page',
						'menu_group_id'=> sys_config('menu_group_id')
					);
					$this->mmenu->save($SQLdata);
				}
				if($i){
					redirect(base_admin_url().$this->_controller_url);
				}
				
			}
		}
	}
	//Add Page to menu

	//Add Link to menu
	private function _add_link_to_menu(){
		if($this->input->post()){
			if($this->_validation_create()){
				$SQLdata = array(
					'parent_id'=>0,
					'menu_name'=> $this->input->post('label', true),
					'menu_link'=> $this->input->post('url', true),
					'menu_type'=> 'link',
					'menu_group_id'=> sys_config('menu_group_id')
				);
				$result = $this->mmenu->save($SQLdata);
				if($result){
					redirect(base_admin_url().$this->_controller_url);
				}
			}	
		}
		$this->load->view(config_item('admin_dir').$this->_controller_url.'list', $this->_data);
	}
	//Add Link to menu


	//Add Categories to menu
	private function _add_categories_to_menu(){
		if($this->input->post()){
			$id = $this->input->post('id',true);
			var_dump($id);
			$i = 0;
			if(!empty($id)){
				foreach($id as $category_id){
					$i++;
					$category_id = real_int($category_id);
					$category = $this->mcategory->find_by_id($category_id);
					$SQLdata = array(
						'parent_id'=>0,
						'menu_name'=> $category['category_name'],
						'category_id'=> $category['category_id'],
						'menu_type'=> 'category',
						'menu_group_id'=> sys_config('menu_group_id')
					);
					$this->mmenu->save($SQLdata);
				}
				if($i){
					redirect(base_admin_url().$this->_controller_url);
				}
				
			}
		}
	}
	//Add Categories to menu

	//************************ Create ***************************//

	//************************ Update **************************//
	public function update($id = null){
		$this->_variable();
		$this->_data['data'] = $this->mmenu->find_by_id($id);
		$data = $this->mmenu->find_by_id($id);
		if($this->_data['data']){
			if($this->input->post()):
				if($data['menu_type'] == 'link'){
					if($this->_validation_update()){

						$SQLDATA = array(
							'menu_name'=> $this->input->post('label', true),
							'menu_link'=> $this->input->post('url', true)
						);
						$result = $this->mmenu->update($id,$SQLDATA);

						if($result){
							redirect(base_admin_url().$this->_controller_url);
						}
					}
				}else{
		
					$SQLDATA = array(
						'menu_name'=> $this->input->post('menu_name', true)
					);
					$result = $this->mmenu->update($id,$SQLDATA);

					if($result){
						redirect(base_admin_url().$this->_controller_url);
					}
						
				}
				
			endif;
		  $this->load->view(config_item('admin_dir').$this->_controller_url.'update', $this->_data);
		}else{
			redirect(base_admin_url().$this->_controller_url);
		}
		
	}

	//************************ Update **************************//


	//********************* Delete *****************//

	public function _delete($id = null){
		if($id == 1){
			redirect(base_admin_url().$this->_controller_url);
		}
		$result = $this->mmenu->delete($id);
		if($result){
			redirect(base_admin_url().$this->_controller_url);
		}
	}

	public function delete($id){
		$result = $this->mmenu->delete($id);
		if($result){
			$this->db->where('parent_id',$id);
			$this->db->update('tbl_menus',array('parent_id'=>0));
			redirect(base_admin_url().$this->_controller_url);
		}
	}

	//********************* Delete *****************//


	public function save(){
			//$this->output->set_content_type('application/json');
				// Get the JSON string
			// secure with ajax request
		
			$jsonstring = $this->input->get('jsonstring',true);
			if(!empty($jsonstring)){
				// Decode it into an array
				var_dump($jsonstring);
				$jsonDecoded = json_decode($jsonstring, true);

				 // Run the function above
				$readbleArray = $this->mmenu->parseJsonArray($jsonDecoded);
				
				// Loop through the "readable" array and save changes to DB
				foreach ($readbleArray as $key => $value) {
				
					// $value should always be an array, but we do a check
					if (is_array($value)) {
					
						// Update DB
						$parent_id = $value['parent_id'];
						$menu_id = $value['id'];
						$data = array(
							'rang'=> $key,
							'parent_id'=>$parent_id
						);
						$this->mmenu->update($menu_id,$data);
						

					}
				}
			}else{
				//redirect(base_url().'cpadmin/menu');
			}
			
			
	}

}
