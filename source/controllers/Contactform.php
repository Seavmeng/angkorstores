<?php 
class contactform extends CI_Controller{
	public function __construct(){
		parent::__construct();
		//$this->load->helper('form_helper');
		$this->load->library('email');
		//$this->load->library('form_validation');
	}
	
	public function index(){
		$this->form_validation->set_rules('first-name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last-name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('subject', 'Subject', 'trim|required');
		$this->form_validation->set_rules('message', 'Message', 'required|max_length[150]');
		
		if($this->form_validation->run() == false){
			$this->load->view('home/contact');
		}
		else{
			$firstName = $this->input->post('first-name');
			$lastName = $this->input->post('last-name');
			$fromEmail = $this->input->post('email');
			$subject = $this->input->post('subject');
			$message = $this->input->post('message');
			
			//set email to be sent to
			$toMail = 'customerservice@angkorstores.com';
			
			//Email configuration
			
			/*$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'mail.angkorstores.com';
			$config['smtp_port'] = '26';
			$config['smtp_user'] = 'info@angkorstores.com';
			$config['mailtype'] = 'html';
			$config['charset'] = 'utf8';
			$config['wordwrap'] = true;
			$config['newline'] = '\r\n';*/
			
			//$this->email->initialize($config);
			
 			//start sending mail
			$this->email->from($fromEmail, $firstName.' '.$lastName);
			$this->email->to($toMail);
			$this->email->subject($subject);
			$this->email->message($message);
			
			//check if email seccessfully sent
			
			if($this->email->send()){
				$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Your mail has been sent successfully!</div>');
				//redirect(base_url().'contact');
				$this->load->view('home/contact');
			}
			else{
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">'.$this->email->print_debugger().'</div>');
				redirect(base_url().'p/contact-us');
				
				//$this->load->view('index');
			}
		}
	}
}
?>