<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Product_detail extends CI_Controller {
		
		function __construct()	{
			parent::__construct();
	//	$this->load->library('ion_auth');.
		$this->load->helper('product');
		$this->load->model('admin/product/moproduct');
		$this->load->model('admin/user/muser');
		$this->load->model('home_model');
		$this->_controller_url = $this->uri->segment(2).'/';
		$this->_controller_urls = $this->uri->segment(2).'/'.$this->uri->segment(3).'/';
		$this->_data['controller_url'] = $this->_controller_url;
		}
		private function __varaible(){
			$page = $this->input->get('start',true);
			$page = ($page) ? $page : 0;
			$page = real_int($page);
			$this->home_model->start = $page;
		}
		
		public function detail($permalink = null){
			//$user_id = user_id_logged();
			$product_id = $this->moproduct->get_id_by_permalink($permalink);
			$product_id=$product_id['product_id'];
			$this->_data['data_'] = $this->moproduct->find_by_id($permalink);
			$this->_data['get_group'] = $this->moproduct->find_group_id($permalink);
			$this->_data['get_contact'] = $this->moproduct->get_contact_user($this->_data['data_']['user_id']);
			$this->_data['gall_'] = $this->moproduct->find_gallery($product_id);
			$this->_data['username']=$this->muser->select_user_detil_by_id($this->_data['data_']['user_id']);
			$this->_data['catId'] = $this->moproduct->relatedCat($product_id);
			$catId = $this->_data['catId']['category_id'];
			$this->_data['relatedPro'] = $this->moproduct->relatedPro($catId);
			$this->_data['relatedProVendor'] = $this->moproduct->relatedProVendor($catId);
			$this->_data['varians']=$this->moproduct->varians($product_id);
			$this->_data['varians_size']=$this->moproduct->varians_size($product_id);
			$this->_data['check_varians']=$this->moproduct->check_varians($product_id);
			$this->_data['banner']=$this->home_model->get_banner(); 
			
			$this->load->view('home/single_product',$this->_data);
		}
		
		public function preview($permalink = null){
			//$user_id = user_id_logged();
			$product_id = $this->moproduct->get_id_by_permalink($permalink);
			$product_id =$product_id['product_id'];
	
			$this->_data['data_'] = $this->moproduct->find_by_id($permalink);
			$this->_data['get_group'] = $this->moproduct->find_group_id($permalink);
			$this->_data['get_contact'] = $this->moproduct->get_contact_user($this->_data['data_']['user_id']);
			$this->_data['gall_'] = $this->moproduct->find_gallery($product_id);
			$this->_data['username']=$this->muser->select_user_detil_by_id($this->_data['data_']['user_id']);
			$this->_data['catId'] = $this->moproduct->relatedCat($product_id);
			$catId = $this->_data['catId']['category_id'];
			$this->_data['relatedPro'] = $this->moproduct->relatedPro($catId);
			
			//$this->_data['varians']=$this->moproduct->varians($product_id);
			//$this->_data['check_varians']=$this->moproduct->check_varians($product_id);
			//$this->_data['comments'] = $this->moproduct->comment($product_id);
			//$this->_data['childComments'] = $this->moproduct->childComment($product_id);
			$this->load->view('home/single_product',$this->_data);
			//var_dump($product_id); exit;
		}

		public function vender_products($user_page=null)
		{	
			$this->__varaible();
			$this->data['slideshow']=$this->home_model->home_vendorPage_Slide();
			$this->data['user_detail']=$this->muser->getuser_detail_byname($user_page);
			$this->data['Featured_homepage'] = $this->home_model->featured_homepage_vendor_id($this->data['user_detail']['user_id']);
			$url = base_url().'/'.$this->uri->segment(1).'/';
			$this->home_model->count_all = true;
			$this->data['page'] = $this->home_model->page($url,$this->home_model->featured_homepage_vendor_id($this->data['user_detail']['user_id']));
			$page = $this->data['user_detail']['page_name'];
			if($user_page == $page){
			 $this->load->view('home/vender_products',$this->data);
			}else{
				echo '<div id="container" style="border: 1px solid #d0d0d0;box-shadow: 0 0 8px #d0d0d0; margin: 10px;">
						<h1 style="background-color: transparent;border-bottom: 1px solid #d0d0d0;color: #444;  font-size: 19px;font-weight: normal;
                                                    margin: 0 0 14px;padding: 14px 15px 10px;">
						    404 Page Not Found
						</h1>
						<p style=" margin: 12px 15px;">The page you requested was not found.</p>
				      </div>';
			}
		}
	
	
	}
?>