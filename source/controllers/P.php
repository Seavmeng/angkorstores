<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('home_model');	
		$this->_controller_url = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$this->_data['controller_url'] = $this->_controller_url;
	}
	private function _variable(){
		$page = $this->input->get('start',true);
		$page = ($page) ? $page : 0;
		$page = real_int($page);
		$this->home_model->start = $page;
		$this->_data['view_action'] = 'view';
		$this->_data['htitle'] = word_r('category');
		$this->_data['update_action'] = 'update';
		$this->_data['create_action'] = 'create';
		$this->_data['icon'] = 'fa fa-folder-open';
		$this->_data['check_ml'] = form_checkbox(array('onchange'=>'check_all(this)','data-set'=>'.checkboxes'));
		$option = array(
						''=>'----',
						'delete'=>word_r('delete')
				  );

		$this->_data['option'] = form_dropdown('ml',$option,'','class="form-control" onchange="go(this);"');
	}
	
	public function index($permilink)
	{
		$this->_variable();
		
		if($permilink =='contact-us'){
			$this->load->view('home/contact');
		}elseif($permilink =='about-us'){
			$this->load->view('home/about');
		}
        elseif($permilink =='customer-services'){
			$this->customer_services();
			
		}elseif($permilink =='terms-con'){
			$this->load->view('home/term_condition');
			
		}elseif($permilink =='about-cambodia'){

             $parent_cat = $this->input->get('parent_cat'); 

			 $data['blog_cantegories'] = $this->home_model->blog_product($parent_cat);
			 $base_url = base_url().$this->_controller_url.'?parent_cat='.$this->input->get('parent_cat').'&';
				$this->home_model->count_all = true;
				$data['page'] = $this->home_model->page_blog($base_url,$this->home_model->blog_product($parent_cat));
				//$data['showing'] =$this->home_model->showing($this->home_model->blog_product($parent_cat));

			 $this->load->view('home/blog',$data);
			
		}else{
			redirect(base_url().'errors');
		}
	}
        
    public function customer_services(){ 
		$this->form_validation->set_rules('name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		if($this->form_validation->run() == false){
			$data['topics'] = $this->home_model->select_topic();
			$this->load->view('home/customer_services',$data);
		}
		else{ 
			$name = $this->input->post('name');
			$phone = $this->input->post('phone');
			$topic = $this->input->post('topic');
			$email = $this->input->post('email');
			$description = $this->input->post('description');
			
                        $messages = "<table>
					<tr>
					  <td>Name:</td>
					  <td>".$name."</td>
					</tr>
					<tr>
					  <td>Phone:</td>
				          <td>".$phone."</td>
					</tr>
							<tr>
								<td>Topic:</td>
								<td>".$topic."</td>
							</tr>
							<tr>
								<td>Description:</td>
								<td>".$description."</td>
							</tr>		
				   </table>";

			//set email to be sent to
			$toMail = 'suncheanfc@gmail.com';
			//start sending mail
            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
			$this->email->set_header('Content-type', 'text/html');
			$this->email->from($email, $name);
			$this->email->to($toMail);
			$this->email->subject('Customer Service');
			$this->email->message($messages);
			//check if email seccessfully sent
			if($this->email->send()){
				$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Your mail has been sent successfully!</div>');
				redirect(base_url().'p/customer-services');
			}
			else{
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">'.$this->email->print_debugger().'</div>');
				redirect(base_url().'p/customer-services');
			}
		}

	}


}