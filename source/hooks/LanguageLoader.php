<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LanguageLoader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');
 
        $sys_lang = $ci->session->userdata('sys_lang');
        if ($sys_lang) {
            $ci->lang->load('system',$ci->session->userdata('sys_lang'));
            $ci->lang->load('form_validation',$ci->session->userdata('sys_lang'));
            $ci->config->set_item('language', $ci->session->userdata('sys_lang'));
        } else {
            $ci->lang->load('system','english');
            $ci->lang->load('form_validation','english');
            $ci->config->set_item('language', 'english');
        }
    }
}
 ?>