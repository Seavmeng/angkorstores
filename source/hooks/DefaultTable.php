<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class DefaultTable
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->library('table');
 
        $tmpl = array (
                    'table_open'          => '<div class="table-scrollable"><table class="table table-striped table-bordered table-hover sort-table">',

                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th>',
                    'heading_cell_end'    => '</th>',

                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td>',
                    'cell_end'            => '</td>',

                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td>',
                    'cell_alt_end'        => '</td>',

                    'table_close'         => '</table></div>'
              );

    $ci->table->set_template($tmpl);
    }
}
 ?>