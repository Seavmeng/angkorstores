<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mydb 
{

	private $ci; 
	public function __construct($config){
		$this->ci =& get_instance();
		$this->table_name = $config['table_name'];
		$this->id = $config['id'];
		$this->field_order = $config['id'];
	}
	private $table_name = '';
	private $id = '';
	public $per_page = 20;
	public $start = 0;
	public $count_all = false;
	public $field_order = '';
	public $order_by = 'DESC';
	public $fields = array();
	public $fields_search = array();
	public $where = '';
	public $base_url = '';
	public function sql($sql=""){
		echo $fields_arr;
		$sql .= " ORDER BY $this->field_order $this->order_by ";
		if(!$this->count_all){
			$sql .= "LIMIT {$this->start}, {$this->per_page}";
		}
		$query = $this->ci->db->query($sql);
		return $query->result_array();
	}

	public function get(){
		$page = $this->ci->input->get('page',true);
		$start = ($page) ? $page : 0;
		$this->start = real_int($start);

		$fields_arr = array();
		foreach($this->fields as $field => $value){
			$fields_arr[] = $field;
		}
		$fields_arr = implode(',', $fields_arr);
		$sql = '';
		$sql .= "SELECT $this->id,$fields_arr FROM $this->table_name ";
		$sql .= $this->where;
		if(!empty($this->fields_search)){
			$count = count($this->fields_search);
			if($count > 0){
				$where_and = '';
				foreach ($this->fields_search as $value) {
					$fields_name = $value['name'];
					$fileds_value = $value['value'];
					//var_dump($value);
					if($value['type'] == "like"){
						if(empty($where_and)){
							$where_and = ' WHERE ';
						}else{
							$where_and = ' AND ';
						}
						$sql .= $where_and;
						$sql .="$fields_name LIKE '%$fileds_value%' ";
						//$sql .= "'.$this->ci->db->like($value['name'], $value['value']).'";
					}elseif($value['type'] == "" || $value['type'] == "="){
						if(empty($where_and)){
							$where_and = ' WHERE ';
						}else{
							$where_and = ' AND ';
						}
						$sql .= $where_and;
						$sql .="$fields_name = '$fileds_value' ";
						//$where = $this->ci->db->where($value['name'], $value['value']);
						
					}
				}
			}	
		}
		$sql .= " ORDER BY $this->field_order $this->order_by ";
		if(!$this->count_all){
			$sql .= "LIMIT {$this->start}, {$this->per_page}";
		}
		$query = $this->ci->db->query($sql);
		return $query->result_array();
	}

	public function showing(){
		$page = $this->ci->input->get('page',true);
		$page = ($page) ? $page : 0;
		$this->count_all = true;
		$count_all = count($this->get());
		if(empty($count_all)) $start = 0; // start panination
			else $start = $page+1; // start panination
			$end   = $page+$this->per_page; // end pagination
		if($end >= $count_all){ //if end >= all records in table set it equal all records
				$end = $count_all;
		}
		
		return 'Showing '.$start.' to '.$end.' of '.$count_all.' entries';
	}
	public function page(){
		$this->count_all = true;
		$config['base_url'] = $this->base_url;
		$config['total_rows'] =  count($this->get());
		$config['per_page'] = $this->per_page;
		//$config['uri_segment'] = $uri_segment;
		$config['cur_tag_open'] = '<li class="active"><a href="#" >';
		$config['cur_tag_close'] = '</a></li>';
		$config['first_link'] = '&laquo; '.word_r('first');
   		$config['last_link'] = word_r('last').' &raquo;';
   		$config['full_tag_open'] = '<ul class="pagination">';
   		$config['full_tag_close'] = '</ul>';
   		// $config['use_page_numbers'] = TRUE;	
   		$config['page_query_string'] = TRUE;
   		$config['query_string_segment'] = "page";
   		$config['first_tag_open'] = '<li>';
   		$config['first_tag_close'] = '</li>';
   		$config['last_tag_open'] = '<li>';
   		$config['last_tag_close'] = '</li>';
   		$config['num_tag_open'] = '<li>';
   		$config['num_tag_close'] = '</li>';
   		$config['next_link'] = '&#8250';
   		$config['next_tag_open'] = '<li>';
   		$config['next_tag_close'] = '</li>';
   		$config['prev_link'] = '&#8249;';
   		$config['prev_tag_open'] = '<li>';
   		$config['prev_tag_close'] = '</li>';
		$this->ci->pagination->initialize($config);
		return $this->ci->pagination->create_links();
	}

	public function get_all(){
		$this->ci->db->select();
		$this->ci->db->from($this->table_name);
		$this->ci->db->order_by($this->field_order, $this->order_by);
		if(!$this->count_all){
			$this->ci->db->limit($this->per_page,$this->start);
		}
		$query = $this->ci->db->get();
		return $query->result_array();
	}

	public function save(){
		$this->ci->db->insert($this->table_name,$this->fields);
		return $this->ci->db->affected_rows();
	}


	public function get_by_id($id){
		$this->ci->db->select()->from($this->table_name)->where($this->id,$id);
		$query = $this->ci->db->get();
		return $query->first_row('array');
	}
	public function update($id){
		$this->ci->db->where($this->id,$id);
		$this->ci->db->update($this->table_name,$this->fields);
		return $this->ci->db->affected_rows();
	}

	public function delete($id){
		$this->ci->db->where($this->id,$id);
		$this->ci->db->delete($this->table_name);
		return $this->ci->db->affected_rows();
	}

	public function search($fields=array()){
		$this->ci->db->select()->from($this->table_name);
		$count = count($fields);
		//var_dump($fields);
		if($count > 0){
			foreach ($fields as $value) {
				//var_dump($value);
				if($value['type'] == "like"){
					$this->ci->db->like($value['name'], $value['value']);
				}else{
					$this->ci->db->where($value['name'], $value['value']);
				}
			}
		}		
		$this->ci->db->order_by($this->field_order, $this->order_by);
		if(!$this->count_all){
			$this->ci->db->limit($this->per_page,$this->start);
		}
		$query = $this->ci->db->get();
		return $query->result_array();
	}

	public function callback($field,$value,$table_name,$field_id,$id,$fn_name){
		$this->ci->db->select($field)->from($table_name)->where(array($field => $value, "$field_id !=" => $id));
		$query = $this->ci->db->get();
		$count = $query->num_rows();
		if($count > 0){
			$this->ci->form_validation->set_message($fn_name, 'The '.word_r($field).' field must contain a unique value');
			return true;
		}else{
			return false;
		}
	}
}
?>