<footer id="colophon" class="site-footer">
	
	<div class="footer-newsletter">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-5 col-md-5">
					<h5 class="newsletter-title">Sign up to Newsletter</h5><span class="error"></span>
				<!--<span class="newsletter-marketing-text">...and receive <strong>$20 coupon for first shopping</strong></span>-->
				</div>
				<div class="col-xs-12 col-sm-7 col-md-7">
					<?php echo form_open(base_url().'newsletter/index', array('name'=>'newsletter', 'id'=>'newsletter')); ?>
						<div class="input-group">
							<?php echo form_input(array('name'=>'email', 'id'=>'email', 'class'=>'form-control', 'placeholder'=>'Enter your email address')); ?>
							<span class="input-group-btn">
							
							<?php echo form_button(array('name'=>'btn_sign_up', 'id'=>'newsletter-signup', 'class'=>'btn btn-secondary','type'=>'submit', 'content'=>'Sign Up')); ?>
								
							</span>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom-widgets">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-7 col-md-push-5">
					<div class="columns">
						<aside id="nav_menu-2" class="widget clearfix widget_nav_menu">
							<div class="body">
								<h4 class="widget-title">Find It Fast</h4>
								<div class="menu-footer-menu-1-container">
									<ul id="menu-footer-menu-1" class="menu">
									<?php 
									$menu = footerMenu();
									foreach($menu as $each): ?>
										<li class="menu-item"><a href="<?php echo base_url(); ?>category/<?php echo $each['category_id'];?>"><?php echo $each['category_name']; ?></a></li>
									
									<?php endforeach; ?>
									</ul>
								</div>
							</div>
						</aside>
					</div><!-- /.columns -->

					<div class="columns">
						<aside id="nav_menu-3" class="widget clearfix widget_nav_menu">
							<div class="body">
								<h4 class="widget-title">Popular Categories</h4>
								<div class="menu-footer-menu-2-container">
									<ul id="menu-footer-menu-2" class="menu">
									<?php 
									$secMenu = footerMenu2();
									foreach($secMenu as $each): 
									?>
										<li class="menu-item"><a href="<?php echo base_url(); ?>category/<?php echo $each['category_id'];?>"><?php echo $each['category_name']; ?></a></li>
									<?php endforeach; ?>
									</ul>
								</div>
							</div>
						</aside>
					</div><!-- /.columns -->

					<div class="columns" style="margin-left: 1.5em;">
						<aside id="nav_menu-4" class="widget clearfix widget_nav_menu">
							<div class="body">
								<h4 class="widget-title">Customer Care</h4>
								<div class="menu-footer-menu-3-container">
									<ul id="menu-footer-menu-3" class="menu">
										<li class="menu-item"><a href="<?php echo base_admin_url();?>">My Account</a></li>
										<li class="menu-item"><a href="<?php echo base_url();?>trackorder">Track your Order</a></li>
										<li class="menu-item"><a href="<?php echo base_url();?>wishlist">Wishlist</a></li>
								<!--	<li class="menu-item"><a href="#">Customer Service</a></li>-->
								<!--	<li class="menu-item"><a href="#">Product Support</a></li>-->
										<li class="menu-item"><a href="<?php echo base_url();?>p/about-us">About Us</a></li>
										<li class="menu-item"><a href="<?php echo base_url();?>p/contact-us">Contact Us</a></li>
										<li class="menu-item"><a href="<?php echo base_url();?>p/terms-con">Terms & Conditions</a></li>
									</ul>
								</div><br/>
								
							</div>
						</aside>
					</div><!-- /.columns -->

				</div><!-- /.col -->

				<div class="footer-contact col-xs-12 col-sm-12 col-md-5 col-md-pull-7">
					<div class="footer-logo">
						<img src="<?php echo base_url();?>images/logo.png" width="190">
					</div><!-- /.footer-contact -->

					<div class="footer-call-us">
						<div class="media">
							<span class="media-left call-us-icon media-middle"><i class="ec ec-support"></i></span>
							<div class="media-body">
								<span class="call-us-number">Got Questions ? Call us 24/7!</span>
								<span class="call-us-number">Tel: +855 23 555 500 8 / +855 23 969 278</span>
								<span class="call-us-number">E-mail: <a href="https://angkorstores.com/p/contact-us">customerservice@angkorstores.com</a></span>
							</div>
						</div>
					</div><!-- /.footer-call-us -->
					<div class="footer-social-icons">
						<ul class="social-icons list-unstyled">
							<li><a class="fa fa-facebook" href="https://www.facebook.com/angkorstorescom-526713224180309/" target="_blank" style="color: #3b5998;"></a></li>
							<li><a class="fa fa-skype" href="skype:live:angkorstores?chat" style="color: #00aff0;"></a></li>
							<!--li><a class="fa fa-weixin" href="#"></a></li-->
							
							</ul>
					</div>	

					<div class="socail-media">				
						<a href="https://play.google.com/store/apps/details?id=com.wAngkorstorescom_4347312" style="float:left;" target="_blank" align="left"><img style="width: 80%;" src="<?php echo base_url();?>images/googleplay.png" ></a>
						<a href="http://afisedu.com/" style="float:left;" target="_blank" align="left"><img style="width: 80%;" src="<?php echo base_url();?>images/school.png"></a>
						<a href="http://www.vjpti.com/" style="float:left;" target="_blank" align="left"><img style="width: 80%;" src="<?php echo base_url();?>images/japantech.png"></a>						
					</div>
					<br><br><br>
					
				</div>

			</div>
		</div>
	</div>
	<div class="copyright-bar">
		<div class="container">
			<div class="pull-left flip copyright">&copy; <a href="<?php echo base_url();?>">Angkor Stores</a> - All Rights Reserved</div>
				<div class="footer-payment-logo">
					<ul class="cash-card card-inline">
						<li class="card-item"><img src="<?php echo base_url();?>images/wing.png" alt="" width="52"></li>
						<li class="card-item"><img src="<?php echo base_url();?>images/acleda.jpg" alt="" width="52"></li>
						<li class="card-item"><img src="<?php echo base_url();?>images/footer/payment-icon/2.png" alt="" width="52"></li>
						<!--li class="card-item"><img src="<?php echo base_url();?>images/footer/payment-icon/3.png" alt="" width="52"></li-->
						<li class="card-item"><img src="<?php echo base_url();?>images/footer/payment-icon/5.png" alt="" width="52"></li>
					</ul>
				</div><!-- /.payment-methods -->
			</div>
		</div><!-- /.container -->
	</div><!-- /.copyright-bar -->
</footer><!-- #colophon -->