<script>
   var base_url="<?php echo base_url();?>";
</script>
<!--Google Analytic Code-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89796918-1', 'auto');
  ga('send', 'pageview');
</script> 
<!--End of Google Analytic Code-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/tether.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/echo.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.easing.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/electro.js"></script>
 
 


<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css"></script>

 

<script src="<?php echo base_url();?>switchstylesheet/switchstylesheet.js"></script>

<script type="text/javascript">
	jQuery(function(){
		var y = jQuery(".time-year").val();
		var m = jQuery(".time-month").val();
		var d = jQuery(".time-day").val();
		var h = jQuery(".time-hour").val();
		var i = jQuery(".time-minute").val();
		var s = jQuery(".time-second").val();
		var dv = jQuery(".discount-value").val();
		var countDownDate = new Date(""+m+", "+d+", "+y+" "+h+":"+i+":"+s+"").getTime();		
		var x = setInterval(function() {
			var now = new Date().getTime();
			var distance = countDownDate - now;
			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			
			var display  = days + "d " + hours + "h :"+ minutes + "m :" + seconds + "s ";
			
			jQuery(".countdown").text(display);
	
			if (distance < 0 && dv > 0 ) {
				clearInterval(x);				
				location.reload();				
			}
			
		}, 1000);
	})
</script>
<style type="text/css">
	.countdown{
		border: 1px solid #ffc7a0;
		border-radius: 8px;
		color: #ed5f00;
		display: inline-block;
		line-height: 18px;
		margin-left: 5px;
		padding: 0 8px;
		font-size:12px;
	}
</style>



<link rel="stylesheet" href="https://payway.ababank.com/checkout-popup.html?file=css"/>
<script src="https://payway.ababank.com/checkout-popup.html?file=js"></script>

<script type="text/javascript">
	var $ = jQuery.noConflict();
	jQuery(function($){
		$('#checkout_button').click(function () {			
			AbaPayway.checkout();
		});
		
	});
</script>
		
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.elevatezoom.js"></script>
<script src="<?php echo base_url();?>assets/js/magnific-popup.js"></script>
<script type="text/javascript">
	jQuery("#zoom-0").elevateZoom({
			gallery:'gallery_01', 
			zoomWindowHeight:350,
			zoomWindowWidth:350,
			cursor: "crosshair",
			}); 
	jQuery("#zoom-0").bind("click", function(e) {
		var ez = jQuery('#zoom-0').data('elevateZoom');	
		jQuery.fancybox(ez.getGalleryList()); return false; 
	});
	jQuery('.zoom').magnificPopup({
		gallery: {
			enabled: true
		},
	  type: 'image'
	});

   (function($) {
	   $(document).ready(function(){
		   $("#zoom-0").elevateZoom({
				gallery:'gallery_01', 
				constrainType:"width", 
				constrainSize:350,
                cursor: "crosshair",
                zoomWindowFadeIn: 300,
                zoomWindowFadeOut: 750,
				galleryActiveClass: 'active', 
				imageCrossfade: true, 
				}); 
			$("#zoom-0").bind("click", function(e) {
				var ez = $('#zoom-0').data('elevateZoom');	
				$.fancybox(ez.getGalleryList()); return false; 
			});

			$('.zoom').magnificPopup({
				gallery: {
					enabled: true
				},
			  type: 'image'
			});
			 
		   $(".changecolor").switchstylesheet( { seperator:"color"} );
		   $('.show-theme-options').click(function(){
			   $(this).parent().toggleClass('open');
			   return false;
		   });

		   $('#home-pages').on( 'change', function() {
			   $.ajax({
				   url : $('#home-pages option:selected').val(),
				   success:function(res) {
					   location.href = $('#home-pages option:selected').val();
				   }
			   });
		   });

			$('#demo-pages').on( 'change', function() {
				$.ajax({
					url : $('#demo-pages option:selected').val(),
					success:function(res) {
						location.href = $('#demo-pages option:selected').val();
					}
				});
			});

			$('#header-style').on( 'change', function() {
				$.ajax({
					url : $('#header-style option:selected').val(),
					success:function(res) {
						location.href = $('#header-style option:selected').val();
					}
				});
			});

			$('#shop-style').on( 'change', function() {
				$.ajax({
					url : $('#shop-style option:selected').val(),
					success:function(res) {
						location.href = $('#shop-style option:selected').val();
					}
				});
			});

			$('#product-category-col').on( 'change', function() {
				$.ajax({
					url : $('#product-category-col option:selected').val(),
					success:function(res) {
						location.href = $('#product-category-col option:selected').val();
					}
				});
			});

			$('#single-products').on( 'change', function() {
				$.ajax({
					url : $('#single-products option:selected').val(),
					success:function(res) {
						location.href = $('#single-products option:selected').val();
					}
				});
			});

			$('.style-toggle').on( 'click', function() {
				$(this).parent('.config').toggleClass( 'open' );
			});
	   });
})(jQuery);
</script>

<script type="text/javascript">
(function($) {
	$(document).ready(function () {
	var carousel = $(".owl-stage");
	  carousel.owlCarousel({
		navigation:true,
		navigationText: [
		  "<i class='icon-chevron-left icon-white'><</i>",
		  "<i class='icon-chevron-right icon-white'>></i>"
		  ],
	  });
	});
})(jQuery);
</script>

