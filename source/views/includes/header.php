<?php $this->load->view('includes/top-bar');?>
<header id="masthead" class="site-header header-v1">
	<div class="container">
		<div class="row">
		<!-- ============================================================= Header Logo ============================================================= -->
		<div class="header-logo" style="margin-top:-25px;">
			<a href="<?php echo base_url();?>" class="header-logo-link">
				<img src="<?php echo base_url();?>images/logo.png" width="230">
			</a>
		</div>
		<!-- ============================================================= Header Logo : End============================================================= -->
		<!--- ============Starting new search=========== -->
	<!-- ============= End of New search====== -->
		<form class="navbar-search" method="get" action="<?php echo base_url();?>search/index">
			<label class="sr-only screen-reader-text" for="search">What are you looking for...</label>
			<div class="input-group">
				<input type="text" id="search" class="form-control search-field ui-autocomplete-input" dir="ltr" value="" name="search" placeholder="What are you looking for..." />
			
				<!--- Old Search Box 
				<div class="input-group-addon search-categories">
					<select name='product_cat' id='product_cat' class='postform resizeselect' >
						<option value='all' selected='selected'>All Categories</option>
						<?php $cats = searchCat();
						//$i =1;
						foreach($cats as $eachCat): ?>
						
							<option class="level-0" value="<?php echo $eachCat['category_id']; ?>"><?php echo $eachCat['category_name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				
				Old Search Box --->
				<div class="input-group-addon search-categories">
					<select name='product_cat' id='product_cat' class='postform resizeselect' >
						<option value='Product' selected='selected'>Product</option>
						<option value='Supplier'>Supplier</option>
					</select>
				</div>
				<div class="input-group-btn">
					<input type="hidden" id="search-param" name="post_type" value="product" />
					<button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
				</div>
			</div>
		</form>
		<?php $this->load->view('home/minicart');?>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-xs-12 col-lg-3">
				<div style="clear:both;" class="menu-list-vertical" >
					<nav>
						<ul class="list-group vertical-menu yamm make-absolute" >
							<li class="list-group-item" ><span><i class="fa fa-list-ul"></i> All Categories</span></li>
							<?php selectCategories(); ?>
							<!--
							<li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
								<a data-hover="dropdown"  href="<?php //echo base_url('category/allcategory');?>" title="More Categories" style="color:blue">More Categories</a>
							</li>-->
						</ul>
					</nav>
					
				</div>
			</div>
			<nav class="navbar navbar-primary yamm left_category" style="background-color: #fff;">
						<div class="container">
							<div class="clearfix" style="background: #fed700;">
							<!-- <button class="navbar-toggler hidden-sm-up pull-right flip" type="button" data-toggle=" -->
								<button class="navbar-toggler pull-right flip" type="button" data-toggle="collapse" data-target="#header-v3" style="float: left;">
									All Categories &#9776;
								</button>
							</div>
							<!-- <div class="collapse navbar-toggleable-xs" id="header-v3"> -->
							<div class="collapse" id="header-v3">
								<ul class="list-group vertical-menu yamm make-absolute nav navbar-nav">
									<?php selectCategories(); ?>
								</ul>
							</div><!-- /.collpase -->
						</div><!-- /.-container -->
					</nav><!-- /.navbar -->
			<div class="col-xs-12 col-lg-9">
				<nav>
					<ul id="menu-secondary-nav" class="secondary-nav">
						<li class="highlight menu-item"><a href="<?php echo base_url();?>">Home</a></li>
						<li class="menu-item"><a href="<?php echo base_url();?>p/about-us">About Us</a></li>
						<li class="menu-item"><a href="<?php echo base_url();?>p/contact-us">Contact Us</a></li>
				<!--	<li class="pull-right menu-item"><a href="blog-v2.html">Free Shipping on Orders $50+</a></li>-->
					</ul>
					</nav>
			</div>
		</div>
	</div>
</header>