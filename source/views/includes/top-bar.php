<?php $this->load->view('includes/chat');?>

<div class="top-bar">
	<div class="container">
		<nav>
			<ul id="menu-top-bar-left" class="nav nav-inline pull-left animate-dropdown flip">
				<li class="menu-item animate-dropdown"><a title="Welcome to Worldwide Electronics Store" href="#">Welcome to our website</a></li>
			</ul>
		</nav>
		<?php
		$user_id=get_user_id();
		$has_user = get_user_info($user_id);
		$get_fuser=$has_user['username'];
		$profile=get_profile($user_id);
		$get_prof_photo=$profile['image'];
		$group_id = $this->session->userdata('group_id');
		switch($group_id){
			case '1':
					$strLink = 'Profile';
			break;
			
			case '2':
					$strLink = 'Profile';
			break;
			
			case '6':
					$strLink ='Shipping address';
			break;
			default:
					$strLink = 'Profile';
			break;
		}
		if($get_prof_photo ==""){
			$get_profile='<img src="'.base_url().'assets/images/default-profile.jpg" align="left" width="30px" height="30px" alt="profile">';
		}else{
			$get_profile='<img src="'. base_url().'images/products/'.$get_prof_photo.'" width="30px" height="30px" alt="profile">';
		}
		if($has_user){
			$user_id=get_user_id();
		?>
		
		<ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
			<li class="nav-item"><a title="Track Your Order" href="<?php echo base_url();?>trackorder"><i class="ec ec-transport"></i>Track Your Order</a></li>
			<li class="nav-item">
				<a href="<?php echo base_admin_url();?>">
				<div class="profile">
					<div class="l_profile"><?php echo $get_profile;?></div>
					<div class="r_profile"><?php echo $get_fuser;?></div>
					<div class="clear"></div>
				</div>
				</a>
			</li>
			<li class="nav-item">
				<div class="btn-group cursor">
				  <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="fa fa-cog"></span>
				  </a>
				  <ul class="dropdown-menu dropdown-menu-right">
					<li><a href="<?php echo base_admin_url().'register/info'; ?>"><span class="fa fa-user">&nbsp;&nbsp;</span><?php echo $strLink; ?></a></li>
					
					
					<li role="separator" class="divider"></li>
					<li><a href="<?php echo base_admin_url().'logout'; ?>"><span class="fa fa-lock">&nbsp;&nbsp;</span>Logout</a></li>
				  </ul>
				</div>
			</li>
		</ul>
		<?php
		}else{
		?>
		<nav>
			<ul id="menu-top-bar-right" class="nav nav-inline pull-right animate-dropdown flip">
                <li class="menu-item animate-dropdown"><a title="Track Your Order" href="<?php echo base_url().'trackorder';?>"><i class="ec ec-transport"></i>Track Your Order</a></li>
				<li class="menu-item animate-dropdown"><a title="My Account" href="<?php echo base_admin_url();?>"><i class="ec ec-user"></i>My Account</a></li>
			</ul>
		</nav>
		<?php
		}
		?>
	</div>
</div><!-- /.top-bar -->
