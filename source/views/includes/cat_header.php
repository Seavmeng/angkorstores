<div class="top-bar">
	<div class="container">
		<nav>
			<ul id="menu-top-bar-left" class="nav nav-inline pull-left animate-dropdown flip">
				<li class="menu-item animate-dropdown"><a title="Welcome to Worldwide Electronics Store" href="#">Welcome to Angkor Stores</a></li>
			</ul>
		</nav>
		<nav>
			<ul id="menu-top-bar-right" class="nav nav-inline pull-right animate-dropdown flip">
				<li class="menu-item animate-dropdown"><a title="Store Locator" href="#"><i class="ec ec-map-pointer"></i>Store Locator</a></li>
				<li class="menu-item animate-dropdown"><a title="Track Your Order" href="track-your-order.html"><i class="ec ec-transport"></i>Track Your Order</a></li>
				<li class="menu-item animate-dropdown"><a title="Shop" href="shop.html"><i class="ec ec-shopping-bag"></i>Shop</a></li>
				<li class="menu-item animate-dropdown"><a title="My Account" href="<?php echo base_admin_url();?>"><i class="ec ec-user"></i>My Account</a></li>
			</ul>
		</nav>
	</div>
</div>
<header id="masthead" class="site-header header-v1">
	<div class="container">
		<div class="row">
		<!-- ============================================================= Header Logo ============================================================= -->
		<div class="header-logo" style="margin-top:-25px;">
			<a href="<?php echo base_url();?>" class="header-logo-link">
				<img src="<?php echo base_url();?>images/logo.jpg" width="190">
			</a>
		</div>
		<!-- ============================================================= Header Logo : End============================================================= -->
		<form class="navbar-search" method="get" action="http://transvelo.github.io/">
			<label class="sr-only screen-reader-text" for="search">Search for:</label>
			<div class="input-group">
				<input type="text" id="search" class="form-control search-field" dir="ltr" value="" name="s" placeholder="Search for products" />
				<div class="input-group-addon search-categories">
					<select name='product_cat' id='product_cat' class='postform resizeselect' >
						<option value='0' selected='selected'>All Categories</option>
						
							<option class="level-0" value="laptops-laptops-computers">Computer</option>
					</select>
				</div>
				<div class="input-group-btn">
					<input type="hidden" id="search-param" name="post_type" value="product" />
					<button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
				</div>
			</div>
		</form>
		<ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
			<li class="nav-item dropdown">
				<a href="cart.html" class="nav-link" data-toggle="dropdown">
					<i class="ec ec-shopping-bag"></i>
					<span class="cart-items-count count">4</span>
					<span class="cart-items-total-price total-price"><span class="amount">&#36;1,215.00</span></span>
				</a>
				<ul class="dropdown-menu dropdown-menu-mini-cart">
					<li>
						<div class="widget_shopping_cart_content">
							<ul class="cart_list product_list_widget ">
								<li class="mini_cart_item">
									<a title="Remove this item" class="remove" href="#">×</a>
									<a href="single-product.html">
										<img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="<?php echo base_url();?>images/products/mini-cart1.jpg" alt="">White lumia 9001&nbsp;
									</a>

									<span class="quantity">2 × <span class="amount">£150.00</span></span>
								</li>
								<li class="mini_cart_item">
									<a title="Remove this item" class="remove" href="#">×</a>
									<a href="single-product.html">
										<img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="<?php echo base_url();?>images/products/mini-cart2.jpg" alt="">PlayStation 4&nbsp;
									</a>

									<span class="quantity">1 × <span class="amount">£399.99</span></span>
								</li>
								<li class="mini_cart_item">
									<a data-product_sku="" data-product_id="34" title="Remove this item" class="remove" href="#">×</a>
									<a href="single-product.html">
									<img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="<?php echo base_url();?>images/products/mini-cart3.jpg" alt="">POV Action Cam HDR-AS100V&nbsp;

									</a>

									<span class="quantity">1 × <span class="amount">£269.99</span></span>
								</li>
							</ul><!-- end product list -->
							<p class="total"><strong>Subtotal:</strong> <span class="amount">£969.98</span></p>
							<p class="buttons">
								<a class="button wc-forward" href="cart.html">View Cart</a>
								<a class="button checkout wc-forward" href="checkout.html">Checkout</a>
							</p>
						</div>
					</li>
				</ul>
			</li>
		</ul>

		<ul class="navbar-wishlist nav navbar-nav pull-right flip">
			<li class="nav-item">
				<a href="wishlist.html" class="nav-link"><i class="ec ec-favorites"></i></a>
			</li>
		</ul>
		<ul class="navbar-compare nav navbar-nav pull-right flip">
			<li class="nav-item">
				<a href="compare.html" class="nav-link"><i class="ec ec-compare"></i></a>
			</li>
		</ul>
		</div><!-- /.row -->
		<div class="row">
			
			<div class="col-xs-12 col-lg-9">
				<nav>
					<ul id="menu-secondary-nav" class="secondary-nav">
						<li class="highlight menu-item"><a href="home-v2.html">Home</a></li>
						<li class="menu-item"><a href="home-v3-full-color.html">Trending</a></li>
						<li class="menu-item"><a href="home-v3.html">About Us</a></li>
						<li class="menu-item"><a href="blog-v1.html">Contact Us</a></li>
						<li class="pull-right menu-item"><a href="blog-v2.html">Free Shipping on Orders $50+</a></li>
					</ul>
					</nav>
			</div>
		</div>
	</div>
	</header>