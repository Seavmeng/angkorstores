<script src="https://use.fontawesome.com/dd87c831e5.js" charset="utf-8"></script>

	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <div class="panel-body">
      <ul class="chat" id="received">

      </ul>
    </div>
    <div class="panel-footer">
      <div class="clearfix">
        <div id="">
          <div class="input-group">
            <input id="chat_message" type="text" class="form-control input-sm has-error" placeholder="Type your message here..." />
            <span class="input-group-btn">
              <button class="btn" id="send_chat">adsSend</button>
            </span>
          </div>
        </div>
      </div>
  </div>
<script type="text/javascript">
var request_timestamp = 0;

var setCookie = function(key, value) {
var expires = new Date();
expires.setTime(expires.getTime() + (100 * 60 * 1000));
document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
console.log('cookie: ' + document.cookie);
}

var getCookie = function(key) {
var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
return keyValue ? keyValue[2] : null;
}

function S4() {
    return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}


// https://gist.github.com/kmaida/6045266
var parseTimestamp = function(timestamp) {
var d = new Date( timestamp * 1000 ), // milliseconds
  yyyy = d.getFullYear(),
  mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
  dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
  hh = d.getHours(),
  h = hh,
  min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
  ampm = 'AM',
  timeString;

if (hh > 12) {
  h = hh - 12;
  ampm = 'PM';
} else if (hh === 12) {
  h = 12;
  ampm = 'PM';
} else if (hh == 0) {
  h = 12;
}

timeString = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;

return timeString;
}

var sendChat = function (message, callback) {
	 $.getJSON('<?php echo base_url(); ?>Clientchat/send_message?message=' + message + '&conversation_id=' + getCookie('conversation_id') + '&send_by= <?= $this->session->userdata("client_user_id") ?>', function (data){
		callback();
	});
}

var append_chat_data = function (chat_data) {
	chat_data.forEach(function (data) {
		var is_me = data.send_by == '<?= $this->session->userdata('client_user_id') ?>';
		if(is_me){
			var html = '<li class="right clearfix">';
			html += '	<span class="chat-img pull-right">';
			// html += '		<img src="http://placehold.it/50/FA6F57/fff&text=' + data.nickname.slice(0,2) + '" alt="User Avatar" class="img-circle" />';
      html += '		<img src="http://placehold.it/50/FA6F57/fff&text=' + 'placeholder'.slice(0,2) + '" alt="User Avatar" class="img-circle" />';
			html += '	</span>';
			html += '	<div class="chat-body clearfix">';
			html += '		<div class="header">';
			html += '			<small class="timestamp text-muted"><span class="glyphicon glyphicon-time"></span>' + parseTimestamp(data.timestamp) + '</small>';
			html += '			<strong class="pull-right username primary-font">' + 'name' + '</strong>';
			html += '		</div>';
			html += '		<p class="message">' + data.message + '</p>';
			html += '	</div>';
			html += '</li>';
		}else{
			var html = '<li class="left clearfix">';
			html += '	<span class="chat-img pull-left">';
			html += '		<img src="http://placehold.it/50/55C1E7/fff&text=' +  'placeholder'.slice(0,2)  + '" alt="User Avatar" class="img-circle" />';
			html += '	</span>';
			html += '	<div class="chat-body clearfix">';
			html += '		<div class="header">';
			html += '			<strong class="primary-font username">' +'name' +'</strong>';
			html += '			<small class="pull-right timestamp text-muted"><span class="glyphicon glyphicon-time"></span>' + parseTimestamp(data.timestamp) + '</small>';
			html += '		</div>';
			html += '		<p class="message">' + data.message + '</p>';
			html += '	</div>';
			html += '</li>';
		}
		$("#received").html( $("#received").html() + html);
	});

	$('#received').animate({ scrollTop: $('#received').height()}, 1000);
}

var update_chats = function () {
	if(typeof(request_timestamp) == 'undefined' || request_timestamp == 0){
		var offset = 60*15; // 15min
		request_timestamp = parseInt( Date.now() / 1000 - offset );
	}
  var sessionValue = "<?php echo $this->session->userdata('user_id');?>";
    $.getJSON('<?php echo base_url(); ?>Clientchat/get_messages?timestamp=' + request_timestamp + '&conversation_id=' + getCookie('conversation_id'), function (data){
  		append_chat_data(data);
  		var newIndex = data.length-1;
  		if(typeof(data[newIndex]) != 'undefined'){
  			request_timestamp = data[newIndex].timestamp;
  		}
  	});
}
$('#send_chat').click(function (e) {
	e.preventDefault();
	var $field = $('#chat_message');
	var data = $field.val();
	$field.addClass('disabled').attr('disabled', 'disabled');
	sendChat(data, function (){
		$field.val('').removeClass('disabled').removeAttr('disabled');
	});
});


$('#chat_message').keyup(function (e) {
	if (e.which == 13) {
		$('#send_chat').trigger('click');
	}
});

setInterval(function (){
	update_chats();
}, 1500);

$( document ).ready(function() {
    var sessionValue = "<?php echo $this->session->userdata('client_user_id');?>" ? "<?php echo $this->session->userdata('client_user_id');?>" : 0;
    $.getJSON('<?php echo base_url(); ?>Clientchat/retrieve_thread?user_1=' +  sessionValue + '&user_2=1' , function (data){
        setCookie('conversation_id', data.conversation_id);
      });
});
</script>
<style media="screen">
.panel{
	margin-bottom: 0;
}
.panel-heading{
	margin: 0;
	cursor: pointer;
}
.chat
{
  list-style: none;
  margin: 0;
  padding: 0;
}
.chat li
{
  margin-bottom: 10px;
  padding-bottom: 5px;
}
.chat-body{
	background-color: #1db5ff;
	padding: 5px 10px;
	border-radius: 10px;
}
.chat li.left .chat-body
{
  margin-left: 50px;
}
.chat li.right .chat-body
{
  margin-right: 50px;
}


.chat li .chat-body p
{
  margin: 0;
  color: #ffffff;
}
.username{
	color: #ffffff;
}
.timestamp{
	color: #e0e0e0;
}
.chat-panel .slidedown .glyphicon, .chat .glyphicon
{
  margin-right: 5px;
}
.threads{
  list-style: none;
  list-style-type: none;
  padding: 0;
  margin: 0;
  overflow-x: hidden;
}
.thread-item{
  padding:0;
  margin: 2px 0 ;
  display: block;
}
.thread-item .row .col-md-3 img{
  margin: auto;
}
.thread-item:hover{
  background-color: #ededed;
  cursor: pointer;
}
.img-circle {
	max-width: 80%;
}

::-webkit-scrollbar-track
{
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
  background-color: #F5F5F5;
}

::-webkit-scrollbar
{
  width: 12px;
  background-color: #F5F5F5;
}

::-webkit-scrollbar-thumb
{
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
  background-color: #555;
}
</style>
