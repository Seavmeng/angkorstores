<footer id="colophon" class="site-footer">
	
	<div class="footer-newsletter">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-5 col-md-5">
					<h5 class="newsletter-title">Sign up to Newsletter</h5><span class="error"></span>
				<!--<span class="newsletter-marketing-text">...and receive <strong>$20 coupon for first shopping</strong></span>-->
				</div>
				<div class="col-xs-12 col-sm-7 col-md-7">
					<?php echo form_open(base_url().'newsletter/index', array('name'=>'newsletter', 'id'=>'newsletter')); ?>
						<div class="input-group">
							<?php echo form_input(array('name'=>'email', 'id'=>'email', 'class'=>'form-control', 'placeholder'=>'Enter your email address')); ?>
							<span class="input-group-btn">
							
							<?php echo form_button(array('name'=>'btn_sign_up', 'id'=>'newsletter-signup', 'class'=>'btn btn-secondary','type'=>'submit', 'content'=>'Sign Up')); ?>
								
							</span>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="footer-bottom-widgets">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="col-md-9">
				<?php 
									

					$menu = footerMenu_about_cambodia();
					$len = count($menu);
					$i=0;
					
					foreach($menu as $each):
					if($i%6==0){
										//echo '<ul class="menu">';
									
				?>
					<div class="col-md-4">
						<aside id="nav_menu-2" class="widget clearfix widget_nav_menu">
							<div class="body">
							<?php if($i==0){ ?>
								<h4 class="widget-title">About Cambodia</h4>
							<?php }else{ ?>
								<div style="position: relative; height:38px;"></div>
							<?php } ?>
								<div class="menu-footer-menu-1-container">
									<ul id="menu-footer-menu-1" class="menu">
									
									<?php
					}
										$link_To_sub_cat =  $each['count_child'] <= 0 ? '#' : base_url('p/about-cambodia').'?parent_cat='.$each['category_id']  ;	
									
									//else{
										//echo '</ul>';
									//}
									$j = $i%6;
									//echo '<br>'. $j;
									 ?>

										<li class="menu-item"><a href="<?php echo $link_To_sub_cat; ?>"><?php echo $each['category_name']; ?></a></li>
									
									<?php 
									if(($i%6==5 && $i!=0) || ($i== $len-1)){
										
									
									//$i++;	
									//endforeach; 
									?>
									</ul>
								</div>
							</div>
						</aside>
					</div>
					<?php
									}
									//if($i >2){break;}
						$i++;	
						endforeach; 
					?>
					</div><!-- /.columns -->
					

					<div class="col-md-3 customer-care">
						<aside id="nav_menu-4" class="widget clearfix widget_nav_menu footer-menu-last">
							<div class="body">
								<h4 class="widget-title">Customer Care</h4>
								<div class="menu-footer-menu-3-container">
									<ul id="menu-footer-menu-3" class="menu">
										<li class="menu-item"><a href="<?php echo base_admin_url();?>">My Account</a></li>
										<li class="menu-item"><a href="<?php echo base_url();?>trackorder">Track your Order</a></li>
										<li class="menu-item"><a href="<?php echo base_url();?>wishlist">Wishlist</a></li>
								<!--	<li class="menu-item"><a href="#">Customer Service</a></li>-->
								<!--	<li class="menu-item"><a href="#">Product Support</a></li>-->
										<li class="menu-item"><a href="<?php echo base_url();?>p/about-us">About Us</a></li>
										<li class="menu-item"><a href="<?php echo base_url();?>p/contact-us">Contact Us</a></li>
										<li class="menu-item"><a href="<?php echo base_url();?>p/terms-con">Terms & Conditions</a></li>

									<!--<li class="menu-item"><a href="<?php echo base_url();?>p/about-cambodia">About Cambodia</a></li>-->
									</ul>
								</div><br/>
								
							</div>
						</aside>
					</div><!-- /.columns -->

				</div><!-- /.col -->
				<div class="footer-contact col-xs-12 col-sm-12 col-md-12">
					
					<!--div class="footer-social-icons">
						<ul class="social-icons list-unstyled">
							<li><a class="fa fa-facebook" href="https://www.facebook.com/angkorstorescom-526713224180309/" target="_blank" style="color: #3b5998;"></a></li>
							<li><a class="fa fa-skype" href="skype:live:angkorstores?chat" style="color: #00aff0;"></a></li>
							
							<li><a href="https://play.google.com/store/apps/details?id=com.wAngkorstorescom_4347312" style="float:left;" target="_blank" align="left"><img style="width: 80%;" src="<?php echo base_url();?>images/googleplay.png" ></a></li>
							<li>
								<a href="http://afisedu.com/" style="float:left;" target="_blank" align="left"><img style="width: 80%;" src="<?php echo base_url();?>images/school.png"></a>
							</li>
							<li>
								<a href="http://www.vjpti.com/" style="float:left;" target="_blank" align="left"><img style="width: 80%;" src="<?php echo base_url();?>images/japantech.png"></a>
							</li>
							</ul>

					</div-->	

					<div class="socail-media">				
						<a href="https://play.google.com/store/apps/details?id=com.angkorstores.angkorstores" style="float:left;" target="_blank" align="left"><img style="width: 80%;" src="<?php echo base_url();?>images/googleplay.png" ></a>
						<a href="https://itunes.apple.com/us/app/angkor-store/id1274880042?mt=8" style="float:left;" target="_blank" align="left"><img style="width: 80%;" src="<?php echo base_url();?>images/appstore.png" ></a>
						<!--a href="http://afisedu.com/" style="float:left;" target="_blank" align="left"><img style="width: 80%;" src="<?php echo base_url();?>images/school.png"></a-->
						<!--a href="http://www.vjpti.com/" style="float:left;" target="_blank" align="left"><img style="width: 80%;" src="<?php echo base_url();?>images/japantech.png"></a-->
						
						<!--a href="#" style="float:left;"><img style="width: 80%;" src="<?php echo base_url();?>images/appstore.png"></a-->
					</div>
					<br><br><br>
					
				</div>
				

			</div>
			
				
			
		</div>
	</div>
	<div class="copyright-bar">
		<div class="container">
			<div class="col-md-4 flip copyright">&copy; <a href="<?php echo base_url();?>">Angkor Stores</a> - All Rights Reserved. </div>
			<div class="col-md-4 ui-follow-us">
				<span><p style="float:left;font-weight: bold;padding: 8px;margin: 0;">Follow Us:</p>
				<a style="float:left; font-size: 20px; padding: 10px;" class="fa fa-facebook" href="https://www.facebook.com/angkorstorescom-526713224180309/" target="_blank" style="color: #3b5998;"></a>
				<a style="float:left; font-size: 20px; padding: 10px;" class="fa fa-skype" href="skype:live:angkorstores?chat" style="color: #00aff0;"></a>
				</span>
			</div>
			
			<div class="col-md-4 flip payment clear-both">
				<div class="footer-payment-logo">
					<ul class="cash-card card-inline">
						<li class="card-item"><img src="<?php echo base_url();?>images/wing.png" alt="" width="52"></li>
						<li class="card-item"><img src="<?php echo base_url();?>images/footer/payment-icon/abapayment.png" alt="" width="102"></li>
					</ul>
				</div><!-- /.payment-methods -->
			</div>
		</div><!-- /.container -->
	</div><!-- /.copyright-bar -->
</footer><!-- #colophon -->