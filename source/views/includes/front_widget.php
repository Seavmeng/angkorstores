<div class="footer-widgets">
		<div class="container">
			<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12">
				<h4 class="widget-title">Hot Products</h4>
			</div>
				<div class="col-lg-4 col-md-4 col-xs-12">
					<aside class="widget clearfix">
						<div class="body">
							<!--h4 class="widget-title"></h4-->
							<ul class="product_list_widget">
							<?php 
							$hotProduct=hotProduct();
							$i = 0;
							foreach($hotProduct as $eachProduct): 
								if($eachProduct['status'] == 1){
							?>
								<li>
									<a href="<?php echo base_url();?>detail/<?php echo $eachProduct['permalink'];?>" title="<?php echo $eachProduct['product_name']; ?>">
										<img class="wp-post-image" src="<?php echo base_url();?>images/products/<?php echo $eachProduct['feature'];?>"  alt="">
										<span class="product-title"><?php echo $eachProduct['product_name']; ?></span>
									</a>
									<span class="electro-price"><span class="amount"><?php echo currency('sign').$eachProduct['sale_price'];?></span></span>
								</li>
							<?php }
							$i++;
							if($i==4){
								break;
							}
							 endforeach; ?>
							</ul>
						</div>
					</aside>
				</div>
				<div class="col-lg-4 col-md-4 col-xs-12">
					<aside class="widget clearfix">
						<div class="body">
							<!--h4 class="widget-title"></h4-->
							<ul class="product_list_widget">
							<?php 
						//	$hotProduct=khmerProduct();
							//$hotProduct=hotProduct();
							foreach($khmerProducts as $eachProduct): 
								if($eachProduct['status'] == 1){
									
							?>
								<li>
									<a href="<?php echo base_url();?>detail/<?php echo $eachProduct['permalink'];?>" title="<?php echo $eachProduct['product_name']; ?>">
										<img class="wp-post-image" src="<?php echo base_url();?>images/products/<?php echo $eachProduct['feature'];?>"  alt="">
										<span class="product-title"><?php echo $eachProduct['product_name']; ?></span>
									</a>
									<span class="electro-price"><span class="amount"><?php echo currency('sign').$eachProduct['sale_price'];?></span></span>
								</li>
							<?php } endforeach; ?>
							</ul>
						</div>
					</aside>
				</div>
				<div class="col-lg-4 col-md-4 col-xs-12">
					<aside class="widget clearfix">
						<div class="body">
							<!--h4 class="widget-title"></h4-->
							<ul class="product_list_widget">
							<?php 
							//$hotProduct=hotProduct();
							foreach($random3 as $eachProduct): 
								if($eachProduct['status'] == 1){
							?>
								<li>
									<a href="<?php echo base_url();?>detail/<?php echo $eachProduct['permalink'];?>" title="<?php echo $eachProduct['product_name']; ?>">
										<img class="wp-post-image" src="<?php echo base_url();?>images/products/<?php echo $eachProduct['feature'];?>"  alt="">
										<span class="product-title"><?php echo $eachProduct['product_name']; ?></span>
									</a>
									<span class="electro-price"><span class="amount"><?php echo currency('sign').$eachProduct['sale_price'];?></span></span>
								</li>
							<?php } endforeach; ?>
							</ul>
						</div>
					</aside>
				</div>
				<!--div class="col-lg-4 col-md-4 col-xs-12">
					<aside class="widget clearfix">
						<div class="body"><h4 class="widget-title">Khmer Products</h4>
							<ul class="product_list_widget">
								<li>
									<a href="#" title="Notebook Black Spire V Nitro  VN7-591G">
										<img class="wp-post-image" data-echo="<?php echo base_url();?>images/footer/3.jpg" src="<?php echo base_url();?>images/blank.gif" alt="">
										<span class="product-title">Notebook Black Spire V Nitro  VN7-591G</span>
									</a>
									<span class="electro-price"><ins><span class="amount">&#36;99.00</span></ins> <del><span class="amount">&#36;299.00</span></del></span>
								</li>

								<li>
									<a href="#" title="Tablet Red EliteBook  Revolve 810 G2">
										<img class="wp-post-image" data-echo="<?php echo base_url();?>images/footer/4.jpg" src="<?php echo base_url();?>images/blank.gif" alt="">
										<span class="product-title">Tablet Red EliteBook  Revolve 810 G2</span>
									</a>
									<span class="electro-price"><ins><span class="amount">&#36;99.00</span></ins> <del><span class="amount">&#36;299.00</span></del></span>
								</li>

								<li>
									<a href="#" title="Widescreen 4K SUHD TV">
										<img class="wp-post-image" data-echo="<?php echo base_url();?>images/footer/5.jpg" src="<?php echo base_url();?>images/blank.gif" alt="">
										<span class="product-title">Widescreen 4K SUHD TV</span>
									</a>
									<span class="electro-price"><ins><span class="amount">&#36;99.00</span></ins> <del><span class="amount">&#36;399.00</span></del></span>
								</li>
							</ul>
						</div>
					</aside>
				</div-->
				<!--div class="col-lg-4 col-md-4 col-xs-12">
					<aside class="widget clearfix">
						<div class="body">
							<h4 class="widget-title">Top Rated Products</h4>
							<ul class="product_list_widget">
								<li>
									<a href="#" title="Notebook Black Spire V Nitro  VN7-591G">
										<img class="wp-post-image" data-echo="<?php echo base_url();?>images/footer/6.jpg" src="<?php echo base_url();?>images/blank.gif" alt="">
										<span class="product-title">Notebook Black Spire V Nitro  VN7-591G</span>
									</a>
									<div class="star-rating" title="Rated 5 out of 5"><span style="width:100%"><strong class="rating">5</strong> out of 5</span></div>		<span class="electro-price"><ins><span class="amount">&#36;199.00</span></ins> <del><span class="amount">&#36;2,299.00</span></del></span>
								</li>

								<li>
									<a href="#" title="Apple MacBook Pro MF841HN/A 13-inch Laptop">
										<img class="wp-post-image" data-echo="<?php echo base_url();?>images/footer/7.jpg" src="<?php echo base_url();?>images/blank.gif" alt="">
										<span class="product-title">Apple MacBook Pro MF841HN/A 13-inch Laptop</span>
									</a>
									<div class="star-rating" title="Rated 5 out of 5"><span style="width:100%"><strong class="rating">5</strong> out of 5</span></div>		<span class="electro-price"><span class="amount">&#36;800.00</span></span>
								</li>

								<li>
									<a href="#" title="Tablet White EliteBook Revolve  810 G2">
										<img class="wp-post-image" data-echo="<?php echo base_url();?>images/footer/2.jpg" src="<?php echo base_url();?>images/blank.gif" alt="">
										<span class="product-title">Tablet White EliteBook Revolve  810 G2</span>
									</a>
									<div class="star-rating" title="Rated 5 out of 5"><span style="width:100%"><strong class="rating">5</strong> out of 5</span></div>		<span class="electro-price"><span class="amount">&#36;99.00</span></span>
								</li>
							</ul>
						</div>
					</aside>
				</div-->
			</div>
		</div>
	</div>