<script src="https://use.fontawesome.com/dd87c831e5.js" charset="utf-8"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<div class="container" id='live-chat'>
    <div class="row">
		<div class="panel panel-primary">
			<div class="panel-heading chat-heading" data-toggle="collapse" data-target="#chat-body" style="vertical-align:center;">
				<span class="glyphicon glyphicon-comment"></span> Live Chat
			</div>
			<div class="chat-panel-main collapse"   id="chat-body">
				<div class="panel-body">
					<ul class="chat" id="received">

					</ul>
				</div>
				<div class="panel-footer">
					<div class="clearfix">
						<div id="">
							<div class="input-group">
								<input id="chat_message " type="text" class="form-control has-error" placeholder="Type your message here..." />
								<span class="input-group-btn">
									<button class="btn" id="send_chat">Send</button>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

<script type="text/javascript">
	
	$(function () {
	
	var nickname = '<?= ($this->session->userdata()?$this->session->userdata('username'):"Anonymous"); ?>';
	var request_timestamp = 0;
	var setCookie = function(key, value) {
		var expires = new Date();
		expires.setTime(expires.getTime() + (5 * 60 * 1000));
		document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
	}

	var getCookie = function(key) {
		var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
		return keyValue ? keyValue[2] : null;
	}

	var guid = function() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	
	if(getCookie('user_guid') == null || typeof(getCookie('user_guid')) == 'undefined'){
		var user_guid = guid();
		setCookie('user_guid', user_guid);
	}
		
	// https://gist.github.com/kmaida/6045266
	var parseTimestamp = function(timestamp) {
	var d = new Date( timestamp * 1000 ), // milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		ampm = 'AM',
		timeString;
		
		if (hh > 12) {
		  h = hh - 12;
		  ampm = 'PM';
		} else if (hh === 12) {
		  h = 12;
		  ampm = 'PM';
		} else if (hh == 0) {
		  h = 12;
		}
		timeString = h + ':' +  min + ' ' + ampm;
		return timeString;
	}

	var sendChat = function (message, callback) {
		 $.getJSON('<?php echo base_url(); ?>api/send_message?message=' + message + '&guid=' + getCookie('user_guid') + '&nickname=' + nickname, function (data){
			console.log('callback response');
			callback();
		});
	}

	var append_chat_data = function (chat_data) {
		chat_data.forEach(function (data) {
				var is_me = data.nickname;
				if(is_me){
					var html = '<li class="right clearfix">';
					html += '	<span class="chat-img pull-right">';
					html += '		<img width="50" src="https://levinescholars.uncc.edu/sites/levinescholars.uncc.edu/files/default_images/no_avatar_placeholder.png"  class="img-circle" />';
						html += '	</span>';
						html += '	<div class="chat-body clearfix">';
						html += '		<div class="header">';
						html += '			<small class="timestamp text-muted"><span class="glyphicon glyphicon-time"></span>' + parseTimestamp(data.timestamp) + '</small>';
						html += '			<strong class="pull-right username primary-font">' + data.nickname + '</strong>';
						html += '		</div>';
						html += '		<p class="message">' + data.message + '</p>';
						html += '	</div>';
						html += '</li>';
				}else{
					var html = '<li class="left clearfix">';
					html += '	<span class="chat-img pull-left">';
					html += '		<img width="50" src="https://levinescholars.uncc.edu/sites/levinescholars.uncc.edu/files/default_images/no_avatar_placeholder.png"  class="img-circle" />';
					html += '	</span>';
					html += '	<div class="chat-body clearfix">';
					html += '		<div class="header">';
					html += '			<strong class="primary-font username">' +data.nickname +'</strong>';
					html += '			<small class="pull-right timestamp text-muted"><span class="glyphicon glyphicon-time"></span>' + parseTimestamp(data.timestamp) + '</small>';
					html += '		</div>';
					html += '		<p class="message">' + data.message + '</p>';
					html += '	</div>';
					html += '</li>';
				}
				$("#received").html( $("#received").html() + html);
			});

			$('#received').animate({ scrollTop: $('#received').height()}, 1000);
		}

		var update_chats = function () {
			if(typeof(request_timestamp) == 'undefined' || request_timestamp == 0){
				var offset = 60*15; // 15min
				request_timestamp = parseInt( Date.now() / 1000 - offset );
			}
		 
			$.getJSON('<?php echo base_url(); ?>api/get_messages?timestamp=' + request_timestamp + '&guid=' + getCookie('user_guid'), function (data){
				append_chat_data(data);
				var newIndex = data.length-1;
				if(typeof(data[newIndex]) != 'undefined'){
					request_timestamp = data[newIndex].timestamp;
				}
			});
		}
		$('#send_chat').click(function (e) {
			e.preventDefault();
			var $field = $('#chat_message');
			var data = $field.val();
			$field.addClass('disabled').attr('disabled', 'disabled');
			console.log('add disbled class');
			sendChat(data, function (){
			console.log('remove disbled class');
				$field.val('').removeClass('disabled').removeAttr('disabled');
			});
		});


		$('#chat_message').keyup(function (e) {
			if (e.which == 13) {
				$('#send_chat').trigger('click');
			}
		});

		setInterval(function (){
			update_chats();
		}, 1500);
	
	});
	</script>

	<style media="screen">
		/*bootstrap*/
		
		.panel {
		  margin-bottom: 20px;
		  background-color: #fff;
		  border: 1px solid transparent;
		  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
				  box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
		}
		.panel-body {
		  padding: 15px;
		}
		.panel-heading {
		  padding: 10px 15px;
		  border-bottom: 1px solid transparent;
		}
		.panel-heading > .dropdown .dropdown-toggle {
		  color: inherit;
		}
		.panel-title {
		  margin-top: 0;
		  margin-bottom: 0;
		  font-size: 16px;
		  color: inherit;
		}
		.panel-title > a,
		.panel-title > small,
		.panel-title > .small,
		.panel-title > small > a,
		.panel-title > .small > a {
		  color: inherit;
		}
		.panel-footer {
		  padding: 10px 15px;
		  background-color: #f5f5f5;
		  border-top: 1px solid #ddd;
		  border-bottom-right-radius: 3px;
		  border-bottom-left-radius: 3px;
		}
		.panel > .list-group,
		.panel > .panel-collapse > .list-group {
		  margin-bottom: 0;
		}
		.panel > .list-group .list-group-item,
		.panel > .panel-collapse > .list-group .list-group-item {
		  border-width: 1px 0;
		  border-radius: 0;
		}
		.panel > .list-group:first-child .list-group-item:first-child,
		.panel > .panel-collapse > .list-group:first-child .list-group-item:first-child {
		  border-top: 0;
		  border-top-left-radius: 3px;
		  border-top-right-radius: 3px;
		}
		.panel > .list-group:last-child .list-group-item:last-child,
		.panel > .panel-collapse > .list-group:last-child .list-group-item:last-child {
		  border-bottom: 0;
		  border-bottom-right-radius: 3px;
		  border-bottom-left-radius: 3px;
		}
		.panel > .panel-heading + .panel-collapse > .list-group .list-group-item:first-child {
		  border-top-left-radius: 0;
		  border-top-right-radius: 0;
		}
		.panel-heading + .list-group .list-group-item:first-child {
		  border-top-width: 0;
		}
		.list-group + .panel-footer {
		  border-top-width: 0;
		}
		.panel > .table,
		.panel > .table-responsive > .table,
		.panel > .panel-collapse > .table {
		  margin-bottom: 0;
		}
		.panel > .table caption,
		.panel > .table-responsive > .table caption,
		.panel > .panel-collapse > .table caption {
		  padding-right: 15px;
		  padding-left: 15px;
		}
		.panel > .table:first-child,
		.panel > .table-responsive:first-child > .table:first-child {
		  border-top-left-radius: 3px;
		  border-top-right-radius: 3px;
		}
		.panel > .table:first-child > thead:first-child > tr:first-child,
		.panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child,
		.panel > .table:first-child > tbody:first-child > tr:first-child,
		.panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child {
		  border-top-left-radius: 3px;
		  border-top-right-radius: 3px;
		}
		.panel > .table:first-child > thead:first-child > tr:first-child td:first-child,
		.panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child td:first-child,
		.panel > .table:first-child > tbody:first-child > tr:first-child td:first-child,
		.panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child td:first-child,
		.panel > .table:first-child > thead:first-child > tr:first-child th:first-child,
		.panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child th:first-child,
		.panel > .table:first-child > tbody:first-child > tr:first-child th:first-child,
		.panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child th:first-child {
		  border-top-left-radius: 3px;
		}
		.panel > .table:first-child > thead:first-child > tr:first-child td:last-child,
		.panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child td:last-child,
		.panel > .table:first-child > tbody:first-child > tr:first-child td:last-child,
		.panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child td:last-child,
		.panel > .table:first-child > thead:first-child > tr:first-child th:last-child,
		.panel > .table-responsive:first-child > .table:first-child > thead:first-child > tr:first-child th:last-child,
		.panel > .table:first-child > tbody:first-child > tr:first-child th:last-child,
		.panel > .table-responsive:first-child > .table:first-child > tbody:first-child > tr:first-child th:last-child {
		  border-top-right-radius: 3px;
		}
		.panel > .table:last-child,
		.panel > .table-responsive:last-child > .table:last-child {
		  border-bottom-right-radius: 3px;
		  border-bottom-left-radius: 3px;
		}
		.panel > .table:last-child > tbody:last-child > tr:last-child,
		.panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child,
		.panel > .table:last-child > tfoot:last-child > tr:last-child,
		.panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child {
		  border-bottom-right-radius: 3px;
		  border-bottom-left-radius: 3px;
		}
		.panel > .table:last-child > tbody:last-child > tr:last-child td:first-child,
		.panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child td:first-child,
		.panel > .table:last-child > tfoot:last-child > tr:last-child td:first-child,
		.panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child td:first-child,
		.panel > .table:last-child > tbody:last-child > tr:last-child th:first-child,
		.panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child th:first-child,
		.panel > .table:last-child > tfoot:last-child > tr:last-child th:first-child,
		.panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child th:first-child {
		  border-bottom-left-radius: 3px;
		}
		.panel > .table:last-child > tbody:last-child > tr:last-child td:last-child,
		.panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child td:last-child,
		.panel > .table:last-child > tfoot:last-child > tr:last-child td:last-child,
		.panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child td:last-child,
		.panel > .table:last-child > tbody:last-child > tr:last-child th:last-child,
		.panel > .table-responsive:last-child > .table:last-child > tbody:last-child > tr:last-child th:last-child,
		.panel > .table:last-child > tfoot:last-child > tr:last-child th:last-child,
		.panel > .table-responsive:last-child > .table:last-child > tfoot:last-child > tr:last-child th:last-child {
		  border-bottom-right-radius: 3px;
		}
		.panel > .panel-body + .table,
		.panel > .panel-body + .table-responsive,
		.panel > .table + .panel-body,
		.panel > .table-responsive + .panel-body {
		  border-top: 1px solid #ddd;
		}
		.panel > .table > tbody:first-child > tr:first-child th,
		.panel > .table > tbody:first-child > tr:first-child td {
		  border-top: 0;
		}
		.panel > .table-bordered,
		.panel > .table-responsive > .table-bordered {
		  border: 0;
		}
		.panel > .table-bordered > thead > tr > th:first-child,
		.panel > .table-responsive > .table-bordered > thead > tr > th:first-child,
		.panel > .table-bordered > tbody > tr > th:first-child,
		.panel > .table-responsive > .table-bordered > tbody > tr > th:first-child,
		.panel > .table-bordered > tfoot > tr > th:first-child,
		.panel > .table-responsive > .table-bordered > tfoot > tr > th:first-child,
		.panel > .table-bordered > thead > tr > td:first-child,
		.panel > .table-responsive > .table-bordered > thead > tr > td:first-child,
		.panel > .table-bordered > tbody > tr > td:first-child,
		.panel > .table-responsive > .table-bordered > tbody > tr > td:first-child,
		.panel > .table-bordered > tfoot > tr > td:first-child,
		.panel > .table-responsive > .table-bordered > tfoot > tr > td:first-child {
		  border-left: 0;
		}
		.panel > .table-bordered > thead > tr > th:last-child,
		.panel > .table-responsive > .table-bordered > thead > tr > th:last-child,
		.panel > .table-bordered > tbody > tr > th:last-child,
		.panel > .table-responsive > .table-bordered > tbody > tr > th:last-child,
		.panel > .table-bordered > tfoot > tr > th:last-child,
		.panel > .table-responsive > .table-bordered > tfoot > tr > th:last-child,
		.panel > .table-bordered > thead > tr > td:last-child,
		.panel > .table-responsive > .table-bordered > thead > tr > td:last-child,
		.panel > .table-bordered > tbody > tr > td:last-child,
		.panel > .table-responsive > .table-bordered > tbody > tr > td:last-child,
		.panel > .table-bordered > tfoot > tr > td:last-child,
		.panel > .table-responsive > .table-bordered > tfoot > tr > td:last-child {
		  border-right: 0;
		}
		.panel > .table-bordered > thead > tr:first-child > td,
		.panel > .table-responsive > .table-bordered > thead > tr:first-child > td,
		.panel > .table-bordered > tbody > tr:first-child > td,
		.panel > .table-responsive > .table-bordered > tbody > tr:first-child > td,
		.panel > .table-bordered > thead > tr:first-child > th,
		.panel > .table-responsive > .table-bordered > thead > tr:first-child > th,
		.panel > .table-bordered > tbody > tr:first-child > th,
		.panel > .table-responsive > .table-bordered > tbody > tr:first-child > th {
		  border-bottom: 0;
		}
		.panel > .table-bordered > tbody > tr:last-child > td,
		.panel > .table-responsive > .table-bordered > tbody > tr:last-child > td,
		.panel > .table-bordered > tfoot > tr:last-child > td,
		.panel > .table-responsive > .table-bordered > tfoot > tr:last-child > td,
		.panel > .table-bordered > tbody > tr:last-child > th,
		.panel > .table-responsive > .table-bordered > tbody > tr:last-child > th,
		.panel > .table-bordered > tfoot > tr:last-child > th,
		.panel > .table-responsive > .table-bordered > tfoot > tr:last-child > th {
		  border-bottom: 0;
		}
		.panel > .table-responsive {
		  margin-bottom: 0;
		  border: 0;
		}
		.panel-group {
		  margin-bottom: 20px;
		}
		.panel-group .panel {
		  margin-bottom: 0;
		  border-radius: 4px;
		}
		.panel-group .panel + .panel {
		  margin-top: 5px;
		}
		.panel-group .panel-heading {
		  border-bottom: 0;
		}
		.panel-group .panel-heading + .panel-collapse > .panel-body,
		.panel-group .panel-heading + .panel-collapse > .list-group {
		  border-top: 1px solid #ddd;
		}
		.panel-group .panel-footer {
		  border-top: 0;
		}
		.panel-group .panel-footer + .panel-collapse .panel-body {
		  border-bottom: 1px solid #ddd;
		}
		.panel-default {
		  border-color: #ddd;
		}
		.panel-default > .panel-heading {
		  color: #333;
		  background-color: #f5f5f5;
		  border-color: #ddd;
		}
		.panel-default > .panel-heading + .panel-collapse > .panel-body {
		  border-top-color: #ddd;
		}
		.panel-default > .panel-heading .badge {
		  color: #f5f5f5;
		  background-color: #333;
		}
		.panel-default > .panel-footer + .panel-collapse > .panel-body {
		  border-bottom-color: #ddd;
		}
		.panel-primary {
		  border-color: #337ab7;
		}
		.panel-primary > .panel-heading {
		  color: #fff;
		  background-color: #337ab7;
		  border-color: #337ab7;
		}
		.panel-primary > .panel-heading + .panel-collapse > .panel-body {
		  border-top-color: #337ab7;
		}
		.panel-primary > .panel-heading .badge {
		  color: #337ab7;
		  background-color: #fff;
		}
		.panel-primary > .panel-footer + .panel-collapse > .panel-body {
		  border-bottom-color: #337ab7;
		}
		.panel-success {
		  border-color: #d6e9c6;
		}
		.panel-success > .panel-heading {
		  color: #3c763d;
		  background-color: #dff0d8;
		  border-color: #d6e9c6;
		}
		.panel-success > .panel-heading + .panel-collapse > .panel-body {
		  border-top-color: #d6e9c6;
		}
		.panel-success > .panel-heading .badge {
		  color: #dff0d8;
		  background-color: #3c763d;
		}
		.panel-success > .panel-footer + .panel-collapse > .panel-body {
		  border-bottom-color: #d6e9c6;
		}
		.panel-info {
		  border-color: #bce8f1;
		}
		.panel-info > .panel-heading {
		  color: #31708f;
		  background-color: #d9edf7;
		  border-color: #bce8f1;
		}
		.panel-info > .panel-heading + .panel-collapse > .panel-body {
		  border-top-color: #bce8f1;
		}
		.panel-info > .panel-heading .badge {
		  color: #d9edf7;
		  background-color: #31708f;
		}
		.panel-info > .panel-footer + .panel-collapse > .panel-body {
		  border-bottom-color: #bce8f1;
		}
		.panel-warning {
		  border-color: #faebcc;
		}
		.panel-warning > .panel-heading {
		  color: #8a6d3b;
		  background-color: #fcf8e3;
		  border-color: #faebcc;
		}
		.panel-warning > .panel-heading + .panel-collapse > .panel-body {
		  border-top-color: #faebcc;
		}
		.panel-warning > .panel-heading .badge {
		  color: #fcf8e3;
		  background-color: #8a6d3b;
		}
		.panel-warning > .panel-footer + .panel-collapse > .panel-body {
		  border-bottom-color: #faebcc;
		}
		.panel-danger {
		  border-color: #ebccd1;
		}
		.panel-danger > .panel-heading {
		  color: #a94442;
		  background-color: #f2dede;
		  border-color: #ebccd1;
		}
		.panel-danger > .panel-heading + .panel-collapse > .panel-body {
		  border-top-color: #ebccd1;
		}
		.panel-danger > .panel-heading .badge {
		  color: #f2dede;
		  background-color: #a94442;
		}
		.panel-danger > .panel-footer + .panel-collapse > .panel-body {
		  border-bottom-color: #ebccd1;
		}


		.collapse {
		  display: none;
		}
		.collapse.in {
		  display: block;
		}
		tr.collapse.in {
		  display: table-row;
		}
		tbody.collapse.in {
		  display: table-row-group;
		}
		.collapsing {
		  position: relative;
		  height: 0;
		  overflow: hidden;
		  -webkit-transition-timing-function: ease;
			   -o-transition-timing-function: ease;
				  transition-timing-function: ease;
		  -webkit-transition-duration: .35s;
			   -o-transition-duration: .35s;
				  transition-duration: .35s;
		  -webkit-transition-property: height, visibility;
			   -o-transition-property: height, visibility;
				  transition-property: height, visibility;
		}
		/*END BOOTSTRAP*/





		.panel{
			margin-bottom: 0;
		}
		.panel-heading{
			margin: 0;
			cursor: pointer;
		}
		#send_chat {
		   padding: 8px 15px !important;
		}

		.chat-panel-heading{
			margin: 0;
			cursor: pointer;
		  background-color: #fed700;
		}
		.chat
		{
		  list-style: none;
		  margin: 0;
		  padding: 0;
		}
		.chat li
		{
		  margin-bottom: 10px;
		  padding-bottom: 5px;
		}
		.chat-body{
			background-color: #1db5ff;
			padding: 5px 10px;
			border-radius: 10px;
			margin-right:60px !important;
		}
		.chat li.left .chat-body
		{
		  margin-left: 50px;
		}
		.chat li.right .chat-body
		{
		  margin-right: 50px;
		}


		.chat li .chat-body p
		{
		  margin: 0;
		  color: #ffffff;
		}
		.username{
			color: #ffffff;
		}
		.timestamp{
			color: #e0e0e0;
		}
		.panel .slidedown .glyphicon, .chat .glyphicon
		{
		  margin-right: 5px;
		}
		.panel-body
		{
		  overflow-y: scroll;
		  height: 250px;
		}
		#panel-thread{
		  height: 300px;
		  padding: 0;
		  margin: 0;
		}

		#live-chat{
		  position: fixed;
		  bottom: 0;
		  right: 0;
		  width: 400px;
			margin: 0;
		  z-index: 9000;

		}

		#conversation_list{
		  position: fixed;
		  bottom: 0;
		  right: 400px;
		  width: 250px;
		  margin: 0;

		}
		.threads{
		  list-style: none;
		  list-style-type: none;
		  padding: 0;
		  margin: 0;
		  overflow-x: hidden;
		}
		.thread-item{
		  padding:0;
		  margin: 2px 0 ;
		  display: block;
		}
		.thread-item .row .col-md-3 img{
		  margin: auto;
		}
		.thread-item:hover{
		  background-color: #ededed;
		  cursor: pointer;
		}
		.img-circle {
			max-width: 80%;
		}

		::-webkit-scrollbar-track
		{
		  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		  background-color: #F5F5F5;
		}

		::-webkit-scrollbar
		{
		  width: 12px;
		  background-color: #F5F5F5;
		}

		::-webkit-scrollbar-thumb
		{
		  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
		  background-color: #555;
		}
	</style>
