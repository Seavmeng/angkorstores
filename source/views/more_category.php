<?php $this->load->view('includes/head');?>
<body class="page home page-template-default">
		<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/cat_header');?>
		<!-- #masthead -->

		<div id="content" class="site-content" tabindex="-1">
		<div class="container">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
					<!-- /.home-v1-slider -->
					<div class="col-lg-12"><h4><b>Product By Category</b></h4></div>
					<div class="home-v1-deals-and-tabs deals-and-tabs row animate-in-view fadeIn animated" data-animation="fadeIn">
				
						<div class="deals-block col-lg-4">
							<section class="section-onsale-product">
								<?php foreach($categories as $row){
								?>
								<header>
										<h5><?php echo $row['category_name']; ?></h5>
										<ul>
												<?php $sub_cat=pagesub_category($row['category_id']);
													$cat=explode(",", $sub_cat);
													foreach($cat as $subs){
														echo "<li>".$subs."</li>";
													}
												?>
										</ul>
								</header><!-- /header -->

								<div class="onsale-products">
									<?php //selectCategories();?>
								</div><!-- /.onsale-products -->
								<?php }?>
							</section><!-- /.section-onsale-product -->
						</div><!-- /.col -->

						<div class="deals-block col-lg-4">
							<section class="section-onsale-product">
								<?php foreach($categories_sub as $row){
								?>
								<header>
										<h5><?php echo $row['category_name']; ?></h5>
										<ul>
												<?php $sub_cat=pagesub_category($row['category_id']);
													$cat=explode(",", $sub_cat);
													foreach($cat as $subs){
														echo "<li>".$subs."</li>";
													}
												?>
										</ul>
								</header><!-- /header -->

								<div class="onsale-products">
									<?php //selectCategories();?>
								</div><!-- /.onsale-products -->
								<?php }?>
							</section><!-- /.section-onsale-product -->
						</div><!-- /.col -->
						

					</div>
				</main>
			</div>
		</div>
		</div>
	</div>
<?php $this->load->view('includes/footer');?>
</body>
</html>
