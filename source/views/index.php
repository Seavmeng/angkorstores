<?php $this->load->view('includes/head');?>
<body class="page home page-template-default">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/header');?>
		<div id="content" class="site-content" tabindex="-1">
		<div class="container">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
				<div class="home-v1-slider" >
					<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
						<?php foreach($slide as $sl){ ?>					
							<div class="item">
								<a href="<?php echo $sl->link; ?>" target="_blank">
									<img style="width: 100%;height: auto;" src="<?php echo base_url().'images/products/'.bigthumb($sl->photo);?>" />
								</a>
							</div>
						<?php } ?>
					</div>
					</div> 
					<section class="section-product-cards-carousel animate-in-view fadeIn animated" data-animation="fadeIn">
					<!-- ==========add banner position 1================== -->
					<div class="row ads">
						<center>
								<?php
								foreach ($banner as $position1) {
									if($position1->show_on=="Home" && $position1->status==1 && $position1->position == "Position 1"){
							?>
							<div class="col-md-6 fixedSize" style="padding-left: 4px;padding-right: 4px;">
								<a  href="<?php echo $position1->link ; ?>" target="_blank">
									<img   src="<?php echo base_url().'images/banner/'.$position1->images ?>" /><br/>
								</a>
							</div>
							<?php
									}
								} 
								?>
							</center>
					</div>	
				<!-- end of banner position 1 ==== -->
					<header>
							<h2 class="h1">Retail Products</h2>
						</header>
						<div id="home-v1-product-cards-careousel">
							<div class="woocommerce columns-3 home-v1-product-cards-carousel product-cards-carousel owl-carousel">
							<ul class="products columns-3">
							<?php
							$all=count($best_sellers);
							$i =0;
								foreach($Featured as $row):
									if ($i % 3 == 0){
										$extraClass = 'first';
									}
									elseif($i % 3 == 2){
										$extraClass = 'last';
									}
									else{
										$extraClass = 'mid';
									}
								?>
									<li class="product product-card <?php echo $extraClass; ?>">
										<div class="product-outer">
											<div class="media product-inner">
												<a class="media-left" href="<?php echo base_url() . 'detail/'. $row['permalink']; ?>" title="<?php echo $row['product_name'];?>">
													<img class="media-object wp-post-image img-responsive" src="<?php echo base_url();?>images/products/<?php echo $row['feature'];?>" data-echo="<?php echo base_url();?>images/products/<?php echo $row['feature'];?>" alt="">
												</a>
												<div class="media-body">
													<a href="<?php echo base_url() . 'detail/'. $row['permalink']; ?>">
														<h3><?php echo $row['product_name'];?></h3>
													</a>

													<div class="price-add-to-cart">
															<?php
															echo '<span class="price">
																<span class="electro-price">';
															$price_item=price_pro_dis($row['product_id']);
															if(!empty($price_item['discount'])){
																echo 
																'<ins><span class="amount">'.currency('sign'). number_format((float)$price_item['price_sale'], 2, '.', '') .'</span></ins>
																<del><span class="amount">'.currency('sign'). number_format((float)$price_item['price_dis'], 2, '.', '') .'</span></del>';
															}else{
																echo '<ins><span class="amount"> </span></ins>
																	<span class="amount">'.currency('sign'). number_format((float)$price_item['price_sale'], 2, '.', '') .'</span>';
															}
															echo '</span>
																</span>';
															?>
													</div><!-- /.price-add-to-cart -->

													<div class="hover-area">
														<div class="action-buttons">

															<a href="<?php echo base_url().'ad_wishlist/'.$row['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
															<a href="<?php echo base_url().'ad_compare/'.$row['product_id'];?>" class="add-to-compare-link"> Compare</a>
														</div>
													</div>

												</div>
											</div><!-- /.product-inner -->
										</div><!-- /.product-outer -->
									</li><!-- /.products -->
								
								<?php  //}
								//if($i == 8 ){break;}
								$i++; 
								endforeach; 
								
								$i =0;
								foreach($discounts as $row):
									if ($i % 3 == 0){
										$extraClass = 'first';
									}
									elseif($i % 3 == 2){
										$extraClass = 'last';
									}
									else{
										$extraClass = 'mid';
									}
								?>
									<li class="product product-card <?php echo $extraClass; ?>" style="display:none">
										<div class="product-outer">
											<div class="media product-inner">
												<a class="media-left" href="<?php echo base_url() . 'detail/'. $row['permalink']; ?>" title="<?php echo $row['product_name'];?>">
													<img class="media-object wp-post-image img-responsive" src="<?php echo base_url();?>images/products/<?php echo $row['feature'];?>" data-echo="<?php echo base_url();?>images/products/<?php echo $row['feature'];?>" alt="">
												</a>
												<div class="media-body">
													<a href="<?php echo base_url() . 'detail/'. $row['permalink']; ?>">
														<h3><?php echo $row['product_name'];?></h3>
													</a>
													<div class="price-add-to-cart">
														<?php
														echo '<span class="price">
															<span class="electro-price">';
														$price_item=price_pro_dis($row['product_id']);
														if(!empty($price_item['discount'])){
															echo 
															'<ins><span class="amount">'.currency('sign'). number_format((float)$price_item['price_sale'], 2, '.', '') .'</span></ins>
															<del><span class="amount">'.currency('sign'). number_format((float)$price_item['price_dis'], 2, '.', '') .'</span></del>';
														}else{
															echo '<ins><span class="amount"> </span></ins>
																<span class="amount">'.currency('sign'). number_format((float)$price_item['price_sale'], 2, '.', '') .'</span>';
														}
														echo '</span>
															</span>';
														?>
													</div>
													<div class="hover-area">
														<div class="action-buttons">
															<a href="<?php echo base_url().'ad_wishlist/'.$row['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
															<a href="<?php echo base_url().'ad_compare/'.$row['product_id'];?>" class="add-to-compare-link"> Compare</a>
														</div>
													</div>

												</div>
											</div><!-- /.product-inner -->
										</div><!-- /.product-outer -->
									</li><!-- /.products -->
								<?php  //}
								$i++; 
								endforeach; 
								?>
							</ul>
							</div>
						</div><!-- #home-v1-product-cards-careousel -->
					</section>
					<section class="section-product-cards-carousel animate-in-view fadeIn animated" data-animation="fadeIn">
						<div class="row ads">
							<center>																
								<?php 
									foreach ($banner as $position2) {
										if($position2->show_on=="Home" && $position2->status==1 && $position2->position == "Position 2"){
								?> 
								<div class="col-md-6 fixedSize" style="padding-left: 4px;padding-right: 4px;">
									<a  href="<?php echo $position2->link ; ?>" target="_blank">
										<img   src="<?php echo base_url().'images/banner/'.$position2->images ?>" /><BR/>
									</a> 
								</div>
								<?php
										}  
									} 
								?>																		 
								</center>
						</div>
						<header>
							<h2 class="h1">Wholesale Products</h2>
						</header>
						<div id="home-v1-product-cards-careousel">
							<div class="woocommerce columns-3 home-v1-product-cards-carousel product-cards-carousel owl-carousel">
							<ul class="products columns-3">
								<?php
							$all=count($best_sellers);
							//echo $all;
							$i =0;
								foreach($best_sellers as $row):
									if ($i % 3 == 0){
										$extraClass = 'first';
									}
									elseif($i % 3 == 2){
										$extraClass = 'last';
									}
									else{
										$extraClass = 'mid';
									}
									if($row['status'] == 1){
								?>
									<li class="product product-card <?php echo $extraClass; ?>">
										<div class="product-outer">
											<div class="media product-inner">
												<a class="media-left" href="<?php echo base_url() . 'detail/'. $row['permalink']; ?>" title="<?php echo $row['product_name'];?>">
													<img class="media-object wp-post-image img-responsive" src="<?php echo base_url();?>images/products/<?php echo $row['feature'];?>" data-echo="<?php echo base_url();?>images/products/<?php echo $row['feature'];?>" alt="">
												</a>
												<div class="media-body">
													<a href="<?php echo base_url() . 'detail/'. $row['permalink']; ?>">
														<h3><?php echo $row['product_name'];?></h3>
													</a>

													<div class="price-add-to-cart">
														<span class="price">
															<span class="electro-price">
																<ins><span class="amount"> </span></ins>
																							<span class="amount"><?php echo currency('sign').$row['sale_price'];?></span>
															</span>
														</span>
																<?php
																echo form_open(base_url()."cart/addcart");
																	echo form_hidden('product_id', $row['product_id']);
																	echo form_hidden('product_name', $row['product_name']);
																	echo form_hidden('sale_price', $row['sale_price']);
																	echo form_hidden('quantity', 1);
																		$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
																	//echo form_submit($btn);
																	$page_name = get_user_page($row['user_id']);
																	if($row['group_id'] != 2){
																		if($row['contact-type'] ==1){ ?>
																			<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().'p/contact-us'; ?>" class="fa fa-phone fa-1x"></a>
																		<?php }else{ 
 																			if(!check_has_attribute($row['product_id'])){
																				//echo form_button($btn['button']);
																			}
 																		}
																	}else{ ?> 
																		<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().$page_name.'/contact'; ?>" class="fa fa-phone fa-1x"></a>
																	<?php }
																//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
																echo form_close();
																?>
													</div><!-- /.price-add-to-cart -->

													<div class="hover-area">
														<div class="action-buttons">

															<a href="<?php echo base_url().'ad_wishlist/'.$row['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
															<a href="<?php echo base_url().'ad_compare/'.$row['product_id'];?>" class="add-to-compare-link"> Compare</a>
														</div>
													</div>

												</div>
											</div><!-- /.product-inner -->
										</div><!-- /.product-outer -->
									</li><!-- /.products -->
								
								<?php  }
								$i++; 
								endforeach; ?>
							</ul>
							</div>
						</div><!-- #home-v1-product-cards-careousel -->
					</section>
					
					<section class="home-v1-recently-viewed-products-carousel section-products-carousel animate-in-view fadeIn animated" data-animation="fadeIn">
						<!--- add banner position3 --- -->
						<div class="row ads">																
							<center>
								<?php
									foreach ($banner as $position3) {
										if($position3->show_on=="Home" && $position3->status==1 && $position3->position == "Position 3"){
								?>
								<div class="col-md-6 fixedSize" style="padding-left: 4px;padding-right: 4px;">
									<a  href="<?php echo $position3->link ; ?>" target="_blank">
										<img   src="<?php echo base_url().'images/banner/'.$position3->images ?>" /><BR/>
									</a> 
								</div>
								<?php
										}  
									} 
								?>
							</center>								
						</div>
					<!-- ========== end of banner position 3 ============ -->
						<header>
							<h2 class="h1">Recently Added</h2>
							<div class="owl-nav">
								<a href="#products-carousel-prev" data-target="#recently-added-products-carousel" class="slider-prev"><i class="fa fa-angle-left"></i></a>
								<a href="#products-carousel-next" data-target="#recently-added-products-carousel" class="slider-next"><i class="fa fa-angle-right"></i></a>
							</div>
						</header>
						<div id="recently-added-products-carousel">
							<div class="woocommerce columns-6">
								<div class="products owl-carousel recently-added-products products-carousel columns-6">
									<?php
									foreach($recently_added as $row){
									     if($row['status'] == 1){
									?>
									<div class="product">
										<div class="product-outer">
											<div class="product-inner">
												<a href="<?php echo base_url() . 'detail/'. $row['permalink']; ?>">
													<h3><?php  echo $row['product_name'];?></h3>
													<div class="product-thumbnail_">
														<img width="150" height="150" src="<?php echo base_url().'images/products/'.$row['feature'];?>" data-echo="<?php echo base_url().'images/products/'.$row['feature'];?>" class="img-responsive" alt="">
													</div>
												</a>
												<div class="price-add-to-cart">
													<span class="price">
														<span class="electro-price">
															<?php 
															$price_item=price_pro_dis($row['product_id']);
															if(!empty($price_item['discount'])){
																echo 
																'<ins><span class="amount">'.currency('sign'). number_format((float)$price_item['price_sale'], 2, '.', '') .'</span></ins>
																<del><span class="amount">'.currency('sign'). number_format((float)$price_item['price_dis'], 2, '.', '') .'</span></del>';
															}else{
																echo '<ins><span class="amount"> </span></ins>
																	<span class="amount">'.currency('sign'). number_format((float)$price_item['price_sale'], 2, '.', '') .'</span>';
															}
															?>
														</span>
													</span>
													<?php
																echo form_open(base_url()."cart/addcart");
																	echo form_hidden('product_id', $row['product_id']);
																	echo form_hidden('product_name', $row['product_name']);
																	echo form_hidden('sale_price', $row['sale_price']);
																	echo form_hidden('quantity', 1);
																		$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
																	//echo form_submit($btn);
																	$page_name = get_user_page($row['user_id']);
																	if($row['group_id'] != 2){
																		if($row['contact-type'] ==1){ ?>
																			<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().'p/contact-us'; ?>" class="fa fa-phone fa-1x"></a>
																		<?php }else{ 
 																			if(!check_has_attribute($row['product_id'])){
																			//	echo form_button($btn['button']);
																			}
 																		}
																	}else{ ?> 
																		<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().$page_name.'/contact'; ?>" class="fa fa-phone fa-1x"></a>
																	<?php }
																//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
																echo form_close();
																?>
													<!--a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a-->
												</div><!-- /.price-add-to-cart -->
												<div class="hover-area">
													<div class="action-buttons">
														<a href="<?php echo base_url().'ad_wishlist/'.$row['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
														<a href="<?php echo base_url().'ad_compare/'.$row['product_id'];?>" class="add-to-compare-link"> Compare</a>
													</div>
												</div>
											</div><!-- /.product-inner -->
											</div><!-- /.product-outer -->
									</div><!-- /.products -->
									<?php 
									}
									}
									?>
								</div>
							</div>
						</div>
					</section>
			<!-- ======== add banner position 4 ===== -->
					<div class="row ads" >
							<center>
								 <?php
									foreach ($banner as $position4) {
										if($position4->show_on=="Home" && $position4->status==1 && $position4->position == "Position 4"){
								?>
								<div class="col-md-6 fixedSize" style="padding-left: 4px;padding-right: 4px;">
									<a href="<?php echo $position4->link ; ?>" target="_blank">
										<img  src="<?php echo base_url().'images/banner/'.$position4->images ?>" /><BR/>
									</a> 
								</div>
								<?php
										}  
									} 
								 ?>
							 </center>
						</div>
						<!-- end position4 ---- >
						
				</main><!-- #main -->
				<!-- ===== add banner position right side ==== -->
				<!-- <div class="ads" style="padding:0px;">
						<br><br><br>
					<center>
						<?php
						foreach ($banner as $side_right) {
							if($side_right->show_on=="Home" && $side_right->status==1 && $side_right->position == "Fly Side Right"){
					?>
							<a  href="<?php echo $side_right->link ; ?>" target="_blank">
							<div class="hidden-xs-down hidden-md-down">
								<img    src="<?php echo base_url().'images/banner/'.$side_right->images ?>" />
							</div>
							</a> 							
					<?php
							}  
						} 
						?>
						</center>
				</div> -->
						<!--======End banner right side=========-->
			</div><!-- #primary -->
		</div><!-- .container -->
	</div><!-- #content -->
<?php $this->load->view('includes/front_widget');?>
<div class="row">
	<div class='col-md-10 col-sm-12 col-xs-12 '>
			<center>
			<div class="row ads" style="margin-left: 218px;width: 100%;">
				<?php
				foreach ($banner as $position5) {
					if($position5->show_on=="Home" && $position5->status==1 && $position5->position == "Position 5"){
				?>
				<div class="col-md-6 fixedSize" >
					<a  href="<?php echo $position5->link ; ?>" target="_blank">
						<img  src="<?php echo base_url().'images/banner/'.$position5->images ?>" /><BR />
					</a> 
				</div>
				<?php
					}  
				} 
				?>
			</div>
		</center>
	</div>
</div>
	<div id="mySidenav" class="sidenav hidden-sm-down">
	<span id="header"><p class="title">
		<b>Recently Added Products</b></p>
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>	
	</span>
		<div class="container-fluid content">
		<?php
		$i = 1;
		foreach($recently_added as $row){
			 if($row['status'] == 1){
		?>
		<div class="row border_bottom">
				<div class="col-md-6 col-sm-6">
					<a href="<?php echo base_url() . 'detail/'. $row['permalink']; ?>">
						<div class="product-thumbnail_">
							<img width="150px" height="150px" src="<?php echo base_url().'images/products/'.$row['feature'];?>" data-echo="<?php echo base_url().'images/products/'.$row['feature'];?>" class="img-responsive" alt="">
						</div>
					</a>
				</div> 
				<div class="col-md-6 col-sm-6">
					<p style="color:#0041bd;"><b><?php echo mb_strimwidth($row['product_name'], 0, 40);?></b></p>
					<p><?php echo "$".$row['sale_price'];?></p>
				</div> 
				
			</div>
			<?php 
			if($i++ == 3)break;
			}
			}
			?>
		</div>
	</div>

	<span class="btn btn-popup vertical-text hidden-sm-down" onclick="openNav()">
	<!--<i class="fa fa-chevron-circle-right fa-xs"></i>-->
	<p><b class="">Recently Added</b></p>
	</span>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "315px";
}
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
$(window).scroll(function(){
    $("#toTop").css("opacity", 1 - $(window).scrollTop() / 2250);
  });
</script>
     
<div style="clear:both;"></div>
<?php $this->load->view('includes/sub_footer');?>
</div><!-- #page -->
<?php $this->load->view('includes/footer');?>
<div style="display:none;">
    <a href="http://info.flagcounter.com/yZTI"><img src="http://s07.flagcounter.com/count2/yZTI/bg_FFFFFF/txt_000000/border_CCCCCC/columns_3/maxflags_15/viewers_0/labels_0/pageviews_0/flags_0/percent_0/" alt="Flag Counter" border="0"></a>
</div>
 
</body>
</html>