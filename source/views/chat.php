<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row chat-window col-xs-4 col-md-3" id="chat_window_1" style="margin-left:10px;">
        <div class="col-xs-12 col-md-12">
        	<div class="panel panel-default">
                <div class="panel-heading top-bar" style="background:#FED700; color:#000;">
                    <div class="col-md-8 col-xs-8">
                        <h3 class="panel-title"><span class="glyphicon glyphicon-comment"></span> Live Chat</h3>
                    </div>
                    <div class="col-md-4 col-xs-4" style="text-align: right; white-space:nowrap;">
                        <a href="#" style=" color:#000;"><span id="minim_chat_window" class="glyphicon glyphicon-minus icon_minim"></span></a>
                        <a href="#" style=" color:#000;"><span class="glyphicon glyphicon-remove icon_close" data-id="chat_window_1"></span></a>
                    </div>
                </div>
                <div class="panel-body msg_container_base" id="received">
                    
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" class="form-control input-sm chat_input" placeholder="Write your message here..." />
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm" id="btn-chat">Send</button>
                        </span>
                    </div>
                </div>
    		</div>
        </div>
    </div>
        
</div>


<style type="text/css">
	body{
		height:400px;
		position: fixed;
		bottom: 0;
	}
	.col-md-2, .col-md-10{
		padding:0;
	}
	.panel{
		margin-bottom: 0px;
	}
	.chat-window{
		bottom:0;
		position:fixed;
		float:right;
		margin-left:10px;
	}
	.chat-window > div > .panel{
		border-radius: 5px 5px 0 0;
	}
	.icon_minim{
		padding:2px 10px;
	}
	.msg_container_base{
	  background: #e5e5e5;
	  margin: 0;
	  padding: 0 10px 10px;
	  max-height:300px;
	  overflow-x:hidden;
	}
	.top-bar {
	  background: #666;
	  color: white;
	  padding: 10px;
	  position: relative;
	  overflow: hidden;
	}
	.msg_receive{
		padding-left:0;
		margin-left:0;
	}
	.msg_sent{
		padding-bottom:20px !important;
		margin-right:0;
	}
	.messages {
	  background: white;
	  padding: 10px;
	  border-radius: 2px;
	  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
	  max-width:100%;
	}
	.messages > p {
		font-size: 13px;
		margin: 0 0 0.2rem 0;
	  }
	.messages > time {
		font-size: 11px;
		color: #ccc;
	}
	.msg_container {
		padding: 10px;
		overflow: hidden;
		display: flex;
	}
	img {
		display: block;
		width: 100%;
	}
	.avatar {
		position: relative;
	}
	.base_receive > .avatar:after {
		content: "";
		position: absolute;
		top: 0;
		right: 0;
		width: 0;
		height: 0;
		border: 5px solid #FFF;
		border-left-color: rgba(0, 0, 0, 0);
		border-bottom-color: rgba(0, 0, 0, 0);
	}

	.base_sent {
	  justify-content: flex-end;
	  align-items: flex-end;
	}
	.base_sent > .avatar:after {
		content: "";
		position: absolute;
		bottom: 0;
		left: 0;
		width: 0;
		height: 0;
		border: 5px solid white;
		border-right-color: transparent;
		border-top-color: transparent;
		box-shadow: 1px 1px 2px rgba(black, 0.2); // not quite perfect but close
	}

	.msg_sent > time{
		float: right;
	}

	.msg_container_base::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	.msg_container_base::-webkit-scrollbar
	{
		width: 12px;
		background-color: #F5F5F5;
	}

	.msg_container_base::-webkit-scrollbar-thumb
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
		background-color: #555;
	}

	.btn-group.dropup{
		position:fixed;
		left:0px;
		bottom:0;
	}
</style>

<script type="text/javascript">
	$(document).on('click', '.panel-heading span.icon_minim', function (e) {
		var $this = $(this);
		if (!$this.hasClass('panel-collapsed')) {
			$this.parents('.panel').find('.panel-body').slideUp();
			$this.addClass('panel-collapsed');
			$this.removeClass('glyphicon-minus').addClass('glyphicon-plus');
		} else {
			$this.parents('.panel').find('.panel-body').slideDown();
			$this.removeClass('panel-collapsed');
			$this.removeClass('glyphicon-plus').addClass('glyphicon-minus');
		}
	});
	$(document).on('focus', '.panel-footer input.chat_input', function (e) {
		var $this = $(this);
		if ($('#minim_chat_window').hasClass('panel-collapsed')) {
			$this.parents('.panel').find('.panel-body').slideDown();
			$('#minim_chat_window').removeClass('panel-collapsed');
			$('#minim_chat_window').removeClass('glyphicon-plus').addClass('glyphicon-minus');
		}
	});
	$(document).on('click', '#new_chat', function (e) {
		var size = $( ".chat-window:last-child" ).css("margin-left");
		 size_total = parseInt(size) + 400;
		alert(size_total);
		var clone = $( "#chat_window_1" ).clone().appendTo( ".container" );
		clone.css("margin-left", size_total);
	});
	$(document).on('click', '.icon_close', function (e) {
		//$(this).parent().parent().parent().parent().remove();
		$( "#chat_window_1" ).remove();
	});
</script>

<script type="text/javascript">
	$(function(){
		var request_timestamp = 0;
		var setCookie = function(key, value) {
			var expires = new Date();
			expires.setTime(expires.getTime() + (5 * 60 * 1000));
			document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
		}

		var getCookie = function(key) {
			var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
			return keyValue ? keyValue[2] : null;
		}

		var guid = function() {
			function s4() {
				return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			}
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
		}

		if(getCookie('user_guid') == null || typeof(getCookie('user_guid')) == 'undefined'){
			var user_guid = guid();
			setCookie('user_guid', user_guid);
		}				

		// https://gist.github.com/kmaida/6045266
		var parseTimestamp = function(timestamp) {
			var d = new Date( timestamp * 1000 ), // milliseconds
				yyyy = d.getFullYear(),
				mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
				dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
				hh = d.getHours(),
				h = hh,
				min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
				ampm = 'AM',
				timeString;
					
			if (hh > 12) {
				h = hh - 12;
				ampm = 'PM';
			} else if (hh === 12) {
				h = 12;
				ampm = 'PM';
			} else if (hh == 0) {
				h = 12;
			}

			timeString = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
				
			return timeString;
		}
		
		/**======================================**/
				
		var sendChat = function (message, callback) {
			$.getJSON('<?php echo base_url(); ?>api/send_message?message=' + message + '&nickname=' + $('#nickname').val() + '&guid=' + getCookie('user_guid'), function (data){
				callback();
			});
		}

		var append_chat_data = function (chat_data) {
			chat_data.forEach(function (data) {
				var is_me = data.guid == getCookie('user_guid');
				
				if(is_me){				
					var html = '<div class="row msg_container base_sent">';
                        html += ' <div class="col-md-10 col-xs-10">';
                        html += '     <div class="messages msg_sent">';
                        html += '         <p>' + data.message + '</p>';
                        html += '         <time datetime="2009-11-13T20:00">' + data.nickname + ' • '+ parseTimestamp(data.timestamp) +'</time>';
                        html += '     </div>';
                        html += ' </div>';
                        html += ' <div class="col-md-2 col-xs-2 avatar">';
                        html += '     <img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">';
                        html += ' </div>';
                    html += ' </div>';
					
				}else{
					
                    var html = '<div class="row msg_container base_receive">';
						html += '	<div class="col-md-2 col-xs-2 avatar">';
						html += '		<img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">';
						html += '	</div>';
						html += '	<div class="col-md-10 col-xs-10">';
						html += '		<div class="messages msg_receive">';
						html += '			<p>' + data.message + '</p>';
						html += '			<time datetime="2009-11-13T20:00">' + data.nickname + ' • '+ parseTimestamp(data.timestamp) +'</time>';
						html += '		</div>';
						html += '	</div>';
						html += '</div>';
				
				}
				$("#received").html( $("#received").html() + html);
			});
		  
			$('#received').animate({ scrollTop: $('#received').height()}, 1000);
		}

		var update_chats = function () {
			if(typeof(request_timestamp) == 'undefined' || request_timestamp == 0){
				var offset = 60*15; // 15min
				request_timestamp = parseInt( Date.now() / 1000 - offset );
			}
			$.getJSON('<?php echo base_url(); ?>api/get_messages?timestamp=' + request_timestamp, function (data){
				append_chat_data(data);	

				var newIndex = data.length-1;
				if(typeof(data[newIndex]) != 'undefined'){
					request_timestamp = data[newIndex].timestamp;
				}
			});      
		}
		
		
		$('#btn-chat').click(function (e) {
			e.preventDefault();
			
			var $field = $('#btn-input');
			var data = $field.val();

			$field.addClass('disabled').attr('disabled', 'disabled');
			sendChat(data, function (){
				$field.val('').removeClass('disabled').removeAttr('disabled');
			});
			
		});

		$('#btn-input').keyup(function (e) {
			if (e.which == 13) {
				$('#submit').trigger('click');
			}
		});

		setInterval(function (){
			update_chats();
		}, 1500);
						
	})
</script>