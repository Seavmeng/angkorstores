<?php $this->load->view('includes/head');?>
<body class="page home page-template-default">
	<div id="page" class="hfeed site">
		<?php $this->load->view('includes/header1');?>
		<div id="content" class="site-content" tabindex="-1">
			<div class="container">
				<nav class="woocommerce-breadcrumb" >
					<a href="home.html">Home</a>
					<span class="delimiter"><i class="fa fa-angle-right"></i></span>
					My Account
				</nav><!-- .woocommerce-breadcrumb -->
				<?php 
				if(isset($errors)){
				   echo show_msg($errors);
				}
				?>
				
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<article id="post-8" class="hentry">
							<div class="entry-content">
								<div class="woocommerce">
									<div class="customer-login-form">
										<h2>Register</h2>
										<?php echo form_open(account_url()."register", array('role' => 'form','class' => 'form-horizontal'))?>
										<div class="page-content">
											<div class="row">
												<div class="col-sm-12">
												   <div class="col-sm-5">
												  		<h4 class="page-title">Authentication</h4>
														<div class="page-content">
															<div class="row">
															  <div class="col-sm-12">
																<p>Register with us for future convenience:</p>
																<ul class="register_type">
																	<li>
																	
																		<label>
																			<input type="radio" name="checkout" value="<?php echo $vender['user_group_id']; ?>" 
																			<?php if($user_info['checkout']==2){
																				echo 'checked';
																			}else{
																				echo '';
																			} ?>>
																			Supplier 
																		</label>
																	</li>
																	<li>
																		<label>
																			<input type="radio" name="checkout" value="<?php echo $billing['user_group_id'];?> " 
																			<?php if($user_info['checkout']==6){
																				echo 'checked';
																			}else{
																				echo '';
																			} ?>>
																			Buyer
																		</label>
																	</li>
																	<font color="red"><?php echo form_error('checkout');?></font>
																</ul>
																<br>
																<h6>Register and save time!</h6>
																<p>Register with us for future convenience:</p>
																<p>
																	<i class="fa fa-check-circle text-primary"></i>
																	Fast and easy check out
																</p>
																<p>
																	<i class="fa fa-check-circle text-primary"></i>
																	Easy access to your order history and status
																</p>
															  </div>
														  </div>
														</div>
												    </div>
												  <!-- <div class="col-md-2"></div> -->
												  <div class="col-sm-7">
													<!-- normal control -->
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo word_r('name')?></label>
														<div class="col-md-9">
															<?php echo form_input('username', $this->input->post('username', true),"class = 'form-control' id= 'username'"); ?>
															<font color="red"><?php echo form_error('username');?></font>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo word_r('email')?></label>
														<div class="col-md-9">
															<?php echo form_input('email', $this->input->post('email', true),"class = 'form-control' id= 'email'"); ?>
															<font color="red"><?php echo form_error('email');?></font>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo word_r('password')?></label>
														<div class="col-md-9">
															<input type="password" name="password" value="<?php echo $this->input->post('password'); ?>" class="form-control" placeholder="Your Password." autocomplete="off" id="pass1"/>
															<font color="red"><?php echo form_error('password');?>
															<div id="passwordDescription">&nbsp;</div>
															<div id="passwordStrength" class="strength0"></div></span></font>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo word_r('password_confirm')?></label>
														<div class="col-md-9">
															<input type="password" class="form-control" name="password_confirm" value="<?php echo $this->input->post('password_confirm'); ?>"  placeholder="Your Password Again." autocomplete="off" id="pass2" />
															<font color="red"><?php echo form_error('password_confirm');?></font>
															<span id="confirmMessage" class="confirmMessage"></span>
														</div>
													</div>
													
													
													<!-- <div class="form-group">
														<label class="col-md-3 control-label">User Type</label>
														<div class="col-md-9">
															<select name="user_type" class="form-control" style="border:1px solid #B0B0B0;">
																<?php foreach ($group_name as $value) { ?>
																	<option value="<?php echo $value['user_group_id'] ?>"><?php echo $value['user_group_name'] ?></option>
																<?php } ?>
															</select><br/>
															<font color="red"><?php echo form_error('user_type');?></font>													
														</div>
													</div> -->
													<div class="form-group">
														<label class="col-md-3 control-label"><?php /*echo word_r('Fill-captcha-code')*/ echo "Captcha";?></label>
														<div class="col-md-9">
															<span class='captcha'>
															<?php echo $cap_img; ?>
															</span>
															<input type="text" name="captcha"/>
															<a class ='refresh_cap'>
																<i id='ref_symbol' class="fa fa-refresh btn-lg"></i>
															</a><span>Refresh</span>
															<font color="red"><?php echo form_error('captcha');?></font>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-6">
															<input type="submit" value="Register" name="btn_gu_submit" class="btn btn-primary" />
														</div>
													</div>
												 </div>
												</div>
												
											</div>
										</div>
										<?php echo form_close(); ?>
										

								

									</div><!-- /.customer-login-form -->
								</div><!-- .woocommerce -->
							</div><!-- .entry-content -->

						</article><!-- #post-## -->

					</main><!-- #main -->
				</div><!-- #primary -->


			</div><!-- .col-full -->
		</div><br/><br/><!-- #content -->
<?php $this->load->view('includes/sub_footer');?>

	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>
</body>
</html>
