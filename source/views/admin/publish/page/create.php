<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $create_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open_multipart(base_admin_url().$controller_url.$this->uri->segment(4), array('role' => 'form') ); ?>
					
						
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
							         <!-- Row -->
							         <div class="row">
							         		 <!-- Form -->
							         		<div class="col-md-8">
							         			<!--Form-body -->
													<div class="form-body">
													
														<!-- ROW -->
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																	<div class="form-group required <?php has_error('title'); ?>">
																		<?php echo form_label(word_r('title'),'title',array('class' => 'control-label') ); ?>
																		<?php echo form_input('title', $this->input->post('title', true),"class = 'form-control', id= 'title'"); ?>
																	</div>
															</div>
															<!--End col-md-6 -->

														</div>
														<!-- End row -->

														
														<!-- ROW -->
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																	<div class="form-group">
																		<?php echo form_label(word_r('article'),'article',array('class' => 'control-label') ); ?>
																		<?php echo form_textarea(array('name' => 'article', 'class' => 'form-control mce', 'id' => 'article',  'value' => $this->input->post('article')) );?>
																	</div>
															</div>
															<!--End col-md-6 -->

														</div>
														<!-- End row -->



													</div>
													<!--End form-body -->
							         		</div>
							         		<!-- End Form -->

							         		<!-- Action -->
							         		<div class="col-md-4">
							         			
							         			
							         			<!-- Row 1-->
							         			<div class="row">
							         				<!-- col-md-12 -->
							         				<div class="col-md-12">
							         					<!-- panel -->
								         				<div class="panel panel-default">

								         					<!-- panel-heading -->
															<div class="panel-heading">
																<h3 class="panel-title"> <?php word('publish'); ?> </h3>
															</div>
															<!-- end panel-heading -->

															<!-- panel-body -->
															<div class="panel-body">
																 <div class="form-group">
																		<?php echo form_label(word_r('visibility'),'visibility',array('class' => 'control-label'));; ?>
																		<div class="check-list">
																			<?php echo '<label>'.form_checkbox('visibility',1,true).word_r('visibility_public').'</label>'; ?>
																		</div>
																</div>

																<div class="form-group">
																	<div class="row">
																		<div class="col-md-12">
																		<?php echo form_label(word_r('publish_date'),'publish_date',array('class' => 'control-label'));; ?>
																		</div>
																	</div>
																	<div class="row">

																		<div class="col-md-12">
																				<div class="input-group <?php has_error('publish_date'); has_error('time');  ?>">
																					<span class="input-group-addon">
																					 	<i class="fa fa-calendar"></i>
																					 </span>
																					<?php echo form_input(array('name' => 'publish_date', 'class' => 'form-control date-picker', 'id' => 'publish_date',  'value' => $this->input->post('publish_date') == "" ? date('d-m-Y') : $this->input->post('publish_date') ) ); ?>											
																					<span class="input-group-addon">
																					 	<i class="fa fa-clock-o"></i>
																					 </span>
																					
																					<?php echo form_input(array('name' => 'time', 'class' => 'form-control  timepicker-24', 'id' => 'time',  'value' => $this->input->post('time') == "" ? date('H:i:s') : $this->input->post('time') ) ); ?>
																				</div>
											
																		</div>
																	</div>
																	
																</div>

																<div class="form form-actions">
																		<?php echo form_submit('create', word_r('create'), "class = 'btn blue' "); ?>
																		<?php echo '<a href="'.base_admin_url().$controller_url.'" class="btn default">'.word_r('cancel').'</a>'; ?>
																</div>

															</div>
															<!-- end panel-body -->

														</div>
														<!-- end panel -->
							         				</div>
							         				<!-- end col-md-12 -->
							         				
							         			</div>
							         			<!--End Row 1-->


							         			



							         				<!-- Row 3-->
							         			<div class="row">
							         				<!-- col-md-12 -->
							         				<div class="col-md-12">
							         					<!-- panel -->
								         				<div class="panel panel-default">

								         					<!-- panel-heading -->
															<div class="panel-heading">
																<h3 class="panel-title"> <?php word('thumbnail'); ?> </h3>
															</div>
															<!-- end panel-heading -->

															<!-- panel-body -->
															<div class="panel-body" >
																<div class="fileinput fileinput-new" data-provides="fileinput">
																  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																    <img data-src="holder.js/100%x100%" alt="">
																  </div>
																  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
																  <div>
																    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="userfile[]"></span>
																    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
																  </div>
																</div>
																
															</div>
															<!-- end panel-body -->

														</div>
														<!-- end panel -->
							         				</div>
							         				<!-- end col-md-12 -->
							         				
							         			</div>
							         			<!--End Row 3-->


							         			
							         		</div>
							         		<!-- End Action -->
							         </div>
							          <!-- End Row -->
							         




									</div>
								<!-- end portlet-body -->
							
						<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->
<script>
    var base_admin_assets_url = "<?php echo base_admin_assets_url(); ?>";
</script>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
