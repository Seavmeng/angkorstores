<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $view_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->

						<!-- portlet-body -->
						<div class="portlet-body">

							<div class="row">
								<div class="col-md-12 col-sm-12">
									<?php echo form_open(base_admin_url().$controller_url.'search', array('role' => 'form','method'=>'get')); ?>
										
										<div class="form-group col-md-4">
											
											<div class="col-md-12">
												<?php echo form_label(word_r('publish_date'), 'publish_date',array('class' => 'control-label')); ?>
												<div class="input-group input-large date-picker input-daterange">
													<?php 
														//From Date
															$from = $this->input->get('from', true);
															if(empty($from)){
																$from = date('01-m-Y');
															}
															$from = strtotime($from);
															$from = date('d-m-Y',$from);
														//From Date
													 ?>
													<?php echo form_input(array('name' => 'from', 'class' => 'form-control date-picker', 'id' => 'from',  'value' => $from ) ); ?>											
													<span class="input-group-addon">
													<?php word('to'); ?> </span>
													<?php 
														//From Date
															$to = $this->input->get('to', true);
															if(empty($to)){
																$to = date('t-m-Y');
															}
															$to = strtotime($to);
															$to = date('d-m-Y',$to);
														//From Date
													 ?>

													<?php echo form_input(array('name' => 'to', 'class' => 'form-control  date-picker', 'id' => 'to',  'value' => $to ) ); ?>
												</div>
											
											</div>
										</div>

											<div class="form-group col-md-2">
												<?php echo form_label(word_r('text'), 'text',array('class' => 'control-label')); ?>
												<?php echo form_input(array('name' => 'text', 'class' => 'form-control', 'id' => 'text',  'value' => $this->input->get('text') ) ); ?>											
										</div>
										
										<div class="form-group col-md-2">
												<?php echo form_label(word_r('visibility'), 'visibility',array('class' => 'control-label')); ?>
												<?php echo $option_search; ?>											
										</div>

										<div class="form-group col-md-2">
												<?php echo form_label(word_r('categories'), 'categories',array('class' => 'control-label')); ?>
												<?php echo show_category_select_option_search('category_id',$this->input->get('category_id'),1 ); ?>											
										</div>

										<div class="form-group col-md-2">
												<label for="" class="control-label">&nbsp;</label>
												<input type="submit" class="btn btn blue form-control" value="<?php word('search'); ?>">											
										</div>
										
									<?php echo form_close(); ?>
								</div>
							</div>
							

							<?php
								echo form_open(base_admin_url().$controller_url.'ml', array('role' => 'form')); 

								$this->table->set_heading($check_ml,word_r('thumbnail'), word_r('title'),word_r('username'),word_r('tags'),word_r('categories'), word_r('publish_date'), word_r('visibility'), array('data' => word_r('action'),'width'=>'140px'));
								$i =0;
								foreach($data as $value){
									$dir = date('m-Y', strtotime($value['create_date']));
									$id = $value['post_id'];
									$image = $value['thumbnail'];
									$title = $value['post_title'];
									$username = $value['username'];
									$publish_date = normal_date($value['publish_date']);
									$active = $value['visibility'];
									$this->table->add_row(
															form_checkbox(array('name'=>'id[]','value'=>$id,'class'=>'checkboxes')),
															admin_image_gallery($dir.'/thumb/'.$image, $title),
															anchor(base_admin_url().$controller_url.'update/'.$id, $title, array('title'=>$title)),
															anchor(base_admin_url().$controller_url.'search?user_id='.$value['user_id'], $username, array('title'=>$username)),
															get_tag_join_link($id),
															get_category_join_link($id),
															"$publish_date",
															check_visibility($active),
															btn_action($controller_url.'update/'.$id, $controller_url.'delete/'.$id)
														);
								}

								echo $this->table->generate();
								
							 ?>
							 <div class="row">

								<div class="col-md-6 col-sm-12">
									<div class="col-sm-12">
										<div class="col-sm-7 dataTables_info" id="sample_1_info" role="status" aria-live="polite">
											<?php echo $showing; ?>
										</div>

										<div class="option col-sm-5">
												<?php echo $option; ?>

										</div>
										
									</div>
								</div>
								<?php echo form_close(); ?>
								<div class="col-md-6 col-sm-12">
									 <div class="dataTables_paginate paging_bootstrap_full_number">
									 	<?php echo $page; ?>
									 </div>
						             
								</div>
							</div>
						</div>
						<!-- end portlet-body -->
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	<script>
			$(document).ready(function(){
				$(".sort-table").tablesorter({
			            headers: { 			                   
			                    0: { 
			                        sorter: false 
			                    },
			                    1: { 
			                        sorter: false 
			                    },
			                    7: { 
			                        sorter: false 
			                    }, 
			                    8: { 
			                        sorter: false 
			                    }
			            }

          }); 

			});
		</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	