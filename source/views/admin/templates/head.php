
<?php 
	if(!has_logined()){
		redirect(base_admin_url().'login');
	}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	<title><?php echo sys_config('sys_name'); ?></title>
	
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/> -->
	<link href="<?php echo base_admin_assets_url();?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link href="<?php echo base_admin_assets_url();?>global/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/plugins/jquery-tags-input/jquery.tagsinput.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/plugins/jquery-nestable/jquery.nestable.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL PLUGIN STYLES -->

	<!-- BEGIN THEME STYLES -->
	<link href="<?php echo base_admin_assets_url();?>global/css/components.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/css/blueimp-gallery.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>global/css/bootstrap-image-gallery.css" rel="stylesheet" type="text/css"/>
	
	<link href="<?php echo base_admin_assets_url();?>admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_admin_assets_url();?>admin/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo base_admin_assets_url();?>admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
	<!-- END THEME STYLES -->
	
	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	

</head>