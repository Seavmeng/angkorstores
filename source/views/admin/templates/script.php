</div>
  <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
         2016 &copy; Angkor Stores.
    </div>
    <div class="scroll-to-top" style="display: block;">
      <i class="icon-arrow-up"></i>
  </div>
</div>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/respond.min.js"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script>
   var base_url="<?php echo base_url();?>";
</script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/jquery.blueimp-gallery.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/bootstrap-image-gallery.js" type="text/javascript"></script>
<!-- END CORE PLUGINS --> 

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>tinymce/tinymce.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_admin_assets_url(); ?>global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>


<!-- END PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_admin_assets_url();?>global/scripts/jquery.nestable.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/plugins/jquery.tablesorter.min.js" type="text/javascript"></script>
<script src="<?php echo base_admin_assets_url(); ?>global/scripts.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>

<!--HTML to PDF-->
<script type="text/javascript" src="<?php echo base_url();?>js/jspdf/jspdf.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>js/jspdf/pdfFromHTML.js"></script> 
<!--=========-->


<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init() // init quick sidebar
  // Index.init();   
   //Index.initDashboardDaterange();
   //Index.initJQVMAP(); // init index page's custom scripts
   //Index.initCalendar(); // init index page's custom scripts
  // Index.initCharts(); // init index page's custom scripts
   //Index.initChat();
   //Index.initMiniCharts();
  // Index.initIntro();
  // Tasks.initDashboardWidget();
});
</script>
<!-- END JAVASCRIPTS -->
