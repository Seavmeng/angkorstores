<?php $this->load->view("account/shared/header.php");?>
	<body>
		<?php $this->load->view("account/shared/nav.php");?> 
		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-9 col-sm-8">
					<h3><span class="fui-document"></span>Manage Categories</h3>
				</div><!-- /.col -->
				<div class="col-md-3 col-sm-4 text-right"></div><!-- /.col -->
			</div><!-- /.row -->
			<hr class="dashed margin-bottom-30">
			<div class="panel panel-default">
				<table class="table">
					<tr>
						<td style="border-bottom: 1px solid #ebebeb;">
							<button class="btn btn-primary btn-labeled fa fa-plus-circle pull-right mar-rgt category" data-toggle="modal" data-target="#myModal">
								Create Category                                
							</button>
						</td>
					</tr>
					<tr>
						<td>
							<div class="columns columns-right btn-group pull-right">
								<button title="Refresh" name="refresh" onclick="ajax_set_list()" type="button" class="btn btn-default"><i class="fa fa-refresh icon-refresh"></i></button>
								<button onclick="export_it('pdf');" title="%s" type="button" class="btn btn-default">pdf</button>
								<button onclick="export_it('csv');" title="%s" type="button" class="btn btn-default">csv</button>
								<button onclick="export_it('excel');" title="%s" type="button" class="btn btn-default">xls</button>
							</div>
							<div class="pull-right search"><input type="text" placeholder="Search" class="form-control"></div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="panel panel-default">
								<table data-search="true" data-show-columns="false" data-show-toggle="true" data-ignorecol="0,2" data-show-refresh="true" data-pagination="true" class="table table-striped table-hover" id="demo-table">

									<thead>
										<tr>
											<th style="">
												<div class="th-inner ">No</div>
												<div class="fht-cell"></div>
											</th>
											<th style="">
												<div class="th-inner ">Name</div>
												<div class="fht-cell"></div>
											</th>
											<th style="" class="text-right">
												<div class="th-inner ">Options</div>
												<div class="fht-cell"></div>
											</th>
										</tr>
									</thead>
				
									<tbody>
										<tr data-index="0">
											<td style="">1</td>
											<td style="">Example Category 5</td>
											<td style="" class="text-right">
												<a data-container="body" data-original-title="Edit" onclick="ajax_modal('edit','Edit Category','Successfully Edited!','category_edit','10')" data-toggle="tooltip" class="btn btn-success btn-xs btn-labeled fa fa-wrench">
													Edit                   
												</a>
												<a data-container="body" data-original-title="Delete" data-toggle="tooltip" class="btn btn-danger btn-xs btn-labeled fa fa-trash" onclick="delete_confirm('10','Really Want To Delete This?')">
													Delete                    
												</a>
											</td>
										</tr>
										<tr data-index="1">
											<td style="">2</td>
											<td style="">Example Category 4</td>
											<td style="" class="text-right">
												<a data-container="body" data-original-title="Edit" onclick="ajax_modal('edit','Edit Category','Successfully Edited!','category_edit','9')" data-toggle="tooltip" class="btn btn-success btn-xs btn-labeled fa fa-wrench">
															Edit                    </a>
												<a data-container="body" data-original-title="Delete" data-toggle="tooltip" class="btn btn-danger btn-xs btn-labeled fa fa-trash" onclick="delete_confirm('9','Really Want To Delete This?')">
														Delete                    </a>
											</td>
										</tr>
										<tr data-index="2">
											<td style="">3</td>
											<td style="">Example Category 3</td>
											<td style="" class="text-right">
												<a data-container="body" data-original-title="Edit" onclick="ajax_modal('edit','Edit Category','Successfully Edited!','category_edit','8')" data-toggle="tooltip" class="btn btn-success btn-xs btn-labeled fa fa-wrench">
													Edit                    
												</a>
												<a data-container="body" data-original-title="Delete" data-toggle="tooltip" class="btn btn-danger btn-xs btn-labeled fa fa-trash" onclick="delete_confirm('8','Really Want To Delete This?')">
													Delete                    
												</a>
											</td>
										</tr>
										<tr data-index="3">
											<td style="">4</td>
											<td style="">Example Category 2</td>
											<td style="" class="text-right">
												<a data-container="body" data-original-title="Edit" onclick="ajax_modal('edit','Edit Category','Successfully Edited!','category_edit','7')" data-toggle="tooltip" class="btn btn-success btn-xs btn-labeled fa fa-wrench">
													Edit                    
												</a>
												<a data-container="body" data-original-title="Delete" data-toggle="tooltip" class="btn btn-danger btn-xs btn-labeled fa fa-trash" onclick="delete_confirm('7','Really Want To Delete This?')">
													Delete                    
												</a>
											</td>
										</tr>
										<tr data-index="4">
											<td style="">5</td>
											<td style="">Digital Product</td>
											<td style="" class="text-right">
												<a data-container="body" data-original-title="Edit" onclick="ajax_modal('edit','Edit Category','Successfully Edited!','category_edit','6')" data-toggle="tooltip" class="btn btn-success btn-xs btn-labeled fa fa-wrench">
															Edit                    
												</a>
												<a data-container="body" data-original-title="Delete" data-toggle="tooltip" class="btn btn-danger btn-xs btn-labeled fa fa-trash" onclick="delete_confirm('6','Really Want To Delete This?')">
														Delete                    
												</a>
											</td>
										</tr>
										<tr data-index="5">
											<td style="">6</td>
											<td style="">Kids</td>
											<td style="" class="text-right">
												<a data-container="body" data-original-title="Edit" onclick="ajax_modal('edit','Edit Category','Successfully Edited!','category_edit','5')" data-toggle="tooltip" class="btn btn-success btn-xs btn-labeled fa fa-wrench">
													Edit                    
												</a>
												<a data-container="body" data-original-title="Delete" data-toggle="tooltip" class="btn btn-danger btn-xs btn-labeled fa fa-trash" onclick="delete_confirm('5','Really Want To Delete This?')">
													Delete                    
												</a>
											</td>
										</tr>
										<tr data-index="6">
											<td style="">7</td>
											<td style="">Men</td>
											<td style="" class="text-right">
												<a data-container="body" data-original-title="Edit" onclick="ajax_modal('edit','Edit Category','Successfully Edited!','category_edit','4')" data-toggle="tooltip" class="btn btn-success btn-xs btn-labeled fa fa-wrench">
													Edit                    
												</a>
												<a data-container="body" data-original-title="Delete" data-toggle="tooltip" class="btn btn-danger btn-xs btn-labeled fa fa-trash" onclick="delete_confirm('4','Really Want To Delete This?')">
													Delete                    
												</a>
											</td>
										</tr>
										<tr data-index="7">
											<td style="">8</td>
											<td style="">Women</td>
											<td style="" class="text-right">
												<a data-container="body" data-original-title="Edit" onclick="ajax_modal('edit','Edit Category','Successfully Edited!','category_edit','3')" data-toggle="tooltip" class="btn btn-success btn-xs btn-labeled fa fa-wrench">
													Edit                    
												</a>
												<a data-container="body" data-original-title="Delete" data-toggle="tooltip" class="btn btn-danger btn-xs btn-labeled fa fa-trash" onclick="delete_confirm('3','Really Want To Delete This?')">
													Delete                   
												</a>
											</td>
										</tr>
										<tr data-index="8">
											<td style="">9</td>
											<td style="">Automobile</td>
											<td style="" class="text-right">
												<a data-container="body" data-original-title="Edit" onclick="ajax_modal('edit','Edit Category','Successfully Edited!','category_edit','2')" data-toggle="tooltip" class="btn btn-success btn-xs btn-labeled fa fa-wrench">
													Edit                    
												</a>
												<a data-container="body" data-original-title="Delete" data-toggle="tooltip" class="btn btn-danger btn-xs btn-labeled fa fa-trash" onclick="delete_confirm('2','Really Want To Delete This?')">
													Delete                    
												</a>
											</td>
										</tr>
										<tr data-index="9">
											<td style="">10</td>
											<td style="">Electronics</td>
											<td style="" class="text-right">
												<a data-container="body" data-original-title="Edit" onclick="ajax_modal('edit','Edit Category','Successfully Edited!','category_edit','1')" data-toggle="tooltip" class="btn btn-success btn-xs btn-labeled fa fa-wrench">
														Edit                    
												</a>
												<a data-container="body" data-original-title="Delete" data-toggle="tooltip" class="btn btn-danger btn-xs btn-labeled fa fa-trash" onclick="delete_confirm('1','Really Want To Delete This?')">
													Delete                    
												</a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div><!---Contain--->	
		
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">
			  <!-- Modal content-->
				<?php //echo '<script>alert("'.$error.'");</script>';?>
				<?php echo form_open_multipart('account/all_template/category')?>
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Create New Category</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="usr">Icon:</label>
								<span class="btn btn-default btn-file">
									Browse <input type="file" name="userfile" id="userfile" size="20"/>
								</span>
							</div>
							<div class="form-group">
								<label for="pwd">Category Name:</label>
								<input type="text" class="form-control" id="title" name="category">
							</div>
						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-default" value="Save" name="btn_insert" id="submit"/>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script src="<?php echo base_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
</html>