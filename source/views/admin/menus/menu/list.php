<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $view_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->

						<!-- portlet-body -->
						<div class="portlet-body">
								<?php 
					                if(isset($errors)){
					                   echo show_msg($errors);
					                }
					            ?>
							 <div class="row">

								<div class="col-md-4 col-sm-4">
									<!-- accordion -->
									<div class="panel-group accordion" id="accordion3">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#pages">
												<?php word('pages'); ?> </a>
												</h4>
											</div>
											<div id="pages" class="panel-collapse <?php echo isset($errors) == ''? 'in':'collapse' ?>">
												<div class="panel-body" style="height:200px; overflow:auto;">

													<?php 
														echo form_open(base_admin_url().$controller_url.'create', array('role' => 'form') );
														echo form_hidden('type', 'page');
														echo show_page_check_box();	
													?>
												</div>

												<div class="panel-body">
													
													<?php 
														echo form_label($check_ml_pages.word_r('select_all')); 
														echo form_submit('create', word_r('add_to_menu'), "class = 'btn blue btn-xs pull-right' ");
													 	echo form_close();
													 ?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#links" aria-expanded="false">
												<?php word('links'); ?> </a>
												</h4>
											</div>
											<div id="links" class="panel-collapse  <?php echo isset($errors) != ''? 'collapse in':'collapse' ?>" >
												<div class="panel-body" style="height:200px; overflow-y:auto;">
												
											
												<?php echo form_open(account_url().$controller_url.'create', array('role' => 'form') ); ?>
													<?php echo form_hidden('type', 'link'); ?>
													<div class="form-group required <?php has_error('label'); ?>">

															<?php echo form_label(word_r('label'),'label',array('class' => 'control-label') ); ?>
															<?php echo form_input('label', $this->input->post('label', true),"class = 'form-control input-sm', id= 'label'"); ?>
													</div>
													<div class="form-group required <?php has_error('url'); ?>">
															<?php echo form_label(word_r('url'),'url',array('class' => 'control-label') ); ?>
															<?php echo form_input('url', $this->input->post('url', true),"class = 'form-control input-sm', id= 'url'"); ?>
													</div>
													<?php 
													echo form_submit('create', word_r('add_to_menu'), "class = 'btn blue btn-xs pull-right' ");
													echo form_close();
													 ?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#categories" aria-expanded="false">
												<?php word('categories'); ?> </a>
												</h4>
											</div>
											<div id="categories" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
												<div class="panel-body" style="height:200px; overflow:auto;">
													<?php 
														echo form_open(account_url().$controller_url.'create', array('role' => 'form') );
														echo form_hidden('type', 'categories');
														echo show_category_check_box(false,sys_config('category_group_id'));
													 	
													?>
												</div>

												<div class="panel-body">
													<?php 
														echo form_label($check_ml_categories.word_r('select_all')); 

													 	echo form_submit('create', word_r('add_to_menu'), "class = 'btn blue btn-xs pull-right' ");
													 	echo form_close();
													 ?>
												</div>	
											</div>
										</div>
									
									</div>
									<!-- accordion -->
								</div>
								
								<div class="col-md-8 col-sm-8">
									<!-- nestable -->
										<?php echo show_menu_sort(sys_config('menu_group_id')); ?>
						             <!-- nestable -->
								</div>
							</div>
						</div>
						<!-- end portlet-body -->
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
				<script type="text/javascript">
						
					$(document).ready(function() {
							 var updateOutput = function(e)
                              {
                                  var list   = e.length ? e : $(e.target),
                                      output = list.data('output');
                                  if (window.JSON) {
                                      output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                                      menu_updatesort(window.JSON.stringify(list.nestable('serialize')));
                                  } else {
                                      output.val('JSON browser support required for this demo.');
                                  }
                              };
                            
                            // activate Nestable for list menu
                            $('#nestableMenu').nestable({
                                group: 1
                            })
                            .on('change', updateOutput);
                            // output initial serialised data
                            updateOutput($('#nestableMenu').data('output', $('#nestableMenu-output')));
					
					});

					 function lagXHRobjekt() {
							var XHRobjekt = null;
							
							try {
								ajaxRequest = new XMLHttpRequest(); // Firefox, Opera, ...
							} catch(err1) {
								try {
									ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP"); // Noen IE v.
								} catch(err2) {
									try {
											ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP"); // Noen IE v.
									} catch(err3) {
										ajaxRequest = false;
									}
								}
							}
							return ajaxRequest;
						}
						
						
						function menu_updatesort(jsonstring) {
							mittXHRobjekt = lagXHRobjekt(); 
						
							if (mittXHRobjekt) {
								mittXHRobjekt.onreadystatechange = function() { 
									if(ajaxRequest.readyState == 4){
										// var ajaxDisplay = document.getElementById('sortDBfeedback');
										// ajaxDisplay.innerHTML = ajaxRequest.responseText;
									} else {
										// Uncomment this an refer it to a image if you want the loading gif
										//document.getElementById('sortDBfeedback').innerHTML = "<img style='height:11px;' src='images/ajax-loader.gif' alt='ajax-loader' />";
									}
								}
								
								ajaxRequest.open("GET", "<?php echo base_admin_url().$controller_url; ?>"+"save?jsonstring=" + jsonstring + "&rand=" + Math.random()*9999, true);
								ajaxRequest.send(null); 
							}
						}
						
                   	
					
                </script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	