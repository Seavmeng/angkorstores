<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php //echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> Add New Brand</span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php //echo btn_actions('admin/product_list/create','account/product_list/create', $create_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open_multipart(base_url().'admin/brands/update/'.$this->uri->segment(4), array('role' => 'form') ); ?>
					
						
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
							         
							         <!-- Row -->
							         	<div class="row">
							         		 <!-- Form -->
							         	 <div class="col-md-12">
							         		<div class="col-md-8">	
							         			<!--Form-body -->
													<div class="form-body">
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																<label>Brand Name</label>
																<input type="text" name="bname" class="form-control" value="<?php echo $update_br['brand_name']; ?>"  required>
																<font style="color:red;"><?php echo form_error('bname'); ?></font>
															</div>
															<!--End col-md-6 -->
														</div><br/>
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																<label>Description</label>
																<textarea name="description" class="form-control mce"><?php echo $update_br['description']; ?></textarea>
															</div>
															<!--End col-md-6 -->
														</div>
													</div>
											</div>
							         		<div class="col-md-4">							         			
							         			<div class="panel panel-default">
													<div class="widget_section">
														<div class="ui-radio ui-radio-primary">
															<div class="panel-heading">
																<div class="widget_title"><h3 class="panel-title">Public</h3></div>
															</div>
															<div class="panel-body">
																
																<div>
																	<input type="submit" name="btn_update" value="<?php echo 'update'?>" class="btn btn-primary">
																	
																	<a href="<?php echo base_url().'admin/brands/brand';?>"><input type="button" name="btn_update" value="Cancel" class="btn btn-danger"></a>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- Feature -->
												<div class="panel panel-default">
													<div class="widget_section">
														<div class="panel-heading">
															<div class="widget_title"><h3 class="panel-title">Feature image</h3></div>
														</div>
														<div class="panel-body" >
															<div class="form-group" style="height:100%; margin:0;">
																<?php 
																	$dir = date('m-Y', strtotime($update_br['created_date']));
																?>
																<div class="fileinput fileinput-<?php echo $update_br['image'] == ''? 'new':'exists';?>" data-provides="fileinput">
																	<div class="fileinput-new thumbnail" style="width: 250px; height: 160px;">
																		<img data-src="holder.js/100%x100%" alt="">
																	</div>
																	<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 250px; max-height: 200px;">
																		
																		<?php if(!empty($update_br['image'])): ?>
																		<img src="<?php echo base_url().'images/products/'.$update_br['image']; ?>" data-src="holder.js/100%x100%" alt="">
																		<?php endif; ?>
																		<input type="hidden" name="feature" value="<?php echo $update_br['image']; ?>" id="feature">
																	</div>

																	<div>
																		<span class="btn btn-default btn-file">
																			<span class="fileinput-new">Select image</span>
																			<span class="fileinput-exists">Change</span>
																			<input type="file" name="userfile[]">
																		</span>
																		<a href="#" class="btn btn-default fileinput-exists remove" data-dismiss="fileinput" >Remove</a>
																	</div>
																</div>
														
															</div>
														</div>
													</div>
												</div>
												
							         		</div>
							         	 </div>
							         		<!-- End Action -->
							         </div>
							          <!-- End Row -->
							         




									</div>
								<!-- end portlet-body -->
							
						<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->
<!-- <script>
    var base_admin_assets_url = "<?php echo base_admin_assets_url(); ?>";
</script>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
  //tag
    $('.tags-input').tagsInput({
        width: 'auto',
        //autocomplete_url:'test/fake_plaintext_endpoint.html' //jquery.autocomplete (not jquery ui)
        autocomplete_url:"<?php echo base_admin_url().$controller_url.'save'; ?>" // jquery ui autocomplete requires a json endpoint
      });
</script>
<script type="text/javascript">
	$(document).ready(function() {		
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			startDate: '-3d'
		});
		$('.timepicker').timepicker({
			showMeridian: false,
			format: 'HH:mm',
			showSeconds: true,
			minuteStep: 1,
			secondStep: 1
		});
		
		$("#have_disc").click(function(){
			$(".show_have_dis").toggle(400);
		});
		$('#select').on('click',function(){
			if ($(this).is(':checked')) {
				$('.categories').each(function(){
					this.checked = true;
				});
			}else{
				$('.categories').each(function(){
					this.checked = false;
				});
			}
		});
		$("#fileupload").change(function () {
			if (typeof (FileReader) != "undefined") {
				var dvPreview = $("#dvPreview");
				dvPreview.html("");
				var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
				$($(this)[0].files).each(function () {
					var file = $(this);
					if (regex.test(file[0].name.toLowerCase())) {
						var reader = new FileReader();
						reader.onload = function (e) {
							var img = $("<img />");
							img.attr("style", "height:100px;width: 100px");
							img.attr("src", e.target.result);
							dvPreview.append(img);
						}
						reader.readAsDataURL(file[0]);
					} else {
						alert(file[0].name + " is not a valid image file.");
						dvPreview.html("");
						return false;
					}
				});
			} else {
				alert("This browser does not support HTML5 FileReader.");
			}
		});
		
		//################### Get value from select ###################//
		var maxappend = 0;
		$('.add_atr').click(function() {
			//alert('he');
			var id = $('#attr_name').val();
			//alert(id);
			if(id == 9){
				$.ajax({
					type:'POST',
					data:{id:id},
					url:'<?php echo site_url('/admin/product_list/attributes')?>',
					success: function(e){
						$('.parent').append(e);
					}
				});
			}
			if(id == 8){
				$.ajax({
					type:'POST',
					data:{id:id},
					url:'<?php echo site_url('/account/product_list/attributes')?>',
					success: function(e){
						$('.parents').append(e);
					}
				});
			}
		});
		

		// attach button click listener on dom ready
		$('.save').click(function(){
			var group = $('#attr_name').val();
			var color = $('input[name^=userfile]').map(function(idx, elem) {
				return $(elem).val();
			}).get();
			var price = $('input[name^=p_color]').map(function(id, eleme) {
				return $(eleme).val();
			}).get();
			var size = $('input[name^=size]').map(function(ids, elems) {
				return $(elems).val();
			}).get();
			
			if(group == 9){
				$('input[name^=str_size]').val(group + ',' + size)
			}
			if(group == 8){
				$('input[name^=str_color]').val(group + ',' + color + '/' + price)
			}

			//alert(group + ',' + color + ',' +price);
			event.preventDefault();
		});
		
		$('#attr_name').change(function(){
			$('.save').show(200);
			//$('#show_attr div').empty();
		});
		
		//####################### Product tags #######################//
		$('.add_tags').click(function(){
			var tag_id = $("#get_tags").val();
			if(tag_id ==""){
				return false;
			}else{
				var list='<li><a class="button_grey" href="#">'+tag_id+'</a>'+
						'<i class="tag_re glyphicon glyphicon-remove-circle"></i>'+
						'<input type="hidden" name="tags_con[]" value="'+tag_id+'"></li>';
				if(maxappend >=10) return;
				$(".tags_container ul").append(list);
				maxappend++;
			}
		});
		$(document).on('click','.tag_re', function(){
			$(this).parent('.tags_container ul li').remove();
		});
		$(document).on('click','.att_re', function(){
			$(this).closest('.att_on').remove();
			$(this).closest('.size_on').remove();
			//alert('ge');
		});
	});
	//CKEDITOR.replace('ckeditor');
</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
 -->