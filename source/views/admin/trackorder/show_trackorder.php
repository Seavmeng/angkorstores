<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<!-- <i class="<?php echo $icon; ?>"></i> -->
								<!-- <span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span> -->
							</div>
							<!-- end caption -->

							<!-- actions -->
							<!-- <div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $create_action); ?>
							</div> -->
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open('admin/trackorder/trackorder/create', array('role' => 'form')); ?>
					
						
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
							         <!--Form-body -->
										<div class="form-body">
										<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														
														<label>Order</label>
														<select name="id" class="selectpicker form-control" data-style="btn-primary" >
																<?php foreach ($get_ornumber as $order) { ?>
																 	<option value="<?php echo $order['order_number']; ?>"><?php echo orderID($order['order_number']); ?></option>
																<?php } ?>	
																																      
														</select>
												</div>

												<!--End col-md-6 -->

											</div>
											<!-- End row -->


											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6"><br/>
														<div class="form-group">
															<label for="titles">Title</label>
															<input type="text" name="titles" class="form-control" required />
															
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->

											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<label for="description">Description</label>
														<textarea class="form-control" rows="10" id="description" name="description"></textarea>
																
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->

										</div>

										<div class="row"><br/>
											<div class="col-md-12">
											
														
														<div class="clearfix">
																		<input type="submit" name="btn_save" value="create" class="btn btn-primary">
																		
																		<a href="<?php echo base_url().'admin/trackorder/trackorder/create';?>" required><input type="button" name="btn_update" value="Cancel" class="btn btn-danger"></a>
														</div>
												
											</div>
										</div>
									</div>							
						<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	