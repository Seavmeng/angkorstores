<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<!-- <i class="<?php echo $icon; ?>"></i> -->
								<!-- <span class="caption-subject bold uppercase"> <?php //echo $htitle; ?></span> -->
							</div>
							<!-- end caption -->

							<!-- actions -->
							<!-- <div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $create_action); ?>
							</div> -->
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open('admin/trackorder/trackorder/create', array('role' => 'form')); ?>
					
						
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
							         <!--Form-body -->
										<table data-show-export="true" data-search="true" data-show-refresh="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-pagination="true" data-side-pagination="server" data-url="#" class="table table-striped table-bordered table-hover" id="events-table">
								
											<thead>
											
												<tr>
													<th style="text-align: center;">N</th>
													<th style="text-align: center;">Order</th>
													<th style="text-align: center; ">Title</th>
													<th style="text-align: center; ">Description</th>
													<th style="text-align: center; ">Date</th>
													<th style="text-align: center; ">Status</th>
													<th style="text-align: center; ">Options</th>
												</tr>
											</thead>
											
											<tbody>
												<?php
													//var_dump($billing);
													$i=0;
													foreach($data as $rows){
														//var_dump($rows);exit();
														$track_id = $rows['track_order_id'];
														$order_id = $rows['order_id'];
														$title = $rows['title'];
														$description = $rows['description'];
														$created_date = $rows['created_date'];
														$i++;
												?>
												<tr>
													<td style="text-align: left; "><?php echo $i;?></td>
													<td style="text-align: center; "><?php echo orderID($order_id);?></td>
													<td style="text-align: center; "><?php echo $title;?></td>
													<td style="text-align: center; "><?php echo $description;?></td>
													<td style="text-align: left; "><?php echo $created_date;?></td>
													<td style="text-align: left; ">On delivery</td>
													<td style="text-align: center; ">  
														<a class="btn btn-default btn-labeled fa fa-eye pull-right mar-rgt category" data-toggle="modal"  href="<?php echo  base_url().'admin/trackorder/trackorder/view/'.$track_id;?>">
															Detail                              
														</a>
														<a href="<?php echo base_admin_url().'trackorder/trackorder/delete/'.$track_id;?>" onclick="return confirm('Are you sure want to delete ?')" class="btn default btn-xs red" title="Delete"><i class="fa fa-trash-o"></i> Delete </a>
													</td>
												
												</tr>
												<?php
													}
												?>
											</tbody>
										</table>
									</div>							
						<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	