<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
 <div class="row">
		<div class="col-md-12"> 
			<div>
				<h3>Customer Reports</h3>
			</div>
			<div class="clearfix"></div>
			<br/>
			<div class="portlet light bordered ">
					<?php echo form_open(base_url()."admin/settings/customers/search_customer",array('method'=>'get'))?>
					<div class="row">
						<div class="col-md-2">
							<label for='status' class='label-control'>Filter</label> 
							<select name="status" class="form-control" >
								<option value="all" <?php echo ($this->input->get("status")=="all")?"selected":""?> >All</option>
								<option value="active"  <?php echo ($this->input->get("status")=="active")?"selected":""?> >Success</option>
								<option value="inactive" <?php echo ($this->input->get("status")=="inactive")?"selected":""?>>Unsuccess</option>
							</select>
						</div> 
						<div class="col-md-3">
							<label for='firstname' class='label-control'>User Name</label>
							<input type="text" name="username" placeholder="First Name | Last Name | User" value="<?php echo $this->input->get("username");  ?>" class="form-control"/>
						</div> 
						<div class="col-md-3">
							<label for='email' class='label-control' >Email</label>
							<input type="text"  value="<?php echo $this->input->get("email");  ?>" name="email" class="form-control"/>
						</div>
						<div class="col-md-2">
							<label for='phone' class='label-control'>Phone</label>
							<input type="text"  value="<?php echo $this->input->get("phone");  ?>" name="phone" class="form-control "/>  
						</div> 
						<div class="col-md-2">
							<label><br/></label><br/>
							<button type="submit" class="btn btn-primary btn-md ">Search</button>
						</div> 
					</div>	 
				<?php echo form_close();?>
				<div class="clearfix"></div>
				<br/>
			   <div class=""> 
					<?php
								echo form_open(base_admin_url().$controller_url.'ml', array('role' => 'form')); 
								$this->table->set_heading("Order Number",
														"First Name",
														"Last Name",
														"Email",
														"Phone",
														"User",
														"Biller",
														"Transaction Date",
														"Amount($)",
														"Paid by",
														"Status"
											);  
								foreach($data as $value){ 
									$order_number = $value['order_number'];
									$first_name = $value['first_name'];
									$last_name = $value['last_name'];
									$email = $value['email'];
									$phone = $value['phone'];
									$user_id = $value['username'];
									$bill_id = $value['bill_id'];
									$created = $value['created'];  
									$amount = $value['prices'];  
									$payment_method = $value['payment_method'];  
									$status = $value['status'] == 1 ? "Success" : "";  
									$this->table->add_row(
															"$order_number",
															"$first_name",
															"$last_name",
															"$email",
															"$phone", 
															"$user_id",
															"$bill_id",
															"$created",
															"$amount",
															"$payment_method",
															"$status"
														);
								}
								echo $this->table->generate();
							 ?>
							 <div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="col-sm-12">
										<div class="col-sm-7 dataTables_info" id="sample_1_info" role="status" aria-live="polite">
											<?php echo $showing; ?>
										</div> 
										<div class="option col-sm-5"> 
										</div>
									</div>
								</div>
								<?php echo form_close(); ?>
								<div class="col-md-6 col-sm-12">
									 <div class="dataTables_paginate paging_bootstrap_full_number">
									 	<?php echo $page; ?>
									 </div>
						             
								</div>
							</div>
						</div> 
				</div>
				<div class="clearfix"></div>
            </div>
		</div>
</div>
<!-- END ROW -->
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
<script>
	$(document).ready(function(){
		$(".sort-table").tablesorter({
			 headers: { 			                   
						3: { 
					//		sorter: false 
						},
				}
		});  
	});
</script>
	