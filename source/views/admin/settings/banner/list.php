<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
 <div class="row">
 
		<?php 
		$msg = $this->session->flashdata('msg'); 
		if(isset($msg)){
		?> 
				<div class="col-md-12">
					<div class="alert alert-success">
						<?php echo $msg;?>
					</div>
				</div>
		<?php }  ?>   
		 
		<div class="col-md-12"> 
			<div class="portlet light bordered"> 
						<div class="portlet-title"> 
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>  
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $view_action); ?>
							</div> 
						</div>  
						<div class="portlet-body"> 
							<div class="row hidden">
								<div class="col-md-12 col-sm-12">
									<?php echo form_open(base_admin_url().$controller_url.'search', array('role' => 'form','method'=>'get')); ?>
										<div class="form-group col-md-3">
												<?php echo form_label(word_r('word'), 'word',array('class' => 'control-label')); ?>
												<?php echo form_input(array('name' => 'word', 'class' => 'form-control', 'id' => 'word',  'value' => $this->input->get('word') ) ); ?>											
										</div>
										<div class="form-group col-md-3">
												<?php echo form_label(word_r('english'), 'english',array('class' => 'control-label')); ?>
												<?php echo form_input(array('name' => 'english', 'class' => 'form-control', 'id' => 'english',  'value' => $this->input->get('english') ) ); ?>											
										</div>
										<div class="form-group col-md-3">
												<?php echo form_label(word_r('khmer'), 'khmer',array('class' => 'control-label')); ?>
												<?php echo form_input(array('name' => 'khmer', 'class' => 'form-control', 'id' => 'khmer',  'value' => $this->input->get('khmer') ) ); ?>																
										</div>
										<div class="form-group col-md-2">
												<label for="" class="control-label">&nbsp;</label>
												<input type="submit" class="btn btn blue form-control" value="<?php word('search'); ?>">											
										</div>
										
									<?php echo form_close(); ?> 
								</div>
							</div>
							

							<?php
								echo form_open(base_admin_url().$controller_url.'ml', array('role' => 'form')); 

								$this->table->set_heading(word_r('id'),
															word_r('image'),
															word_r('name'),
															word_r('position'),
															word_r('show_on'),
															word_r('advertising_date'),
															word_r('expiring_date'),
 															word_r('status'),
															word_r('action')
											); 
								foreach($data as $value){
									 

									$id = $value['id'];
									$image = "<img style='width:50px;' src='".base_url().'images/banner/'.$value['images']."'/>";
									$name = $value['name'];
									$position = $value['position'];
									$show_on = $value['show_on'];
									$advertising_date = $value['advertising_date'];
									$expiring_date = $value['expiring_date']; 
									$status = $value['status']; 
									$current_date = date('Y-m-d H:i:s');
									$exp = ($expiring_date > $current_date?1:0);
									
									$this->table->add_row(
															"$id",
															"$image",
															"$name",
															"$position", 
															"$show_on",
															"$advertising_date",
															"$expiring_date", 
															check_public($status,$exp),
															btn_action($controller_url.'update/'.$id, $controller_url.'delete/'.$id)
														);
								}
								echo $this->table->generate();
							 ?>
							 <div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="col-sm-12">
										<div class="col-sm-7 dataTables_info" id="sample_1_info" role="status" aria-live="polite">
											<?php echo $showing; ?>
										</div> 
										<div class="option col-sm-5">
												 
										</div>
										
									</div>
								</div>
								<?php echo form_close(); ?>
								<div class="col-md-6 col-sm-12">
									 <div class="dataTables_paginate paging_bootstrap_full_number">
									 	<?php echo $page; ?>
									 </div>
						             
								</div>
							</div>
						</div> 
			</div> 
		</div> 
</div> 

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	<script>
			$(document).ready(function(){
				$(".sort-table").tablesorter({
					 headers: { 			                   
			                    3: { 
			                        sorter: false 
			                    },
			            }
				});  
			});
		</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	