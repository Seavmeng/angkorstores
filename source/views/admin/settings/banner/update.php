<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<?php 
$position = array(
		array("value"=>"Fly Side Left","name"=>"Fly Side Left (300 x 700)"),
		array("value"=>"Fly Side Right","name"=>"Fly Side Right (300 x 700)"),
		array("value"=>"Position 1","name"=>"Position 1 (300 x 250) or (700 x 300)"),
		array("value"=>"Position 2","name"=>"Position 2 (300 x 250) or (700 x 300)"),
		array("value"=>"Position 3","name"=>"Position 3 (300 x 250) or (700 x 300)"), 
		array("value"=>"Position 4","name"=>"Position 4 (300 x 250) or (700 x 300)"), 
		array("value"=>"Position 5","name"=>"Position 5 (300 x 250) or (700 x 300)") 
);   
$show_on = array(
		array("id"=>1,"name"=>"Home"),
		array("id"=>2,"name"=>"Category"),
		array("id"=>3,"name"=>"Product Details"),
);  
?>
 <div class="row"> 
		<div class="col-md-12">
			<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase">Update Banner</span>
							</div> 
						</div>
						<?php echo form_open_multipart(base_url().'admin/settings/banner/update/'.$id, array('role' => 'form', '') ); ?>
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
										<div class="form-body">
											<div class="col-md-12">
												<div class="col-md-6">
															<div class="col-md-12">
																<div class="form-group required ">
																	<?php echo form_label(word_r('name'),'name',array('class' => 'control-label') ); ?>
																	<?php echo form_input('name',$result->name," class = 'form-control', id= 'name'"); ?>
																</div>
															</div>  
															<div class="col-md-12">
																<div class="form-group required  ">
																	<?php echo form_label(word_r('position'),'position',array('class' => 'control-label') ); ?>
																	<select name='position' class="form-control"> 
																		<option value=""></option>	 
																		<?php foreach($position as $row) {?>
																			<option value="<?php echo $row['value'] ?>"   <?php  echo  $result->position==$row['value']?'selected':''   ?>><?php echo $row['name'] ?></option>	
																		<?php } ?>
																	</select> 
																</div>
															</div> 
															<div class="col-md-12">
																<div class="form-group required">
																	<?php echo form_label(word_r('show_on'),'show_on',array('class' => 'control-label') ); ?>
																	 
																	<select name='show_on' class="form-control">  
																		<option value=""></option>	
																		<?php foreach($show_on as $row) {?> 
																			<option value="<?php echo $row['name'] ?>"  <?php  echo  $result->show_on==$row['name']?'selected':''   ?>><?php echo $row['name'] ?></option>	
																		<?php } ?>
																	</select> 
																</div>
															</div>
															
															<div class="form-group required ">
																<div class="col-sm-12"> 
																<?php echo form_label(word_r('advertising_date'),'advertising_date',array('class' => 'control-label') ); ?>
																</div> 
																<div class="col-sm-6">
																	<div class="clearfix">
																		<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																			<?php 
																					$datetimeAds = new DateTime($result->advertising_date);
																					$dateAds = $datetimeAds->format('Y-m-d');
																					$timeAds = $datetimeAds->format('H:i:s');
																			?>
																			<input name="ads_ddate" value="<?php echo $dateAds ;?> " class="form-control datepicker" type="text"> 
																			<span class="input-group-addon datepicker-icon">
																				<span class="glyphicon glyphicon-calendar"></span>
																			</span>
																			<font color="red"><font color="red"></font></font>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="clearfix">
																		<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
																			<input class="form-control timepicker" name="ads_dtime" value="<?php echo $timeAds ;?>" type="text">
																			<span class="input-group-addon timepicker-icon">
																				<span class="glyphicon glyphicon-time"></span>
																			</span>
																			<font color="red"><font color="red"></font></font>
																		</div>
																	</div>
																</div>
														   </div> 
															<div class="form-group required ">
																<div class="col-sm-12">
																<br/>
																<?php echo form_label(word_r('expiring_date'),'expiring_date',array('class' => 'control-label') ); ?>
																</div> 
																<div class="col-sm-6">
																		<div class="clearfix">
																			<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																			 <?php 
																					$datetimeEx = new DateTime($result->expiring_date);
																					$dateEx = $datetimeEx->format('Y-m-d');
																					$timeEx = $datetimeEx->format('H:i:s');
																			?>
																				<input name="ex_ddate" value="<?php echo $dateEx;?>" class="form-control datepicker" type="text"> 
																				<span class="input-group-addon datepicker-icon">
																					<span class="glyphicon glyphicon-calendar"></span>
																				</span>
																				<font color="red"><font color="red"></font></font>
																			</div>
																		</div>
																</div>
																<div class="col-sm-6">
																	<div class="clearfix">
																		<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
																			<input class="form-control timepicker" name="ex_dtime" value="<?php echo  $timeEx;?>" type="text">
																			<span class="input-group-addon timepicker-icon">
																				<span class="glyphicon glyphicon-time"></span>
																			</span>
																			<font color="red"><font color="red"></font></font>
																		</div>
																	</div>
																</div> 
														   </div> 
															 
															<div class="col-md-12">			
																<br/>
																 <div class="panel panel-default">
																	<div class="widget_section">
																		<div class="panel-heading">
																			<div class="widget_title"><h3 class="panel-title">Image</h3></div>
																		</div>
																		<div class="panel-body" >
																			<div class="fileinput fileinput-new" data-provides="fileinput">
																			  <div class="fileinput-new thumbnail" style="width: 263px; height: 160px;">
																				
																				<img name="img_img" src="<?php echo base_url().'images/banner/'.$result->images?>" data-src="holder.js/100%x100%" alt="">
																			  </div>
																			  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 263px; max-height: 200px;"></div>
																			  <div>
																				<span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
																					
																					<input type="file" name="image_banner" accept=".gif,.jpg,.png" data-maxfile="1024" />
																					
																				</span> 
																				
																				<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
																			  </div>
																			</div>
																		</div>
																	
																	</div>
																</div>  
															</div> 
															<div class="col-md-12">
																<div class="form-group required  ">
																	<?php echo form_label("Link",'name',array('class' => 'control-label') ); ?>
																	<?php echo form_input('link', $result->link,"class = 'form-control' placeholder='http://www.example.com' id= 'name'"); ?>
																</div>
															</div> 
															
												</div>
												<div class="col-md-6"> 
													<div class="panel panel-default">
														<div class="widget_section">
															<div class="ui-radio ui-radio-primary">
																<div class="panel-heading">
																	<div class="widget_title"><h3 class="panel-title"><span><input type="checkbox" name='status'  value="1" <?php echo $result->status==1?"checked":""?>/></span>Public</h3></div>
																</div>
																<div class="panel-body">
																	<div>
																			
																		<div class="clearfix">
																			<input type="submit" name="btn_save" value="<?php echo 'Save'?>" class="btn btn-primary"> 
																			<?php echo '<a href="'.base_admin_url().$controller_url.'settings/banner" class="btn default">'.word_r('cancel').'</a>'; ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												
												</div>
											</div> 
										 
											<div class="row">
												 
											</div>
									</div> 
						<?php echo form_close(); ?> 
			</div>
		</div>
</div>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?> 
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>

<script type="text/javascript">
	$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			startDate: '1d'
		});
	$('.timepicker').timepicker({
		showMeridian: false,
		format: 'HH:mm',
		showSeconds: true,
		minuteStep: 1,
		secondStep: 1
	});
</script>

