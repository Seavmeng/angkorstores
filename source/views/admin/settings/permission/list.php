<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php //echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php //echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $view_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<!-- msg -->
						<?php
							if($this->session->flashdata('message'))
							{
								?>
									<div class="alert alert-success">
										<?php echo $this->session->flashdata('message')?>
									</div>
								<?php
							}
						?>

						<!-- portlet-body -->
						<div class="portlet-body">

							<div class="row">
								
									<table class="table table-striped table-bordered table-hover">
										<tr>
											<th>Group_id</th>
											<th>Menu_id</th>
											<th>Create</th>
											<th>Update</th>
											<th>Delete</th>
											<th>Search</th>
											<th>Ajax</th>
											<th>Ml</th>
											<th>Permission</th>
											<th  style="text-align: center;">Action</th>
										</tr>
									<?php
									foreach($data as $row){
									?>
										<tr>
											<td><?php echo $row['user_group_name'];?></td>
											<td><?php echo $row['menu_name'];?></td>
											<td><?php echo $row['create'];?></td>
											<td><?php echo $row['update'];?></td>
											<td><?php echo $row['delete'];?></td>
											<td><?php echo $row['search'];?></td>
											<td><?php echo $row['ajax'];?></td>
											<td><?php echo $row['ml'];?></td>
											<td><?php echo $row['permission'];?></td>
											<td style="text-align: center; ">  
												<?php echo btn_action('settings/permission/update/'.$row['permission_id'],'settings/permission/delete/'.$row['permission_id']);?>
											</td>
											
										</tr>
									<?php
									}
									?>
									</table>
							
							</div>
							
						</div>
						<!-- end portlet-body -->
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	<script>
			$(document).ready(function(){
				$(".sort-table").tablesorter({
					 headers: { 			                   
			                    3: { 
			                        sorter: false 
			                    },
			            }
				}); 
			});
		</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	