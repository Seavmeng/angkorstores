<?php $this->load->view(config_item('include_dir').'head');?>
<body id="app" class="app off-canvas">
	<!-- header -->
	<?php $this->load->view(config_item('include_dir').'top_navbar'); ?>
	<?php //$this->load->view(config_item('include_dir').'header'); ?>
	<!-- #end header -->
	<!-- main-container -->
	<div class="main-container clearfix">
		<!-- main-navigation -->
		<?php $this->load->view('includes/sidebar'); ?>
		<!-- #end main-navigation -->
		<!-- content-here -->
		<div class="content-container" id="content">
			<!-- dashboard page -->
			<div class="page page-dashboard">
				<div class="page-wrap">
					<div class="row">
						<div class="col-md-12">
						<?php $this->load->view(config_item('include_dir').'header'); ?>
						</div>
					</div>
					<!-- row -->
					<div class="row">
						<?php 
							if(isset($errors)){
							   echo msg($errors);
							}
							if(isset($success)){
								echo msg_suc($success);
							}
						echo form_open_multipart(account_url()."export", array('role' => 'form','class' => 'form-horizontal'))
						?>
						<div class="col-md-12">
							<div class="panel panel-default panel-hovered panel-stacked">
							<div class="widget_title">Setting</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-3">
											<?php
											echo '
											<div class="profile_section"><a href="'.account_url().'settings/config" class="fa fa-bullseye fa-1_5"> General</a></div>
											<div class="profile_section"><a href="#" class="fa fa-shield fa-1_5"> Security</a></div>
											<div class="profile_section"><a href="#" class="fa fa-ban fa-1_5"> Privacy</a></div>
											<div class="profile_section"><a href="#" class="fa fa-language fa-1_5"> Language</a></div>
											<div class="profile_section"><a href="#" class="fa fa-globe fa-1_5"> Notifications</a></div>
											<div class="profile_section"><a href="'.account_url().'setting/social" class="fa fa-facebook-square fa-1_5"> Social Network</a></div>
											';
											$user_id=get_user_id();
											$data=social_by_user_id($user_id);
											?>
										</div>
										<div class="col-md-9 border_left">
											<div class="page_title">Export CSV/Excel file</div>
											<div class="row">
												<div class="col-sm-10">
														<div class="form-group">
															<label class="col-sm-3 control-label required">All Bussuness Type</label>
															<div class="col-sm-9">
																<input type="checkbox" name="all_bus" value="all_bus"/>
																<font color="red"><?php echo form_error('all_bus'); ?></font>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label required">Business Type</label>
															<div class="col-sm-9">
																<?php echo BusinessType($this->input->post('businessType', true))?>
																<font color="red"><?php echo form_error('businessType'); ?></font>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label required">Country/Region</label>
															<div class="col-sm-9">
																<?php echo country(34)?>
																<font color="red"><?php echo form_error('country'); ?></font>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label required">City/State</label>
															<div class="col-sm-9">
																<?php echo city(34)?>
																<font color="red"><?php echo form_error('country'); ?></font>
															</div>
														</div>
														<div class="form form-actions">
															<label class="col-sm-3 control-label"></label>
																<?php echo form_submit('upload', word_r('export'), "class = 'btn btn-info' "); ?>
														</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>	
						</div>
						<?php echo form_close(); ?>
					</div> <!-- #end row -->
				</div> <!-- #end page-wrap -->
			</div>
			<!-- #end dashboard page -->
			<?php $this->load->view(config_item('include_dir').'footer-section'); ?>
		</div>
	</div> <!-- #end main-container -->
	<!-- theme settings -->
	<?php $this->load->view(config_item('include_dir').'theme_setting'); ?>
	<!-- #end theme settings -->
<!-- Dev only -->

<!-- Vendors -->
<?php $this->load->view(config_item('include_dir').'footer'); ?>
</body>
</html>