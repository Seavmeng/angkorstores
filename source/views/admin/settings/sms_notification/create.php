<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>

<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>

<div class="row">
		
		<div class="col-md-12">
		<div class="portlet light bordered">
		
			<div class="portlet-title">
				<div class="caption">
					<i class="<?php echo $icon; ?>"></i>
					<span class="caption-subject bold uppercase">SMS Notification</span>
				</div>
			</div>
			
			<div class="portlet-body">
				<div class="col-md-12"> 
						<div class="col-md-8"> 
							<form method="POST" action="<?= site_url("admin/settings/sms_notification"); ?>"/>
								<div class="row"> 
									<?php 
									$msg = $this->session->flashdata('msg'); 
									if(isset($msg)){?>
										<div class="alert alert-success">
											<strong>Success!</strong> <?php echo $msg;?>
										</div>
									<?php } ?> 
							
									<div class="col-md-12">
										<div class="form-group">
											<label for="name" class="control-label">ISMS Enable</label><br/>
											<input type="checkbox" value="<?php echo set_value('isms', 1); ?>" <?= ($result->isms > 0 ? "checked": ""); ?> name="isms" class="form-control input-sm"  />
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group required">
											<label for="name" class="control-label">API Live </label>
											<small style="color:#CCC;">(API Live) <?= form_error('api_live','<small>', '</small>') ?></small>
											<input type="text" value="<?php echo set_value('api_live', $result->api_live); ?>" name="api_live"  class="form-control input-sm" />
											
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group required">
											<label for="name" class="control-label">Type</label>
											<small style="color:#CCC;">(Type) <?= form_error('type','<small>', '</small>') ?></small>
											<input type="text" value="<?php echo set_value('type', $result->type); ?>" name="type" class="form-control input-sm"  />
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group required">
											<label for="name" class="control-label">Sender</label>
											<small style="color:#CCC;">(Sender) <?= form_error('sender','<small>', '</small>') ?></small>
											<input type="text" value="<?php echo set_value('sender', $result->sender); ?>" name="sender" class="form-control input-sm" />
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group required">
											<label for="name" class="control-label">&nbsp;</label>
											<small style="color:#CCC;">(Description) <?= form_error('description','<small>', '</small>') ?></small>
											<input type="text" value="<?php echo set_value('description', $result->description); ?>" name="description"  class="form-control input-sm" />
										</div>
									</div>
									
									<div class="col-md-12 text-right">
										<div class="form-group">									
											<input type="submit" name="submit" class="btn btn-success"  />
										</div>
									</div>
									
								</div> 
							</form>
						</div> 
				</div> 
			</div>
			<div class="clearfix"></div>
	 </div>
	 </div>
</div> 

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
	$(document).ready(function(){
		$(".sort-table").tablesorter({
			 headers: { 			                   
						3: { 
							sorter: false 
						},
				}
		});  
	});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	