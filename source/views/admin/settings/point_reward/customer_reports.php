<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>

<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>

<div class="row">
		
		<?php 
		$msg = $this->session->flashdata('msg'); 
		if(isset($msg)){
		?> 
				<div class="col-md-12">
					<div class="alert alert-success">
						<?php echo $msg;?>
					</div>
				</div>
		<?php }  ?> 
		<div class="col-md-12">
		<div class="portlet light bordered">
		
			<div class="portlet-title">
				<div class="caption">
					<i class="<?php echo $icon; ?>"></i>
					<span class="caption-subject bold uppercase">Customer Report  <small><i class="fa fa-line-chart"></i>  POINT & REWARD</small></span>
				</div>
			</div>
			
			<div class="portlet-body">
				<div class="col-md-12">
					<form method="POST" action="<?= site_url("admin/settings/point_reward/filter_customer_reports"); ?>"/>
						<div class="row"> 
							<div class="col-md-2">
								<div class="form-group">
									<label for="name" class="control-label">Account</label>
									<input type="text" name="account" value="<?php echo isset($_POST['account'])?$_POST['account']:""?>" class="form-control input-sm"  />
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="name" class="control-label">Email</label>
									<input type="text" name="email" value="<?php echo isset($_POST['email'])?$_POST['email']:""?>" class="form-control input-sm"  />
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="name" class="control-label">Phone</label>
									<input type="text" name="phone"  value="<?php echo isset($_POST['phone'])?$_POST['phone']:""?>" class="form-control input-sm"  />
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group"> 
									<br/>
									<button type="submit" class="btn btn-primary " name="btn_search">Search</button>
								</div>
							</div> 
						</div>
					</form>
				</div>
				
				<div class="col-md-12">
					<br/>
					<div class="table-scrollable">
						<table class="table table-striped table-bordered table-hover sort-table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Account</th>
									<th class="text-center">Username</th>
									<th class="text-center">Email</th>
									<th class="text-center">Phone</th>
									<th class="text-center">Amount</th>
									<th class="text-center" style="width:120px;">Qty</th>
									<th class="text-center" style="width:120px;">Reward Point</th>
									<th class="text-center" style="width:120px;">Amount Wallet</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($results)){?>
								<?php foreach($results as $i => $row){ ?>
									<tr>
										<td class="text-center"><?= ($i+1) ?></td>
										<td><?= $row->account ?></td>
										<td><?= $row->first_name ?></td>
										<td><?= $row->email ?></td>
										<td><?= $row->phone ?></td>
										<td class="text-right"><?= number_format($row->price,2) ?></td>
										<td class="text-center"><?= $row->qty ?></td>
										<td class="text-center"><?= floor(number_format($row->price,2)/30)?></td>
										<td class="text-right">0</td>
										<td class="text-center">
											<a onclick="return confirm('Are you sure to delete?')" class="btn btn-danger btn-xs" href="<?php echo base_url().'admin/settings/point_reward/delete_customer/'.$row->bill_id?>">Delete</a>
										</td>
									<tr>
								<?php }
									}
								  else { ?>
										<tr><td colspan="10">Not found !</td></tr>
								<?php }?>
							</tbody>
						</table> 
					</div>
					 <div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="col-sm-12">
										<div class="col-sm-7 dataTables_info" id="sample_1_info" role="status" aria-live="polite">
											<?php echo $showing; ?>
										</div> 
										<div class="option col-sm-5">
												 
										</div>
									</div>
								</div>
								<?php echo form_close(); ?>
								<div class="col-md-6 col-sm-12">
									 <div class="dataTables_paginate paging_bootstrap_full_number">
									 	<?php echo $page; ?>
									 </div>
						             
								</div>
							</div>
				</div> 
			</div>
			
			<div class="clearfix"></div>
	 </div>
	 </div>
</div> 

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
	$(document).ready(function(){
		$(".sort-table").tablesorter({
			 headers: { 			                   
						3: { 
							sorter: false 
						},
				}
		});  
	});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	