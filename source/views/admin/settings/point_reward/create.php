<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>

<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>

<div class="row">
		
		<div class="col-md-12">
		<div class="portlet light bordered">
		
			<div class="portlet-title">
				<div class="caption">
					<i class="<?php echo $icon; ?>"></i>
					<span class="caption-subject bold uppercase">Point & Reward</span>
				</div>
			</div>
			
			<div class="portlet-body">
				<div class="col-md-12"> 
						<div class="col-md-8"> 
							<form method="POST" action="<?= site_url("admin/settings/point_reward"); ?>"/>
							
								<div class="row"> 
									<?php 
									$msg = $this->session->flashdata('msg'); 
									if(isset($msg)){?>
										<div class="alert alert-success">
											<strong>Success!</strong> <?php echo $msg;?>
										</div>
									<?php } ?> 
							
									<div class="col-md-12">
										<div class="form-group">
											<label for="name" class="control-label">Enable Reward Points</label><br/>
											<input type="checkbox" value="<?php echo set_value('enable_point', 1); ?>" <?= ($result->enable_point > 0 ? "checked": ""); ?> name="enable_point" class="form-control input-sm"  />
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group required">
											<label for="name" class="control-label">Conversion Point </label>
											<small style="color:#CCC;">(USD) <?= form_error('from_amount','<small>', '</small>') ?></small>
											<input type="text" value="<?php echo set_value('from_amount', $result->from_amount); ?>" name="from_amount" placeholder="50" class="form-control input-sm" />
											
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group required">
											<label for="name" class="control-label">&nbsp;</label>
											<small style="color:#CCC;">(Point) <?= form_error('to_point','<small>', '</small>') ?></small>
											<input type="text" value="<?php echo set_value('to_point', $result->to_point); ?>" name="to_point" placeholder="1" class="form-control input-sm"  />
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group required">
											<label for="name" class="control-label">Conversion Reward</label>
											<small style="color:#CCC;">(Point) <?= form_error('from_point','<small>', '</small>') ?></small>
											<input type="text" value="<?php echo set_value('from_point', $result->from_point); ?>" name="from_point" placeholder="50" class="form-control input-sm" />
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group required">
											<label for="name" class="control-label">&nbsp;</label>
											<small style="color:#CCC;">(USD) <?= form_error('to_amount','<small>', '</small>') ?></small>
											<input type="text" value="<?php echo set_value('to_amount', $result->to_amount); ?>" name="to_amount" placeholder="1" class="form-control input-sm" />
										</div>
									</div>
									
									<div class="col-md-12">
										<div class="form-group">
											<label for="name" class="control-label">Maximum Discount</label>
											<small style="color:#CCC;">(Set maximum product discount allowed in cart when redeeming points)</small>
											<input type="text" value="<?php echo set_value('max_discount', $result->max_discount); ?>" name="max_discount" placeholder="50" class="form-control input-sm" id="max_discount" />
										</div>
									</div> 
									<div class="col-md-12">
										<div class="form-group">
											<label for="name" class="control-label">Gift Card</label><br/>
											<select class="form-control" name="gift_card">
													<option value=""></option>
												<?php foreach($gift_card as $row) {	?> 
													<option <?php echo ($row->id==$result->gift_card_id)?"selected":""; ?> value="<?php echo $row->id?>"><?php echo $row->coupon_code." | ".$row->description;?></option>
												<?php }?>
											 </select>
										</div>
									</div> 
									
									<div class="col-md-12">
										<div class="form-group">
											<label for="name" class="control-label">Up to Point</label><br/>
											<input type="checkbox" value="1" <?= ($result->up_to_point > 0 ? "checked": ""); ?> name="up_to_point" class="form-control input-sm" />
										</div>
									</div>
									
									<div class="col-md-12">
										<div class="form-group">
											<label for="name" class="control-label">Allow free shipping to Redeem</label>
											<small style="color:#CCC;">(Check this box a valid free shipping coupon if redeeming".)</small><br/>
											<input type="checkbox" value="1" <?= ($result->allow_free_shipping > 0 ? "checked": ""); ?> name="allow_free_shipping" placeholder="USD" class="form-control input-sm" />
										</div>
									</div>
									 
									<div class="col-md-12">
										<div class="form-group">
											<label for="name" class="control-label">Show points in "My Account" page</label><br/>
											<input type="checkbox" value="1" <?= ($result->allow_show_point > 0 ? "checked": ""); ?> name="allow_show_point" placeholder="USD" class="form-control input-sm" />
										</div>
									</div> 
									<div class="col-md-12 text-right">
										<div class="form-group">									
											<input type="submit" name="submit" class="btn btn-success"  />
										</div>
									</div>
								</div> 
							</form>
						</div> 
				</div> 
			</div>
			<div class="clearfix"></div>
	 </div>
	 </div>
</div> 

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
	$(document).ready(function(){
		$(".sort-table").tablesorter({
			 headers: { 			                   
						3: { 
							sorter: false 
						},
				}
		});  
	});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	