<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>

 <div class="row">

		<div class="col-md-12">
		
			<div class="portlet light bordered">
						
			   <div class="col-md-3 well">
				   <?php  echo form_open_multipart("admin/settings/systems/update"); ?>
				   <div class="form-group">
						<label>Recent Products</label>
						<div class="controls">
							<select name="order_by" class="form-control">
								<option value="desc" <?= ($result->value=="desc"?"selected":"") ?>>DESC</option>
								<option value="asc" <?= ($result->value=="asc"?"selected":"") ?>>ASC</option>
								<option value="random" <?= ($result->value=="random"?"selected":"") ?>>Random</option>
							</select>							
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<input type="submit" name="submit" class="btn btn-primary" value="Submit" />						
						</div>
					</div>
					 <?php  echo form_close(); ?>
				</div>
				
				<div class="clearfix"></div>
            </div>
			
			
			
		</div>
		
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	