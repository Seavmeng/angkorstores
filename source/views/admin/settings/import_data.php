<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php //echo $icon; ?>"></i>
								<span class="caption-subject bold">Import products</span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php //echo btn_actions('admin/product_list/create','account/product_list/create', $create_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open_multipart('admin/product/import', array('role' => 'form') ); ?>
					
						
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($success)){
						                   echo show_msg($success);
						                }
										if(isset($error)){
											echo show_msg($error);
											//echo show_msg($exist_code);
										}
							         ?>
							         <!-- Row -->
							         <div class="row">
							         		 <!-- Form -->
							         		<div class="col-md-8">	
							         			<!--Form-body -->
													<div class="form-body">
														<!-- ROW -->
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																<div class="form-group">
																	<?php echo form_label('Import CSV','pageID',array('class' => 'col-sm-3 control-label required')); ?>
																	<div class="col-sm-9">
																		<input type="file" name="file" id="file" class="input-large">
																		<font color="red"><?php echo form_error('pageID'); ?></font>
																	</div>
																</div>
																<div class="form-group">
																	
																	<div class="col-sm-12">
																		<br/>
																		Update
																		<input type="checkbox" name="update_import" value="1">
																		<br/>
																		<br/>
																	</div>
																</div>
																<div class="form-group">
																	<div class="col-sm-9">
																		<input type="submit" name="btn_save" value="<?php echo 'Import'?>" class="btn btn-primary">
																	</div>
																</div>
															</div>
															<!--End col-md-6 -->
														</div>														
													</div>
													<!--End form-body -->
													
							         		</div>
							         		<!-- End Form -->
							         		
							         </div>
							          <!-- End Row -->
							         




									</div>
								<!-- end portlet-body -->
							
						<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->
<script>
    var base_admin_assets_url = "<?php echo base_admin_assets_url(); ?>";
</script>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>

