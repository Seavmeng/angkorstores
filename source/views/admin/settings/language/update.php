<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $update_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open(base_admin_url().$controller_url.$this->uri->segment(4).'/'.$data['lang_id'], array('role' => 'form') ); ?>
					
						
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
							         <!--Form-body -->
										<div class="form-body">


											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group required <?php has_error('word'); ?>">
															<?php echo form_label(word_r('word'),'word',array('class' => 'control-label') ); ?>
															<?php echo form_input('word', $data['word'],"class = 'form-control', id= 'word'"); ?>
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->

											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group required <?php has_error('english'); ?>">
															<?php echo form_label(word_r('english'),'english',array('class' => 'control-label') ); ?>
															<?php echo form_input('english', $data['english'],"class = 'form-control', id= 'english'"); ?>
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->

											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group required <?php has_error('khmer'); ?>">
															<?php echo form_label(word_r('khmer'),'khmer',array('class' => 'control-label') ); ?>
															<?php echo form_input('khmer', $data['khmer'],"class = 'form-control', id= 'khmer'"); ?>
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->



										</div>
										<!--End form-body -->
											

										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
											
														<div class="form form-actions">
																<?php echo form_submit('update', word_r('update'), "class = 'btn blue' "); ?>
																<?php echo '<a href="'.base_admin_url().$controller_url.'" class="btn default">'.word_r('cancel').'</a>'; ?>
														</div>
												
											</div>
										</div>
										<!-- End row -->

									</div>
								<!-- end portlet-body -->
							
						<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	