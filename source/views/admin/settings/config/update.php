<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-6">
			<!-- portlet -->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="<?php echo $icon; ?>"></i>
						<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
					</div>
				</div>
						<!--end portlet-title -->
				<?php echo form_open(base_admin_url().$controller_url.'update/x', array('role' => 'form') ); ?>
						<!-- portlet-body -->
							<div class="portlet-body">
							<?php 
								if(isset($errors)){
								   echo show_msg($errors);
								}
							 ?>
							 <!--Form-body -->
								<div class="form-body">

									<?php 
										foreach($data as $v){
											$name = $v['name'];
											$value = $v['value'];
											if($name == 'sys_name'){
												?>
													<!-- ROW -->
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-6">
																	<div class="form-group ">
																		<?php echo form_label(word_r('sys_name'),'sys_name',array('class' => 'control-label') ); ?>
																		<?php echo form_input('sys_name', $value,"class = 'form-control', id= 'sys_name'"); ?>
																	</div>
															</div>
															<!--End col-md-6 -->

														</div>
														<!-- End row -->

												<?php
													}//end if
												?>

												<?php
													if($name == 'category_group_id'){
												?>
														<!-- ROW -->
															<div class="row">
																<!-- col-md-6 -->
																<div class="col-md-6">
																		<div class="form-group ?>">
																			<?php echo form_label(word_r('category_group'),'group_id',array('class' => 'control-label')); ?>
																			<?php echo drop_down(array('category_group_id','name'), 'tbl_category_groups', $value ); ?>
																		</div>
																</div>
																<!--End col-md-6 -->

															</div>
															<!-- End row -->

												<?php
													}// end if
												?>


												<?php
													if($name == 'menu_group_id'){
												?>
														<!-- ROW -->
															<div class="row">
																<!-- col-md-6 -->
																<div class="col-md-6">
																		<div class="form-group ?>">
																			<?php echo form_label(word_r('menu_group'),'group_id',array('class' => 'control-label')); ?>
																			<?php echo drop_down(array('menu_group_id','name'), 'tbl_menu_groups', $value ); ?>
																		</div>
																</div>
																<!--End col-md-6 -->

															</div>
															<!-- End row -->

												<?php
													}//end if
												?>


											<?php
										}//end foreach
									?>
								</div>
								<!--End form-body -->
									

								<!-- Row -->
								<div class="row">
									<div class="col-md-12">
										<div class="form form-actions">
											<?php echo form_submit('update', word_r('update'), "class = 'btn blue' "); ?>
											<?php echo '<a href="'.base_admin_url().$controller_url.'" class="btn default">'.word_r('cancel').'</a>'; ?>
										</div>
									</div>
								</div>
								<!-- End row -->
							</div>
						<!-- end portlet-body -->
				<?php echo form_close(); ?>
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
		
		<div class="col-md-6">
		   <div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="<?php echo $icon; ?>"></i>
						<span class="caption-subject bold uppercase"> Recent Products</span>
					</div>
				</div>
			   <div class="col-md-12">
				   <?php  echo form_open_multipart("admin/settings/config/update/y"); ?>
				   <div class="form-group">
						<label>Recent Products</label>
						<div class="controls">
							<select name="order_by" class="form-control">
								<option value="desc" <?= ($result->value=="desc"?"selected":"") ?>>DESC</option>
								<option value="asc" <?= ($result->value=="asc"?"selected":"") ?>>ASC</option>
								<option value="random" <?= ($result->value=="random"?"selected":"") ?>>Random</option>
							</select>							
						</div>
					</div>
					<div class="form-group">
						<div class="controls">
							<input type="submit" name="recent_product" class="btn btn-primary" value="Submit" />						
						</div>
					</div>
					 <?php  echo form_close(); ?>
				</div>
				
				<div class="clearfix"></div>
		    </div>
		</div>
	
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	