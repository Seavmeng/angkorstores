<?php
$this->load->view(config_item('include_dir').'header-admin');
?>
<body>
<div class="wide_layout">
	<?php $this->load->view(config_item('include_dir').'top_header'); ?>
	<div class="secondary_page_wrapper">
	<div class="container">
		<div class="row">
			<aside class="col-md-3 col-sm-4 has_mega_menu">
				<?php $this->load->view('includes/sidebar_config'); ?>
			</aside><!--/ [col]-->

			<div class="col-md-9 col-sm-8">
				<section class="theme_box">
					<h4>Contact Information</h4>
					<p>John Doe<br><a href="#" class="mail_to">john.doe@gmail.com</a></p>
					<div class="buttons_row">
						<a href="#" class="button_grey middle_btn">Edit Account Information</a>
						<a href="#" class="button_grey middle_btn">Change Password</a>
					</div>
				</section><!--/ .theme_box -->
			</div><!--/ [col]-->
		</div><!--/ .row-->
	</div><!--/ .container-->
</div><!--/ .page_wrapper-->
	    <?php $this->load->view(config_item('include_dir').'footer-sector'); ?>
</div><!--/ [layout]-->
<?php $this->load->view(config_item('include_dir').'footer'); ?>
</body>
</html>