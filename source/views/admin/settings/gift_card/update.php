<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
 <div class="row">
		<div class="col-md-12">
			<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase">Update Gift Card</span>
							</div>
						</div>
						<?php echo form_open_multipart(base_url().'admin/settings/gift_card/update/'.$id, array('role' => 'form', '') ); ?>
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                } 
							         ?>
										<div class="form-body">
											<div class="col-md-12">
												<div class="col-md-6">
															<div class="col-md-12">
																<div class="form-group required  ">
																	<?php  echo form_label('Coupon code', 'coupon_code');?>
																	<?php echo form_input('coupon_code', $result->coupon_code,"class = 'form-control', id= 'coupon_code'"); ?>
																</div>
															</div> 
															<div class="form-group required ">
																<div class="col-sm-12"> 
																<?php echo form_label(word_r('expiring_date'),'expiring_date',array('class' => 'control-label') ); ?>
																</div> 
																<div class="col-sm-6">
																		<div class="clearfix">
																			<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																			 <?php 
																					$datetimeEx = new DateTime($result->expiry_date);
																					$dateEx = $datetimeEx->format('Y-m-d');
																					$timeEx = $datetimeEx->format('H:i:s');
																			?>
																				<input name="ex_ddate" value="<?php echo  $dateEx; ?>" class="form-control datepicker" type="text"> 
																				<span class="input-group-addon datepicker-icon">
																					<span class="glyphicon glyphicon-calendar"></span>
																				</span>
																				<font color="red"><font color="red"></font></font>
																			</div>
																		</div>
																</div>
																<div class="col-sm-6">
																	<div class="clearfix">
																		<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
																			<input class="form-control timepicker" name="ex_dtime" value="<?php echo  $timeEx; ?>" type="text">
																			<span class="input-group-addon timepicker-icon">
																				<span class="glyphicon glyphicon-time"></span>
																			</span>
																			<font color="red"><font color="red"></font></font>
																		</div>
																	</div>
																</div> 
														   </div> 
															<div class="col-md-12">
															<br/>
																<div class="form-group required  ">
																	<?php  echo form_label('Description', 'description');?>
																	<?php echo form_input('description', $result->description,"class = 'form-control', id= 'description'"); ?>
																</div>
															</div>   
															<div class="col-md-12">
																<div class="form-group required  ">
																	<?php  echo form_label('Discount', 'discount');?>(%)
																	<?php echo form_input('discount', $result->discount,"class = 'form-control', id= 'discount'"); ?>
																</div>
															</div>
															<div class="col-md-12">
																<div class="form-group ">
																	<?php  echo form_label('Allow Free Shipping', 'allow_free_shipping');?><br/>
																	<input type="checkbox"  name="allow_free_shipping" value="1" <?= ($result->allow_free_shipping > 0 ? "checked": ""); ?>  class="form-control"/>
																</div>
															</div>
															<div class="col-md-12">
																<div class="form-group required  ">
																	<?php  echo form_label('Usage Limit', 'usage_limit');?>
																	<input type="number" value="<?php echo $result->usage_limit ?>" class="form-control" name="usage_limit"/> 
																</div>
															</div> 
															<div class="col-md-12">
																<input type="submit" name="btn_save" value="<?php echo 'Save'?>" class="btn btn-primary"> 
																<?php echo '<a href="'.base_admin_url().$controller_url.'" class="btn default">'.word_r('cancel').'</a>'; ?>
															</div> 
													</div>	 

													<div class="col-md-3">
																 <div class="panel panel-default">
																	<div class="widget_section">
																		<div class="panel-heading">
																			<div class="widget_title"><h3 class="panel-title">Image</h3></div>
																		</div>
																		<div class="panel-body" >
																			<div class="fileinput fileinput-new" data-provides="fileinput">
																			  <div class="fileinput-new thumbnail" style="width: 263px; height: 160px;">
																				<img name="img_img" src="<?php echo base_url().'images/gift_card/'.$result->image?>" data-src="holder.js/100%x100%" alt="">
																			  </div>
																			  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 263px; max-height: 200px;"></div>
																			  <div>
																				<span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
																					<input type="file" name="image_gift" accept=".gif,.jpg,.png" data-maxfile="1024" />
																				</span> 
																				
																				<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
																			  </div>
																			</div>
																		</div>
																	
																	</div>
																</div>  
													</div>
											</div>   
											<div class="clearfix"></div>
									</div> 
						<?php echo form_close(); ?> 
			</div>
		</div>
</div>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?> 
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>

<script type="text/javascript">
	$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			startDate: '1d'
		});
	$('.timepicker').timepicker({
		showMeridian: false,
		format: 'HH:mm',
		showSeconds: true,
		minuteStep: 1,
		secondStep: 1
	});
</script>

