<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
 <div class="row">
 
		<?php 
		$msg = $this->session->flashdata('msg'); 
		if(isset($msg)){
		?> 
				<div class="col-md-12">
					<div class="alert alert-success">
						<?php echo $msg;?>
					</div>
				</div>
		<?php }  ?>   
		 
		<div class="col-md-12"> 
			<div class="portlet light bordered"> 
						<div class="portlet-title"> 
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>  
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $view_action); ?>
							</div> 
						</div>  
						<div class="portlet-body">  
						<br/>
						<br/> 

							<?php
								echo form_open(base_admin_url().$controller_url.'ml', array('role' => 'form')); 
							 
								$this->table->set_heading("Coupon Code",
														"Description",
														"Coupon Amount",
														"Discount($)",
														"Allow free shipping",
														"Coupon expiry date",
														"Usage limits",
														"Image Event",
														"Action"
											); 
								foreach($data as $value){ 
									$id = $value['id'];
									$coupon_code = $value['coupon_code'];
									$description = $value['description'];
									$amount = $value['amount'];
									$discount = $value['discount'];
									$allow_free_shipping = $value['allow_free_shipping'];
									$expiry_date = $value['expiry_date'];
									$usage_limit = $value['usage_limit'];  
									$image = "<img style='width:50px;' src='".base_url().'images/gift_card/'.$value['image']."'/>";
									   
									
									$this->table->add_row(
															"$coupon_code",
															"$description",
															"$"."$amount",
															"$discount",
															"$allow_free_shipping", 
															"$expiry_date",
															"$usage_limit",
															"$image",  
															btn_action($controller_url.'update/'.$id, $controller_url.'delete/'.$id)
														);
								}
								echo $this->table->generate();
							 ?>
							 <div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="col-sm-12">
										<div class="col-sm-7 dataTables_info" id="sample_1_info" role="status" aria-live="polite">
											<?php echo $showing; ?>
										</div> 
										<div class="option col-sm-5">
												 
										</div>
									</div>
								</div>
								<?php echo form_close(); ?>
								<div class="col-md-6 col-sm-12">
									 <div class="dataTables_paginate paging_bootstrap_full_number">
									 	<?php echo $page; ?>
									 </div>
						             
								</div>
							</div>
						</div> 
			</div> 
		</div> 
</div>  	
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	<script>
			$(document).ready(function(){
				$(".sort-table").tablesorter({
					 headers: { 			                   
			                    3: { 
			                        sorter: false 
			                    },
			            }
				});  
			});
		</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	