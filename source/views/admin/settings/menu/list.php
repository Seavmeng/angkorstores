<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $view_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->

						<!-- portlet-body -->
						<div class="portlet-body">
								<?php 
					                if(isset($errors)){
					                   echo show_msg($errors);
					                }
					            ?>
							 <div class="row">

								<div class="col-md-4 col-sm-4">
								
												
											
												<?php echo form_open(base_admin_url().$controller_url.'create', array('role' => 'form') ); ?>
													<div class="form-group ">
															<?php echo form_label(word_r('icon'),'icon',array('class' => 'control-label') ); ?>
															<?php echo form_input('icon', $this->input->post('icon', true),"class = 'form-control', id= 'icon'"); ?>
													</div>

													<div class="form-group required <?php has_error('menu_name'); ?>">

															<?php echo form_label(word_r('name'),'menu_name',array('class' => 'control-label') ); ?>
															<?php echo form_input('menu_name', $this->input->post('menu_name', true),"class = 'form-control', id= 'menu_name'"); ?>
													</div>
													<div class="form-group required <?php has_error('menu_name_kh'); ?>">
															<?php echo form_label(word_r('name_kh'),'menu_name_kh',array('class' => 'control-label') ); ?>
															<?php echo form_input('menu_name_kh', $this->input->post('menu_name_kh', true),"class = 'form-control', id= 'menu_name_kh'"); ?>
													</div>

													<div class="form-group required <?php has_error('url'); ?>">
															<?php echo form_label(word_r('url'),'url',array('class' => 'control-label') ); ?>
															<?php echo form_input('url', $this->input->post('url', true),"class = 'form-control', id= 'url'"); ?>
													</div>
													<div class="form-group">
															<?php echo form_label(word_r('active'),'active',array('class' => 'control-label'));; ?>
															<div class="check-list">
																<?php echo '<label>'.form_checkbox('active',1,true).word_r('active').'</label>'; ?>
															</div>
													</div>
													<?php 
													echo form_submit('create', word_r('add_to_menu'), "class = 'btn blue btn-xs pull-right' ");
													echo form_close();
													 ?>
												
								</div>
								
								<div class="col-md-8 col-sm-8">
									<!-- nestable -->
										<?php echo show_sys_menu_sort(); ?>
						             <!-- nestable -->
								</div>
							</div>
						</div>
						<!-- end portlet-body -->
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
				<script type="text/javascript">
						
					$(document).ready(function() {
							 var updateOutput = function(e)
                              {
                                  var list   = e.length ? e : $(e.target),
                                      output = list.data('output');
                                  if (window.JSON) {
                                      output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                                      menu_updatesort(window.JSON.stringify(list.nestable('serialize')));
                                  } else {
                                      output.val('JSON browser support required for this demo.');
                                  }
                              };
                            
                            // activate Nestable for list menu
                            $('#nestableMenu').nestable({
                                group: 1
                            })
                            .on('change', updateOutput);
                            // output initial serialised data
                            updateOutput($('#nestableMenu').data('output', $('#nestableMenu-output')));
					
					});

					 function lagXHRobjekt() {
							var XHRobjekt = null;
							
							try {
								ajaxRequest = new XMLHttpRequest(); // Firefox, Opera, ...
							} catch(err1) {
								try {
									ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP"); // Noen IE v.
								} catch(err2) {
									try {
											ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP"); // Noen IE v.
									} catch(err3) {
										ajaxRequest = false;
									}
								}
							}
							return ajaxRequest;
						}
						
						
						function menu_updatesort(jsonstring) {
							mittXHRobjekt = lagXHRobjekt(); 
						
							if (mittXHRobjekt) {
								mittXHRobjekt.onreadystatechange = function() { 
									if(ajaxRequest.readyState == 4){
										// var ajaxDisplay = document.getElementById('sortDBfeedback');
										// ajaxDisplay.innerHTML = ajaxRequest.responseText;
									} else {
										// Uncomment this an refer it to a image if you want the loading gif
										//document.getElementById('sortDBfeedback').innerHTML = "<img style='height:11px;' src='images/ajax-loader.gif' alt='ajax-loader' />";
									}
								}
								
								ajaxRequest.open("GET", "<?php echo base_admin_url().$controller_url; ?>"+"save?jsonstring=" + jsonstring + "&rand=" + Math.random()*9999, true);
								ajaxRequest.send(null); 
							}
						}
						
                   	
					
                </script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	