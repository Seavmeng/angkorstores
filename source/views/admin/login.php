<?php 
if(has_logined()){
	redirect(base_admin_url().'dashboard');
}
$this->load->view('includes/head');?>
<body class="page home page-template-default">
	<div id="page" class="hfeed site">
		<?php $this->load->view('includes/header1');?>
		<div id="content" class="site-content" tabindex="-1">
			<div class="container">
				<nav class="woocommerce-breadcrumb" >
					<a href="<?php echo base_url(); ?>">Home</a>
					<span class="delimiter"><i class="fa fa-angle-right"></i></span>
					My Account
				</nav><!-- .woocommerce-breadcrumb -->
				<?php 
				if(isset($errors)){ echo show_error_msg($errors);}
				if(isset($success)){ echo show_msg($success);}
				if(isset($rmsg)){echo show_msg($rmsg,'success');}
				?>
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<article id="post-8" class="hentry">
							<div class="entry-content">
								<div class="woocommerce">
									<div class="customer-login-form">
										<span class="or-text">or</span>
										<div class="col2-set" id="customer_login">
											<div class="col-1">
												<h2>Login</h2>
												<?php echo form_open(base_admin_url().'login'); ?>
													<p class="before-login-text">
														Welcome back! Sign in to your account
													</p>
													<p class="form-row form-row-wide <?php has_error('username');?>">
														<label for="username">E-mail
														<span class="required">*</span></label>
														<?php echo form_input(array('name' => 'username', 'class' => 'form-control form-control-solid placeholder-no-fix', 'id' => 'username', 'placeholder' => 'Username', 'autocomplete' => 'off')); ?>
													</p>
													<p class="form-row form-row-wide <?php has_error('password');?>">
														<label for="password">Password
														<span class="required">*</span></label>
														<?php echo form_password(array('name' => 'password', 'class' => 'form-control form-control-solid placeholder-no-fix', 'id' => 'password', 'placeholder' => 'Password', 'autocomplete' => 'off')); ?>
													</p>
													<p class="form-row">
														<input class="button" type="submit" value="Login" name="login">
														<!--<label for="rememberme" class="inline">
															<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember me
														</label>-->
													</p>
													
													
													
													
													<p class="lost_password">
														<!-- <a href="login-and-register.html">Lost your password?</a> -->
														<a href="<?php echo base_admin_url().'register/reset_pwd'; ?>">Lost your password?</a>
													</p>
													
													
													
													
												<?php echo form_close(); ?>
											</div><!-- .col-1 -->

											<div class="col-2">

												<h2>Register</h2>

												<form method="post" class="register">

													<p class="before-register-text">
														Create a new account
													</p>
													<!--
													<p class="form-row form-row-wide">
														<label for="reg_email">Email address
														<span class="required">*</span></label>
														<input type="email" class="input-text" name="email" id="reg_email" value="" />
													</p>-->


													<p class="form-row">
														<a href="<?php echo base_admin_url().'register';?>" name="register">Register Now</a>
													</p>

													<div class="register-benefits">
														<h3>Sign up today and you will be able to :</h3>
														<ul>
															<li>Speed your way through checkout</li>
															<li>Track your orders easily</li>
															<li>Keep a record of all your purchases</li>
														</ul>
													</div>

												</form>

											</div><!-- .col-2 -->

										</div><!-- .col2-set -->

									</div><!-- /.customer-login-form -->
								</div><!-- .woocommerce -->
							</div><!-- .entry-content -->

						</article><!-- #post-## -->

					</main><!-- #main -->
				</div><!-- #primary -->


			</div><!-- .col-full -->
		</div><!-- #content -->
<?php $this->load->view('includes/sub_footer');?>

	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>
</body>
</html>
