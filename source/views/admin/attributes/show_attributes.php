<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="<?php echo $icon; ?>"></i>
						<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
					</div>
					<div class="actions">
						<?php echo title_actions($controller_url.'create',$controller_url, $view_action); ?>
					</div>
				</div>
				<?php 
				if(isset($errors)){
					echo show_msg($errors);
				}
				if(isset($rmsg)){
					echo show_msg($rmsg);
				}
				// $catFilter = catForFilter();
				?>
				<?php echo $this->session->flashdata('msg'); ?>
				<div class="portlet-body">
					<div class="small text-bold left mt5">
						Show&nbsp;
						<select class="lengthSelect">
							<option value="5">5</option>
							<option value="10" selected>10</option>
							<option value="20">20</option>
							<option value="50">50</option>
						</select> 
					</div>
					<?php 
					echo form_open(account_url().$controller_url.'results', array('role' => 'form','method'=>'get'));?>

						<div class="row">
							<div class="col-md-5">
								filter
							</div>
							<div class="col-md-7 right">
								<div class="clearfix search">
									<input type="text" class="alignleft search_width" placeholder="<?php echo word_r('enter-keyword')?>" name="search_query">
									<input type="button" class="btn btn-info def_icon_btn alignleft button" value="Search">
								</div>
							</div>
						</div>
					<?php 
					echo form_close(); 
					
					echo form_open(account_url(), array('role' => 'form','class'=>'form-horizontal')); 

						$i=0;
						$output ="";
						$output .='<div class="table-responsive"><table class="table table-striped table-bordered table-hover">';
						$output .= '<thead><tr>';
						$output .= '<th width="50px"></th>';
						$output .= '<th>'.word_r('n').'</th>';
						$output .= '<th >'.word_r('attributes').'</th>';
						$output .= '<th>'.word_r('description').'</th>';
						$output .= '<th>'.word_r('action').' </th>';
						$output .= '</tr></thead>';
						foreach($data as $value){
							$i++;
							$attr_id = $value['attr_id'];
							$attr_name = $value['attr_name'];
							$description = $value['description'];
							
							$output .='<tr>';
							$output .= '<td width="50px"><input type="checkbox" class="select"></td>';
							$output .= '<td>'.$i.'</td>';

							$output .='<td ';
							$output .=' id="'.$attr_id.'" class="opt-parent" >';
							$output .= $attr_name.'</td>';

							$output .='<td ';
							$output .=' id="'.$attr_id.'" class="opt-parent" >';
							$output .= $description.'</td>';

							$output .='<td ';
							$output .=' id="'.$attr_id.'" class="opt-parent" >';
							$output .= btn_action('attributes/attribute/update/'.$attr_id,'attributes/attribute/delete/'.$attr_id).'</td>';
							$output .=_child_attribute($attr_id,has_logged('user_id'),$level=1);
		
							$output .='</tr>';
						}
						$output .='</table></div>';	
						echo $output;
					 ?>	
					<div class="row">
						<div class="col-md-6">
							<?php echo $showing; ?>
						</div>
						<div class="col-md-6 right">
							<?php echo $page; ?>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
				<!-- end portlet-body -->
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
	$(document).ready(function(){
		$(".sort-table").tablesorter({
			 headers: {  
				4: { 
					sorter: false 
				}, 
				0: { 
					sorter: false 
				}
			}
		}); 
	});
</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	