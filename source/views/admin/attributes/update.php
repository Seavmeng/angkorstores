<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
				<!-- portlet-title -->
				<div class="portlet-title">
					<!-- caption -->
					<div class="caption">
						<i class="<?php echo $icon; ?>"></i>
						<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
					</div>
					<!-- end caption -->
					<!-- actions -->
					<div class="actions">
						<?php echo btn_actions($controller_url.'create',$controller_url, $update_action); ?>
					</div>
					<!-- end actions -->
				</div>
				<?php echo form_open(account_url().$controller_url.$this->uri->segment(4).'/'.$data['attr_id'], array('role' => 'form','class' => 'form-horizontal') ); ?>
				<?php 
					if(isset($errors)){
					   echo msg($errors);
					}
					if(isset($success)){
						echo msg_suc($success);
					}
				 ?>
					<div class="form-body">
						<div class="row">
							<!-- col-md-6 -->
							<div class="col-md-6">
								<div class="form-group">
									<div class="col-sm-12">
										<?php echo form_label(word_r('parent'),'parent'); ?>				
										<?php  echo show_parent_attribute_select_option('parent',$data['attr_id'],has_logged('user_id')); ?>
										<font color="red"><?php echo form_error('parent'); ?></font>	
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<!-- col-md-6 -->
							<div class="col-md-6">
								<div class="form-group">
									<div class="col-sm-12">
										<?php echo form_label('Attributes','attributes',array('class' => 'required') ); ?>								
										<?php echo form_input('attr_name',$data['attr_name'],"class = 'form-control', id= 'attr_name'"); ?>										
										<font color="red"><?php echo form_error('attr_name'); ?></font>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<div class="col-sm-12">
										<?php echo form_label(word_r('description'),'description'); ?>
										<?php echo form_textarea(array('name' => 'description', 'class' => 'form-control', 'id' => 'description','value' => $data['description']) );?>
										<font color="red"><?php echo form_error('description'); ?></font>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form form-actions">
									<?php echo form_submit('update', word_r('update'), "class = 'btn btn-info' "); ?>
									<?php echo '<a href="'.account_url().$controller_url.'" class="btn btn-danger">'.word_r('cancel').'</a>'; ?>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>	
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	