<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- - - - - - - - - - - - - - Main Wrapper - - - - - - - - - - - - - - - - -->
<div class="wide_layout">
	<!-- - - - - - - - - - - - - - End Header - - - - - - - - - - - - - - - - -->
    <?php $this->load->view(config_item('include_dir').'top_header'); ?>
	<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->
	<div class="page_wrapper">
		<div class="container">
			<!-- - - - - - - - - - - - - - Checkout method - - - - - - - - - - - - - - - - -->
			<section class="section_offset">
			 <?php echo form_open(account_url()."register/")?>
				<h3 class="offset_title">Step 2 register</h3>
				<?php 
					if(isset($errors)){
					   echo msg($errors);
					}
					if(isset($success)){
						echo msg_suc($success);
					}
				?>
				<div class="relative">
					<a class="icon_btn button_dark_grey edit_button" href="#"><i class="icon-pencil"></i></a>
					<div class="table_layout">
						<div class="table_row">
							<div class="table_cell">
								<ul>
									<li class="row">
										<div class="col-xs-12">
											<label for="input_1">I am a</label>
											<div class="form_el">
												<?php
											//	echo get_roles();
												?>
												<font color="red"></font>
											</div>
										</div>
									</li>
									<li class="row"><div class="col-xs-12">	&nbsp;</div></li>
									<li class="row">
										<div class="col-xs-12">
											<label class="required" for="input_1"><?php echo word_r('adr')?></label>
											<div class="form_el">
												<textarea name="address">
													
												</textarea>
												<font color="red"></font>
											</div>
										</div>
									</li>
									<li class="row"><div class="col-xs-12">	&nbsp;</div></li>
									<li class="row">
										<div class="col-xs-12">
											<label for="input_1">Phone :</label>
											<div class="form_el">
												<input type="text" id="username" class="form-control" value="" name="phone">
												<font color="red"></font>
											</div>
										</div>
									</li>
									<li class="row"><div class="col-xs-12">	&nbsp;</div></li>
									<li class="row">
										<div class="col-xs-12">
											<label class="required" for="input_1">Password :</label>
											<div class="form_el">
											  <input type="password" id="pass1" placeholder="Your Password." class="form-control" name="password">
												<font color="red">												<div id="passwordDescription">&nbsp;</div>
												<div class="strength0" id="passwordStrength"></div></font>
											</div>
										</div>
									
									</li><li class="row"><div class="col-xs-12">	&nbsp;</div></li>
									<li class="row">
										<div class="col-xs-12">
											<label class="required" for="input_1">Confirm Password :</label>
											<div class="form_el">
											  <input type="password" id="pass2" placeholder="Your Password Again." name="password_confirm" class="form-control">
												<font color="red"></font>
												<span class="confirmMessage" id="confirmMessage"></span>
											</div>
										</div>
									
									</li><li class="row"><div class="col-xs-12">	&nbsp;</div></li>
									
									<li class="row">
										<div class="col-xs-12">
										<input type="submit" class="button_blue middle_btn" name="btn_gu_submit" value="Register">
										</div>
									</li>
								</ul>				
							</div><!--/ .table_cell -->
						</div><!--/ .table_row -->
					</div><!--/ .table_layout -->
				</div><!--/ .relative -->
			<?php echo form_close(); ?>
			</section><!--/ .section_offset -->
			<!-- - - - - - - - - - - - - - End of checkout method - - - - - - - - - - - - - - - - -->
		</div>
	</div><!--/ .page_wrapper-->
	<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
	<!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->
    <?php $this->load->view(config_item('include_dir').'footer-sector'); ?>
	<!-- - - - - - - - - - - - - - End Footer - - - - - - - - - - - - - - - - -->
</div><!--/ [layout]-->
<!-- - - - - - - - - - - - - - End Main Wrapper - - - - - - - - - - - - - - - - -->	
<?php $this->load->view(config_item('include_dir').'footer'); ?>
</body>
</html>