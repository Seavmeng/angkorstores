<?php $this->load->view('includes/head');?>
<body class="page home page-template-default">
	<div id="page" class="hfeed site">
		<?php $this->load->view('includes/header1');?>
		<div id="content" class="site-content" tabindex="-1">
			<div class="container">
				<nav class="woocommerce-breadcrumb" >
					<a href="<?php echo base_admin_url() ?>">Home</a>
					<span class="delimiter"><i class="fa fa-angle-right"></i></span>
					My Account
				</nav><!-- .woocommerce-breadcrumb -->
				<?php echo $this->session->flashdata('msg'); ?>
				<?php 
				if(isset($errors)){
				   echo show_msg($errors);
				}
				?>
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<article id="post-8" class="hentry">
							<div class="entry-content">
								<div class="woocommerce">
									<div class="customer-login-form">
										<h2>Forgotten your password?</h2>
										<?php echo form_open(account_url()."register/reset_pwd", array('role' => 'form','class' => 'form-horizontal'))?>
										<div class="page-content">
											<div class="row">
												<div class="col-sm-8">
													<!-- normal control -->
													
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo word_r('email')?></label>
														
															<input type="text" name="email" class="form-control" placeholder="Type the email address you used when you registed with Angkorstores.Then we'll email a code to this address" required >												
													</div>													
													
													<div class="form-group">
														<div>
															<label class="control-label">Fill captcha code</label>	
														</div>		
														<div>									
															<span class='captcha'>
															<?php echo $cap_img; ?>
															</span>
															<input type="text" name="captcha"/>
															<a href='javascript:void(0)' class ='refresh_cap'>
																<i id='ref_symbol' class="fa fa-refresh btn-lg"></i>
															</a>
															<font color="red"><?php echo form_error('captcha');?></font>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-6">
															<input type="submit" value="Submit" name="btn_gu_submit" class="btn btn-primary" />
														</div>
													</div>
												</div>
											</div>
										</div>
										<?php echo form_close(); ?>										

									</div><!-- /.customer-login-form -->
								</div><!-- .woocommerce -->
							</div><!-- .entry-content -->

						</article><!-- #post-## -->

					</main><!-- #main -->
				</div><!-- #primary -->


			</div><!-- .col-full -->
		</div><!-- #content --><br/><br/><br/><br/>
<?php $this->load->view('includes/sub_footer');?>

	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>
</body>
</html>
