<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $create_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open_multipart(base_admin_url().$controller_url.$this->uri->segment(4), array('role' => 'form') ); ?>
					
						
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
							         <!--Form-body -->
										<div class="form-body">

										<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="fileinput fileinput-new" data-provides="fileinput">
														  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
														    <img data-src="holder.js/100%x100%" alt="">
														  </div>
														  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
														  <div>
														    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="userfile[]"></span>
														    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
														  </div>
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->

											

											

											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group required <?php has_error('username'); ?>">
															<?php echo form_label(word_r('username'),'username',array('class' => 'control-label') ); ?>
															<?php echo form_input('username', $this->input->post('username', true),"class = 'form-control', id= 'username'"); ?>
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->

											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group required <?php has_error('email'); ?>">
															<?php echo form_label(word_r('email'),'email',array('class' => 'control-label') ); ?>
															<?php echo form_input('email', $this->input->post('email', true),"class = 'form-control', id= 'email'"); ?>
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->


											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group required <?php has_error('password'); ?>">
															<?php echo form_label(word_r('password'),'password',array('class' => 'control-label') ); ?>
															<?php echo form_password('password', $this->input->post('password', true),"class = 'form-control', id= 'password'"); ?>
														</div>
												</div>
												<!--End col-md-6 -->
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group required <?php has_error('password_confirm'); ?>">
															<?php echo form_label(word_r('password_confirm'),'password_confirm',array('class' => 'control-label') ); ?>
															<?php echo form_password('password_confirm', $this->input->post('password_confirm', true),"class = 'form-control', id= 'password_confirm'"); ?>
														</div>
												</div>
												<!--End col-md-6 -->

												

											</div>
											<!-- End row -->


											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group required <?php has_error('group_id'); ?>">
															<?php echo form_label(word_r('user_group'),'group_id',array('class' => 'control-label')); ?>
															<?php echo drop_down(array('user_group_id','user_group_name'), 'sys_user_groups', $this->input->post('user_group_id') ); ?>
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->

											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group">
															<?php echo form_label(word_r('bio'),'bio',array('class' => 'control-label') ); ?>
															<?php echo form_textarea(array('name' => 'bio', 'class' => 'form-control', 'id' => 'bio',  'value' => $this->input->post('bio')) );?>
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->

											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group">
															<?php echo form_label(word_r('active'),'user_active',array('class' => 'control-label'));; ?>
															<div class="check-list">
																<?php echo '<label>'.form_checkbox('user_active',1,true).word_r('active').'</label>'; ?>
															</div>
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->
<!-- ROW -->

										</div>
										<!--End form-body -->
											

										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
											
														<div class="form form-actions">
																<?php echo form_submit('create', word_r('create'), "class = 'btn blue' "); ?>
																<?php echo '<a href="'.base_admin_url().$controller_url.'" class="btn default">'.word_r('cancel').'</a>'; ?>
														</div>
												
											</div>
										</div>
										<!-- End row -->

									</div>
								<!-- end portlet-body -->
							
						<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	