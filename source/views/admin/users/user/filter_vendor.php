<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
	<?php echo breadcrumb();
	//var_dump($supplier_info);
	?>
	 <?php 
		if($supplier_info['page_status'] == 1){
			 $pageStatus = '(Account actived)';
			 $method = 'disable';
			 $btnDisable = 'Disable';
		 }else{
			 $pageStatus = '(Account disabled)';
			 $method = 'enable';
			 $btnDisable = 'Enable';
		 }
													 ?>
	
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			
			<!-- portlet -->
			
					<div class="portlet light bordered">
								<?php /*<!-- portlet-title -->
								<div class="portlet-title">
									<!-- caption -->
									<div class="caption">
										<i class="<?php //echo $icon; ?>"></i>
										<span class="caption-subject bold uppercase"> <?php echo '<h2>Profile</h2>'; ?></span>
									</div>
									<!-- end caption -->

									<!-- actions -->
									<div class="actions">
										<?php //echo btn_actions($controller_url.'create',$controller_url, $create_action); ?>
									</div>
									<!-- end actions -->
								</div>
									<!--end portlet-title --> */ ?>
								<?php echo form_open_multipart(base_admin_url().'register/info'); ?>
							
								
										<!-- portlet-body -->
											<div class="portlet-body">
											<?php 
												if(isset($errors)){
												   echo show_msg($errors);
												}
												if(isset($success)){
												   echo show_msg($success);
												}
												if($this->session->flashdata('msg')){
												   $rmsg = $this->session->flashdata('msg');
												   echo show_msg($rmsg);
												}

												
											 ?>
											 <!-- Row -->
											 <h3>Vendor Information <?php echo $pageStatus; ?></h3>
											 <div class="row">
													 <!-- Form -->
													<div class="col-md-8">
														<!--Form-body -->
																<div class="form-body">
																	<!-- ROW -->
																	<div class="row">
																		<!-- col-md-6 -->
																		<div class="col-md-3">
																			<div class="form-group required">
																					<?php echo form_label(word_r('first_name'),'first_name',array('class' => 'control-label') ); ?>
																				</div>
																		</div>
																		<?php 
																		if($supplier_info['first_name'] =='' || $supplier_info['first_name'] == null){
																			$value = $this->input->post('first_name');	
																		}else{
																			$value = $supplier_info['first_name'];
																		}
																		?>
																		<div class="col-md-9">
																			<div class="form-group">

																				<input type="text" value="<?php echo $value; ?>" name="first_name" id="title" class="form-control">
																			</div>
																		</div>
																		<!--End col-md-6 -->
																	</div>
																	<div class="row">
																		<!-- col-md-6 -->
																		<div class="col-md-3">
																			<div class="form-group required">
																					<?php echo form_label(word_r('last_name'),'last_name',array('class' => 'control-label') ); ?>
																			</div>
																		</div>
																		<?php 
																		if($supplier_info['last_name'] =='' || $supplier_info['last_name'] == null){
																			$value = $this->input->post('last_name');	
																		}else{
																			$value = $supplier_info['last_name'];
																		}
																		?>
																		<div class="col-md-9">
																			<div class="form-group">
																				<!-- <?php echo form_input('last_name',$data['last_name'],"class = 'form-control', id= 'title'"); 
																				 echo form_error('last_name');
																				?> -->
																				<input type="text" name="last_name" value="<?php echo $value; ?>" id="title" class="form-control" >
																			</div>
																		</div>
																		<!--End col-md-6 -->
																	</div>

																<?php if($this->session->userdata('group_id') == 1 || $this->session->userdata('group_id')== 2 ){ ?>	
																	<div class="row">
																		<!-- col-md-6 -->
																		<div class="col-md-3">
																			<div class="form-group required">
																			<label>Company Name</label>
																			</div>
																		</div>
																		<?php 
																		if($supplier_info['company_name'] =='' || $supplier_info['company_name'] == null){
																			$value = $this->input->post('company');	
																		}else{
																			$value = $supplier_info['company_name'];
																		}
																		?>
																		<div class="col-md-9">
																			<div class="form-group">
																				<input type="text" value="<?php echo $value; ?>" name="company" id="company_name_" class="form-control">
																				<?php echo form_error('company');?>
																			</div>
																		</div>
																		<!--End col-md-6 -->
																	</div>
																	<div class="row">
																		<!-- col-md-6 -->
																		<div class="col-md-3">
																			<div class="form-group required">
																			<label>Page Name </label>
																			<?php $pageName = $supplier_info['page_name']; ?>
																			</div>
																		</div>
																		
																		<div class="col-md-9">
																			<div class="form-group">
																			<?php 
																			if($pageName!=null || $pageName !=''){
																				//echo '<p>'.base_url().$pageName.'</p>';
																				echo form_input('page_name', $pageName, 'class="form-control" id="page-name-url" disabled');
																				
																				 if($this->session->userdata('group_id') == 2){ ?>
																					<a target="_blank" href="<?php echo base_url().$supplier_info['page_name']; ?>">
																						<input type="button" name="btn" class="btn btn-danger btn-xs" value="Visit Page">
																					</a>
																				<?php } 
																			}
																			else{
																			?>
																				<input type="text" value="<?php echo $supplier_info['page_name']; ?>" name="page_name" class="form-control" id="page-value">
																				<?php
																				if(isset($exist)){
																					echo '<font color="red" id="exist-page">'.$exist.'<span></span></font>';
																				}
																			}
																				//echo '<label>Note: Can\'t change</label>';
																				echo '<font color="red" id="page_name">'.'<span></span></font>';
																				
																				$data = array(
																						'type'  => 'hidden',
																						'name'  => 'page-exist',
																						'id'    => 'page-exist',
																						'value' => ''
																				);

																				echo form_input($data);
																				//echo form_hidden('exist', '', array('class'=>'exist'));
																				echo form_error('page_name');

																				?>
																				
																			</div>

																		</div>
																		<!--End col-md-6 -->
																	</div>
																	
																	<!-- Websit -->
																	<div class="row">
																		<div class="col-md-3">
																			<div class="form-group">
																			<label>Website</label>
																			</div>
																		</div>
																		<?php 
																		if($supplier_info['website'] =='' || $supplier_info['website'] == null){
																			$value = $this->input->post('website');	
																		}else{
																			$value = $supplier_info['website'];
																		}
																		?>
																		<div class="col-md-9">
																			<div class="form-group">
																				<input type="text" value="<?php echo $value; ?>" name="website" id="website" class="form-control" placeholder="www.example.com">
																				
																			</div>
																		</div>
																	</div><!-- End websit -->
																	<!-- Facebook Page -->
																	<div class="row">
																		<div class="col-md-3">
																			<div class="form-group">
																				<label>Facebook Page</label>
																			</div>
																		</div>
																		<?php 
																		if($supplier_info['facebook_page'] =='' || $supplier_info['facebook_page'] == null){
																			$value = $this->input->post('facebook_page');	
																		}else{
																			$value = $supplier_info['facebook_page'];
																		}
																		?>
																		<div class="col-md-9">
																			<div class="form-group">
																				<input type="text" value="<?php echo $value; ?>" name="facebook_page" id="facebook_page" class="form-control" placeholder="www.facebook.com/example" >
																				
																			</div>
																		</div>
																	</div><!-- End Facebook_page -->
																	<?php }
																?>

																	<div class="row">
																		<!-- col-md-6 -->
																		<div class="col-md-3">
																			<div class="form-group required">
																					<?php echo form_label('Country','Country',array('class' => 'control-label') ); ?>
																			</div>
																		</div>
																		<?php 
																		if($supplier_info['country'] =='' || $supplier_info['country'] == null){
																			$value = $this->input->post('country');	
																		}else{
																			$value = $supplier_info['country'];
																		}
																		?>
																		<div class="col-md-9">
																			<div class="form-group">
																				<?php 
																				echo '<select class="form-control" name="country" id="custom-country">
																						<option value="">Select a country…</option>';
																						foreach($get_country as $data){
																							if($value == $data['loc_id']){
																								$selected="selected";
																							}else{
																								$selected="";
																							}
																							echo '<option value="'.$data['loc_id'].'" '.$selected.'>
																							'.$data['location_name'].'
																							</option>';
																						}
																				echo '</select>';
																				?>
																				<!-- <input type="text" name="country" class="form-control" id="title" required> -->
																			</div>
																		</div>
																		<!--End col-md-6 -->
																	</div>
																	<div class="row">
																		<!-- col-md-6 -->
																		<div class="col-md-3">
																			<div class="form-group required">
																					<?php echo form_label('City','City',array('class' => 'control-label') ); ?>
																				</div>
																		</div>
																		<?php
																		if($supplier_info['city'] =='' || $supplier_info['city'] == null){
																			$value = $this->input->post('city');	
																		}else{
																			$value = $supplier_info['city'];
																		}
																		?>
																		<div class="col-md-9">
																			<div class="form-group">	
																				<?php 
																				/*echo '<select class="form-control" name="city" id="khmer-city">
																						<option value="">Select a city</option>';
																						foreach($get_kh_city as $data){
																							if($value == $data['loc_id']){
																								$selected="selected";
																							}else{
																								$selected="";
																							}
																							echo '<option value="'.$data['loc_id'].'" '.$selected.'>
																							'.$data['location_name'].'
																							</option>';
																						}
																				echo '</select>';*/
																				?>
																				<input type="text" name="city" value="<?php echo $value; ?>" class="form-control" id="title" >
																			</div>
																			
																		</div>
																		<!--End col-md-6 -->
																	</div>
																	<div class="row">
																		<!-- col-md-6 -->
																		<div class="col-md-3">
																			<div class="form-group required">
																					<?php echo form_label('Contact','title',array('class' => 'control-label') ); ?>
																			</div>
																		</div>
																		<div class="col-md-9">
																			<div class="row">
																				<div class="col-md-4">
																					<div class="form-group required">
																						<?php echo form_label('Postal / Zip','Zip',array('class' => 'control-label') ); ?>
																						<?php
																						if($supplier_info['zip'] =='' || $supplier_info['zip'] == null){
																							$value = $this->input->post('zip');	
																						}else{
																							$value = $supplier_info['zip'];
																						}
																						?>
																						<input type="text" name="zip" value="<?php echo $value; ?>" class="form-control" id="title">
																					</div>
																				</div>
																				<div class="col-md-4">
																					<div class="form-group required">
																					<?php echo form_label('Phone','Phone',array('class' => 'control-label') ); ?>
																					<?php
																						if($supplier_info['phone'] =='' || $supplier_info['phone'] == null){
																							$value = $this->input->post('phone');	
																						}else{
																							$value = $supplier_info['phone'];
																						}
																						?>
																					<input type="text" placeholder="123456789" value="<?php echo $value; ?>" name="phone" class="form-control" id="title">

																					</div>
																				</div>
																				<div class="col-md-4">
																					<div class="form-group">
																						<label>Fax</label>
																						<?php
																						if($supplier_info['fax'] =='' || $supplier_info['fax'] == null){
																							$value = $this->input->post('fax');	
																						}else{
																							$value = $supplier_info['fax'];
																						}
																						?>
																						<input type="text" value="<?php echo $value; ?>" name="fax" class="form-control" id="title">
																					</div>
																					
																				</div>
																			</div>
																		</div>
																		<!--End col-md-6 -->
																	</div>
																	<div class="row">
																		<!-- col-md-6 -->
																		<div class="col-md-12">
																				<div class="form-group">
																				<label>Address</label>
																				<?php
																						if($supplier_info['address'] =='' || $supplier_info['address'] == null){
																							$value = $this->input->post('description');	
																						}else{
																							$value = $supplier_info['address'];
																						}
																						?>
																					<textarea style="height: 180px;" name="description" class="form-control" id="article"><?php echo $value; ?></textarea>
																				</div>
																		</div>
																		<!--End col-md-6 -->

																	</div>

																	<?php if($this->session->userdata('group_id')== 2 ){ ?>
																		<div class="row">
																			<!-- col-md-6 -->
																			<div class="col-md-12">
																					<div class="form-group">
																					<label>Company Profile</label>
																					<?php
																						if($supplier_info['info'] =='' || $supplier_info['info'] == null){
																							$value = $this->input->post('info');	
																						}else{
																							$value = $supplier_info['info'];
																						}
																						?>
																						<textarea style="height: 180px;" name="info" class="form-control" id="article"><?php echo $value; ?></textarea>
																						<span><?php echo form_error('info'); ?></span>
																					</div>
																			</div>
																			<!--End col-md-6 -->
																		</div>
																		<?php }
																	?>
																	<!-- End row -->
																</div>
																<!--End form-body -->
														
															
														
													</div>
													<!-- End Form -->
													<!-- Action -->
													<div class="col-md-4">
														<!-- Row 1-->
														<div class="row">
															<!-- col-md-12 -->
															<div class="col-md-12">
																<!-- panel -->
																<div class="panel panel-default">

																	<!-- panel-heading -->
																	<div class="panel-heading">
																		<h3 class="panel-title"> <?php word('publish'); ?> </h3>
																	</div>
																	<!-- end panel-heading -->

																	<!-- panel-body -->
																	<div class="panel-body">
																		

																		<div class="form form-actions">
																				<a href="<?php echo base_url(). 'admin/manage/vendors/'.$method.'/' . $supplier_info['user_id'] ?> " class="btn btn-primary"><?php echo $btnDisable;?></a>
																				<a href="<?php echo base_url(). 'admin/users/user/update/' . $supplier_info['user_id'] ?> " class="btn btn-primary" target="_blank">Chang Password</a>
																				
																		</div>
																	</div>
																	<!-- end panel-body -->
																</div>
																<!-- end panel -->
															</div>
														</div>
														<div class="row">
															<!-- col-md-12 -->
															<div class="col-md-12">
																<!-- panel -->
																<div class="panel panel-default">

																	<!-- panel-heading -->
																	<div class="panel-heading">
																	<?php if($this->session->userdata('group_id') == 1 || $this->session->userdata('group_id')== 2 ){ 
																			$logo = 'Company Logo (Recommend size 150 x 150 pixels)';
																		}else{
																			$logo = 'Profile Picture';
																		}
																	?>
																		<h3 class="panel-title"> <?php echo $logo; ?> </h3>
																	</div>
																	<!-- end panel-heading -->

																	<!-- panel-body -->
																	<div class="panel-body" >
																		<img src="<?php echo base_url().'images/products/'.$supplier_info['image']; ?>" data-src="holder.js/100%x100%" alt="">
																		
																	</div>
																	<!-- end panel-body -->

																</div>
																<!-- end panel -->
															</div>
															<!-- end col-md-12 -->
															
														</div>
														<!--End Row 3-->

													<!-- Cover Logo for Vendor Page -->
													<?php if($this->session->userdata('group_id') == 2){ ?>
														<div class="row">
															<!-- col-md-12 -->
															<div class="col-md-12">
																<!-- panel -->
																<div class="panel panel-default">
																	<div class="widget_section">
																		<div class="panel-heading">
																			<div class="widget_title"><h3 class="panel-title">Cover Image (Size should be 1150 x 280 pixels)</h3></div>
																		</div>
																		<div class="panel-body" >
																			<div class="gallery_">
																				<div class="fileinput fileinput-new" data-provides="fileinput">
																					<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																						<img src="<?php echo base_url().'images/products/'.$supplier_info['cover_image']; ?>" data-src="holder.js/100%x100%" alt="">
																						<input type="hidden" name="cover_image" value="<?php echo $supplier_info['cover_image']; ?>" id="image">
																					</div>
																					<div id="dvPreview"></div>
																					<div>
																						<span class="btn btn-default btn-file">
																						<span class="fileinput-new">Select image</span><span class="fileinput-exists">Select again</span>
																						<input id="fileupload" type="file" multiple="multiple" class="file" name="gallery[]">
																						</span>
																						<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>	
																<!-- end panel -->
															</div>
															<!-- end col-md-12 -->
														</div><!-- //End Cover Logo for Vendor Page -->
													<?php } ?>


														
													</div>
													<!-- End Action -->
											 </div>
											  <!-- End Row -->
											 




											</div>
										<!-- end portlet-body -->
									
								<?php echo form_close(); ?>
								
					

			<!-- end portlet -->
				
			</div>
	
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->
<script>
    var base_admin_assets_url = "<?php echo base_admin_assets_url(); ?>";
</script>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
