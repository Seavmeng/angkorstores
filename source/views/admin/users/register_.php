<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
<!-- - - - - - - - - - - - - - Main Wrapper - - - - - - - - - - - - - - - - -->
<div class="wide_layout">
	<!-- - - - - - - - - - - - - - End Header - - - - - - - - - - - - - - - - -->
    <?php $this->load->view(config_item('include_dir').'top_header'); ?>
	<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->
	<div class="page_wrapper">
		<div class="container">
			<!-- - - - - - - - - - - - - - Checkout method - - - - - - - - - - - - - - - - -->
			<section class="section_offset">
			 <?php echo form_open(account_url()."register/")?>
				<h3 class="offset_title">Login Or Create An Account</h3>
				<?php 
					if(isset($errors)){
					   echo msg($errors);
					}
					if(isset($success)){
						echo msg_suc($success);
					}
				?>
				<div class="relative">
					<a class="icon_btn button_dark_grey edit_button" href="#"><i class="icon-pencil"></i></a>
					<div class="table_layout">
						<div class="table_row">
							<div class="table_cell">
								Description

							</div><!--/ .table_cell -->
							<div class="table_cell">
								<ul>
									<li class="row">
										<div class="col-xs-12">
											<label for="input_1">Your Name :</label>
											<div class="form_el">
												<?php echo form_input('username', $this->input->post('username', true),"class = 'form-control' id= 'username'"); ?>
												<font color="red"><?php echo form_error('username');?></font>
											</div>
										</div>
									</li>
									<li class="row"><div class="col-xs-12">	&nbsp;</div></li>
									<li class="row">
										<div class="col-xs-12">
											<label for="input_1" class="required">Email :</label>
											<div class="form_el">
												<?php echo form_input('email', $this->input->post('email', true),"class = 'form-control' id= 'email'"); ?>
												<font color="red"><?php echo form_error('email');?></font>
											</div>
										</div>
									</li>
									<li class="row"><div class="col-xs-12">	&nbsp;</div></li>
									<li class="row">
										<div class="col-xs-12">
											<label for="input_1">Phone :</label>
											<div class="form_el">
												<?php echo form_input('phone', $this->input->post('phone', true),"class = 'form-control' id= 'username'"); ?>
												<font color="red"><?php echo form_error('phone');?></font>
											</div>
										</div>
									</li>
									<li class="row"><div class="col-xs-12">	&nbsp;</div></li>
									<li class="row">
										<div class="col-xs-12">
											<label for="input_1" class="required">Password :</label>
											<div class="form_el">
											  <input type="password" name="password" class="form-control" placeholder="Your Password." id="pass1"/>
												<font color="red"><?php echo form_error('password');?>
												<div id="passwordDescription">&nbsp;</div>
												<div id="passwordStrength" class="strength0"></div></span></font>
											</div>
										</div>
									</i>
									<li class="row"><div class="col-xs-12">	&nbsp;</div></li>
									<li class="row">
										<div class="col-xs-12">
											<label for="input_1" class="required">Confirm Password :</label>
											<div class="form_el">
											  <input type="password" class="form-control" name="password_confirm"  placeholder="Your Password Again." id="pass2" />
												<font color="red"><?php echo form_error('password_confirm');?></font>
												<span id="confirmMessage" class="confirmMessage"></span>
											</div>
										</div>
									</i>
									<li class="row"><div class="col-xs-12">	&nbsp;</div></li>
									<li class="row">
										<div class="col-xs-12">
										<label for="input_1" class="required">Fill captcha code :</label>
										<div class="form_el">
											<span class='captcha'>
											<?php echo $cap_img; ?>
											</span>
											<a href='javascript:void(0)' class ='refresh_cap'>
												<i id='ref_symbol' class="glyphicon glyphicon-refresh btn-lg"></i>
											</a>
											<input  type="text" name="captcha" value=""/>
											<font color="red"><?php echo form_error('captcha');?></font>
										</div>
										</div>
									</i>
										<li class="row">
										<div class="col-xs-12">
								<input type="submit" value="Register" name="btn_gu_submit" class="button_blue middle_btn" />
								</ul>
							</div><!--/ .table_cell -->
						</div><!--/ .table_row -->
					</div><!--/ .table_layout -->
				</div><!--/ .relative -->
			<?php echo form_close(); ?>
			</section><!--/ .section_offset -->
			<!-- - - - - - - - - - - - - - End of checkout method - - - - - - - - - - - - - - - - -->
		</div>
	</div><!--/ .page_wrapper-->
	<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
	<!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->
    <?php $this->load->view(config_item('include_dir').'footer-sector'); ?>
	<!-- - - - - - - - - - - - - - End Footer - - - - - - - - - - - - - - - - -->
</div><!--/ [layout]-->
<!-- - - - - - - - - - - - - - End Main Wrapper - - - - - - - - - - - - - - - - -->	
<?php $this->load->view(config_item('include_dir').'footer'); ?>
</body>
</html>