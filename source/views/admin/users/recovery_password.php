<?php $this->load->view('includes/head');?>
<body class="page home page-template-default">
	<div id="page" class="hfeed site">
		<?php $this->load->view('includes/header1');?>
		<div id="content" class="site-content" tabindex="-1">
			<div class="container">
				<nav class="woocommerce-breadcrumb" >
					<a href="home.html">Home</a>
					<span class="delimiter"><i class="fa fa-angle-right"></i></span>
					My Account
				</nav><!-- .woocommerce-breadcrumb -->
				<?php 
				if(isset($errors)){
				   echo show_msg($errors);
				}
				?>
				<?php 
									$Reset = $this->uri->segment(4);
									
								 ?>
				
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<article id="post-8" class="hentry">
							<div class="entry-content">
								<div class="woocommerce">
									<div class="customer-login-form">
									<?php echo $this->session->flashdata('msg');?>
										<h2>Recovery Password</h2>
										<?php echo form_open(base_url()."admin/register/recov/".$Reset, array('role' => 'form','class' => 'form-horizontal'))?>
										<div class="page-content">
											<div class="row">
												<div class="col-sm-8">
													<!-- normal control -->								
													<div class="form-group">
														<label class="col-md-3 control-label">Email</label>
														<div class="col-md-9">
															<input type="email" name="email" class="form-control" placeholder="Your email" required />														
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label">New password</label>
														<div class="col-md-9">
															<input type="password" name="password" class="form-control" placeholder="Your Password." autocomplete="off" id="pass1" required />
															<font color="red"><?php echo form_error('password');?>
															<div id="passwordDescription">&nbsp;</div>
															<div id="passwordStrength" class="strength0"></div></span></font>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label">New password_confirm</label>
														<div class="col-md-9">
															<input type="password" class="form-control" name="password_confirm"  placeholder="Your Password Again." autocomplete="off" id="pass2" required />
															<font color="red"><?php echo form_error('password_confirm');?></font>
															<span id="confirmMessage" class="confirmMessage"></span>
														</div>
													</div>
													
													<div class="form-group">
														<div class="col-md-6">
															<input type="submit" value="Submit" name="btn_gu_submit" class="btn btn-primary" />
														</div>
													</div>
												</div>
											</div>
										</div>
										<?php echo form_close(); ?>
										
									</div><!-- /.customer-login-form -->
								</div><!-- .woocommerce -->
							</div><!-- .entry-content -->

						</article><!-- #post-## -->

					</main><!-- #main -->
				</div><!-- #primary -->


			</div><!-- .col-full -->
		</div><!-- #content -->
<?php $this->load->view('includes/sub_footer');?>

	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>
</body>
</html>
