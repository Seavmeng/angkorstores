<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $view_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->

						<!-- portlet-body -->
						<div class="portlet-body">

							<div class="row">
								<div class="col-md-12 col-sm-12">
									<?php echo form_open(base_admin_url().$controller_url.'search', array('role' => 'form','method'=>'get')); ?>
									
										<div class="form-group col-md-3">
												<?php echo form_label(word_r('user_group_name'), 'user_group_name',array('class' => 'control-label')); ?>
												<?php echo form_input(array('name' => 'user_group_name', 'class' => 'form-control', 'id' => 'user_group_name',  'value' => $this->input->get('user_group_name') ) ); ?>											
										</div>
										<div class="form-group col-md-3">
												<?php echo form_label(word_r('active'), 'active',array('class' => 'control-label')); ?>
												<?php echo $option_search; ?>																
										</div>
										<div class="form-group col-md-2">
												<label for="" class="control-label">&nbsp;</label>
												<input type="submit" class="btn btn blue form-control" value="<?php word('search'); ?>">											
										</div>
										
									<?php echo form_close(); ?>
								</div>
							</div>
							

							<?php
								echo form_open(base_admin_url().$controller_url.'ml', array('role' => 'form')); 

								$this->table->set_heading($check_ml,word_r('user_group_name'),word_r('permission'),word_r('active'),word_r('action'));
								foreach($data as $value){
									$id = $value['user_group_id'];
									$user_group_name = $value['user_group_name'];
									$active = $value['user_group_active'];
									$this->table->add_row(
															form_checkbox(array('name'=>'id[]','value'=>$id,'class'=>'checkboxes')),
															"$user_group_name",
															anchor(base_admin_url().$controller_url."permission/$id" ,"<i class='fa fa-key'></i>",array('class' => 'btn btn-sm green', 'title'=>word_r('permission'))),
															check_active($active),
															btn_action($controller_url.'update/'.$id, $controller_url.'delete/'.$id)
														);
								}

								echo $this->table->generate();
								
							 ?>
							 <div class="row">

								<div class="col-md-6 col-sm-12">
									<div class="col-sm-12">
										<div class="col-sm-7 dataTables_info" id="sample_1_info" role="status" aria-live="polite">
											<?php echo $showing; ?>
										</div>

										<div class="option col-sm-5">
												<?php echo $option; ?>

										</div>
										
									</div>
								</div>
								<?php echo form_close(); ?>
								<div class="col-md-6 col-sm-12">
									 <div class="dataTables_paginate paging_bootstrap_full_number">
									 	<?php echo $page; ?>
									 </div>
						             
								</div>
							</div>
						</div>
						<!-- end portlet-body -->
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	<script>
			$(document).ready(function(){
				$(".sort-table").tablesorter({
					 headers: { 			                   
			                    0: { 
			                        sorter: false 
			                    },  
			                    2: { 
			                        sorter: false 
			                    },
			                    
			                    4: { 
			                        sorter: false 
			                    },
			            }
				}); 
			});
		</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	