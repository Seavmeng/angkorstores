<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $update_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open(base_admin_url().$controller_url.'permission_update/'.$this->uri->segment(5), array('role' => 'form') ); ?>
					
						
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
							         <!--Form-body -->
										<div class="form-body">

											<?php echo form_hidden('permission_type', 'menu'); ?>
											<?php echo show_sys_menu_check_box($menu_id); ?>



										</div>
										<!--End form-body -->
											

										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
											
														<div class="form form-actions">
																<?php echo form_submit('update', word_r('update'), "class = 'btn blue' "); ?>
																<?php echo '<a href="'.base_admin_url().$controller_url.'" class="btn default">'.word_r('cancel').'</a>'; ?>
														</div>
												
											</div>
										</div>
										<!-- End row -->

									</div>
								<!-- end portlet-body -->


							
						<?php echo form_close(); ?>

						<?php echo form_open(base_admin_url().$controller_url.'permission_update/'.$this->uri->segment(5), array('role' => 'form') ); ?>
							<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
							         <!--Form-body -->
										<div class="form-body">
											<?php echo form_hidden('permission_type', 'media'); ?>


											<h4>Media</h4>
											<?php $group_id = $this->uri->segment(5); ?>
											<div>
												<ul class="ul-category">
													<li>
														<label> <input type="checkbox" name="use" <?php media_checked($group_id,'use'); ?> value="1" > <?php word('use'); ?></label>
													

													
														<label> <input type="checkbox" name="upload" <?php media_checked($group_id,'upload'); ?> value="1"> <?php word('upload'); ?></label>
													

													
														<label> <input type="checkbox" name="copy" <?php media_checked($group_id,'copy'); ?> value="1"> <?php word('copy'); ?></label>
													

													
														<label><input type="checkbox" name="move" <?php media_checked($group_id,'move'); ?> value="1"> <?php word('move'); ?> </label>
													

													
														<label> <input type="checkbox" name="rename" <?php media_checked($group_id,'rename'); ?> value="1"> <?php word('rename'); ?></label>
														

														<label> <input type="checkbox" name="delete" <?php media_checked($group_id,'delete'); ?> value="1"> <?php word('delete'); ?></label>
													

													
														<label> <input type="checkbox" name="create_dir" <?php media_checked($group_id,'create_dir'); ?> value="1"> <?php word('create_dir'); ?></label>
													

													
														<label><input type="checkbox" name="rename_dir" <?php media_checked($group_id,'rename_dir'); ?> value="1"> <?php word('rename_dir'); ?> </label>
													

													
														<label> <input type="checkbox" name="delete_dir" <?php media_checked($group_id,'delete_dir'); ?> value="1"> <?php word('delete_dir'); ?></label>
													
													</li>
												</ul>
												<?php //var_dump($this->session->userdata('ADMIN_KCFINDER')); ?>
											</div>


										</div>
										<!--End form-body -->
											

										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
											
														<div class="form form-actions">
																<?php echo form_submit('update', word_r('update'), "class = 'btn blue' "); ?>
																<?php echo '<a href="'.base_admin_url().$controller_url.'" class="btn default">'.word_r('cancel').'</a>'; ?>
														</div>
												
											</div>
										</div>
										<!-- End row -->

									</div>
								<!-- end portlet-body -->
								<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
