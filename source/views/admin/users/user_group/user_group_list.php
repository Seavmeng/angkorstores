<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $view_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->

						<!-- portlet-body -->
						<div class="portlet-body">
						  <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
								<?php 
									
									$this->table->set_heading(word_r('user_group'),word_r('permission'), word_r('active'), word_r('action'));
									$i =0;
									foreach($user_groups as $value){
										$user_group = $value['user_group_name'];
										$id = $value['user_group_id'];
										$active = $value['user_group_active'];
										$this->table->add_row(
																"$user_group",
																anchor(base_admin_url()."user/user_group/permission/$id" ,"<i class='entypo-key'></i>",array('class' => 'btn btn-xs btn-orange', 'title'=>word_r('permission'))),
																check_active($active),
																btn_action('user/user_group/update/'.$id, 'user/user_group/delete/'.$id)
															);
									}

									echo $this->table->generate();
									
								 ?>
											 <div class="row">
								<div class="col-md-3 col-sm-12">
									<div class="dataTables_info" id="sample_1_info" role="status" aria-live="polite"><?php echo $this->mydb->showing(); ?></div>
								</div>
								<div class="col-md-9 col-sm-12">
									 <div class="dataTables_paginate paging_bootstrap_full_number">
									 	<?php echo $this->mydb->page(); ?>
									 </div>
						             
								</div>
							</div>

							</div>
						</div>
						<!-- end portlet-body -->
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	<script>
			$(document).ready(function(){
				$(".sort-table").tablesorter({
			            headers: { 			                   
			                    0: { 
			                        sorter: false 
			                    },
			                    2: { 
			                        sorter: false 
			                    },
			                    4: { 
			                        sorter: false 
			                    }
			            }

          }); 

			});
		</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	