<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $update_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open(base_admin_url().$controller_url.$this->uri->segment(4).'/'.$data['user_group_id'], array('role' => 'form') ); ?>
					
						
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
							         <!--Form-body -->
										<div class="form-body">


											<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group required <?php has_error('user_group_name'); ?>">
															<?php echo form_label(word_r('user_group_name'),'user_group_name',array('class' => 'control-label') ); ?>
															<?php echo form_input('user_group_name', $data['user_group_name'],"class = 'form-control', id= 'user_group_name'"); ?>
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->

												<!-- ROW -->
											<div class="row">
												<!-- col-md-6 -->
												<div class="col-md-6">
														<div class="form-group">
															<?php echo form_label(word_r('active'),'user_group_active',array('class' => 'control-label'));; ?>
															<div class="check-list">
																<?php echo '<label>'.form_checkbox('user_group_active',1, $data['user_group_active'] == 1? true:false).word_r('active').'</label>'; ?>
															</div>
														</div>
												</div>
												<!--End col-md-6 -->

											</div>
											<!-- End row -->
<!-- ROW -->



										</div>
										<!--End form-body -->
											

										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
											
														<div class="form form-actions">
																<?php echo form_submit('update', word_r('update'), "class = 'btn blue' "); ?>
																<?php echo '<a href="'.base_admin_url().$controller_url.'" class="btn default">'.word_r('cancel').'</a>'; ?>
														</div>
												
											</div>
										</div>
										<!-- End row -->

									</div>
								<!-- end portlet-body -->
							
						<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	