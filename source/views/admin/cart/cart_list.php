<?php $this->load->view('includes/head')?>
<body  class="home option3">
	<div id="header" class="header">
		<?php $this->load->view('includes/header')?>
		<!-- page wapper-->
		<div class="columns-container">
			<div class="container" id="columns">
				<h2 class="page-heading no-line">
					<span class="page-heading-title2"><?php echo word_r('shopping cart');?></span>
				</h2>
				<!-- ../page heading-->
				<?php echo form_open(base_admin_url().'cart/payment')?>
					<div class="page-content page-order">
						<ul class="step">
							<li class="current-step active">
								<a href="#Summary" data-toggle="tab"><span>01. <?php echo word_r('Summary');?></span></a>
							</li>
							<li class="sign">
								<a href="#Sign" data-toggle="tab"><span>02. <?php echo word_r('signin');?></span></a>
							</li>
							<?php
								if($user_id = has_logged()){
							?>
							<li class="payment">
								<a href="#Payment" data-toggle="tab"><span>03. <?php echo word_r('payment');?></span></a>
							</li>
							<?php
								}else{
							?>
							<li class="Payment" id="pay">
								<a data-toggle="tab"><span>03. <?php echo word_r('payment');?></span></a>
							</li>
							<?php
								}
							?>
						</ul>
						<div class="tab-content">
							<div id="Summary" class="tab-pane fade in active">
								<div class="heading-counter warning"><?php echo word_r('Your shopping');?>:
									<span>
									<?php 
										$count = count($this->cart->contents());
										echo $count;
									?> <?php echo word_r('product');?></span>
								</div>
								<div class="order-detail-content">
									<table class="table table-bordered table-responsive cart_summary">
										<thead>
											<tr>
												<th class="cart_product"><?php echo word_r('product');?></th>
												<th><?php echo word_r('Description');?></th>
												<th>Avail.</th>
												<th><?php echo word_r('Unit price');?></th>
												<th><?php echo word_r('qty');?></th>
												<th><?php echo word_r('Total');?></th>
												<th class="action" onClick="clear_cart()"><i class="fa fa-trash-o"></i></th>
											</tr>
										</thead>
										<tbody>
										<?php  
										$cart = $this->cart->contents();
											$total = '';
											$grand_total = '';
											foreach ($cart as $item):
												$product_id = $item['id'];
												$row        = get_pro_field($product_id);
												$image 		= cut_string($row['feature']);
												$dir 		= date('m-Y', strtotime($row['created_date']));
												$name 		= $row['product_name'];
												$code 		= $row['product_code'];
												$get_data	= $row['attribute'];
												$get_size	= $row['attribute_size'];
												$avail		= $row['available'];
												if($avail == 5){
													$avail = 'In Stock';
												}else if($avail == 6){
													$avail = 'Out Of Stock';
												}else{
													$avail = 'N/A';
												}
												$price 		= $row['sale_price'];
												$stock 		= $row['stock_qty']; 
										?>
											<tr>
												<td class="cart_product">
													<a href="#"><img src="<?php echo base_url().'images/products/'.$image;?>" alt="Product"></a>
												</td>
												<td class="cart_description">
													<p class="product-name"><a href="#"><?php echo $name;?> </a></p>
													<input type="hidden" value="<?php echo $name;?>" name="pro_name[]"/>
													<small class="cart_ref">Product Code : <?php echo $code;?></small><br>
													<?php
													if($item['pcolor'] == ""){
														
													}else{
													?>
													<small><a href="#">Color : 
														<div style="position:absolute; z-index:1;margin-left:40px;margin-top:-22px;">
															<img class="list-color" src="<?php echo base_url().'images/products/'.cut_string($item['pcolor']);?>"/>
															<input type="hidden" value="<?php echo $item['pcolor'];?>" name="color[]"/>
														</div>
													</a></small><br> 
													<?php
													}
													if($item['size'] == ""){
														
													}else{
													?>
													<small><a href="#">Size : 
														<?php
															echo '<input type="hidden" value="'.$item['size'].'" name="size[]"/>';
														?>
													</a></small>
													<?php
													}
													?>
												</td>
												<td class="cart_avail">
													<span class="label label-success">
														<?php echo $avail;?>
													</span>
												</td>
												<td class="price">$<span name="sale_price[]" class="sale_price"><?php echo $price;?></span>
												<input type="hidden" value="<?php echo $price;?>" name="price[]">
												</td>
												<td class="qty">
													<input class="form-control input-sm" type="text" value="<?php echo $item['stock'];?>" name="qty[]" id="qty" onkeypress="return isNumberKey(event)">
												</td>
												<td class="price">
													$<span class="subtotal">
														<?php 
															$grand_subtotal = $item['stock'] * $item['subtotal'];
															$grand_total = $grand_total + $grand_subtotal;
															echo number_format($grand_subtotal, 2);
														?>
													</span>
													<input type="hidden" value="<?php echo $grand_subtotal;?>" name="subtotal[]"/>
												</td>
												<td class="action">
													<?php 
													// cancle image.
													$path = '<img src="<?php echo base_url(); ?>/images/cart_cross.jpg" width="25px" height="20px">';
													echo anchor('cart/remove/' . $item['rowid'], 'delete'); ?>
												</td>
											</tr>
											<input type="hidden" value="<?php echo $product_id;?>" name="id_pro[]"/>
										<?php endforeach;?>
											
										</tbody>
										<tfoot>
											<tr>
												<td colspan="2" rowspan="2"></td>
												<td colspan="3"><strong><?php echo word_r('Total');?></strong></td>
												<td colspan="2">
													<strong id="total">
														$<?php 
															//$grand_total = $grand_total + $total; 
															if($grand_total == ""){
																
															}else{
																echo number_format($grand_total, 2);
															}
														?>
													</strong>
													<input type="hidden" value="<?php echo $grand_total;?>" name="total[]"/>
												</td>
											</tr>
										</tfoot>    
									</table>
									<div class="cart_navigation">
										<a class="prev-btn" href="<?php echo base_url().'index.php';?>"><?php echo word_r('Continue shopping');?></a>
										<span class="timer" data-minutes-left=3></span>
										<a class="next-btn" href="#Sign" data-toggle="tab"><?php echo word_r('checkout');?></a>
									</div>
								</div>
							</div>
							<div id="Sign" class="tab-pane fade">
								<?php
									if($user_id = has_logged()){
										echo '<span style="text-align:center;margin: 30px auto;display: block;">'.word_r("you login").'</span>';
									}else{
								?>
									<a href="<?php echo base_url().'index.php/account';?>" class="btn btn-default go_to"><?php echo word_r('go signin');?></a>
								<?php
									}
								?>
								<div class="cart_navigation">
									<a class="prev-btn" href="#Summary" data-toggle="tab"><?php echo word_r('Back to Summary');?></a>
									<?php
										if($user_id = has_logged()){
									?>
									<a class="next-btn" href="#Payment" data-toggle="tab"><?php echo word_r('checkout');?></a>
									<?php
										}else{
									?>
									<a class="next-btn" href="" data-toggle="tab"><?php echo word_r('checkout');?></a>
									<?php
										}
									?>
								</div>
							</div>
							<div id="Payment" class="tab-pane fade">	
								<div class="heading-counter warning">
									<ul class="pay-pro">
										<li>
											<label for="radio_button_5">
												<input type="radio" checked name="bank" id="bank" value="bank"> Bank Transfer
											</label>
											<div class="panel panel-default bank">
												<div class="panel-body">
													Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.
												</div>
											</div>
										</li>

										<li>
											<label for="radio_button_6">
												<input type="radio" name="bank" id="paypal" value="paypal"> PayPal
												<img src="<?php echo base_url().'images/paypal.png'?>">
												<span style="color:red;">What is PayPal?</span>
											</label>
											<div class="panel panel-default paypal">
												<div class="panel-body">
													Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.
												</div>
											</div>
										</li>
										
										<li>
											<label for="radio_button_6">
												<input type="radio" name="bank" value="wing" id="wing"> Wing Payment
											</label>
											<div class="panel panel-default wing">
												<div class="panel-body">
													Pay on Wing
												</div>
											</div>
										</li>
									
									</ul>
								</div>
								<div class="cart_navigation">
									<a class="prev-btn" href="#Sign" data-toggle="tab"><?php echo word_r('Continue shopping');?></a>
									<?php 
										$value = word_r('checkout');
										$btn = array(
											'class' => 'next-btn',
											'value' => $value.' >',
											'name' => 'btn_pay'
										);
									echo form_submit($btn);
									?>
									<!--<a class="next-btn" href="#" data-toggle="tab">Proceed to checkout</a>-->
								</div>
							</div>
						</div>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
		<!-- ./page wapper-->
	</div>
	<div class="container">
		<div class="tabs">
			<ul class="tab-links">
				<li class="active"><a href="#tab1"><?php echo word_r('hot');?></a></li>
				<li><a href="#tab2"><?php echo word_r('view');?></a></li>
				<li><a href="#tab3"><?php echo word_r('may_like');?></a></li>
			</ul>
		 
			<div class="tab-content">
				<div id="tab1" class="tab active">
					<div class="galerie">
						<div class="slider">
							<?php
								foreach($hotp as $values){
									$himg = cut_string($values['feature']);
									$hsale = $values['sale_price'];
									$hold = $values['regular_prices'];
									$hname = $values['product_name'];
									$hpid = $values['view_pro_id'];
									$hid[] = $hpid;
									$harr = implode(',',$hid);
									$hdir = date('m-Y', strtotime($values['created_date']));
							?>
							<div class="main">
								 <div class="post-thumb image-hover2">
									<a href="<?php echo base_url().'product-detail/'.$hpid;?>"><img src="<?php echo base_url().'images/products/'.$himg;?>" alt="Blog"></a>
								</div>
								<div class="post-desc">
									<h5 class="post-title">
										<a href="<?php echo base_url().'product-detail/'.$hpid;?>">
											<?php echo $hname;?>
										</a>
									</h5>
								</div>
							</div>
							<?php
								}
							?>
						</div>
						<?php
							$num = count($hotp);
							if($num == 6){
								
							}else{
						?>
						<div class="suiv">></div>
						<div class="prec"><</div>
						<?php
							}
						?>
					</div>
				</div>
		 
				<div id="tab2" class="tab">
					<div class="galerie">
						<div class="slider">
							<?php
								foreach($views as $value){
									$vimg = cut_string($value['feature']);
									$vsale = $value['sale_price'];
									$vold = $value['regular_prices'];
									$vname = $value['product_name'];
									$vpid = $value['view_pro_id'];
									$gid[] = $vpid;
									$varr = implode(',',$gid);
									$vdir = date('m-Y', strtotime($value['created_date']));
							?>
							<div class="main">
								<div class="post-thumb image-hover2">
									<a href="<?php echo base_url().'product-detail/'.$vpid;?>"><img src="<?php echo base_url().'images/products/'.$vimg;?>" alt="Blog"></a>
								</div>
								<div class="post-desc">
									<h5 class="post-title">
										<a href="<?php echo base_url().'product-detail/'.$vpid;?>"><?php echo $vname;?></a>
									</h5>
									<!--
									<div class="content_price">
										<span class="price product-price">
											$<?php echo $vsale;?>
										</span>
										<span class="price old-price">
											$<?php echo $vold;?>
										</span>
									</div>
									-->
								</div>
							</div>
							
							<?php
								}
							?>
						</div>
						<?php
							$nums = count($views);
							if($nums == 6){
								
							}else{
						?>
						<div class="suiv">></div>
						<div class="prec"><</div>
						<?php
							}
						?>
					</div>
				</div>
		 
				<div id="tab3" class="tab">
					<div class="galerie">
						<div class="slider">
							<?php
								$earr = explode(',',$varr);
								$cate_id = guess_u_like($earr);
								foreach($cate_id as $idcate){
									$idc[] = $idcate['category_id'];
									$id_cate = implode(',',$idc);
									$garr = explode(',',$id_cate);
									$get_guess = guess_product($garr);
								}
								foreach($get_guess as $guess){
									$gimg = cut_string($guess['feature']);
									$gsale = $guess['sale_price'];
									$gold = $guess['regular_prices'];
									$gname = $guess['product_name'];
									$gpid = $guess['product_id'];
									$gdir = date('m-Y', strtotime($guess['created_date']));
							?>
							<div class="main">
								<div class="post-thumb image-hover2">
									<a href="<?php echo base_url().'product-detail/'.$gpid;?>"><img src="<?php echo base_url().'images/products/'.$gimg;?>" alt="Blog"></a>
								</div>
								<div class="post-desc">
									<h5 class="post-title">
										<a href="<?php echo base_url().'product-detail/'.$gpid;?>"><?php echo $gname;?></a>
									</h5>
									<!--
									<div class="content_price">
										<span class="price product-price">$38,95</span>
										<span class="price old-price">$52,00</span>
									</div>
									-->
								</div>
							</div>
							<?php
								}
							?>
						</div>
						<?php
							$numv = count($get_guess);
							if($numv == 6){
								
							}else{
						?>
						<div class="suiv">></div>
						<div class="prec"><</div>
						<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('includes/footer')?>
	<script type="text/javascript">
		$('.page-order li').on('click', function(){
			$('li.active').removeClass('current-step');
			$(this).addClass('current-step');
		});
		
		$('.suiv').hide();
		$('.prec').hide();
		
		$('.galerie').mouseover(function(){
			$('.suiv').show();
			$('.prec').show();
		});
		$('.galerie').mouseout(function(){
			$('.suiv').hide();
			$('.prec').hide();
		});
		
		$('.paypal').hide();
		$('.wing').hide();
		
		$('#bank').click(function(){
			$( ".bank" ).slideDown( "slow" );
			$( ".paypal" ).slideUp( "slow" );
			$( ".wing" ).slideUp( "slow" );			
		});
		$('#paypal').click(function(){
			$( ".paypal" ).slideDown( "slow" );
			$( ".bank" ).slideUp( "slow" );
			$( ".wing" ).slideUp( "slow" );
		});
		$('#wing').click(function(){
			$( ".wing" ).slideDown( "slow" );
			$( ".bank" ).slideUp( "slow" );
			$( ".paypal" ).slideUp( "slow" );
		});
		// To conform clear all data in cart.
		function clear_cart() {
			var result = confirm('Are you sure want to clear all bookings?');

			if (result) {
				window.location = "<?php echo base_url(); ?>index.php/cart/remove/all";
			} else {
				return false; // cancel button
			}
		}
		
		function isNumberKey(evt) {
			var charCode = (evt.which) ? evt.which : event.keyCode;
			if (charCode != 46 && charCode > 31
			&& (charCode < 48 || charCode > 57))
				return false;

			return true;
		}
		/*$(document).on('keyup','.input-sm', function(){
			//var price = $('.sale_price').text();
			var price = $('.price').closest('span').find('.sale_price').val();
			//$(this).closest('.sale_price').text();
			alert(price);
		});*/
		/*$('.input-sm').keyup(function(){
			var result = "";
			//var price = $('.sale_price').html();
			var qty = $(this).val();
			$('span.sale_price').click(function() {
				var text = $(this).text();
				// do something with the text
				result = text * qty;
				$('span.subtotal').click(function(){
					$(this).text(result);
				});
			});
			//result = price * qty;
			//alert(price);
			//document.getElementsByClassName("subtotal")[0].innerHTML = result;
		});*/
		jQuery(document).ready(function() {
			jQuery('.tabs .tab-links a').on('click', function(e)  {
				var currentAttrValue = jQuery(this).attr('href');
		 
				// simple slide
				//jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
				
				// fade slide
				//jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();
				
				// simple slide up down
				//jQuery('.tabs ' + currentAttrValue).siblings().slideUp(400);
				//jQuery('.tabs ' + currentAttrValue).delay(400).slideDown(400);
				
				// slide up down
				jQuery('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400);
				// Change/remove current tab to active
				jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
		 
				e.preventDefault();
			});
		});
	</script>
	<?php
		if($buy == 'test1'){
	?>
		<script type="text/javascript">
			//alert('he');
			
			function activaTab(tab){
				$('.step a[href="#' + tab + '"]').tab('show');
				$('li.sign').addClass('current-step');
				$('li.payment').addClass('current-step');
			};
			activaTab('Payment');
		</script>
	<?php
		}
	?>
	<script src="<?php echo base_url();?>js/script.js"></script>
</body>
</html>
