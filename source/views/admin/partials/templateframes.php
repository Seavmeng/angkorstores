<?php foreach( $pages as $key => $frames ):?>

<?php
	
	$frameIDs = array();
	$indivHeights = array();
	$totalHeight = 0;
	
	foreach( $frames as $frame ) {
		
		//frame ID's
		$frameIDs[] = $frame['id'];
		
		
		//total height
		$totalHeight += $frame['height'];
		
		
		//individual heights
		$indivHeights[] = $frame['height'];
		
		
		//original Urls
		$urls[] = $frame['original_url'];
			
	}
	
	$pageName = $frames[0]['pageName'];
	$pageID = $frames[0]['pageID'];
	
	$frameIDstring = implode("-", $frameIDs);
	
	$indivHeightsString = implode("-", $indivHeights);
	
	$originalUrlsString = implode("-", $urls);
	
	
?>

<?php endforeach;?>