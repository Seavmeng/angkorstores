<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-8">
				<h3><span class="fui-document"></span>My Orders</h3>
			</div><!-- /.col -->
			<div class="col-md-3 col-sm-4 text-right"><h3><?php //echo $success; ?></h3></div><!-- /.col -->
		</div><!-- /.row -->
		<?php 
			if(isset($errors)){
			   echo show_msg($errors);
			}
			if(isset($rmsg)){
			   echo show_msg($rmsg);
			}
		//	$catFilter = catForFilter();
		
		 ?>
		<hr class="dashed margin-bottom-30">
		<div class="panel panel-default">
			<table class="table" id="product-list">
				<tr>
					<!-- <td>
						<?php echo form_dropdown(); ?>
					</td> -->
					
					<td style="border-bottom: 1px solid #ebebeb;">
						<form action="order/find_id" method="post" class="search">
						<?php if ($this->session->userdata('user_id') == 1) {?>
							<?php if($this->uri->segment(3) != 'find_id'){ ?>
							<input type="text" placeholder="Order Id" class="searchBox " id="searchBox" name="find_id"> </input>
							<input type="submit" value="Search" class="btnInput" id="btnInput"> </input>
						<?php }else{?>

							<a class="btn btn-default btn-labeled fa fa-reply pull-left mar-rgt category" href="<?php echo  base_url().'admin/order';?>">
								Back                              
							</a>
						<?php	}} ?>
						</form>
						
					</td>
				</tr>
				<tr>
					<td>
						<div class="panel panel-default">
							<table data-show-export="true" data-search="true" data-show-refresh="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-pagination="true" data-side-pagination="server" data-url="#" class="table table-striped table-bordered table-hover" id="events-table">
								<thead>
									<tr>
										<th style="text-align: center;">N</th>
										<th style="text-align: center;">Order</th>
										<th style="text-align: center; ">Bill</th>
										<th style="text-align: center; ">Product Name</th>
										<th style="text-align: center; ">Price</th>
										<th style="text-align: center; ">Options</th>
									</tr>
								</thead>
								
								<tbody>
									<?php
										$i=0;
										foreach($billing as $rows){
											//var_dump($rows);exit();
											$order_id = $rows['order_id'];
											$product_name = $rows['product_name'];
											$bill_id = $rows['bill_id'];
											$price = $rows['prices'];
											$id = $rows['user_id'];
											//$dir = date('m-Y', strtotime($rows['created_date']));
											$i++;
									?>
									<tr>
										<td style="text-align: left; "><?php echo $i;?></td>
										<td style="text-align: center; "><?php echo $order_id;?></td>
										<td style="text-align: center; "><?php echo $bill_id;?></td>
										<td style="text-align: left; "><a href="<?php echo  base_url().'admin/order/detail_order/'.$order_id;?>"><?php echo $product_name;?></a></td>
										<!-- <td style="text-align: left; "><?php echo $last_name;?></td> -->
										<td style="text-align: left; "><?php echo "$" .$price;?></td>
										<td style="text-align: center; ">  
											<a class="btn btn-default btn-labeled fa fa-eye pull-right mar-rgt category" data-toggle="modal"  href="<?php echo  base_url().'admin/order/detail_order/'.$order_id;?>">
													Detail                              
											</a>
											
										</td>
									
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
						</div>
					</td>
					
				</tr>
				<tr>
					<td>
						<div class="pull-right">
							<div class="title_page">
								<!-- <?php echo $showing;?> -->
							</div>
							<!-- <?php echo $page;?> -->
						</div>
					</td>
				</tr>
			</table>
	</div>
	</div>
	</body>


			<!---Contain--->	
	
	<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
function show_data(){
 $('.show_content').load('test');
 // $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
$(".send_msg").on('click',function(){
	var send_msg=$(this).attr('id');
	var name=$('#msg_name').val();
	var email=$('#msg_email').val();
	var phone= $('#msg_phnoe').val();
	var subject= $('#msg_subject').val();
	var message= $('#msg_message').val();
	$.ajax({
		url: "http://localhost:81/ci3/admin/dashboard/message",
		type: 'POST',
		data: {
			'name':name,
			'email':email,
			'phone':phone,
			'subject':subject,
			'message':message
		},
		success: function(msg) {
			if(msg == 'YES'){
				alert('yes');
			}else if(msg == 'No'){
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}else{
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
		}
	});
});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>

</html>