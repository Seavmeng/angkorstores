
<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-8">
				<h3><span class="fui-document"></span>My Orders</h3>
			</div><!-- /.col -->
			<div class="col-md-3 col-sm-4 text-right"><h3><?php //echo $success; ?></h3></div><!-- /.col -->
		</div><!-- /.row -->
		<?php 
			if(isset($errors)){
			   echo show_msg($errors);
			}
			if(isset($rmsg)){
			   echo show_msg($rmsg);
			}
		//	$catFilter = catForFilter();
		
		 ?>
		<hr class="dashed margin-bottom-30">
		<div class="panel panel-default">
			<table class="table" id="product-list">
				<tr>
					<!-- <td>
						<?php echo form_dropdown(); ?>
					</td> -->

					
					<td style="border-bottom: 1px solid #ebebeb;">
						<a class="btn btn-primary btn-labeled fa fa-plus-circle pull-right mar-rgt category" data-toggle="modal" href="<?php echo  base_url().'admin/product_list/create'?>">
							Create Product                                
						</a>
					</td>
				</tr>
				<tr>
					<td>
						<div class="panel panel-default">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<center><h3 class="entry-title" itemprop="name">Order Received</h3></center>
									</div>
									<!-- left side -->
									<?php 
													
											foreach ($billing as $row) {
												$product_name = $row['product_name'];
												$price = $row['prices'];
												$qty = $row['qty'];
												$order_id = $row['order_id'];
												$bill_id = $row['bill_id'];
												$fullname = $row['first_name'].' '.$row['last_name'];
												$company = $row['company_name'];
												$email = $row['email'];
												$phone = $row['phone'];
												$shipping = $row['total_shipping'];
												$subtotal = $row['prices']*$row['qty'];
												$total = $shipping + $subtotal;														
									?>	
									<div class="col-md-12">
									<div class="col-md-8">
										<ul class="woocommerce-thankyou-order-details order_details">
											<li class="order">
												Order Number: <strong><?php echo $order_id; ?></strong>
											</li>
											<li class="date">
												Billind Id: <strong><?php echo $bill_id; ?></strong>
											</li>
											<li class="order">
												Name: <strong><?php echo $fullname; ?></strong>
											</li>
											<li class="date">
												Company: <strong><?php echo $company; ?></strong>
											</li>
											<li class="date">
												Email: <strong><?php echo $email; ?></strong>
											</li>
											<li class="order">
												Phone: <strong><?php echo $phone; ?></strong>
											</li>
										</ul>
									</div>
									<div class="col-md-4">
									<form action="<?php echo base_url().'admin/order/send_mail'?>" method="post">
										<ul class="woocommerce-thankyou-order-details order_details">
										 <li>
											<label id="i">Title </label>
											<select class="selectpicker form-control" name="title[]" style="width:200px;">
											<?php foreach ($post as $value) {?>											
												<option style="width: 180px;" value="<?php echo $value['post_title']; ?>"><?php echo $value['post_title']; ?></option>
											<?php } ?>
											</select>
											<!-- <input type="text" name="description" value="<?php echo $value['description']; ?>"> -->
											<input type="hidden" name="email" value="<?php echo $email; ?>">

										 </li><br/>
										 
											<!-- <a class="btn btn-default btn-labeled fa fa-paper-plane mar-rgt category" data-toggle="modal"  href="<?php echo  base_url().'admin/order/send_mail/';?>">											Send                              
											</a> -->
											<input type="submit" name="btn_save" value="Send" class="btn btn-default btn-labeled fa fa-paper-plane mar-rgt category">
										 
										</ul>
										</form>
									</div>	
									</div>
									<div class="clear"></div>
									<div class="col-md-12""><br/>
										<h4>Order Details</h4>							
										
										<table class="table table-hover shop_table order_details">
												<thead>
													<tr>
														<th class="product-name">Product Name</th>
														<th class="qty">Qty</th>
														<th class="price">Price</th>
														<th class="product-total">Total</th>
													</tr>
												</thead>
												<tbody>		
																							
														<tr class="order_item">
															<td class="product-name">
																<strong class="product-quantity" style="color: gray;"><?php echo $product_name;?></strong>
															</td>
															<td class="product-name">
																<span class="woocommerce-Price-currencySymbol"><?php echo $qty; ?></span>
															</td>
															<td class="product-total" style="color: gray;">
																<span class="woocommerce-Price-currencySymbol">&#36; <?php echo $price; ?></span>	
															</td>
															<td class="product-total">
																<span class="woocommerce-Price-currencySymbol">&#36; <?php echo $subtotal; ?></span>
															</td>

														</tr>
													<?php } ?>													
												</tbody>
												<tfoot>
													
													<tr>
														<th scope="row">Sub Total:</th>
														<td></td><td></td>
														<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36; </span><b><?php echo $subtotal ?></b></span></td>
														
														
													</tr>
													<tr>
														<th scope="row">Shipping:</th>
														<td><td></td></td>
														<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36; </span><b><?php echo $shipping?></b></span></td>
													</tr>
													<tr>
														<th scope="row">Total:</th>
														<td><td></td></td>
														<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36; </span><b><?php echo $total?></b></span></td>
													</tr>
												</tfoot>
												
										</table><br/><br/>					
									</div>
								</div>
							</div>
						</div>
					</td>
					
				</tr>
				
			</table>
		</div>
	</div>

	<!---Contain--->

	<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
function show_data(){
 $('.show_content').load('test');
 // $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
<sc
$(".send_msg").on('click',function(){
	var send_msg=$(this).attr('id');
	var name=$('#msg_name').val();
	var email=$('#msg_email').val();
	var phone= $('#msg_phnoe').val();
	var subject= $('#msg_subject').val();
	var message= $('#msg_message').val();
	$.ajax({
		url: "http://localhost:81/ci3/admin/dashboard/message",
		type: 'POST',
		data: {
			'name':name,
			'email':email,
			'phone':phone,
			'subject':subject,
			'message':message
		},
		success: function(msg) {
			if(msg == 'YES'){
				alert('yes');
			}else if(msg == 'No'){
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}else{
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
		}
	});
});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>

</html>
	
