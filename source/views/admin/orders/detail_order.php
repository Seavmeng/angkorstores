<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-8">
				<h3><span class="fui-document"></span>My Orders</h3>
			</div><!-- /.col -->
			<div class="col-md-3 col-sm-4 text-right"><h3><?php //echo $success; ?></h3></div><!-- /.col -->
		</div><!-- /.row -->
		<?php 
			if(isset($errors)){
			   echo show_msg($errors);
			}
			if(isset($rmsg)){
			   echo show_msg($rmsg);
			}

		//	$catFilter = catForFilter();
		 ?>
		<hr class="dashed margin-bottom-30">
		<div class="panel panel-default">
			<table class="table" id="product-list">
				<?php echo $this->session->flashdata('msg'); ?>
				<tr>
					<td>
						<div id="HTMLtoPDF">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<center><h3 class="entry-title" itemprop="name">Order Received</h3></center>
									</div>	
									<div class="col-md-12">
										<div class="col-md-8">
											<ul class="woocommerce-thankyou-order-details order_details">
												<li class="order">
													Order Number: <strong><?php echo orderID($order_info['order_number']); ?></strong>
												</li>
												<li class="date">
													Billing Id: <strong><?php echo $order_info['bill_id']; ?></strong>
												</li>
												<li class="order">
													Name: <strong><?php echo $order_info['first_name'].'&nbsp;'.$order_info['last_name']; ?></strong>
												</li>
												<li class="date">
													Email: <strong><?php echo $order_info['email']; ?></strong>
												</li>
												<li class="order">
													Phone: <strong><?php echo $order_info['phone']; ?></strong>
												</li>
												<li class="order">
													Address: <strong><?php echo $order_info['address'].', '.$order_info['city'].', '.country_byid($order_info['country']); ?></strong>
												</li>
											</ul>
										</div>
									<div class="col-md-4">
									<form action="<?php echo base_url().'admin/orders/order/send_mail'?>" method="post" id="template_email">
										<ul class="woocommerce-thankyou-order-details order_details">
										 <li>
											<label>Template Email</label>
											<select id="selectpicker" class="selectpicker form-control" name="title" style="width:200px;">
											<?php 
											foreach ($post as $value) {?>											
												<option value="<?php echo $value['category_name']; ?>"><?php echo $value['category_name']; ?></option>
											<?php } ?>
											</select>
											<input type="hidden" class="selectpicker_desc" name="description" value="<?php echo $value['description']; ?>">
											<input type="hidden" name="email" value="<?php echo $order_info['email']; ?>">
										 </li>
										 <br/>
											<input type="submit" class="btn btn-default btn-labeled mar-rgt category fa fa-paper-plane" name="btn_save"  value="Send">
										 
										</ul>
										</form>
									</div>	
									</div>
									<div class="clear"></div>
									<div class="col-md-12"><br/>
										<h4>Order Details</h4>							
										<table  class="table table-hover shop_table order_details">
												 
													<tr style="white-space:nowrap;">
														<th class="product-img">Image</th>
														<th>Product Code</th>
														<th class="product-name">Product Name</th>
														<th class="qty">Qty</th>
														<th class="price">Price</th>
														<th>Discount</th>
														<th class="product-total">Total</th>
													</tr>	
													<?php
														$subtotal=0;
														$total=0;
														
														foreach ($billing  as $key => $row) {
														//----varian-
														$color_id=$row['color'];
														$size_id=$row['size'];
														$varian=attributes($row['product_id'],$color_id);
														if($color_id){
															$image=$varian['image'];
														}else{
															$image=$row['feature'];
														}														
														
														//------
														$product_name = $row['product_name'];
                                                        $payment = $row['payment_method'];
														$proimage = $row['feature'];
														$product_code = $row['product_code'];
														if($product_code ==""){
															$product_code=$row['product_id'];
														}else{
															$product_code=$product_code;
														}
														$price = $row['prices'];
														$qty = $row['qty'];
														$order_id = $row['order_id'];
														$bill_id = $row['bill_id'];
														$fullname = $row['first_name'].' '.$row['last_name'];
														$company = $row['company_name'];
														$email = $row['email'];
														$phone = $row['phone'];
														
														$item_discount = (($row['original_price']*$row['qty']) * $row['discount_amount'] / 100);

														$subto = ($row['original_price']*$row['qty']) - $item_discount;
														$shipp_company = $row['shipping_company'];

														if($shipp_company==""){
															$s_company = "No Shipping";
															$shipping = 0;
														}else{
															$s_company = $row['shipping_company'];
															$shipping = $row['total_shipping'];
														}
													?>								
														<tr class="order_item">
														    <td width="120"> 
																<img src="<?php echo base_url().'images/products/'.$image;?>" style="width:150px;height:150px;border:1px solid #ddd;padding:2px; border-radius:2px !important;" class="img-sm">
															</td>
														    	<td class="product-name">
																<div style="margin-top: 60px;"><strong class="product-quantity" style="color: gray;"><?php echo $product_code;?></strong></div>
															</td>
															<td class="product-name">
																<div style="margin-top: 60px;"><strong class="product-quantity" style="color: gray;"><?php echo $product_name;?></strong>
																<?php 
																	echo '<br/>';
																	if($color_id){
																		echo '<strong>Color: </strong>'.$varian['attr_name'].'<br />';
																	}
																	if($size_id){
																		$size=get_attr_name($size_id);
																		echo '<strong>Size: </strong>'.$size['attr_name'].'<br />';
																	}
																?>
																</div>
															</td>
															<td class="product-name">
																<div style="margin-top: 60px;"><span class="woocommerce-Price-currencySymbol"><?php echo $qty; ?></span></div>
															</td>
															<td class="product-total" style="color: gray;">
																<div style="margin-top: 60px;"><span class="woocommerce-Price-currencySymbol">&#36;<?php echo $row['original_price']; ?></span></div>	
															</td>
															<td width="50">
																<div style="margin-top: 60px;"><span class="woocommerce-Price-currencySymbol"><?php echo $row['discount_amount'].'%'; ?></span></div>	
															</td>
															<td class="product-total">
																<div style="margin-top: 60px;"><span class="woocommerce-Price-currencySymbol">&#36; <?php echo number_format($subto,2); ?></span></div>
															</td> 
														</tr> 
														
													<?php 
														if($key==1){
															echo "<tr style='page-break-after: always'></tr>";
														}
													?>
													<?php
													$subtotal += $subto;
														
													} 
													$total += $shipping + $subtotal;
														
													?>					
													<tr>
														<td colspan="4"></td>
														<td scope="row">Sub Total:</td>
														
														<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36; </span><b><?php echo number_format($subtotal,2) ?></b></span></td>
													</tr>
													<tr>
														<td colspan="4"></td>
														<td scope="row">Pay By:</td>
														<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span><b><?php echo $payment; ?></b></span></td><td></td>
													</tr>
													<tr>
														<td colspan="4"></td>
														<th scope="row">Shipping:</th>
														<td>Via: <?php echo $s_company?></b></span>
															<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36; </span><b><?php echo $shipping ?></b></span></td>
													</tr>
													<tr>
														<td colspan="4"></td>
														<th scope="row">Total:</th>
														<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36; </span><b><?php echo number_format($total,2); ?></b></span></td>
													</tr>
										</table>				
									</div>
								</div>
							</div>
						</div>
					</td>
					
				</tr>
				
			</table>
		</div>
	</div>
	
	 <center>
		<a id="download_pdf" class="btn btn-primary" href="#" onclick="printFile('HTMLtoPDF')">Download PDF</a>
	 </center>
	 
	
	</body>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
		
		$(function(){
			$("#selectpicker").on('change',function(){
				var p = $('option:selected').val();
				$.ajax({  
					url:'<?php echo base_url('admin/order/getselect/'); ?>',
					type:'get',
					dataType:'json',
					contentType:'application/json; charset=utf-8',
					data:{ id : p },
					success:function(result){
						$(".selectpicker_desc").val(result.description);
					}
				})
			})
		});

</script>
			<!---Contain--->	
	
	<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
function show_data(){
 $('.show_content').load('test');
 // $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
$(".send_msg").on('click',function(){
	var send_msg=$(this).attr('id');
	var name=$('#msg_name').val();
	var email=$('#msg_email').val();
	var phone= $('#msg_phnoe').val();
	var subject= $('#msg_subject').val();
	var message= $('#msg_message').val();
	$.ajax({
		url: "http://localhost:81/ci3/admin/dashboard/message",
		type: 'POST',
		data: {
			'name':name,
			'email':email,
			'phone':phone,
			'subject':subject,
			'message':message
		},
		success: function(msg) {
			if(msg == 'YES'){
				alert('yes');
			}else if(msg == 'No'){
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}else{
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
		}
	});
});
 
function printFile(divId) 
    { 
	   $('#template_email').hide(); 
	   $('#download_pdf').hide(); 
	   var originalContents = document.body.innerHTML;
	   var printContents = document.getElementById(divId).innerHTML;
	   document.body.innerHTML = "<html><head><title></title></head><body><div style='border-collapse: 0; border: 0px solid black;width:100%;'>" + printContents +'</br>'+ "</div></body>";
	   
	   
	   window.print();
	   document.body.innerHTML = originalContents; 
    }
    window.onafterprint=function(){
        window.location.reload(true);
    };

</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>

</html>
 