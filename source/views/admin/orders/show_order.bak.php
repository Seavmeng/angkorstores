<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-8">
				<h3><span class="fui-document"></span>My Orders</h3>
			</div><!-- /.col -->
			<div class="col-md-3 col-sm-4 text-right"><h3><?php //echo $success; ?></h3></div><!-- /.col -->
		</div><!-- /.row -->
		<?php 
			if(isset($errors)){
			   echo msg($errors);
			}
			if(isset($rmsg)){
			   echo show_msg($rmsg);
			}
		//	$catFilter = catForFilter();
		
		 ?>
		 <?php echo $this->session->flashdata('msg'); ?>
		<hr class="dashed margin-bottom-30">
		<div class="panel panel-default">
		<form id="search-name" class="search-frm search" accept-charset="utf-8" action="<?php echo base_admin_url().'reports/report/find_id';?>" method="get">
			<table class="table" id="product-list">
				<tr>

					<td style="border-bottom: 1px solid #ebebeb;">
						<div class="col-md-12 col-sm-12">
							
							<div class="form-group col-md-4">
								<?php echo form_label(word_r('text'), 'find_id',array('class' => 'control-label')); ?>
								<?php echo form_input(array('name' => 'find_id', 'class' => 'form-control', 'id' => 'text',  'value' => $this->input->get('find_id') ) ); ?>											
							</div>
							
							<div class="form-group col-md-5 ">
								
								<div class="col-sm-4">
								<label class="control-label"><?php echo 'From'?> :</label>
									<div class="clearfix">
										<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
										
											<input type="text"  name="from" value="<?php echo $this->input->post('from_ddate'); ?>" class="form-control datepicker"> 
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
											<font color="red"><?php echo form_error('from_ddate'); ?></font>
										</div>
									</div>
								</div>
								
								<div class="col-sm-4">
								<label class="control-label"><?php echo 'To'?> :</label>
									<div class="clearfix">
										<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
										
											<input type="text" name="to" value="<?php echo $this->input->post('to_ddate'); ?>" class="form-control datepicker"> 
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
											<font color="red"><?php echo form_error('to_ddate'); ?></font>
										</div>
									</div>
								</div>
							</div>
																					
							
						
							<div class="form-group col-md-3">
								<label for="" class="control-label">&nbsp;</label>
								<input type="submit" class="btn btn blue form-control" value="<?php word('search'); ?>">											
							</div>
						</div>
				    </td>
				</tr>
						
						</form>
				    </td>
				</tr>
				<tr>
					<td>
						<div class="panel panel-default">
							<table data-show-export="true" data-search="true" data-show-refresh="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-pagination="true" data-side-pagination="server" data-url="#" class="table table-striped table-bordered table-hover" id="events-table">
								
								<thead>
								
									<tr>
										<th style="text-align: center;">N</th>
										<th style="text-align: center;">Order</th>
										<th style="text-align: center; ">Bill</th>
										<th style="text-align: center; ">Billing Email</th>
										<th style="text-align: center; ">Order Date</th>
										<th style="text-align: center; ">Price</th>
										<th style="text-align: center; ">Options</th>
									</tr>
								</thead>
								
								<tbody>
									<?php
										//var_dump($billing);
										$i=0;
										foreach($billing as $rows){
											//var_dump($rows);exit();
											$order_id = $rows['order_number'];
											$billing_email = $rows['email'];
											$bill_id = $rows['bill_id'];
										//	$price = $rows['prices'];
									//		$id = $rows['user_id'];
											//$dir = date('m-Y', strtotime($rows['created_date']));
											$i++;
											$order_number='ID'.str_pad($order_id, 6, '0', STR_PAD_LEFT);
									?>
									<tr>
										<td style="text-align: left; "><?php echo $i;?></td>
										<td style="text-align: center; "><?php echo $order_number;?></td>
										<td style="text-align: center; "><?php echo $bill_id;?></td>
										<td style="text-align: left; "><a href="<?php echo  base_url().'admin/orders/order/view/'.$order_id;?>"><?php echo $billing_email;?></a></td>
										<td style="text-align: left; "><?php echo $rows['created'];?></td>
										<td style="text-align: left; "><?php //get_totalprice($bill_id);?></td>
										<td style="text-align: center; ">  
											<a class="btn btn-default btn-labeled fa fa-eye pull-right mar-rgt category" data-toggle="modal"  href="<?php echo  base_url().'admin/orders/order/view/'.$bill_id;?>">
													Detail                              
											</a>
											<a href="<?php echo base_admin_url().'orders/order/delete_order/'.$bill_id;?>" onclick="return confirm('Are you sure want to delete ?')" class="btn default btn-xs red" title="Delete"><i class="fa fa-trash-o"></i> Delete </a>
										</td>
									
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
						</div>
					</td>
					
				</tr>
				<tr>
					<td>
						<div class="pull-right">
							<div class="title_page">
								<!-- <?php echo $showing;?> -->
							</div>
							<!-- <?php echo $page;?> -->
						</div>
					</td>
				</tr>
			</table>
	</div>
	</div>
	</body>


			<!---Contain--->	
	
	<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
$(document).ready(function() {		
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			//startDate: '-3d'
		});
		
});
function show_data(){
 $('.show_content').load('test');
 // $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
$(".send_msg").on('click',function(){
	var send_msg=$(this).attr('id');
	var name=$('#msg_name').val();
	var email=$('#msg_email').val();
	var phone= $('#msg_phnoe').val();
	var subject= $('#msg_subject').val();
	var message= $('#msg_message').val();
	$.ajax({
		url: "http://localhost:81/ci3/admin/dashboard/message",
		type: 'POST',
		data: {
			'name':name,
			'email':email,
			'phone':phone,
			'subject':subject,
			'message':message
		},
		success: function(msg) {
			if(msg == 'YES'){
				alert('yes');
			}else if(msg == 'No'){
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}else{
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
		}
	});
});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>

</html>