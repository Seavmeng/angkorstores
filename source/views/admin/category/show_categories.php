<?php $this->load->view("account/shared/header.php");?>
	<body>
		<?php $this->load->view("account/shared/nav.php");?> 
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-9 col-sm-8">
					<h3><span class="fui-document"></span>Manage Categories</h3>
				</div><!-- /.col -->
				<div class="col-md-3 col-sm-4 text-right"></div><!-- /.col -->
			</div><!-- /.row -->
			<hr class="dashed margin-bottom-30">

			<div class="panel panel-default">
				<table class="table">
					<tr>
						<td style="border-bottom: 1px solid #ebebeb;">
							<?php 
								$admin = $this->ion_auth->is_admin();
								if(!$admin){
							?>
								<a class="btn btn-primary btn-labeled fa fa-plus-circle pull-right mar-rgt category" data-toggle="modal" href="<?php echo base_url().'account/category/choose_category'?>">
									Create Category                                
								</a>
							<?php
								}else{
							?>
								<a class="btn btn-primary btn-labeled fa fa-plus-circle pull-right mar-rgt category" data-toggle="modal" href="<?php echo base_url().'account/category/create'?>">
									Create Category                                
								</a>
							<?php
								}
							?>
						</td>
					</tr>
					<tr>
						<td>
							<div class="panel panel-default">
								<?php
									$i=0;
									$output ="";
									$output .='<div class="table-responsive"><table class="table table-striped table-bordered table-hover">';
									$output .= '<thead><tr>';
									$output .= '<th width="50px"></th>';
									$output .= '<th>No</th>';
									$output .= '<th>Category English</th>';
									$output .= '<th>Category Khmer</th>';
									$output .= '<th>Category Chinese</th>';
									$output .= '<th>Category Vietnamese</th>';
									//$output .= '<th>Description</th>';
									$output .= '<th style="width:180px;">Action </th>';
									$output .= '</tr></thead>';
									foreach($data as $value){
										$i++;
										$category_id = $value['category_id'];
										$category_name = $value['category'];
										$category_kh = $value['category_kh'];
										$category_ch = $value['category_ch'];
										$category_vn = $value['category_vn'];
										$description = $value['description'];
										
										$parent_id = check_sub_menu($category_id);
										$array = array($parent_id);
										
										if(in_array($category_id, $array, true)){
											echo 'run';
										}
										
										$output .='<tr>';
										$output .= '<td width="50px"><input type="checkbox" class="select"></td>';
										$output .= '<td>'.$i.'</td>';

										$output .='<td ';
										$output .=' id="'.$category_id.'" class="opt-parent" >';
										$output .= $category_name.'</td>';
										
										$output .='<td ';
										$output .=' id="'.$category_id.'" class="opt-parent" >';
										$output .= $category_kh.'</td>';
										
										$output .='<td ';
										$output .=' id="'.$category_id.'" class="opt-parent" >';
										$output .= $category_ch.'</td>';
										
										$output .='<td ';
										$output .=' id="'.$category_id.'" class="opt-parent" >';
										$output .= $category_vn.'</td>';
										
										$output .='<td ';
										$output .=' id="'.$category_id.'" class="opt-parent" >';
										$output .= btn_action('category/update/'.$category_id,'category/delete/'.$category_id).'</td>';
										
										$output .=_child_category($category_id,user_id_logged(),$level=1);
					
										$output .='</tr>';
									}
									
									$output .='</table></div>';	
									echo $output;
								?>	
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="pull-right">
								<div class="title_page">
									<?php echo $showing;?>
								</div>
								<?php echo $page;?>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div><!---Contain--->	
	</body>
	<script src="<?php echo base_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
</html>