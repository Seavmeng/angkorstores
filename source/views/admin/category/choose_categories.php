<?php $this->load->view("account/shared/header.php");

?>
<body>
<!-- - - - - - - - - - - - - - Main Wrapper - - - - - - - - - - - - - - - - -->
<div class="wide_layout">
	<!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->
	<?php $this->load->view("account/shared/nav.php");?>
	<!-- - - - - - - - - - - - - - End Header - - - - - - - - - - - - - - - - -->
	<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->
	<div class="secondary_page_wrapper">
	<div class="container">
		<div class="row">
			<div class="portlet-title">
				<h3><span class="fui-document"></span>Add New Categories</h3>
			</div>
			<hr class="dashed margin-bottom-30">
				
			<div class="panel panel-default">
				<?php echo form_open_multipart('account/category/use_category', array('role' => 'form','class' => 'form-horizontal')); ?>
					<div class="panel-body">
						<a class="btn btn-primary btn-labeled fa fa-plus-circle pull-right mar-rgt category" data-toggle="modal" href="<?php echo base_url().'account/category/create'?>">
							Add New                              
						</a>
						<input class="btn btn-primary btn-labeled fa fa-plus-circle pull-right mar-rgt category" data-toggle="modal" type="submit" value="Use this">
					</div>
					<div class="panel-body">
						<?php
							echo choose_category();
						?>
					</div>	
				<?php echo form_close(); ?>
			</div>
		</div><!--/ .row-->

	</div><!--/ .container-->
	<div class="span6" style="padding-bottom:50px;">
		&nbsp;
	</div>

</div><!--/ .page_wrapper-->

</script>
<script src="<?php echo base_url();?>js/jquery-2.0.3.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		/*$("input[type=checkbox]").change( function() {
			if($(this).is(":checked")){
			   $('.show_hide').s
			}else{
				alert('not checked');
			}
		});*/
	});
</script>
</body>
</html>