<?php $this->load->view("account/shared/header.php");?>
<body>
<!-- - - - - - - - - - - - - - Main Wrapper - - - - - - - - - - - - - - - - -->
<div class="wide_layout">
	<!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->
	<?php $this->load->view("account/shared/nav.php");?>
	<!-- - - - - - - - - - - - - - End Header - - - - - - - - - - - - - - - - -->
	<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->
	<div class="secondary_page_wrapper">
	<div class="container">
		<div class="row">
			<div class="portlet-title">
				<h3><span class="fui-document"></span>Add New Categories</h3>
			</div>
			<hr class="dashed margin-bottom-30">

			<aside class="col-md-2 col-sm-4 has_mega_menu">
			
				<?php //$this->load->view('includes/sidebar'); ?>
				
			</aside>
				
			<div class="panel panel-default">
				<div class="panel-body">
				<div class="col-md-9 col-sm-8">
					<div class="col-sm-12">
						<div id="full">	
							<?php 
								echo form_open(account_url().$controller_url.$this->uri->segment(3), array('role' => 'form','class' => 'form-horizontal')); ?>
							<!-- - - - - - - - - - - - - - Contact information - - - - - - - - - - - - - - - - -->
							<section class="theme_box">
				
								<?php 
									if(isset($errors)){
									   echo msg($errors);
									}
									if(isset($success)){
										echo msg_suc($success);
									}
								?>
								<div class="form-group">
									<?php echo form_label('Parent','parent',array('class' => 'col-sm-2 control-label') ); ?>
									<div class="col-sm-9">
										<?php  echo get_category_select_option('parent',false,user_id_logged('user_id') ); ?>
										<font color="red"><?php echo form_error('parent'); ?></font>
									</div>
								</div>
								<div class="form-group">
									<?php echo form_label('En Category','category',array('class' => ' col-sm-2 control-label required') ); ?>
									<div class="col-sm-9">
										<?php echo form_input('category_name', $this->input->post('category_name', true),"class = 'form-control', id= 'category_name'"); ?>
										<font color="red"><?php echo form_error('category_name'); ?></font>
									</div>
								</div>
								<div class="form-group">
									<?php echo form_label('Kh Category','category',array('class' => ' col-sm-2 control-label required') ); ?>
									<div class="col-sm-9">
										<?php echo form_input('category_kh', $this->input->post('category_kh', true),"class = 'form-control', id= 'category_kh'"); ?>
										<font color="red"><?php echo form_error('category_kh'); ?></font>
									</div>
								</div>
								<div class="form-group">
									<?php echo form_label('Ch Category','category',array('class' => ' col-sm-2 control-label required') ); ?>
									<div class="col-sm-9">
										<?php echo form_input('category_ch', $this->input->post('category_ch', true),"class = 'form-control', id= 'category_ch'"); ?>
										<font color="red"><?php echo form_error('category_ch'); ?></font>
									</div>
								</div>
								<div class="form-group">
									<?php echo form_label('VN Category','category',array('class' => ' col-sm-2 control-label required') ); ?>
									<div class="col-sm-9">
										<?php echo form_input('category_vn', $this->input->post('category_vn', true),"class = 'form-control', id= 'category_vn'"); ?>
										<font color="red"><?php echo form_error('category_vn'); ?></font>
									</div>
								</div>
								<div class="form-group">
									<?php echo form_label('Icon','icon',array('class' => 'col-sm-2  control-label') ); ?>
									<div class="col-sm-9">
										<?php echo form_input('icon', $this->input->post('icon', true),"class = 'form-control', id= 'icon'"); ?>
										<font color="red"><?php echo form_error('icon'); ?></font>
									</div>
								</div>
								<div class="form-group">
									<?php echo form_label('Tag','tag',array('class' => 'col-sm-2  control-label') ); ?>
									<div class="col-sm-9">
										<?php echo form_input('tag', $this->input->post('tag', true),"class = 'form-control', id= 'tag'"); ?>
										<font color="red"><?php echo form_error('tag'); ?></font>
									</div>
								</div>
								<div class="form-group">
									<?php echo form_label('Description','description',array('class' => 'col-sm-2 control-label') ); ?>
									<div class="col-sm-9">
										<?php echo form_textarea(array('name' => 'description', 'class' => 'ckeditor form-control', 'id' => 'description',  'value' => $this->input->post('description')) );?>
										<font color="red"><?php echo form_error('description'); ?></font>
									</div>
								</div>
								<div class="form-group">
									<?php echo form_label('Order','rang',array('class' => 'col-sm-2  control-label') ); ?>
									<div class="col-sm-9">
										<?php echo form_input('rang',$this->input->post('rang', true), "class = 'form-control', id= 'rang'" ,$this->input->post('rang', true)); ?>
										<font color="red"><?php echo form_error('rang'); ?></font>
									</div>
								</div>
								<div class="form form-actions">
									<?php echo form_label('','rang',array('class' => 'col-sm-2  control-label') ); ?>
									<?php echo form_submit('create', word_r('create'), "class = 'btn btn-info' "); ?>
									<?php echo '<a href="'.account_url().$controller_url.'" class="btn btn-danger">'.word_r('cancel').'</a>'; ?>
								</div>
							</section><!--/ .theme_box -->

							<!-- - - - - - - - - - - - - - End of contact information - - - - - - - - - - - - - - - - -->
							<?php echo form_close(); ?>
						</div>
					</div>
				</div><!--/ [col]-->
				</div>
			</div>
		</div><!--/ .row-->

	</div><!--/ .container-->
	<div class="span6" style="padding-bottom:50px;">
		&nbsp;
	</div>

</div><!--/ .page_wrapper-->
<script src="<?php echo base_url();?>js/jquery-2.0.3.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
</body>
</html>