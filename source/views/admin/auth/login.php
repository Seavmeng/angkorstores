<?php $this->load->view('includes/head')?>

<body>
	<div id="header" class="header">
		<?php $this->load->view('includes/login_head')?>
	</div>
	
	<div class="col-md-12 contain">
		<div class="container">
			<div class="row">
				<div class="col-md-6 img">
					<img src="<?php echo base_url().'images/11.png'?>"/>
				</div>
				<div class="col-md-6">
					<!--<p><?php echo lang('login_subheading');?></p>-->
				
					<?php if( isset($message) && $message != '' ):?>
					<div class="alert alert-success" style="width:60%;">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</button>
						<?php echo $message;?>
					</div>
					<?php endif;?>
					<div class="login-form">
						<div class="form-title">
							Account Log In
						</div>
						<form role="form" action="<?php echo site_url("account/auth/login");?>" method="post">
							<div class="input-group">
								<span class="input-group-btn">
									<button class="btn btn-default">
										<span class="glyphicon glyphicon-user"></span>
									</button>
								</span>
								<input type="email" class="form-control" id="identity" name="identity" placeholder="Your email address">
							</div>
							<div class="input-group">
								<span class="input-group-btn">
									<button class="btn btn-default">
										<span class="glyphicon glyphicon-lock"></span>
									</button>
								</span>
								<input type="password" class="form-control" id="password" name="password" placeholder="Your password">
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12" style="margin-left:20px;">
									<label class="checkbox margin-bottom-20" for="checkbox1">
										<input type="checkbox" value="1" id="remember" name="remember" data-toggle="checkbox">
										<?php echo $this->lang->line('login_rememberme')?>
									</label>
								</div>
							</div>
							<div class="row" style="margin-bottom:20px;">
								<div class="col-md-6">
									<a href="<?php echo site_url("account/auth/forgot_password");?>">FORGET PASSWORD</a>
								</div>
								<div class="col-md-6">
									<a href="<?php echo site_url("account/auth/create_user");?>" style="float:right"><?php echo $this->lang->line('login_button_signup')?></a>
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-block btn-embossed">
								<?php echo $this->lang->line('login_button_login')?><span class="fui-arrow-right"></span>
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div class="footer-login">
		<?php $this->load->view('includes/footer')?>
	</div>
	
	<!-- Load JS here for greater good =============================-->
    <script src="<?php echo base_url();?>js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.ui.touch-punch.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-select.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-switch.js"></script>
    <script src="<?php echo base_url();?>js/flatui-checkbox.js"></script>
    <script src="<?php echo base_url();?>js/flatui-radio.js"></script>
    <script src="<?php echo base_url();?>js/jquery.tagsinput.js"></script>
    <script src="<?php echo base_url();?>js/flatui-fileinput.js"></script>
    <script src="<?php echo base_url();?>js/jquery.placeholder.js"></script>
    <script src="<?php echo base_url();?>js/application.js"></script>
  </body>
</html>
