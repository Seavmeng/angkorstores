<?php $this->load->view("account/shared/header.php");?>
<body class="descript">
    <div class="container">
    	<div class="row">
			<div class="col-sm-9 col-md-offset-1">
    			<h2 class="text-center">
    				<img alt="shop" src="<?php echo base_url()?>images/logo_new.png" />
    			</h2>
    			
    			<!--<p><?php echo lang('login_subheading');?></p>-->
    			    		
    			<?php echo form_open_multipart('account/auth/complete_form', array('role' => 'form','class' => 'form-horizontal')); ?>
    				<?php
						$id = $ids['user_permission'];
						$idd = $ids['id'];
						if($id == 2 or $id==0 or $id==4){
					?>
						<div class="heading-counter warning">
							<ul>				
								<li class="row" style="display:none;">

									<div class="col-xs-12">

										<label for="address_1" class="required">id</label>
										<input class="input form-control" type="text" name="id" id="" value="<?php echo $idd;?>">

									</div><!--/ [col] -->

								</li><!--/ .row -->
								
								<li class="row">

									<div class="col-xs-12">
										<label for="logo" class="required">Logo image</label><br/>
										<span class="btn btn-default btn-file">Add Your Logo
											<input type="file" name="userfile[]" onchange="readURL(this);">
										</span>
										<br/>
										<br/>
										<span>
											<img id="blah" src="#" alt="" />
										</span>
									</div><!--/ [col] -->
								
								</li><!--/ .row -->
								
								<li class="row">
									
									<div class="col-sm-12">
										
										<label for="company_name_1">Company Name</label>
										<input class="input form-control" type="text" name="company_name" id="company_name_1">

									</div><!--/ [col] -->

								</li><!--/ .row -->

								<li class="row">

									<div class="col-xs-12">

										<label for="address_1" class="required">Address</label>
										<input class="input form-control" type="text" name="address" id="address_1">

									</div><!--/ [col] -->

								</li><!--/ .row -->

								<li class="row">

									<div class="col-sm-6">
										
										<label for="city_1" class="required">City</label>
										<input class="input form-control" type="text" name="city" id="city_1">

									</div><!--/ [col] -->

									<div class="col-sm-6">

										<label class="required">State/Province</label>

										<div class="custom_select">

											<select class="input form-control" name="state" style="color:#B2BCC5;background-color:#2C3E50;border:0;-webkit-appearance: none;-moz-appearance: none;">

												<option value="Alabama">Alabama</option>
												<option value="Illinois">Illinois</option>
												<option value="Kansas">Kansas</option>

											</select>

										</div>

									</div><!--/ [col] -->

								</li><!--/ .row -->

								<li class="row">

									<div class="col-sm-6">

										<label for="postal_code_1" class="required">Zip/Postal Code</label>
										<input class="input form-control" type="text" name="zip_code" id="postal_code_1">

									</div><!--/ [col] -->

									<div class="col-sm-6">

										<label class="required">Country</label>

										<div class="custom_select">

											<select class="input form-control" name="country" style="color:#B2BCC5;background-color:#2C3E50;border:0;-webkit-appearance: none;-moz-appearance: none;">
												
												<option value="USA">USA</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Argentina">Argentina</option>
												<option value="Canada">Canada</option>

											</select>

										</div>

									</div><!--/ [col] -->

								</li><!--/ .row -->

								<li class="row">

									<div class="col-sm-6">

										<label for="telephone_1" class="required">Telephone</label>
										<input class="input form-control" type="text" name="phone" id="telephone_1">

									</div><!--/ [col] -->

									<div class="col-sm-6">

										<label for="fax_1">Fax</label>
										<input class="input form-control" type="text" name="fax" id="fax_1">

									</div><!--/ [col] -->

								</li><!--/ .row -->

							</ul>
						</div>
					
					<?php
						}else{
					?>
					
						<div class="heading-counter warning">
							<ul>				
								
								<li class="row" style="display:none;">

									<div class="col-xs-12">

										<label for="address_1" class="required">id</label>
										<input class="input form-control" type="text" name="id" id="" value="<?php echo $id;?>">

									</div><!--/ [col] -->

								</li><!--/ .row -->
								
								<li class="row">

									<div class="col-xs-12">

										<label for="address_1" class="required">Address</label>
										<input class="input form-control" type="text" name="address" id="address_1">

									</div><!--/ [col] -->

								</li><!--/ .row -->

								<li class="row">

									<div class="col-sm-6">
										
										<label for="city_1" class="required">City</label>
										<input class="input form-control" type="text" name="city" id="city_1">

									</div><!--/ [col] -->

									<div class="col-sm-6">

										<label class="required">State/Province</label>

										<div class="custom_select">

											<select class="input form-control" name="state" style="color:#B2BCC5;background-color:#2C3E50;border:0;-webkit-appearance: none;-moz-appearance: none;">

												<option value="Alabama">Alabama</option>
												<option value="Illinois">Illinois</option>
												<option value="Kansas">Kansas</option>

											</select>

										</div>

									</div><!--/ [col] -->

								</li><!--/ .row -->

								<li class="row">

									<div class="col-sm-6">

										<label for="postal_code_1" class="required">Zip/Postal Code</label>
										<input class="input form-control" type="text" name="zip_code" id="postal_code_1">

									</div><!--/ [col] -->

									<div class="col-sm-6">

										<label class="required">Country</label>

										<div class="custom_select">

											<select class="input form-control" name="country" style="color:#B2BCC5;background-color:#2C3E50;border:0;-webkit-appearance: none;-moz-appearance: none;">
												
												<option value="USA">USA</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Argentina">Argentina</option>
												<option value="Canada">Canada</option>

											</select>

										</div>

									</div><!--/ [col] -->

								</li><!--/ .row -->

								<li class="row">

									<div class="col-sm-6">

										<label for="telephone_1" class="required">Telephone</label>
										<input class="input form-control" type="text" name="phone" id="telephone_1">

									</div><!--/ [col] -->

									<div class="col-sm-6">

										<label for="fax_1">Fax</label>
										<input class="input form-control" type="text" name="fax" id="fax_1">

									</div><!--/ [col] -->

								</li><!--/ .row -->

							</ul>
						</div>
					
					<?php
						}
					?>
    			  	<input type="submit" class="btn btn-primary btn-block btn-embossed" name="btn_com" value="<?php echo $this->lang->line('login_button_login')?>" />
    			<?php echo form_close(); ?>
    		</div>
    	</div><!-- /.row -->
    </div><!-- /.container -->
    
	<script>
		function readURL(input) {
		  if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
			  $('#blah')
				.attr('src', e.target.result)
				.width(150)
				.height(150);
			};
			reader.readAsDataURL(input.files[0]);
		  }
		};
	</script>
    <!-- Load JS here for greater good =============================-->
    <script src="<?php echo base_url();?>js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.ui.touch-punch.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-select.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-switch.js"></script>
    <script src="<?php echo base_url();?>js/flatui-checkbox.js"></script>
    <script src="<?php echo base_url();?>js/flatui-radio.js"></script>
    <script src="<?php echo base_url();?>js/jquery.tagsinput.js"></script>
    <script src="<?php echo base_url();?>js/flatui-fileinput.js"></script>
    <script src="<?php echo base_url();?>js/jquery.placeholder.js"></script>
    <script src="<?php echo base_url();?>js/application.js"></script>
  </body>
</html>