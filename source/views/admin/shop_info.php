<?php $this->load->view(config_item('include_dir').'head');?>
<body class="option3 nonbgwhite">
<div class='site-head-front'>
<?php $this->load->view(config_item('include_dir').'top_navbar'); ?>
</div>
	<!-- header -->
<?php $this->load->view(config_item('include_dir').'header'); ?>
	<!-- ./header -->
	<?php 
	if(isset($errors)){ echo msg($errors);}
	if(isset($success)){ echo msg_suc($success);}
	if(isset($rmsg)){
		echo msg_suc($rmsg);
	}
	?>
	<div class="container">
		<div class="row">
			<div class="block block-breadcrumbs">
				<ul>
					<li class="home">
						<a href="#"><i class="fa fa-home"></i></a>
						<span></span>
					</li>
					<li>Authentication</li>
				</ul>
			</div>
			<div class="main-page">
				 <?php echo form_open_multipart(account_url()."register/info", array('role' => 'form','class' => 'form-horizontal'))?>
				<h1 class="page-title">Complete checkout as shop vendor detail.</h1>
				<div class="page-content">
					<div class="page-content checkout-page">
						<h3 class="checkout-sep"><b>Key Infomations</b> ( Make customers have confidence on you.)</h3>
						<div class="box-border">
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group">
										<label class="col-sm-3 control-label required">First Name</label>
										<div class="col-sm-9">
											<?php echo form_input('first_name', $this->input->post('first_name', true),"class='form-control'")?>
											<font color="red"><?php echo form_error('first_name'); ?></font>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-sm-3 control-label required">Last Name</label>
										<div class="col-sm-9">
											<?php echo form_input('last_name', $this->input->post('last_name', true),"class='form-control'")?>
											<font color="red"><?php echo form_error('last_name'); ?></font>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label required"><?php echo word_r('email')?></label>
										<div class="col-md-9">
											<?php echo form_input('email', $this->input->post('email', true),"class = 'form-control' id= 'email'"); ?>
											<font color="red"><?php echo form_error('email');?></font>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label required">Company/Saler Name</label>
										<div class="col-sm-9">
											<?php echo form_input('company', $this->input->post('company', true),"class='form-control'")?>
											<font color="red"><?php echo form_error('company'); ?></font>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label required">Business Type</label>
										<div class="col-sm-9">
											<?php echo BusinessType($this->input->post('businessType', true))?>
											<font color="red"><?php echo form_error('businessType'); ?></font>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label required">Country/Region</label>
										<div class="col-sm-9">
											<?php echo country(34)?>
											<font color="red"><?php echo form_error('country'); ?></font>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label required">City/State</label>
										<div class="col-sm-9">
											<?php 
											echo city(34);
											//echo form_input('city', $this->input->post('city', true),"class='form-control'")?>
											<font color="red"><?php echo form_error('city'); ?></font>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label required">District</label>
										<div class="col-sm-9">
											<?php echo form_input('district', $this->input->post('district', true),"class='form-control'")?>
											<font color="red"><?php echo form_error('district'); ?></font>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label required">Street</label>
										<div class="col-sm-9">
											<?php echo form_input('street', $this->input->post('street', true),"class='form-control'")?>
											<font color="red"><?php echo form_error('street'); ?></font>
										</div>
									</div>
									<div class="form-group">
										<label for="first_name" class="col-sm-3 control-label required">Phone</label>
										<div class="col-sm-9">
											<ul>
												<li class="row">
													<div class="col-sm-4">
														<label class="">Country Code</label>
														<?php echo form_input('countryCode', $this->input->post('countryCode', true),"class='form-control'")?>
														<font color="red"><?php echo form_error('countryCode'); ?></font>
													</div><!--/ [col] -->
													<div class="col-sm-8">
														<label class="">Number</label>
														<?php echo form_input('phoneNumber', $this->input->post('phoneNumber', true),"class='form-control'")?>
														<font color="red"><?php echo form_error('phoneNumber'); ?></font>
													</div><!--/ [col] -->
												</li><!--/ .row -->
											</ul>
										</div>
									</div>
									<div class="form-group">
										<label for="first_name" class="col-sm-3 control-label">Fax</label>
										<div class="col-sm-9">
											<?php echo form_input('fax', $this->input->post('fax', true),"class='form-control'")?>
											<font color="red"><?php echo form_error('fax'); ?></font>
										</div>
									</div>
									<div class="form-group">
										<label for="first_name" class="col-sm-3 control-label">Company Introduction</label>
										<div class="col-sm-9">
											<?php echo form_textarea(array('name' => 'companyInfo', 'id' => 'companyInfo',  'value' => $this->input->post('companyInfo')) );?>
											<font color="red"><?php echo form_error('companyInfo'); ?></font>
										</div>
									</div>								
								</div>
								<div class="col-md-4">
									<div class="cross_sec">
									<div class="form-group">
										<ul>
											<li class="row">
												<div class="col-sm-6">
													<label class="" for="first_name">Company Logo/Profile</label>
													<div class="fileinput fileinput-<?php echo $data['profile'] == ''? 'new':'exists';?>" data-provides="fileinput">
													  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
														<img data-src="holder.js/100%x100%" alt="">
													  </div>
													  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
														
														<?php if(!empty($data['profile'])): ?>
														<img src="<?php echo base_url().'images/products/'.$data['profile']; ?>" data-src="holder.js/100%x100%" alt="">
														<?php endif; ?>
														<input type="hidden" name="photo" value="<?php echo $data['profile']; ?>" id="image">
													  </div>
													 

													  <div>
														<span class="btn btn-default btn-file waves-effect"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
														<input type="hidden" value="" name=""><input type="file" name="userfile[]" multiple=""></span>
														<a data-dismiss="fileinput" class="btn btn-default fileinput-exists waves-effect" href="#">Remove</a>
													  </div>
													</div>
												</div>
											</li>
											<li class="row">
												<div class="col-sm-8">
													<label class="" for="first_name">Company Licence/Certificate</label>
													<div class="fileinput fileinput-<?php echo $data['licence'] == ''? 'new':'exists';?>" data-provides="fileinput">
													  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
														<img data-src="holder.js/100%x100%" alt="">
													  </div>
													  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
														
														<?php if(!empty($data['licence'])): ?>
														<img src="<?php echo base_url().'images/products/'.$data['licence']; ?>" data-src="holder.js/100%x100%" alt="">
														<?php endif; ?>
														<input type="hidden" name="image" value="<?php echo $data['licence']; ?>" id="image">
													  </div>
													 

													  <div>
														<span class="btn btn-default btn-file waves-effect"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
														<input type="hidden" value="" name=""><input type="file" name="gallery[]" multiple=""></span>
														<a data-dismiss="fileinput" class="btn btn-default fileinput-exists waves-effect" href="#">Remove</a>
													  </div>
													</div>
												</div>
											</li>
										</ul>
									
									</div>	
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label for="first_name" class="col-sm-3 control-label"></label>
										<div class="col-sm-9">
											<input type="submit" name="btn_save" value="Submit" class="btn btn-primary">
										</div>
									</div>
								</div>
							
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>

	</div>
	<!-- footer -->
	<?php $this->load->view(config_item('include_dir').'footer-section'); ?>
	<!-- ./footer -->
<?php $this->load->view(config_item('include_dir').'footer'); ?>
</body>

<!-- Mirrored from kutethemes.com/html/edo/html/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 02 Oct 2015 01:16:59 GMT -->
</html>