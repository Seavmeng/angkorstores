<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row"> 
		<!-- COL-MD-12 -->
		<div class="col-md-12">  
			<h3>Discount</h3>
			<?php 
				$msg = $this->session->flashdata('msg'); 
				if(isset($msg)){
				   echo $msg;
				}
			 ?>
			<div class="portlet light bordered">
				<!-- portlet-title -->
				<div class="portlet-title"> 
					<div class="caption">
						<i class="<?php echo $icon; ?>"></i>
						<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
					</div>  
					<div class="clearfix"></div>
					
					<?php
						echo form_open(base_admin_url().$controller_url.'filter_discount/'.$id, array('role' => 'form')); 
					?>
					<div class="form-group col-md-3">
						<label  class="control-label">Filter</label>	
						<select name='filter' class="form-control">
							<option value="all" >All</option>
							<option value="sale_price" <?php echo $_POST['filter'] == "sale_price" ? ' selected="selected"' : ''?> >Sale Price</option>
							<option value="regular_price" <?php echo $_POST['filter'] == "regular_price" ? ' selected="selected"' : ''?>>Regular Price</option> 
						</select>
					</div>
					<div class="col-md-3">
						<div class="form-group"> 
							<label><br/></label>	
							<div class="input-group input-large ">  
							<span class="input-group-addon">Between</span>
							<?php echo form_input(array('placeholder' => '...','name' => 'between', 'class' => 'form-control text-center',  'value' => set_value('between') ) ); ?>											
							<span class="input-group-addon">-</span>
							<?php echo form_input(array('placeholder' => '...','name' => 'toValue', 'class' => 'form-control text-center',  'value' => set_value('toValue') ) ); ?>
							</div>
						</div> 
					</div>   
					
					
					<?php 
							if($this->session->userdata('group_id') == 1){
						 ?> 
							<div class="col-md-2">
							<?php
								$supplierRemem = $this->session->userdata('supplier');
						
							?> 
								<label>Choose Supplier</label>
								<select name="supplier" class="form-control">
								<option value="">---------Please choose supplier-----</option>
									<?php foreach ($supplier as $row):
									?>
										<option value="<?php echo $row['user_id']; ?>" <?php echo ($_POST['supplier']== $row['user_id'])?'selected':''; ?>><?php echo $row['username']; ?></option>
									<?php endforeach;?>
								</select>
							</div> 
						<?php }?>
					
					<div class="col-md-3">
						<label><br/></label>	
						<div class="form-group">
							<input type="submit" value="Search" class="btn btn-success" value="default" name="search">
						</div>
					</div> 
					<?php
						echo form_close(); 
					?> 
					<div class="clearfix"></div>
					<?php
						echo form_open(base_admin_url().$controller_url.'discount/'.$id, array('role' => 'form')); 
					?> 
					<div class="col-md-3">
						<div class="form-group">
							<label class="control-label" for="category_name">Discount Amount(%)</label>												
							<input type="text" id="category_name" class="form-control" value="<?php echo set_value("category_name");?>" name="category_name">
						</div>  
					</div>   
					<div class="col-sm-3"> 
						<label class="control-label">From :</label>
						<div class="clearfix">
							<div class="input-group input-large date-picker input-daterange text-left" >
							
								<input name="from"  value="<?php echo date('01-m-Y');?>" class="form-control datepicker" type="text"> 
								
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
								<font color="red"><font color="red"></font></font>
							</div>  
						</div> 
					</div> 
					<div class="col-sm-2">
						<label class="control-label"><br/></label>
						<div class="clearfix">
							<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
								<input type="text" class="form-control timepicker" name="from_dtime" value="">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-time"></span>
								</span> 
							</div>
						</div>
					</div> 
					<div class='clearfix'></div>
					<div class="col-sm-3"></div>
					<div class="col-sm-3">
						<label class="control-label">To :</label>
						<div class="clearfix">
							<div class="input-group input-large date-picker input-daterange" >
							
								<input name="to" value="<?php echo date('d-m-Y');?>" class="form-control datepicker" type="text"> 
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
								<font color="red"><font color="red"></font></font>
							</div>
						</div>
					</div> 
					<div class="col-sm-2">
						<label class="control-label"><br/></label>
						<div class="clearfix">
							<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
								<input type="text" class="form-control timepicker" name="to_time"  value="">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-time"></span>
								</span> 
							</div>
						</div>
					</div>  
					 
					<div class="col-md-3">
						<label><br/></label>	
						<div class="form-group">
							<input type="submit" <?php echo (empty($data))?"disabled":"" ?> value="Submit" class="btn btn-primary" value="default" name="submit">
						</div>
					</div> 
				</div> 
					<div class="portlet-body">  
						<div class="table-scrollable">   
								<table class="table table-bordered table-hover sort-table">
									 <thead>
										 <tr>
												<th><?php echo $this->_data['check_ml']?></th>
												<th>Product Name</th>
												<th>Supplier</th>
												<th>Sale Price</th>
												<th>Regular Price</th>
												<th>From Date</th>
												<th>To Date</th>
												<th>Discount(%)</th> 
										</tr>
									</thead>   
									<?php if(!empty($data)) {?>
										<?php foreach ($data as  $value){ ?> 
												<tr>
													<td>
														<div class="checker"><span><input name="id[]" value="<?php echo $value['category_id']?>" class="checkboxes" type="checkbox"></span></div>
													</td>
													<td><?php echo $value['product_name']?></td> 
													<td><?php echo $value['username']?></td>  
													<td><?php echo $value['sale_price']?></td>  
													<td><?php echo $value['regular_prices']?></td>  
													<td><?php echo $value['from_date']?></td>  
													<td><?php echo $value['todate']?></td>   
													<td><input name="product_id[]" value="<?= $value['product_id']?>" type="hidden"><input value="<?= $value['discount_amount']?>" class="form-control discount" style="width:160px;" name="discount[]" type="text"></td>
												</tr>  
										<?php }?>  
									<?php }else { 	
									?>  
										<tr>
											<td colspan="8">
												Not found!
											</td>
										</tr>
									<?php		}?> 
								</table>   
						</div>
						 <div class="row hidden">
							<?php echo form_close(); ?>
							<div class="col-md-6 col-sm-12">
								 <div class="dataTables_paginate paging_bootstrap_full_number">
									<?php echo $page; ?>
								 </div>
							</div>
						</div>
					</div> 
			</div> 
		</div> 
</div> 
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	<script>
			$(document).ready(function(){
				$(".sort-table").tablesorter({
					 headers: {  
			                    4: { 
			                        sorter: false 
			                    }, 
			                    0: { 
			                        sorter: false 
			                    }
			            }
				}); 
			});
			
			$("#category_name").on("change",function(){
				var val = $(this).val() - 0;
				$(".discount").each(function(){
					if (!isNaN(val)) {
						$(this).val(val);
					}else{
						$(this).val(0);
					}
				}); 
			}); 
			$('.timepicker').timepicker({
				showMeridian: false,
				format: 'HH:mm',
				showSeconds: true,
				minuteStep: 1,
				secondStep: 1
			});
		</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	