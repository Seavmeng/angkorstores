<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $view_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->

						<!-- portlet-body -->
						<div class="portlet-body">

							<div class="row">
								<div class="col-md-12 col-sm-12">
									<?php echo form_open(base_admin_url().$controller_url.'search', array('role' => 'form','method'=>'get')); ?>
									
										<div class="form-group col-md-3">
												<?php echo form_label(word_r('category_name'), 'category_name',array('class' => 'control-label')); ?>
												<?php echo form_input(array('name' => 'category_name', 'class' => 'form-control', 'id' => 'category_name',  'value' => $this->input->get('category_name') ) ); ?>											
										</div>
										
										<div class="form-group col-md-2">
												<label for="" class="control-label">&nbsp;</label>
												<input type="submit" class="btn btn blue form-control" value="<?php word('search'); ?>">											
										</div>
										
									<?php echo form_close(); ?>
								</div>
							</div>
							
							<div class="table-scrollable">

							
							<?php
								echo form_open(base_admin_url().$controller_url.'ml', array('role' => 'form')); 
								echo $data; 
							 ?>
							</div>
							 <div class="row">

								<div class="col-md-6 col-sm-12">
									<div class="col-sm-12">
										<div class="col-sm-7 dataTables_info" id="sample_1_info" role="status" aria-live="polite">
										<?php echo $option; ?>
										</div>

										<div class="option col-sm-5">
												
											
										</div>
										
									</div>
								</div>
								<?php echo form_close(); ?>
								<div class="col-md-6 col-sm-12">
									 <div class="dataTables_paginate paging_bootstrap_full_number">
									 	<?php echo $page; ?>
									 </div>
						             
								</div>
							</div>
						</div>
						<!-- end portlet-body -->
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	<script>
			$(document).ready(function(){
				$(".sort-table").tablesorter({
					 headers: {  
			                    4: { 
			                        sorter: false 
			                    }, 
			                    0: { 
			                        sorter: false 
			                    }
			            }
				}); 
			});
		</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	