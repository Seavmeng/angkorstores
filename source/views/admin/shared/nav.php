<nav class="mainnav navbar navbar-inverse navbar-embossed navbar-fixed-top" role="navigation" id="mainNav">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
			<span class="sr-only"><?php echo $this->lang->line('nav_toggle_navigation')?></span>
		</button>
		<a class="navbar-brand" href="<?php echo base_url();?>">
			<!--<img src="<?php echo base_url();?>images/logo.png" style="height: 30px; position: relative; top: -3px; margin-right: 5px;">-->
			<?php //echo $this->lang->line('nav_name')
			echo '<img src="'.base_url().'images/logo.png" width="175px">';
			?>
		</a>
	</div>
	<div class="collapse navbar-collapse" id="navbar-collapse-01">
		
		<ul class="nav navbar-nav">
			<li class="active">
				<a href="#"><i class="fa fa-shopping-cart"></i> Cart List</a>
			</li>
		</ul>
		<ul class="nav navbar-nav navbar-right" style="margin-right: 20px;">
			<li class="dropdown">
				<?php
					$u = $this->ion_auth->user()->row();
				?>
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi, <?php //echo $u->first_name." ".$u->last_name;?> <b class="caret"></b></a>
				<span class="dropdown-arrow"></span>
			  	<ul class="dropdown-menu">
			    	<li><a href="#accountModal" data-toggle="modal"><span class="fui-cmd"></span> <?php //echo $this->lang->line('nav_myaccount')?></a></li>
			    	<li class="divider"></li>
			    	<li><a href="<?php //echo base_url().'account/logout'?>"><span class="fui-exit"></span> <?php echo $this->lang->line('nav_logout')?></a></li>
			  	</ul>
			</li>			      
		</ul>	      
	</div><!-- /.navbar-collapse -->
</nav><!-- /navbar -->