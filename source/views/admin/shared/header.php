<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>My shop
	<?php //if( isset($pageTitle) ){ echo $pageTitle; } else { echo $this->lang->line('alternative_page_title'); }?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Loading Bootstrap -->
	<link href="<?php echo base_url();?>bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/datepicker.css" rel="stylesheet">
	<link href="<?php echo base_url();?>bootstrap/css/bootstrap-timepicker.css" rel="stylesheet">
	<link href="<?php echo base_url();?>bootstrap/jquery-tags-input/jquery.tagsinput.css" rel="stylesheet">
	<!--link href="<?php echo base_url();?>bootstrap/jquery-ui/jquery-ui.css" rel="stylesheet" /-->

	<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.minicolors.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/build.css"/>
    
	<link href="<?php echo base_url();?>css/fileinput.css" rel="stylesheet">
	<!--<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">-->
		
	<!-- Loading Flat UI -->
	<link href="<?php echo base_url();?>less/flat-ui.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/style.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/login.css" rel="stylesheet">
	
	<link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet">
	
	<?php if( isset($builder) ):?>
	<link href="<?php echo base_url();?>css/builder.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/spectrum.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/chosen.css" rel="stylesheet">
	<link href="<?php echo base_url();?>js/redactor/redactor.css" rel="stylesheet">
	<?php endif;?>
	
	
	<!------------ Ckeditor -------------->
	<link rel="shortcut icon" href="<?php echo base_url();?>images/favicon.png">
	<script src="<?php echo base_url();?>ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url();?>ckeditor/samples/js/sample.js"></script>
	<!--script type="text/javascript" src="<?php echo base_url();?>ckfinder/ckfinder.js"></script-->
	<!------------ Ckeditor -------------->
	
	<!------------ Group pro ------------->
	<link href="<?php echo base_url();?>css/template_shop.css" rel="stylesheet">
</head>