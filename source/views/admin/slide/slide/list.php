<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> <?php echo $htitle; ?></span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php echo btn_actions($controller_url.'create',$controller_url, $view_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
	                                         <?php 
							if(isset($errors)){
								echo show_msg($errors);
							}
							if(isset($rmsg)){
								echo show_msg($rmsg);
							}
						// $catFilter = catForFilter();
		
						?>
						<?php echo $this->session->flashdata('msg'); ?>
						<!-- portlet-body -->
						<div class="portlet-body">

							

							<?php
								echo form_open(base_admin_url().$controller_url.'ml', array('role' => 'form')); 

								$this->table->set_heading($check_ml,word_r('photo'), word_r('title'),word_r('description'),'Create Date',array('data' => word_r('action'),'width'=>'140px'));
								$i =0;
								foreach($data as $value){
									$dir ='10-2016';
									$id = $value['slide_id'];
									$image = $value['photo'];
									$title = $value['title'];
									$description=$value['description'];
									//$username = $value['username'];
									$publish_date = $value['create_date'];
									//$active = $value['visibility'];
									$this->table->add_row(
															form_checkbox(array('name'=>'id[]','value'=>$id,'class'=>'checkboxes')),
															admin_image_gallery($image,$title),
															anchor(base_admin_url().$controller_url.'update/'.$id, $title, array('title'=>$title)),
															anchor(base_admin_url().$controller_url.'update/'.$id, $description, array('description'=>$description)),
															//anchor(base_admin_url().$controller_url.'search?user_id='.$value['user_id'], $username, array('title'=>$username)),
															//get_tag_join_link($id),
															//get_category_join_link($id),
															"$publish_date",
															//check_visibility($active),
															btn_action($controller_url.'update/'.$id, $controller_url.'delete/'.$id)
														);
								}

								echo $this->table->generate();
								
							 ?>
							 <div class="row">

								<div class="col-md-6 col-sm-12">
									<div class="col-sm-12">
										<div class="col-sm-7 dataTables_info" id="sample_1_info" role="status" aria-live="polite">
											<?php echo $showing; ?>
										</div>

																
									</div>
								</div>
								<?php echo form_close(); ?>
								<div class="col-md-6 col-sm-12">
									 <div class="dataTables_paginate paging_bootstrap_full_number">
									 	<?php echo $page; ?>
									 </div>
						             
								</div>
							</div>
						</div>
						<!-- end portlet-body -->
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->

<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
	<script>
			$(document).ready(function(){
				$(".sort-table").tablesorter({
			            headers: { 			                   
			                    0: { 
			                        sorter: false 
			                    },
			                    1: { 
			                        sorter: false 
			                    },
			                    7: { 
			                        sorter: false 
			                    }, 
			                    8: { 
			                        sorter: false 
			                    }
			            }

          }); 

			});
		</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
	