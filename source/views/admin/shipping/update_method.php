<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div id="show_msg"></div>
			<?php 
				if(isset($errors)){
				   echo show_msg($errors);
				}
				if(isset($test)){
				   echo $test;exit;
				}
			 ?>
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php //echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> Add New Product</span>
							</div>
							<!-- end caption -->
							<!-- <input type="hidden" id="shipp_id" value="<?php echo $ship_name_id;?>"/> -->
							<!-- actions -->
							<div class="actions">
								<?php //echo btn_actions('admin/product_list/create','account/product_list/create', $create_action); ?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open_multipart(base_admin_url().'shipping/update_md', array('role' => 'form') ); ?>
								<!-- portlet-body -->
									<div class="portlet-body">
							         <!-- Row -->
							         <div class="row">
							         		 <!-- Form -->
							         		<div class="col-md-8">	
							         			<!--Form-body -->
													<div class="form-body">
														<!-- ROW -->
														<input class="form-control" type="hidden" name="method_id" value="<?php echo $md_update['method_id']; ?>" />
														<input class="form-control" type="hidden" name="price_id" value="<?php echo $md_update['shipping_price_id']; ?>">
														<div class="row">
															<div class="form-group">
																<!-- col-md-6 -->
																<div class="col-md-12">
																	<div class="form-group required <?php has_error('method_name'); ?>">
																		<?php echo '<label>Shipping method</label>';?>
																		<?php echo form_input('method_name', $md_update['method_name'],"class = 'form-control' id= 'method_name'") ?>
																		<font color="red"><?php echo form_error('method_name'); ?></font>
																	</div>
																</div>
															</div>
															<!--End col-md-6 -->
														</div>
														<div class="row">
															<div class="form-group">
																<!-- col-md-6 -->
																<div class="col-md-4">
																	<select class="form-control" name="city" id="city">
																	<?php if($md_update['zone_name'] == 'country'){ ?>
																		<option value="">---please select option---</option>
																		<option value="country" selected="selected">Country</option>
																		<option value="zone">Shipping Zone</option>
																	<?php }elseif ($md_update['zone_name'] == 'zone'){ ?>
																		<option value="">---please select option---</option>
																		<option value="country">Country</option>
																		<option value="zone" selected="selected">Shipping Zone</option>
																	<?php }else{ ?>
																		<option value="">---please select option---</option>
																		<option value="country">Country</option>
																		<option value="zone">Shipping Zone</option>
																	<?php }; ?>
																	</select>
																</div>
																<div class="col-md-4">
																	<select class="form-control" name="compare" id="compare">
																	<?php if($md_update['compare'] == 'equal'){ ?>
																		<option value="equal" selected="selected">Equal To</option>
																		<option value="greater_or_equal">Greater or equal to</option>
																		<option value="less_or_equal">Less or equal to</option>
																	<?php }else if($md_update['compare'] == 'greater_or_equal'){ ?>
																		<option value="equal">Equal To</option>
																		<option value="greater_or_equal"  selected="selected">Greater or equal to</option>
																		<option value="less_or_equal">Less or equal to</option>
																	<?php }else if($md_update['compare'] == 'less_or_equal'){ ?>
																		<option value="equal">Equal To</option>
																		<option value="greater_or_equal">Greater or equal to</option>
																		<option value="less_or_equal"  selected="selected">Less or equal to</option>
																	<?php }; ?>
																	</select>
																</div>
																<div class="col-md-4" id="contain_zone">
																	<select class="form-control" id="zone" name="zone">
																	<?php if($md_update['zone_name'] == 'country'){ ?>
																		<option value="<?php echo $md_update['location_id']; ?>" selected="selected"><?php echo $md_update['location_id']; ?></option>
																		<option value="">Shipping Zone</option>
																	<?php }else if($md_update['zone_name'] == 'zone'){ ?>
																		<option value="">Country</option>
																		<option value="<?php echo $md_update['location_id']; ?>" selected="selected"><?php echo $md_update['location_id']; ?></option>
																	<?php }else{ ?>
																		<option value="">Country</option>
																		<option value="">Shipping Zone</option
																	<?php }; ?>
																	</select>
																</div>
															</div>
															<!--End col-md-6 -->
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="sec_title">Advanced Pricing</div>
															</div>
														</div>
														
														<!-- <div class="row">
																																														
															<div class="form-group">													
																<div class="col-md-3">
																	<label>Min Weight:</label>
																	<input class="form-control minweight" type="text" name="minweight[]" value="<?php echo $md_update['min_weight']; ?>" />
																</div>
																<div class="col-md-3">
																	<label>Max Weight:</label>
																	<input class="form-control maxweight" type="text" name="maxweight[]" value="<?php echo $md_update['max_weight']; ?>" />
																</div>
																<div class="col-md-3">
																	<label>Fee Amount:</label>
																	<input class ="form-control feeamount" type="text" name="feeamount[]" value="<?php echo $md_update['fee_amount']; ?>" />
																</div>																
																<div class="col-md-3">
																	<br>
																	<a class="btn btn-primary btn-labeled fa fa-plus-circle add_cond" href="#"/>
																		Add New
																	</a>
																</div>
															</div>
															</div> -->
															<div class="row">
															<div class="form-group">
																<div class="col-md-3">
																	<label>Min Weight:</label>
																	<input class="form-control minweight" type="text" name="minweight[]" />
																	
																</div>
																<div class="col-md-3">
																	<label>Max Weight:</label>
																	<input class="form-control maxweight" type="text" name="maxweight[]"/>

																</div>
																<div class="col-md-3">
																	<label>Fee Amount:</label>
																	<input class ="form-control feeamount" type="text" name="feeamount[]"/>
																</div>
																<div class="col-md-3">
																	<br>
																	<a class="btn btn-primary btn-labeled fa fa-plus-circle add_cond" href="#" />
																		Add New
																	</a>
																</div>
															</div>
														</div>

															<?php 
																$update_price=update_price($md_update['method_id']);
																foreach ($update_price as $key){ 

															?>
															<div class="row" id="re">
															
																<div class="form-group">
																	<div class="col-md-3">
																		<label></label>
																		<input class="form-control minweight" type="text" name="min[]" value="<?php echo $key['min_weight']; ?>" />
																	</div>
																	<div class="col-md-3">
																		<label></label>
																		<input class="form-control maxweight" type="text" name="max[]" value="<?php echo $key['max_weight']; ?>" />
																	</div>
																	<div class="col-md-3">
																		<label></label>
																		<input class ="form-control feeamount" type="text" name="fee[]" value="<?php echo $key['fee_amount']; ?>" />
																	</div>
																	<input type="hidden" name="shipping_price_id[]" value="<?php echo $key['shipping_price_id']; ?>">
																	<div class="col-md-3">
																		<br/><a href="<?php echo base_url()."admin/shipping/delete_price/" .$key['shipping_price_id']; ?>" class="btn default btn-xs red rem_cond"  title="Delete"><i class="fa fa-minus"></i> Remove </a>

																	</div>
																</div>
																
															</div>
															<?php }; ?>

													
														
														<div class="row">
															<div class="col-md-12 add_morecod">
															</div>
														</div>
														<div class="row">
															<div class="col-md-12">
																<div class="sec_title">Shipping Day:</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group">
																<div class="col-md-4">
																	<label>Between From:</label>
																	<?php echo form_input('from_day', $md_update['from_day'],"class = 'form-control' id='from_day'") ?>
																	<font color="red"><?php echo form_error('from_day'); ?></font>
																</div>
																<div class="col-md-4">
																	<label>To:</label>
																	<?php echo form_input('to_day', $md_update['to_day'],"class = 'form-control' id='to_day'") ?>
																	<font color="red"><?php echo form_error('to_day'); ?></font>
																</div>
															</div>
														</div>
													</div>
													<!--End form-body -->
							         		</div>
							         		<!-- End Form -->
							         		<!-- Action -->
							         		<div class="col-md-4">
							         			
							         			<div class="panel panel-default">
													<div class="widget_section">
														<div class="ui-radio ui-radio-primary">
															<div class="panel-heading">
																<div class="widget_title"><h3 class="panel-title">Public</h3></div>
															</div>
															<div class="panel-body">
																<div>
																	<div class="clearfix">
																		<input type="submit" name="btn_save" id="btn_save" value="submit" class="btn btn-primary">
																		
																		<a href="<?php echo base_url().'admin/shipping/all_method';?>"><input type="button" name="btn_update" value="Cancel" class="btn btn-danger"></a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
							         		</div>
							         		<!-- End Action -->
							         </div>
							          <!-- End Row -->
							         




									</div>
								<!-- end portlet-body -->
							
						<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->
<script>
    var base_admin_assets_url = "<?php echo base_admin_assets_url(); ?>";
</script>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
  //tag
    $('.tags-input').tagsInput({
        width: 'auto',
        //autocomplete_url:'test/fake_plaintext_endpoint.html' //jquery.autocomplete (not jquery ui)
        autocomplete_url:"<?php echo base_admin_url().$controller_url.'save'; ?>" // jquery ui autocomplete requires a json endpoint
      });
</script>
<script type="text/javascript">
	$(document).ready(function() {		
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			startDate: '-3d'
		});
		$('.timepicker').timepicker({
			showMeridian: false,
			format: 'HH:mm',
			showSeconds: true,
			minuteStep: 1,
			secondStep: 1
		});
		
		$("#have_disc").click(function(){
			$(".show_have_dis").toggle(400);
		});
		$('#select').on('click',function(){
			if ($(this).is(':checked')) {
				$('.categories').each(function(){
					this.checked = true;
				});
			}else{
				$('.categories').each(function(){
					this.checked = false;
				});
			}
		});
		$("#fileupload").change(function () {
			if (typeof (FileReader) != "undefined") {
				var dvPreview = $("#dvPreview");
				dvPreview.html("");
				var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
				$($(this)[0].files).each(function () {
					var file = $(this);
					if (regex.test(file[0].name.toLowerCase())) {
						var reader = new FileReader();
						reader.onload = function (e) {
							var img = $("<img />");
							img.attr("style", "height:100px;width: 100px");
							img.attr("src", e.target.result);
							dvPreview.append(img);
						}
						reader.readAsDataURL(file[0]);
					} else {
						alert(file[0].name + " is not a valid image file.");
						dvPreview.html("");
						return false;
					}
				});
			} else {
				alert("This browser does not support HTML5 FileReader.");
			}
		});
		
		//################### Get value from select ###################//
		var maxappend = 0;
		$('.add_atr').click(function() {
			//alert('he');
			var id = $('#attr_name').val();
			//alert(id);
			if(id == 9){
				$.ajax({
					type:'POST',
					data:{id:id},
					url:'<?php echo site_url('/admin/product_list/attributes')?>',
					success: function(e){
						$('.parent').append(e);
					}
				});
			}
			if(id == 8){
				$.ajax({
					type:'POST',
					data:{id:id},
					url:'<?php echo site_url('/account/product_list/attributes')?>',
					success: function(e){
						$('.parents').append(e);
					}
				});
			}
		});
		

		// attach button click listener on dom ready
		$('.save').click(function(){
			var group = $('#attr_name').val();
			var color = $('input[name^=userfile]').map(function(idx, elem) {
				return $(elem).val();
			}).get();
			var price = $('input[name^=p_color]').map(function(id, eleme) {
				return $(eleme).val();
			}).get();
			var size = $('input[name^=size]').map(function(ids, elems) {
				return $(elems).val();
			}).get();
			
			if(group == 9){
				$('input[name^=str_size]').val(group + ',' + size)
			}
			if(group == 8){
				$('input[name^=str_color]').val(group + ',' + color + '/' + price)
			}

			//alert(group + ',' + color + ',' +price);
			event.preventDefault();
		});
		
		$('#attr_name').change(function(){
			$('.save').show(200);
			//$('#show_attr div').empty();
		});
		
		//####################### Product tags #######################//
		$('.add_tags').click(function(){
			var tag_id = $("#get_tags").val();
			if(tag_id ==""){
				return false;
			}else{
				var list='<li><a class="button_grey" href="#">'+tag_id+'</a>'+
						'<i class="tag_re glyphicon glyphicon-remove-circle"></i>'+
						'<input type="hidden" name="tags_con[]" value="'+tag_id+'"></li>';
				if(maxappend >=10) return;
				$(".tags_container ul").append(list);
				maxappend++;
			}
		});
		$(document).on('click','.tag_re', function(){
			$(this).parent('.tags_container ul li').remove();
		});
		$(document).on('click','.att_re', function(){
			$(this).closest('.att_on').remove();
			$(this).closest('.size_on').remove();
			//alert('ge');
		});
	});
	// $(document).ready(function(){
	// 	$('.rem_cond').click(function(event)){
	// 	$('#re').remove();
	// }
	// });
	/* comfirm delete */
	// $('.confirm-delete').click(function() {
 //        var id = $(this).data('id');
 //        bootbox.confirm("Are you sure that you want to delete this Vehicles?", function(result) {
 //            if (result) {
 //                document.location = '<?php echo base_url();?>admin/shipping/delete_price' + id + "/<?php echo $this->uri->segment(2); ?>";
 //            }
 //        });
 //    });
	//CKEDITOR.replace('ckeditor');
</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
