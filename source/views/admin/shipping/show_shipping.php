<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-8">
				<h3><span class="fui-document"></span>Manage Shipping Company</h3>
			</div><!-- /.col -->
			<div class="col-md-3 col-sm-4 text-right"><h3><?php //echo $success; ?></h3></div><!-- /.col -->
		</div><!-- /.row -->
		<?php 
			if(isset($errors)){
			   echo show_msg($errors);
			}
			if(isset($rmsg)){
			   echo show_msg($rmsg);
			}
		 ?>
		<hr class="dashed margin-bottom-30">
		<div class="panel panel-default">
			<table class="table" id="product-list">
				<tr>
					<td style="border-bottom: 1px solid #ebebeb;">
						<a class="btn btn-primary btn-labeled fa fa-plus-circle pull-right mar-rgt category" data-toggle="modal" href="<?php echo  base_url().'admin/shipping/add_new_shipping'?>">
							New Shipping
						</a>
					</td>
				</tr>
				<tr>
					<td>
						<div class="panel panel-default">
							<table data-show-export="true" data-search="true" data-show-refresh="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-pagination="true" data-side-pagination="server" data-url="#" class="table table-striped table-bordered table-hover" id="events-table">
								<thead>
									<tr>
										<th style="text-align: center;">No</th>
										<th style="text-align: center;">Image</th>
										<th style="text-align: center; ">Shipping Name</th>
										<th style="text-align: center; ">Create Date</th>
										<th style="text-align: center; ">Options</th>
									</tr>
								</thead>
								
								<tbody>
									<?php
										$i=0;
										foreach($data as $rows){
											
											$shipping_id = $rows['shipping_id'];
											$image = $rows['image'];
											$shipping_name = $rows['shipping_name'];
											$public_date = $rows['created_date'];

											$dir = date('m-Y', strtotime($rows['created_date']));
											$i++;
												$img_path = 'images/products/'.$image; 
												if (file_exists($img_path)) {
													$images = $img_path;
												} else {
													$images = 'images/'.$image; 
												}
									?>
									<tr> 
										<td style="text-align: left; ">
											<?php echo $i;?>
										</td> 
										<td style="text-align: center; ">
											<img src="<?php echo base_url($images);?>" style="width:60px;height:60px;border:1px solid #ddd;padding:2px; border-radius:2px !important;" class="img-sm">
										</td>
										<td style="text-align: left; "><?php echo $shipping_name;?></td>
										<td style="text-align: left; "><?php echo $public_date;?></td>
										<td style="text-align: right; ">
											<?php echo btn_action('shipping/edit_new_shipping/'.$shipping_id,'shipping/delete_shipping/'.$shipping_id);?>
											<a class="btn btn-primary btn-labeled fa fa-plus-circle btn-xs" href="<?php echo base_admin_url().'shipping/all_method/'.$shipping_id;?>"> Show all</a>
										</td> 
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div><!---Contain--->	
	
	<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
function show_data(){
 $('.show_content').load('test');
 // $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
$(".send_msg").on('click',function(){
	var send_msg=$(this).attr('id');
	var name=$('#msg_name').val();
	var email=$('#msg_email').val();
	var phone= $('#msg_phnoe').val();
	var subject= $('#msg_subject').val();
	var message= $('#msg_message').val();
	$.ajax({
		url: "http://localhost:81/ci3/admin/dashboard/message",
		type: 'POST',
		data: {
			'name':name,
			'email':email,
			'phone':phone,
			'subject':subject,
			'message':message
		},
		success: function(msg) {
			if(msg == 'YES'){
				alert('yes');
			}else if(msg == 'No'){
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}else{
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
		}
	});
});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>

</html>