<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php //echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> Add Sub Shipping</span>
							</div>

							<div class="actions">
								<?php //echo btn_actions('admin/product_list/create','account/product_list/create', $create_action); ?>
							</div>
						</div>
						<?php echo form_open_multipart('admin/shipping/add_zone', array('role' => 'form') ); ?>
					
						
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
							         ?>
							         <!-- Row -->
							         <div class="row">
							         		 <!-- Form -->
							         		<div class="col-md-8">	
							         			<!--Form-body -->
													<div class="form-body">
														<!-- ROW -->
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																<div class="form-group">
																	<label for="zone_name">Shipp Name</label>
																	  <select name="shipp" class="selectpicker form-control" data-style="btn-primary">
																	  	<?php 
																	  		foreach ($shipp as $ship) {
																	  	?>
																	  		<option value="<?php echo $ship['shipping_id']; ?>" required><?php echo $ship['shipping_name']; ?></option>
																	  	<?php 
																	  		}
																	  	 ?> 
																	  </select>
										                         </div>
										                    </div>   
										       			</div> 
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																<div class="form-group">
																	<label for="zone_name">Zone Name</label>
										                            <input type="text" class="form-control" name="zone_name" id="zone_name" required />
										                            <span style="color:red;"><?php echo form_error('zone_name'); ?></span>								                        
																</div>
															</div>
															<!--End col-md-6 -->
														</div>
	
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
															 <div class="form-group">
																<label for="description">Description</label>
																<textarea class="form-control" rows="10" id="description" name="description"></textarea>
																<span style="color:red;"><?php echo form_error('description'); ?></span>
															</div>
	
															</div>
															<!--End col-md-6 -->
														</div>
														
														<!-- End row -->
														
													</div>
													<!--End form-body -->
							         		</div>
							         		<!-- End Form -->
							         		<!-- Action -->
							         		<div class="col-md-4">
							         			
							         			<div class="panel panel-default">
													<div class="widget_section">
														<div class="ui-radio ui-radio-primary">
															<div class="panel-heading">
																<div class="widget_title"><h3 class="panel-title">Public</h3></div>
															</div>
															<div class="panel-body">
																<div>
																	<div class="clearfix">
																		<input type="submit" name="btn_save" value="submit" class="btn btn-primary">
																		
																		<a href="<?php echo base_url().'admin/shipping/zone';?>" required><input type="button" name="btn_update" value="Cancel" class="btn btn-danger"></a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

							         		</div>
							         		<!-- End Action -->
							         </div>
							          <!-- End Row -->							         
									</div>							
						<?php echo form_close(); ?>						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->
<script>
    var base_admin_assets_url = "<?php echo base_admin_assets_url(); ?>";
</script>
<script type="text/javascript">
		$(document).ready(function(){
			$("#ss").click(function(){
		    if($('input[name=subjects\\[\\]]:checked').length<=0)
		    {
		       alert("No radio checked")
		    }
		});
	});
</script>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
