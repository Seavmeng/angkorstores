<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
				<!-- portlet-title -->
				<div class="portlet-title">
					<!-- caption -->
					<div class="caption">
						<i class="<?php //echo $icon; ?>"></i>
						<span class="caption-subject bold uppercase">Edit Shipping Company</span>
					</div> 
				</div>
				<?php echo form_open_multipart('admin/shipping/edit_new_shipping/'.$id, array('role' => 'form') ); ?>
			 
			<!-- portlet-body -->
				<div class="portlet-body">
				<?php 
					if(isset($errors)){
					   echo show_msg($errors);
					}
				 ?>
				 <!-- Row -->
				 <div class="row">  
						<div class="col-md-8">	 
								<div class="form-body"> 
									<div class="row"> 
										<div class="col-md-12">
											<div class="form-group">
												<label for="shipping_name">Shipping Name</label>
												 <input value="<?php echo set_value('shipping_name',$result->shipping_name);?>" type="text" class="form-control" name="shipping_name" />
												 <span style="color:red;"><?php echo form_error('shipping_name'); ?></span>
											 </div>
										</div>   
									</div> 
									<div class="row"> 
										<div class="col-md-12">
										 <div class="form-group">
											<label for="description">Description</label>
											<textarea class="form-control" rows="10" id="description" name="description"> <?php echo set_value('description',$result->description);?></textarea>
											<span style="color:red;"><?php echo form_error('description'); ?></span>
										</div>

										</div> 
									</div>
									<div class="row">   
										<div class='col-md-12'>
										<div class="panel panel-default">
												<!-- panel-heading -->
												<div class="panel-heading">
														<h3 class="panel-title"> Company Logo (Recommend size 150 x 150 pixels) </h3>
												</div>
												<!-- end panel-heading --> 
												<!-- panel-body -->
												<div class="panel-body">
												 
													<div class="form-group" style="height:100%; margin:0;">
														<div class="fileinput fileinput-exists" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="width: 265px; height: 160px;">
																<img data-src="holder.js/100%x100%" alt="">
															</div> 
															<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 400px; max-height: 200px;">
																	<?php  
																			$img_path = 'images/products/'.$result->image; 
																			if (file_exists($img_path)) {
																				$images = $img_path;
																			} else {
																				$images = 'images/'.$result->image; 
																			}
																	?> 
																	<img src="<?php echo base_url($images);?>" data-src="holder.js/100%x100%" alt="image">
																	<input type="hidden" name="photo" value="" id="image">
															</div>
															<div>
																<span class="btn btn-default btn-file">
																	<span class="fileinput-new">Select image</span>
																	<span class="fileinput-exists">Change</span>
																	<input type="file" name="userfile[]">
																</span>
																<a href="#" class="btn btn-default fileinput-exists remove" data-dismiss="fileinput">Remove</a>
															</div>
														</div> 
													</div> 
												</div>  
											</div>
										</div>
									</div>  
								</div>
								<!--End form-body -->
						</div>
									<!-- End row -->
						<!-- End Form -->
						<!-- Action -->
						<div class="col-md-4"> 
							<div class="panel panel-default">
								<div class="widget_section">
									<div class="ui-radio ui-radio-primary">
										<div class="panel-heading">
											<div class="widget_title"><h3 class="panel-title">Public</h3></div>
										</div>
										<div class="panel-body">
											<div>
												<div class="clearfix">
													<input type="submit" name="btn_save" value="submit" class="btn btn-primary">
													
													<a href="<?php echo base_url().'admin/shipping';?>" required><input type="button" name="btn_update" value="Cancel" class="btn btn-danger"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>  
						</div>
						<!-- End Action -->
				 </div>
				  <!-- End Row -->							         
				</div>							
				<?php echo form_close(); ?>						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->
<script>
    var base_admin_assets_url = "<?php echo base_admin_assets_url(); ?>";
</script>
<script type="text/javascript">  
</script>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
