<?php $this->load->view("account/shared/header.php");?>
<body>
	<?php $this->load->view("account/shared/nav.php");?> 
	
	<div class="container-fluid">
    	<div class="row">
    		<div class="col-md-9 col-sm-8">
		    	<h3><span class="fui-document"></span>Images Library</h3>
    		</div><!-- /.col -->
    		<div class="col-md-3 col-sm-4 text-right"></div><!-- /.col -->
    	</div><!-- /.row -->
    	<hr class="dashed margin-bottom-30">
		<div class="row">
			<div class="col-md-12">
				<div class="col-lg-12 bhoechie-tab-container">
					<div class="col-lg-1 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
						<div class="list-group">
							<a href="#" class="list-group-item active text-center">
								<i class="glyphicon glyphicon-film"></i>
								<br>Slide
							</a>
							<a href="#" class="list-group-item text-center">
								<i class="fa fa-comments"></i>
								<br>Promotion
							</a>
							<a href="#" class="list-group-item text-center">
								<i class="glyphicon glyphicon-star"></i>
								<br>Rate
							</a>
						</div>
					</div>
					<?php echo form_open_multipart('account/product_list/images', array('role' => 'form')); ?>
					<?php
						foreach($images as $img_all){
							$slide = $img_all['img_slide'];
							$promotion = $img_all['img_promotion'];
							$rate = $img_all['img_rate'];
						}
					?>
					<div class="col-lg-8 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
						<!-- flight section -->
						<div class="bhoechie-tab-content active">
							<div class="row">
								<div class="col-lg-12 col-md-5 col-sm-8 col-xs-9">
									<div class="panel penel-default">
										<div class="panel-heading">
											Add Image Slide 
										</div>
										<div class="panel-body">
											<div class="gallery_">
												<div class="fileinput fileinput-<?php echo $slide == ''? 'new':'exists';?>" data-provides="fileinput">
													<div class="fileinput-new thumbnail" style="width: 100%; height: 150px;">
														<img data-src="holder.js/100%x100%" alt="">
													</div>
													<div id="dvPreview">
														<?php
															$eximg = explode(',',$slide);
															foreach($eximg as $simg){
																$slideimg = cut_string($simg);
														?>
														<img class="img-thumbnail" src="<?php echo base_url().'images/products/'.$slideimg;?>" />
														<?php
																}
														?>
														<input type="hidden" name="slide" value="<?php echo $slide;?>" >
													</div>
													<div>
														<span class="btn btn-default btn-file">
														<span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
														<input id="fileupload" type="file" multiple="multiple" class="file" name="userfile[]">
														</span>
														<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- train section -->
						<div class="bhoechie-tab-content">
							<div class="row">
								<div class="col-lg-12 col-md-5 col-sm-8 col-xs-9">
									<div class="panel penel-default">
										<div class="panel-heading">
											Add Promotion image
										</div>
										<div class="panel-body">
											<div class="gallery_">
												<div class="fileinput fileinput-<?php echo $promotion == ''? 'new':'exists';?>" data-provides="fileinput">
													<div class="fileinput-new thumbnail" style="width: 100%; height: 150px;">
														<img data-src="holder.js/100%x100%" alt="">
													</div>
													<div id="SePreview">
														<?php
															$expromotion = explode(',',$promotion);
															foreach($expromotion as $spromotion){
																$proimg = cut_string($spromotion);
														?>
														<img class="img-thumbnail" src="<?php echo base_url().'images/products/'.$proimg;?>" />
														<?php
																}
														?>
														<input type="hidden" name="promotion" value="<?php echo $promotion;?>" >
													</div>
													<div>
														<span class="btn btn-default btn-file">
														<span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
														<input id="fileuploads" type="file" multiple="multiple" class="file" name="gallery[]">
														</span>
														<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
			
						<!-- hotel search -->
						<div class="bhoechie-tab-content">
							<div class="row">
								<div class="col-lg-12 col-md-5 col-sm-8 col-xs-9">
									<div class="panel penel-default">
										<div class="panel-heading">
											Add rate image
										</div>
										<div class="panel-body">
											<div class="gallery_">
												<div class="fileinput fileinput-<?php echo $rate == ''? 'new':'exists';?>" data-provides="fileinput">
													<div class="fileinput-new thumbnail" style="width: 100%; height: 150px;">
														<img data-src="holder.js/100%x100%" alt="">
													</div>
													<div id="dvPreviewed">
														<?php
															$exrate = explode(',',$rate);
															foreach($exrate as $sexrate){
																$rateimg = cut_string($sexrate);
																if($rate == ""){
																	
																}else{
														?>
														<img class="img-thumbnail" src="<?php echo base_url().'images/products/'.$rateimg;?>" />
														<?php
																}
															}
														?>
														<input type="hidden" name="rate" value="<?php echo $rate;?>" >
													</div>
													<div>
														<span class="btn btn-default btn-file">
														<span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
														<input id="fileuploaded" type="file" multiple="multiple" class="file" name="img[]">
														</span>
														<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
						<div class="row save-area">
							<div class="panel penel-default">
								<div class="panel-heading">
									Public
								</div>
								<div class="panel-body">
									<div>
										<div class="clearfix">
											<input type="submit" name="btn_image" value="Edit" class="btn btn-primary">
											
											<a href="<?php echo base_url().'account/sites/template';?>"><input type="button" name="btn_update" value="Cancel" class="btn btn-danger"></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Load JS here for greater good =============================-->
	<script src="<?php echo base_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>bootstrap/bootstrap-fileinput/bootstrap-fileinput.js"></script>
	<script>
		$(document).ready(function() {
			$("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
				e.preventDefault();
				$(this).siblings('a.active').removeClass("active");
				$(this).addClass("active");
				var index = $(this).index();
				$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
				$("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
			});
			
			$("#fileupload").change(function () {
				if (typeof (FileReader) != "undefined") {
					var dvPreview = $("#dvPreview");
					dvPreview.html("");
					var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
					$($(this)[0].files).each(function () {
						var file = $(this);
						if (regex.test(file[0].name.toLowerCase())) {
							var reader = new FileReader();
							reader.onload = function (e) {
								var img = $('<img class="img-thumbnail" />');
								img.attr("style", "height:100px;width: 100px");
								img.attr("src", e.target.result);
								dvPreview.append(img);
							}
							reader.readAsDataURL(file[0]);
						} else {
							alert(file[0].name + " is not a valid image file.");
							dvPreview.html("");
							return false;
						}
					});
				} else {
					alert("This browser does not support HTML5 FileReader.");
				}
			});
			
			$("#fileuploads").change(function () {
				if (typeof (FileReader) != "undefined") {
					var SePreview = $("#SePreview");
					SePreview.html("");
					var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
					$($(this)[0].files).each(function () {
						var file = $(this);
						if (regex.test(file[0].name.toLowerCase())) {
							var reader = new FileReader();
							reader.onload = function (e) {
								var img = $('<img class="img-thumbnail" />');
								img.attr("style", "height:100px;width: 100px");
								img.attr("src", e.target.result);
								SePreview.append(img);
							}
							reader.readAsDataURL(file[0]);
						} else {
							alert(file[0].name + " is not a valid image file.");
							SePreview.html("");
							return false;
						}
					});
				} else {
					alert("This browser does not support HTML5 FileReader.");
				}
			});
			
			$("#fileuploaded").change(function () {
				if (typeof (FileReader) != "undefined") {
					var dvPreview = $("#dvPreviewed");
					dvPreview.html("");
					var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
					$($(this)[0].files).each(function () {
						var file = $(this);
						if (regex.test(file[0].name.toLowerCase())) {
							var reader = new FileReader();
							reader.onload = function (e) {
								var img = $('<img class="img-thumbnail" />');
								img.attr("style", "height:100px;width: 100px");
								img.attr("src", e.target.result);
								dvPreview.append(img);
							}
							reader.readAsDataURL(file[0]);
						} else {
							alert(file[0].name + " is not a valid image file.");
							dvPreview.html("");
							return false;
						}
					});
				} else {
					alert("This browser does not support HTML5 FileReader.");
				}
			});
		});
	</script>
</body>
</html>