run
<!--
<li class="col-sx-12 col-sm-4">
	<div class="product-container">
		<div class="left-block">
			<a href="">
				<img class="img-responsive" alt="product" src="<?php echo base_url()?>images/01_blue-dress.jpg" style="height:200px;"/>
			</a>
			<div class="quick-view">
				<a title="Add to my wishlist" class="heart" href="#"></a>
				<a title="Add to compare" class="compare" href="#"></a>
				<a title="Quick view" class="search" href="#"></a>
			</div>
			<div class="add-to-cart">
				<a title="Add to Cart" href="#add">Add to Cart</a>
			</div>
		</div>
		<div class="right-block">
			<h5 class="product-name">
				<a href="">
					test
				</a>
			</h5>
			<div class="product-star">
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-half-o"></i>
			</div>
			<div class="content_price">
				<span class="price product-price">
					$ 230
				</span>
				<span class="price old-price">
					$ 250
				</span>
			</div>
			<div class="info-orther">
				<p>Item Code: #453217907</p>
				<p class="availability">Availability: <span>In stock</span></p>
				<div class="product-desc">
					Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien. Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium. Fusce egestas elit eget lorem. In auctor lobortis lacus. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor.
				</div>
			</div>
		</div>
	</div>
</li>

<!-- ./PRODUCT LIST -->