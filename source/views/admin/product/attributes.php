<link href="<?php echo base_url();?>assets/multi_select/select2.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url();?>assets/multi_select/select2.full.js"></script>
<?php
if($ids==""){
}else{
	if($ids == 8){
?>
	<div class="att_on border_attr">
		<div class="form-group" style="margin:0;">
			<label class="col-sm-2 control-label" for="wheel-price">Color</label>
			<div class="col-sm-3">
				<?php
				echo '<select class="form-control" name="color['.$count.'][color]">';
					$colors=attr_pro($ids);
					foreach($colors as $row){
						echo '<option value="'.$row['attr_id'].'">'.$row['attr_name'].'</option>';
					}
				echo '</select><br/>';
				?>
				<font color="red"></font>
			</div>
			<div class="col-sm-4">
				<?php
				echo '<select class="form-control js_multiple count_row" multiple="multiple" name="color['.$count.'][size][]">
						<option value="">Default Size</option>';
					$colors=attr_pro(9);
					foreach($colors as $row){
						echo '<option value="'.$row['attr_id'].'">'.$row['attr_name'].'</option>';
					}
				echo '</select>';
				?>
				<font color="red"></font>
			</div>
			<div class="col-sm-2">
				<i class="att_re glyphicon glyphicon-trash"></i>
				<br/>
				<br/>
				<br/>
			</div>
		</div>
		<div class="form-group" style="margin:0;">
			<label class="col-sm-2 control-label">Image Color</label>
			<div class="col-sm-10">
				<div class="row">
					<div class="fileinput fileinput-new" data-provides="fileinput">
						<div class="col-md-6">
							<div class="fileinput-new thumbnail" style="width: 100px; height: 60px;">
								<img data-src="holder.js/100%x100%" alt="">
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px;"></div>
						</div>
						<div class="col-md-6">
							<span class="btn btn-default btn-file">
								<span class="fileinput-new">Select image</span>
								<span class="fileinput-exists">Change</span>
								<input type="file" name="img[]">
							</span><br/>
							<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
						</div>
					</div>
				</div>
			</div>
			<font color="red"></font>
		</div>
		<div style="clear:both;height:0;width:0;"></div>
	</div>
<?php
	}else{
?>
	<div class="row size_on border_attr">
		<div class="col-md-8">
			<div class="form-group">
				<label class="col-sm-2 control-label" for="wheel-size">Size</label>
				<?php
				echo '<select class="form-control" name="size[]" id="wheel-size">';
					$colors=attr_pro($ids);
					foreach($colors as $row){
						echo '<option value="'.$row['attr_id'].'">'.$row['attr_name'].'</option>';
					}
				echo '</select><br/>';
				?>
				<font color="red"></font>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<i class="remove glyphicon glyphicon-trash"></i>
			</div>
		</div>
	</div>
<?php
	}
}
?>
<script>
$(document).ready( function() {
	$(".js_multiple").select2();
	$('.demo').each( function() {
		$(this).minicolors({
			control: $(this).attr('data-control') || 'hue',
			defaultValue: $(this).attr('data-defaultValue') || '',
			format: $(this).attr('data-format') || 'hex',
			keywords: $(this).attr('data-keywords') || '',
			inline: $(this).attr('data-inline') === 'true',
			letterCase: $(this).attr('data-letterCase') || 'lowercase',
			opacity: $(this).attr('data-opacity'),
			position: $(this).attr('data-position') || 'bottom left',
			change: function(value, opacity) {
				if( !value ) return;
				if( opacity ) value += ', ' + opacity;
				if( typeof console === 'object' ) {
					console.log(value);
				}
			},
			theme: 'bootstrap'
		});
	});
});
</script>