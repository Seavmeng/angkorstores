<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
<!-- ROW -->
 <div class="row">
	<!-- COL-MD-12 -->
	<div class="col-md-12">
		<!-- portlet -->
		<div class="portlet light bordered">
			<!-- portlet-title -->
			<div class="portlet-title">
				<!-- caption -->
				<div class="caption">
					<i class="<?php //echo $icon; ?>"></i>
					<span class="caption-subject bold uppercase"> Add New Product</span>
				</div>
				<!-- end caption -->

				<!-- actions -->
				<div class="actions">
					<?php //echo btn_actions('admin/product_list/create','account/product_list/create', $create_action); ?>
				</div>
				<!-- end actions -->
			</div>
				<!--end portlet-title -->
                                <?php 
                                $controller = "product_list";  
                                 if($this->uri->segment(3) == 'supplier_product'){
                                     $controller = "supplier_product";  
                                }        
                                ?>
				<?php echo form_open_multipart("admin/product/". $controller."/create" , array('role' => 'form', 'id'=>'product_submit') ); ?>
				<!-- portlet-body -->
				<div class="portlet-body">
				<?php 
					if(isset($errors)){
					   echo show_msg($errors);
					}
				 ?>
				 <!-- Row -->
				 <div class="row">
					 <!-- Form -->
					<div class="col-md-8">	
						<!--Form-body -->
						<div class="form-body">
							<!-- ROW -->
							<div class="row">
								<!-- col-md-6 -->
								<div class="col-md-12">
										<div class="form-group required <?php has_error('Procode'); ?>">
											<?php echo 'Product  Code';?>
											<?php echo form_input('pro_code' ,$this->input->post('pro_code', true), "class='form-control'")?>
											<font color="red"><?php echo form_error('procode'); ?></font>
										</div>
								</div>
								<!--End col-md-6 -->
							</div>
							<div class="row">
								<!-- col-md-6 -->
								<div class="col-md-12">
										<div class="form-group required <?php has_error('product_name'); ?>">
											<?php echo 'Product Name';?>
											<?php echo form_input('product_name', $this->input->post('product_name', true),"class = 'form-control'"); ?>
											<font color="red"><?php echo form_error('product_name'); ?></font>
										</div>
								</div>
								<!--End col-md-6 -->
							</div>
							<div class="row">
								<!-- col-md-6 -->
								<div class="col-md-12">
										<div class="form-group">
											<?php echo form_label(word_r('description'),'pro_des',array('class' => 'control-label') ); ?>
											<?php echo form_textarea(array('name' => 'pro_des', 'class' => 'form-control mce', 'id' => 'pro_detail',  'value' => $this->input->post('pro_des')) );?>
										</div>
								</div>
								<!--End col-md-6 -->
							</div>
							<!--div class="row">
								
								<div class="col-md-12">
										<div class="form-group">
											<?php echo form_label('Short Description','pro_short_des',array('class' => 'control-label') ); ?>
											<?php echo form_textarea(array('name' => 'pro_short_des', 'class' => 'form-control mce', 'id' => 'pro_short_detail',  'value' => $this->input->post('pro_short_des')) );?>
										</div>
								</div>
								
							</div-->
							<div class="row">
								<!-- col-md-6 -->
								<div class="col-md-12">
										<div class="form-group">
											<label class="col-sm-12 control-label">Inventory</label><br /><br />
											<div class="clearfix tabs-vertical">
												<ul class="nav nav-tabs">
													<li class="active"><a data-toggle="tab" href="#tab-general" aria-expanded="false"><span class="glyphicon glyphicon-signal"></span> General</a></li>
													<?php 
														if($this->session->userdata('group_id') == 1){
													 ?>
													<li class=""><a data-toggle="tab" href="#tab-inventory" aria-expanded="false"><span class="glyphicon glyphicon-folder-close"></span> Inventory</a></li>
													<li class=""><a data-toggle="tab" href="#tab-shopping" aria-expanded="true"><span class="glyphicon glyphicon-gift"></span> Shipping</a></li>
													<li class=""><a data-toggle="tab" href="#tab-attribute" aria-expanded="true"><span class="glyphicon glyphicon-paperclip"></span> Attributes</a></li>
													<li class="">
														<a data-toggle="tab" href="#tab-varian" aria-expanded="true"><span class="glyphicon glyphicon-paperclip"></span> Varians</a>
													</li>
													<?php } ?>
												</ul>
												<div class="tab-content">
													<div id="tab-general" class="tab-pane active">
													<?php if($this->session->userdata('group_id') == 1):
														
													
													?>
														<div class="form-group col-md-12">
															<label for="inputEmail3" class="col-sm-4 control-label">Regular Price ($)</label>
															<div class="col-sm-8">
															  <input type="text" class="form-control number_only" value="<?php echo $this->input->post('regular_price', true)?>" name="regular_price">
															  <font color="red"><?php echo form_error('regular_price'); ?></font>
															</div>
														</div>
													<?php
													endif;
													?>
														<div class="form-group col-md-12">
															<label class="col-sm-4 control-label">Sale Price ($)</label>
															<div class="col-sm-8">
															  <input type="text" class="form-control number_only" value="<?php echo $this->input->post('sale_price', true)?>" name="sale_price" >
															  <font color="red"><?php echo form_error('sale_price'); ?></font>
															</div>
														</div>
														<?php 
															if($this->session->userdata('group_id') == 1){
														 ?>
														<div class="form-group col-md-12">
															<label class="col-sm-4 control-label">Product Type</label>
															<div class="col-sm-8">
															<?php 
																$option_value =get_option('Product Status');
																echo '<select name="pro_status" class="form-control">';
																foreach($option_value as $value){
																	if($this->input->post('pro_status') == $value['option_id']){
																		$selected = 'selected';
																	}else{
																		$selected ='';
																	}
																	echo '<option value="'.$value['option_id'].'"'.$selected.'>'.$value['option_value'].'</option>';
																}
																echo '</select>';
															?>
															</div>
														</div>
														<!-- khorng -->
														<div class="form-group col-md-12">
															<label class="col-sm-4"><b><?php echo 'Contact Now';?></b></label>
															<div class="col-md-8">
																<?php if($this->input->post('contact-type') == 1){
																	  $checked = 'checked';
																	} else{
																	  $checked = '';
																	}
																?>
																<input type="checkbox" name="contact-type" value="1" <?php echo $checked; ?>>
															</div>
														</div>
														<!-- end -->
														<div class="form-group col-md-12">
															<label class="col-sm-4 control-label"><b><?php echo 'Hot Product';?></b></label>
															<div class="col-sm-8">
															  <?php if($this->input->post('hot_pro') == 1){
																	$checked ='checked';
																}else{
																	$checked = '';
																} 
															  ?>
															  <input type="checkbox" id="hot_pro" value="1" name="hot_pro" <?php echo $checked; ?>> 
															  <font color="red"><?php echo form_error('hot_pro'); ?></font>
															</div>
														</div>
														<div class="form-group col-md-12">
															<label class="col-sm-4 control-label"><b><?php echo 'Discount';?></b></label>
															<div class="col-sm-8">
															<?php if($this->input->post('have_disc') == 1){
																	$checked ='checked';
																}else{
																	$checked = '';
																} 
															  ?>
															  <input type="checkbox" id="have_disc" value="1" name="have_disc" <?php echo $checked; ?>> 
															  <font color="red"><?php //echo form_error('have_disc'); ?></font>
															</div>
														</div>
														<div class="form-group col-md-12 is_dis show_have_dis" >
															<label class="col-sm-4 control-label"><?php echo 'Discount Amount(%)'?> :</label>
															<div class="col-sm-8">
																<div class="clearfix">
																	<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																		<input type="text" value="<?php echo $this->input->post('dis_amount'); ?>" name="dis_amount" id="dis_amount" class="form-control"> 
																		
																		<font color="red"><?php echo form_error('dis_amount'); ?></font>
																	</div>
																</div>
															</div>
														</div>

														<div class="form-group col-md-12 is_dis show_have_dis">
															<label class="col-sm-4 control-label"><?php echo 'From'?> :</label>
															<div class="col-sm-4">
																<div class="clearfix">
																	<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																	
																		<input type="text"  name="from_ddate" value="<?php echo $this->input->post('from_ddate'); ?>" class="form-control datepicker"> 
																		<span class="input-group-addon datepicker-icon">
																			<span class="glyphicon glyphicon-calendar"></span>
																		</span>
																		<font color="red"><?php echo form_error('from_ddate'); ?></font>
																	</div>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="clearfix">
																	<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
																		<input type="text" class="form-control timepicker" name="from_dtime" value="">
																		<span class="input-group-addon timepicker-icon">
																			<span class="glyphicon glyphicon-time"></span>
																		</span>
																		<font color="red"><?php echo form_error('from_dtime'); ?></font>
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group col-md-12 is_dis show_have_dis">
															<label class="col-sm-4 control-label"><?php echo 'To'?> :</label>
															<div class="col-sm-4">
																<div class="clearfix">
																	<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																	
																		<input type="text" name="to_ddate" value="<?php echo $this->input->post('to_ddate'); ?>" class="form-control datepicker"> 
																		<span class="input-group-addon datepicker-icon">
																			<span class="glyphicon glyphicon-calendar"></span>
																		</span>
																		<font color="red"><?php echo form_error('to_ddate'); ?></font>
																	</div>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="clearfix">
																	<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
																		<input type="text" class="form-control timepicker" name="to_dtime" value="">
																		<span class="input-group-addon timepicker-icon">
																			<span class="glyphicon glyphicon-time"></span>
																		</span>
																		<font color="red"><?php echo form_error('to_dtime'); ?></font>
																	</div>
																</div>
															</div>
														</div> 
														<?php } ?>
													</div>
													<div id="tab-inventory" class="tab-pane">
														<div class="form-group col-md-12">
															<label for="inputEmail3" class="col-sm-3 control-label">Stock Qty</label>
															<div class="col-sm-9">
															  <input type="text" class="form-control" name="stock_qty" id="inputEmail3" value="<?php echo '50';/*echo $this->input->post('stock_qty');*/ ?>">
															  <font color="red"><?php echo form_error('stock_qty'); ?></font>
															</div>
														</div>
														<div class="form-group col-md-12">
															<label for="inputEmail3" class="col-sm-3 control-label">Available</label>
															<div class="col-sm-9">
																<?php 
																$option_value =get_option('Stock Available');
																echo '<select name="stock_available" class="form-control">';
																foreach($option_value as $value){
																	if($this->input->post('stock_available') == $value['option_id']){
																		$selected = 'selected';
																	}else{
																		$selected ='';
																	}
																	echo '<option value="'.$value['option_id'].'"'.$select.'>'.$value['option_value'].'</option>
																		'; 

																}
																echo '</select>';
																?>
																<font color="red"><?php echo form_error('stock_available'); ?></font>
															</div>
														</div>

													</div>
													<div id="tab-shopping" class="tab-pane">
														<div class="form-group col-md-12">
															<label for="inputEmail3" class="col-sm-3 control-label">Weight (kg)</label>
															<div class="col-sm-9">
															  <input type="text" class="form-control" name="weight" id="inputEmail3" value="<?php echo $this->input->post('weight'); ?>">
															  <font color="red"><?php echo form_error('weight'); ?></font>
															</div>
														</div>
														<div class="form-group col-md-12">
															<label for="inputEmail3" class="col-sm-3 control-label">Dimensions (cm)</label>
															<div class="col-sm-3">
															  <input type="text" class="form-control" name="length" id="inputEmail3" placeholder="Length" value="<?php echo $this->input->post('length'); ?>">
															  <font color="red"><?php echo form_error('length'); ?></font>
															</div>
															<div class="col-sm-3">
															  <input type="text" class="form-control" name="width" id="inputEmail3" placeholder="Width" value="<?php echo $this->input->post('width'); ?>">
															  <font color="red"><?php echo form_error('width'); ?></font>
															</div>
															<div class="col-sm-3">
															  <input type="text" class="form-control" name="height" id="inputEmail3" placeholder="Height" value="<?php echo $this->input->post('height'); ?>">
															   <font color="red"><?php echo form_error('height'); ?></font>
															</div>
														</div>
													</div>
													<div id="tab-attribute" class="tab-pane">
														<div class="form-group col-md-12">
															<div class="col-sm-6">
																<?php  echo get_attribute_product('parent',false,has_logged('user_id') );?>
															</div>
															
															<div class="col-sm-3">
																<button class="btn btn-default add_atr" name="add_atr" type="button">Add</button>
															</div>
															<div class="col-sm-2">
																<img src="<?php echo base_url();?>images/loading.gif" class="load_wait" style="display:none;" />
															</div>
														</div>
														
														
														<div class="form-group col-md-12">
															<div class="col-sm-12">
																<div class="scroll_widgets" id="show_attr"></div>
															</div>
														</div>
														<div class="form-group col-md-12">
															<div class="col-sm-12">
																<div class="scroll_widgets" id="show_size"></div>
															</div>
														</div>
													</div>
													<div id="tab-varian" class="tab-pane">
														<div class="form-group">
															<div class="col-sm-2">
																<button class="btn btn-default add_varian" name="add_varian" type="button">Add</button>
															</div>
															<div class="col-sm-2">
																<img src="<?php echo base_url();?>images/loading.gif" class="load_wait" style="display:none;" />
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-12">
																<div class="scroll_widgets" id="show_varian">
																	<div class="parents">
																	
																	</div>
																</div>
															</div>
															
														</div>
													</div>
												</div>
											</div>
										</div>
								</div>
								<!--End col-md-6 -->
							</div>
							<!-- End row -->
							
						</div>
						<!--End form-body -->
					</div>
					<!-- End Form -->
					<!-- Action -->
					<div class="col-md-4">
						<div class="panel panel-default">
							<div class="widget_section">
								<div class="ui-radio ui-radio-primary">
									<div class="panel-heading">
										<div class="widget_title"><h3 class="panel-title">Public</h3></div>
									</div>
									<div class="panel-body">
										<div>
										 <?php if($this->session->userdata('group_id') != 2){ ?>
											<div class="form-group">
												<input type="checkbox" name="status" value="1" checked />	
												<input type="hidden" name="create_valid" value="1"/>
												<label class="control-label">visibility</label>	
											</div>
										<?php } ?>																
											<div class="clearfix">
												<input type="submit" name="btn_save" value="<?php echo 'Save'?>" class="btn btn-primary">
												
												<a href="<?php echo base_url().'admin/product/product_list';?>"><input type="button" name="btn_update" value="Cancel" class="btn btn-danger"></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- khorng 14-17-2017 -->
						<?php if($this->session->userdata('group_id')==2){ ?>
						<div class="panel panel-default">
							<div class="widget_section">
								<div class="panel-heading">
								
									<div class="widget_title"><h3 class="panel-title">Categories</h3></div>
									
									<ul class="nav nav-tabs" role="tablist">
										<li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Latest used</a></li>
										<li role="presentation"><a href="#shipping" aria-controls="shipping" role="tab" data-toggle="tab">View all structures</a></li>
									
									</ul>
									
								</div>
									<?php $user = $this->session->userdata('group_id');
										if(isset($_POST['btn_save'])){
											$rememberCat = $this->session->userdata('categorie');
										}
										else if($this->session->userdata('categories')){
											$rememberCat = $this->session->userdata('categories');
										}
										else{
											$rememberCat = '';
										}
											
										//if($user == 2){ ?>
											<div class="tab-content">
												<div role="tabpanel" class="tab-pane active" id="profile">		
													<div class="panel-body" style="height:300px; overflow:auto;">
														<div class="scroll_widget">																		
															<?php 
															echo $cat;
															/*$output ='';
															
															foreach ($cat as  $value) {
																
																$output.='<input type="checkbox" name="categories[]" class="form-control categories"';
																
																	if(!empty($rememberCat)){
																		foreach($rememberCat as $val){
																			if($val == $value['category_id']){
																				$output .=" checked ";
																			}								
																		}				
																	}
																$output.= 'value="'.$value['category_id'].'">'.$value['category_name'].'<br/>';
																
															} echo $output;*/?>
														</div>
														
													</div>
												
											
												<!--a href="<?php echo base_admin_url(). 'categories/vendor_category/create';?>">Manage your categories here</a-->
											</div>
										<?php //}else{ ?>
											<div role="tabpanel" class="tab-pane" id="shipping"> 
												<div class="panel-body" style="height:300px; overflow:auto;">						
													<div class="scroll_widget">
													<?php $category_type=category_type('Product');
													echo widget_category($category_type,$this->input->post('category_id'), $rememberCat);?>
												
													</div>
												</div>
											<?php //} ?>
											</div>
										<!--div class="panel-body">
											<input type="checkbox" id="select"/>&nbsp;&nbsp;&nbsp;<label>Select all</label>
										</div-->
										</div>
							</div>
						</div>
						<?php }else{ ?>
						<div class="panel panel-default">
							<div class="widget_section">
								<div class="panel-heading">
									<div class="widget_title"><h3 class="panel-title">Categories</h3></div>
								</div>
								<div class="panel-body" style="height:300px; overflow:auto;">
									<div class="scroll_widget">
										<?php 
										$user = $this->session->userdata('group_id');
										if(isset($_POST['btn_save'])){
											$rememberCat = $this->session->userdata('categorie');
										}
										else if($this->session->userdata('categories')){
											$rememberCat = $this->session->userdata('categories');
										}
										else{
											$rememberCat = '';
										}
										$category_type=category_type('Product');
													echo widget_category($category_type,$this->input->post('category_id'), $rememberCat);?>
									</div>
								</div>
								<div class="panel-body">
									<input type="checkbox" id="select"/>&nbsp;&nbsp;&nbsp;<label>Select all</label>
								</div>
							</div>
						</div> 
						<?php 
						}
							if($this->session->userdata('group_id') == 1){
						?>
						<div class="row">
							<div class="col-md-12">
								<label>Tag Name</label>
								<select name="tgname" class="form-control" id="select_tag_name">
									<option value="0">---------Please choose Tag---------</option>
									<?php foreach($tags as $value){?>
										<option value="<?= $value['tag_id']?>"><?= $value['tag_name']?></option>
									<?php 
									}
									?>
								</select> 
								<br/>
								<input class='form-control  tag_name' value='' name='tag_name'></input>
							</div> 
						</div>
						<br/> 
						<div class='row'>
							<!-- col-md-6 -->
							<div class="col-md-12">
									
									<label>Brand Name</label>
								<select name="brname" class="form-control">
								<option value="">---------Please choose brand---------</option>
									<?php foreach ($brand as $brands):
										if($this->input->post('brname') == $brands['brand_id']){
											$selected = 'selected';
										}else{
											$selected = '';
										}
									?>

										<option value="<?php echo $brands['brand_id']; ?>" <?php echo $selected; ?>><?php echo $brands['brand_name']; ?></option>
									<?php endforeach;?>
								</select>
							</div>
							<!--End col-md-6 -->
						</div><br/>
										<?php  } ?>
						<div class="row">
							<!-- col-md-6 -->
							<?php 
								if($this->session->userdata('group_id') == 1){
							 ?>
							<div class="col-md-12">	
								<label>Choose Vendor</label>
								<select name="vendor" class="form-control">
								<option value="">---------Please choose vendor---------</option>
									<?php foreach ($vendors as $row):
										if($this->input->post('vendor') == $row['user_id']){
											$selected = 'selected';
										}else{
											$selected = '';
										}
									?>
										<option value="<?php echo $row['user_id']; ?>" <?php echo $selected; ?>><?php echo $row['company_name']; ?></option>
									<?php endforeach;?>
								</select>
							</div>
							<?php } ?>
							<!--End col-md-6 -->
						</div><br/>
						<?php 
							if($this->session->userdata('group_id') == 1){
						 ?>
						<div class="row">
							<div class="col-md-12">
							<?php
								$supplierRemem = $this->session->userdata('supplier');
						
							?>

								<label>Choose Supplier</label>
								<select name="supplier" class="form-control">
								<option value="">---------Please choose supplier-----</option>
									<?php foreach ($supplier as $row):
										if($supplierRemem == $row['user_id']){
											$selected = ' selected=selected ';
										}
										else{
											$selected = '';
										}
									?>
										<option value="<?php echo $row['user_id']; ?>" <?php echo $selected; ?>><?php echo $row['username']; ?></option>
									<?php endforeach;?>
								</select>
							</div>
						</div>
						<?php }?>
						<div class="panel panel-default">
							<div class="widget_section">
								<div class="panel-heading">
									<div class="widget_title"><h3 class="panel-title">Product image (Size should be 600 x 600 pixels)</h3></div>
								</div>
								<div class="panel-body" >
									<div class="fileinput fileinput-new" data-provides="fileinput">
									  <div class="fileinput-new thumbnail" style="width: 263px; height: 160px;">
										
										<img name="img_img" data-src="holder.js/100%x100%" alt="">
									  </div>
									  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 263px; max-height: 200px;"></div>
									  <div>
										<span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="userfile[]"></span>
										<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
									  </div>
									</div>
									
								</div>
							
							</div>
						</div>
						<div class="panel panel-default">
							<div class="widget_section">
								<div class="panel-heading">
									<div class="widget_title"><h3 class="panel-title">Product Gallery (Size should be 600 x 600 pixels)</h3></div>
								</div>
								<div class="panel-body" >
									<div class="gallery_">
										<div class="fileinput fileinput-new" data-provides="fileinput">
											<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
												<img data-src="holder.js/100%x100%" alt="">
												
											</div>
											<div id="dvPreview"></div>
											<div>
												<span class="btn btn-default btn-file">
												<span class="fileinput-new">Select image</span><span class="fileinput-exists">Select again</span>
												<input id="fileupload-new" type="file" multiple="multiple" class="file" name="gallery[]">
												</span>
												<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>								
						
					</div>
					<!-- End Action -->
				 </div>
				  <!-- End Row -->
				</div>
				<!-- end portlet-body -->
				<?php echo form_close(); ?>
		</div>
		<!-- end portlet -->
	</div>
	<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->
<script>
    var base_admin_assets_url = "<?php echo base_admin_assets_url(); ?>";
</script>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
  //tag
    $('.tags-input').tagsInput({
        width: 'auto',
        //autocomplete_url:'test/fake_plaintext_endpoint.html' //jquery.autocomplete (not jquery ui)
        autocomplete_url:"<?php echo base_admin_url().$controller_url.'save'; ?>" // jquery ui autocomplete requires a json endpoint
      });
</script>
<?php 
if($this->session->userdata('group_id') == 2){
?>
<script type="text/javascript">
		tinymce.init({
		  selector: "textarea",  // change this value according to your HTML
		  plugins: "paste",
		  menubar: "edit",
		  toolbar: "paste",
		  paste_as_text: true
		});
</script>
<?php } ?>
<script type="text/javascript">
	$(document).ready(function() {		
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			startDate: '1d'
		});
		$('.timepicker').timepicker({
			showMeridian: false,
			format: 'HH:mm',
			showSeconds: true,
			minuteStep: 1,
			secondStep: 1
		});
		
		$(".tag_name").tagsInput({
			width: '100%',
		}); 
		 
		$("#select_tag_name").on("click",function() {
			 var tag_txt =  $(this).find(":selected").text(); 
			 if($(this).find(":selected").val() !=0 ) { 
				$('.tag_name').removeTag(tag_txt);
				$('.tag_name').addTag(tag_txt);
			 }
		}); 	
		$(".datepicker-icon").on("click",function(){
			var parent = $(this).parent().parent().parent();
			parent.find(".datepicker").focus();
		});
		$(".timepicker-icon").on("click",function(){
			var parent = $(this).parent().parent().parent();
			parent.find(".timepicker").focus();
		});
		
		$("#have_disc").click(function(){
			$(".show_have_dis").toggle(400);
		});
		$('#select').on('click',function(){
			if ($(this).is(':checked')) {
				$('.categories').each(function(){
					this.checked = true;
				});
			}else{
				$('.categories').each(function(){
					this.checked = false;
				});
			}
		});
		//--------attribute------
		$i=0;
		$(".add_atr").click(function(e) {
			var attr_id = $("#attr_name").val();
			$('.load_wait').show();
			$.ajax({
				type: "POST",
				url: base_url+"admin/product/product_list/attributes/"+$i+"/",
				data: {'id':attr_id},
				success: function(re){
					if(attr_id ==8){
						$("#show_attr").append(re);
					}else{
						$("#show_size").append(re);
					}
					$('.load_wait').hide();			
				}
			});
			$i++;
		});
		$(document).on('click','.remove', function(){
			$(this).closest('.size_on').remove();
		});
		$(".add_varian").click(function(e) {
			$('.load_wait').show();
			$.ajax({
				type: "POST",
				url: base_url+"admin/product/product_list/varian",
				success: function(re){
					if(re){
						$("#show_varian").append(re);
					}
					$('.load_wait').hide();			
				}
			});
		});

		$('.save').click(function(){
			var group = $('#attr_name').val();
			var color = $('input[name^=userfile]').map(function(idx, elem) {
				return $(elem).val();
			}).get();
			var price = $('input[name^=p_color]').map(function(id, eleme) {
				return $(eleme).val();
			}).get();
			var size = $('input[name^=size]').map(function(ids, elems) {
				return $(elems).val();
			}).get();
			
			if(group == 9){
				$('input[name^=str_size]').val(group + ',' + size)
			}
			if(group == 8){
				$('input[name^=str_color]').val(group + ',' + color + '/' + price)
			}
			event.preventDefault();
		});
		
		$('#attr_name').change(function(){
			$('.save').show(200);
		});
		
		var maxAppend = 0;
		$('.add_tags').click(function(){
			var tag_id = $("#get_tags").val();
			if(tag_id ==""){
				return false;
			}else{
				var list='<li><a class="button_grey" href="#">'+tag_id+'</a>'+
						'<i class="tag_re glyphicon glyphicon-remove-circle"></i>'+
						'<input type="hidden" name="tags_con[]" value="'+tag_id+'"></li>';
				if(maxappend >=10) return;
				$(".tags_container ul").append(list);
				maxappend++;
			}
		});
		
		$(document).on('click','.tag_re', function(){
			$(this).parent('.tags_container ul li').remove();
		});
		
		$(document).on('click','.att_re', function(){
			$(this).closest('.att_on').remove();
			$(this).closest('.size_on').remove();
		});

	});
</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
