<?php $this->load->view(config_item('admin_template_dir').'head'); ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/multi_select/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/multi_select/select2.full.js"></script>
<link href="<?php echo base_url();?>assets/multi_select/select2.min.css" type="text/css" rel="stylesheet" />
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<div class="page-bar">
				<?php echo breadcrumb();?>
				
</div>
<!-- ROW -->
 <div class="row">
		<!-- COL-MD-12 -->
		<div class="col-md-12">
			<!-- portlet -->
			<div class="portlet light bordered">
						<!-- portlet-title -->
						<div class="portlet-title">
							<!-- caption -->
							<div class="caption">
								<i class="<?php //echo $icon; ?>"></i>
								<span class="caption-subject bold uppercase"> Update Product</span>
							</div>
							<!-- end caption -->

							<!-- actions -->
							<div class="actions">
								<?php //echo btn_actions('account/product_list/create','account/product_list/create', $create_action); ?>
                                                                <?php echo '<a target="_blank" href="'.base_url().'detail/'.$data['permalink'].'">View Product</a>';?>
							</div>
							<!-- end actions -->
						</div>
							<!--end portlet-title -->
						<?php echo form_open_multipart(base_admin_url().$controller_url.$this->uri->segment(4).'/'.$data['product_id'],array('role' => 'form','class' => 'form-horizontal')); ?>
					
						
								<!-- portlet-body -->
									<div class="portlet-body">
									<?php 
						                if(isset($errors)){
						                   echo show_msg($errors);
						                }
										if(isset($rmsg)){
						                   echo show_msg($rmsg);
						                }
						                $this->session->flashdata('msg');
							         ?>
							         <!-- Row -->
							         <div class="row">
							         		 <!-- Form -->
							         		<div class="col-md-8">	
							         			<!--Form-body -->
													<div class="form-body">
														<!-- ROW -->
														<input type="hidden" name="create" value="<?php echo $data['created_date']?>">
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																	<div class="form-group required <?php has_error('Procode'); ?>">
																		<?php echo 'Product Code';?>
																		<?php echo form_input('pro_code' ,$data['product_code'], "class='form-control'")?>
																		<font color="red"><?php echo form_error('procode'); ?></font>
																	</div>
															</div>
															<!--End col-md-6 -->
														</div>
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																	<div class="form-group required <?php has_error('product_name'); ?>">
																		<?php echo 'Product Name';?>
																		<?php echo form_input('product_name',$data['product_name'],"class = 'form-control'"); ?>
																		<font color="red"><?php echo form_error('product_name'); ?></font>
																	</div>
															</div>
															<!--End col-md-6 -->
														</div>
														
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																	<div class="form-group">
																		<?php echo form_label(word_r('description'),'pro_des',array('class' => 'control-label') ); ?>
																		<?php echo form_textarea(array('name' => 'pro_des', 'class' => 'form-control mce', 'id' => 'pro_detail',  'value' => $data['description']) );?>
																	</div>
															</div>
															<!--End col-md-6 -->
														</div>
														<!--div class="row">
															
															<div class="col-md-12">
																	<div class="form-group">
																		<?php echo form_label('Short Description','pro_short_des',array('class' => 'control-label') ); ?>
																		<?php echo form_textarea(array('name' => 'pro_short_des', 'class' => 'form-control mce', 'id' => 'pro_short_detail',  'value' => $data['detail']) );?>
																	</div>
															</div>
															-
														</div-->
														<div class="row">
															<!-- col-md-6 -->
															<div class="col-md-12">
																	<div class="form-group">
																		<label class="col-sm-12 control-label">Inventory</label><br /><br />
																		<div class="clearfix tabs-vertical">
																			<ul class="nav nav-tabs">
																				<li class="active"><a data-toggle="tab" href="#tab-general" aria-expanded="false"><span class="glyphicon glyphicon-signal"></span> General</a></li>
																				<?php 
																					if($this->session->userdata('group_id') == 1 && $this->uri->segment(3) != 'supplier_product'){
																				 ?>
																				<li class=""><a data-toggle="tab" href="#tab-inventory" aria-expanded="false"><span class="glyphicon glyphicon-folder-close"></span> Inventory</a></li>
																				<li class=""><a data-toggle="tab" href="#tab-shopping" aria-expanded="true"><span class="glyphicon glyphicon-gift"></span> Shipping</a></li>
																				<li class="">
																					<a data-toggle="tab" href="#tab-attribute" aria-expanded="true"><span class="glyphicon glyphicon-paperclip"></span> Attributes</a>
																				</li>
																				<li class="">
																					<a data-toggle="tab" href="#tab-varian" aria-expanded="true"><span class="glyphicon glyphicon-paperclip"></span> Varians</a>
																				</li>
																				<?php } ?>
																			</ul>
																			<div class="tab-content">
																				<div id="tab-general" class="tab-pane active">
																					<?php if ($this->session->userdata('group_id') == 1): ?>
																					<div class="form-group col-md-12">
																						<label for="inputEmail3" class="col-sm-4 control-label">Regular Price ($)</label>
																						<div class="col-sm-8">
																						  <input type="text" class="form-control" value="<?php echo $data['regular_prices']?>" name="regular_price">
																						  <font color="red"><?php echo form_error('regular_price'); ?></font>
																						</div>
																					</div>
																				<?php endif; ?>
																					<div class="form-group col-md-12">
																						<label class="col-sm-4 control-label">Sale Price ($)</label>
																						<div class="col-sm-8">
																						  <input type="text" class="form-control" value="<?php echo $data['sale_price']?>" name="sale_price">
																						  <font color="red"><?php echo form_error('sale_price'); ?></font>
																						</div>
																					</div>
																					<?php 
																						if($this->session->userdata('group_id') == 1 && $this->uri->segment(3) != 'supplier_product'){
																					?>
																					<div class="form-group col-md-12">
																						<label class="col-sm-4 control-label">Product Type</label>
																						<div class="col-sm-8">
																						<?php 			
																							$option_value =get_option('Product Status');
																							echo '<select name="pro_status" class="form-control">';
																							foreach($option_value as $value){
																						?>
																							<option value="<?php echo $value['option_id'];?>" <?php if($data['product_type']==$value['option_id']){echo 'selected';}?>>
																							<?php echo $value['option_value'];?>
																							</option>';
																						<?php
																							}
																							echo '</select>';
																						?>
																						</div>
																					</div>
																					<!-- khorng -->
																					<div class="form-group col-md-12">
																						<label class="col-sm-4"><b><?php echo 'Contact Now';?></b></label>
																				
																							<div class="col-md-8">
																							<?php
																							if($data['contact-type']==1){
																								$checked = 'checked';
																							}else{
																								$checked = '';
																							}																						
																							?>
																							<input type="checkbox" name="contact-type" value="1" <?php echo $checked;?>>
																							
																						</div>
																						
																					</div>
																					<!-- end -->
																					<div class="form-group col-md-12">
																						<label class="col-sm-4 control-label"><b><?php echo 'Hot Product';?></b></label>
																						<div class="col-sm-8">
																						  <input type="checkbox" id="hot_pro" value="1" name="hot_pro" <?php if($data['hot_pro']==1){echo "checked";}?>> 
																						  <font color="red"><?php echo form_error('hot_pro'); ?></font>
																						</div>
																					</div>
																					<div class="form-group col-md-12">
																						<label class="col-sm-4 control-label"><b><?php echo 'Discount Amount';?></b></label>
																						<div class="col-sm-8">
																						  <input type="checkbox" id="have_disc" value="1" name="have_disc" <?php if($data['have_discount']==1){echo "checked";}?>> 
																						  <font color="red"><?php //echo form_error('have_disc'); ?></font>
																						</div>
																					</div>
																					<div class="form-group col-md-12 is_dis show_have_dis" <?php if($data['have_discount']==1){echo 'style=display:block;';}?>>
																						<label class="col-sm-4 control-label"><?php echo 'Discount Amount(%)'?> :</label>
																						<div class="col-sm-8">
																							<div class="clearfix">
																								<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" value="<?php echo $data['discount_amount'];?>" name="dis_amount" id="dis_amount" class="form-control"> 
																									
																									<font color="red"><?php echo form_error('dis_amount'); ?></font>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="form-group col-md-12 is_dis show_have_dis" <?php if($data['have_discount']==1){echo 'style=display:block;';}?>>
																						<label class="col-sm-4 control-label"><?php echo 'From'?> :</label>
																						<div class="col-sm-4">
																							<div class="clearfix">
																								<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" value="<?php echo date('Y-m-d', strtotime($data['from_date']))?>" name="from_ddate" class="form-control datepicker"> 
																									<span class="input-group-addon datepicker-icon">
																										<span class="glyphicon glyphicon-calendar "></span>
																									</span>
																									<font color="red"><?php echo form_error('from_ddate'); ?></font>
																								</div>
																							</div>
																						</div>
																						<div class="col-sm-4">
																							<div class="clearfix">
																								<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" class="form-control timepicker" name="from_dtime" value="<?php echo date('H:i:s', strtotime($data['from_date']))?>">
																									<span class="input-group-addon timepicker-icon">
																										<span class="glyphicon glyphicon-time "></span>
																									</span>
																									<font color="red"><?php echo form_error('from_dtime'); ?></font>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="form-group col-md-12 is_dis show_have_dis" <?php if($data['have_discount']==1){echo "style=display:block;";}?>>
																						<label class="col-sm-4 control-label"><?php echo 'To'?> :</label>
																						<div class="col-sm-4">
																							<div class="clearfix">
																								<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" value="<?php echo date('Y-m-d', strtotime($data['todate']))?>" name="to_ddate" class="form-control datepicker"> 
																									<span class="input-group-addon datepicker-icon">
																										<span class="glyphicon glyphicon-calendar"></span>
																									</span>
																									<font color="red"><?php echo form_error('to_ddate'); ?></font>
																								</div>
																							</div>
																						</div>
																						<div class="col-sm-4">
																							<div class="clearfix">
																								<div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
																									<input type="text" class="form-control timepicker" name="to_dtime" value="<?php echo date('H:i:s', strtotime($data['todate']))?>">
																									<span class="input-group-addon timepicker-icon">
																										<span class="glyphicon glyphicon-time"></span>
																									</span>
																									<font color="red"><?php echo form_error('to_dtime'); ?></font>
																								</div>
																							</div>
																						</div>
																					</div> 
																						<?php } ?>
																				</div>
																				<div id="tab-inventory" class="tab-pane">
																					<div class="form-group col-md-12">
																						<label for="inputEmail3" class="col-sm-3 control-label">Stock Qty</label>
																						<div class="col-sm-9">
																						  <input type="text" class="form-control" name="stock_qty" value="<?php echo $data['stock_qty']?>" id="inputEmail3">
																						  <font color="red"><?php echo form_error('stock_qty'); ?></font>
																						</div>
																					</div>
																					<div class="form-group col-md-12">
																						<label for="inputEmail3" class="col-sm-3 control-label">Available</label>
																						<div class="col-sm-9">
																							<?php 						
																								$option_value =get_option('Stock Available');
																								echo '<select name="pro_status" class="form-control">';
																								foreach($option_value as $value){
																							?>
																								<option value="<?php echo $value['option_id'];?>" <?php if($data['available']==$value['option_id']){echo 'selected';}?>>
																								<?php echo $value['option_value'];?>
																								</option>';
																							<?php
																								}
																								echo '</select>';
																							?>
																							<font color="red"><?php echo form_error('stock_available'); ?></font>
																						</div>
																					</div>

																				</div>
																				<div id="tab-shopping" class="tab-pane">
																					<div class="form-group col-md-12">
																						<label for="inputEmail3" class="col-sm-3 control-label">Weight (kg)</label>
																						<div class="col-sm-9">
																						  <input type="text" class="form-control" name="weight" value="<?php echo $data['weight']?>" id="inputEmail3">
																						  <font color="red"><?php echo form_error('weight'); ?></font>
																						</div>
																					</div>
																					<div class="form-group col-md-12">
																						<label for="inputEmail3" class="col-sm-3 control-label">Dimensions (cm)</label>
																						<div class="col-sm-3">
																						  <input type="text" class="form-control" name="length" id="inputEmail3" value="<?php echo $data['length']?>" placeholder="Length">
																						  <font color="red"><?php echo form_error('length'); ?></font>
																						</div>
																						<div class="col-sm-3">
																						  <input type="text" class="form-control" name="width" id="inputEmail3" value="<?php echo $data['width']?>" placeholder="Width">
																						  <font color="red"><?php echo form_error('width'); ?></font>
																						</div>
																						<div class="col-sm-3">
																						  <input type="text" class="form-control" name="height" id="inputEmail3" value="<?php echo $data['height']?>" placeholder="Height">
																						   <font color="red"><?php echo form_error('height'); ?></font>
																						</div>
																					</div>
																				</div>
																				<!-- Attribute -->
																				<div id="tab-attribute" class="tab-pane">
																					<div class="form-group">
																						<div class="col-sm-6">
																							<?php  echo get_attribute_product('parent',false,has_logged('user_id') ); ?>
																						</div>
																						<div class="col-sm-3">
																							<button class="btn btn-default add_atr" name="add_atr" type="button">Add</button>
																						</div>
																						<div class="col-sm-2">
																							<img src="<?php echo base_url();?>images/loading.gif" class="load_wait" style="display:none;" />
																						</div>
																					</div>
																					<div class="form-group">
																						<div class="col-sm-12">
																							<div class="scroll_widgets" id="show_attr">
																								<?php
																								$count=0;
																								echo '<div class="parents">';
																								foreach($get_attribute as $data_price){			
																									$product_id = $data_price['product_id'];
																									$color = $data_price['color'];
																									$size = $data_price['size'];
																									$image = $data_price['image'];
																									$cdir = date('m-Y', strtotime($data['created_date']));
																									if($color>0){
																								?>
																								<div class="att_on border_attr">
																									<div class="form-group" style="margin:0;">
																										<label class="col-sm-2 control-label" for="wheel-price">Color</label>
																										<div class="col-sm-3">
																											<?php
																											echo '<select class="form-control" name="color['.$count.'][color]">';
																												$colors=attr_pro(8);
																												foreach($colors as $row){
																													$selected=$row['attr_id'] == $color ? 'Selected' : '';
																													echo '<option '.$selected.' value="'.$row['attr_id'].'">'.$row['attr_name'].'</option>';
																												}
																											echo '</select><br/>';
																											?>
																											<font color="red"></font>
																										</div>
																										<div class="col-sm-4">
																											<?php
																											$size= explode(',', $size);	
																											echo '<select style="width:180px;" class="form-control js_multiple_ count_row" multiple="multiple" name="color['.$count.'][size][]">
																													<option value="">Default Size</option>';
																												$colors=attr_pro(9);
																												foreach($size as $size_){
																													foreach($colors as $row){
																														$selected= $size_ == $row['attr_id'] ? 'selected':'';
																														echo '<option '.$selected.' value="'.$row['attr_id'].'">'.$row['attr_name'].'</option>';
																													}
																												}
																											echo '</select>';
																											?>
																											<script type="text/javascript">
																												var basicMultiple = $(".js_multiple_");
																												basicMultiple.select2();
																											</script>
																											<font color="red"></font>
																										</div>
																										<div class="col-sm-2">
																											<i class="att_re glyphicon glyphicon-trash"></i>
																										</div>
																									</div>
																									<div class="form-group" style="margin:0;">
																										<label class="col-sm-2 control-label" for="wheel-demo">Image Color</label>
																										<div class="col-sm-9">
																											<div class="row">
																												<div class="fileinput fileinput-<?php echo $image == ''? 'new':'exists';?>" data-provides="fileinput">
																													<div class="col-md-6">
																														<div class="fileinput-new thumbnail" style="width: 100px; height: 60px;">
																															<img data-src="holder.js/100%x100%" alt="">
																														</div>
																														<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px;">
																															<?php if(!empty($image)): ?>
																															<img src="<?php echo base_url().'images/products/'.$image; ?>" data-src="holder.js/100%x100%" alt="">
																															<?php endif; ?>
																															
																															<?php 
																																$match = date('m-Y').'/thumb/';
																																$images = str_replace(date('m-Y').'/thumb/',"",$image);
																															?>
																															
																															<input type="hidden" name="imgcolor[]" value="<?php echo $images; ?>" id="features">
																														 
																														</div>
																													</div>
																													<div class="col-md-6">
																														<span class="btn btn-default btn-file">
																															<span class="fileinput-new">Select image</span>
																															<span class="fileinput-exists">Change</span>
																															<input type="file" name="img[]">
																														</span><br/>
																														<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
																													</div>
																												</div>
																											</div>
																											<font color="red"></font>
																										</div>
																									</div>
																								</div>
																								<?php
																									}
																									$count++;
																								}
																								echo '</div>';
																								?>
																							</div>
																							
																						</div>
																						<div class="col-sm-12">
																							<div class="scroll_widgets" id="show_size">
																								<div class="parent">
																								<?php
																								foreach($get_attribute as $data_size){			
																									$product_id = $data_size['product_id'];
																									$color = $data_size['color'];
																									$size = $data_size['size'];
																									$cdir = date('m-Y', strtotime($data_size['created']));
																									if($color ==0){
																								?>
																								<div class="row size_on border_attr">
																									<div class="col-md-8">
																										<div class="form-group">
																											<label class="col-sm-2 control-label" for="wheel-size">Size</label>
																											<?php
																											echo '<select class="form-control" name="size[]" id="wheel-size">';
																												$colors=attr_pro(9);
																												foreach($colors as $row){
																													$selected=$row['attr_id'] == $size ? 'Selected' : '';
																													echo '<option '.$selected.' value="'.$row['attr_id'].'">'.$row['attr_name'].'</option>';
																												}
																											echo '</select>';
																											?>
																											<font color="red"></font>
																										</div>
																									</div>
																									<div class="col-md-4">
																										<div class="form-group">
																											<i class="remove glyphicon glyphicon-trash"></i>
																										</div>
																									</div>
																								</div>
																								<?php
																									}
																								}
																								?>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<!-- End attribute -->
																				<!-- varian -->
																				<div id="tab-varian" class="tab-pane">
																					<div class="form-group">
																						<div class="col-sm-2">
																							<button class="btn btn-default add_varian" name="add_varian" type="button">Add</button>
																						</div>
																						<div class="col-sm-2">
																							<img src="<?php echo base_url();?>images/loading.gif" class="load_wait" style="display:none;" />
																						</div>
																					</div>
																					<div class="form-group">
																						<div class="col-sm-12">
																							<div class="scroll_widgets" id="show_varian">
																								<div class="parents">
																								<?php
																								foreach($get_varians as $varian_data){
																									$vcolor=$varian_data['colors'];
																									$vsize=$varian_data['sizes'];
																									$vprice=$varian_data['price'];
																								?>
																								<div class="row att_on border_attr">
																									<div class="col-sm-3">
																										<div class="form-group" style="margin:0;">
																											<label class="col-sm-12 control-label" for="wheel-price">Color</label>
																											<div class="col-sm-12">
																												<?php
																												echo '<select class="form-control" name="varian_color[]">
																														<option value="">Default Color</option>';
																													$colors=attr_pro(8);
																													foreach($colors as $row){
																														$selected=$row['attr_id'] == $vcolor ? 'Selected' : '';
																														echo '<option '.$selected.' value="'.$row['attr_id'].'">'.$row['attr_name'].'</option>';
																													}
																												echo '</select><br/>';
																												?>
																												<font color="red"></font>
																											</div>
																											
																										</div>
																									</div>
																									<div class="col-sm-3">
																										<div class="form-group" style="margin:0;">
																											<label class="col-sm-12 control-label" for="wheel-demo">Size</label>
																											<div class="col-sm-12">
																												<?php
																												echo '<select class="form-control" name="varian_size[]">
																														<option value="">Default Size</option>';
																													$colors=attr_pro(9);
																													foreach($colors as $row){
																														$selected=$row['attr_id'] == $vsize ? 'Selected' : '';
																														echo '<option '.$selected.' value="'.$row['attr_id'].'">'.$row['attr_name'].'</option>';
																													}
																												echo '</select><br/>';
																												?>
																											</div>
																										</div>
																									</div>
																									<div class="col-sm-3">
																										<div class="form-group" style="margin:0;">
																											<label class="col-sm-3 control-label" for="wheel-price">Price</label>
																											<div class="col-sm-12">
																												<input type="text" class="form-control" value="<?php echo $vprice;?>" name="varian_price[]"><br/>
																												<font color="red"></font>
																											</div>
																										</div>
																									</div>
																									<div class="col-sm-2">
																										<div class="col-sm-12">
																											<i class="att_re glyphicon glyphicon-trash"></i>
																										</div>
																									</div>
																								</div>
																								<?php
																								}
																								?>
																								</div>
																							</div>
																						</div>
																						
																					</div>
																				</div>
																				<!-- End attribute -->
																			</div>
																		</div>
																	</div>
															</div>
															<!--End col-md-6 -->
														</div>
														<!-- End row -->
														
													</div>
													<!--End form-body -->
							         		</div>
							         		<!-- End Form -->
							         		<!-- Action -->
							         		<div class="col-md-4">
							         			<div class="panel panel-default">
													<div class="widget_section">
														<div class="ui-radio ui-radio-primary">
															<div class="panel-heading">
																<div class="widget_title"><h3 class="panel-title">Public</h3></div>
															</div>
															<div class="panel-body">
															 <div>
															 <?php if($this->session->userdata('group_id') != 2){ ?>
																<div class="form-group"> 
																  <div class="col-md-12">
																    <?php if($data['visibility']== 1){
																    		$checked = 'checked';
																    	}else{
																    		$checked = 'unchecked';
																    	} 
$status = $data['status']== 1 ? 1:0 ;
																    ?>	  
																	<input type="checkbox" name="status" value="<?php echo $status;?>" <?php echo $checked; ?> />	
																	<label class="control-label">visibility</label>	
																  </div>
																</div>			
															<?php } ?>	
																<div class="clearfix">
																	<input type="hidden" name="stat" value="<?php echo $data['status'];?>">
																	<input type="hidden" name="visible" value="<?php echo $data['visibility'];?>">
																	<input type="submit" name="btn_update" value="<?php echo 'Update'?>" class="btn btn-primary">
																	
																	<a href="<?php echo base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3);?>"><input type="button" name="btn_update" value="Cancel" class="btn btn-danger"></a>
																</div>
															 </div>
															</div>
														</div>
													</div>
												</div>
												<!-- khorng 1-18-2017 -->
												<div class="panel panel-default">
													<div class="widget_section">
														<div class="panel-heading">
															<div class="widget_title"><h3 class="panel-title">Categories</h3></div>
														</div>
															<?php  
															$user = $this->session->userdata('group_id');
																if($user == 2){ ?>
																	
																	<div class="panel-body" style="height:300px; overflow:auto;">
																		<div class="scroll_widget">
																		<?php 
																		//var_dump($relate);
																			$category_type=category_type('Product');
																			//echo update_widget_vendor($category_type,$relate);
																			echo update_widget_category($category_type,$relate);
																			?>
																		</div>
																	</div>

																<?php }else{ ?>	
																<div class="panel-body" style="height:300px; overflow:auto;">
																		<div class="scroll_widget">
																		<?php 
																		//var_dump($relate);
																			$category_type=category_type('Product');
																			echo update_widget_category($category_type,$relate);
																			?>
																		</div>
																	</div>
																	<?php } ?>
										
																<div class="panel-body">
																	<input type="checkbox" id="select"/>&nbsp;&nbsp;&nbsp;<label>Select all</label>
																</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<label>Tag Name</label>
														<select name="tgname" class="form-control" id="select_tag_name">
															<option value="0">---------Please choose Tag---------</option>
															<?php foreach($tags as $value){?>
																<option value="<?= $value['tag_id']?>"><?= $value['tag_name']?></option>
															<?php 
															}
															?>
														</select>  
														<br/>
														<input class='form-control  tag_name' value='<?php echo $data['tags']?>' name='tag_name'></input>
													</div> 
												</div>
												<br/>
												
												<div class='row'>
													<!-- col-md-6 -->
													<div class="col-md-12">
															<?php 
														if($this->session->userdata('group_id') == 1){
														?>
															<label>Brand</label>
														<select name="brname" class="form-control">
															<option value="">---------Please choose brand---------</option>
															<?php foreach ($brand as $brands):
																if($brands['brand_id'] == $brandid['brand']){
																	$selected = ' selected="selected"';
																}
																else{
																	$selected = '';
																}
															?>
																<option value="<?php echo $brands['brand_id']; ?>" <?php echo $selected; ?> ><?php echo $brands['brand_name']; ?></option>
															<?php endforeach;?>
														</select>
													</div>
															<!--End col-md-6 -->
												</div><br/>
												<?php } ?>
												<div class="row">
															<!-- col-md-6 -->
															<?php if($this->session->userdata('group_id')==1 && $this->uri->segment(3) != 'supplier_product'){ ?>
															<div class="col-md-12">
																	
																	<label>Choose Vendor</label>
																<select name="vendor" class="form-control">
																	<option value="">---------Please choose vendor---------</option></option>
																	<?php foreach ($vendors as $row):
																		if($row['user_id'] == $vendorsId['vendor_id']){
																			$selected = ' selected="selected"';
																		}
																		else{
																			$selected = '';
																		}
																	?>
																		<option value="<?php echo $row['user_id']; ?>" <?php echo $selected; ?> ><?php echo $row['company_name']; ?></option>
																	<?php endforeach;?>
																</select>
															</div>
															<?php } ?>
															<!--End col-md-6 -->
												</div><br/>
												<?php 
													if($this->session->userdata('group_id')==1){
												 ?>
												<div class="row">
													<div class="col-md-12">
														<label>Choose Supplier</label>
														<select name="supplier" class="form-control">
														<option value="">---------Please choose supplier---------</option>
															<?php 
															foreach ($supplier as $row):
																if($row['user_id'] == $data['supplier_id']){
																	$selected = ' selected="selected"';
																}else{
																	$selected = '';
																}
															?>
																<option value="<?php echo $row['user_id']; ?>" <?php echo $selected; ?>><?php echo $row['username']; ?></option>
															<?php endforeach;?>
														</select>
													</div>
												</div>
												<?php }?>
												<div class="panel panel-default">
													<div class="widget_section">
														<div class="panel-heading">
															<div class="widget_title"><h3 class="panel-title">Product image (Size must be 600 x 600 pixels)</h3></div>
														</div>
														<div class="panel-body" >
															<div class="form-group" style="height:100%; margin:0;">
																<?php 
																	$dir = date('m-Y', strtotime($data['created_date']));
																?>
																<div class="fileinput fileinput-<?php echo $data['feature'] == ''? 'new':'exists';?>" data-provides="fileinput">
																	<div class="fileinput-new thumbnail" style="width: 263px; height: 160px;">
																		<img data-src="holder.js/100%x100%" alt="">
																	</div>
																	<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 263px; max-height: 200px;">
																		
																		<?php if(!empty($data['feature'])): ?>
																		<img src="<?php echo base_url().'images/products/'.$data['feature']; ?>" data-src="holder.js/100%x100%" alt="">
																		<?php endif; ?>
																		<input type="hidden" name="feature" value="<?php echo $data['feature']; ?>" id="feature">
																	</div>
			
																	<div>
																		<span class="btn btn-default btn-file">
																			<span class="fileinput-new">Select image</span>
																			<span class="fileinput-exists">Change</span>
																			<input type="file" name="userfile[]">
																		</span>
																		<a href="#" class="btn btn-default fileinput-exists remove" data-dismiss="fileinput" >Remove</a>
																	</div>
																</div>
														
															</div>
														</div>
													</div>
												</div> 
												<div class="panel panel-default">
													<div class="widget_section">
														<div class="panel-heading">
														<?php //var_dump($gall); exit; ?>
															<div class="widget_title"><h3 class="panel-title">Product Gallery (Size must be 600 x 600 pixels)</h3></div>
														</div>
														<div class="panel-body" >
															<div class="gallery_">
																
																<div class="fileinput fileinput-<?php echo $gall['image'] == ''? 'new':'exists';?>" data-provides="fileinput">
																	<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																		<img data-src="holder.js/100%x100%" alt="">
																	</div>
																	<?php 
																		$dir = date('m-Y', strtotime($data['created_date']));
																	?>
																	<div id="dvPreview">
																		<?php 
																			if(!empty($gall['image'])): 
																			$img = explode(',',$gall['image']);
																			//var_dump($img); //exit();
																			foreach($img as $imgg){
																				
																		?>
																		<img src="<?php echo base_url().'images/products/'.$imgg; ?>" data-src="holder.js/100%x100%" alt="">
																		<?php 
																			}
																		endif; ?>
																		<input type="hidden" name="image" value="<?php echo $gall['image']; ?>" id="image">
																	</div>
																	<div>
																		<span class="btn btn-default btn-file">
																		<span class="fileinput-new">Select image</span><span class="fileinput-exists">Select again</span>
																		<input id="fileupload" type="file" multiple="multiple" class="file" name="gallery[]">
																		</span>
																		<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
																	</div>
																</div>
																	
															</div>
														</div>
													</div>
												</div>
									
							         		</div>
							         		<!-- End Action -->
							         </div>
							          <!-- End Row -->
							         




									</div>
								<!-- end portlet-body -->
							
						<?php echo form_close(); ?>
						
			</div>
			<!-- end portlet -->
		</div>
		<!-- END COL-MD-12 -->
</div>
<!-- END ROW -->
<script>
    var base_admin_assets_url = "<?php echo base_admin_assets_url(); ?>";
</script>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
  //tag
    $('.tags-input').tagsInput({
        width: 'auto',
        //autocomplete_url:'test/fake_plaintext_endpoint.html' //jquery.autocomplete (not jquery ui)
        autocomplete_url:"<?php echo base_admin_url().$controller_url.'save'; ?>" // jquery ui autocomplete requires a json endpoint
      });
</script>
<?php 
if($this->session->userdata('group_id') == 2){
?>
<script type="text/javascript">
		tinymce.init({
		  selector: "textarea",  // change this value according to your HTML
		  plugins: "paste",
		  menubar: "edit",
		  toolbar: "paste",
		  paste_as_text: true
		});
</script>
<?php } ?>
<script type="text/javascript">
	$(document).ready(function() {	
		$(".tag_name").tagsInput({
			width: '100%',
		}); 
		 
		$("#select_tag_name").on("click",function() {
			 var tag_txt =  $(this).find(":selected").text(); 
			 if($(this).find(":selected").val() !=0 ) { 
				$('.tag_name').removeTag(tag_txt);
				$('.tag_name').addTag(tag_txt);
			 }
		}); 	
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			startDate: '1d'
		});
		$('.timepicker').timepicker({
			showMeridian: false,
			format: 'HH:mm',
			showSeconds: true,
			minuteStep: 1,
			secondStep: 1
		});
		
		$(".datepicker-icon").on("click",function(){
			var parent = $(this).parent().parent().parent();
			parent.find(".datepicker").focus();
		});
		$(".timepicker-icon").on("click",function(){
			var parent = $(this).parent().parent().parent();
			parent.find(".timepicker").focus();
		});
		
		$("#have_disc").click(function(){
			$(".show_have_dis").toggle(400);
		});
		$('#select').on('click',function(){
			if ($(this).is(':checked')) {
				$('.categories').each(function(){
					this.checked = true;
				});
			}else{
				$('.categories').each(function(){
					this.checked = false;
				});
			}
		});
		
		
		//################### Get value from select ###################//
		
		$i=0;
		$(".add_atr").click(function(e) {
			var attr_id = $("#attr_name").val();
			var count=$('.count_row').length;
			$('.load_wait').show();
			$.ajax({
				type: "POST",
				url: base_url+"admin/product/product_list/attributes_update/"+count,
				data: {'id':attr_id},
				success: function(re){
					if(attr_id ==8){
						$("#show_attr").append(re);
					}else if(attr_id ==9){
						$("#show_size").append(re);
					}else{
						
					}
					$('.load_wait').hide();			
				}
			});
			$i++;
		});
		$(document).on('click','.remove', function(){
			$(this).closest('.size_on').remove();
		});
		$(".add_varian").click(function(e) {
			$('.load_wait').show();
			$.ajax({
				type: "POST",
				url: base_url+"admin/product/product_list/varian",
				success: function(re){
					if(re){
						$("#show_varian").append(re);
					}
					$('.load_wait').hide();			
				}
			});
		});
		// attach button click listener on dom ready
		$('.save').click(function(){
			var group = $('#attr_name').val();
			var color = $('input[name^=userfile]').map(function(idx, elem) {
				return $(elem).val();
			}).get();
			var price = $('input[name^=p_color]').map(function(id, eleme) {
				return $(eleme).val();
			}).get();
			var size = $('input[name^=size]').map(function(ids, elems) {
				return $(elems).val();
			}).get();
			
			if(group == 9){
				$('input[name^=str_size]').val(group + ',' + size)
			}
			if(group == 8){
				$('input[name^=str_color]').val(group + ',' + color + '/' + price)
			}

			//alert(group + ',' + color + ',' +price);
			event.preventDefault();
		});
		
		$('#attr_name').change(function(){
			$('.save').show(200);
			//$('#show_attr div').empty();
		});
		
		//-------- Product tags -----------//
		var maxappend = 0;
		$('.add_tags').click(function(){
			var tag_id = $("#get_tags").val();
			if(tag_id ==""){
				return false;
			}else{
				var list='<li><a class="button_grey" href="#">'+tag_id+'</a>'+
						'<i class="tag_re glyphicon glyphicon-remove-circle"></i>'+
						'<input type="hidden" name="tags_con[]" value="'+tag_id+'"></li>';
				if(maxappend >=10) return;
				$(".tags_container ul").append(list);
				maxappend++;
			}
		});
		$(document).on('click','.tag_re', function(){
			$(this).parent('.tags_container ul li').remove();
		});
		$(document).on('click','.att_re', function(){
			$(this).closest('.att_on').remove();
			$(this).closest('.size_on').remove();
			//alert('ge');
		});
	});
	//CKEDITOR.replace('ckeditor');
</script>

<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>