<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-8">
				<h3><span class="fui-document"></span>Supplier Products</h3>
			</div><!-- /.col -->
			<div class="col-md-3 col-sm-4 text-right"><h3><?php //echo $success; ?></h3></div><!-- /.col -->
		</div><!-- /.row -->
		<?php 
			if(isset($errors)){
			   echo show_msg($errors);
			}
			if(isset($rmsg)){
			   echo show_msg($rmsg);
			} 
		 ?> 
		<hr class="dashed margin-bottom-30">
		<div class="panel panel-default table-responsive">
			<table class="table" id="product-list">
				<tr>
					<td style="border-bottom: 1px solid #ebebeb;">
						<?php echo form_open(base_admin_url().'admin_search/find_supplier_product', array('class'=> 'search-frm', 'id' => 'search-name','method'=>'get')); ?>
								<div id="custom-search-input">
									<div class="form-group col-md-3">
									<?php //echo form_label('Filter', 'find_id',array('class' => 'control-label')); ?>
									
									<select name="status" class="form-control">
										<option value="all"  <?php echo ($this->input->get("status")=="all")?"selected":""?> >Filter All</option>
										<option value="approved" <?php echo ($this->input->get("status")=="approved")?"selected":""?>>Approved</option>
										<option value="pending" <?php echo ($this->input->get("status")=="pending")?"selected":""?>>Pending</option>
										
									</select>
							
								</div>
								<div class="form-group col-md-3">
									<input type="text" name="search" id="search-product" value="<?php echo $this->input->get('search'); ?>" class="search-query search-boxes form-control" placeholder="Search Product">
								
								</div>
								<div class="form-group col-md-3">
									<input type="text" name="search_vendor" id="search-vendor" value="<?php echo $this->input->get('search_vendor'); ?>" class="search-query search-boxes form-control search-field ui-autocomplete-input" placeholder="Search Vendor">
									
									
								</div>
								<div class="form-group col-md-3">
									<input type="submit" class="btn btn blue form-control" value="Search">
								</div>
						
							</div>
						<?php echo form_close();?>
							
					</td>
					<td style="border-bottom: 1px solid #ebebeb;">
						<a class="btn btn-primary btn-labeled fa fa-plus-circle pull-right mar-rgt category" data-toggle="modal" href="<?php echo  base_url().'admin/product/supplier_product/create'?>">
								Create Product                                
						</a>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<div class="panel panel-default">
							<table data-show-export="true" data-search="true" data-show-refresh="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-pagination="true" data-side-pagination="server" data-url="#" class="table table-striped table-bordered table-hover" id="events-table">
								<thead>
									<tr>
										<th style="text-align: center;">N</th>
										<th style="text-align: center;">Image</th>
										<th style="text-align: center; ">Product Code</th>
										<th style="text-align: center; ">Product Name</th>
										<th style="text-align: center; ">Prices</th>
										<th style="text-align: center; ">Categories</th>
										<th style="text-align: center; ">Permalink</th>
									<?php if($this->session->userdata('group_id') == 1){ ?>
										<th style="text-align: center; ">Vendor</th>
									<?php } ?>	

										<th style="text-align: center; ">Create Date</th>
										<th style="text-align: center; ">Options</th>
									</tr>
								</thead>
								
								<tbody>
									<?php
										$i=0;
										foreach($data as $rows){
											
											$proimage = $rows['feature'];
											$proname_code = $rows['product_code'];
											$product_id = $rows['product_id'];
											$proname = $rows['product_name'];
											$permalink = $rows['permalink'];
											$username = '<br><a href="'.base_admin_url() 
																		.'product/supplier_product/supplierdetails/'
																		.$rows['user_id'].'" class="btn btn-success btn-xs" target="_blank" style="border-radius: 5px !important;">'
																		.$rows['username']. '</a>';
											$public_date = $rows['created_date'];									
											$proid = $rows['product_id'];
											$dir = date('m-Y', strtotime($rows['created_date']));
											$i++;
									?>
									<tr>

										<td style="text-align: left; ">
											<?php echo $i;?>
										</td>
										<td style="text-align: center; ">
											<img src="<?php echo base_url('images/products/'.$proimage);?>" style="width:60px;height:60px;border:1px solid #ddd;padding:2px; border-radius:2px !important;" class="img-sm">
										</td>
										<?php if($proname_code == null || $proname_code == ''){ ?>
											<td style="text-align: left; "><?php echo $product_id;?></td>
										<?php }else{ ?>
											<td style="text-align: left; "><?php echo $proname_code;?></td>
										<?php } ?>	
										<td style="text-align: left; "><?php echo $proname;?></td>
										<td style="text-align: left; "><?php echo '<div><b>Regular Price<b>: '.$rows['regular_prices'].'</div>'.'<div><b>Sale Price: </b>'.$rows['sale_price'].'</div>';?></td>
										<td style="text-align: left; "><?php echo get_category_join_link($proid);?></td>
										<td style="text-align: left; "><br><a class="btn btn-success btn-xs" target="_blank" href="<?php echo base_url() . 'product_detail/preview/'. $permalink; ?>" title="<?php echo $proname;?>">Preview</a></td>
									<?php if($this->session->userdata('group_id') == 1){ ?>
										<td style="text-align: left; "><?php echo $username; ?></td>
									<?php } ?>
										<td style="text-align: left; "><?php echo $public_date;?></td>
										<td style="text-align: right; ">  
											<?php echo btn_action('product/supplier_product/update/'.$proid,'product/product_list/update_poststatus/'.$proid.'/supplier_product');?>

											
											<?php if($this->session->userdata('group_id') == 1){ 
												if($rows['status'] == 1){
													$btn = 'btn-success';
													$status = '';
													$val = 'Approved';
												}else{
													$btn = 'btn-default';
													$status = '1';
													$val = 'Pending';
												}
											?>
												<form action="<?php echo base_url().'admin/product/supplier_product/change_visible/'.$proid; ?>" method="POST">
													<p style="display: none;"><input type="text" class="visible" value="<?php echo $status; ?>" name="show_visible"></p>
													<input type="submit" name="btn_save" class="btn <?php echo $btn; ?> btn-xs" value="<?php echo $val; ?>">
												</form>	
											<?php } ?>			
										</td>
									
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<div class="pull-right">
							<div class="title_page">
								<?php echo $showing;?>
							</div>
							<?php echo $page;?>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div><!---Contain-->	
	
	<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>

function show_data(){
 $('.show_content').load('test');
 // $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
$(".send_msg").on('click',function(){
	var send_msg=$(this).attr('id');
	var name=$('#msg_name').val();
	var email=$('#msg_email').val();
	var phone= $('#msg_phnoe').val();
	var subject= $('#msg_subject').val();
	var message= $('#msg_message').val();
	$.ajax({
		url: "http://localhost:81/ci3/admin/dashboard/message",
		type: 'POST',
		data: {
			'name':name,
			'email':email,
			'phone':phone,
			'subject':subject,
			'message':message
		},
		success: function(msg) {
			if(msg == 'YES'){
				alert('yes');
			}else if(msg == 'No'){
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}else{
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
		}
	});
});

</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>

</html>
