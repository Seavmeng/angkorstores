<div class="row att_on">
	<div class="col-sm-3">
		<div class="form-group" style="margin:0;">
			<label class="col-sm-12 control-label" for="wheel-price">Color</label>
			<div class="col-sm-12">
				<?php
				echo '<select class="form-control" name="varian_color[]">
						<option value="">Default Color</option>';
					$colors=attr_pro(8);
					foreach($colors as $row){
						echo '<option value="'.$row['attr_id'].'">'.$row['attr_name'].'</option>';
					}
				echo '</select><br/>';
				?>
				<font color="red"></font>
			</div>
			
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group" style="margin:0;">
			<label class="col-sm-12 control-label" for="wheel-demo">Size</label>
			<div class="col-sm-12">
				<?php
				echo '<select class="form-control" name="varian_size[]">
						<option value="">Default Size</option>';
					$colors=attr_pro(9);
					foreach($colors as $row){
						echo '<option value="'.$row['attr_id'].'">'.$row['attr_name'].'</option>';
					}
				echo '</select><br/>';
				?>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group" style="margin:0;">
			<label class="col-sm-3 control-label" for="wheel-price">Price</label>
			<div class="col-sm-12">
				<input type="text" class="form-control" name="varian_price[]"><br/>
				<font color="red"></font>
			</div>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="col-sm-12">
			<i class="att_re glyphicon glyphicon-trash"></i>
		</div>
	</div>
</div>

