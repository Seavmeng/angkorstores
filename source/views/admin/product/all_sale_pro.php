<?php $this->load->view('includes/head')?>
<body class="home option3">
<!-- HEADER -->
<div id="header" class="header">
	<?php $this->load->view('includes/header')?>
</div>
<!-- end header -->
<div class="sale-container">
	<div class="container sale-slide">
		<div class="col-sm-3 sale-title">
			BEST SELLERS
		</div>
		<div class="col-sm-9">
			
		</div>
	</div>
</div>
<div class="sale-container">
	<div class="container sale-pro">
		<ul class="pro-list">
			<?php
				foreach($top as $sale){
					$img = cut_string($sale['feature']);
					$id = $sale['product_id'];
					$name = $sale['product_name'];
					$price = $sale['regular_prices'];
					$oldprice = $sale['sale_price'];
					$sell = $sale['sale_amoung'];
					$disdate = $sale['created_date'];
					$dir = date('m-Y', strtotime($sale['created_date']));
			?>
			<li class="pro-lists">
				<div class="product-container">
					<div class="left-block">
						<a href="<?php echo base_url().'product-detail/'.$id;?>">
							<img class="img-responsive" alt="product" src="<?php echo base_url().'images/products/'.$img;?>" style="height:200px;"/>
						</a>
					</div>
					<div class="right-block">
						<h5 class="product-name">
							<a href="<?php echo base_url().'product-detail/'.$id;?>"><?php echo $name;?></a>
						</h5>
						<div class="product-star">
							Sales <?php echo $sell;?>
						</div>
						<div class="content_price">
							<span class="price product-price">
								$<?php echo $price;?>
							</span>
							<?php
								if($oldprice == 0 or $disdate <= date('Y-m-d h:i:s')){
									
								}else{
							?>
								<span class="price old-price">
									$ <?php echo $oldprice;?>
								</span>
							<?php
								}
							?>
						</div>
					</div>
				</div>
			</li>
			<?php
				}
			?>
		</ul>
	</div>
</div>
<!-- Footer -->
<?php $this->load->view('includes/footer')?>

</body>
</html>