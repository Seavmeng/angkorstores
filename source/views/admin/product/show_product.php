 <?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-9 col-sm-8">
			<h3><span class="fui-document"></span>Manage Product</h3>
		</div><!-- /.col -->
		<div class="col-md-3 col-sm-4 text-right"><h3><?php //echo $success; ?></h3></div><!-- /.col -->
	</div><!-- /.row -->
	
	<?php 
		if(isset($errors)){
		   echo show_msg($errors);
		}
		if(isset($rmsg)){
		   echo show_msg($rmsg);
		}
		$group_id = $this->session->userdata('group_id'); 
	 
	?>
	 
	<hr class="dashed margin-bottom-30">
	<div class="panel panel-default table-responsive">
					
		<table class="table" id="product-list">
			<tr>
				<?php echo form_open(base_admin_url().'admin_search/filterSearch', array('class'=> 'search-frm', 'id' => 'search-name', 'method' => 'get')); ?>		
				<td>
					<div class="col-md-12 col-sm-12">
						<div class="form-group col-md-3">
							<?php echo form_label('Filter', 'find_id',array('class' => 'control-label')); ?>
							<?php if($this->session->userdata('group_id')== 1){ ?>
							<select name="status" class="form-control">
								<option value="all" <?php echo $this->input->get('status')=="all"?"selected":"" ;?>>All</option>
								<option value="approved" <?php echo $this->input->get('status')=="approved"?"selected":"" ;?>>Approved</option>
								<option value="pending" <?php echo $this->input->get('status')=="pending"?"selected":"" ;?>>Pending</option>
								<option value="no_weight" <?php echo $this->input->get('status')=="no_weight"?"selected":"" ;?>>No Weight</option>
								<option value="weight" <?php echo $this->input->get('status')=="weight"?"selected":"" ;?>>Has Weight</option>
								<option value="contact_now" <?php echo $this->input->get('status')=="contact_now"?"selected":"" ;?>>Contact Now</option>
                                <option value="discount" <?php echo $this->input->get('status')=="discount"?"selected":"" ;?>>Discount</option>
							</select>
							<?php }else{ ?>
							<select name="status" class="form-control">
								<option value="all">All</option>
								<option value="approved">Approved</option>
								<option value="pending">Pending</option>
								
							</select>
							<?php } ?>
						</div>
                        <div class="form-group col-md-1" id="discount_percent">
							<label for="" class="control-label">Min Disc</label>
							<!--<input type="text" class="form-control" name="discount_percent" placeholder="1% - 100%">-->
							<input type="text" class="form-control" name="discount_percent" value="<?php echo $this->input->get('discount_percent')==""?"1":$this->input->get('discount_percent');?>">
						</div>
						<div class="form-group col-md-1" id="discount_percent">
							<label for="" class="control-label">Max Disc</label>
							<input type="text" class="form-control" name="max_discount" value="<?php echo $this->input->get('max_discount')==""?"100":$this->input->get('max_discount');?>">
						</div>
						<div class="form-group col-md-5">
							<?php echo form_label('Search Products', 'search',array('class' => 'control-label')); ?>
							<?php echo form_input(array('name' => 'search', 'class' => 'form-control', 'id' => 'text',  'value' => $this->input->get('search') ) ); ?>											
						</div>
						<div class="form-group col-md-2">
							<label for="" class="control-label">&nbsp;</label>
							<input type="submit" class="btn btn blue form-control" value="<?php word('search'); ?>">											
						</div>
					</div>
					</div>
				</td>
				
			
			
				<td style="border-bottom: 1px solid #ebebeb;">
					<a class="btn btn-primary btn-labeled fa fa-plus-circle pull-right mar-rgt category" data-toggle="modal" href="<?php echo  base_url().'admin/product/product_list/create'?>">
							Create Product                                
					</a>
				</td>
				<?php echo form_close();?>
			</tr>
			<tr>
				<td colspan='2'>
					<div class="panel panel-default">
						<table data-show-export="true" data-search="true" data-show-refresh="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-pagination="true" data-side-pagination="server" data-url="#" class="table table-striped table-bordered table-hover" id="events-table">
							<thead>
								<tr>
									<th style="text-align: center;">N</th>
									<th style="text-align: center;">Image</th>
									<th style="text-align: center; ">Product Code</th>
									<th style="text-align: center; ">Product Name</th>
									<?php if($this->session->userdata('group_id')==1){ ?>
										<th style="text-align: center; ">Weight & Prices</th>
										<?php }else{ ?>
										<th style="text-align: center; ">Prices</th>
										<?php } ?>
									<th style="text-align: center; ">Categories</th>
								<?php if($this->session->userdata('group_id') == 1){ ?>
									<th style="text-align: center; ">Product By</th>
								<?php } ?>	

									<th style="text-align: center; ">Create Date</th>
									<th style="text-align: center; ">Options</th>
								</tr>
							</thead>
							
							<tbody>
								<?php
									$i=0;
									foreach($data as $rows){
										
										$proimage = $rows['feature'];
										$proname_code = $rows['product_code'];
										$product_id = $rows['product_id'];
										$proname = $rows['product_name'];
										$username = '<b>'.$rows['user_group_name'].'</b>: <br/>'.$rows['username'];
										$public_date = $rows['created_date'];									
										$proid = $rows['product_id'];
										$dir = date('m-Y', strtotime($rows['created_date']));
										$hot_pro = $rows['hot_pro'] ;
										$i++;
								?>
								<tr>

									<td style="text-align: left; ">
										<?php echo $i;?>
									</td>
									<td style="text-align: center; ">
									
										<img src="<?php if(!empty($proimage)){
															echo base_url('images/products/'.$proimage);
														}
														else{
															echo base_url('images/no_img.gif');
														}?>" style="width:60px;height:60px;border:1px solid #ddd;padding:2px; border-radius:2px !important;" class="img-sm">
									</td>
									<?php if($proname_code == null || $proname_code == ''){ ?>
										<td><?php echo $product_id;?></td>
									<?php }else{ ?>
										<td><?php echo $proname_code;?></td>
									<?php } ?>	
									<td><a href="<?php echo base_url().'detail/'.$rows['permalink'];?>" target="_blank"><?php echo $proname;?></a></td>
									<td width="150px">
										<?php  
										if($this->session->userdata('group_id')==1){
											echo '<div><font color="#C63927">Weight: '.$rows['weight'].'</font></div>';
											echo '<div><font color="#C63927">Length: </b>'.$rows['length'].'</font></div>';
											echo '<div><font color="#C63927">Width: </b>'.$rows['width'].'</font></div>';
											echo '<div><font color="#C63927">Height: </b>'.$rows['height'].'</font></div>';
											echo '<div><b>Regular Price<b>: '.$rows['regular_prices'].'</div>';
										}
											echo '<div><b>Sale Price: </b>'.$rows['sale_price'].'</div>';
											if(!empty($rows['discount_amount'])){
												echo '<div><b>Discount: </b>'.$rows['discount_amount'].' % <br/><small><i>'.date('d-m-Y',strtotime($rows['from_date'])).' to '.date('d-m-Y',strtotime($rows['todate'])).'</i></small></div>';
											}
											if($rows['contact-type']){
												echo '<span class="badge badge-primary">Contact Now</span>';
											}
										?>
									</td>
									<td><?php echo get_category_join_link($proid);?></td>
								<?php if($this->session->userdata('group_id') == 1){ ?>
									<td><?php echo $username; ?></td>
								<?php } ?>
									<td><?php echo $public_date;?></td>
									<td>  
										<?php echo btn_action('product/product_list/update/'.$proid,'product/product_list/update_poststatus/'.$proid);?>

										
										<?php 
											if($rows['status'] == 1){
												$btn = 'btn-success';
												$status = '';
												$val = 'Approved';
											}else{
												$btn = 'btn-default';
												$status = '1';
												$val = 'Pending';
											}
										if($this->session->userdata('group_id') == 1){ 	
										?>
											<form action="<?php echo base_url().'admin/product/product_list/change_visible/'.$proid; ?>" method="POST">
												<p style="display: none;"><input type="text" class="visible" value="<?php echo $status; ?>" name="show_visible"></p>
												<input type="submit" name="btn_save" class="btn <?php echo $btn; ?> btn-xs" value="<?php echo $val; ?>">
											</form>	
										<?php }else{ ?>
											<input type="submit" name="btn_save" class="btn <?php echo $btn; ?> btn-xs" value="<?php echo $val; ?>">	
										<?php } if($this->session->userdata('group_id') == 1): ?>
										  <button class="do_action <?php echo $hot_pro? "hot_pro" : "no_hot_pro " ?> " do_action="<?=  $hot_pro ?>" do_action_id="<?=  $product_id ?>" > 
										  		<i class="fa fa-star " aria-hidden="true"  ></i> Hot 
										  </button>
										 <?php endif; ?> 	
									</td>
								
								</tr>
								<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan='2'>
					<div class="pull-right">
						<div class="title_page">
							<?php echo $showing;?>
						</div>
						<?php echo $page;?>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div><!---Contain-->	

 <style>
 	 .hot_pro i{
 	 	color: #faa61a;
 	 }
 	 .no_hot_pro i{

 	 	color: #444;
 	 }

 </style>
<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>


$(document).ready(function(){   

    $(".do_action ").click(function()
    {     
       var  status = $(this).attr('do_action') ;
       var  p_id   = $(this).attr('do_action_id') ;
       var  url_base = <?= json_encode(base_url());?> ;
       var  buttons = $(this) ;   
	    jQuery.ajax({
					type: "POST",
					url: url_base + "admin/product/product_list/check_hot_product",
					dataType: 'json',
					data: {hot_pro: status, pro_id: p_id},
					success: function(res) {
					     console.log(res);

                         buttons.attr('do_action',res.hot_pro);

                         if(res.hot_pro){					                        	
                         	buttons.addClass('hot_pro') ;
                         	buttons.removeClass('no_hot_pro') ;                          	
                         }else{

                         	buttons.addClass('no_hot_pro') ;
                         	buttons.removeClass('hot_pro') ;                          
                         	 }

					}
		 });
    });
 });


function show_data(){
$('.show_content').load('test');
// $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
$(".send_msg").on('click',function(){
var send_msg=$(this).attr('id');
var name=$('#msg_name').val();
var email=$('#msg_email').val();
var phone= $('#msg_phnoe').val();
var subject= $('#msg_subject').val();
var message= $('#msg_message').val();
$.ajax({
	url: "http://localhost:81/ci3/admin/dashboard/message",
	type: 'POST',
	data: {
		'name':name,
		'email':email,
		'phone':phone,
		'subject':subject,
		'message':message
	},
	success: function(msg) {
		if(msg == 'YES'){
			alert('yes');
		}else if(msg == 'No'){
			 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
		}else{
			 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
		}
	}
});
});

</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>

</html>
