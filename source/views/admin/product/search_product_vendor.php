<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-8">
			<?php 
				$strSearch = $this->input->get('search');
				$strFilter = $this->input->get('status');
				if($strSearch == ''){
					switch($strFilter){
						case 'pending':
						$searchLabel = 'Filter for all pending products.';
						break;
						case 'approved':
						$searchLabel = 'Filter for all approved products.';
						break;
						case 'weight':
						$searchLabel = 'Filter for all products with weight.';
						break;
						case 'no_weight':
						$searchLabel = 'Filter for all products with no weight.';
						default:
						$searchLabel = 'No filter or search has been applied.';
						break;
					}
					
				}
				else{
					switch($strFilter){
						case 'pending':
						$searchLabel = 'Search Result for all pending products containing "' . $strSearch . '".';
						break;
						case 'approved':
						$searchLabel = 'Search Result for all approved products containing "' . $strSearch . '".';
						break;
						case 'weight':
						$searchLabel = 'Search Result for all products having weight and containin "' . $strSearch . '".';
						break;
						case 'no_weight': 
						$searchLabel = 'Search Result for all products having no weight and containing "' . $strSearch . '".';
						break;
						default:
						$searchLabel = 'Search Result for all products containing "' . $strSearch . '".';
						break;
					}
					
				}
				
			?>
				<h3><span class="fui-document"></span><?php echo $searchLabel; ?></h3>
			</div><!-- /.col -->
			<div class="col-md-3 col-sm-4 text-right"><h3><?php //echo $success; ?></h3></div><!-- /.col -->
		</div><!-- /.row -->
		<?php 
			if(isset($errors)){
			   echo show_msg($errors);
			}
			if(isset($rmsg)){
			   echo show_msg($rmsg);
			}
			// $catFilter = catForFilter();
			$group_id = $this->session->userdata('group_id');
		
		 ?>
		 
		 
		<hr class="dashed margin-bottom-30">
		<div class="panel panel-default">
						
			<table class="table" id="product-list">
				<tr>
					<td style="border-bottom: 1px solid #ebebeb;">
						<?php echo form_open(base_admin_url().'admin_search/find_supplier_product', array('class'=> 'search-frm', 'id' => 'search-name','method'=>'get')); ?>
								<div id="custom-search-input">
									<div class="form-group col-md-3">
									<?php //echo form_label('Filter', 'find_id',array('class' => 'control-label')); ?>
									
									<select name="status" class="form-control">
										<option value="all" <?php echo ($this->input->get("status")=="all")?"selected":""?>>Filter All</option>
										<option value="approved" <?php echo ($this->input->get("status")=="approved")?"selected":""?>>Approved</option>
										<option value="pending" <?php echo ($this->input->get("status")=="pending")?"selected":""?>>Pending</option>
									</select>
							
								</div>
								<div class="form-group col-md-3">
									<input type="text" name="search" id="search-product" value="<?php echo $this->input->get('search'); ?>" class="search-query search-boxes form-control" placeholder="Search Product">
								
								</div>
								<div class="form-group col-md-3">
									<input type="text" name="search_vendor" id="search-vendor" value="<?php echo $this->input->get('search_vendor'); ?>" class="search-query search-boxes form-control search-field ui-autocomplete-input" placeholder="Search Vendor">
									
									
								</div>
								<div class="form-group col-md-3">
									<input type="submit" class="btn btn blue form-control" value="Search">
								</div>
						
							</div>
						<?php echo form_close();?>
							
					</td>
					
				
				
					<td style="border-bottom: 1px solid #ebebeb;">
						<a class="btn btn-primary btn-labeled fa fa-plus-circle pull-right mar-rgt category" data-toggle="modal" href="<?php echo  base_url().'admin/product/product_list/create'?>">
								Create Product                                
						</a>
					</td>
					
				</tr>
				<tr>
					<td colspan='2'>
						<div class="panel panel-default">
							<table data-show-export="true" data-search="true" data-show-refresh="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-pagination="true" data-side-pagination="server" data-url="#" class="table table-striped table-bordered table-hover" id="events-table">
								<thead>
									<tr>
										<th style="text-align: center;">N</th>
										<th style="text-align: center;">Image</th>
										<th style="text-align: center; ">Product Code</th>
										<th style="text-align: center; ">Product Name</th>
										<?php if($this->session->userdata('group_id')==1){ ?>
										<th style="text-align: center; ">Weight & Prices</th>
										<?php }else{ ?>
										<th style="text-align: center; ">Prices</th>
										<?php } ?>
										<th style="text-align: center; ">Categories</th>
										<?php if($this->session->userdata('group_id') == 1){ ?>
										<th style="text-align: center; ">Product By</th>
										<?php } ?>	
										<th style="text-align: center; ">Create Date</th>
										<th style="text-align: center; ">Options</th>
									</tr>
								</thead>
								
								<tbody>
									<?php
										$i=0;
										foreach($results as $rows){
											
											$proimage = $rows['feature'];
											$proname_code = $rows['product_code'];
											$product_id = $rows['product_id'];
											$proname = $rows['product_name'];
											$public_date = $rows['created_date'];
											$username = '<b>'.$rows['user_group_name'].'</b>: <br/>'.$rows['username'];
											$public_date = $rows['created_date'];

											$proid = $rows['product_id'];
											$dir = date('m-Y', strtotime($rows['created_date']));
											$i++;
									?>
									<tr>
										<td>
											<?php echo $i;?>
										</td>
										<td>
											<img src="<?php echo base_url('images/products/'.$proimage);?>" style="width:60px;height:60px;border:1px solid #ddd;padding:2px; border-radius:2px !important;" class="img-sm">
										</td>
										<?php if($proname_code == null || $proname_code == ''){ ?>
											<td><?php echo $product_id;?></td>
										<?php }else{ ?>
											<td><?php echo $proname_code;?></td>
										<?php } ?>
										<td><?php echo $proname;?></td>
										<td width="150px">
										<?php 
										if($this->session->userdata('group_id')==1){
											echo '<div><font color="#C63927">Width: '.$rows['weight'].'</font></div>';
											echo '<div><font color="#C63927">Length: </b>'.$rows['length'].'</font></div>';
											echo '<div><font color="#C63927">Width: </b>'.$rows['width'].'</font></div>';
											echo '<div><font color="#C63927">Height: </b>'.$rows['height'].'</font></div>';
											echo '<div><b>Regular Price<b>: '.$rows['regular_prices'].'</div>';
										}
											echo '<div><b>Sale Price: </b>'.$rows['sale_price'].'</div>';
											if($rows['contact-type']){
												echo '<span class="badge badge-primary">Contact Now</span>';
											}
										?>
										</td>
										<td><?php echo get_category_join_link($proid);?></td>
										<?php if($this->session->userdata('group_id') == 1){ ?>
										<td><?php echo $username; ?></td>
									<?php } ?>
										<td><?php echo $public_date;?></td>
										<td style="text-align: right; ">  
											<?php 
											
											if($rows['group_id'] !=2):
												echo btn_action('product/product_list/update/'.$proid,'product/product_list/delete/'.$proid);
											else:
												echo btn_action('product/supplier_product/update/'.$proid,'product/product_list/delete/'.$proid);
											endif;
											?>
											
											<?php if($this->session->userdata('group_id') == 1 || $this->session->userdata('group_id') == 2){ 
												if($rows['status'] == 1){
													$btn = 'btn-success';
													$status = '';
													$val = 'Approved';
												}else{
													$btn = 'btn-default';
													$status = '1';
													$val = 'Pending';
												}
											?>
												<form action="<?php echo base_url().'admin/product/product_list/change_visible/'.$proid; ?>" method="POST">
													<p style="display: none;"><input type="text" class="visible" value="<?php echo $status; ?>" name="show_visible"></p>
													<input type="submit" name="btn_save" class="btn <?php echo $btn; ?> btn-xs" value="<?php echo $val; ?>">
												</form>	
											<?php }else{ ?>
												<input type="submit" name="btn_save" class="btn <?php echo $btn; ?> btn-xs" value="<?php echo $val; ?>">
											<?php } ?>	
										</td>
									
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<div class="pull-right">
							<div class="title_page">
								<?php echo $showing;?>
							</div>
							<?php echo $page; ?>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div><!---Contain--->	
	
	<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
function show_data(){
 $('.show_content').load('test');
 // $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
$(".send_msg").on('click',function(){
	var send_msg=$(this).attr('id');
	var name=$('#msg_name').val();
	var email=$('#msg_email').val();
	var phone= $('#msg_phnoe').val();
	var subject= $('#msg_subject').val();
	var message= $('#msg_message').val();
	$.ajax({
		url: "http://localhost:81/ci3/admin/dashboard/message",
		type: 'POST',
		data: {
			'name':name,
			'email':email,
			'phone':phone,
			'subject':subject,
			'message':message
		},
		success: function(msg) {
			if(msg == 'YES'){
				alert('yes');
			}else if(msg == 'No'){
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}else{
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
		}
	});
});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>

</html>