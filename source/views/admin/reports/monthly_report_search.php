<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-sm-4">
				<h3><span class="fui-document"></span>Monthly Reports</h3>
			</div><!-- /.col -->
			
		</div><!-- /.row -->
		<?php 
			if(isset($errors)){
			   echo msg($errors);
			}
			if(isset($rmsg)){
			   echo show_msg($rmsg);
			}
			//	$catFilter = catForFilter();
			echo $this->session->flashdata('msg'); 
			$year = date('Y-m');
			$date = isset($_GET['from']) ? $_GET['from'] : date('Y-m');
			$prev_date = date('Y-m', strtotime($date .' -1 month'));
			$next_date = date('Y-m', strtotime($date .' +1 month'));

			$get_year_month = $this->input->get('from');
			$get_month = substr($get_year_month, 5);
			$get_year = substr($get_year_month, 0 ,-3);
			switch ($get_month) {
				case '01':
					$get_month = str_replace('01', 'January', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				case '02':
					$get_month = str_replace('02', 'February', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				case '03':
					$get_month = str_replace('03', 'March', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				case '04':
					$get_month = str_replace('04', 'April', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				case '05':
					$get_month = str_replace('05', 'May', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				case '06':
					$get_month = str_replace('06', 'June', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				case '07':
					$get_month = str_replace('07', 'July', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				case '08':
					$get_month = str_replace('08', 'August', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				case '09':
					$get_month = str_replace('09', 'September', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				case '10':
					$get_month = str_replace('10', 'October', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				case '11':
					$get_month = str_replace('11', 'November', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				case '12':
					$get_month = str_replace('12', 'December', $get_month);
					$from = $get_month.'-'.$get_year;
					break;
				default:
					$from = $this->input->get('from');
					break;
			}
		?>
		<?php echo $this->session->flashdata('msg'); ?>
		<hr class="dashed margin-bottom-30">
		<div class="panel panel-default">
			<form id="search-name" class="search-frm search" accept-charset="utf-8" action="<?php echo base_admin_url().'reports/report/monthly_search';?>" method="get">
			<table class="table" id="product-list">
			        <div style="background:#cccccc;" class="col-md-12 col-sm-12">							
						<div class="col-sm-4 col-md-4">
							<p class="pull-left" style="margin-top:8px;"><a href="?from=<?=$prev_date;?>"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Previous</a></p>
						</div>
						<div class="col-sm-4 col-md-4">
							<center style="margin-top:8px;"><b><?php echo $from; ?></b></center>
						</div>
						<div class="col-sm-4 col-md-4">
							<p class="pull-right" style="margin-top:8px;"><a href="?from=<?=$next_date;?>">Next <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
						</div>
					</div>
				<tr>
					<td style="border-bottom: 1px solid #ebebeb;">
						<div class="col-md-12 col-sm-12">							
							<div class="form-group col-md-5 ">
								
								<div class="col-sm-6">
								<label class="control-label"><?php echo 'Select Monthly'?> :</label>
									<div class="clearfix">
										<div class="input-group pull-center" data-placement="left" data-align="top" data-autoclose="true">
										
											<input type="text"  name="from" value="<?php echo $this->input->get('from'); ?>" class="form-control datepicker"> 
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
											<font color="red"><?php echo form_error('from'); ?></font>
										</div>
									</div>
								</div>	
							</div>
																										
							<div class="form-group col-md-3">
								<label for="" class="control-label">&nbsp;</label>
								<input type="submit" class="btn btn blue form-control" value="<?php word('search'); ?>">											
							</div>
						</div>
				    </td>
				</tr>
				<tr>
					<td>
						<div class="panel panel-default">
							<table data-show-export="true" data-search="true" data-show-refresh="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-pagination="true" data-side-pagination="server" data-url="#" class="table table-striped table-bordered table-hover" id="events-table">
								<thead>
									<tr>
										<th style="text-align: center;">N</th>
										<th style="text-align: center; ">Bill ID</th>
										<th style="text-align: center; ">Product ID</th>
										<th style="text-align: center; ">Product Name</th>
										<th style="text-align: center; ">Date</th>
										<th style="text-align: center; ">Price</th>
									</tr>
								</thead>
								
								<tbody>
									<?php
										// var_dump($billing);
										$i=0;
										$total=0;
										foreach($billing as $rows){
											$order_id = $rows['order_id'];
											$bill_id = $rows['bill_id'];
											$product_id = $rows['product_id'];
											$product_name = $rows['product_name'];
											$qty = $rows['qty'];
											$prices = $rows['prices'];
											$payment_id = $rows['payment_id'];
											$created = $rows['created'];
											$total_price = $rows['prices']*$rows['qty'];
											$i++;
											$order_number='ID'.str_pad($order_id, 6, '0', STR_PAD_LEFT);
									?>
									<tr>
										<td style="text-align: left; "><?php echo $i;?></td>
										<td style="text-align: center; "><?php echo $bill_id;?></td>
										<td style="text-align: center; "><?php echo $product_id;?></td>
										<td style="text-align: left; "><a href="<?php echo  base_url().'admin/reports/report/view/'.$order_id;?>"><?php echo $product_name;?></a></td>
										<td style="text-align: left; "><?php echo $rows['created'];?></td>
										<td style="text-align: left; "><?php echo '<span>$</span> '.$total_price; ?></td>
									</tr>
									<?php
										$total += $total_price;
										}
									?>
									<tr>
										<td colspan="4"></td>
										<td><strong>Sub Total</strong></td>
										<td><?php echo currency('sign').$total;?></td>
									</tr>
									<tr>
										<td colspan="4"></td>
										<td><strong>Shipping</strong></td>
										<td>
											<?php
											$shipping=0;
											foreach($billing_detail as $rows){
												$shipping +=$rows['total_shipping'];
											}
											echo currency('sign').$shipping;
											?>
										</td>
									</tr>
									<tr>
										<td colspan="4"></td>
										<td><strong>Total</strong></td>
										<td><?php echo currency('sign').($total+$shipping);?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</td>
					
				</tr>
			</table>
			</form>
		</div>
	</div>
	</body>
			<!---Contain--->	
	<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
$(document).ready(function() {		
	$('.datepicker').datepicker({
		format: "yyyy-mm",
		viewMode: "months", 
		minViewMode: "months"
	});
		
});
function show_data(){
 $('.show_content').load('test');
 // $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
$(".send_msg").on('click',function(){
	var send_msg=$(this).attr('id');
	var name=$('#msg_name').val();
	var email=$('#msg_email').val();
	var phone= $('#msg_phnoe').val();
	var subject= $('#msg_subject').val();
	var message= $('#msg_message').val();
	$.ajax({
		url: "http://localhost:81/ci3/admin/dashboard/message",
		type: 'POST',
		data: {
			'name':name,
			'email':email,
			'phone':phone,
			'subject':subject,
			'message':message
		},
		success: function(msg) {
			if(msg == 'YES'){
				alert('yes');
			}else if(msg == 'No'){
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}else{
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
		}
	});
});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>
</html>