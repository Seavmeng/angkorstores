 <?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); 
function get_supplier_name($user_id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_users');
	$ci->db->where('user_id',$user_id);
	$query = $ci->db->get();
	return $query->first_row('array');
}
?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-8">
				<h3><span class="fui-document"></span>My Orders</h3>
			</div><!-- /.col -->
			<div class="col-md-3 col-sm-4 text-right"><h3><?php //echo $success; ?></h3></div><!-- /.col -->
		</div><!-- /.row -->
		<?php 
			if(isset($errors)){
			   echo msg($errors);
			}
			if(isset($rmsg)){
			   echo show_msg($rmsg);
			}
			$supplier=get_supplier_name($rows['supplier_id']);
		 ?>
		 <?php echo $this->session->flashdata('msg'); ?>
		<hr class="dashed margin-bottom-30">
		<div class="panel panel-default">
		<form id="search-name" class="search-frm search" accept-charset="utf-8" action="<?php echo base_admin_url().'reports/report/find_id';?>" method="get">
			<table class="table" id="product-list">
				<tr>
					<td>
						<ul class="woocommerce-thankyou-order-details order_details">
							<li class="order">
								Supplier Name: <strong><?php 
								if($supplier['username']){
									echo $supplier['username'];
								}else{
									echo 'No Supplier';
								}
								?></strong>
							</li>
							<li class="order">
								Supplier Detail: <strong><?php echo $supplier['bio']; ?></strong>
							</li>
						</ul>
						<div class="panel panel-default">
							<table class="table table-striped table-bordered table-hover" id="events-table">
								
									<?php
										$proimage = $rows['feature'];
										if($proimage){
											$image='<img src="'.base_url('images/products/'.$proimage).'" style="width:150px;height:150px;border:1px solid #ddd;padding:2px; border-radius:2px !important;" class="img-sm">';
										}else{
											$image="No Image";
										}
										$pro_id = $rows['product_id'];
										$product_code=$rows['product_code'];
										$product_name = $rows['product_name'];
										$supplier_id = $rows['supplier_id'];
										$price = $rows['sale_price'];
									?>
									<tr>
										<th>Image</th>
										<td>
											<?php echo $image;?>
										</td>
									</tr>
										<th>Product Code</th>
										<td><?php echo $product_code;?></td>
									</tr>
									<tr>
										<th>Product Name</th>									
										<td><?php echo $product_name;?></td>
									</tr>
									<tr>
										<th>Weight</th>									
										<td><?php echo $rows['weight'];?></td>
									</tr>
									<tr>
										<th>Stock</th>									
										<td><?php echo $rows['stock_qty'];?></td>
									</tr>
									<tr>
										<th>Created Date</th>
										<td><?php echo $rows['created_date'];?></td>
									</tr>
										<th>Regular price</th>
										<td><?php echo '<span>$</span> '.$rows['regular_prices']; ?></td>
									</tr>
									</tr>
										<th>Sale Price</th>
										<td><?php echo '<span>$</span> '.$price; ?></td>
									</tr>
								
							</table>
						</div>
					</td>
					
				</tr>
				<tr>
					<td>
						<div class="pull-right">
							<div class="title_page">
								<?php //echo $showing;?>
							</div>
							<?php //echo $page;?>
						</div>
					</td>
				</tr>
			</table>
	</div>
	</div>
	</body>


			<!---Contain--->	
	
	<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
$(document).ready(function() {		
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			//startDate: '-3d'
		});
		
});
function show_data(){
 $('.show_content').load('test');
 // $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
$(".send_msg").on('click',function(){
	var send_msg=$(this).attr('id');
	var name=$('#msg_name').val();
	var email=$('#msg_email').val();
	var phone= $('#msg_phnoe').val();
	var subject= $('#msg_subject').val();
	var message= $('#msg_message').val();
	$.ajax({
		url: "http://localhost:81/ci3/admin/dashboard/message",
		type: 'POST',
		data: {
			'name':name,
			'email':email,
			'phone':phone,
			'subject':subject,
			'message':message
		},
		success: function(msg) {
			if(msg == 'YES'){
				alert('yes');
			}else if(msg == 'No'){
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}else{
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
		}
	});
});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>

</html>