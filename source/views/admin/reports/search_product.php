<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); ?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-8">
				<h3><span class="fui-document"></span>Search Result</h3>
			</div><!-- /.col -->
			<div class="col-md-3 col-sm-4 text-right"><h3><?php //echo $success; ?></h3></div><!-- /.col -->
		</div><!-- /.row -->
		<?php 
			if(isset($errors)){
			   echo show_msg($errors);
			}
			if(isset($rmsg)){
			   echo show_msg($rmsg);
			}
			// $catFilter = catForFilter();
		
		 ?>
		 
		 
		<hr class="dashed margin-bottom-30">
		<div class="panel panel-default">
						
			<table class="table" id="product-list">
				<tr>
					<td style="border-bottom: 1px solid #ebebeb;">
						<?php echo form_open(base_admin_url().'admin_search', array('class'=> 'search-frm', 'id' => 'search-name' )); ?>
							<div id="custom-search-input">
								<div class="input-group col-md-4">
									
									<?php echo form_input(array('type'=> 'text', 'name'=>'search', 'class'=>'search-query form-control', 'placeholder'=>'Search') ); ?>
									<span class="input-group-btn">
										<button class="btn btn-danger" type="submit">
											<span class=" glyphicon glyphicon-search"></span>
										</button>
									</span>
								</div>
							</div>
						<?php echo form_close();?>
							
					</td>
					<td style="border-bottom: 1px solid #ebebeb;">
						<a class="btn btn-primary btn-labeled fa fa-plus-circle pull-right mar-rgt category" data-toggle="modal" href="<?php echo  base_url().'admin/product_list/create'?>">
								Create Product                                
						</a>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<div class="panel panel-default">
							<table data-show-export="true" data-search="true" data-show-refresh="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-pagination="true" data-side-pagination="server" data-url="#" class="table table-striped table-bordered table-hover" id="events-table">
								<thead>
									<tr>
										<th style="text-align: center;">N</th>
										<th style="text-align: center;">Image</th>
										<th style="text-align: center; ">Product Code</th>
										<th style="text-align: center; ">Product Name</th>
										<th style="text-align: center; ">Categories</th>
										<th style="text-align: center; ">Create Date</th>
										<th style="text-align: center; ">Options</th>
									</tr>
								</thead>
								
								<tbody>
									<?php
										$i=0;
										foreach($results as $rows){
											
											$proimage = $rows['feature'];
											$proname_code = $rows['product_code'];
											$proname = $rows['product_name'];
											$public_date = $rows['created_date'];
											$proid = $rows['product_id'];
											$dir = date('m-Y', strtotime($rows['created_date']));
											$i++;
									?>
									<tr>
										<td style="text-align: left; ">
											<?php echo $i;?>
										</td>
										<td style="text-align: center; ">
											<img src="<?php echo base_url('images/products/'.$proimage);?>" style="width:60px;height:60px;border:1px solid #ddd;padding:2px; border-radius:2px !important;" class="img-sm">
										</td>
										<td style="text-align: left; "><?php echo $proname_code;?></td>
										<td style="text-align: left; "><?php echo $proname;?></td>
										<td style="text-align: left; "><?php echo get_category_join_link($proid);?></td>
										<td style="text-align: left; "><?php echo $public_date;?></td>
										<td style="text-align: right; ">  
											<?php echo btn_action('product/product_list/update/'.$proid,'product/product_list/delete/'.$proid);?>
											
											<?php if($this->session->userdata('group_id') == 1){ 
												if($rows['status'] == 1){
													$btn = 'btn-success';
													$status = '';
													$val = 'Enable';
												}else{
													$btn = 'btn-default';
													$status = '1';
													$val = 'Disable';
												}
											?>
												<form action="<?php echo base_url().'admin/product/product_list/change_visible/'.$proid; ?>" method="POST">
													<p style="display: none;"><input type="text" class="visible" value="<?php echo $status; ?>" name="show_visible"></p>
													<input type="submit" name="btn_save" class="btn <?php echo $btn; ?> btn-xs" value="<?php echo $val; ?>">
												</form>	
											<?php } ?>	
										</td>
									
									</tr>
									<?php
										}
									?>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan='2'>
						<div class="pull-right">
							<div class="title_page">
								<?php echo $showing;?>
							</div>
							<?php echo $page;?>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div><!---Contain--->	
	
	<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
function show_data(){
 $('.show_content').load('test');
 // $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
$(".send_msg").on('click',function(){
	var send_msg=$(this).attr('id');
	var name=$('#msg_name').val();
	var email=$('#msg_email').val();
	var phone= $('#msg_phnoe').val();
	var subject= $('#msg_subject').val();
	var message= $('#msg_message').val();
	$.ajax({
		url: "http://localhost:81/ci3/admin/dashboard/message",
		type: 'POST',
		data: {
			'name':name,
			'email':email,
			'phone':phone,
			'subject':subject,
			'message':message
		},
		success: function(msg) {
			if(msg == 'YES'){
				alert('yes');
			}else if(msg == 'No'){
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}else{
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
		}
	});
});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>

</html>