<?php $this->load->view(config_item('admin_template_dir').'head'); ?>	
<?php $this->load->view(config_item('admin_template_dir').'header'); ?>
<?php $this->load->view(config_item('admin_template_dir').'sidebar'); 
function get_supplier_name($user_id){
	$ci =& get_instance();
	$ci->db->select('*');
	$ci->db->from('sys_users');
	$ci->db->where('user_id',$user_id);
	$query = $ci->db->get();
	return $query->first_row('array');
//	return $row['username'];
}
?>
<body>
<div class="page-bar">
	<?php echo breadcrumb(); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-sm-4">
				<h3><span class="fui-document"></span>Reports</h3>
			</div><!-- /.col -->
			
		</div><!-- /.row -->
		<?php 
		if(isset($errors)){
		   echo msg($errors);
		}
		if(isset($rmsg)){
		   echo show_msg($rmsg);
		}
		//	$catFilter = catForFilter();
		?>
		<?php echo $this->session->flashdata('msg'); ?>
		<hr class="dashed margin-bottom-30">
		<div class="panel panel-default">
			<form id="search-name" class="search-frm search" accept-charset="utf-8" action="<?php echo base_admin_url().'reports/report/search_code';?>" method="get">
			<table class="table" id="product-list">
				<tr>
					<td style="border-bottom: 1px solid #ebebeb;">
						<div class="col-md-12 col-sm-12">
							
							<div class="form-group col-md-3">
								<?php echo form_label('Supplier', 'Supplier',array('class' => 'control-label')); ?>
								<select class="form-control" name="supplier">
									<?php 
									echo '<option value=""></option>';
									foreach($get_supplier as $row){
										echo '<option value="'.$row['user_id'].'" '.($this->input->get('supplier')==$row['user_id']?'selected':'').'>'.$row['username'].'</option>';
									}
									?> 
								</select>
							</div>
							<div class="form-group col-md-4">
								<?php echo form_label('Search', 'find_id',array('class' => 'control-label')); ?>
								<?php echo form_input(array('name' => 'search_code','placeholder'=>'Product code | Name', 'class' => 'form-control', 'id' => 'text',  'value' => $this->input->get('search_code') ) ); ?>											
							</div>
							<div class="form-group col-md-3">
								<label for="" class="control-label">&nbsp;</label>
								<input type="submit" class="btn btn blue form-control" value="<?php word('search'); ?>">											
							</div>
						</div>
				    </td>
				</tr>
				<tr>
					<td>
						<div class="panel panel-default">
							<table data-show-export="true" data-search="true" data-show-refresh="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-pagination="true" data-side-pagination="server" data-url="#" class="table table-striped table-bordered table-hover" id="events-table">
								<thead>
									<tr>
										<th>N</th>
										<th>Product Code</th>
										<th>Product Name</th>
										<th>Supplier</th>
										<th>Price</th>
										<th>Date</th>
										<th>Options</th>
									</tr>
								</thead>
								
								<tbody>
									<?php
									
										// var_dump($billing);
										$i=0;
										$total=0;
									if(!empty($suppliers)) {
										foreach($suppliers as $rows){
											$pro_id = $rows['product_id'];
											$product_code=$rows['product_code'];
											$product_name = $rows['product_name'];
											$supplier_id = $rows['supplier_id'];
											$price = $rows['sale_price'];
											$i++;
											$get_supplier=get_supplier_name($supplier_id)
									?>
									<tr>
										<td><?php echo $i;?></td>
										<td><?php echo $product_code;?></td>
										<td><?php echo $product_name;?></td>
										<td><?php echo $get_supplier['username'];?></a></td>
										<td><?php echo $price;?></a></td>
										<td><?php echo $rows['created_date'];?></td>
										<td>  
											<a class="btn btn-default btn-labeled fa fa-eye pull-right mar-rgt category" data-toggle="modal"  href="<?php echo  base_url().'admin/reports/report/view_pro/'.$pro_id;?>">
												Detail                              
											</a>
										</td>
									
									</tr>
									<?php
											
										}
									}else {
										?>
										<tr>
											<td colspan='7'>Not found !</td>
										</tr>
									<?php
									}  
									?>
									
								</tbody>
							</table>
						</div>
					</td>
					
				</tr>
				<tr>
					<td>
						<div class="pull-right">
							<div class="title_page">
								<?php echo $showing;?>
								
							</div>
							<?php echo $page;?> 
						</div>
					</td>
				</tr>
			</table>
			</form>
		</div>
	</div>
	</body>
			<!---Contain--->	
	<?php $this->load->view(config_item('admin_template_dir').'script'); ?>
<script>
$(document).ready(function() {		
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			//startDate: '-3d'
		});
		
});
function show_data(){
 $('.show_content').load('test');
 // $('.show_content').load('test').hide().fadeIn(4000);
}
setInterval('show_data()', 5000);
//-----------------------
$(".send_msg").on('click',function(){
	var send_msg=$(this).attr('id');
	var name=$('#msg_name').val();
	var email=$('#msg_email').val();
	var phone= $('#msg_phnoe').val();
	var subject= $('#msg_subject').val();
	var message= $('#msg_message').val();
	$.ajax({
		url: "http://localhost:81/ci3/admin/dashboard/message",
		type: 'POST',
		data: {
			'name':name,
			'email':email,
			'phone':phone,
			'subject':subject,
			'message':message
		},
		success: function(msg) {
			if(msg == 'YES'){
				alert('yes');
			}else if(msg == 'No'){
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}else{
				 $('#alert-msg').html('<div class="alert alert-danger">' + msg + '</div>');
			}
		}
	});
});
</script>
<?php $this->load->view(config_item('admin_template_dir').'footer'); ?>
</body>
</html>