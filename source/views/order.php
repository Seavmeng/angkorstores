<?php $this->load->view('includes/head');?>
<body class="page page-id-7 page-template-default woocommerce-checkout woocommerce-page woocommerce-order-received wpb-js-composer js-comp-ver-4.12 vc_responsive">
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
	<?php $this->load->view('includes/header1');?>
	<div id="content" class="site-content" tabindex="-1">
		<div class="container">
		<nav class="woocommerce-breadcrumb" ><a href="http://demo2.transvelo.in/electro">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span><a href="http://demo2.transvelo.in/electro/checkout/">Checkout</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Order Received</nav>
			<div id="primary" class="content-area">
				<main id="main" class="site-main">	
					<article id="post-7" class="post-7 page type-page status-publish hentry">
						<header class="entry-header">
						<?php
						
						?>
						<h1 class="entry-title" itemprop="name">Order Received</h1>
						</header><!-- .entry-header -->
						<div class="entry-content" itemprop="mainContentOfPage">
							<div class="woocommerce">
								<p class="woocommerce-thankyou-order-received">Thank you. Your order has been received.</p>

								<ul class="woocommerce-thankyou-order-details order_details">
									<li class="order">
										Order Number:<strong> <?php echo 'Customer&nbsp00'.$rec_order['order_number']?></strong>
									</li>
									<li class="date">
										Date: <strong><?php echo $rec_order['created']?></strong>
									</li>
								</ul>
								<div class="clear"></div>
							<h2>Order Details</h2>
							<table class="shop_table order_details">
								<thead>
									<tr>
										<th class="product-name">Product</th>
										<th class="product-total">Total</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$sub_total=0;
									foreach($rec_product_order as $data){
										
										$per_price=$data['qty']*$data['prices'];
										echo '
										<tr class="order_item">
											<td class="product-name">
												<a href="#">'.$data['product_name'].'</a> <strong class="product-quantity">&times; '.$data['qty'].'</strong>	
											</td>
											<td class="product-total">
												<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>'.$per_price.'</span>	
											</td>
										</tr>';
										$sub_total += $per_price;
										$pay_id=$data['payment_id'];
									}
									$total_ship=$rec_order['total_shipping'];
									?>
									
								</tbody>
								<tfoot>
									<tr>
										<th scope="row">Subtotal:</th>
										<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span><?php echo $sub_total;?></span></td>
									</tr>
									<tr>
										<th scope="row">Shipping:</th>
										<td>
										<?php 
										if(empty($rec_order['shipping_company'])){
											echo '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">No Shipping</span></span>';
										}else{
											echo '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>'.$total_ship.'</span>&nbsp;<small class="shipped_via">via '.$rec_order['shipping_company'].'</small>';
										}
										?>
										
										</td>
									</tr>
									<tr>
										<th scope="row">Payment Method:</th>
										<td><?php echo payment_method($pay_id);?></td>
									</tr>
									<tr>
										<th scope="row">Total:</th>
										<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span><?php echo $sub_total + $total_ship;?></span></td>
									</tr>
								</tfoot>
							</table>
							</div>
						</div><!-- .entry-content -->
							
					</article><!-- #post-## -->
				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- .col-full -->
	</div><!-- #content -->
	<?php $this->load->view('includes/contact_footer');?>
	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>
</body>
</html>