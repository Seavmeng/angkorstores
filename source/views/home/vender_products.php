<?php $this->load->view('home/vendor_pages/header'); ?>
								<!-- Content -->
								
								<div class="tabs-block col-lg-12 h-cover">
									<div class="col-md-12" style="padding: 0 42px 15px 35px;">
										<div class="products-carousel-tabs" style="margin-top: 50px;">
											<ul class="nav nav-inline">
												<li class="nav-item">
													<a class="nav-link active" href="#tab-products-1" data-toggle="tab products-carousel-tabs">Featured Products</a></li>
											</ul>
										</div>
									</div>
								<div class="products-carousel-tabs">
									<div class="tab-content" style="margin: 0 15px 27px 15px; padding: 20px;background: #FFFFFF;">
									<div class="tab-pane active" id="tab-products-1" role="tabpanel">
										<div class="woocommerce columns-3">
											<ul class="products columns-4">
												<?php
													foreach($Featured_homepage as $row){
												?>
												<li class="product">
													<div class="product-outer">
														<div class="product-inner">
															<a href="<?php echo base_url() . 'detail/'. $row['permalink']; ?>" target="_blank">
																<h3><?php echo $row['product_name'];?></h3>
																<div class="product-thumbnail">
																	<!--img src="<?php echo base_url();?>images/blank.gif" data-echo="<?php echo base_url();?>images/products/6.jpg" class="img-responsive" alt=""-->
																	<img class="vendor_img" src="<?php echo base_url().'images/products/'.$row['feature'];?>" class="img-responsive" alt="">
																</div>
															</a>

															<div class="price-add-to-cart">
																<span class="price">
																	<span class="electro-price">
																		<ins><span class="amount"> </span></ins>
																		<?php
																			if($row['sale_price']>0)
																			{
																			?>
																				<span class="amount"><?php echo currency('sign').$row['sale_price']; ?></span>
																			<?php
																			}
																		?>
																	</span>
																</span>
																<?php
																echo form_open(base_url()."cart/addcart");
																	echo form_hidden('product_id', $row['product_id']);
																	echo form_hidden('product_name', $row['product_name']);
																	echo form_hidden('sale_price', $row['sale_price']);
																	echo form_hidden('quantity', 1);
																		$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
																	//echo form_submit($btn);
																	//echo form_button($btn['button']);
																//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
																echo form_close();
																?>
																<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().$user_detail['page_name']; ?>/contact" class="fa fa-phone fa-1x vendor-btn"></a>
															</div><!-- /.price-add-to-cart -->

															<div class="hover-area">
																<div class="action-buttons">
																	<a href="<?php echo base_url().'ad_wishlist/'.$row['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																	<a href="<?php echo base_url().'ad_compare/'.$row['product_id'];?>" class="add-to-compare-link"> Compare</a>
																</div>
															</div>
														</div><!-- /.product-inner -->
													</div><!-- /.product-outer -->
												</li><!-- /.products -->
												<?php
												}
												?>
												<div class="vender col-xs-12" style="margin-bottom: -50px; padding-top: 25px;">
										 			<center><?php echo $page; ?></center>
												</div> 

										</div>
											</ul>
											
									</div>
								 </div><!--End tab-content-->
								</div>
							</div><!-- /.tabs-block -->
<?php $this->load->view('home/vendor_pages/footer'); ?>
								



