<?php $this->load->view('includes/head');?>

<body class="left-sidebar">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/header1');?>
		<div id="content" class="site-content" tabindex="-1">
			<div class="container">

				<nav class="woocommerce-breadcrumb" >
					<a href="<?php echo base_url(); ?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span><span class="woocommerce-breadcrumb"><a href="<?php echo base_url().'category/'.$this->uri->segment(2);?>"><?php echo $cat_title['category_name']; ?></a></span>
				</nav>

				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						
						<header class="page-header">
						
							<h1 class="page-title"><?php echo $cat_title['category_name'];?></h1>
							<p class="woocommerce-result-count"><?php echo $showing;?></p>
						</header>
						
						<div class="shop-control-bar">
							<ul class="shop-view-switcher nav nav-tabs" role="tablist">
								<li class="nav-item"><a class="nav-link active" data-toggle="tab" title="Grid View" href="#grid"><i class="fa fa-th"></i></a></li>
								<li class="nav-item"><a class="nav-link " data-toggle="tab" title="Grid Extended View" href="#grid-extended"><i class="fa fa-align-justify"></i></a></li>
								<li class="nav-item"><a class="nav-link " data-toggle="tab" title="List View" href="#list-view"><i class="fa fa-list"></i></a></li>
								<li class="nav-item"><a class="nav-link " data-toggle="tab" title="List View Small" href="#list-view-small"><i class="fa fa-th-list"></i></a></li>
							</ul>
							<nav class="electro-advanced-pagination">
								<?php echo $page;?>
							</nav>
						</div>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="grid" aria-expanded="true">
								<ul class="products columns-3">
								<?php 
								$count = 0;
								foreach($product as $item): 
								
								if($count % 3 === 0){
									$first = "first";
									} 
									
									elseif($count % 3 ===2){
										$first="last";
									}
									else{
										$first="";
									}
								?>
										<li class="product <?php echo $first;?>">
											<div class="product-outer">
												<div class="product-inner">
													<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>">
														<h3><?php echo $item['product_name'];?></h3>
														<div class="product-thumbnail">

															<!--img data-echo="assets/images/products/1.jpg" src="assets/images/blank.gif" alt=""-->
															<img data-echo="<?php echo base_url()."images/products/".$item['feature'];?>" src="<?php echo base_url()."assets/images/blank.gif";?>" alt="">
														</div>
													</a>

													<div class="price-add-to-cart">
														<span class="price">
															<span class="electro-price">
															<?php if($item['have_discount'] == 1){ ?>
																<ins><span class="amount"><?php echo currency("sign").$item['after_discount'];?></span></ins>
																<del><span class="amount"><?php echo currency("sign").$item['sale_price'];?></span></del>
															<?php }else{ ?>
																<ins><span class="amount"><?php echo currency("sign").$item['sale_price'];?></span></ins>
															<?php } ?>
															</span>
														</span>
														<?php
														echo form_open(base_url()."cart/addcart");
															echo form_hidden('product_id', $item['product_id']);
															echo form_hidden('product_name', $item['product_name']);
															echo form_hidden('sale_price', $item['sale_price']);
															echo form_hidden('quantity', 1);
																$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
															if($item['group_id'] != 2){
																echo form_button($btn['button']);
															}
														//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
														echo form_close();
														?>
													</div><!-- /.price-add-to-cart -->

													<div class="hover-area">
														<div class="action-buttons">

															<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																	<a href="<?php echo base_url().'ad_compare/'.$item['product_id'];?>" class="add-to-compare-link"> Compare</a>
														</div>
													</div>
												</div><!-- /.product-inner -->
											</div><!-- /.product-outer -->
										</li>
										<?php 
										$count++;
										endforeach; ?>
								</ul>
							</div>					
							<div role="tabpanel" class="tab-pane" id="grid-extended" aria-expanded="true">
								<ul class="products columns-3">
								<?php 
								$count=0;
								foreach($product as $item):
								if($count % 3 === 0){
									$first = "first";
								}
								elseif($count % 3 === 2){
									$first = "last";
								}
								else{
									$first = "";
								}
								?>
														<li class="product <?php echo $first;?>">
											<div class="product-outer">
												<div class="product-inner">
													
													<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>">
														<h3><?php echo $item['product_name'];?></h3>
														<div class="product-thumbnail">
															<img class="wp-post-image" data-echo="<?php echo base_url(). "images/products/". $item['feature'];?>" src="<?php echo base_url()."assets/images/blank.gif";?>" alt="">
														</div>

														

														<div class="product-short-description">
														<?php echo $item['description']; ?>
															<!--ul>
																<li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
																<li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
																<li><span class="a-list-item">Flash storage</span></li>
																<li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
																<li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
															</ul-->
														</div>

														<div class="product-sku">SKU: <?php if ($item['product_code']==''){echo $item['product_id'];}else{echo $item['product_code'];}?></div>
													</a>
													<div class="price-add-to-cart">
														<span class="price">
															<span class="electro-price">
																<ins><span class="amount"><?php echo currency('sign').$item['sale_price'];?></span></ins>
																
															</span>
														</span>
														<?php
														echo form_open(base_url()."cart/addcart");
															echo form_hidden('product_id', $item['product_id']);
															echo form_hidden('product_name', $item['product_name']);
															echo form_hidden('sale_price', $item['sale_price']);
															echo form_hidden('quantity', 1);
																$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
															if($item['group_id'] != 2){
																echo form_button($btn['button']);
															}
														//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
														echo form_close();
														?>
													</div><!-- /.price-add-to-cart -->
													<div class="hover-area">
														<div class="action-buttons">

															<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																	<a href="<?php echo base_url().'ad_compare/'.$item['product_id'];?>" class="add-to-compare-link"> Compare</a>
														</div>
													</div>
												</div><!-- /.product-inner -->
											</div><!-- /.product-outer -->
										</li>
										<?php 
										$count++;
										endforeach; ?>
										</ul>
							</div>					
							<div role="tabpanel" class="tab-pane" id="list-view" aria-expanded="true">

								<ul class="products columns-3">
								<?php foreach($product as $item): ?> 
												<li class="product list-view">
											<div class="media">
												<div class="media-left">
													<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>">
														<img class="wp-post-image" data-echo="<?php echo base_url() . "images/products/" . $item['feature'];?> " src="<?php echo base_url()."assets/images/blank.gif";?>" alt="">
													</a>
												</div>
												<div class="media-body media-middle">
													<div class="row">
														<div class="col-xs-12">
															<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>"><h3><?php echo $item['product_name'];?></h3>
																
																<div class="product-short-description">
																	<?php echo $item['description']; ?>
																</div>
															</a>
														</div>
														<div class="col-xs-12">

															<div class="availability in-stock">
																Availablity: <span>In stock</span>
															</div>


															<span class="price"><span class="electro-price"><span class="amount"><?php echo currency('sign').$item['sale_price'];?></span></span></span>
															<?php
														echo form_open(base_url()."cart/addcart");
															echo form_hidden('product_id', $item['product_id']);
															echo form_hidden('product_name', $item['product_name']);
															echo form_hidden('sale_price', $item['sale_price']);
															echo form_hidden('quantity', 1);
																$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button product_type_simple add_to_cart_button ajax_add_to_cart',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
															if($item['group_id'] != 2){
																echo form_button($btn['button']);
															}
														//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
														echo form_close();
														?>
															
															<div class="hover-area">
																<div class="action-buttons">
																	<div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
																		<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

																		<div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
																			<span class="feedback">Product added!</span>
																			<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																	
																		</div>

																		<div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
																			<span class="feedback">The product is already in the wishlist!</span>
																			<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																		</div>

																		<div style="clear:both"></div>
																		<div class="yith-wcwl-wishlistaddresponse"></div>

																	</div>
																	<div class="clear"></div>
																	<a href="<?php echo base_url().'ad_compare/'.$item['product_id'];?>" class="add-to-compare-link"> Compare</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
								<?php endforeach; ?>
								
										</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="list-view-small" aria-expanded="true">
								<ul class="products columns-3">
								<?php foreach($product as $item): ?>
												<li class="product list-view list-view-small">
											<div class="media">
												<div class="media-left">
													<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>">
														<img class="wp-post-image" data-echo="<?php echo base_url() . "images/products/" . $item['feature'];?>" src="<?php echo base_url()."assets/images/blank.gif";?>" alt="">
													</a>
												</div>
												<div class="media-body media-middle">
													<div class="row">
														<div class="col-xs-12">
															<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>"><h3><?php echo $item['product_name'];?></h3>
																
															</a><div class="product-short-description">
																	<?php echo $item['description']; ?>
																</div>
																
														</div>
														<div class="col-xs-12">
															<div class="price-add-to-cart">
																<span class="price"><span class="electro-price"><span class="amount"><?php echo currency('sign').$item['sale_price'];?></span></span></span>
																<?php
														echo form_open(base_url()."cart/addcart");
															echo form_hidden('product_id', $item['product_id']);
															echo form_hidden('product_name', $item['product_name']);
															echo form_hidden('sale_price', $item['sale_price']);
															echo form_hidden('quantity', 1);
																$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
															if($item['group_id'] != 2){
																echo form_button($btn['button']);
															}
														//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
														echo form_close();
														?>
															</div><!-- /.price-add-to-cart -->
															<div class="hover-area">
																<div class="action-buttons">

																	<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																	<a href="<?php echo base_url().'ad_compare/'.$item['product_id'];?>" class="add-to-compare-link"> Compare</a>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</li>
										<?php endforeach;?> 
										</ul>
							</div>
						</div>
						<div class="shop-control-bar-bottom">
							<p class="woocommerce-result-count"><?php echo $showing;?></p>
							<div class="pull-right">
								<?php echo $page;?>
							</div>
						</div>

					</main><!-- #main -->
				</div><!-- #primary -->

				<div id="sidebar" class="sidebar" role="complementary">
					<aside class="widget woocommerce widget_product_categories electro_widget_product_categories">
						<ul class="category-single">
							<li class="product_cat">
								<ul class="show-all-cat">
									<li class="product_cat"><span class="show-all-cat-dropdown">All Categories</span>
										<ul class="parent" style="display:block !important;">
											<?php
												$allCats = sidebarCategories();
												$allSubCats = sidebarSubCategories();
												//foreach($recommend as $item):
													foreach($allCats as $eachCat ):
													?>
													<li class="cat-item"><a href="<?php echo base_url().'category/'.$eachCat['category_id']; ?>"><?php echo $eachCat['category_name']; ?></a> <span class="count"><?php echo '('.countItem($eachCat['category_id']).')'; ?></span>
													<?php
														foreach($allSubCats as $eachSubCat):
												?>
													
												<?php 
													if($eachSubCat['parent_id'] == $eachCat['category_id']) {
												?>
														<ul class='children' >
												
														<li class="cat-item"><a href="<?php echo base_url().'category/'.$eachCat['category_id']; ?>"><?php echo $eachSubCat['category_name']; ?></a> <span class="count"><?php echo '('.countItem($eachSubCat['category_id']).')'; ?></span><span class='children'></span></li>
													
														</ul>
												<?php 
													}
														endforeach;
														echo '</li>';
													endforeach;
												//endforeach;?>
										</ul>
									</li>
								</ul>
								<!--ul>
									<li class="cat-item current-cat"><a href="product-category.html">Laptops &amp; Computers</a> <span class="count">(13)</span>
										<ul class='children'>
											<li class="cat-item"><a href="product-category.html">Laptops</a> <span class="count">(6)</span></li>
											<li class="cat-item"><a href="product-category.html">Ultrabooks</a> <span class="count">(1)</span></li>
											<li class="cat-item"><a href="product-category.html">Computers</a> <span class="count">(0)</span></li>
											<li class="cat-item"><a href="product-category.html">Mac Computers</a> <span class="count">(1)</span></li>
											<li class="cat-item"><a href="product-category.html">All in One</a> <span class="count">(1)</span></li>
											<li class="cat-item"><a href="product-category.html">Servers</a> <span class="count">(1)</span></li>
											<li class="cat-item"><a href="product-category.html">Peripherals</a> <span class="count">(1)</span></li>
											<li class="cat-item"><a href="product-category.html">Gaming</a> <span class="count">(1)</span></li>
											<li class="cat-item"><a href="product-category.html">Accessories</a> <span class="count">(2)</span></li>
										</ul>
									</li>
								</ul-->
							</li>
						</ul>
					</aside>
					<!--aside class="widget widget_electro_products_filter">
			<h3 class="widget-title">Filters</h3>
			<aside class="widget woocommerce widget_layered_nav">
				<h3 class="widget-title">Brands</h3>
				<ul>
					<li style=""><a href="#">Apple</a> <span class="count">(4)</span></li>
					<li style=""><a href="#">Gionee</a> <span class="count">(2)</span></li>
					<li style=""><a href="#">HTC</a> <span class="count">(2)</span></li>
					<li style=""><a href="#">LG</a> <span class="count">(2)</span></li>
					<li style=""><a href="#">Micromax</a> <span class="count">(1)</span></li>
				</ul>
				<p class="maxlist-more"><a href="#">+ Show more</a></p>
			</aside>
			<aside class="widget woocommerce widget_layered_nav">
				<h3 class="widget-title">Color</h3>
				<ul>
					<li style=""><a href="#">Black</a> <span class="count">(4)</span></li>
					<li style=""><a href="#">Black Leather</a> <span class="count">(2)</span></li>
					<li style=""><a href="#">Turquoise</a> <span class="count">(2)</span></li>
					<li style=""><a href="#">White</a> <span class="count">(4)</span></li>
					<li style=""><a href="#">Gold</a> <span class="count">(4)</span></li>
				</ul>
				<p class="maxlist-more"><a href="#">+ Show more</a></p>
			</aside>
			<aside class="widget woocommerce widget_price_filter">
				<h3 class="widget-title">Price</h3>
				<form action="#">
					<div class="price_slider_wrapper">
						<div style="" class="price_slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
							<div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div>
							<span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></span>
							<span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;"></span>
						</div>
						<div class="price_slider_amount">
							<a href="#" class="button">Filter</a>
							<div style="" class="price_label">Price: <span class="from">$428</span> &mdash; <span class="to">$3485</span></div>
							<div class="clear"></div>
						</div>
					</div>
				</form>
			</aside>
		</aside-->
					<!--aside class="widget widget_text">
			<div class="textwidget">
				<a href="#">
				<img src="assets/images/banner/ad-banner-sidebar.jpg" alt="Banner"></a>
			</div>
		</aside-->
					<aside class="widget widget_products">
			<h3 class="widget-title">Latest Products</h3>
			<ul class="product_list_widget">
			<?php foreach($latestPro as $eachPro) : ?>
				<li>
					<a href="<?php echo base_url(); ?>detail/<?php echo $eachPro['permalink'];?>" title="<?php echo $eachPro['product_name']; ?>">
						<img width="180" height="180" src="<?php echo base_url(); ?>images/products/<?php echo $eachPro['feature'];?>" alt="" class="wp-post-image"/><span class="product-title"><?php echo $eachPro['product_name']; ?></span>
					</a>
					<span class="electro-price"><ins><span class="amount"><?php echo currency('sign').$eachPro['sale_price'];?></span></ins> <!--del><span class="amount">&#36;2,299.00</span></del--></span>
				</li>
			<?php endforeach; ?>
			</ul>
		</aside>
				</div>

			</div><!-- .container -->
		</div><!-- #content -->
	<?php $this->load->view('includes/contact_footer');?>
	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>
</body>
</html>
