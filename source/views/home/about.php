<?php $this->load->view('includes/head');?>
<body class="about full-width page page-template-default">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/header1');?>
		<div id="content" class="site-content" tabindex="-1">
			<div class="container">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<article class="has-post-thumbnail hentry">
				

							<div class="entry-content">
								<div class="row inner-sm">
									<div class="text-boxes col-sm-7">
										<div class="row inner-bottom-xs">
										
												<div class="wpb_wrapper about-justify">
													<h3>Welcome to Angkorstores.com!</h3>
													<p>Angkorstores.com is the E-Commerce website opened worldwide and based in Cambodia. The project is invested by Mom Varin Investment (<a href='http://www.varinholdings.com' target='_blank'>www.varinholdings.com</a>), which has its affiliated companies – V Japan Tech Inter (<a href='http://www.vjpti.com' target='_blank'>www.vjpti.com</a>) and Asia-Pacific Fredfort International School (<a href='http://www.afisedu.com' target='_blank'>www.afisedu.com</a>).  V Japan Tech Inter is the leading company of LPG and fuel equipment and services while Asia-Pacific Fredfort International School is an IB PYP Curriculum School in Cambodia.

</p>
<p>

Owned by Mom Varin Investment, Angkorstores.com represents the Cambodian symbol of online shopping and marketing which allows accesses of local and international suppliers to log in and create account for uploading their product information, advertisements and promotions. It is a bridging point to link between the suppliers and customers by 13 markets, which serve all as needed by businesses, industries, works, schools and households. It covers B2C and B2B services. We guarantee the processes and transactions related to the purchase orders, payments and deliveries are fully secured by our great logistics networks with DHL, FedEx, EMS and some outstanding local and international banks.

The website’s basic concept is about the intention to promote the local and international products to domestic and global markets. It is typically trying to transform the way the products and services are marketed, sold and operated locally and internationally to save time and money, to provide rich choices of various products and features and to aid in better business and individual decisions. We build such an online infrastructure to help merchants, wholesalers, retailers and suppliers to leverage the power of the Internet to engage with their users and customers.

We commit to the purchase order and E-payment with high security for all processes and transactions. We accommodate the responsive logistics with scheduling and tracking route from the origin to the destination. We provide door to door delivery by our agents and carriers with caring treatment. Our Customer Service wakes you up within 24-hour standby for the solutions and on-time response. We surely envisage the change of standard of living and the growths of business and daily life’s needs, and of course everybody should be busy. So, let us be your assistant on shopping and delivery to your place.

Angkorstores.com Makes Your Life Easiest!!


</p>

												</div>
											
										</div>

										<!--div class="row inner-bottom-xs">
											<div class="col-sm-12">
												<div class="wpb_wrapper">
													<h3 class="highlight">Our Customer Service or address</h3>
													<p>Address: #40, St. 348, Sangkat Toul Svay Prey 1, Khan Camkar Morn, Phnom Penh, Cambodia 
													Tel: +855 23 5555 008/ +855 23 969 278
													Email: info@angkorstores.com
													Website: www.angkorstores.com
													</p>

												</div>
											</div>
										</div-->
									</div>


									<div class="col-sm-5">
										
												<div class="col-sm-12" style="margin-bottom: 20px">
														
																	<img class="service-icon" src='<?php echo base_url(); ?>assets/images/payment.png'/>
																	
																	<h6 class="service-title">Purchase Order & E-Payment</h6>
																
																	<p>With high security for all processes and transactions.</p>
	
														
												</div>

												<div class="col-sm-12" style="margin-bottom: 20px">
																
																	<img class="service-icon" src='<?php echo base_url(); ?>assets/images/logistics.png'/>
																	<h6 class="service-title">Responsive Logistics</h6>
																		
																	<p>Scheduling and tracking route from the origin to destination.</p>
														
												</div>

															<div class="col-sm-12" style="margin-bottom: 20px">
																
																<img class="service-icon" src='<?php echo base_url(); ?>assets/images/delivery.png'/>
																	
																		<h6 class="service-title">Delivery</h6>
																		<p>Door to door delivery by our agent and carrier with caring & treatment.</p>
															</div>

															
														
														<div class="col-sm-12" style="margin-bottom: 20px">
															
																
																	<img class="service-icon" src='<?php echo base_url(); ?>assets/images/customer-service.png'/>
																	
																		<h6 class="service-title">Customer Service</h6>
																		<p>24-hour standby for solutions and on-time responses.</p>
															
														</div>
										</div>
													
								</div>
							</div><!-- .entry-content -->

						</article><!-- #post-## -->
					</main><!-- #main -->
				</div><!-- #primary -->
			</div><!-- .col-full -->
			</div><!-- #content -->
<?php $this->load->view('includes/contact_footer');?>
	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>
</body>
</html>
