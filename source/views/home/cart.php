<?php $this->load->view('includes/head');
$count_products=count($this->cart->contents());
?>
<body class="page home page-template-default">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/header1');?>
		<div id="content" class="site-content" tabindex="-1">
			<div class="container">
				<nav class="woocommerce-breadcrumb"><a href="<?php echo base_url(); ?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Cart</nav>
				<?php if(isset($rmsg)){
					echo show_msg($rmsg,'success');}
				?>
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<article class="page type-page status-publish hentry">
							<header class="entry-header"><h1 itemprop="name" class="entry-title">Cart</h1></header><!-- .entry-header -->
							<form action="<?php echo base_url(); ?>cart/update_addtocart" method="post" name="cart-item">
								<table class="shop_table shop_table_responsive cart">
									<thead>
										<tr>
											<th class="product-remove">&nbsp;</th>
											<th class="product-thumbnail">&nbsp;</th>
											<th class="product-name">Product</th>
											<th class="product-price">Price</th>
                                                                                        <th class="product-price">Discount</th>
											<th class="product-quantity">Quantity</th>
											<th class="product-subtotal">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if(has_logged()){
											$i=0;
											$total=0;
											$weight=0;
											foreach($cart as $item){
												$i++;
												$product_id=$item['product_id'];
												$row=get_pro_field($product_id);
												$price=$row['sale_price'] * $item['qty'];
												//----varian-
												$color_id=$item['color'];
												$size_id=$item['size'];
												$varian=attributes($product_id,$color_id);
												if($color_id){
													$image=$varian['image'];
												}else{
													$image=$row['feature'];
												}
												$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
												$price=number_format((float)$items_price['price'], 2, '.', '');
												$item_price=number_format((float)$items_price['price'], 2, '.', '');
											?>
												<tr class="cart_item">
													<td class="product-remove">
														<a title="Remove this item" class="remove" href="<?php echo base_url().'cart/remove_cart/'.$item['cart_id'];?>">×</a>
													</td>
													<td class="product-thumbnail">
														<a href="<?php echo base_url().'detail/'.$row['permalink'];?>"><img width="180" height="180" alt="LaptopYoga" class="wp-post-image" src="<?php echo base_url().'images/products/'.$image;?>"></a>
													</td>
													<td data-title="Product" class="product-name">
														<a href="<?php echo base_url().'detail/'.$row['permalink'];?>"><?php echo $row['product_name'];?></a>
														<span>
														 <?php
															echo '<p><br />';
															if($color_id){
																echo '<strong>Color: </strong>'.$varian['attr_name'].'<br />';
															}
															if($size_id){
																$size=get_attr_name($size_id);
																echo '<strong>Size: </strong>'.$size['attr_name'].'<br />';
															}
															?>
														</span>
													</td>
													<td data-title="Price" class="product-price">
														<span class="amount"><?php echo $items_price['original_price'];?></span>
													</td>
													<td data-title="Price" class="product-price">
														<span class="amount"><?php echo $items_price['discount_amount'].'%';?></span>
													</td>

													<td data-title="Quantity" class="product-quantity">
														<div class="quantity buttons_added">
															<input type="button" class="qtyminus minus" value="-">
															<label>Quantity:</label>
															<input type="number" size="4" class="num_only input-text cartqty text" title="Qty" value="<?php echo $item['qty'];?>" name="custom[]" max="30" min="0" step="1">
															<input type="hidden" value="<?php echo $product_id; ?>" name="product_id[]">
															<input type="button" class="qtyplus plus" value="+">
														</div>
													</td>
													<td data-title="Total" class="product-subtotal">
														<span class="amount"><?php echo currency('sign').$price;?></span>
													</td>
												</tr>
											<?php
												$total +=$price;
											}
										}else{
											if ($cart = $this->cart->contents()):
												$i=0;
												$total=0;
												$weight=0;
												foreach ($cart as $item):
												$i++;
												$product_id=$item['id'];
												$row=get_pro_field($product_id);
												echo form_hidden('cart[' . $item['id'] . '][qty]', $item['qty']);
												//----varian-
												$color_id=$item['options']['color'];
												$size_id=$item['options']['size'];
												$varian=attributes($product_id,$color_id);
												if($color_id){
													$image=$varian['image'];
												}else{
													$image=$row['feature'];
												}
												$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
												$price=number_format((float)$items_price['price'], 2, '.', '');
												$item_price=number_format((float)$items_price['price'], 2, '.', '');
												?>
												<tr class="cart_item">
													<td class="product-remove">
														<a data-product_sku="5487FB8/26" data-product_id="<?php echo $product_id;?>" title="Remove this item" class="remove" href="<?php echo base_url().'cart/remove_cart/'.$item['rowid'];?>">×</a>
													</td>
													<td class="product-thumbnail">
														<a href="<?php echo base_url().'detail/'.$row['permalink']?>"><img width="120" height="120" alt="LaptopYoga" class="wp-post-image" src="<?php echo base_url().'images/products/'.$image;?>"></a>
													</td>
													<td data-title="Product" class="product-name">
														<a href="<?php echo base_url().'detail/'.$row['permalink'];?>"><?php echo $item['name']?></a>
														<span>
														 <?php
															echo '<p><br />';
															if($color_id){
																echo '<strong>Color: </strong>'.$varian['attr_name'].'<br />';
															}
															if($size_id){
																$size=get_attr_name($size_id);
																echo '<strong>Size: </strong>'.$size['attr_name'].'<br />';
															} 
															/*
															if ($this->cart->has_options($item['rowid']) == TRUE): 
																foreach ($this->cart->product_options($item['rowid']) as $option_name => $option_value):
																	if($option_value){
																		echo '<strong>'.$option_name.':</strong>'.$option_value.'<br />';
																	}
																endforeach;
															endif;
															echo '</p>';
															*/?>

														</span>
													</td>
													<td data-title="Price" class="product-price">
														<span class="amount"><?php echo $items_price['original_price'];?></span>
													</td>
													<td data-title="Price" class="product-price">
														<span class="amount"><?php echo $items_price['discount_amount'].'%';?></span>
													</td>
													<td data-title="Quantity" class="product-quantity">
														<div class="quantity buttons_added">
															<input type="button" class="qtyminus minus" value="-">
															<label>Quantity:</label>
															<input type="number" size="4" class="num_only input-text cartqty text" title="Qty" value="<?php echo $item['qty'];?>" name="custom[]" max="30" min="0" step="1">
															<input type="button" class="qtyplus plus" value="+">
														</div>
													</td>
													<td data-title="Total" class="product-subtotal">
														<span class="amount"><?php echo currency('sign').$price;?></span>
													</td>
												</tr>
												<?php 
													$total +=$price;
												endforeach; 
											endif;
										}
										?>
										<tr>
											<td class="actions" colspan="6">
												<!--
												<div class="coupon">
													<label for="coupon_code">Coupon:</label> <input type="text" placeholder="Coupon code" value="" id="coupon_code" class="input-text" name="coupon_code"> <input type="submit" value="Apply Coupon" name="apply_coupon" class="button">
												</div>-->

												<input type="submit" value="Update Cart" name="update_cart" class="button">

												<div class="wc-proceed-to-checkout">

													<a class="checkout-button button alt wc-forward" href="<?php echo base_url().'checkout';?>">Proceed to Checkout</a>
												</div>

											</td>
										</tr>
									</tbody>
									</table>
							</form>
							<div class="cart-collaterals">
								<div class="cart_totals ">
									<h2>Cart Totals</h2>
									<table class="shop_table shop_table_responsive">
										<tbody>
											<tr class="cart-subtotal">
												<th>Subtotal</th>
												<td data-title="Subtotal"><span class="amount">
												<?php 
												if(isset($total)){
													echo currency('sign').$total;
												}else{
													echo 0;
												}?></span></td>
											</tr>
											<tr class="order-total">
												<th>Total</th>
												<td data-title="Total"><strong><span class="amount">
												<?php 
												if(isset($total)){
													echo currency('sign').$total;
												}else{ echo 0;	}?>
												</span></strong> </td>
											</tr>
										</tbody>
									</table>
									<div class="wc-proceed-to-checkout">
										<a class="checkout-button button alt wc-forward" href="checkout.html">Proceed to Checkout</a>
									</div>
								</div>
							</div>
						</article>
					</main><!-- #main -->
				</div><!-- #primary -->
			</div><!-- .container -->
			</div><!-- #content -->

	<?php $this->load->view('includes/contact_footer');?>
	</div><!-- #page -->

	<?php $this->load->view('includes/footer');?>

</body>

</html>
