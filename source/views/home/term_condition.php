<?php $this->load->view('includes/head');?>
<body class="about full-width page page-template-default">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/header1');?>
		<div id="content" class="site-content" tabindex="-1">
			<div class="container">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<article class="has-post-thumbnail hentry">
							<div class="entry-content">
								<div class="row inner-sm">
									<div class="text-boxes col-sm-12">
										<div class="row inner-bottom-xs">
												<div class="wpb_wrapper about-justify">
													<h3 class="highlight"><center>TERM OF SERVICES</center></h3>

<p><strong>&nbsp;</strong></p>
<p><span style="text-decoration: underline;">&nbsp;<span style="color: #000000; text-decoration: underline;"><strong>JOIN (REGISTRATION)</strong></span></span></p>
<p>Registration at our website is FREE, you can become a member of Angkorstores website without any admission fee or membership charges. Everyone can use services of Angkorstores website, whether it is a member or not.</p>
<p><strong>1. Instructions for sign up</strong>&nbsp;</p>
<p>By following the instructions below, you can create your free membership of Angkorstores.com</p>
<p>Click on "My Account" button at the front page</p>
<p>&nbsp; 1.1. Click &ldquo;Register Now&rdquo;.</p>
<p>&nbsp;&nbsp; &nbsp;1.2. Enter all required information.</p>
<p>&nbsp; 1.3. Registration complete.</p>
<p>* In case if you forget your ID or PASSWORD, please contact us through e-mail.</p>
<p><strong>2. How to edit personal information</strong></p>
<p>First, you need to log in to edit your information. Then, move to "My Account" page and click &ldquo;Account Information&rdquo;. Password, name, phone number, address and zip code can be modified.</p>
<p><span style="text-decoration: underline;"><strong>SAFETY MEASURES</strong></span></p>
<p>To protect our customers&rsquo; private information, we have been always planning some strict safety measures. For more detailed information, please take a look at the following [Privacy Policy].</p>
<p><strong>2.1. Security</strong></p>
<p>Customer&rsquo;s privacy and shopping information are encrypted with SSL secret code and processed by Bank. So, we cannot see your Credit card number or internet banking user and password.</p>
<p><strong>2.2. Minimum requirements for PC</strong></p>
<p>Angkorstores website is compatible with Window Internet Explorer version 7.0, Firefox 38.0.5, Safari version 7 or latest version.</p>
<p><strong>3-BUY</strong></p>
<p><strong>A. Find a desired product by categories or by search engine at our website.</strong></p>
<p>3.1&nbsp; Click on the name or on the image of the product, and add it to the shopping cart (Click [Add to Cart] button).</p>
<p>3.2&nbsp; If you want to buy more than one product, click [Update Cart] button or close the pop-up window that comes out every time you add something to your Cart.</p>
<p>3.3&nbsp; Check your product list you are willing to purchase in the [Shopping Cart], and click [Proceed to Checkout] button if you are ready to place an order.</p>
<p>3.4&nbsp; 2<sup>nd</sup>&nbsp;Step of placing an order will be [Billing Information] on the page you have to input address of payment</p>
<p>3.5&nbsp; 3<sup>rd</sup>&nbsp;Step will be [Shipping Information] on the page you have to select address items will be shipped to</p>
<p>3.6&nbsp; 4<sup>th</sup>&nbsp;Step will be [Shipping Method] on the page you have to select method of delivery</p>
<p>3.7&nbsp; 5<sup>th</sup>&nbsp;Step is [Payment Information] where you are suggested to choose a payment method for overall cost:</p>
<p>a. Visa Card</p>
<p>b. MasterCard Card</p>
<p>c. Wing</p>
<p>3.8&nbsp; For Payment with Credit and Debit cards and Internet banking, which will redirect you to Canadia, ACLEDA Bank and Wing integrated system. Follow option and instruction to make payment.</p>
<p>3.9&nbsp; After order is complete, order confirmation alert will be automatically sent to your e-mail address.</p>
<p>*If the e-mail doesn&rsquo;t arrive in a few minutes, it might be considered as a spam mail. So please check your spam mail folder.</p>
<p><strong>B. After the order shipped out, the e-mail alert will be sent to customer&rsquo;s address.</strong></p>
<p>* All goods are shipped after your payment is confirmed.</p>
<p>* We don&rsquo;t take an order through phone or e-mail. So, please note this in advance.</p>
<p><strong>4-PAYMENT METHOD</strong></p>
<p>We use a Canadia/ACELDA Bank and Wing integrated system for payments. Bank offerings worldwide: VISA / MasterCard online payment system has advanced security to prevent e-commerce frauds, therefore system is secure and successful online transfer system.</p>
<p>NOTE: As we use Canadia/ACLEDA Bank/Wing Standard Integrated system, we do not have any access to credit card information of our clients. All credit card information of the income payments are safely encrypted by the Bank.</p>
<p><strong>5-HOW TO EDIT AND CANCEL ORDER INFORMATION</strong></p>
<p>You can contact us through e-mail or phone hotline</p>
<p><strong>5.1. CANCEL order (Before shipping)</strong></p>
<p>Please contact us through e-mail or phone hotline to cancel an order. If your product(s) are not shipped yet, you can be refunded immediately/within 3 working days after the confirmation from the bank.</p>
<p><strong>&nbsp;5.2. CANCEL order (After shipping)</strong></p>
<p>We consider any changes of your order after shipping as exchange and return requests. Shipping cost for return can be added, so please note this in advance</p>
<p><strong>6-RETURN &amp; EXCHANGE POLICY</strong></p>
<p>We want you to be totally satisfied with your order; however, we understand if a product doesn&rsquo;t meet your expectations. We want you to be sure that we do our best to make the return as painless as possible. Needless to say, we cannot accept back products, which are overly used or purchased not from Angkorstores. To return items to our company, please follow these simple steps:</p>
<p><strong>A. Return &amp; Exchange policy</strong></p>
<p>We will always do our best to ensure that your product arrives directly to you and in perfect condition. Unfortunately shipping services cannot provide 100% perfect deliveries. Please understand that some uncertainties, mistakes and special circumstances could occur, so we want you to trust us and we will do our best to avoid or resolve them for your best.</p>
<ul>
<li>A request for refund and exchange should be&nbsp;made within 3 days after the delivery date.&nbsp;If 3 days already passed, Request can be refused. The refund and exchange should made as following reasons:</li>
</ul>
<p>1) Customer&rsquo;s request: Wrong or Missing Item: Refund and exchange is possible</p>
<p>2) On our side: Lost or Damaged item: Please contact us by our Hotline or Email.&nbsp;Refund and exchange will be made depending on each case.</p>
<p>3) Delivery company's fault : Compensated by the delivery company. All the missing items or replacements for damaged items will be re-sent to you via Standard Delivery only. Please note this in advance.</p>
<ul>
<li>After your order is shipped, some minor deformation or damage of the contents might be caused by the impact of Customs procedures and Delivery process such as Small scratches or package deformation, etc. In this case, the refund or exchange is not available as long as the contents are not usable.</li>
<li>If your product is defective, we will proceed with the return process after checking the picture and conditions of the product you had received.&nbsp;Please do not send the products until we review your request.</li>
<li>The process of refund and replacement of defective products could vary depending on each case, distance and features of each location.</li>
<li>To insure yourself and to get proper resolution, please save a shipping box, packing materials and the damaged items for inspection by us.</li>
<li>We'll check all items returned as damaged or defective.&nbsp;In if there would be no inconsistencies found, we reserve a right to not refund you and charge you to recover our fees and expenses.</li>
</ul>
<p><strong>NOTE:</strong></p>
<p>* Please contact us by our Hotline or Email. All returning and additional shipping costs should be paid by the customer.</p>
<p>* The side that will be responsible for shipping cost and handling fees for return products vary depending on the situation.</p>
<p>* Items shipped by Angkorstore.com may be returned after 3 days received.</p>
<p><strong>B. Angkorstore.com Return Policy</strong></p>
<p>Items shipped from Angkorstore.com, including Warehouse Deals, can be returned within 30 days of receipt of shipment in most cases. Some products have different policies or requirements associated with them.&nbsp;</p>
<p><strong>C. Angkorstores.com Devices</strong></p>
<p>Some devices and their accompanying Angkorstore.com-branded accessories purchased from Angkorstore.com can be returned within 7 days of receipt of shipment as long as they are in new condition.</p>
<p><strong>7. Out of stock</strong></p>
<p>Sometimes, during packaging after an order was placed some items may occur to be out of stock or discontinued.</p>
<ul>
<li>Sold out or discontinued items after the order was already placed: The sold-out item&rsquo;s cost is automatically refunded. (If there is no request from the customer to cancel the whole order, the rest items only will be shipped).</li>
<li>The sold-out notice email is automatically sent to the customer's registered e-mail.<br /> &rarr; If you want to cancel the whole order, please contact us via Hotline or email as soon as possible.</li>
<li>You can see the refunded cost and details on your order sheet</li>
<li>The refund amount is returned to the balance of the payment method you used to pay. (to Credit card balance or to Bank account)</li>
</ul>
<p><strong>8. REFUNDS</strong></p>
<p>We want to have trustworthy and friendly relations with our customers. To do so we established a Refund policy that would be proper for both sides:</p>
<p>o&nbsp;&nbsp; Direct Credit card payment could be refunded as partially [Partial refund] as fully [Payment Cancellation]</p>
<p>o&nbsp;&nbsp; The refunded amount could be refunded to your credit card account instantly if the refund was provided in 1-2 days after the payment. If a refund was made later the refund amount will be deducted from customer&rsquo;s credit card bill the following month. The pending period for refund return back to the credit card depends on the amount of the refund, the credit card company, and some other factors. It may take up to 30 days to reflect on the credit card statement. (Note: refund pending period could vary depending on circumstances)</p>
<p><strong>NOTE:</strong></p>
<ul>
<li>Refund procedure and feature can be changed depending on each case, as we want our policy to be more flexible in favor of our customer.</li>
<li>Payments with no evidence of ID and unclaimed payment would not be refunded</li>
</ul>
<p><strong>9. To Return</strong></p>
<p>Please contact us through Contact US page or by e-mail with a &ldquo;Request for Return Form&rdquo; before returning anything. After negotiation of the return procedure details, such as refund, charges, and shipping cost responsible side; we will give you instructions how to send back returning products back to us. Please pack the contents carefully to avoid damage.</p>
<ul>
<li>「Request for Return」 Form</li>
</ul>
<p>* Order number (reported in the order confirmation e-mail) :</p>
<p>* Name or ID</p>
<p>* Reasons for return:</p>
<p>* Customer&rsquo;s Angkorstores website account to get a refund</p>
<p>Products must be resent&nbsp;within 3 days after the Delivery Date, which would be printed on the order packaging slip.</p>
<p>All FREE products and FREE gifts that were included to the order must be returned as well</p>
<p>Your return will be processed promptly upon arrival.&nbsp;We are unable to process your refund until we receive the product back.</p>
<p>Items must be returned&nbsp;in the original condition&nbsp;in which they were delivered to you. Otherwise we reserve a right to refuse in refund.</p>
<p><strong>All returns should be mailed to:</strong></p>
<ul>
<li>Mom Varin Investment Co., Ltd.</li>
<li>Attention : Online Returns</li>
<li>Mom Varin Investment Head Office: #40, St 348, Toul Svay Prey I Commune, Chamkar Morn District, Phnom Penh</li>
<li>Phone : +855 023 5555 008, +855 011 666 195</li>
</ul>
<p><strong>10-PRIVACY POLICY</strong></p>
<p><strong>10.1. Preface</strong></p>
<p>A. This privacy policy has the regulations of Angkorstores for user&rsquo;s private information and privacy &nbsp;&nbsp;<br /> &nbsp;&nbsp;&nbsp;&nbsp; policy</p>
<p>B. Private information is the information about an individual, that is, the information that can identify &nbsp;<br /> &nbsp;&nbsp;&nbsp;&nbsp; a certain individual with detailed data like names, address and so on. (It also includes the <br /> &nbsp;&nbsp;&nbsp;&nbsp; information that cannot identify a person by itself, but that can do together with other information)</p>
<p>C. Please frequently confirm this policy when you use our site.</p>
<p>D. Our company has the right to modify, edit, add and delete this policy at our think best.</p>
<p><strong>10.2. The agreement for collection of private information</strong></p>
<p>You need to agree with all the regulations and terms of our site when you use our service.</p>
<p>If a user uses our service, our company considers that he or she agrees with the regulation of private information collection.</p>
<p><strong>10.3. Purpose for collection and use of private information</strong></p>
<p>Some part of our service is available without membership, but we are collecting private information to offer the best service according to user&rsquo;s characteristics and a various kind of membership service.</p>
<p><strong>10.4. Here are the purposes of collecting private information.</strong></p>
<ul>
<li>To offer better services according to user&rsquo;s need</li>
<li>To deliver ad for user&rsquo;s need or other information</li>
</ul>
<p>We don&rsquo;t disclose user&rsquo;s private information to third parties without agreement. However, if the information is needed for statistical treatment, academic research, market research and so on, some parts can be offered, not able to be identified.</p>
<p><strong>10.5. The methods and details of collecting private information</strong></p>
<ul>
<li>On the process of joining membership, you should enter necessary information online like names, Zip code and so on for us to offer service.</li>
<li>To offer better service according to user&rsquo;s characteristics, you can enter some information as you want such as interests, e-mail address, address, cell phone number, etc.</li>
</ul>
<p>In addition to this, sometimes, during questionnaire or special event, you need to enter your information as you want on the purpose of statistical treatment or special prize.</p>
<p>There are some cases that you need to enter your information or sometimes you are collected automatically by some websites or ad linked to our site, but our policy is not applicable for this cases. So please be careful.</p>
<p><strong>10.6.</strong><strong>​ </strong><strong>Period of keeping private information</strong></p>
<p>If a user joins our membership after passing the procedure, our company keeps and uses his or her information at the same time for offering services according to characteristics, while the member is using our service.</p>
<p><strong>10.7.</strong><strong>​</strong> <strong>Disclosure and share private information</strong></p>
<p>Our company uses user&rsquo;s information within the limit of representing it as &lsquo;B&rsquo;, and we ask him or her for agreement in advance. And, we don&rsquo;t use the information for other purpose or disclose it outside.</p>
<p>But, the reasonable exceptions are as below.</p>
<ul>
<li>If a user agrees with the disclosure of information in advance;</li>
<li>If a user violates the regulation or terms of our service and the related law;</li>
<li>If there are some possibilities that a user causes a mental and physical loss to others;</li>
<li>If there are some evidences that we need to disclose the user who is potential to cause some troubles; and</li>
<li>If it is based on other related laws.</li>
</ul>
<p>Our company can only share users&rsquo; information with third parties on the purposes of researching, developing and offering better services.</p>
<p><strong>10.8.</strong> <strong>Management of private information by users</strong> (Lookups, modifying, cancellation, etc)</p>
<p>Users can check, modify or cancel his or her information as you want.</p>
<p>Users can stop using the membership or cancel it. The canceled information by users will be immediately discarded not to be used or checked.</p>
<p><strong>10.9.</strong> <strong>Management of private information</strong></p>
<ul>
<li>Responsibility for management of user&rsquo;s password belongs to the user, so please make sure that password is not known to third parties.</li>
<li>Logout online before closing the browser is highly recommended when you finish using a computer. Especially in the circumstances such as workplace, school, library, internet caf&eacute;, etc, the place that you share the computer with other people, please remind that you should follow the above steps to protect from the leakage of private information.</li>
</ul>
<p><strong>10.10.</strong> <strong>Private information consultation</strong></p>
<p>Our company accepts your opinion or request for private information protection. Please contact the person in charge of legal affairs of Angkorstores.com Team:</p>
<ul>
<li>Address: #40, St. 348, Sangkat Toul Svay Prey I, Khan Chamkarmon, Phnom Penh, Cambodia</li>
<li>Tel: +855 23 555 500 8 / +855 11 666 195</li>
<li>Email: info@angkorstores.com</li>
</ul>
												</div>
											
										</div>

										<!--div class="row inner-bottom-xs">
											<div class="col-sm-12">
												<div class="wpb_wrapper">
													<h3 class="highlight">Our Customer Service or address</h3>
													<p>Address: #40, St. 348, Sangkat Toul Svay Prey 1, Khan Camkar Morn, Phnom Penh, Cambodia 
													Tel: +855 23 5555 008/ +855 23 969 278
													Email: info@angkorstores.com
													Website: www.angkorstores.com
													</p>

												</div>
											</div>
										</div-->
									</div>
			
								</div>
							</div><!-- .entry-content -->

						</article><!-- #post-## -->
					</main><!-- #main -->
				</div><!-- #primary -->
			</div><!-- .col-full -->
			</div><!-- #content -->
<?php $this->load->view('includes/contact_footer');?>
	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>
</body>
</html>
