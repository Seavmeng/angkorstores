<?php $this->load->view('includes/head');?>
<body class="page home page-template-default">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/header1');?>

		<div id="content" class="site-content" tabindex="-1">
			<div class="container">
				<nav class="woocommerce-breadcrumb"><a href="home.html">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Checkout</nav>
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<article class="page type-page status-publish hentry">
							<header class="entry-header"><h1 itemprop="name" class="entry-title">Checkout</h1></header><!-- .entry-header -->
							<?php if ($this->session->flashdata('m_success')) { ?>
								<div class="alert alert-success"><?php $this->session->flashdata('m_success'); ?> </div>
							<?php } ?>
							<?php
					//		$this->session->unset_userdata('billing');
							if(has_logged()){
								$first_name=$billing_info['first_name'];
								if($first_name==""){ $first_name= $this->input->post('billing_first_name');}
								$last_name=$billing_info['last_name'];
								if($last_name==""){ $last_name=$this->input->post('billing_last_name');}
								$email=$billing_info['email'];
								$phone=$billing_info['phone'];
								if($phone==""){ $phone=$this->input->post('billing_phone');}
								$company_name=$billing_info['company_name'];
								if($company_name==""){ $company_name=$this->input->post('billing_company');}
								$address=$billing_info['address'];
								if($address==""){ $address=$this->input->post('billing_address_1');}
								$city=$billing_info['city'];
								if($city==""){ $city=$this->input->post('city');}
								$country=$billing_info['country'];
								if($country==""){ $this->input->post('billing_country');}
								$zip=$billing_info['zip'];
								if($zip==""){ $zip=$this->input->post('billing_postcode');}
								$payment_methods=$this->input->post('payment_method');
								if($payment_methods==""){ $payment_methods=$this->input->post('payment_method');}
								$terms=$this->input->post('terms');
								if($terms==""){ $terms=$this->input->post('terms');}
							}else{
								echo '<div class="woocommerce-info">Returning customer? <a href="'.base_admin_url().'" class="showlogin">Click here to login</a></div>';
								$first_name= $this->input->post('billing_first_name');
								$last_name=$this->input->post('billing_last_name');
								$email=$this->input->post('billing_email');
								$phone=$this->input->post('billing_phone');
								$company_name=$this->input->post('billing_company');
								$address=$this->input->post('billing_address_1');
								$city=$this->input->post('city');
								$country=$this->input->post('billing_country');
								$zip=$this->input->post('billing_postcode');
								$payment_methods=$this->input->post('payment_method');
								$terms=$this->input->post('terms');
							}
							$tem_bill=$this->session->userdata('billing');
						//	var_dump($tem_bill);
							if(isset($tem_bill)){
								$first_name= $tem_bill['first_name'];
								$last_name= $tem_bill['last_name'];
								$email= $tem_bill['email'];
								$phone= $tem_bill['phone'];
								$country= $tem_bill['country'];
								$address= $tem_bill['address'];
								$city= $tem_bill['city'];
								$zip= $tem_bill['postcode'];
								$payment_methods= $tem_bill['payment_method'];
							//	$terms=$tem_bill['terms'];
								if(isset($terms)==""){
									$terms=$this->input->post('terms');
								}
							}
							if($this->session->userdata('shipping')){
								$checked = " checked=checked";
							}
							else{
								$checked ='';
							}
							if($this->session->userdata('shipping_chose')){
								$check = " checked=checked";
							}
							else{
								$check ='';
							}
							?>
							
								<form action="<?php echo base_url().'checkout';?>" class="checkout woocommerce-checkout" method="post" name="checkout">
									<div class="row">
										<div class="col-sm-7">
											<h3>Choose Shipping</h3>
											<!--div class="button btn btn-info btn-lg" style="float:right;" data-toggle="modal" data-target="#myModal">Choose Shipping</div-->
											<!--div class="clear"></div-->
											
											<div class="woocommerce-checkout-payment" id="payment" style="border-top:none;">
												<ul class="wc_payment_methods payment_methods methods">
													<li class="wc_payment_method payment_method_bacs">
														<input type="radio" data-order_button_text="" value="bacs" name="ship_method" class="button btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" id="payment_method_bacs" <?php echo $check; ?>>
														<label for="payment_method_bacs">Shipping</label>
														
													</li>
													<li class="wc_payment_method payment_method_cheque">
														<input type="radio" data-order_button_text="" value="cheque" name="ship_method" class="button btn btn-info btn-lg" id="payment_method_cheque" <?php echo $checked; ?>>
														<label for="payment_method_cheque">No Shipping</label>
														
													</li>
												</ul>
											</div>
											
											<!-- Modal -->
											<div id="myModal" class="modal fade" role="dialog">
											  <div class="modal-dialog">

												<!-- Modal content-->
												<div class="modal-content">
												  <div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Choose Shipping</h4>
												  </div>
												  <div class="modal-body">
														<div class="form-group">
															<div class="col-md-12">
																<table class="table table-bordered">
																	<tr>
																		<th></th>
																		<th>Shipping Company</th>
																		<th></th>
																	</tr>
																	<?php 
																	foreach ($ship_company as $data){
																		if ($data['shipping_id']==3){ 
																			$parcel='<tr>
																				<td width="50px;">'.
													                            form_radio(array("name"=>"con_name","class"=>"con_name","value"=>$data['shipping_id'],'id'=>$data['shipping_id'], 'checked'=>set_radio('con_name', $data['shipping_id'], FALSE))).'</td>
																				<td><img style="width:25%;" src="'.base_url().'images/'.$data['image'].'" align="left" alt="'.$data['shipping_name'].'"></td>
																				<td>'.$data['description'].'</td>
																			</tr>';
																		continue; }
																		echo '<tr>
																				<td width="50px;">'.
													                            form_radio(array("name"=>"con_name","class"=>"con_name","value"=>$data['shipping_id'],'id'=>$data['shipping_id'], 'checked'=>set_radio('con_name', $data['shipping_id'], FALSE))).'</td>
																				<td><img style="width:25%;" src="'.base_url().'images/'.$data['image'].'" align="left" alt="'.$data['shipping_name'].'"></td>
																				<td>'.$data['description'].'</td>
																			</tr>';
																	}
																		echo $parcel;
																		
																	?>
																</table>
															</div>
														</div>
														<div id="loading" style="position:relative;top:50%;left:50%;display:none;">
														<img src="<?php echo base_url();?>images/loader.gif">
														</div>
														<!--div class="form-group" id="country">
															<div class="col-md-12">
																<label class="control-label"><?php echo 'Country';?></label>
																<?php 
																//echo form_input('billing_country', $this->input->post('billing_country', true),"class = 'form-control' id= 'username'"); 
																echo '<select class="form-control" id="shipping_country" name="shipping_country">
																		<option value="">Select country…</option>';
																		foreach($ship_country as $data){
																			echo '<option value="'.$data['loc_id'].'" '.set_select('calc_shipping_country', $data['loc_id'], False).'>
																			'.$data['location_name'].'
																			</option>';
																		}
																echo '</select>';
																?>
																<font color="red"><?php echo form_error('billing_country');?></font>
															</div>
														</div-->
														<div class="form-group">
															<div class="col-md-12">
																<label class="control-label" id="location-label"><?php echo '';?></label>
																<p id="show_shipping_state" class="form-row form-row-wide validate-required"></p>
																<font color="red"><?php echo form_error('city');?></font>
															</div>
														</div>
												  </div>
												  <div class="modal-footer">
													<button type="button" id="cal_shiping" class="btn btn-default">Submit</button>
												  </div>
												</div>
											  </div>
											</div>
											<!-- Trigger the modal with a button -->
											<h3>Shipping information</h3>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-12">
															<label class="control-label"><?php echo 'First Name';?></label>
															<?php echo form_input('billing_first_name', $first_name,"class = 'form-control' id= 'first_name'"); ?>
															<font color="red"><?php echo form_error('billing_first_name');?></font>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-12">
															<label class="control-label"><?php echo 'Last Name';?></label>
															<?php echo form_input('billing_last_name',$last_name,"class = 'form-control' id= 'last_name'"); ?>
															<font color="red"><?php echo form_error('billing_last_name');?></font>
														</div>
													</div>
												</div>
											</div><!--
											<div class="form-group">
												<div class="col-md-12">
													<label class="control-label"><?php// echo 'Company Name';?></label>
													<?php// echo form_input('billing_company', $company_name,"class = 'form-control' id= 'username'"); ?>
													<font color="red"><?php// echo form_error('billing_company');?></font>
												</div>
											</div>-->
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-12">
															<label class="control-label"><?php echo 'Email Address';?></label>
															<?php echo form_input('billing_email', $email,"class = 'form-control' id= 'billing_email'"); ?>
															<font color="red"><?php echo form_error('billing_email');?></font>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-12">
															<label class="control-label"><?php echo 'Phone';?></label>
															<?php echo form_input('billing_phone', $phone,"class = 'form-control' id= 'billing_phone'"); ?>
															<font color="red"><?php echo form_error('billing_phone');?></font>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label class="control-label"><?php echo 'Country';?></label>
													<?php 
													if($this->session->userdata('ship_cal')){ 
														$ship_data=$this->session->userdata('ship_cal');
													//echo $ship_data['shipping_city'];
													echo '<p style="margin: 5px 0px 5px 20px;">'.country_byid($ship_data['shipping_value']).'</p>';
													echo form_hidden('billing_country', $ship_data['shipping_value'],"class = 'country_to_state form-control' id= 'calc_shipping_country'");
													}
													else{
													echo '<select class="country_to_state form-control" id="calc_shipping_country" name="billing_country">
															<option value="">Select a country…</option>';
															foreach($ship_country as $data){
																/*if($ship_data['shipping_value']){
																	$ship = $ship_data['shipping_value'];
																	if($ship == $data['loc_id']){
																		$selected="selected";
																	}
																	else{
																		$selected="";
																	}
																}
																else{*/
																	if($country == $data['loc_id']){
																		 
																		$selected="selected";
																	}else{
																		$selected="";
																	}
																//}
																echo '<option value="'.$data['loc_id'].'" '.$selected.'>
																'.$data['location_name'].'
																</option>';
															}
													echo '</select>';
													}
													?>
													<font color="red"><?php echo form_error('billing_country');?></font>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label class="control-label"><?php echo 'Address';?></label>
													<?php echo form_input('billing_address_1', $address,"class = 'form-control' id= 'billing_address_1'"); ?>
													<font color="red"><?php echo form_error('billing_address_1');?></font>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-12">
															<label class="control-label"><?php echo 'City';?></label>
															<p id="calc_shipping_state_field" class="form-row form-row-wide validate-required">
															<?php echo form_input('calc_shipping_state', '',"class = 'form-control' id= 'billing_address_1'"); ?>
															<?php 
															//echo form_input('billing_city', $this->input->post('billing_city', true),"class = 'form-control' id= 'username'"); 
															/*echo '<select class="country_to_state form-control" id="calc_shipping_state" name="calc_shipping_state">
																	<option value="">Select a city…</option>';
																	foreach($ship_city as $data){
																		if($ship_data['shipping_city']){
																			$ship = $ship_data['shipping_city'];
																			if($ship == $data['loc_id']){
																				$selected="selected";
																			}
																			else{
																				$selected="";
																			}
																		}
																		else{
																			$selected="";
																		}
																		echo '<option value="'.$data['loc_id'].'" '.$selected.' '.set_select('calc_shipping_country', $data['loc_id'], False).'>
																		'.$data['location_name'].'
																		</option>';
																	}
															echo '</select>'*/
															?>
															</p>
															<font color="red"><?php echo form_error('city');?></font>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-12">
															<label class="control-label"><?php echo 'Postcode / ZIP';?></label>
															<?php 
															echo form_input('billing_postcode', $zip,"class = 'form-control' id= 'billing_postcode'");
															?>
															<font color="red"><?php echo form_error('billing_postcode');?></font>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-5" style="background:#f5f5f5;" id="checkout_sidebar">
											<h3 id="order_review_heading">Your order</h3>
											<div class="woocommerce-checkout-review-order" id="order_review">
												<table class="shop_table woocommerce-checkout-review-order-table">
													<thead>
														<tr>
															<th class="product-name">Product</th>
															<th class="product-total">Total</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$buy_now=$this->session->userdata('buy_now');
														if(isset($buy_now)){
														?>
															<tr class="cart_item">
																<td class="product-name">
																	<?php echo $buy_now['name']?>&nbsp;
																	<strong class="product-quantity">× <?php echo $buy_now['qty'];?></strong>													</td>
																<td class="product-total">
																	<span class="amount"><?php echo currency('sign').$buy_now['price'];?></span>
																</td>
															</tr>
														<?php
															$sub_total=$buy_now['price'] * $buy_now['qty'];
														}else{
															if(has_logged()){
																$cart=show_cart(get_user_id());
																$sub_total=0;
																foreach ($cart as $item):
																	$row=get_pro_field($item['product_id']);
																	$total=$item['qty'] * $row['sale_price'];
																?>
																<tr class="cart_item">
																	<td class="product-name">
																		<?php echo $row['product_name']?>&nbsp;
																		<strong class="product-quantity">× <?php echo $item['qty'];?></strong>													</td>
																	<td class="product-total">
																		<span class="amount"><?php echo currency('sign').$total;?></span>
																	</td>
																</tr>
																<?php
																$sub_total +=$total;
																endforeach;
															}else{
																if ($cart = $this->cart->contents()):
																$sub_total=0;
																foreach ($cart as $item):
																	$total=$item['qty'] * $item['price'];
															?>
																<tr class="cart_item">
																	<td class="product-name">
																		<?php echo $item['name']?>&nbsp;
																		<strong class="product-quantity">× <?php echo $item['qty'];?></strong>													</td>
																	<td class="product-total">
																		<span class="amount"><?php echo currency('sign').$total;?></span>
																	</td>
																</tr>
																<?php
																	$sub_total +=$total;
																	endforeach;
																endif;
															}
														}
														?>
													</tbody>
													<tfoot>

														<tr class="cart-subtotal">
															<th>Subtotal</th>
															<td><span class="amount"><?php 
															if(isset($sub_total)){
																echo currency('sign').$sub_total;
															}else{ echo '0.00';}
															?></span></td>
														</tr>

														<tr class="shipping">
															<th>Shipping</th>
															<td data-title="Shipping" id ="amount-row">
																<?php
																if($this->session->userdata('ship_cal')){
																	$ship_data=$this->session->userdata('ship_cal');
																	if(isset($buy_now)){
																		$total_weight=0;
																		$row=get_pro_field($buy_now['id']);
																		$total_weight +=$row['weight'] * $buy_now['qty'];
																	}else{
																		if(has_logged()){
																			$cart=show_cart(get_user_id());
																			$total_weight=0;
																			foreach ($cart as $item):
																				$row=get_pro_field($item['product_id']);
																				$total_weight +=$row['weight'] * $item['qty'];
																			endforeach;
																		}else{
																			$total_weight=0;
																			if ($cart = $this->cart->contents()):
																				
																				foreach ($cart as $item):
																					$row=get_pro_field($item['id']);
																					$total_weight +=$row['weight'] * $item['qty'];
																				endforeach;
																			endif;
																		}
																	}
																	$results=total_ship($total_weight,$ship_data['method_id']);
																	
																    //echo $total_weight;
																	foreach($results as $row){
																		if($total_weight >= $row['min_weight'] && $total_weight <=$row['max_weight']){
																			echo $ship_data['company_name'].': <span class="amount">'.currency('sign').$row['fee_amount'].'</span>';
																			$fee_amount =$row['fee_amount'];
																		}
																	}
																	if(isset($fee_amount)){
																		$total_ship=$fee_amount;
																	}else{
																		$total_ship=0;
																		echo '<span class="amount" id="shipping-amount">No Shipping</span>';
																	}
																	echo '
																	<span class="product_list_widget">
																		<span class="mini_cart_item">
																			<a class="remove change_ship" href="#">×</a>
																		</span>
																	</span>
																	';
																}else{
																	echo '<span class="amount" id="shipping-amount">No Shipping</span>';
																}
																?>
																
															</td>
														</tr>

														<tr class="order-total">
															<th>Total</th>
															<td><strong><span class="amount">
															<?php
															if(isset($sub_total)){
																if(isset($total_ship)){
																	$total_ship=$total_ship;
																}else{
																	$total_ship=0;
																}
																echo currency('sign').($sub_total+$total_ship);
															}else{ echo '0.00';}
															?>
															</span></strong> </td>
														</tr>
														<?php
														if(isset($buy_now)){
															echo '<tr>
																		<td colspan="2"><a href="'.base_url().'cart/cancel_buynow" class="canc_buynow">Cancel buy now</a></td>
																	</tr>';
														}
														?>
													</tfoot>
												</table>

												<div class="woocommerce-checkout-payment" id="payment">
													<ul class="wc_payment_methods payment_methods methods">
														<?php
														echo '<table>';
														foreach($payment_method as $data){
															if($data['payment_id']==1){
																$width = ' width="55"';
															}
															else{
																$width = '';
															}
															if($payment_methods == $data['payment_id']){
																$check ='checked="checked"';
															}else{
																$check ='';
															}
															echo '
															<tr>
																<td width="30px"><input style="margin: 14px 0 12px 0;" type="radio" '.$check.' value="'.$data['payment_id'].'" name="payment_method" class="payment_method_cheque" id="payment_method_cheque"></td>
																<td><label style="margin-top: 0.757em;" for="payment_method_cheque">'.$data['payment_method'].'</label></td>
																<td><img src="'.base_url().'images/'.$data['logo'].'" height="50"'.$width.'></td>
															</tr>';
														}
														echo '</table>';
														?>
														<font color="red"><?php echo form_error('payment_method');?></font>
													</ul>
													<div class="form-row place-order">

														<p class="form-row terms wc-terms-and-conditions">
															<input type="checkbox"  <?php if($terms=='terms'){echo 'checked';} ?> value="terms" id="terms" name="terms" class="input-checkbox">
															<label class="checkbox" for="terms">I’ve read and accepted the <a target="_blank" href="<?php echo base_url().'p/terms-con';?>">terms &amp; conditions</a> <span class="required">*</span></label>
															<font color="red"><?php echo form_error('terms');?></font>
															<input type="hidden" value="1" name="terms-field">
														</p>

														<input type="submit" name="place_order" id="btn-order" data-value="Place order" value="Place order" class="button alt">
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
						</article>
					</main><!-- #main -->
				</div><!-- #primary -->
			</div><!-- .container -->
			</div><!-- #content -->
	<?php $this->load->view('includes/sub_footer');?>
	</div><!-- #page -->
	<?php $this->load->view('includes/footer');?>

</body>

</html>