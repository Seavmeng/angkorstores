<?php $this->load->view('includes/head');?>
    <body class="page home page-template-default">
        <div id="page" class="hfeed site">
            <a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
            <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
			<?php $this->load->view('includes/header1');?>
			<div tabindex="-1" class="site-content" id="content">
				<div class="container">
					<nav class="woocommerce-breadcrumb"><a href="home.html">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Compare</nav>
					<div class="content-area" id="primary">
						<main class="site-main" id="main">
							<article class="post-2917 page type-page status-publish hentry" id="post-2917">
								<div itemprop="mainContentOfPage" class="entry-content">
									<?php
									foreach($members as $row){
										echo "<div class='head_package'>";
											echo '<div class="product-info">';
												echo $row['package_name'];
											echo '</div>';
											$get_package=sub_package($row['package_id']);
											foreach($get_package as $data){
												echo '<div class="sub_package">'.$data['package_name'].'</div>';
											}
											echo '<div class="sub_package"><a class="button" href="'.base_admin_url().'register/user_info/'.$row['package_id'].'" rel="nofollow">Apply Now</a></div>';
										echo "</div>";
									}
									?>
								</div><!-- .entry-content -->
							</article><!-- #post-## -->
						</main><!-- #main -->
					</div><!-- #primary -->
				</div><!-- .col-full -->
			</div>
		<?php $this->load->view('includes/sub_footer');?>
        </div><!-- #page -->
		<?php $this->load->view('includes/footer');?>
    </body>
</html>
