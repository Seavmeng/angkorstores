<?php $this->load->view('includes/head');?>
    <body class="page-template-default contact-v1">
        <div id="page" class="hfeed site">
            <a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
            <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
			<?php $this->load->view('includes/header1') ;?>
            <div id="content" class="site-content" tabindex="-1">
				<div class="container">
					<nav class="woocommerce-breadcrumb"><a href="<?php echo base_url();?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Customer Service</nav>
					<div id="primary" class="content-area">
						<main id="main" class="site-main">
							<article class="post-2508 page type-page status-publish hentry" id="post-2508">
								<div itemprop="mainContentOfPage" class="entry-content">
									<div class="vc_row-full-width vc_clearfix"></div>
									<div class="vc_row wpb_row vc_row-fluid">
									<div class="contact-form wpb_column vc_column_container vc_col-sm-8 col-sm-8">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper">
												<div class="wpb_text_column wpb_content_element ">
													<div class="wpb_wrapper">
														<h2 class="contact-page-title">Customer Service / Help</h2>
													</div>
												</div>
												<div lang="en-US" dir="ltr" id="wpcf7-f2507-p2508-o1" class="wpcf7" role="form">
													<div class="screen-reader-response"></div>
													<?php echo $this->session->flashdata('msg'); ?>
													<?php echo form_open(base_url().'p/customer-services', array('class'=>'contact-form', 'name'=>'contact-form')); ?>

														<div style="display: none;">
															<input type="hidden" value="2507" name="_wpcf7">
															<input type="hidden" value="4.4.1" name="_wpcf7_version">
															<input type="hidden" value="en_US" name="_wpcf7_locale">
															<input type="hidden" value="wpcf7-f2507-p2508-o1" name="_wpcf7_unit_tag">
															<input type="hidden" value="47d6f1c9ce" name="_wpnonce">
														</div>
														<div class="form-group row">
															<div class="col-xs-12 col-md-12">
																<label>Name</label> <label style="color:red">*</label><br>
																<span class="wpcf7-form-control-wrap name"><input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="<?= set_value("name") ?>" name="name"></span>
																<span class="text-danger"><?php echo form_error('first-name'); ?></span>
															</div>
														</div>
														<div class="form-group">
															<label>E-mail</label> <label style="color:red">*</label><br>
															<span class="wpcf7-form-control-wrap email"><input type="email" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text input-text" size="40" value="<?= set_value("email") ?>" name="email"></span>
															<span class="text-danger"><?php echo form_error('email'); ?></span>
														</div>
														<div class="form-group">
															<label>Phone</label> <label style="color:red">*</label><br>
															<span class="wpcf7-form-control-wrap phone"><input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text input-text" size="40" value="<?= set_value("phone") ?>" name="phone"></span>
															<span class="text-danger"><?php echo form_error('phone'); ?></span>
														</div>
														<div class="form-group">
															<label>Topic</label> <label style="color:red">*</label><br>
															<span class="wpcf7-form-control-wrap topic">
																<?php
																	$topic = array();
																	foreach($topics as $row)
																	{
																		$topic[$row->topic] = $row->topic;
																	}
																	 echo form_dropdown('topic', $topic, 0, 'id="topic" class="form-control"');
																?>
															</span>
															
															<span class="text-danger"><?php echo form_error('topic'); ?></span>
														</div>
														<div class="form-group">
															<label>Description</label> <label style="color:red">*</label><br>
															<span class="wpcf7-form-control-wrap description"><textarea aria-invalid="false"  aria-required="true" class="wpcf7-form-control wpcf7-textarea" rows="10" cols="40" value="<?= set_value("description") ?>" name="description"></textarea></span>
															<span class="text-danger"><?php echo form_error('description'); ?></span>
														</div>
														<div class="form-group clearfix">

															<p><input type="submit" value="Send Message"></p>
														<?php form_close(); ?>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
									<div class="store-info wpb_column vc_column_container vc_col-sm-4 col-sm-4">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper">
												<div class="wpb_text_column wpb_content_element ">
													<div class="wpb_wrapper">
														<h2 class="contact-page-title">Video</h2>
														
													</div>
												</div>
											</div>
										</div>
									</div>
									</div>
								</div>
							</article>
						</main><!-- #main -->
					</div><!-- #primary -->
				</div><!-- .container -->
			</div><!-- #content -->
        <?php $this->load->view('includes/contact_footer');?>
        </div><!-- #page -->
<?php $this->load->view('includes/footer');?>
    </body>
</html>
