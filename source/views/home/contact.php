<?php $this->load->view('includes/head');?>

    <body class="page-template-default contact-v1">
        <div id="page" class="hfeed site">
            <a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
            <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
			<?php $this->load->view('includes/header1');?>
            <div id="content" class="site-content" tabindex="-1">
				<div class="container">
					<nav class="woocommerce-breadcrumb"><a href="<?php echo base_url();?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Contact Us</nav>
					<div id="primary" class="content-area">
						<main id="main" class="site-main">
							<article class="post-2508 page type-page status-publish hentry" id="post-2508">
								<div itemprop="mainContentOfPage" class="entry-content">
									<div class="map" style="width: 100vw; position: relative; margin-left: -50vw; left: 50%; margin-bottom: 3em;">
										<iframe height="514" allowfullscreen="" style="border: 0px none; pointer-events: none;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6574.181809466445!2d104.90737000783078!3d11.549564217073188!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109511bb823719d%3A0x3d6a3b306ac605b1!2sSt+348%2C+Phnom+Penh!5e0!3m2!1sen!2skh!4v1481687113470"></iframe>
									</div>
									<div class="vc_row-full-width vc_clearfix"></div>
									<div class="vc_row wpb_row vc_row-fluid">
									<div class="contact-form wpb_column vc_column_container vc_col-sm-9 col-sm-9">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper">
												<div class="wpb_text_column wpb_content_element ">
													<div class="wpb_wrapper">
														<h2 class="contact-page-title">Leave us a Message</h2>
													</div>
												</div>
												<div lang="en-US" dir="ltr" id="wpcf7-f2507-p2508-o1" class="wpcf7" role="form">
													<div class="screen-reader-response"></div>
													<?php echo $this->session->flashdata('msg'); ?>
													<?php echo form_open(base_url().'contactform/index', array('class'=>'contact-form', 'name'=>'contact-form')); ?>

														<div style="display: none;">
															<input type="hidden" value="2507" name="_wpcf7">
															<input type="hidden" value="4.4.1" name="_wpcf7_version">
															<input type="hidden" value="en_US" name="_wpcf7_locale">
															<input type="hidden" value="wpcf7-f2507-p2508-o1" name="_wpcf7_unit_tag">
															<input type="hidden" value="47d6f1c9ce" name="_wpnonce">
														</div>
														<div class="form-group row">
															<div class="col-xs-12 col-md-6">
																<label>First name*</label><br>
																<span class="wpcf7-form-control-wrap first-name"><input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="" name="first-name"></span>
																<span class="text-danger"><?php echo form_error('first-name'); ?></span>
															</div>
															<div class="col-xs-12 col-md-6">
																<label>Last name*</label><br>
																<span class="wpcf7-form-control-wrap last-name"><input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="" name="last-name"></span>
																<span class="text-danger"><?php echo form_error('last-name'); ?></span>
															</div>
														</div>
														<div class="form-group">
															<label>E-mail</label><br>
															<span class="wpcf7-form-control-wrap email"><input type="email" aria-invalid="false" class="wpcf7-form-control wpcf7-text input-text" size="40" value="" name="email"></span>
															<span class="text-danger"><?php echo form_error('email'); ?></span>
														</div>
														<div class="form-group">
															<label>Subject</label><br>
															<span class="wpcf7-form-control-wrap subject"><input type="text" aria-invalid="false" class="wpcf7-form-control wpcf7-text input-text" size="40" value="" name="subject"></span>
															<span class="text-danger"><?php echo form_error('subject'); ?></span>
														</div>
														<div class="form-group">
															<label>Your Message</label><br>
															<span class="wpcf7-form-control-wrap your-message"><textarea aria-invalid="false" class="wpcf7-form-control wpcf7-textarea" rows="10" cols="40" name="message"></textarea></span>
															<span class="text-danger"><?php echo form_error('message'); ?></span>
														</div>
														<div class="form-group clearfix">

															<p><input type="submit" value="Send Message"></p>
														<?php form_close(); ?>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
									<div class="store-info wpb_column vc_column_container vc_col-sm-3 col-sm-3">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper">
												<div class="wpb_text_column wpb_content_element ">
													<div class="wpb_wrapper">
														<h2 class="contact-page-title">Our Store</h2>
														<i class="fa fa-home" aria-hidden="true"></i><span class="bold"> Address</span> <br>
															<address>#40, St. 348, Sangkat Toul Svay Prey I, Khan Chamkarmon, Phnom Penh, Cambodia</address>
														
														<i class="fa fa-phone" aria-hidden="true"></i><span class="bold"> Call Us</span> <br>
															<address>+855 23 5555 008/+855 23 969 278</address>
															
															
														<i class="fa fa-envelope" aria-hidden="true"></i><span class="bold"> Mail Us</span><br>
															<address><a href="mailto:customerservice@angkorstores.com">customerservice@angkorstores.com</a></address>
															
														<i class="fa fa-globe" aria-hidden="true"></i><span class="bold"> Website</span><br>
															<address><a href='http://angkorstores.com'>www.angkorstores.com</a></address>
														<i class="fa fa-shopping-cart" aria-hidden="true"></i><span> You can contact customer service through phone number to purchase items</span>	
															
													</div>
												</div>
											</div>
										</div>
									</div>
									</div>
								</div>
							</article>
						</main><!-- #main -->
					</div><!-- #primary -->
				</div><!-- .container -->
			</div><!-- #content -->
        <?php $this->load->view('includes/contact_footer');?>
        </div><!-- #page -->
<?php $this->load->view('includes/footer');?>
    </body>
</html>
