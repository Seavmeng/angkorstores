<?php $this->load->view('includes/top-bar');?>
<header id="masthead" class="site-header header-v2">
	<div class="container">
		<div class="row">
		<!-- =============== Header Logo ============================================================= -->
		<div class="header-logo">
			<a href="<?php echo base_url();?>" class="header-logo-link">
				<img src="<?php echo base_url();?>images/logo.png" width="190">
			</a>
		</div>
		<!-- =============== Header Logo : End================ -->
		<div class="primary-nav animate-dropdown">
			<div class="clearfix">
				 <button class="navbar-toggler hidden-sm-up pull-right flip" type="button" data-toggle="collapse" data-target="#default-header">
						&#9776;
				 </button>
			 </div>
			<div class="collapse navbar-toggleable-xs" id="default-header">
				<nav>
					<ul id="menu-main-menu" class="nav nav-inline yamm menu-main">
						<li class="menu-item animate-dropdown"><a href="<?php echo base_url();?>">Home</a></li>
						<li class="menu-item"><a href="<?php echo base_url();?>p/about-us">About Us</a></li>
						<li class="menu-item"><a href="<?php echo base_url();?>p/contact-us">Contact Us</a></li>
					</ul>
				</nav>
			</div>
		</div>
		
		</div><!-- /.row -->
	</div>
</header><!-- #masthead -->
<nav class="navbar navbar-primary navbar-full">
    <div class="container">
		<ul class="nav navbar-nav departments-menu animate-dropdown">
			<li class="nav-item dropdown ">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="departments-menu-toggle" >Shop by Department</a>
				<ul id="menu-vertical-menu" class="dropdown-menu yamm departments-menu-dropdown">
					<?php selectCategories(); ?>
				</ul>
			</li>
		</ul>
        <form class="navbar-search" method="get" action="<?php echo base_url();?>search/index">
			<label class="sr-only screen-reader-text" for="search">What are you looking for...</label>
			<div class="input-group">
				<input type="text" id="search" class="form-control search-field ui-autocomplete-input" dir="ltr" value="" name="search" placeholder="What are you looking for..." />
				<!--- Old Search Box 
				<div class="input-group-addon search-categories">
					<select name='product_cat' id='product_cat' class='postform resizeselect' >
						<option value='all' selected='selected'>All Categories</option>
						<?php $cats = searchCat();
						//$i =1;
						foreach($cats as $eachCat): ?>
						
							<option class="level-0" value="<?php echo $eachCat['category_id']; ?>"><?php echo $eachCat['category_name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				
				Old Search Box --->
				<div class="input-group-addon search-categories">
					<select name='product_cat' id='product_cat' class='postform resizeselect' style='font-size:13px;' >
						<option value='Product' selected='selected'>Product</option>
						<option value='Supplier'>Supplier</option>
						
					</select>
				</div>
				<div class="input-group-btn">
					<input type="hidden" id="search-param" name="post_type" value="product" />
					<button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
				</div>
			</div>
		</form>          
<?php $this->load->view('home/minicart');?> 
</div>
</nav>
