<?php $this->load->view('home/vendor_pages/head');?>
<body class="single-product full-width">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('home/vendor_pages/header1');?>
		<div id="content" class="site-content" tabindex="-1">
			<div class="container" style="background: #F5F5F5;border-radius: 4px;">
			<?php $cover_img = base_url().'images/products/'.bigthumb($user_detail['cover_image']); ?>
			    <div class="col-md-12 col-xs-12 col-ms-12"  style="background-image: url('<?php echo $cover_img; ?>');width: 100%; background-position: center; background-size: 97.5% 250px; background-repeat: no-repeat;">
				<nav class="woocommerce-breadcrumb"​>
				 <div class="col-md-8">
					<h2 style="margin-top: 0px;float: left;">
						<a href="<?php echo base_url().$user_detail['page_name'];?>" class="header-logo-link">
							<?php if ($user_detail['image'] == null){ ?>
							<!--img class="pag-img img-responsive" src="<?php echo base_url().'assets/images/default-profile.jpg';?>" style="width:150px; height:150px;"-->
							<div style="position: relative; width:150px; height:150px"></div>
							<?php }else{ ?>
							<img class="pag-img img-responsive" src="<?php echo base_url();?>images/products/<?php echo $user_detail['image'];?>" style="width:150px; height:150px;">
							<?php } ?>
						</a>
					</h2>
				 </div>
				  <div class="col-md-8">
					<h2 style="margin-top: 30px;float: left;">
						<a class="page-title" style="color:#ffd800; font-family: Tahoma; font-size: 1em; text-shadow: 2px 2px 2px rgb(0, 0, 0);" href="<?php echo base_url().$user_detail['page_name']; ?>"><?php echo strtoupper($user_detail['company_name']); ?></a>
					</h2>

					
				 </div>
				</nav><!-- /.woocommerce-breadcrumb -->
				</div>
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
					<?php $page_name = $this->uri->segment(1); ?>

				<!-- Blog menu -->
				<?php 
					$ui2 = $this->uri->segment(2);
					//var_dump($ui2);exit();
					$v_active = '';
					$ca_active = '';
					$pro_active='';
					$con_active = '';
					if($ui2 == null){
						$v_active = 'vendor_active';
					}
					if ($ui2 == 'category') {
						$ca_active = 'vendor_active';
					}
					if ($ui2 == 'companyprofile'){
						$pro_active = 'vendor_active';
					}
					if ($ui2 == 'contact') {
						$con_active = 'vendor_active';
					}
					if($user_detail['page_status'] == 1){
				 ?>
				<div class="col-xs-12 col-ms-12 col-md-12 content-page" style="float:none;">
				<!-- Fixed navbar -->
				    <nav class="navbar navbar-default">
				      <div class="container">
				        <div class="navbar-header">
				          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				          </button>
				          <a class="navbar-brand home" href="#"><?php if($this->uri->segment(2) == null){echo "Home";}else{echo ucfirst($this->uri->segment(2));} ?></a>
				        </div>
				        <div id="navbar" class="navbar-collapse collapse">
				          <ul class="nav navbar-nav">
				            <li class="li <?php echo $v_active; ?>"><a href="<?php echo base_url().$page_name;?>">Home</a></li>
				            <li class="dropdown">
				              <a href="#" class="li dropdown-toggle <?php echo $ca_active; ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Product Categories</a>
				              <div class="dropdown-menu" style="min-width: 1000px;">
				                	<?php 
				                		$allCats = sidebarVendorCategories();
										$allSubCats = sidebarVendorSubCategories();
										//var_dump($allSubCats); exit;
										$i = 1;
										foreach($allSubCats as $eachSubCat){
											if($i < 2){
												echo '<div class="row"><div class="col-sm-3">';
											}
											$count = countItemVendor($eachSubCat['category_id']);
											if($count > 0){
											?>
											<div><a href="<?php echo base_url().$page_name.'/category/'.$eachSubCat['category_id']; ?>"><?php echo show_category_name($eachSubCat['category_id']);?><span class="count"><?php echo '('.$count.')'; ?></span></a>
											</div>
											<div role="separator" class="divider"></div>
											<?php
											
											}
											if($i % 5 == 0){
												echo '</div><div class="col-sm-3">';
											}
											if($i == count($allSubCats)){
												echo '</div></div>';
											}
										$i++;
										}
				                	?>
								</div>
				            </li>
				            <li class="li <?php echo $pro_active; ?>"><a href="<?php echo base_url().$page_name.'/companyprofile'; ?>">Company Profile</a></li>
				            <li class="li <?php echo $con_active; ?>"><a href="<?php echo base_url().$page_name.'/contact'; ?>">Contact</a></li>
				          </ul>
				        </div><!--/.nav-collapse -->
				      </div>
				    </nav>
				</div>
					<?php }else{ 
							echo '<center><h1>Sorry, this vendor account has been disabled!</h1></center>'; exit;
					}
					
					?>
					<!-- End blog Menu -->
                                       
					<div class="home-v1-deals-and-tabs deals-and-tabs row animate-in-view fadeIn animated" data-animation="fadeIn" >
					<!--open Menu Home -->
					<!-- Slide show vendor home page -->
					<?php if($this->uri->segment(2) == null){ ?>
							<div class="col-md-12">
								<div class="col-md-12" style="margin-top: -16px;width: 100%">
									<!-- <img src="<?php echo base_url().'images/products/12-2016/0cced7fe145e1f110c9edb8b30dfae2a.jpg'; ?>" width="100%"> -->
									<div id="home_v_slider" class="home-v1-slider">
										<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm icon-rendom">
											<?php foreach($slideshow as $slide){ ?>

											    <div class="home_slide" ><img  src="<?php echo base_url().'images/products/'.bigthumb($slide['photo']);?>"></div>
											<?php } ?>
										</div>
									</div>
									<?php //$this->load->view('includes/slide');?>
								</div>
							</div>
					<?php } ?><!-- End Slide show vendor home page -->
