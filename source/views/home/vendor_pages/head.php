<?php //include 'icloudguard_v4/project-security.php';?>
<!DOCTYPE html>
<html lang="en-US" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Angkor Stores</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/bootstrap.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animate.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-electro.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl-carousel.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/style.css" media="all" />
<link href="<?php echo base_url();?>assets/css/magnific-popup.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/colors/yellow.css" media="all" />

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/config.css">

<link href="<?php echo base_url();?>assets/css/colors/green.css" rel="alternate stylesheet" title="Green color">
<link href="<?php echo base_url();?>assets/css/colors/pink.css" rel="alternate stylesheet" title="Pink color">
<link href="<?php echo base_url();?>assets/css/colors/blue.css" rel="alternate stylesheet" title="Blue color">
<link href="<?php echo base_url();?>assets/css/colors/red.css" rel="alternate stylesheet" title="Red color">
<link href="<?php echo base_url();?>assets/css/colors/orange.css" rel="alternate stylesheet" title="Orange color">
<link href="<?php echo base_url();?>assets/css/colors/black.css" rel="alternate stylesheet" title="Black color">
<link href="<?php echo base_url();?>assets/css/colors/gold.css" rel="alternate stylesheet" title="Gold color">
<link href="<?php echo base_url();?>assets/css/colors/yellow.css" rel="alternate stylesheet" title="Yellow color">
<link href="<?php echo base_url();?>assets/css/colors/flat-blue.css" rel="alternate stylesheet" title="Flat Blue color">

<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<!-- Demo Purpose Only. Should be removed in production : END -->

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,700italic,800,800italic,600italic,400italic,300italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="<?php echo base_url();?>images/logo.png">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/min.css"  media="all">

</head>