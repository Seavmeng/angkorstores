<?php $this->load->view('home/vendor_pages/header'); ?>
					
					<!-- End blog Menu -->
							<div class="row">
							  <div class="container" id="pro-content">
								<div class="col-md-12" id="pro-page">
									<div class="vc-col-sm-8 col-sm-8 col-md-8">
										  	<div class="vc_column-inner ">
												<div class="wpb_wrapper">
													<div class="wpb_text_column wpb_content_element">
														<div class="wpb_wrapper">
																<h2 class="contact-page-title">Leave us a Message</h2>
														</div>
													</div><!--End .wpb_text_column wpb_content_element-->
													<div lang="en-US" dir="ltr" id="wpcf7-f2507-p2508-o1" class="wpcf7" role="form">
														    <div class="screen-reader-response"></div>
															<?php echo $this->session->flashdata('msg'); 
																  $page_name = $this->uri->segment(1);
															?>

															<?php echo form_open(base_url().$page_name.'/contact/contact_mail', array('class'=>'contact-form', 'name'=>'contact-form')); ?>
															    <div style="display: none;">
																	<input type="hidden" value="2507" name="_wpcf7">
																	<input type="hidden" value="4.4.1" name="_wpcf7_version">
																	<input type="hidden" value="en_US" name="_wpcf7_locale">
																	<input type="hidden" value="wpcf7-f2507-p2508-o1" name="_wpcf7_unit_tag">
																	<input type="hidden" value="47d6f1c9ce" name="_wpnonce">
																</div>
																<div class="form-group row">
																	<div class="col-xs-12 col-md-6">
																		<label>First name*</label><br>
																		<span class="wpcf7-form-control-wrap first-name"><input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="" name="first-name" required></span>
																		<span class="text-danger"><?php echo form_error('first-name'); ?></span>
																	</div>
																	<div class="col-xs-12 col-md-6">
																		<label>Last name*</label><br>
																		<span class="wpcf7-form-control-wrap last-name"><input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="" name="last-name" required></span>
																		<span class="text-danger"><?php echo form_error('last-name'); ?></span>
																	</div>
																</div>
																<div class="form-group">
																	<label>E-mail</label><br>
																	<span class="wpcf7-form-control-wrap email"><input type="email" aria-invalid="false" class="wpcf7-form-control wpcf7-text input-text" size="40" value="" name="email" required></span>
																	<span class="text-danger"><?php echo form_error('email'); ?></span>
																</div>
																<div class="form-group">
																	<label>Subject</label><br>
																	<span class="wpcf7-form-control-wrap subject"><input type="text" aria-invalid="false" class="wpcf7-form-control wpcf7-text input-text" size="40" value="" name="subject" required></span>
																	<span class="text-danger"><?php echo form_error('subject'); ?></span>
																</div>
																<div class="form-group">
																	<label>Your Message</label><br>
																	<span class="wpcf7-form-control-wrap your-message"><textarea aria-invalid="false" class="wpcf7-form-control wpcf7-textarea" rows="10" cols="40" name="message" required></textarea></span>
																	<span class="text-danger"><?php echo form_error('message'); ?></span>
																</div>
																<div class="form-group clearfix">

																	<p><input type="submit" value="Send Message" style="font-size: 1.5rem;"></p>
																</div>
																<?php echo form_close(); ?>
													</div><!--End .wpcf7-->
												</div><!--End .wpb_wrapper-->
										  	</div>
									</div><!--End .col-md-9-->
									
									<div class="vc_col-sm-4 col-sm-4 col-md-4">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper">
												<div class="wpb_text_column wpb_content_element ">
													<div class="wpb_wrapper">
														<h2 class="contact-page-title">Our Store</h2>
														
														
														<i class="fa fa-phone" aria-hidden="true"></i><span class="bold"> Call Us</span> <br>
															<address><?php  echo $user_detail['phone'];?></address>
															
															
														<i class="fa fa-envelope" aria-hidden="true"></i><span class="bold"> Mail Us</span><br>
															<address><a href="mailto:<?php echo $user_detail['email']; ?>"><?php echo $user_detail['email']; ?></a></address>
															
														<!-- website -->
														<?php $website = $user_detail['website'];
															  if($website != "" || $website != null ){
														?>	
														 <i class="fa fa-globe" aria-hidden="true"></i><span class="bold"> Website</span><br> 
															<address><a target="_blank" href="http://<?php echo $user_detail['website'];?>"><?php echo $user_detail['website'];?></a></address>
														<?php } ?> <!--End website -->
														
														<!-- Facebook Page -->
														<?php $facebook_page = $user_detail['facebook_page'];
															  if($facebook_page != "" || $facebook_page != null ){
														?>	
														 <i class="fa fa-facebook-square" aria-hidden="true"></i><span class="bold"> Facebook Page</span><br> 
															<address><a target="_blank" href="http://<?php echo $user_detail['facebook_page'];?>"><?php echo $facebook_page;?></a></address>
														<?php } ?><!-- End Facebook Page -->
														
														<i class="fa fa-home" aria-hidden="true"></i><span class="bold"> Address</span> <br>
															<address><?php  echo $user_detail['address'];?></address>
															
													</div>
												</div>
											</div><!--End .wpb_wrapper-->
										</div><!--End .vc_column-inner-->
									</div><!--Emd .col-md-3-->

								</div><!--End .col-md-12-->
							  </div><!--End .container-->
							</div><!--End row-->
<?php $this->load->view('home/vendor_pages/footer'); ?>

