<?php 
$this->load->view('includes/head');
?>
<body class="page home page-template-default">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/header1');?>
		<div id="content" class="site-content" tabindex="-1">
			<div class="container">
				<nav class="woocommerce-breadcrumb"><a href="<?php echo base_url();?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Cart</nav>
				<?php if(isset($rmsg)){
					echo show_msg($rmsg,'danger');}
				?>
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<article class="page type-page status-publish hentry">
							<header class="entry-header"><h1 itemprop="name" class="entry-title">Wishlist</h1></header><!-- .entry-header -->
						
								<table class="shop_table shop_table_responsive cart">
									<thead>
										<tr>
											<th class="product-remove">&nbsp;</th>
											<th class="product-thumbnail">&nbsp;</th>
											<th class="product-name">Product</th>
											<th class="product-price">Price</th>
											<th class="product-quantity">Stock Status </th>
											<th class="product-subtotal">Total</th>
										</tr>
									</thead>
									<?php if ($wishlist): ?>
									<tbody>
										<?php
										$i=0;
										$total=0;
										$weight=0;
										foreach ($wishlist as $item):
										$i++;
										$product_id=$item['product_id'];
										$row=get_pro_field($product_id);
										?>
										<tr class="cart_item">
											<td class="product-remove">
												<a data-product_sku="5487FB8/26" data-product_id="<?php echo $product_id;?>" title="Remove this item" class="remove" href="<?php echo base_url().'cart/remove_wishlist/'.$item['product_id'];?>">×</a>
											</td>
											<td class="product-thumbnail">
												<a href="<?php echo base_url().'detail/'.$row['permalink']?>"><img width="180" height="180" alt="LaptopYoga" class="wp-post-image" src="<?php echo base_url().'images/products/'.$row['feature'];?>"></a>
											</td>
											<td data-title="Product" class="product-name">
												<a href="<?php echo base_url().'detail/'.$row['permalink']?>"><?php echo $row['product_name']?></a>
											</td>
											<td data-title="Price" class="product-price">
												<span class="amount"><?php echo $row['sale_price']?></span>
											</td>
											<td class="product-stock-status">
												<span class="in-stock">In stock</span>
											</td>
											<td class="product-add-to-cart">
											   <?php
												echo form_open(base_url()."cart/addcart");
													echo form_hidden('product_id', $item['product_id']);
													echo form_hidden('product_name', $row['product_name']);
													echo form_hidden('sale_price', $row['sale_price']);
													echo form_hidden('quantity', 1);
														$btn = array('add_to_cart'=>array(
																	'class' => 'single_add_to_cart_button button',
																	'value' => 'Add to cart',
																	'name' => 'action')
															
														);
													$user_id = get_uid_by_pid($item['product_id']);
													$group_id = get_group_id_by_uid($user_id);
													if($group_id ==1){
														echo form_submit($btn['add_to_cart']);
													}else{
														?>
														<a style="background: #fed700;color: white;font-weight: bold;" rel="nofollow" href="<?php echo base_url().getSupplierUrl($user_id).'/contact'; ?>" class="checkout-button button alt wc-forward">Contact Supplier</a>
													
													<?php
													}
													
												echo form_close();
											?>
											</td>
										</tr>
										<?php 
										endforeach; 
										?>
										<tr>
											<td class="actions" colspan="6">
												<div class="wc-proceed-to-checkout">

													<a class="checkout-button button alt wc-forward" href="<?php echo base_url().'clear_wishlist';?>">Clear Wishlist</a>
												</div>
											</td>
										</tr>
									</tbody>
									<?php endif; ?>
									</table>
			
						</article>
					</main><!-- #main -->
				</div><!-- #primary -->
			</div><!-- .container -->
		</div><!-- #content -->
	<?php $this->load->view('includes/sub_footer');?>
	</div><!-- #page -->
	<?php $this->load->view('includes/footer');?>
</body>
</html>
