<?php $this->load->view('includes/head');?>
<body class="page page-id-7 page-template-default woocommerce-checkout woocommerce-page woocommerce-order-received wpb-js-composer js-comp-ver-4.12 vc_responsive">
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
	<?php $this->load->view('includes/header1');?>
	<div id="content" class="site-content" tabindex="-1">
		<div class="container">
		<nav class="woocommerce-breadcrumb" ><a href="<?php echo base_url();?>">Home</a><span class="delimiter"><span class="delimiter"><i class="fa fa-angle-right"></i></span>Order Received</nav>
			<div id="primary" class="content-area">
				<main id="main" class="site-main">	
					<article id="post-7" class="post-7 page type-page status-publish hentry">
						<header class="entry-header">
						<?php if(isset($rmsg)){
							echo show_msg($rmsg,'success');}
						?>
						<h1 class="entry-title" itemprop="name">Order Received</h1>
						</header><!-- .entry-header -->
						<div class="entry-content" itemprop="mainContentOfPage">
							<div class="woocommerce">
								<p class="woocommerce-thankyou-order-received">Thank you. Your order has been received.</p>

								<ul class="woocommerce-thankyou-order-details order_details">
								        <li class="date">
										<?php $fullname = $rec_order['first_name'].' '.$rec_order['last_name']; ?>
										Name: <strong><?php echo $fullname; ?></strong>
									</li>
									<li class="date">
										Phone: <strong><?php echo $rec_order['phone']?></strong>
									</li>
									<li class="order">
										Order Number:<strong> <?php echo 'ID'.str_pad($rec_order['order_number'], 6, '0', STR_PAD_LEFT);?></strong>
									</li>
									<li class="date">
										Date: <strong><?php echo $rec_order['created']?></strong>
									</li>
								</ul>
								<div class="clear"></div>
							<h2>Order Details</h2>
							<table class="shop_table order_details">
								<thead>
									<tr>
										<th class="product-name">Product</th>
										<th class="product-total">Total</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$sub_total=0; 
									foreach($rec_product_order as $data){
										
										$color_id =$data['color'];
										$size_id = $data['size'];
										$varian = attributes($data['product_id'],$color_id);
										$per_price = $data['prices'];
										?>
										<tr class="order_item">
											<td class="product-name">
												<?php 
												echo '<a href="#">'.$data['product_name'].'</a> ';
												echo '<strong class="product-quantity">&times; '.$data['qty'].'</strong>';
												echo '<br/>';
												if($color_id){
													echo '<strong>Color: </strong>'.$varian['attr_name'].'<br />';
												}
												if($size_id){
													$size=get_attr_name($size_id);
													echo '<strong>Size: </strong>'.$size['attr_name'].'<br />';
												}
												?>
											</td>
											<?php echo '
											<td class="product-total">
												<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>'.$per_price.'</span>	
											</td>'; ?>
										</tr>
									<?php
										$sub_total += $per_price;
										$pay_id = $data['payment_id'];
									}
										if(empty($rec_order['shipping_company'])){
											$total_ship= 0;
										}else{
											$total_ship = $rec_order['total_shipping'];
										}
									?>
									
								</tbody>
								<tfoot>
									<tr>
										<th scope="row">Subtotal:</th>
										<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span><?php echo $sub_total;?></span></td>
									</tr>
									<tr>
										<th scope="row">Shipping:</th>
										<td>
										<?php 
										if(empty($rec_order['shipping_company'])){
											echo '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">No Shipping</span></span>';
										}else{
											echo '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>'.$total_ship.'</span>&nbsp;<small class="shipped_via">via '.$rec_order['shipping_company'].'</small>';
										}
										?>
										</td>
									</tr>
									
									<tr>
										<th scope="row">Payment Method:</th>
										<td><?php echo payment_method($pay_id);?></td>
									</tr>
									
									<!--START REWARD POINT AND COUPON-->
										<?php 
											$coupon = $rec_order['amount_coupon'];
											$reward_point = $rec_order['amount_reward_point'];
										?>
										
										<?php if(!empty($coupon)){ ?>
											<tr>
												<th scope="row">Coupon:</th>
												<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span><?php echo $coupon;  ?></span></td>
											</tr>
										<?php } ?>
										<?php if(!empty($reward_point)){ ?>  
										<tr>
											<th scope="row">Reward Points:</th>
											<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span><?php echo $reward_point;?></span></td>
										</tr> 
										<?php } ?>
									<!--END REWARD POINT AND COUPON-->
									
									<tr>
										<th scope="row">Total:</th>
										<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span><?php echo $sub_total + $total_ship - $coupon - $reward_point ;?></span></td>
									</tr>  
								</tfoot>
							</table>
							</div>
						</div><!-- .entry-content -->
						<div class="wc-proceed-to-checkout">
							<a class="checkout-button button alt wc-forward" href="<?php echo base_url();?>">Back To Home</a>
							<a class="checkout-button button alt wc-forward" href="<?php echo base_url();?>">Continue Shopping</a>
						</div>
					</article><!-- #post-## -->
				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- .col-full -->
	</div><!-- #content -->
	<?php $this->load->view('includes/contact_footer');?>
	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>
</body>
</html>