<?php $this->load->view('includes/head');?>
<body class="page home page-template-default">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/header1');?>

		<div id="content" class="site-content" tabindex="-1">
			<div class="container">
				<nav class="woocommerce-breadcrumb"><a href="<?php echo base_url(); ?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Checkout</nav>
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<article class="page type-page status-publish hentry">
							<header class="entry-header"><h1 itemprop="name" class="entry-title">Order Review</h1></header><!-- .entry-header -->
							<?php
								$billing_info=$this->session->userdata('billing');
								$first_name=$billing_info['first_name'];
								$last_name=$billing_info['last_name'];
								$email=$billing_info['email'];
								$phone=$billing_info['phone'];
								$address=$billing_info['address'];
								$city=$billing_info['city'];
								$country=$billing_info['country'];
								$zip=$billing_info['postcode'];
								$payment_methods=$this->input->post('payment_method');
								$terms=$this->input->post('terms');
							?>
							
								<form method="post">
									<div class="row">
										<div class="col-sm-7">
											<h3>Billing Details</h3>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-12">
															<label class="control-label"><?php echo 'First Name:';?></label>
															<?php echo $first_name; ?>
				
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-12">
															<label class="control-label"><?php echo 'Last Name:';?></label>
															<?php echo $last_name; ?>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-12">
															<label class="control-label"><?php echo 'Email Address:';?></label>
															<?php echo $email; ?>
															
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-12">
															<label class="control-label"><?php echo 'Phone:';?></label>
															<?php echo $phone; ?>
															
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label class="control-label"><?php echo 'Country:';?></label>
													<?php  
													foreach($ship_country as $data){
														if($country == $data['loc_id']){
															echo $data['location_name'];
														}
													}
													?>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-12">
													<label class="control-label"><?php echo 'Address:';?></label>
													<?php echo $address; ?>
												
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-12">
															<label class="control-label"><?php echo 'City:';?></label>
															<?php 
                                                                                                                    /*	foreach($ship_city as $data){
																if($data['loc_id'] ==$city){
																	echo $data['location_name'];
																}
															}*/
															echo $city;
															?>
														</div>
													</div>
												</div>
												<?php
												if($zip){
													echo '<div class="col-md-6">
														<div class="form-group">
															<div class="col-md-12">
																<label class="control-label">Postcode / ZIP:</label>'.$zip.'
															</div>
														</div>
													</div>';
												}
												?>
											</div>
										</div>
										<div class="col-sm-5" style="background:#f5f5f5;" id="checkout_sidebar">
											<h3 id="order_review_heading">Your order</h3>
											<div class="woocommerce-checkout-review-order" id="order_review">
												<table class="shop_table woocommerce-checkout-review-order-table">
													<thead>
														<tr>
															<th class="product-name">Product</th>
															<th class="product-total">Total</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$buy_now=$this->session->userdata('buy_now');
														if(isset($buy_now)){
															//----varian-
															$product_id=$buy_now['id'];
															$color_id=$buy_now['options']['color'];
															$size_id=$buy_now['options']['size'];
															
															$varian=attributes($product_id,$color_id);
															$items_price=price_item($product_id,$color_id,$size_id,$buy_now['qty']);
															$price=$items_price['price'];
															$item_price=$items_price['price'];
														?>
															<tr class="cart_item">
																<td class="product-name">
																	<?php echo $buy_now['name']?>&nbsp;
																	<strong class="product-quantity">× <?php echo $buy_now['qty'];?></strong>													</td>
																	<?php
																	echo '<br/>';
																	if($color_id){
																		echo '<strong>Color: </strong>'.$varian['attr_name'].'<br />';
																	}
																	if($size_id){
																		$size=get_attr_name($size_id);
																		echo '<strong>Size: </strong>'.$size['attr_name'].'<br />';
																	}
																	?>
																</td>
																<td class="product-total">
																	<span class="amount"><?php echo currency('sign').number_format((float)$price, 2, '.', '');?></span>
																</td>
															</tr>
														<?php
															$sub_total = number_format((float)$price, 2, '.', '');
														}else{
														
															if(has_logged()){
																$cart=show_cart(get_user_id());
																$sub_total=0;
																foreach ($cart as $item):			
																	$row=get_pro_field($item['product_id']);
																	$product_id=$item['product_id'];
																	$total=$item['qty'] * $row['sale_price'];
																	//----varian-
																	$color_id=$item['color'];
																	$size_id=$item['size'];
																	$varian=attributes($product_id,$color_id);
																	if($color_id){
																		$image=$varian['image'];
																	}else{
																		$image=$row['feature'];
																	}
																	$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
																	$price=$items_price['price'];
																	$item_price=$items_price['price'];
																?>
																<tr class="cart_item">
																	<td class="product-name">
																		<?php echo $row['product_name']?>&nbsp;
																		<strong class="product-quantity">× <?php echo $item['qty'];?></strong>	
																		<?php
																		echo '<br/>';
																		if($color_id){
																			echo '<strong>Color: </strong>'.$varian['attr_name'].'<br />';
																		}
																		if($size_id){
																			$size=get_attr_name($size_id);
																			echo '<strong>Size: </strong>'.$size['attr_name'].'<br />';
																		}
																		?>
																	</td>
																	<td class="product-total">
																		<span class="amount"><?php echo currency('sign').number_format((float)$price, 2, '.', '');?></span>
																	</td>
																</tr>
																<?php
																$sub_total +=$price;
																endforeach;
															}else{
																if ($cart = $this->cart->contents()):
																$sub_total=0;
																foreach ($cart as $item):
																	//----varian-
																	$product_id=$item['id'];
																	$row=get_pro_field($product_id);
																	echo form_hidden('cart[' . $item['id'] . '][qty]', $item['qty']);
																	$color_id=$item['options']['color'];
																	$size_id=$item['options']['size'];
																	$varian=attributes($product_id,$color_id);
																	if($color_id){
																		$image=$varian['image'];
																	}else{
																		$image=$row['feature'];
																	}
																	$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
																	$price=$items_price['price'];
																	$item_price=$items_price['price'];
															?>
																<tr class="cart_item">
																	<td class="product-name">
																		<?php echo $item['name']?>&nbsp;
																		<strong class="product-quantity">× <?php echo $item['qty'];?></strong>	
																		<?php
																		if($color_id){
																			echo '<strong>Color: </strong>'.$varian['attr_name'].'<br />';
																		}
																		if($size_id){
																			$size=get_attr_name($size_id);
																			echo '<strong>Size: </strong>'.$size['attr_name'].'<br />';
																		}
																		?>
																	</td>
																	<td class="product-total">
																		<span class="amount"><?php echo currency('sign').number_format((float)$price, 2, '.', '');?></span>
																	</td>
																</tr>
																<?php
																	$sub_total +=$price;
																	endforeach;
																endif;
															}
														}
														?>
													</tbody>
													<tfoot>

														<tr class="cart-subtotal">
															<th>Subtotal</th>
															<td><span class="amount"><?php 
															if(isset($sub_total)){
																echo currency('sign').number_format((float)$sub_total, 2, '.', '');
															}else{ echo '0.00';}
															?></span></td>
														</tr>

														<tr class="shipping">
															<th>Shipping</th>
															<td data-title="Shipping">
																<?php
																if($this->session->userdata('ship_cal')){
																	$ship_data=$this->session->userdata('ship_cal');
																	if(isset($buy_now)){
																		$total_weight=0;
																		$row=get_pro_field($buy_now['id']);
																		$total_weight +=$row['weight'] * $buy_now['qty'];
																	}else{
																		if(has_logged()){
																			$cart=show_cart(get_user_id());
																			$total_weight=0;
																			foreach ($cart as $item):
																				$row=get_pro_field($item['product_id']);
																				$total_weight +=$row['weight'] * $item['qty'];
																			endforeach;
																		}else{
																			if ($cart = $this->cart->contents()):
																				$total_weight=0;
																				foreach ($cart as $item):
																					$row=get_pro_field($item['id']);
																					$total_weight +=$row['weight'] * $item['qty'];
																				endforeach;
																			endif;
																		}
																	}
																	$results=total_ship($total_weight,$ship_data['method_id']);
																	
																    //echo $total_weight;
																	foreach($results as $row){
																		if($total_weight >= $row['min_weight'] && $total_weight <=$row['max_weight']){
																			if($this->session->userdata('parcel_city')){
																				if($this->session->userdata('parcel_city') == 386){
																					echo $ship_data['company_name'].': <span class="amount">'.currency('sign').'0</span>';
																					$fee_amount = 0;
																				}
																				else{
																					echo $ship_data['company_name'].': <span class="amount">'.currency('sign').$row['fee_amount'].'</span>';
																					$fee_amount =$row['fee_amount'];
																				}
																			}
																			else{
																				echo $ship_data['company_name'].': <span class="amount">'.currency('sign').$row['fee_amount'].'</span>';
																				$fee_amount =$row['fee_amount'];
																			}
																			
																		}
																	}
																	if(isset($fee_amount)){
																		$total_ship=$fee_amount;
																	}else{
																		$total_ship=0;
																		echo '<span class="amount">No Shipping</span>';
																	}
																}else{
																	if(has_logged()){
																		$ship_data=getUserShipMethod(get_user_id());
																		$ship_to = getUserShipTo(get_user_id());
																		$getShipM = getShipMethod($ship_data, $ship_to);
																		if(isset($buy_now)){
																			$total_weight=0;
																			$row=get_pro_field($buy_now['id']);
																			$total_weight +=$row['weight'] * $buy_now['qty'];
																		}else{
																			//if(has_logged()){
																				$cart=show_cart(get_user_id());
																				$total_weight=0;
																				foreach ($cart as $item):
																					$row=get_pro_field($item['product_id']);
																					$total_weight +=$row['weight'] * $item['qty'];
																				endforeach;
																			/*}else{
																				$total_weight=0;
																				if ($cart = $this->cart->contents()):
																					
																					foreach ($cart as $item):
																						$row=get_pro_field($item['id']);
																						$total_weight +=$row['weight'] * $item['qty'];
																					endforeach;
																				endif;
																			}*/
																		}
																		
																			$results=total_ship($total_weight,$getShipM);
																		
																		
																		
																		//echo $total_weight;
																		//var_dump($results); exit;
																		//var_dump($total_weight);
																		//echo getUserShipMethod(get_user_id()) . "HEllo!"; exit;
																		//var_dump($total_weight); 
																		//var_dump($ship_data);
																		//var_dump($results); 
																		foreach($results as $row){
																			if($total_weight >= $row['min_weight'] && $total_weight <=$row['max_weight']){
																				//var_dump($this->session->userdata('parcel_city')); exit;
																				//if($this->session->userdata('parcel_city')){
																					if($ship_to == 386){
																						echo getShipCompany($ship_data).': <span class="amount">'.currency('sign').'0</span>';
																						$fee_amount = 0;
																					}
																					else{
																						echo getShipCompany($ship_data).': <span class="amount">'.currency('sign').$row['fee_amount'].'</span>';
																						$fee_amount =$row['fee_amount'];
																					}
																			}
																		}
																	
																		if(isset($fee_amount)){
																			$total_ship=$fee_amount;
																		}else{
																			$total_ship=0;
																			echo '<span class="amount" id="shipping-amount">'.$total_ship.'</span>';
																		}
																		
																																//echo '<span class="amount" id="shipping-amount">No Shipping</span>';
																	}else{
																		echo '<span class="amount" id="shipping-amount">No Shipping</span>';
																	}
																}
																?>
																
															</td>
														</tr>
  
														<!-- STRAT Coupon -->
														<?php
															$total_coupon = 0 ;
															if($this->session->userdata('coupon')){
																$coupons = $this->session->userdata('coupon');
																$total_amount = 0 ;
																if(isset($sub_total)){
																	if(isset($total_ship)){
																		$total_ship=$total_ship;
																	}else{
																		$total_ship=0;
																	}
																	$total_amount = $sub_total+$total_ship;
																}
																$total_coupon = ($total_amount * $coupons['discount'] / 100) + $coupons['amount'];
																
																?>
																	<tr class="coupon">
																		<th>Coupon</th>
																		<td data-title="Soupon" id ="coupon-row">
																			<span class="product_list_widget">
																				<span class="mini_cart_item">
																					<a class="remove remove_coupon" href="#">×</a>
																				</span>
																			</span>
																			<span class="coupon" id="coupon-amount"> 
																				<?php echo $coupons['discount']."%  = ".currency('sign').number_format((float)$total_coupon, 2, '.', ''); ?>
																			</span>
																			<input type="hidden" name="amount_coupon" value="<?php  echo ($total_coupon>0)?number_format((float)$total_coupon, 2, '.', ''):""; ?>">
																		</td>
																	</tr>
																<?php
															}
														?>
														<!-- END Coupon -->
														
														<!-- START POINT-->
														<?php 
															$point_to_amount = round(( $reward->to_amount * $point->reward_point )/$reward->from_point,2);
														?> 
														<?php if($point->reward_point > 0){?>
															<tr>
																<th>
																	Reward Points
																</th>
																<td>
																	<span><?php  echo $point->reward_point;  ?>pt<?php echo ($point->reward_point>0)?" = $".$point_to_amount:'' ?></span>
																</td>
																<input type="hidden" value="<?php echo $point_to_amount?>" name="amount_reward"/>
																<input type="hidden" value="<?php  echo $point->reward_point;  ?>" name="reward_point"/> 
															</tr>  
														<?php }?>

														<!--END POINT-->
														 <tr class="order-total">
															<th>Total</th>
															<td><strong><span class="amount">
															<?php
															if(isset($sub_total)){
																if(isset($total_ship)){
																	$total_ship=$total_ship;
																}else{
																	$total_ship=0;
																}
																$grand_total=($sub_total+$total_ship) - $point_to_amount - $total_coupon;
																echo currency('sign').number_format((float)$grand_total, 2, '.', '');
															}else{ echo '0.00';}
															?>
															</span></strong> 
																<input type="hidden" id="grand_total" value="<?php echo currency('sign').number_format((float)$grand_total, 2, '.', '') ;?>">
																<input type="hidden" name="total_with_coupon" value="<?php echo number_format((float)$grand_total, 2, '.', '');  ?>"/>
															</td>
														</tr>
														   
													</tfoot>
												</table>

												<div class="woocommerce-checkout-payment" id="payment">
													<div class="form-row place-order">
														<input type="button" name="place_order" id="checkout_button" data-value="Place order" value="Confirm Pay" class="button alt">
													</div>
												</div>
											</div>
										</div>
									</div>
									
								</form>
						</article>
					</main><!-- #main -->
				</div><!-- #primary -->
			</div><!-- .container -->
			</div><!-- #content -->
	<?php $this->load->view('includes/sub_footer');?>
	</div><!-- #page -->
	<?php $this->load->view('includes/footer');?>

	<div id="aba_main_modal" class="aba-modal"> 
		<div class="aba-modal-content">

			<!-- Include PHP class -->
			<?php
				define('ABA_PAYWAY_API_URL', 'https://payway.ababank.com/api/akstore/');
				define('ABA_PAYWAY_API_KEY', 'd51891393ade0ca570a6ab26464a24a1');
				define('ABA_PAYWAY_MERCHANT_ID', 'angkorstore');
				class PayWayApiCheckout {
					public static function getHash($transactionId, $amount) {
						$hash = base64_encode(hash_hmac('sha512', ABA_PAYWAY_MERCHANT_ID . $transactionId . $amount, ABA_PAYWAY_API_KEY, true));
						return $hash;
					}
					public static function getApiUrl() {
						return ABA_PAYWAY_API_URL;
					}
				}
				$transactionId = $order_number;
				$amount = $grand_total;
			?>
			<form method="POST" target="aba_webservice" action="<?php echo PayWayApiCheckout::getApiUrl(); ?>" id="aba_merchant_request">
				<input type="hidden" name="hash" value="<?php echo PayWayApiCheckout::getHash($transactionId, $amount); ?>" id="hash"/>
				<input type="hidden" name="tran_id" value="<?php echo $transactionId; ?>" id="tran_id"/>
				<input type="hidden" name="amount" value="<?php echo $amount; ?>" id="amount"/>
				<input type="hidden" name="firstname" value="<?php echo $first_name; ?>"/>
				<input type="hidden" name="lastname" value="<?php echo $last_name; ?>"/>
				<input type="hidden" name="phone" value="<?php echo $phone; ?>"/>
				<input type="hidden" name="email" value="<?php echo $email; ?>"/>
			</form> 
		</div> 
	</div> 
	
</body>
</html>
<script>
	
	$(".remove_coupon").click(function() {
		$.ajax({
			url: base_url+"cart/remove_coupon",
			type: 'POST',
			success: function(msg) {
				location.reload();     
			}
		});
		return false;
	});

</script>