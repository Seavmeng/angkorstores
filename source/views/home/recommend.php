<?php $this->load->view('includes/head');?>

    <body class="left-sidebar">
        <div id="page" class="hfeed site">
			<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
			<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
			<?php $this->load->view('includes/header1');?>
            <div id="content" class="site-content" tabindex="-1">
	<div class="container">

		<nav class="woocommerce-breadcrumb" >
					<a href="<?php echo base_url(); ?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span><span class="woocommerce-breadcrumb"><a href="<?php echo base_url().'category/'.$this->uri->segment(2);?>"><?php echo $cat_title['category_name']; ?></a></span>
				</nav>

		<div id="primary" class="content-area">
			<main id="main" class="site-main">

				<section class="section-product-cards-carousel" >
				<h1 class="page-title">Sorry, this category is not yet available!</h1>
					<header>
						<h2 class="h1">Recommended Products</h2>
						<div class="owl-nav">
							<a href="#products-carousel-prev" data-target="#recommended-product" class="slider-prev"><i class="fa fa-angle-left"></i></a>
							<a href="#products-carousel-next" data-target="#recommended-product" class="slider-next"><i class="fa fa-angle-right"></i></a>
						</div>
					</header>

					<div id="recommended-product">
						<div class="woocommerce columns-4">
							<div class="products owl-carousel products-carousel columns-4 owl-loaded owl-drag">

			<?php foreach($recommend as $item): ?>
			<div class="product">
				<div class="product-outer">
    <div class="product-inner">
        
        <a href="<?php echo base_url().'detail/'.$item['permalink'];?>">
            <h3><?php echo $item['product_name'];?></h3>
            <div class="product-thumbnail">
                <img src="assets/images/blank.gif" data-echo="<?php echo base_url().'images/products/'.$item['feature'];?>" class="img-responsive" alt="">
            </div>
        </a>

        <div class="price-add-to-cart">
            <span class="price">
                <span class="electro-price">
                    <ins><span class="amount"> </span></ins>
                                        <span class="amount"><?php echo currency('sign').$item['sale_price'];?></span>
                </span>
            </span>
			<?php
														echo form_open(base_url()."cart/addcart");
															echo form_hidden('product_id', $item['product_id']);
															echo form_hidden('product_name', $item['product_name']);
															echo form_hidden('sale_price', $item['sale_price']);
															echo form_hidden('quantity', 1);
																$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
															if($item['group_id'] != 2){
																echo form_button($btn['button']);
															}
														//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
														echo form_close();
														?>
            <!--a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a-->
        </div><!-- /.price-add-to-cart -->

        <div class="hover-area">
            <div class="action-buttons">

                <a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																	<a href="<?php echo base_url().'ad_compare/'.$item['product_id'];?>" class="add-to-compare-link"> Compare</a>
            </div>
        </div>
    </div><!-- /.product-inner -->
</div><!-- /.product-outer -->
			</div><!-- /.products -->
<?php endforeach; ?>


			


								</div>
						</div>
					</div>
				</section>

				


			</main><!-- #main -->
		</div><!-- #primary -->

		<div id="sidebar" class="sidebar" role="complementary">
			<aside class="widget woocommerce widget_product_categories electro_widget_product_categories">
    <ul class="category-single">
        <li class="product_cat">
            <ul class="show-all-cat">
                <li class="product_cat"><span class="show-all-cat-dropdown">All Categories</span>
				
		<?php /*
			$allCats = sidebarCategories();
			$allSubCats = sidebarSubCategories();
						
			foreach($allCats as $eachCat ): ?>		
					<div class="list-group panel">
						<a href="#SubMenu1" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu"><?php echo $eachCat['category_name']; ?></a>
							<?php
								foreach($allSubCats as $eachSubCat):
							?>
									<div class="collapse">
										<a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1"><?php echo $eachSubCat['category_name']; ?><i class="fa fa-caret-down"></i></a>
            
									</div>
							<?php 
								endforeach; 
							?>
					</div>
		<?php 					
			endforeach; */
		?>
      
                    <ul class="parent" style="display:block !important;">
					<?php
						$allCats = sidebarCategories();
						$allSubCats = sidebarSubCategories();
						//foreach($recommend as $item):
							foreach($allCats as $eachCat ):
							?>
							<li class="cat-item"><a href="<?php echo base_url().'category/'.$eachCat['category_id']; ?>"><?php echo $eachCat['category_name']; ?></a> <span class="count"><?php echo '('.countItem($eachCat['category_id']).')'; ?></span>
							<?php
								foreach($allSubCats as $eachSubCat):
						?>
							
						<?php 
							if($eachSubCat['parent_id'] == $eachCat['category_id']) {
						?>
								<ul class='children' >
						
								<li class="cat-item"><a href="<?php echo base_url().'category/'.$eachCat['category_id']; ?>"><?php echo $eachSubCat['category_name']; ?></a> <span class="count"><?php echo '('.countItem($eachSubCat['category_id']).')'; ?></span><span class='children'></span></li>
							
								</ul>
						<?php 
							}
								endforeach;
								echo '</li>';
							endforeach;
						//endforeach;?>
                    </ul>
                </li>
            </ul>
            <ul>
			<?php /*
			 
			foreach($recommend as $item):
				$postedCats = postedCat($item['product_id']);
				foreach($postedCats as $eachCat):
				if($item['product_id'] == $eachCat['product_id']){
			?>
					<li class="cat-item current-cat"><a href="product-category.html"><?php echo $eachCat['category_name']; ?></a> <span class="count"><?php echo '('.countItem($item['product_id']).')'; ?></span>
					<!--ul class='children'>
					
                        <li class="cat-item"><a href="product-category.html">Laptops</a> <span class="count">(6)</span></li>
                        
                    </ul-->
                </li>
				<?php } endforeach;
			endforeach;*/
			?>
            </ul>
        </li>
    </ul>
</aside>

			<!--aside class="widget widget_text">
    <div class="textwidget">
        <a href="#">
        <img src="assets/images/banner/ad-banner-sidebar.jpg" alt="Banner"></a>
    </div>
</aside-->
		<aside class="widget widget_products">
			<h3 class="widget-title">Latest Products</h3>
			<ul class="product_list_widget">
			<?php foreach($latestPro as $eachPro) : ?>
				<li>
					<a href="<?php echo base_url(); ?>detail/<?php echo $eachPro['permalink'];?>" title="<?php echo $eachPro['product_name']; ?>">
						<img width="180" height="180" src="<?php echo base_url(); ?>images/products/<?php echo $eachPro['feature'];?>" alt="" class="wp-post-image"/><span class="product-title"><?php echo $eachPro['product_name']; ?></span>
					</a>
					<span class="electro-price"><ins><span class="amount"><?php echo currency('sign').$eachPro['sale_price'];?></span></ins> <!--del><span class="amount">&#36;2,299.00</span></del--></span>
				</li>
			<?php endforeach; ?>
			</ul>
		</aside>	
		</div>

	</div><!-- .container -->
</div><!-- #content -->

        <?php $this->load->view('includes/sub_footer');?>
	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>


    </body>

<!-- Mirrored from transvelo.github.io/electro-html/shop.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Sep 2016 07:10:04 GMT -->
</html>
