<?php $this->load->view('includes/head');
$count_products=count($this->cart->contents());
?>
<body class="page home page-template-default">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/header1');?>
		<div id="content" class="site-content" tabindex="-1">
			<div class="container">
				<nav class="woocommerce-breadcrumb"><a href="home.html">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Cart</nav>
				<?php if(isset($rmsg)){
					echo show_msg($rmsg,'success');}
				?>
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<article class="page type-page status-publish hentry">
							<header class="entry-header"><h1 itemprop="name" class="entry-title">Your Track Order</h1></header><!-- .entry-header -->
							<div class="container">
							  <ul class="timeline">
								<!-- <li><div class="tldate">Apr 2014</div></li> -->
							  <?php
								if(isset ($results)){
								  if(count($results) !=0){
									$i=0; 
									foreach($results as $value){

										if($i % 2 == 0){
										$strAlign = '';
										}
										else{
										$strAlign = 'timeline-inverted';
										}
							  ?>

									  <li class="<?php echo $strAlign;?>">
										<div class="tl-circ"></div>
										<div class="timeline-panel">
										  <div class="tl-heading">
											<h4><?php echo $value['title'];?></h4>
											<p><small class="text-muted"><i class="fa fa-calendar"></i> <?php echo $value['created_date'];?></small></p>
										  </div>
										  <div class="tl-body">
											<p><?php echo $value['description'];?></p>
										  </div>
										</div>
									  </li>
									
							  <?php $i++; } 

								  }else{?>
									<li class="">
										<div class="tl-circ"></div>
										<div class="timeline-panel">
										<div class="tl-heading">
											<h4>No results found for keyword "<b  style="color: red;"><?php echo $this->input->post('orderid');?></b>"</h4>
										</div>
										</div>
									</li>
									<script>window.confirm("No results found for keyword")</script>
									<?php
								  }
								}
							  ?>
							  </ul>
							</div>
						</article>
					</main><!-- #main -->
				</div><!-- #primary -->
			</div><!-- .container -->
			</div><!-- #content -->

	<?php $this->load->view('includes/contact_footer');?>
	</div><!-- #page -->

	<?php $this->load->view('includes/footer');?>

</body>

</html>
