<?php 
$this->load->view('includes/head');
$compare=$this->session->userdata('compare');
?>
<body class="page home page-template-default">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/header1');?>
		<div tabindex="-1" class="site-content" id="content">
			<div class="container">
				<nav class="woocommerce-breadcrumb"><a href="home.html">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Compare</nav>
				<?php if(isset($rmsg)){
					echo show_msg($rmsg,'danger');}
				?>
				<div class="content-area" id="primary">
					<main class="site-main" id="main">
						<article class="post-2917 page type-page status-publish hentry" id="post-2917">
							<div itemprop="mainContentOfPage" class="entry-content">
								<div class="table-responsive">
									<table class="table table-compare compare-list">
										<tbody>
											<tr>
												<th>Product</th>
												<?php 
												if ($compare):
													foreach ($compare as $item):
													$product_id=$item['id'];
													$row=get_pro_field($product_id);
												?>
												<td>
													<a class="product" href="<?php echo base_url().'detail/'.$row['permalink']?>">
														<div class="product-image">
															<div class="">
															<img width="250" height="232" alt="1" class="wp-post-image" src="<?php echo base_url().'images/products/'.$row['feature'];?>">
														</div>
														</div>
														<div class="product-info">
															<h3 class="product-title"><?php echo $row['product_name']?></h3>

														</div>
													</a><!-- /.product -->
												</td>
												<?php 
													endforeach; 
												endif; 
												?>
											</tr>
											<tr>
												<th>Price</th>
												<?php 
												if ($compare):
													foreach ($compare as $item):
													$product_id=$item['id'];
													$row=get_pro_field($product_id);
												?>
												<td>
													<div class="product-price price"><span class="electro-price"><span class="amount"><?php echo $row['sale_price']?></span></span></div>
												</td>
												<?php 
													endforeach; 
												endif; 
												?>
											</tr>
											<tr>
												<th>Availability</th>
												<?php 
												if ($compare):
													foreach ($compare as $item):
													$product_id=$item['id'];
													$row=get_pro_field($product_id);
												?>
												<td><span class="in-stock">In stock</span></td>
												<?php 
													endforeach; 
												endif; 
												?>
											</tr>
											<tr>
												<th>Description</th>
												<?php 
												if ($compare):
													foreach ($compare as $item):
													$product_id=$item['id'];
													$row=get_pro_field($product_id);
												?>
												<td>
													<?php echo $row['description']?>
												</td>
												<?php 
													endforeach; 
												endif; 
												?>
												
											</tr>
											<tr>
												<th>Add to cart</th>
												<?php 
												if ($compare):
													foreach ($compare as $item):
													$product_id=$item['id'];
													$row=get_pro_field($product_id);
												?>
												<td>
													 <?php
														echo form_open(base_url()."cart/addcart");
															echo form_hidden('product_id', $item['id']);
															echo form_hidden('product_name', $row['product_name']);
															echo form_hidden('sale_price', $row['sale_price']);
															echo form_hidden('quantity', 1);
																$btn = array('add_to_cart'=>array(
																			'class' => 'single_add_to_cart_button button',
																			'value' => 'Add to cart',
																			'name' => 'action')
																	
																);
													$user_id = get_uid_by_pid($row['product_id']);
													$group_id = get_group_id_by_uid($user_id);
													if($group_id ==1){
														echo form_submit($btn['add_to_cart']);
													}else{
														?>
														<a style="background: #fed700;color: white;font-weight: bold;" rel="nofollow" href="<?php echo base_url().getSupplierUrl($user_id).'/contact'; ?>" class="checkout-button button alt wc-forward">Contact Supplier</a>
													
													<?php
													}
														echo form_close();
													?>
												</td>
												<?php 
													endforeach; 
												endif; 
												?>
											</tr><!--
											<tr>
												<th>&nbsp;</th>
												<?php 
												/*
												if ($compare):
													foreach ($compare as $item):
													$product_id=$item['id'];
													$row=get_pro_field($product_id);
												?>
												<td class="text-center">
													<a href="<?php echo base_url().'cart/remove_compare/'.$item['id'];?>" title="Remove" class="remove-icon"><i class="fa fa-times"></i></a>
												</td>
												<?php 
													endforeach; 
												endif;
												*/												
												?>
											</tr>--->
											<tr>
											<td class="actions" colspan="6">
												<div class="wc-proceed-to-checkout">

													<a class="checkout-button button alt wc-forward" href="<?php echo base_url().'clear_compare';?>">Clear Compare</a>
												</div>
											</td>
										</tr>
										</tbody>
									</table>
								</div><!-- /.table-responsive -->
							</div><!-- .entry-content -->
						</article><!-- #post-## -->
					</main><!-- #main -->
				</div><!-- #primary -->
			</div><!-- .col-full -->
		</div>
   <?php $this->load->view('includes/sub_footer');?>
	</div>
<?php $this->load->view('includes/footer');?>
</body>
</html>
