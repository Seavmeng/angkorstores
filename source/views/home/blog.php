<?php $this->load->view('includes/head');?>

    <body class="right-sidebar blog-grid">
        <div id="page" class="hfeed site">
            <a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
            <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
			<?php $this->load->view('includes/header1');?>
			<div id="content" class="site-content" tabindex="-1">
				<div class="container">
                    <?php # var_dump($blog_cantegories);?>

					<style type="text/css">
						.article-around{
						    max-height: 78px;
						    overflow: hidden;
						    margin-top: 28px;
						}
						.article-around:hover{
							overflow-y: scroll;
						}
						.aticle-around{
							
							font-size: 13px;
						}
						.aticle-around h2{
							 line-height: 0.9;
							 margin-bottom: 5px;
						}
						.aticle-around h2 a {
							 font-size: 16px;
							 color: #434343;
							 font-weight: bold;

						}
						.aticle-around p {
					       color: #747474;
						}	

					</style>

					
					<nav class="woocommerce-breadcrumb"> <a href="<?= base_url() ?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span> About Cambodia </nav>

					<div class="row"  >
							<?php  foreach($blog_cantegories as $blog_cantegory): ?>
							<?php 

							  $link_To_sub_cat =  $blog_cantegory['count_child'] <= 0 ? '#' : base_url(uri_string()).'?parent_cat='.$blog_cantegory['category_id'];

							  $tilte_cat =  $blog_cantegory['category_name'] ;

							  $description_cat = $blog_cantegory['description'] ;
							  if('' != $description_cat){
								  $extraClass = 'article-around';
								  $newTab ='';
							  }else{
								  $extraClass = '';
								  $newTab = ' target="_blank" ';
							  }
							  

							?>


					        <div class="col-md-4 aticle-around <?php echo $extraClass; ?>"  >

					          <h2><a href="<?=  $link_To_sub_cat ?>" <?= $newTab ?> rel="bookmark"> <?= $tilte_cat ?> </a></h2>
					          <?= $description_cat ?>
					         
					        </div>

					        <?php  endforeach;?>
							
					  </div> <!-- .row -->
								<nav class="electro-advanced-pagination">
									<?php echo $page;?>
								</nav>
				</div><!-- .container -->
			</div><!-- #content -->
	<?php $this->load->view('includes/contact_footer');?>
    </div><!-- #page -->
	<?php $this->load->view('includes/footer');?>
    </body>
</html>
