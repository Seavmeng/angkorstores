<?php 
if(has_logged()){
	$allcart=show_cart(get_user_id());
	$count_products=count($allcart);
}else{
	$allcart=$this->cart->contents();
	$count_products=count($allcart);
}
?>
<ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
	<li class="nav-item dropdown">
		<a href="<?php echo base_url().'cart';?>" class="nav-link" data-toggle="dropdown">
			<i class="fa fa-shopping-cart f5"></i>
			<span class="cart-items-count count"><?php echo $count_products;?></span>
		</a>
		<ul class="dropdown-menu dropdown-menu-mini-cart">
			<li>
				<div class="widget_shopping_cart_content">
					<ul class="cart_list product_list_widget ">
						<?php 
						if(has_logged()){
							$i=0;
							$total=0;
							$weight=0;
							foreach ($allcart as $item):
								$i++;
								$product_id=$item['product_id'];
								$row=get_pro_field($product_id);
								$price=$row['sale_price'] * $item['qty'];
								//----varian-
								$color_id=$item['color'];
								$size_id=$item['size'];
								$varian=attributes($product_id,$color_id);
								if($color_id){
									$image=$varian['image'];
								}else{
									$image=$row['feature'];
								}
								$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
								$price=$items_price['price'];
								$item_price=$items_price['price'];
							?>
								<li class="mini_cart_item">
									<a title="Remove this item" class="remove" href="<?php echo base_url().'cart/remove_cart/'.$item['cart_id'];?>">×</a>
									<a href="<?php echo base_url().'detail/'.$row['permalink']?>">
										<img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="<?php echo base_url().'images/products/'.$image;?>" alt=""><?php echo $row['product_name']?>&nbsp;
									</a>

									<span class="quantity"><?php echo $item['qty'];?> × <span class="amount"><?php echo $item_price;?></span></span>
								</li>
							<?php
							endforeach; 
						}else{
							$total=0;
							if ($cart = $this->cart->contents()): 
								$i=0;
								
								$weight=0;
								foreach ($cart as $item):
								$i++;
								$product_id=$item['id'];
								$row=get_pro_field($product_id);
								echo form_hidden('cart[' . $item['id'] . '][qty]', $item['qty']);
								$price=$item['price'] * $item['qty'];
								//----varian-
								$color_id=$item['options']['color'];
								$size_id=$item['options']['size'];
								$varian=attributes($product_id,$color_id);
								if($color_id){
									$image=$varian['image'];
								}else{
									$image=$row['feature'];
								}
								$items_price=price_item($product_id,$color_id,$size_id,$item['qty']);
								$price=$items_price['price'];
								$item_price=$items_price['price'];
								?>
								<li class="mini_cart_item">
									<a title="Remove this item" data-product_id="<?php echo $product_id;?>" class="remove" href="<?php echo base_url().'cart/remove_cart/'.$item['rowid'];?>">×</a>
									<a href="<?php echo base_url().'detail/'.$row['permalink']?>">
										<img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="<?php echo base_url().'images/products/'.$image;?>" alt=""><?php echo $item['name']?>&nbsp;
									</a>

									<span class="quantity"><?php echo $item['qty'];?> × <span class="amount"><?php echo $item_price;?></span></span>
								</li>
								<?php 
								$total +=$price;
								endforeach; 
							endif;
						}
						?>
					</ul><!-- end product list -->
					<p class="total"><strong>Subtotal:</strong> <span class="amount"><?php echo currency('sign').$total;?></span></p>
					<p class="buttons">
						<a class="button wc-forward" href="<?php echo base_url().'cart';?>">View Cart</a>
						<a class="button checkout wc-forward" href="<?php echo base_url().'checkout';?>">Checkout</a>
					</p>
				</div>
			</li>
		</ul>
	</li>
</ul>
<ul class="navbar-wishlist nav navbar-nav pull-right flip">
	<li class="nav-item">
		<a href="<?php echo base_url().'wishlist';?>" title="Wishlist" class="nav-link"><i class="ec ec-favorites"></i></a>
	</li>
</ul>
<ul class="navbar-compare nav navbar-nav pull-right flip">
	<li class="nav-item">
		<a href="<?php echo base_url().'compare';?>" class="nav-link" title="Compare"><i class="ec ec-compare"></i></a>
	</li>
</ul>