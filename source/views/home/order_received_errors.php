<?php $this->load->view('includes/head');?>
<body class="page page-id-7 page-template-default woocommerce-checkout woocommerce-page woocommerce-order-received wpb-js-composer js-comp-ver-4.12 vc_responsive">
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
	<?php $this->load->view('includes/header1');?>
	<div id="content" class="site-content" tabindex="-1">
		<div class="container">
		<nav class="woocommerce-breadcrumb" ><a href="<?php echo base_url();?>">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span><a href="<?php echo base_url().'checkout';?>">Checkout</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Order Received</nav>
			<div id="primary" class="content-area">
				<main id="main" class="site-main">	
					<article id="post-7" class="post-7 page type-page status-publish hentry">
						<header class="entry-header">
						<h2 class="entry-title" itemprop="name">Your order was not successful</h2>
						</header><!-- .entry-header -->
                                                <div class="alert alert-danger" role="alert">
						<?php if(isset($rmsg)){
							echo 'AN ERROR OCCURRED IN THE PROCESS OF PAYMENT :';
							echo show_msg($rmsg,'error');}
						?>
						</div>
						<p class="woocommerce-thankyou-order-received">Please go to <a href="<?php echo base_url();?>">shopping</a> again</p>
					</article><!-- #post-## -->
				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- .col-full -->
	</div><!-- #content -->
	<?php $this->load->view('includes/contact_footer');?>
	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>
</body>
</html>