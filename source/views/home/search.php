<?php $this->load->view('includes/head');?>

    <body class="left-sidebar">
        <div id="page" class="hfeed site">
			<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
			<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
			<?php $this->load->view('includes/header1');?>
            <div id="content" class="site-content" tabindex="-1">
	<div class="container">

		<nav class="woocommerce-breadcrumb" ><a href="home.html">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Smart Phones &amp; Tablets</nav>

		<div id="primary" class="content-area">
			<main id="main" class="site-main">

				<header class="page-header">
				   <h1 class="page-title">Search Results: “<?php echo $searchFor;?>”</h1>
				   <p class="woocommerce-result-count">
					  <?php echo $showing; ?>
				   </p>
				</header>
				<div class="shop-control-bar">
				   <ul class="shop-view-switcher nav nav-tabs" role="tablist">
					  <li class="nav-item"><a class="nav-link active" data-toggle="tab" title="Grid View" href="#grid"><i class="fa fa-th"></i></a></li>
					  <li class="nav-item"><a class="nav-link " data-toggle="tab" title="Grid Extended View" href="#grid-extended"><i class="fa fa-align-justify"></i></a></li>
					  <li class="nav-item"><a class="nav-link " data-toggle="tab" title="List View" href="#list-view"><i class="fa fa-list"></i></a></li>
					  <li class="nav-item"><a class="nav-link " data-toggle="tab" title="List View Small" href="#list-view-small"><i class="fa fa-th-list"></i></a></li>
				   </ul>
				   <form class="woocommerce-ordering" method="get">
					  <select name="orderby" class="orderby">
						 <option value="menu_order" selected="selected">Default sorting</option>
						 <option value="popularity">Sort by popularity</option>
						 <option value="rating">Sort by average rating</option>
						 <option value="date">Sort by newness</option>
						 <option value="price">Sort by price: low to high</option>
						 <option value="price-desc">Sort by price: high to low</option>
					  </select>
					  <input type="hidden" name="s" value="Purple NX Mini F1 aparat  SMART NX"><input type="hidden" name="product_cat" value="0"><input type="hidden" name="post_type" value="product">
				   </form>
				   <nav class="electro-advanced-pagination">
								<?php echo $page;?>
					</nav>
				</div>
				<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="grid" aria-expanded="true">
								<ul class="products columns-3">
								<?php 
								$count = 0;
								foreach($results as $item): 
								
								if($count % 3 === 0){
									$first = "first";
									} 
									
									elseif($count % 3 ===2){
										$first="last";
									}
									else{
										$first="";
									}
									
								//if($item['status'] == 1){	
								?>
										<li class="product <?php echo $first;?>">
											<div class="product-outer">
												<div class="product-inner">
													<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>">
														<h3><?php echo $item['product_name'];?></h3>
														<div class="product-thumbnail">

															<!--img data-echo="assets/images/products/1.jpg" src="assets/images/blank.gif" alt=""-->
															<img data-echo="<?php echo base_url()."images/products/".$item['feature'];?>" src="<?php echo base_url()."assets/images/blank.gif";?>" alt="">
														</div>
													</a>

													<div class="price-add-to-cart">
														<?php
														echo '<span class="price">
															<span class="electro-price">';
														$price_item=price_pro_dis($item['product_id']);
														if(!empty($price_item['discount'])){
															echo 
															'<ins><span class="amount">'.currency('sign'). number_format((float)$price_item['price_sale'], 2, '.', '') .'</span></ins>
															<del><span class="amount">'.currency('sign'). number_format((float)$price_item['price_dis'], 2, '.', '') .'</span></del>';
														}else{
															echo '<ins><span class="amount"> </span></ins>
																<span class="amount">'.currency('sign'). number_format((float)$price_item['price_sale'], 2, '.', '') .'</span>';
														}
														echo '</span>
														</span>';
														echo form_open(base_url()."cart/addcart");
															echo form_hidden('product_id', $item['product_id']);
															echo form_hidden('product_name', $item['product_name']);
															echo form_hidden('sale_price', $item['sale_price']);
															echo form_hidden('quantity', 1);
																$btn = array('input'=>array(
																			'class' => 'product_type_simple add_to_cart_button ajax_add_to_cart',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
															$page_name = get_user_page($item['user_id']);
															if($item['group_id'] != 2){
																if($item['contact-type']==1){ ?>
																	<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().'p/contact-us'; ?>" class="fa fa-phone fa-1x"></a>
																<?php }else{ 
																	//echo form_button($btn['button']);
																}
															}else{  												
															?>
																<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().$page_name.'/contact'; ?>" class="fa fa-phone fa-1x"></a>
															<?php }
															
														//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
														echo form_close();
														?>
													</div><!-- /.price-add-to-cart -->

													<div class="hover-area">
														<div class="action-buttons">

															<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																	<a href="<?php echo base_url().'ad_compare/'.$item['product_id'];?>" class="add-to-compare-link"> Compare</a>
														</div>
													</div>
												</div><!-- /.product-inner -->
											</div><!-- /.product-outer -->
										</li>
										<?php //}
										$count++;
										endforeach; ?>
								</ul>
							</div>					
							<div role="tabpanel" class="tab-pane" id="grid-extended" aria-expanded="true">
								<ul class="products columns-3">
								<?php 
								$count=0;
								foreach($results as $item):
								if($count % 3 === 0){
									$first = "first";
								}
								elseif($count % 3 === 2){
									$first = "last";
								}
								else{
									$first = "";
								}
								//if($item['status'] == 1){
								?>
														<li class="product <?php echo $first;?>">
											<div class="product-outer">
												<div class="product-inner">
													
													<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>">
														<h3><?php echo $item['product_name'];?></h3>
														<div class="product-thumbnail">
															<img class="wp-post-image" data-echo="<?php echo base_url(). "images/products/". $item['feature'];?>" src="<?php echo base_url()."assets/images/blank.gif";?>" alt="">
														</div>

														

														<div class="product-short-description">
														<?php echo $item['description']; ?>
															<!--ul>
																<li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
																<li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
																<li><span class="a-list-item">Flash storage</span></li>
																<li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
																<li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
															</ul-->
														</div>

														<div class="product-sku">SKU: <?php if ($item['product_code']==''){echo $item['product_id'];}else{echo $item['product_code'];}?></div>
													</a>
													<div class="price-add-to-cart">
														<span class="price">
															<span class="electro-price">
																<ins><span class="amount"><?php echo currency('sign').$item['sale_price'];?></span></ins>
																
															</span>
														</span>
														<?php
														echo form_open(base_url()."cart/addcart");
															echo form_hidden('product_id', $item['product_id']);
															echo form_hidden('product_name', $item['product_name']);
															echo form_hidden('sale_price', $item['sale_price']);
															echo form_hidden('quantity', 1);
																$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
															$page_name = get_user_page($item['user_id']);
															if($item['group_id'] != 2){
																if($item['contact-type']==1){ ?>
																	<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().'p/contact-us'; ?>" class="fa fa-phone fa-1x"></a>
																<?php }else{ 
																	echo form_button($btn['button']);
																}
															}else{  												
															?>
																<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().$page_name.'/contact'; ?>" class="fa fa-phone fa-1x"></a>
															<?php }
														//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
														echo form_close();
														?>
													</div><!-- /.price-add-to-cart -->
													<div class="hover-area">
														<div class="action-buttons">

															<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																	<a href="<?php echo base_url().'ad_compare/'.$item['product_id'];?>" class="add-to-compare-link"> Compare</a>
														</div>
													</div>
												</div><!-- /.product-inner -->
											</div><!-- /.product-outer -->
										</li>
										<?php //}
										$count++;
										endforeach; ?>
										</ul>
							</div>					
							<div role="tabpanel" class="tab-pane" id="list-view" aria-expanded="true">

								<ul class="products columns-3">
								<?php foreach($results as $item): 
								//if($item['status'] == 1){
								?> 
												<li class="product list-view">
											<div class="media">
												<div class="media-left">
													<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>">
														<img class="wp-post-image" data-echo="<?php echo base_url() . "images/products/" . $item['feature'];?> " src="<?php echo base_url()."assets/images/blank.gif";?>" alt="">
													</a>
												</div>
												<div class="media-body media-middle">
													<div class="row">
														<div class="col-xs-12">
															<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>"><h3><?php echo $item['product_name'];?></h3>
																
																<div class="product-short-description">
																	<?php echo $item['description']; ?>
																</div>
															</a>
														</div>
														<div class="col-xs-12">

															<div class="availability in-stock">
																Availablity: <span>In stock</span>
															</div>


															<span class="price"><span class="electro-price"><span class="amount"><?php echo currency('sign').$item['sale_price'];?></span></span></span>
															<?php
														echo form_open(base_url()."cart/addcart");
															echo form_hidden('product_id', $item['product_id']);
															echo form_hidden('product_name', $item['product_name']);
															echo form_hidden('sale_price', $item['sale_price']);
															echo form_hidden('quantity', 1);
																$btn = array('input'=>array(
																			'class' => 'product_type_simple add_to_cart_button ajax_add_to_cart',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
															$page_name = get_user_page($item['user_id']);
															if($item['group_id'] != 2){
																if($item['contact-type']==1){ ?>
																	<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().'p/contact-us'; ?>" class="fa fa-phone fa-1x"></a>
																<?php }else{ 
																	echo form_button($btn['button']);
																}
															}else{  												
															?>
																<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().$page_name.'/contact'; ?>" class="fa fa-phone fa-1x"></a>
															<?php }
														//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
														echo form_close();
														?>
															
															<div class="hover-area">
																<div class="action-buttons">
																	<div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
																		<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

																		<div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
																			<span class="feedback">Product added!</span>
																			<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																	
																		</div>

																		<div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
																			<span class="feedback">The product is already in the wishlist!</span>
																			<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																		</div>

																		<div style="clear:both"></div>
																		<div class="yith-wcwl-wishlistaddresponse"></div>

																	</div>
																	<div class="clear"></div>
																	<a href="<?php echo base_url().'ad_compare/'.$item['product_id'];?>" class="add-to-compare-link"> Compare</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
								<?php //} 
								endforeach; ?>
								
										</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="list-view-small" aria-expanded="true">
								<ul class="products columns-3">
								<?php foreach($results as $item):
								// if($item['status'] == 1){
								 ?>
												<li class="product list-view list-view-small">
											<div class="media">
												<div class="media-left">
													<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>">
														<img class="wp-post-image" data-echo="<?php echo base_url() . "images/products/" . $item['feature'];?>" src="<?php echo base_url()."assets/images/blank.gif";?>" alt="">
													</a>
												</div>
												<div class="media-body media-middle">
													<div class="row">
														<div class="col-xs-12">
															<a href="<?php echo base_url() . 'detail/'. $item['permalink']; ?>"><h3><?php echo $item['product_name'];?></h3>
																
															</a><div class="product-short-description">
																	<?php echo $item['description']; ?>
																</div>
																
														</div>
														<div class="col-xs-12">
															<div class="price-add-to-cart">
																<span class="price"><span class="electro-price"><span class="amount"><?php echo currency('sign').$item['sale_price'];?></span></span></span>
																<?php
														echo form_open(base_url()."cart/addcart");
															echo form_hidden('product_id', $item['product_id']);
															echo form_hidden('product_name', $item['product_name']);
															echo form_hidden('sale_price', $item['sale_price']);
															echo form_hidden('quantity', 1);
																$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
															$page_name = get_user_page($item['user_id']);
															if($item['group_id'] != 2){
																if($item['contact-type']==1){ ?>
																	<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().'p/contact-us'; ?>" class="fa fa-phone fa-1x"></a>
																<?php }else{ 
																	echo form_button($btn['button']);
																}
															}else{  												
															?>
																<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().$page_name.'/contact'; ?>" class="fa fa-phone fa-1x"></a>
															<?php }
														//	echo '<input type="submit" value="Buy" class="btn btn-link button-radius btn-add-cart teal"/> <span class="icon"></span>';
														echo form_close();
														?>
															</div><!-- /.price-add-to-cart -->
															<div class="hover-area">
																<div class="action-buttons">

																	<a href="<?php echo base_url().'ad_wishlist/'.$item['product_id'];?>" rel="nofollow" class="add_to_wishlist"> Wishlist</a>
																	<a href="<?php echo base_url().'ad_compare/'.$item['product_id'];?>" class="add-to-compare-link"> Compare</a>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</li>
										<?php //} 
										endforeach;?> 
										</ul>
							</div>
						</div>
				<div class="shop-control-bar-bottom">
				   <p class="woocommerce-result-count">
					  <?php echo $showing; ?>
				   	</p>
				   	<div class="pull-right">
						<?php echo $page;?>
					</div>
				</div>


			</main><!-- #main -->
		</div><!-- #primary -->

		<div id="sidebar" class="sidebar" role="complementary">
			<aside class="widget woocommerce widget_product_categories electro_widget_product_categories">
    <ul class="category-single">
        <li class="product_cat">
            <ul class="show-all-cat">
                <li class="product_cat"><span class="show-all-cat-dropdown">All Categories</span>
				
		<?php /*
			$allCats = sidebarCategories();
			$allSubCats = sidebarSubCategories();
						
			foreach($allCats as $eachCat ): ?>		
					<div class="list-group panel">
						<a href="#SubMenu1" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu"><?php echo $eachCat['category_name']; ?></a>
							<?php
								foreach($allSubCats as $eachSubCat):
							?>
									<div class="collapse">
										<a href="#SubMenu1" class="list-group-item" data-toggle="collapse" data-parent="#SubMenu1"><?php echo $eachSubCat['category_name']; ?><i class="fa fa-caret-down"></i></a>
            
									</div>
							<?php 
								endforeach; 
							?>
					</div>
		<?php 					
			endforeach; */
		?>
      
                    <ul class="parent" style="display:block !important;">
					<?php
						$allCats = sidebarCategories();
						$allSubCats = sidebarSubCategories();
						//foreach($recommend as $item):
							foreach($allCats as $eachCat ):
							?>
							<li class="cat-item"><a href="<?php echo base_url().'category/'.$eachCat['category_id']; ?>"><?php echo $eachCat['category_name']; ?></a> <span class="count"><?php echo '('.countItem($eachCat['category_id']).')'; ?></span>
							<?php
								foreach($allSubCats as $eachSubCat):
						?>
							
						<?php 
							if($eachSubCat['parent_id'] == $eachCat['category_id']) {
						?>
								<ul class='children' >
						
								<li class="cat-item"><a href="<?php echo base_url().'category/'.$eachCat['category_id']; ?>"><?php echo $eachSubCat['category_name']; ?></a> <span class="count"><?php echo '('.countItem($eachSubCat['category_id']).')'; ?></span><span class='children'></span></li>
							
								</ul>
						<?php 
							}
								endforeach;
								echo '</li>';
							endforeach;
						//endforeach;?>
                    </ul>
                </li>
            </ul>
            <ul>
			<?php /*
			 
			foreach($recommend as $item):
				$postedCats = postedCat($item['product_id']);
				foreach($postedCats as $eachCat):
				if($item['product_id'] == $eachCat['product_id']){
			?>
					<li class="cat-item current-cat"><a href="product-category.html"><?php echo $eachCat['category_name']; ?></a> <span class="count"><?php echo '('.countItem($item['product_id']).')'; ?></span>
					<!--ul class='children'>
					
                        <li class="cat-item"><a href="product-category.html">Laptops</a> <span class="count">(6)</span></li>
                        
                    </ul-->
                </li>
				<?php } endforeach;
			endforeach;*/
			?>
            </ul>
        </li>
    </ul>
</aside>

			<!--aside class="widget widget_text">
    <div class="textwidget">
        <a href="#">
        <img src="assets/images/banner/ad-banner-sidebar.jpg" alt="Banner"></a>
    </div>
</aside-->	
		</div>

	</div><!-- .container -->
</div><!-- #content -->

        <?php $this->load->view('includes/contact_footer');?>
	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>


    </body>

<!-- Mirrored from transvelo.github.io/electro-html/shop.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Sep 2016 07:10:04 GMT -->
</html>
