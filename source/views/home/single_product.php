<?php $this->load->view('includes/head');?>
<?php
 	/// check if the product if out of stock it will redirect to home page
	/* if(get_stock_status($data_['product_id']) != 'In Stock') {
		redirect(base_url());
	}    */
?>
<body class="single-product left-sidebar">
	<div id="page" class="hfeed site">
		<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
		<?php $this->load->view('includes/header1');

		?>
		
		<div id="content" class="site-content" tabindex="-1">
		
			<div class="row">
			<!--ads-->
				<div class="col-md-2 ads">  
					<br/>
					<br/>
					<br/>
						 <?php
							foreach ($banner as $side_left) {
								if($side_left->show_on=="Product Details" && $side_left->status==1 && $side_left->position == "Fly Side Left"){
						?> 
							<a    href="<?php echo $side_left->link ; ?>" target="_blank">
								<img   class="fly-side-left" src="<?php echo base_url().'images/banner/'.$side_left->images ?>" />
							</a>
						<?php
								}  
							} 
						 ?>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-8">
				<nav class="woocommerce-breadcrumb">
					<a href="<?php echo base_url(); ?>">Home</a>
					<span class="delimiter"><i class="fa fa-angle-right"></i></span>
					<span class="woocommerce-breadcrumb">
						<a href="<?php echo base_url().'category/'.$data_['category_id']; ?>"><?php echo getCatTitle($data_['product_id']); ?></a>
					</span>
					<span class="delimiter"><i class="fa fa-angle-right"></i>
					</span>
					<span class="woocommerce-breadcrumb">
						<a href="<?php echo base_url().'detail/'.$this->uri->segment(2); ?>"><?php echo $data_['product_name']; ?></a>
					</span>
				</nav><!-- /.woocommerce-breadcrumb -->
				<?php if($get_group['group_id'] == 1){ ?>
					<div id="primary" class="content-area">
						<main id="main" class="site-main">
							<div class="product">
								<div class="single-product-wrapper">
									<div class="product-images-wrapper">
										<span class="onsale">Sale!</span>
										 <div class="images electro-gallery">
											<div class="thumbnails-single owl-carousel">
												<!-- <div class="img_size"> -->
												<?php //foreach($data_ as $data){ 
												//var_dump($data_);
												?>
												<?php //var_dump($gall_['image']); 
													$thumbArray = explode(",", $gall_['image']);
													//var_dump($thumbArray);
													//echo count($thumbArray); exit;
													if($thumbArray[0]==''){ 
													//echo count($thumbArray); exit;
													?>
												<div class="img-size">
													<?php 
												if(!empty($data_['feature'])){ ?>
															<a href="<?php echo base_url().'images/products/'.bigthumb($data_['feature']); ?>" class="zoom" title="" data-rel="prettyPhoto[product-gallery]" >
															<!--img src="assets/images/blank.gif" data-echo="assets/images/single-product/s1-1.jpg" class="wp-post-image" alt=""-->
															<img id="zoom-0" src="<?php echo base_url().'images/products/'.bigthumb($data_['feature']); ?>" data-echo="<?php echo base_url().'images/products/'.bigthumb($data_['feature']); ?>" class="img-gall feature" data-zoom-image="<?php echo base_url().'images/products/'.bigthumb($data_['feature']); ?>" alt="">
															</a>
												<?php }else{ ?>
															<a href="#" class="zoom" title="">
															<!--img src="assets/images/blank.gif" data-echo="assets/images/single-product/s1-1.jpg" class="wp-post-image" alt=""-->
															<img id="no_img" src="<?php echo base_url().'images/no_img.gif'; ?>" class="img-gall feature" alt="">
															</a>
												<?php		} ?>
												</div>
												<?php
													}else{ 
													$i=0;
													foreach($thumbArray as $thumb){
													if ($i==0){
														$first = 'zoom-';
													}else{
														$first = '';
													}	
												?>
												<div class="img-size">
													<a href="<?php echo base_url().'images/products/'.bigthumb($thumb); ?>" class="zoom" title="" data-rel="prettyPhoto[product-gallery]" >
														
														<img id="zoom-0" width="450" height="450" src="assets/images/blank.gif" data-echo="<?php echo base_url().'images/products/'.bigthumb($thumb); ?>" class="img-gall feature" data-zoom-image="<?php echo base_url().'images/products/'.bigthumb($thumb); ?>" alt="">
													</a>
												</div>
												<?php $i++; } ?>
												<!-- </div> -->
											</div><!-- .thumbnails-single -->
											<div id="gallery_01" class="thumbnails-all columns-5 owl-carousel">
												<!--a href="<?php echo base_url().'images/products/'.bigthumb($data_['feature']); ?>" class="first" title="">
												
													<img src="assets/images/blank.gif" data-echo="<?php echo base_url().'images/products/'.$data_['feature']; ?>" class="" alt="">
												</a-->
												<?php  
													$i=0;
													//var_dump($thumbArray);
													foreach($thumbArray as $thumb){
													if ($i==0){
														$first = 'first';
													}	
													else{
														$first = '';
													}
												?>
													<a style="width:50px;height:50px;" href="#" data-image="<?php echo base_url().'images/products/'.bigthumb($thumb); ?>" data-zoom-image="<?php echo base_url().'images/products/'.bigthumb($thumb); ?>" class="<?php echo $first;?>" title="">
														<img id="zoom-0" src="<?php var_dump($thumb);echo base_url().'images/products/'. $thumb; ?>" data-echo="<?php echo base_url().'images/products/'. $thumb; ?>" alt="">
													</a>
													<?php } } ?>
											</div><!-- .thumbnails-all -->
										</div><!-- .electro-gallery -->	
									</div><!-- /.product-images-wrapper -->
									
									<div class="summary entry-summary">
										<!--span class="loop-product-categories">
											<a href="product-category.html" rel="tag">Headphones</a>
										</span><!-- /.loop-product-categories -->

										<h1 itemprop="name" class="product_title entry-title"><?php echo $data_['product_name'];?></h1>
										<div class="brand">
											<a href="product-category.html">
												<h3><?php echo $data_['brand'];?></h3>
											</a>
										</div><!-- .brand -->

										<div class="availability in-stock">
											Availablity: <span><?php echo get_stock_status($data_['product_id']); ?></span>
										</div><!-- .availability -->

										<hr class="single-product-title-divider" />

										<div class="action-buttons">
											<a href="<?php echo base_url().'ad_wishlist/'.$data_['product_id'];?>" class="add_to_wishlist" > Wishlist</a>
											<a href="<?php echo base_url().'ad_compare/'.$data_['product_id'];?>" class="add-to-compare-link" data-product_id="2452"> Compare</a>
										</div><!-- .action-buttons -->
										<div itemprop="description">
											<?php echo str_replace('\n',"<br/>",$data_['description']); ?>
											<p><strong>SKU</strong>: <?php if ($data_['product_code']==''){echo $data_['product_id'];}else{echo $data_['product_code'];}?></p>
											<p><strong>Call</strong>: <?php echo ' +855 23 969 278';?></p>
											
										</div>
									<?php
									if(($data_['contact-type']!= 1)){
										echo '<span class="msg_varian"></span>';
										$has_color=$check_varians['color'];
										if($has_color){
											echo '<input type="hidden" name="color" id="hidden_color">';
										?>
											<div class="detail_width">
												<div class="detail_left"><strong>Color</strong>:</div>
												<div class="detail_right">
													<div class="varian">
														<?php
														foreach($varians as $row){
															$color=get_attr_name($row['color']);
															if(!empty($row['color'])){
															//	echo '<a href="'.base_url().'detail/'.$data_['permalink'].'/'.$row['color'].'">';
																echo '<img align="left" class="color" pro_id="'.$data_['product_id'].'" href="'.$row['color'].'" value="'.$row['color'].'" src="'.base_url().'images/products/'.$row['image'].'" alt="'.$color['attr_name'].'">';
															//	echo '</a>';
															}
														}
														?>
														<div class="clear"></div>
													</div>
												</div>
												<div class="clear"></div>
											</div><br/>
										<?php
										}
										$has_size=$check_varians['size'];
										if($has_size){
											echo '<input type="hidden" name="size" id="hidden_size">';
										?>
										<div class="detail_width" id="show_color_size">
											<div class="detail_left"><strong>Size</strong>:</div>
											<div class="detail_right">
												<div class="varian">
													<?php
													$varians_sizes=explode(',',$varians_size);
													foreach($varians_sizes as $row){
														$size=get_attr_name($row);
														if(!empty($row)){
															echo '<a href="javascript:void(0)" class="size" value="'.$row.'">'.$size['attr_name'].'</a>';
														}
													}
													?>
													<div class="clear"></div>
												</div>
											</div>
											<div class="clear"></div>
										</div><br/>
										<?php
										}
										echo '<div id="price_varian"></div>';
										echo '<div id="original_price_varian">';
										$price_item=price_pro_dis($data_['product_id']);
										if(!empty($price_item['discount'])){
											echo '
												<div class="detail_width">
													<div class="detail_left"><strong>Price</strong>:</div>
													<div class="detail_right price">
														<del><span class="amount">'.currency('sign'). number_format((float)$price_item['price_dis'], 2, '.', '') .'</span></del>					  
													</div>
													<div class="clear"></div>
												</div>';
											echo '
											<div class="detail_width">
												<div class="detail_left"><strong>Discount</strong>:</div>
												<div class="detail_right price">
													<ins>
														<span class="amount">'.currency('sign'). number_format((float)$price_item['price_sale'], 2, '.', '') .'</span>
													</ins> 
														<span class="discount_amount">-'.$price_item['disount_percent'].'% Off</span>
														<span class="countdown">0</span>
												</div>
												<div class="clear"></div>
											</div>';
											
										}else{
											echo '
											<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
												<p class="price"><span class="electro-price"><ins><span class="amount">&#36;'.number_format((float)$data_['sale_price'], 2, '.', '').'</span></ins></span></p>
											</div>';
										}
										echo '</div>';										
										echo form_open(base_url()."cart/addcart",array('class' => 'variations_form cart'));
										?>
										
										<input type="hidden" class="time-year" value="<?= $price_item['year'] ?>" />
										<input type="hidden" class="time-month" value="<?= $price_item['month'] ?>" />
										<input type="hidden" class="time-day" value="<?= $price_item['day'] ?>" />
										<input type="hidden" class="time-hour" value="<?= $price_item['hour'] ?>" />
										<input type="hidden" class="time-minute" value="<?= $price_item['minute'] ?>" />
										<input type="hidden" class="time-second" value="<?= $price_item['second'] ?>" />
										<input type="hidden" class="discount-value" value="<?= $price_item['discount']; ?>" />
										
										<!-- Buy Now -->
										<div id="loading" style="position:relative;top:50%;left:50%;display:none;">
											<img src="<?php echo base_url();?>images/loader.gif">
										</div>
										
										<div class="single_variation_wrap">
											<div class="woocommerce-variation single_variation"></div>
											<div class="woocommerce-variation-add-to-cart variations_button">
												<?php
												
												echo form_open(base_url()."cart/buynow");
													echo '
													<div class="quantity">
														<label>Quantity:</label>
														<input type="number" id="quantity" name="quantity" min="1" max="30" value="1" title="Qty" class="input-text qty text"/>
													</div><br><br>';
													echo form_input(array("name"=>"product_id",'type'=>'hidden',"id"=>"product_id","value"=>$data_['product_id']));
													echo form_input(array("name"=>"product_name",'type'=>'hidden',"id"=>"product_name","value"=>$data_['product_name']));
													echo form_input(array("name"=>"sale_price",'type'=>'hidden',"id"=>"sale_price","value"=>$data_['sale_price']));
													$btn = array('add_to_cart'=>array(
																'class' => 'single_add_to_cart_button button',
																'value' => 'Add to cart',
																'id' => 'add_to_cart',
																'name' => 'action'),
														'buy_now'=>array(
																'class' => 'single_add_to_cart_button button',
																'value' => 'Buy now',
																'id' => 'buy_now',
																'name' => 'action_buy')
														
													);
													$addcart_botton="<input type='button' class='single_add_to_cart_button button' value='Add to cart' id='add_to_cart' name='action' style='background:#fed700;' />";
													$buy_now_botton="<input type='button' class='single_add_to_cart_button button' value='Buy now' id='buy_now' name='action_buy' style='background:#fed700;'/>";
													if(get_stock_status($data_['product_id']) == 'In Stock') 
													{  
														echo $addcart_botton;
														echo $buy_now_botton;
													}
												?>
											</div>
										</div>
										<?php
											echo form_close();
									}
									else{
										echo form_open(base_url().'p/contact-us');
									?>
									<div class="single_variation_wrap">
										<div class="woocommerce-variation-add-to-cart variations_button">
											<?php
												$btn = array('find_product_by_user'=>array(
													'class' => 'single_add_to_cart_button button',
													'value' => 'Contact Now',
													'name'  => 'action')
												);
												echo form_submit($btn['find_product_by_user']);
											?>
										</div>
									</div>
									<?php
										echo form_close();
									}
								?>
									</div><!-- .summary -->
								</div><!-- /.single-product-wrapper -->
							</div><!-- /.product -->
						</main><!-- /.site-main -->
					</div><!-- /.content-area -->
					<!--Sidebar Start-->
					<div id="sidebar" class="sidebar" role="complementary">
					
					
					<!-- ads position 1-->
					<div class="ads">   
					
					
						<?php
								foreach ($banner as $position1) {
								if($position1->show_on=="Product Details" && $position1->status==1 && $position1->position == "Position 1"){
						?> 
							<a   href="<?php echo $position1->link ; ?>" target="_blank">
								<img     src="<?php echo base_url().'images/banner/'.$position1->images ?>" />
							</a>
						<?php
								}  
							} 
						 ?>  
					 </div>
						<aside class="widget woocommerce widget_product_categories electro_widget_product_categories ads">
							<ul class="category-single">
								<li class="product_cat">
									<ul class="show-all-cat">
										<li class="product_cat"><span class="show-all-cat-dropdown">All Categories</span>
											<ul class="parent" style="display:block !important;">
												<?php
													$allCats = sidebarCategories();
													$allSubCats = sidebarSubCategories();												
													//foreach($recommend as $item):
														foreach($allCats as $eachCat ):
														?>
														<li class="cat-item"><a href="<?php echo base_url().'category/'.$eachCat['category_id']; ?>"><?php echo $eachCat['category_name']; ?></a> <span class="count"><?php echo '('.countItem($eachCat['category_id']).')'; ?></span>
														<?php
															foreach($allSubCats as $eachSubCat):
													?>
														
													<?php 
														if($eachSubCat['parent_id'] == $eachCat['category_id']) {
													?>
															<ul class='children' >
													
															<li class="cat-item"><a href="<?php echo base_url().'category/'.$eachCat['category_id']; ?>"><?php echo $eachSubCat['category_name']; ?></a> <span class="count"><?php echo '('.countItem($eachSubCat['category_id']).')'; ?></span><span class='children'></span></li>
														
															</ul>
													<?php 
														}
															endforeach;
															echo '</li>';
														endforeach;
													//endforeach;?>
											</ul>
										</li>
									</ul>
								</li>
								
							</ul>
						</aside>
										
										
					<!-- ads position 2-->
					<div class="ads">


						<?php
								foreach ($banner as $position2) {
								if($position2->show_on=="Product Details" && $position2->status==1 && $position2->position == "Position 2"){
						?> 
							<a   href="<?php echo $position2->link ; ?>" target="_blank">
								<img     src="<?php echo base_url().'images/banner/'.$position2->images ?>" />
							</a>
						<?php
								}  
							} 
						 ?>  
					</div>						 
						<br/>
					</div>
					<!--End Sidebar-->
					<?php }else{
									$getPageStatus = getPageStatus($data_['user_id']);
									//var_dump($getPageStatus);
									if($getPageStatus != 1){redirect(base_url());}
						
						?>
					<div id="" class="content-area">
					<main id="main" class="site-main">
					<div class="row">
					   <div class="col-md-12">
							<div class="col-md-9">
								<div class="product">
									<div class="single-product-wrapper">
										<div class="product-images-wrapper">
											<span class="onsale">Sale!</span>
											 <div class="images electro-gallery">
												<div class="thumbnails-single owl-carousel">
													<!-- <div class="img_size"> -->
													<?php //foreach($data_ as $data){ 
													//var_dump($data_);
													?>
													<?php //var_dump($gall_['image']); 
														$thumbArray = explode(",", $gall_['image']);
														//var_dump($thumbArray);
														//echo count($thumbArray); exit;
														if($thumbArray[0]==''){ 
														//echo count($thumbArray); exit;
														?>
													<div class="img-size">
													<?php 
													if(!empty($data_['feature'])){ ?>
																<a href="<?php echo base_url().'images/products/'.bigthumb($data_['feature']); ?>" class="zoom" title="" data-rel="prettyPhoto[product-gallery]" >
																<!--img src="assets/images/blank.gif" data-echo="assets/images/single-product/s1-1.jpg" class="wp-post-image" alt=""-->
																<img id="zoom-0" src="<?php echo base_url().'images/products/'.bigthumb($data_['feature']); ?>" data-echo="<?php echo base_url().'images/products/'.bigthumb($data_['feature']); ?>" class="img-gall feature" data-zoom-image="<?php echo base_url().'images/products/'.bigthumb($data_['feature']); ?>" alt="">
																</a>
													<?php }else{ ?>
																<a href="#" class="zoom" title="" data-rel="prettyPhoto[product-gallery]" >
																<!--img src="assets/images/blank.gif" data-echo="assets/images/single-product/s1-1.jpg" class="wp-post-image" alt=""-->
																<img id="no_img" src="<?php echo base_url().'images/no_img.gif'; ?>" class="img-gall feature" alt="">
																</a>
													<?php		} ?>
													
													</div>
													<?php
														}else{
														//var_dump($data_['feature']);	
														$i=0;
														foreach($thumbArray as $thumb){
														if ($i==0){
															$first = 'zoom-';
														}	
														else{
															$first = '';
														}	
													?>
													<div class="img-size">
														<a href="<?php echo base_url().'images/products/'.bigthumb($thumb); ?>" class="zoom" title="" data-rel="prettyPhoto[product-gallery]" >
															
															<img id="zoom-0" width="450" src="assets/images/blank.gif" data-echo="<?php echo base_url().'images/products/'.bigthumb($thumb); ?>" class="img-gall feature" data-zoom-image="<?php echo base_url().'images/products/'.bigthumb($thumb); ?>" alt="">
														</a>
													</div>
													<?php $i++; } ?>
													<!-- </div> -->
												</div><!-- .thumbnails-single -->
												<div id="gallery_01" class="thumbnails-all columns-5 owl-carousel">
													<!--a href="<?php echo base_url().'images/products/'.bigthumb($data_['feature']); ?>" class="first" title="">
													
														<img src="assets/images/blank.gif" data-echo="<?php echo base_url().'images/products/'.$data_['feature']; ?>" class="" alt="">
													</a-->
													<?php  
														$i=0;
														foreach($thumbArray as $thumb){
														if ($i==0){
															$first = 'first';
														}	
														else{
															$first = '';
														}
													?>
														<a style="width:50px;height:50px;" href="#" data-image="<?php echo base_url().'images/products/'.bigthumb($thumb); ?>" data-zoom-image="<?php echo base_url().'images/products/'.bigthumb($thumb); ?>" class="<?php echo $first;?>" title="">
															<img id="zoom-0" src="<?php echo base_url().'images/products/'. $thumb; ?>" data-echo="<?php echo base_url().'images/products/'. $thumb; ?>" alt="">
														</a>
														<?php } } ?>
												</div><!-- .thumbnails-all -->
											</div><!-- .electro-gallery -->	
										</div><!-- /.product-images-wrapper -->
										
										<div class="summary entry-summary"> 

											<h1 itemprop="name" class="product_title entry-title"><?php echo $data_['product_name'];?></h1>
											<div class="brand">
												<a href="product-category.html">
													<h3><?php echo $data_['brand'];?></h3>
												</a>
											</div><!-- .brand -->

											<div class="availability in-stock">
												Availablity: <span><?php  echo "In Stock"; ?></span>
											</div><!-- .availability -->

											<hr class="single-product-title-divider" />

											<div class="action-buttons">
												<a href="<?php echo base_url().'ad_wishlist/'.$data_['product_id'];?>" class="add_to_wishlist" > Wishlist</a>
												<a href="<?php echo base_url().'ad_compare/'.$data_['product_id'];?>" class="add-to-compare-link" data-product_id="2452"> Compare</a>
											</div><!-- .action-buttons -->

											<div itemprop="description">
												<?php echo $data_['description']; ?> 
												<p><strong>SKU</strong>: <?php if ($data_['product_code']==''){echo $data_['product_id'];}else{echo $data_['product_code'];}?></p>
												
											</div><!-- .description -->
											<div itemprop="offers" itemscope>
												<p class="price"><span class="electro-price"><ins><span class="amount">&#36;<?php echo number_format((float)$data_['sale_price'], 2, '.', ''); ?></span></ins></span></p>
												<meta itemprop="price" content="1215" />
												<meta itemprop="priceCurrency" content="USD" />
												
											</div><!-- /itemprop -->

											<?php 
											$page_name = $get_contact['page_name'];
											echo form_open(base_url().$page_name.'/contact');?>
											<div class="single_variation_wrap">
												<div class="woocommerce-variation-add-to-cart variations_button">
													<?php
														$btn = array('find_product_by_user'=>array(
															'class' => 'single_add_to_cart_button button',
															'value' => 'Contact Supplier',
															'name'  => 'action')
														);
														echo form_submit($btn['find_product_by_user']);
													?>
												</div>
											</div>
											<?php echo form_close();?>
										</div><!-- .summary -->
									</div><!-- /.single-product-wrapper -->
								</div>
							</div>
							<div class="col-md-3"  style="border: 1px solid #DDDDDD;border-radius: 5px;">
								<!-- <div class="store-info wpb_column vc_column_container vc_col-sm-3 col-sm-3"> -->
								<div class="vc_column-inner ">
									<div class="wpb_wrapper">
										<div class="wpb_text_column wpb_content_element">
											<div class="wpb_wrapper">
												<!--div class="col-md-12 col-sm-12 col-xs-12 contact-page-title"-->
												<?php if($username['image']!=null){ ?>
													<div class="col-md-12 col-sm-12 col-xs-12">
													
														<h2 style="margin-top: 20px;"><a href="<?php echo base_url().$username['page_name'];?>" class="header-logo-link" target="_blank">
															<img src="<?php echo base_url().'images/products/'. bigthumb($username['image']);?>" style="text-align:center; margin:auto; width:120px; height:120px">
														</a>
														</h2>
													</div>
													<?php } ?>
													<div class="col-md-12 col-sm-12 col-xs-12"> 
														<a href="<?php echo base_url().$username['page_name']; ?>" target="_blank"><p style="font-size: 16px;font-weight: bold; margin-top: 5px; text-align:center;"><?php echo $get_contact['company_name']; ?>
														</p></a>
													</div> 								
												<!--/div-->													
													
												<i class="fa fa-phone" aria-hidden="true"></i><span class="bold"> Call Us</span> <br>
													<address><?php echo $get_contact['phone']; ?></address>
												<i class="fa fa-envelope" aria-hidden="true"></i><span class="bold"> Mail Us</span><br>
													<address><a href="mailto:<?php echo $get_contact['email']; ?>"><?php echo $get_contact['email']; ?></a></address>	

												<!-- website -->
												<?php $website = $get_contact['website'];
													  if($website != "" || $website != null ){
												?>	
												 <i class="fa fa-globe" aria-hidden="true"></i><span class="bold"> Website</span><br> 
													<address><a target="_blank" href="http://<?php echo $website;?>"><?php echo $website;?></a></address>
												<?php } ?> <!--End website -->
												
												<!-- Facebook Page -->
												<?php $facebook_page = $get_contact['facebook_page'];
													  if($facebook_page != "" || $facebook_page != null ){
												?>	
												 <i class="fa fa-facebook-square" aria-hidden="true"></i><span class="bold"> Facebook Page</span><br> 
													<address><a target="_blank" href="http://<?php echo $facebook_page;?>"><?php echo $facebook_page;?></a></address>
												<?php } ?><!-- End Facebook Page -->

												<i class="fa fa-home" aria-hidden="true"></i><span class="bold"> Address</span> <br>
													<address><?php  echo $get_contact['address'];?></address>

													<div style="float:left">
														
														<a href="<?php echo base_url().$username['page_name']; ?>" class="header-logo-link" target="_blank">
															Visit Minisite
														</a>
														
													</div>
													<div style="float:right">
														<a href="<?php echo base_url().$username['page_name'] . '/contact'; ?>" target="_blank">
															Contact Supplier
														</a>
													</div> 								
												
																												
											</div>
										</div>
									</div>
								</div>
							<!-- </div> -->
							</div><!--End col-md-3-->
						</div>
					 </div>
					</main><!-- /.site-main -->
				</div><!-- /.content-area -->
				<!--Sidebar Start-->
				
				<?php }?>
				<!-- Related Products -->
				<div class="clearfix"></div>
				<!-- ads position 3-->
				<div class="ads">
					<center>
						<?php
							foreach ($banner as $position3) {
							if($position3->show_on=="Product Details" && $position3->status==1 && $position3->position == "Position 3"){
						?> 
							<a  href="<?php echo $position3->link ; ?>" target="_blank">
								<img     src="<?php echo base_url().'images/banner/'.$position3->images ?>" />
							</a>
						<?php
								}  
							} 
						 ?>
					</center>
				</div> 
				<?php 
				if($get_group['group_id'] != 2){
				?>
				<div class="col-md-12">
					<section class="home-v1-recently-viewed-products-carousel section-products-carousel animate-in-view fadeIn animated" data-animation="fadeIn">
						<header>
							<h2 class="h1">Related Products</h2>
							<div class="owl-nav">
								<a href="#products-carousel-prev" data-target="#recently-added-products-carousel" class="slider-prev"><i class="fa fa-angle-left"></i></a>
								<a href="#products-carousel-next" data-target="#recently-added-products-carousel" class="slider-next"><i class="fa fa-angle-right"></i></a>
							</div>
						</header>
						<div id="recently-added-products-carousel">
							<div class="woocommerce columns-6">
								<div class="products owl-carousel recently-added-products products-carousel columns-6">
									<?php

									foreach($relatedPro  as $row){
										if($row['status'] == 1){
									?>
									<div class="product">
										<div class="product-outer">
											<div class="product-inner">
												<a href="<?php echo base_url() . 'detail/'. $row['permalink']; ?>">
													<h3><?php echo $row['product_name'];?></h3>
													<div class="product-thumbnail_">
														<img width="150" height="150" src="<?php echo base_url().'images/products/'.$row['feature'];?>" data-echo="<?php echo base_url().'images/products/'.$row['feature'];?>" class="img-responsive" alt="">
													</div>
												</a>
												<div class="price-add-to-cart">
													<span class="price">
														<span class="electro-price">
															<ins><span class="amount"><?php echo currency('sign').number_format((float)$row['sale_price'], 2, '.', '');?></span></ins>
															<!--del><span class="amount"><?php echo currency('sign').$row['regular_prices'];?></span></del-->
															<span class="amount"> </span>
														</span>
													</span>
													<?php 
													echo form_open(base_url()."cart/addcart");
																	echo form_hidden('product_id', $row['product_id']);
																	echo form_hidden('product_name', $row['product_name']);
																	echo form_hidden('sale_price', number_format((float)$row['sale_price'], 2, '.', ''));
																	echo form_hidden('quantity', 1);
																		$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
																	//echo form_submit($btn);
																	$page_name = get_user_page($row['user_id']);
													if($row['group_id'] != 2){ 
															if($row['contact-type'] == 1){ ?>
																<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().'p/contact-us'; ?>" class="fa fa-phone fa-1x"></a>
															<?php }else{ 
																echo form_button($btn['button']);
															?>
																	
															<?php }
													?>
														
													<?php }else{ ?>
																<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().$username['page_name'] . '/contact'; ?>" class="fa fa-phone fa-1x"></a>											
													<?php } ?>
												</div><!-- /.price-add-to-cart -->
											</div><!-- /.product-inner -->
											</div><!-- /.product-outer -->
									</div><!-- /.products -->
									<?php
									}
									}
									?>
								</div>
							</div>
						</div>
					</section>
				</div><br/><!-- //End Related Products -->
				<?php 
				}else{
				?>
				<div class="col-md-12">
					<section class="home-v1-recently-viewed-products-carousel section-products-carousel animate-in-view fadeIn animated" data-animation="fadeIn">
						<header>
							<h2 class="h1">Related Products</h2>
							<div class="owl-nav">
								<a href="#products-carousel-prev" data-target="#recently-added-products-carousel" class="slider-prev"><i class="fa fa-angle-left"></i></a>
								<a href="#products-carousel-next" data-target="#recently-added-products-carousel" class="slider-next"><i class="fa fa-angle-right"></i></a>
							</div>
						</header>
						<div id="recently-added-products-carousel">
							<div class="woocommerce columns-6">
								<div class="products owl-carousel recently-added-products products-carousel columns-6">
									<?php

									foreach($relatedProVendor  as $row){
										if($row['status'] == 1){
									?>
									<div class="product">
										<div class="product-outer">
											<div class="product-inner">
												<a href="<?php echo base_url() . 'detail/'. $row['permalink']; ?>">
													<h3><?php echo $row['product_name'];?></h3>
													<div class="product-thumbnail_">
														<img width="150" height="150" src="<?php echo base_url().'images/products/'.$row['feature'];?>" data-echo="<?php echo base_url().'images/products/'.$row['feature'];?>" class="img-responsive" alt="">
													</div>
												</a>
												<div class="price-add-to-cart">
													<span class="price">
														<span class="electro-price">
															<ins><span class="amount"><?php echo currency('sign').number_format((float)$row['sale_price'], 2, '.', '');?></span></ins>
															<!--del><span class="amount"><?php echo currency('sign').$row['regular_prices'];?></span></del-->
															<span class="amount"> </span>
														</span>
													</span>
													<?php 
													echo form_open(base_url()."cart/addcart");
																	echo form_hidden('product_id', $row['product_id']);
																	echo form_hidden('product_name', $row['product_name']);
																	echo form_hidden('sale_price', number_format((float)$row['sale_price'], 2, '.', ''));
																	echo form_hidden('quantity', 1);
																		$btn = array('input'=>array(
																			'class' => 'button cart_bg',
																			'value' => 'Add to cart',
																			'name' => 'action'),
																			'button'=>array('class'=>'button add_to_cart_button',
																							'id'=>'button',
																							'type'=>'submit',
																							'content'=>'Add to cart',
																							'name'=>'action')
																	
																		);
																	//echo form_submit($btn);
																	$page_name = get_user_page($row['user_id']);
													if($row['group_id'] != 2){ 
															if($row['contact-type'] == 1){ ?>
																<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().'p/contact-us'; ?>" class="fa fa-phone fa-1x"></a>
															<?php }else{ 
																echo form_button($btn['button']);
															?>
																	
															<?php }
													?>
														
													<?php }else{ ?>
																<a style="color:#ffffff;border-radius:30px;padding: 10px 12px;background:#efecec;" rel="nofollow" href="<?php echo base_url().$username['page_name'] . '/contact'; ?>" class="fa fa-phone fa-1x"></a>											
													<?php } ?>
												</div><!-- /.price-add-to-cart -->
											</div><!-- /.product-inner -->
											</div><!-- /.product-outer -->
									</div><!-- /.products -->
									<?php
									}
									}
									?>
								</div>
							</div>
						</div>
					</section>
				</div><br/><!-- //End Related Products -->
				<?php } ?>
				<div class="woocommerce-tabs wc-tabs-wrapper" style="display:none;">
					<ul class="nav nav-tabs electro-nav-tabs tabs wc-tabs" role="tablist">
						<li class="nav-item description_tab">
							<a href="#tab-description" class="active" data-toggle="tab">Description</a>
						</li>

						<li class="nav-item specification_tab">
							<a href="#tab-specification" data-toggle="tab">Specification</a>
						</li>

						<li class="nav-item reviews_tab">
							<a href="#tab-reviews" data-toggle="tab">Reviews</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active in panel entry-content wc-tab" id="tab-description">
							<div class="electro-description">
							description
							</div>
						</div>

						<div class="tab-pane panel entry-content wc-tab" id="tab-specification">
							<h3>Technical Specifications</h3>
						</div><!-- /.panel -->

						<div class="tab-pane panel entry-content wc-tab" id="tab-reviews">
							<div id="reviews" class="electro-advanced-reviews">
								<div class="advanced-review row">
									<div class="col-xs-12 col-md-6">
										<h2 class="based-title">Based on 3 reviews</h2>
										<div class="avg-rating">
											<span class="avg-rating-number">4.3</span> overall
										</div>

										<div class="rating-histogram">
											<div class="rating-bar">
												<div class="star-rating" title="Rated 5 out of 5">
													<span style="width:100%"></span>
												</div>
												<div class="rating-percentage-bar">
													<span style="width:33%" class="rating-percentage">

													</span>
												</div>
												<div class="rating-count">1</div>
											</div><!-- .rating-bar -->

											<div class="rating-bar">
												<div class="star-rating" title="Rated 4 out of 5">
													<span style="width:80%"></span>
												</div>
												<div class="rating-percentage-bar">
													<span style="width:67%" class="rating-percentage"></span>
												</div>
												<div class="rating-count">2</div>
											</div><!-- .rating-bar -->

											<div class="rating-bar">
												<div class="star-rating" title="Rated 3 out of 5">
													<span style="width:60%"></span>
												</div>
												<div class="rating-percentage-bar">
													<span style="width:0%" class="rating-percentage"></span>
												</div>
												<div class="rating-count zero">0</div>
											</div><!-- .rating-bar -->

											<div class="rating-bar">
												<div class="star-rating" title="Rated 2 out of 5">
													<span style="width:40%"></span>
												</div>
												<div class="rating-percentage-bar">
													<span style="width:0%" class="rating-percentage"></span>
												</div>
												<div class="rating-count zero">0</div>
											</div><!-- .rating-bar -->

											<div class="rating-bar">
												<div class="star-rating" title="Rated 1 out of 5">
													<span style="width:20%"></span>
												</div>
												<div class="rating-percentage-bar">
													<span style="width:0%" class="rating-percentage"></span>
												</div>
												<div class="rating-count zero">0</div>
											</div><!-- .rating-bar -->
										</div>
									</div><!-- /.col -->

									<div class="col-xs-12 col-md-6">
										<div id="review_form_wrapper">
											<div id="review_form">
											<?php
											echo form_open(base_url()."comment/index",array('class' => 'variations_form cart'));
											?>
												<div id="respond" class="comment-respond">
													<h3 id="reply-title" class="comment-reply-title">Add a review
														<small><a rel="nofollow" id="cancel-comment-reply-link" href="#" style="display:none;">Cancel reply</a>
														</small>
													</h3>

													<form action="#" method="post" id="commentform" class="comment-form">
														<p class="comment-form-rating">
															<label>Your Rating</label>
														</p>

														<p class="stars">
															<span><a class="star-1" href="#">1</a>
																<a class="star-2" href="#">2</a>
																<a class="star-3" href="#">3</a>
																<a class="star-4" href="#">4</a>
																<a class="star-5" href="#">5</a>
															</span>
														</p>

														<p class="comment-form-comment">
															<label for="comment">Your Review</label>
															<input type="hidden" name="product_id" id="product_id" value="<?php echo $data_['product_id'];?>"/>
															<input type="hidden" name="user_id" id="user_id" cols="45" required value="<?php echo get_user_id();?>"/>
															<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
														</p>
														<div id="msg_review"></div>
														<p class="form-submit">
															<input name="button" type="submit" id="submit_review" class="submit" value="Add Review" />
															<input type='hidden' name='comment_post_ID' value='2452' id='comment_post_ID' />
															<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
														</p>

														<input type="hidden" id="_wp_unfiltered_html_comment_disabled" name="_wp_unfiltered_html_comment_disabled" value="c7106f1f46" />
														<script>(function(){if(window===window.parent){document.getElementById('_wp_unfiltered_html_comment_disabled').name='_wp_unfiltered_html_comment';}})();</script>
													</form><!-- form -->
												</div><!-- #respond -->
											<?php echo form_close();?>
											</div>
										</div>

									</div><!-- /.col -->
								</div><!-- /.row -->
							   <div id="error-msg"></div>
								<div id="comments">

									<ol class="commentlist">
										<?php 
										foreach($comments as $row){
										?>
										<li itemprop="review" class="comment even thread-even depth-1">
											<div id="comment-390" class="comment_container">
												<?php $user=get_profile($row['user_id']);?>
												<img alt='' src="<?php echo base_url();?>images/products/<?php echo $user['image'];?>" class='avatar' height='60' width='60' />
												<div class="comment-text">
													<div class="star-rating" title="Rated 4 out of 5">
														<span style="width:80%"><strong itemprop="ratingValue">4</strong> out of 5</span>
													</div>
													<div itemprop="description" class="description">
														<p><?php echo $row['comment_desc'];?></p>
													</div>
													
													<p class="meta">
														<strong itemprop="author">
														<?php 
														//$user=get_profile($row['user_id']);
														echo $user['first_name'].'&nbsp;'.$user['last_name'];
														?>
														</strong> &ndash; <time itemprop="datePublished"><?php echo normal_date($row['comment_date']);?></time>
													</p>
													
													<div class='reply-comment' style="padding-left:50px;">
													<ol class="commentlist" >
														<?php foreach($childComments as $comment):
																if ($comment['comment_parent']==$row['comment_id']): 
														 ?>
														<li itemprop="review" class="comment even thread-even depth-1">
															<div id="comment-390" class="comment_container">
																<?php $user=get_profile($row['user_id']);?>
																<img alt='' src="<?php echo base_url();?>images/products/<?php echo $user['image'];?>" class='avatar' height='60' width='60' />
																<div class="comment-text">
																	<div class="star-rating" title="Rated 4 out of 5">
																		<span style="width:80%"><strong itemprop="ratingValue">4</strong> out of 5</span>
																	</div>
																	<div itemprop="description" class="description">
																		<p><?php echo $comment['comment_desc'];?></p>
																	</div>
																	
																	<p class="meta">
																		<strong itemprop="author">
																		<?php 
																		//$user=get_profile($row['user_id']);
																		echo $user['FirstName'].'&nbsp;'.$user['LastName'];
																		?>
																		</strong> &ndash; <time itemprop="datePublished"><?php echo normal_date($row['comment_date']);?></time>
																	</p>
																</div>
															</div>
														</li>
													<?php 
													endif;
													endforeach;
													?>
													</ol>
													<?php echo form_open(base_url()."comment/treeComment", array('class'=>'tree-comment')); ?>
													<a class='reply'>Post a reply</a>
													<div id="reply-section" style="display:none;">
													<input type="hidden" name="product_id" id="product_id" value="<?php echo $data_['product_id'];?>"/>
													<input type="hidden" name="user_id" id="user_id" cols="45" required value="<?php echo get_user_id();?>"/>
													<input type="hidden" name="comment_parent_id" id="comment_parent_id" required value="<?php echo $row['comment_id'];?>"/>
													<input type="text" name="comment-reply" class="comment-reply" placeholder="Write a reply..."/>
													<input name="btn-comment-reply" class="btn-comment-reply" type="button" value="Reply"/>
													</div>
													</div>
													<?php echo form_close(); ?>
												</div>
											</div>
										</li><!-- #comment-## -->
										<?php }?>
									</ol><!-- /.commentlist -->

								</div><!-- /#comments -->

								<div class="clear"></div>
							</div><!-- /.electro-advanced-reviews -->
						</div><!-- /.panel -->
					</div>
				</div><!-- /.woocommerce-tabs -->
				
				 
				
				
						
				</div>
				<div class="col-md-2 ads"> 
				
					<br/>
					<br/>
					<br/>
						 <?php
							foreach ($banner as $side_right) {
								if($side_right->show_on=="Product Details" && $side_right->status==1 && $side_right->position == "Fly Side Right"){
						?> 
							<a  href="<?php echo $side_right->link ; ?>" target="_blank">
								<img  class="fly-side-left" src="<?php echo base_url().'images/banner/'.$side_right->images ?>" />
							</a>
						<?php
								}  
							} 
						 ?>
				
				</div> 
			</div> 
			<div class="container"> 				
			</div> 
		</div><!-- /.site-content -->  
	</div><!-- #page -->
	<?php $this->load->view('includes/sub_footer');?>
	<?php $this->load->view('includes/footer');?>
</body>
</html>
