<?php $this->load->view('includes/head');?>

    <body class="left-sidebar">
        <div id="page" class="hfeed site">
			<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
			<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
			<?php $this->load->view('includes/header1');?>
            <div id="content" class="site-content" tabindex="-1">
	<div class="container">

		<nav class="woocommerce-breadcrumb" ><a href="home.html">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Search Supplier</nav>

		<div>
			<main id="main" class="site-main">

				<header class="page-header">
				   <h1 class="page-title" style="text-align:left;font-size:2.26em;">Search Results: “<?php echo $searchFor;?>”</h1>
				   <p class="woocommerce-result-count">
					  <?php echo $showing; ?>
				   </p>
				</header>
				
				   <!--ul class="shop-view-switcher nav nav-tabs" role="tablist">
					  <li class="nav-item"><a class="nav-link active" data-toggle="tab" title="Grid View" href="#grid"><i class="fa fa-th"></i></a></li>
					  <li class="nav-item"><a class="nav-link " data-toggle="tab" title="Grid Extended View" href="#grid-extended"><i class="fa fa-align-justify"></i></a></li>
					  <li class="nav-item"><a class="nav-link " data-toggle="tab" title="List View" href="#list-view"><i class="fa fa-list"></i></a></li>
					  <li class="nav-item"><a class="nav-link " data-toggle="tab" title="List View Small" href="#list-view-small"><i class="fa fa-th-list"></i></a></li>
				   </ul-->

				   <!-- <form class="woocommerce-ordering" method="get"> 
					  <select name="orderby" class="orderby">
						 <option value="menu_order" selected="selected">Default sorting</option>
						 <option value="popularity">Sort by popularity</option>
						 <option value="rating">Sort by average rating</option>
						 <option value="date">Sort by newness</option>
						 <option value="price">Sort by price: low to high</option>
						 <option value="price-desc">Sort by price: high to low</option>
					  </select>
					  <input type="hidden" name="s" value="Purple NX Mini F1 aparat  SMART NX"><input type="hidden" name="product_cat" value="0"><input type="hidden" name="post_type" value="product">
				   </form>-->
				   
				
								
						<div class="tab-content">	
							
								<div role="tabpanel" class="tab-pane active" id="list-view" aria-expanded="true">
								
									<?php foreach($results as $item): 
									?> 
									<?php $latestThree = vendorSearchProducts($item['user_id']);
										if(count($latestThree) >= 1){
											$hide = '';
										}else{
											$hide = ' style="display:none;"';
										}
									?>
									<div data-spm="35" class="f-icon m-item  " data-ctrdot="230660572" data-spm-max-idx="16" <?php echo $hide; ?>>
										<div class="item-main">
											<div class="top">
												<div class="corp">
													<div class="item-title">
																			
															<a class="ico-year" target="_blank" rel="nofollow" title="What is Gold Supplier?" data-domdot="id:2639" href="http://static.alibaba.com/hermes/goldsuppliers.html" data-spm-anchor-id="0.0.0.0">
																<span class="gs4"></span>
															</a>
															<a class="ico ico-ta" data-ta="item-tips-action" data-ta-price="69,000" data-domdot="id:17084" rel="nofollow" target="_blank" href="//tradeassurance.alibaba.com/bao/buyer_advertise.htm?tracelog=from_list_item" data-spm-anchor-id="0.0.0.0"></a>
																<h2 class="title ellipsis">
															<a target="_blank" title="" href="<?php echo base_url() . $item['page_name']; ?>" data-hislog="230660572" data-domdot="id:2638,sid:230660572" data-spm-anchor-id="0.0.0.0"><?php echo $item['company_name']; ?></a>
														</h2>
													</div>
												</div>
											</div>
											<div class="content util-clearfix">
												<div class="left">
													<div class="products util-clearfix">
														<?php foreach($latestThree as $each) : ?>
															<div class="product">
																<div class="img-thumb" data-role="thumb">
																	<a target="_blank" href="<?php echo base_url();?>detail/<?php echo $each['permalink'];?>" data-domdot="id:2636,sid:230660572,pid:60592617813" data-spm-anchor-id="0.0.0.0">
																		<img alt="" src="<?php echo base_url();?>images/products/<?php echo $each['feature'];?>">
																	</a>
																</div>
																<h3>
																	<a target="_blank" title="<?php echo $each['product_name']; ?>" href="<?php echo base_url();?>detail/<?php echo $each['permalink'];?>"><?php echo $each['product_name']; ?></a>
																</h3>
															</div>
														<?php endforeach; ?>
														</div>
													</div>
												<div class="right">
													<div class="attrs">
														
														<div class="attr">
															<span class="name">Website:</span>
															<div class="value ellipsis ph"><strong><?php $site = ($item['website']) ? $item['website'] : 'N/A'; echo $site;?></strong></div>
														</div>
														<div class="attr">
															<span class="name">Facebook:</span>
															<div class="value" title="">
																<span class="ellipsis search"><?php $fb = ($item['facebook_page']) ? $item['facebook_page'] : 'N/A'; echo $fb;?></span>
															</div>
														</div>
														<div class="attr">
															<span class="name">Country:</span>
															<div class="value">
																								<span class="ellipsis search"><?php echo country_byid($item['country']); ?></span>
															</div>
														</div>
														<div class="contact">
															<a class="button csp" target="_blank" href="<?php echo base_url(). $item['page_name'];?>" rel="nofollow"><span class="ico-cs"></span>Visit Page</a>
															<a class="button csp" target="_blank" href="<?php echo base_url(). $item['page_name'];?>/contact" rel="nofollow"><span class="ico-cs"></span>Contact Supplier</a>
															
														</div>
													</div>
													
												</div>
											</div>
										</div>
									</div>
									<?php endforeach; ?>
									
								</div>
								
							</div>
						</div>
				

			<nav class="electro-advanced-pagination">
								<?php echo $page;?>
					</nav>
			</main><!-- #main -->
		</div><!-- #primary -->
		
	
<!-- .container -->
<!-- #content -->
	</div>
        <?php $this->load->view('includes/contact_footer');?>
	</div><!-- #page -->
<?php $this->load->view('includes/footer');?>


    </body>

<!-- Mirrored from transvelo.github.io/electro-html/shop.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Sep 2016 07:10:04 GMT -->
</html>
