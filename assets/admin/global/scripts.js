	function check_all(obj){
		var set = $(obj).attr("data-set");
        var checked = $(obj).is(":checked");
        $(set).each(function () {
            if (checked) {
                $(set).attr("checked", true);
                $(set).closest('div.checker').find('span').attr('class','checked');

            } else {
                $(set).attr("checked", false);
                $(set).closest('div.checker').find('span').attr('class','');
            }
        });
        //$.uniform.update(set);
	}

	function go(obj){
		$('form').submit();
	}

    $('.date-picker').datepicker({
        format: 'dd-mm-yyyy'
    })

    $('.timepicker-24').timepicker({
        autoclose: true,
        minuteStep: 5,
        showSeconds: true,
        showMeridian: false
    });

    //for tinymce
    tinymce.init({
        selector: "textarea.mce",
        plugins: [
            "advlist autolink lists link image charmap print preview jbimages anchor sh4tinymce",
            "searchreplace visualblocks code fullscreen pagebreak",
            "insertdatetime media table contextmenu paste textcolor "
        ],
        toolbar: " fontsizeselect | forecolor backcolor |  bullist numlist outdent indent | link image jbimages",
        height : 300,

        file_browser_callback: function(field, url, type, win) {
            tinyMCE.activeEditor.windowManager.open({
                file: ""+base_admin_assets_url+"kcfinder/browse.php?opener=tinymce4&field=" + field + '&type='+ type +'s',
                title: 'KCFinder',
                width: 700,
                height: 500,
                inline: true,
                close_previous: false
            }, {
                window: win,
                input: field
            });
            return false;
        },
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
          //document_base_url: "http://www.site.com/path1/",
           remove_script_host: false,
           relative_urls: false,
        image_advtab: true,
        image_class_list: [
            {title: 'None', value: ''},
            {title: 'Text-center', value: 'text-center'},
            {title: 'Image Responsive', value: 'img-responsive'}
        ],
        // forced_root_block : false
        invalid_elements : "script"

     });
    //for tinymce
