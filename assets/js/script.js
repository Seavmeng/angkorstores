jQuery(function($) {
        $('.color').click(function(){
                var imgsrc= $(this).attr('src');
		var color=$(this).attr('value');
		$('.varian .color').removeClass('active');
		$(this).addClass('active');
		$("#hidden_color").val(color);
		
		var id=$(this).attr('href');
		var pro_id=$(this).attr('pro_id');
		var form_data = {
			id: id,
			pro_id:pro_id
		}
		$.ajax({
			url: base_url+"home/size_bycolor",
			type: 'POST',
			data: form_data,
			success: function(msg){
				$("#show_color_size").html(msg);
                                document.getElementById("zoom-0").src= imgsrc;
				$("#hidden_size").val('');
				$("#price_varian").html('');
			}
		});
		return false;
	});
	$("#show_color_size").on("click",".size", function(){
		var size=$(this).attr('value');
		$('.varian .size').removeClass('active');
		$(this).addClass('active');
		$("#hidden_size").val(size);
		//---------
		var color=$("#hidden_color").val();
		var pro_id=$("#product_id").val();
		if(color !="" && size !=""){
			var form_data = {
				color: color,
				size: size,
				pro_id: pro_id
			}
			$.ajax({
				url: base_url+"home/varian_price",
				type: 'POST',
				data: form_data,
				success: function(msg){
					if(msg !=00){
						$("#price_varian").html(msg);
						document.getElementById("original_price_varian").setAttribute('style','display:none');
					}else{
						$("#price_varian").html('');
						document.getElementById("original_price_varian").removeAttribute("style"); 
						
					}
				}
			});
		}
		return false;
	});
	//---------
	$('#add_to_cart,#buy_now').click(function(){
		var buy_now=$(this).attr('id');
		var quantity=$('#quantity').val();
		var product_id=$('#product_id').val();
		var product_name=$('#product_name').val();
		var sale_price=$('#sale_price').val();
		var color=$('#hidden_color').val();
		var size=$('#hidden_size').val();
		$("#loading").show();
		if(color =="" || size ==""){
			$('.msg_varian').html('Please select Color Or Size.');
			$('.msg_varian').attr("style","background:#EE0000;color:3px;color:#ffffff;");
		}else{
			if(buy_now=='buy_now'){
				var form_data = {
					quantity: quantity,    
					product_id: product_id,    
					product_name: product_name,    
					sale_price: sale_price,    
					color: color, 
					size: size,
					buy_now: buy_now
				}
			}else{
				var form_data = {
					quantity: quantity,    
					product_id: product_id,    
					product_name: product_name,    
					sale_price: sale_price,    
					color: color, 
					size: size
				}
			}
			$.ajax({
				url: base_url+"cart/addcart",
				type: 'POST',
				data: form_data,
				success: function(msg){
					if(buy_now=='buy_now'){
						window.location=base_url+"checkout?vc=cart";
					}else{
						window.location.reload();
					}
                    $("#loading").hide();	 
				}
			});
		}
		return false;
	});
	
	$('.product').mouseover(function(){
		$(this).find('.fa.fa-phone.fa-1x').css('background','#fed700');
	});
	$('.product').mouseleave(function(){
		$(this).find('.fa.fa-phone.fa-1x').css('background','#efecec');
	});
	
	$("#postal-code").css('display', 'none');
        $("#search").autocomplete({   
		source: base_url+"search/auto_complete",
		minLength:2,
		open: function(event, ui) {
        var main_search=$("#search").val();
		 //$('.ui-autocomplete').append('<li class="seemore"><i class="fa fa-search"></i> <a href="'+base_url+'search/index?search='+main_search+'">See All Result</a></li>');
                }
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
		var inner_html = '<a href="'+base_url+'detail/'+item.permalink+'">'+
				'<div class="list-item">'+
					//'<div class="image"><img src="' + item.picture + '"></div>'+
					'<div class="label-product">' + item.product_name + '</div>'+
					'<div class="descriptions"></div>'+
				'</div>'+
			'</a>';
		return $( "<li></li>")
			.data( "item.autocomplete", item )
			.append(inner_html)
			.appendTo(ul);
	};
	
	$('#search-vendor').autocomplete({
		      	source: function( request, response ) {
		      		$.ajax({
		      			url : base_url+"search/auto_complete_vendor",
		      			dataType: "json",
						data: {
						   name_startsWith: request.term,
						   type: 'vendor'
						},
						 success: function( data ) {
							 response( $.map( data, function( item ) {
								return {
									label: item.company_name,
									value: item.company_name
								}
							}));
						}
		      		});
		      	},
		      	autoFocus: true,
		      	minLength: 0      	
		      });
	function onSubmit() 
	{ 
		var fields = $("input[name='categories[]']").serializeArray(); 
		var recentFields = $("input[name='recents[]']").serializeArray();
		if (fields.length > 0 || recentFields.length > 0) 
		{ 
			
			// cancel submit
			return true;
		}else{
			alert('Please choose at least 1 category!'); 
			return false;
		}
			
		
	}
	$('#product_submit').submit(onSubmit)
        /* by khaing 
	 *2/15/2017
	 *Manage slidshow
	*/
	$('.home-v1-slider').mouseover(function(){
		$('.owl-carousel .owl-nav.disabled, .owl-carousel .owl-dots.disabled').css('display','inline-block');
		$('.home-v1-slider .owl-dots, .home-v2-slider .owl-dots, .home-v3-slider .owl-dots').css('position','absolute','bottom', '50px','display','block','text-align','center','width','100%');	
	});	
	$('.home-v1-slider').mouseleave(function(){
		$('.owl-carousel .owl-nav.disabled, .owl-carousel .owl-dots.disabled').css('display','none');
	});	
		/* ----------by khaing 2/15/2017----------*/        

        $('#hover').mouseover(function(){
		$(this).css("background-color","gold");
	});
	$('#hover').mouseleave(function(){
		$(this).css("background-color","#FFFFFF");
	});

	// // $('#p-cate').mouseleave(function(){
	// // 	$(".show-cate").slideUp('slow');
	// // });
	// $('#s-cate').hover(function(){
	// 	$(".show-cate").slideDown('400');
	// });
	// $('#s-cate').mouseleave(function(){
	// 	$(".show-cate").slideUp('slow');
	// });
	/* vendor page sigle product*/
	// $(".m-home").click(function() {
	// 	$("div.home").show();
	// 	$("div.contact").hide();
	// });
	// $(".m-contact").click(function(){
	// 	$("div.contact").show();
	// 	$("div.home").hide();
	// });
	// $('.cat-item').click(function(){
	// 	$("div.home div.contact").hide();
	// });
	
	/* Thean for backend shipping */
	
	var ship = $('#shipcompany').val();
		//alert(ship);
		if(ship == 1){
			
			var form_data= {
			val: ship
			}
			alert(ship);
			$.ajax({
				url: base_url+"cart/shipDHL",
				type: 'POST',
				data: form_data,
				success: function(msg){
					$("#country").html("");
					$("#location-label").html("Country");
					$("#show_shipping_state").html(msg);
					$("#loading").hide();
				}
			});
		}else if(ship == 2){
			var form_data= {
			val: ship
			}
			$.ajax({
				url: base_url+"cart/shipfedex",
				type: 'POST',
				data: form_data,
				success: function(msg){
					$("#country").html("");
					$("#location-label").html("Country");
					$("#show_shipping_state").html(msg);
					$("#loading").hide();
				}
			});
		}else if(ship == 3){
			var form_data= {
			val: ship
			}
			$.ajax({
				url: base_url+"cart/shipLocal",
				type: 'POST',
				data: form_data,
				success: function(msg){
					$("#country").html("");
					$("#location-label").html("City");
					$("#show_shipping_state").html(msg);
					$("#loading").hide();
				}
			});
		}else{
			var form_data= {
			val: ship
			}
			$.ajax({
				url: base_url+"cart/shipfedex",
				type: 'POST',
				data: form_data,
				success: function(msg){
					$("#country").html("");
					$("#location-label").html("Country");
					$("#show_shipping_state").html(msg);
					$("#loading").hide();
				}
			});
		}
	
	$('#shipcompany').change(function(){
		var ship = $(this).val();
		//alert(ship);
		if(ship == 1){
			
			var form_data= {
			val: ship
			}
			alert(ship);
			$.ajax({
				url: base_url+"cart/shipDHL",
				type: 'POST',
				data: form_data,
				success: function(msg){
					$("#country").html("");
					$("#location-label").html("Country");
					$("#show_shipping_state").html(msg);
					$("#loading").hide();
				}
			});
		}else if(ship == 2){
			var form_data= {
			val: ship
			}
			$.ajax({
				url: base_url+"cart/shipfedex",
				type: 'POST',
				data: form_data,
				success: function(msg){
					$("#country").html("");
					$("#location-label").html("Country");
					$("#show_shipping_state").html(msg);
					$("#loading").hide();
				}
			});
		}else if(ship == 3){
			var form_data= {
			val: ship
			}
			$.ajax({
				url: base_url+"cart/shipLocal",
				type: 'POST',
				data: form_data,
				success: function(msg){
					$("#country").html("");
					$("#location-label").html("City");
					$("#show_shipping_state").html(msg);
					$("#loading").hide();
				}
			});
		}else{
			var form_data= {
			val: ship
			}
			$.ajax({
				url: base_url+"cart/shipfedex",
				type: 'POST',
				data: form_data,
				success: function(msg){
					$("#country").html("");
					$("#location-label").html("Country");
					$("#show_shipping_state").html(msg);
					$("#loading").hide();
				}
			});
		}
	});
	
	/* End backend shipping */
	$(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });

	/*------------product--------------*/
	$("#page-value").change(function() {
		var company_name=$(this).val();
		var form_data = {
			company_name: company_name    
		}
		$.ajax({
			url: base_url+"admin/register/page_name",
			type: 'POST',
			data: form_data,
			success: function(msg){
				//$("#page_name").html(msg);
				//alert(msg);
				//$("#page-value").val(msg);
				if(msg==1){
					//alert(msg);
					$("#page_name").html('Page Name is already taken. Please try another one!');
					$("#page-exist").val(1);
					$("#exist-page").html('');
				}
				else{
					$("#page-value").val(msg);
					$("#page_name").html('');
					$("#page-exist").val('');
				}
			}
		});
	});
    /*$("#company_name_").change(function() {
		var company_name=$(this).val();
		var form_data = {
			company_name: company_name    
		}
		$.ajax({
			url: base_url+"admin/register/page_name",
			type: 'POST',
			data: form_data,
			success: function(msg){
				$("#page_name").html(msg);
			}
		});
	});*/
	$(".refresh_cap").click(function() {
	//alert('hello');
		$.ajax({
			type: "POST",
			url: base_url+"admin/register/captcha_refresh",
			success: function(res) {
				if (res){
					$("span.captcha").html(res);
				}
			}
		});
	});
	
	
	
	/*-----------change language----*/
	$('#btn-order').click(function(){
		var ship = $('#shipping-amount').html();
		if(ship === 'No Shipping'){
			alert('Please choose your shipment method to proceed to next step.');
			//alert('You will have to pick your stuff(s) up directly at our stores!');
			return false;
		}
	});
	$("#subscribe").click(function() {
		var subscriber = $('#subscriber').val();
		var form_data = {
			subscriber: subscriber    
		}
		if (subscriber==""){
			alert('Please input your email');
		}else{
			$.ajax({
				url: base_url+"home/subscribe",
				type: 'POST',
				data: form_data,
				success: function(msg) {
					if (msg == "no"){   
						$('#msg_review').html('Your Email already Exist...');
					}
					if (msg == "yes"){	
						$('#msg_review').html('Congradulation! You are successfull subscribe...');
					}               
				}
			});
		}
		return false;
	});
        $('#1').click(function(){
		//event.preventDefault();
		$("#loading").show();
		var val = $(this).val();
		var form_data= {
			val: val
		}
		$.ajax({
			url: base_url+"cart/shipDHL",
			type: 'POST',
			data: val,
			success: function(msg){
				$("#country").html("");
				$("#location-label").html("Country");
				$("#show_shipping_state").html(msg);
				$("#loading").hide();
			}
		});
		
		
	});
	$('#2').click(function(){
		//event.preventDefault();
		$("#loading").show();
		var val = $(this).val();
		var form_data= {
			val: val
		}
		$.ajax({
			url: base_url+"cart/shipfedex",
			type: 'POST',
			data: val,
			success: function(msg){
				$("#country").html("");
				$("#location-label").html("Country");
				$("#show_shipping_state").html(msg);
				$("#loading").hide();
			}
		});
		
		
	});
    $('#4').click(function(){
		//event.preventDefault();
		$("#loading").show();
		var val = $(this).val();
		var form_data= {
			val: val
		}
		$.ajax({
			url: base_url+"cart/shipEms",
			type: 'POST',
			data: val,
			success: function(msg){
				$("#country").html("");
				$("#location-label").html("Country");
				$("#show_shipping_state").html(msg);
				$("#loading").hide();
			}
		});
	});
        
	$('#3').click(function(){
		//event.preventDefault();
		$("#loading").show();
		var val = $(this).val();
		var form_data= {
			val: val
		}
		$.ajax({
			url: base_url+"cart/shipLocal",
			type: 'POST',
			data: val,
			success: function(msg){
				$("#country").html("");
				$("#location-label").html("City");
				$("#show_shipping_state").html(msg);
				$("#loading").hide();
			}
		});
		
		
	});
	/* $("input[type=\"radio\"]").click(function(){
		var thisElem = $(this);
		var value = thisElem.val();
        $(".box").hide();
		$("."+value).show();
		//localStorage:
		localStorage.setItem("option", value);
		//Cookies:
		//document.cookie="option="+value;
    });
	//localStorage:
	var itemValue = localStorage.getItem("option");
	if (itemValue !== null) {
		$("input[value=\""+itemValue+"\"]").click();
	}*/
    /*
	//Cookies:
	var n = document.cookie;
	if (n.indexOf("option=") !== -1) {
		var cookieValue = n.substring(n.indexOf("option=")+7, n.indexOf(";"))
		$("input[value=\""+cookieValue+"\"]").click();
	}
    */
	
	$('#payment_method_bacs').click(function(){ 
		$("#myModal").modal();
		
	});
	/*if($('#payment_method_cheque').is(':checked')) { 
		$("#billing_postcode").prop("disabled", "disabled");
		//$("#postal-code").css("display","none");
		$("#postal-code").remove();
	}*/
	if($("#ship-method-val").val()==3){
		$("#billing_postcode").prop("disabled", "disabled");
		//$("#postal-code").css("display","none");
		$("#postal-code").remove();
	}
	else{
		//$("#postal-code").css('display', 'block');
	}
	if(!$("input[name='billing_country']").val() || $("input[name='billing_country']").val() == 103){
		
		$("#postal-code").css('display', 'none');
		
	}
	else{
		$("#postal-code").css('display', 'block');
	}
	$('#payment_method_cheque').click(function(){
		//event.preventDefault();
		//$("#loading").show();
		//alert("it's checked");
		//sessionStorage.setItem("billing_first_name", $('#first_name').val());
		var val = $(this).val();
		var shipping_country = $('#shipping_country').val();
		var shipping_city = $('#shipping_city').val();
		//
		var first_name = $('#first_name').val();
		var last_name = $('#last_name').val();
		var billing_email = $('#billing_email').val();
		var billing_phone = $('#billing_phone').val();
		var billing_address_1 = $('#billing_address_1').val();
		var calc_shipping_country = $('#calc_shipping_country').val();
		var calc_shipping_state = $('#billing_city').val();
		var billing_postcode = $('#billing_postcode').val();
		var payment_method_cheque = $('input[name=payment_method]:checked').val();
		//var ship_val = 
		var terms = $('#terms').val();
		
			var form_data = {
				//con_name:con_name,
				ship_val: val,
				shipping_country:shipping_country,
				shipping_city:shipping_city,
				first_name:first_name,
				last_name:last_name,
				billing_email:billing_email,
				billing_phone:billing_phone,
				billing_address_1:billing_address_1,
				calc_shipping_country:calc_shipping_country,
				calc_shipping_state:calc_shipping_state,
				billing_postcode:billing_postcode,
				payment_method_cheque:payment_method_cheque,
				terms:terms,
			}
		/*var form_data= {
			val: val,
		first_name: firstName
		}
		
		$.ajax({
			url: base_url+"cart/shipLocal",
			type: 'POST',
			data: form_data,
			success: function(msg){
				$("#country").html("");
				$("#show_shipping_state").html(msg);
				$("#loading").hide();
			}
		});*/
		//alert('Your shipping has been removed!');
		$.ajax({
			url: base_url+"cart/change_ship",
			type: 'POST',
			data: form_data,
			success: function(msg) {
				//alert(billing_first_name);
				$("#billing_address_1").val("hello");
				//location.reload();
				window.location=base_url+"checkout";
				document.getElementById("payment_method_cheque").checked = 'checked';
				//$('#amount-row').html('<span class="amount" id="shipping-amount">No Shipping</span>');
			}
		});
		//return false;
	});
	$(".canc_buynow").click(function() {
		$.ajax({
			url: base_url+"cart/cancel_buynow",
			type: 'POST',
			success: function(msg) {
				location.reload();     
			}
		});
		return false;
	});
	$(".change_ship").click(function() {
		$.ajax({
			url: base_url+"cart/change_ship",
			type: 'POST',
			success: function(msg) {
				location.reload();     
			}
		});
		return false;
	});
	$("#submit_review").click(function() {
		var comment = $('#comment').val();
		var product_id = $('#product_id').val();
		var user_id = $('#user_id').val();
		var form_data = {
			comment: comment,
			product_id: product_id,
			user_id: user_id    
		}
		if (comment=="" || product_id==""){
			alert('Field Required ): Please enter your comments');
		}else{
			$.ajax({
				url: base_url+"comment/index",
				type: 'POST',
				data: form_data,
				success: function(msg) {
					if (msg == "no"){   
						$('#msg_review').html('Invalid! please try again later...');
					}
					if(msg=="no_log"){
						$('#msg_review').html('Please login to enter your comments');
					}
					if (msg == "yes"){	
						$('#msg_review').html('Thank you! For your comments ):');
					}               
				}
			});
		}
		return false;
	});

	$(".btn-comment-reply").click(function(event) {
		event.preventDefault();
		var comment = $(this).parent().find('#comment-reply').val();
		var product_id = $(this).parent().find('#product_id').val();
		var user_id = $(this).parent().find('#user_id').val();
		var comment_parent_id = $(this).parent().find('#comment_parent_id').val();
		var form_data = {
			comment: comment,
			product_id: product_id,
			user_id: user_id,
			comment_parent_id: comment_parent_id
		}
		if (product_id==""){
			alert('Field Required ): Please enter your comments');
		}else{
			$.ajax({
				url: base_url+"comment/treeComment",
				type: 'POST',
				data: form_data,
				success: function(msg) {
					if (msg == "no"){   
						$('#error-msg').html('Invalid! please try again later...');
					}else if(msg=="no_log"){
						$('#error-msg').attr("class","alert alert-warning");
						$('#error-msg').html('Please login to enter your comments');
					}else if (msg == "yes"){
						$('#error-msg').html('Thank you! For your comments ):');
					}else{
						$('#error-msg').attr("class","alert alert-warning");
						$('#error-msg').html(msg);
					}              
				}
			});
		}
		return false;
	});
	$('#newsletter-signup').click(function(event){
		event.preventDefault();
		var email = $('#email').val();
		var form_data={
			email: email
		}
		$.ajax({
			url: base_url+'newsletter/index',
			type: 'POST',
			data: form_data,
			success: function(msg){
				$('.error').html(msg);
			}
		});
		return false;
	});
	
	$('a.reply').click(function(event){
		//$(this).find('.tree').attr('id', 'mytree');
		//$(this).find('.tree').css('display','block');
		//$(this).html("");
		event.preventDefault();
		$(this).next('.reply-comment').fadeToggle();
			//$(this).append("<input type='text' name='tree' class='tree'/>");
		
		
	});
	$('#btn-bid').click(function(){
		var product_id = $('#product_id').val();
		var bidValue = $('#bid_price').val();
		var bidData = {
			product_id:product_id,
			bidVal: bidValue
		}
		if(bidValue == ""){
			alert('Please enter price to bid');
		}else{
			$.ajax({
				url: base_url+"product_detail/independent",
				type: 'POST',
				data: bidData,
				success: function(msg){
					if(msg=="no"){
						$('#error-bid').html("")
						$('#error-bid').html("The next bid must be bigger than previous bid or sale price.");
					}
					if(msg=="small"){
						$('#error-bid').html("")
						$('#error-bid').html("The next bid must be bigger than previous bid.");
					}
					if(msg=="yes"){
						$('#error-bid').html("")
						$('#error-bid').html('Your bid has been successfully submitted. Thank you!');
					}
					if(msg=="no_log"){
						$('#error-bid').html("")
						$('#error-bid').html('Please login to bid!');
					}
				}
			})
		}
		return false;
	});
	$("#city").change(function() {
		var city =  $('#city').val();
		var form_data = {
			city: city 
		}
		$.ajax({
			url: base_url+"admin/shipping/fitler_method",
			type: 'POST',
			data: form_data,
			success: function(msg) {
				$("#contain_zone").html(msg);
			}
		});
		return false;
	});
	$(".add_cond").click(function() {
		
		var list='<div class="row" id="re"><div class="form-group">'+
					'<div class="col-md-3">'+
						'<label></label>'+
						'<input class="form-control minweight" type="text" name="minweight[]"/>'+
					'</div>'+
					'<div class="col-md-3">'+
						'<label></label>'+
						'<input class="form-control maxweight" type="text" name="maxweight[]"/>'+
					'</div>'+
					'<div class="col-md-3">'+
						'<label></label>'+
						'<input class ="form-control feeamount" type="text" name="feeamount[]"/>'+
					'</div>'+
					'<div class="col-md-3">'+
						'<br/><a href="#" class="btn default btn-xs red rem_cond"  title="Delete"><i class="fa fa-minus"></i> Remove </a>'+
					'</div>'+
				'</div></div>';
			$(".add_morecod").append(list);

			var count = jQuery('div#re').size();
			$('a.rem_cond').click(function(event){
				var v = $(this).parent().parent().parent();
				v.remove();
			});
	});
	$('#cal_shiping').click(function(){
		//alert('hello');
		var con_name = $('.con_name:checked').val();
		var shipping_country = $('#shipping_country').val();
		var shipping_city = $('#shipping_city').val();
		//
		var shipping = $('#payment_method_bacs').val();
		var first_name = $('#first_name').val();
		var last_name = $('#last_name').val();
		var billing_email = $('#billing_email').val();
		var billing_phone = $('#billing_phone').val();
		var billing_address_1 = $('#billing_address_1').val();
		var calc_shipping_country = $('#calc_shipping_country').val();
		var calc_shipping_state = $('#calc_shipping_state').val();
		var billing_postcode = $('#billing_postcode').val();
		var payment_method_cheque = $('input[name=payment_method]:checked').val();
		//alert(con_name);
		//return false;
		var terms = $('#terms').val();
		if(!con_name){
			alert('Please choose shipping company!...');
		}else if(shipping_country =="" || shipping_city=="" ){
			alert('Please select Country and City...');
		}else{
			var form_data = {
				con_name:con_name,
				shipping_country:shipping_country,
				shipping_city:shipping_city,
				shipping:shipping,
				first_name:first_name,
				last_name:last_name,
				billing_email:billing_email,
				billing_phone:billing_phone,
				billing_address_1:billing_address_1,
				calc_shipping_country:calc_shipping_country,
				calc_shipping_state:calc_shipping_state,
				billing_postcode:billing_postcode,
				payment_method_cheque:payment_method_cheque,
				terms:terms,
			}
			$.ajax({
				url: base_url+"cart/update_shipping",
				type: 'POST',
				data: form_data,
				success: function(msg) {
					if(msg=="yes"){
						$('#display_shipping').html("")
						$('#display_shipping').html(msg);
					}else if(msg=="no"){
						$('#display_shipping').html("")
						$('#display_shipping').html(msg);
					}else{
						$("#display_shipping").html(msg);
						 location.reload();
						 //alert(msg);
					}
				}
			});
		}

		return false;
	});
	/*$("#calc_shipping_country").change(function(){
		var parent_id=$(this).val();
		if(parent_id ==""){
			alert('Please select Country');
		}else{
			var form_data = {
				parent_id:parent_id
			}
			$.ajax({
				url: base_url+"cart/city_bycountry/"+parent_id,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					$("#calc_shipping_state_field").html(msg);
				}
			});
		}
		return false;
	});*/
	$('#3').click(function(){
		//event.preventDefault();
		var val = $(this).val();
		var form_data= {
			val: val
		}
		$.ajax({
			url: base_url+"cart/shipLocal",
			type: 'POST',
			data: val,
			success: function(msg){
				$("#country").html("");
				$("#show_shipping_state").html(msg);
			}
		});
		
		
	});
	$("#shipping_country").change(function(){
		var parent_id=$(this).val();
		if(parent_id ==""){
			alert('Please select Country');
		}else{
			var form_data = {
				parent_id:parent_id
			}
			$.ajax({
				url: base_url+"cart/ship_city/"+parent_id,
				type: 'POST',
				data: form_data,
				success: function(msg) {
					$("#show_shipping_state").html(msg);
				}
			});
		}
		return false;
	});
	$("#shipping_country").change(function(){
		var country=$(this).val();
		return false;
		var form_data = {
			country:country
		}
		$.ajax({
			url: base_url+"cart/update_shipping/"+country,
			type: 'POST',
			data: form_data,
			success: function(msg) {
			//	$("#calc_shipping_state_field").html(msg);
				if(msg=="yes"){
					alert('Your Condition has been successfully submitted. Thank you!');
				}else{
					alert('fail');
				}
			}
		});
		return false;
	});
	//-----------cart-------------
	$(".remove_allcart").click(function(){
		var result = confirm('Are you sure want to clear all Products?');
		if (result) {
			window.location = base_url+"cart/remove_allcart";
		} else {
			return false; // cancel button
		}		
	});
	// This button will increment the value
    $('#submit').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(".cartqty").val();
		if( fieldName <50){
			// Get its current value
			var currentVal = parseInt($('.cartqty').val());
			// If is not undefined
			if (!isNaN(currentVal)) {
				// Increment
				$('.cartqty').val(currentVal + 1);
			/*	var row_id=$(this).attr('id');
				var upqty=$(this).attr('value');
				$.ajax({
					type:'POST',
					url: base_url+'cart/update_qty',
					type: 'POST',
					data: {
						row_id:row_id,
						upqty:upqty
					},
					success:function(data){
						location.reload();
					}
				});*/
			} else {
				// Otherwise put a 0 there
				$('.cartqty').val(1);
			}
		}else{
			alert('You can\'t enter qty more than 50.');
		}
    });
    
    
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).parent().find(".cartqty").val();
        // Get its current value
        var currentVal = parseInt($(this).parent().find('.cartqty').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
			$(this).parent().find('.cartqty').val(currentVal - 1);
			if($('.cartqty').val()==0){
				$('.cartqty').val(1);
			}
        } else {
            // Otherwise put a 0 there
            $('.cartqty').val(1);
        }
    });
    // This button will increment the value till 0
    $(".qtyplus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).parent().find(".cartqty").val();
        // Get its current value
        var currentVal = parseInt($(this).parent().find('.cartqty').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
			$(this).parent().find('.cartqty').val(currentVal + 1);
			if($('.cartqty').val()==0){
				$('.cartqty').val(1);
			}
        } else {
            // Otherwise put a 0 there
            $('.cartqty').val(1);
        }
    });
    
	$(".input-sm").change(function(){
		var row_id=$(this).attr('id');
		var upqty=$(this).val();
		$.ajax({
			type:'POST',
			url: base_url+'page/update_qty',
			type: 'POST',
			data: {
                row_id:row_id,
				upqty:upqty
            },
			success:function(data){
				location.reload();
			}
		});
		
	});
       $(".num_only").keypress(function(evt){
		 var charCode = (evt.which) ? evt.which : event.keyCode
		 if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;

		 return true;
	});
	//..........only number............
	function isNumberKey(evt){
	 var charCode = (evt.which) ? evt.which : event.keyCode
	 if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	 return true;
	}
	//.......only number and dot...........
	function isdotKey(evt){
	  var charCode = (evt.which) ? evt.which : event.keyCode
	  if (charCode != 46 && charCode > 31 
		&& (charCode < 48 || charCode > 57))
		 return false;

	  return true;
	}
	//-----------enter number only-------------	
	function enter_num_only(element){
		$(element).keydown(function(event) {
		// Allow: backspace, delete, tab and escape
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
				 // Allow: Ctrl+A
				(event.keyCode == 65 && event.ctrlKey === true) || 
				 // Allow: home, end, left, right
				(event.keyCode >= 35 && event.keyCode <= 39)) {
					 // let it happen, don't do anything
					 return;
			}
			else {
				// Ensure that it is a number and stop the keypress
				if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
					event.preventDefault(); 
				}   
			}
		});
	}
	//-----------active menu----
	$(".search_menu li").on('click',function(){
		$('.search_menu li.active').removeClass('active');
		$(this).addClass("active");
	});
//	$('.search_menu ul > li').addClass('active');
	//-----------
	$('.date_picker').datepicker({
		format: "yyyy-mm-dd"
	}); 
	//$('.clockpicker').clockpicker();
	$('.timepicker-24').timepicker({
        autoclose: true,
        minuteStep: 5,
        showSeconds: true,
        showMeridian: false
    });
	/*--------check discount---------*/
	
	$("#have_disc").click(function() {
		if ( $('input[name="have_disc"]').is(':checked') ) {
			$('.is_dis').removeClass("show_have_dis");
		}else {
			$('.is_dis').addClass("show_have_dis");
		}
	});
	/*-----------change language----*/
	$(".lang").click(function() {
		var id = $(this).attr('id');
		$.ajax({
			type:'POST',
			url: base_url+'account/switch_language/index/'+id,
			success:function(data){
				location.reload();
			}
		});
	});
	$(".lang").change(function() {
		var id = $(this).val();
		$.ajax({
			type:'POST',
			url: base_url+'account/switch_language/index/'+id,
			success:function(data){
				location.reload();
			}
		});
	});
	$(".div_topopup").click(function() {
		var fea_id=$(this).attr('id');
		$('#get_fea_g').val(fea_id);
			loading_pop(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
			loadPopup_(); // function show popup 
			}, 500); // .5 second
		return false;

	});
	
	/* event for closeme the popup */
	$("div.closeme").hover(
		function() {
			$('span.ecs_tooltip').show();
		},
		function () {
			$('span.ecs_tooltip').hide();
		}
	);
	
	$("div.closeme").click(function() {
		disablePopup();  // function closeme pop up
	});
	
	$(this).keyup(function(event) {
		if (event.which == 27) { // 27 is 'Ecs' in the keyboard
			disablePopup();  // function closeme pop up
		}  	
	});
	
	$('a.livebox').click(function() {
		alert('Hello World!');
	return false;
	});
	

	 /************** start: functions. **************/
	function loading_pop() {
		$("div.loader_pop").show();  
	}
	function closemeloading() {
		$("div.loader_pop").fadeOut('normal');  
	}
	
	var popupStatus = 0; // set value
	
	function loadPopup_() { 
		if(popupStatus == 0) { // if value is 0, show popup
			closemeloading(); // fadeout loading
			$("#toPopup_").fadeIn(0500); // fadein popup div
			$("#backgroundPopup_").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup_").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}	
	}
		
	function disablePopup() {
		if(popupStatus == 1) { // if value is 1, closeme popup
			$("#toPopup_").fadeOut("normal");  
			$("#backgroundPopup_").fadeOut("normal");  
			popupStatus = 0;  // and set value to 0
		}
	}
	/************** end: functions. **************/
	$('.thumnail').hover(function() {
		$('.thumnail').removeClass("img_selected");
		$(this).addClass("img_selected");
	});
	$('.thumnail').click(function() {
		var id_feature=$('#get_fea_g').val();
		var img_id=$(this).attr('id');
		var dis_img='<img src="'+img_id+'" class="dis_img">';
		if(id_feature=='feature_img'){
			$('.disply_img').html(dis_img);
			$('#tem_fea').val(img_id);
		}else{
			$('.disply_img').append(dis_img);
		}
	});
	$('.upload_url').click(function() {
		var getup_tab=$(this).attr('id');
		$('#get_uptab').val(getup_tab);
	});
	$('#insert_post').click(function() {
		var id_feature=$('#get_fea_g').val();
		if(id_feature=='feature_img'){
			var ff_img= $('#tem_fea').val();
			if(ff_img==""){
				$('#show_feature').html('');
			}else{
				var img='<img src="'+ff_img+'" class="feature_image">';
				$('#show_feature').html(img);
			}
			disablePopup();
		}else{
			alert('g');
		}

		/*
		var product_url=$('#product_url').val();
		var tmpImg = new Image();
		tmpImg.src=product_url;
		$(tmpImg).one('load',function(){
		  imgWidth = tmpImg.width;
		  imgHeight = tmpImg.height;
		}); 
		var ext = product_url.substring(product_url.lastIndexOf('.') + 1);
		if(ext == "gif" || ext == "GIF" || 
				ext == "png" || ext == "PNG" ||
				ext == "jpg" || ext == "JPG" ||
				ext == "BMP" || ext == "bmp" ||
				ext == "TIFF" || ext == "tiff"){
			return true;
		}else{
			alert("All images only");
			return false;
		}
		
		var img='<img src="'+product_url+'">';
		$('#show_feature').html(img);
		
		*/
	});
	var maxAppend = 0;

	$('.add_tags').click(function() {
		var tag_id = $("#get_tags").val();
		if(tag_id ==""){
			return false;
		}else{
			var list='<li><a class="button_grey" href="#">'+tag_id+'</a>'+
					'<i class="tag_re glyphicon glyphicon-remove-circle"></i>'+
					'<input type="hidden" name="tags_con[]" value="'+tag_id+'"></li>';
			$(".tags_container ul").append(list);
		}
	});
	$(document).on('click','.tag_re', function(){
        $(this).parent('.tags_container ul li').remove();
    });
	

	/*-----------------upload----------------**/
/*	$('#photoimg').blind('click').live('change', function(){ 
		//$("#preview").html('');
		$("#imageform").ajaxForm({target: '#preview', 
			 beforeSubmit:function(){ 
			
			console.log('ttest');
			$("#imageloadstatus").show();
			 $("#imageloadbutton").hide();
			 }, 
			success:function(){ 
			console.log('test');
			 $("#imageloadstatus").hide();
			 $("#imageloadbutton").show();
			}, 
			error:function(){ 
			console.log('xtest');
			 $("#imageloadstatus").hide();
			$("#imageloadbutton").show();
			} }).submit();
	});*/
	//---------scroll to top------*/
	$('.scrollup').bind("click", function(event) {
		if ($(this).scrollTop() > 100) {
			$('.scrollup').fadeIn();
		} else {
			$('.scrollup').fadeOut();
		}
	}); 
	$('.scrollup').bind("click", function(event) {
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});
	
	$('.fullscreen').click(function(e){
		$('#full').toggleClass('fullscreen'); 
	});
	//------left menu--
	$(".amenu .category").mouseover(function(){
	  $(".amenu .sub-menu").hide();
	  $($(this).children(".sub-menu")).show();
	});

	$(".amenu .sub-menu").mouseout(function(){
	  $(".amenu .sub-menu").hide();
	});
	$(".amenu").mouseout(function(){
	  $(".amenu .sub-menu").hide();
	});
	//----------------check------------ 
	$("#pass1").keyup(function(){
		var password=$(this).val();
		var desc = new Array();
		desc[0] = "Very Weak";
		desc[1] = "Weak";
		desc[2] = "Better";
		desc[3] = "Medium";
		desc[4] = "Strong";
		desc[5] = "very Strong";

		var score   = 0;

		//if password bigger than 6 give 1 point
		if (password.length > 6) score++;

		//if password has both lower and uppercase characters give 1 point	
		if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;

		//if password has at least one number give 1 point
		if (password.match(/\d+/)) score++;

		//if password has at least one special caracther give 1 point
		if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) )	score++;

		//if password bigger than 12 give another 1 point
		if (password.length > 12) score++;

		 document.getElementById("passwordDescription").innerHTML = desc[score];
		 document.getElementById("passwordStrength").className = "strength" + score;
	});
	$("#pass2").keyup(function(){
		var pass1 = document.getElementById('pass1');
		var pass2 = document.getElementById('pass2');
		var message = document.getElementById('confirmMessage');
		//Set the colors we will be using ...
		var goodColor = "#B8242A";
		var badColor = "#B8242A";
		if(pass1.value == pass2.value){
			pass2.style.border = goodColor;
			message.style.color = goodColor;
			message.innerHTML = "Passwords Match!"
		}else{
			pass2.style.border = badColor;
			message.style.color = badColor;
			message.innerHTML = "Passwords Do Not Match!"
		}
		
	});
	//-----------tiny editor-------------
	/*
	tinymce.init({
            selector: ".tinyeditor",
			relative_urls:false,
			remove_script_host:false,
			plugins: [
				"advlist autolink lists link image charmap print preview jbimages anchor sh4tinymce",
				"searchreplace visualblocks code fullscreen pagebreak",
				"insertdatetime media table contextmenu paste textcolor responsivefilemanager"
			],
			toolbar: " fontsizeselect | forecolor backcolor |  bullist numlist outdent indent | responsivefilemanager",
			height : 300,
			fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
			  //document_base_url: "http://www.site.com/path1/",
			   remove_script_host: false,
			   relative_urls: false,
			image_advtab: true,
			image_class_list: [
				{title: 'None', value: ''},
				{title: 'Text-center', value: 'text-center'},
				{title: 'Image Responsive', value: 'img-responsive'}
			],
			// forced_root_block : false
			invalid_elements : "script",
			external_filemanager_path:base_url+"manange_file/",
			filemanager_title:"SBC Upload" ,
			external_plugins: { "filemanager" : base_url+"manange_file/plugin.min.js"}
    });
	*/
	$("#file-1").fileinput({
        uploadUrl: '#', // you must set a valid URL here else you will get an error
        allowedFileExtensions : ['jpg', 'png','gif'],
        overwriteInitial: false,
        maxFileSize: 1000,
        maxFilesNum: 10,
        //allowedFileTypes: ['image', 'video', 'flash'],
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
	});
	$("#file-4").fileinput({
		uploadUrl: '#', // you must set a valid URL here else you will get an error
        allowedFileExtensions : ['jpg', 'png','gif'],
        overwriteInitial: false,
        maxFileSize: 1000,
        maxFilesNum: 10,
        //allowedFileTypes: ['image', 'video', 'flash'],
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
	}); 
	$("#fileupload").change(function () {
		if (typeof (FileReader) != "undefined") {
			var dvPreview = $("#dvPreview");
			dvPreview.html("");
			var regex = /^([a-zA-Z0-9()\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
			var i =0;
			if(this.files.length>5){
				alert('Only 5 images are allowed!');
				//$('.fileinput-new').css('display','block');
				//$('.fileinput-exists').css('display','none');
				//$('.fileinput-new').css('display','none');
			}
			else{
				$($(this)[0].files).each(function () {
					var file = $(this);
					if (regex.test(file[0].name.toLowerCase())) {
						var reader = new FileReader();
						reader.onload = function (e) {
							var img = $("<img />");
							img.attr("style", "height:100px;width: 100px");
							img.attr("src", e.target.result);
							
							dvPreview.append(img);
						}
						//alert(i);
						reader.readAsDataURL(file[0]);
					} else {
						alert(file[0].name + " is not a valid image file.");
						dvPreview.html("");
						return false;
					}
					i++;
				});
			}
		} else {
			alert("This browser does not support HTML5 FileReader.");
		}
	});
	
	$("#fileupload-new").on('change', function () {
		var prev = $(this).data('val');
		//alert($(this).data('val'));
		if (typeof (FileReader) != "undefined") {
			var dvPreview = $("#dvPreview");
			
			dvPreview.html("");
			var regex = /^([a-zA-Z0-9()\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
			var i =0;
			if(this.files.length>5){
				alert('Only 5 images are allowed!');
				//$('.fileinput-new').css('display','block');
				//$('.fileinput-exists').css('display','none');
				//$('.fileinput-new').css('display','none');
				//return false;
			}
			
			//else{
				$($(this)[0].files).each(function () {
				var file = $(this);
				
				if(i<=4){
					if (regex.test(file[0].name.toLowerCase())) {
						var reader = new FileReader();
						reader.onload = function (e) {
							var img = $("<img />");
							img.attr("style", "height:100px;width: 100px");
							img.attr("src", e.target.result);
								dvPreview.append(img);
						}
						//alert(i);
						//alert(prev);
						reader.readAsDataURL(file[0]);
						
					} 
					
					else {
						alert(file[0].name + " is not a valid image file.");
						//location.reload()
						//dvPreview.html("");
						return false;
					}
				}
				
				i++;
				//alert(i);
				});
			//}
			
		} else {
			alert("This browser does not support HTML5 FileReader.");
		}
	});
	//-------------close cart----------
	//--------sort table----------------
	//$(".sort-table").tablesorter( {sortList: [[0,0], [1,0]]} ); 
	//-------google analytic-------
	
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73243216-1', 'auto');
  ga('send', 'pageview');
}); 
// jQuery End